from typing import TYPE_CHECKING
from uuid import UUID

from sqlalchemy import ForeignKey, String
from sqlalchemy.dialects.mysql import INTEGER
from sqlalchemy.orm import Mapped, mapped_column, relationship

from clowm.db.base_class import Base
from clowm.db.types import BucketPermissionScopesDB, SQLIntFlag

if TYPE_CHECKING:
    from .bucket import Bucket
    from .user import User


class BucketPermission(Base):
    """
    Database model for the permission for a user on a bucket.
    Will be deleted if either the user or the bucket is deleted.
    """

    __tablename__: str = "bucketpermission"
    uid: Mapped[UUID] = mapped_column(ForeignKey("user.uid", ondelete="CASCADE"), name="uid", primary_key=True)
    bucket_name: Mapped[str] = mapped_column(ForeignKey("bucket.name", ondelete="CASCADE"), primary_key=True)
    from_: Mapped[int | None] = mapped_column("from", INTEGER(unsigned=True), nullable=True)
    to: Mapped[int | None] = mapped_column(INTEGER(unsigned=True), nullable=True)
    file_prefix: Mapped[str | None] = mapped_column(String(512), nullable=True)
    scopes: Mapped[BucketPermissionScopesDB] = mapped_column(
        SQLIntFlag(flag_type=BucketPermissionScopesDB), default=BucketPermissionScopesDB(1), nullable=False
    )
    grantee: Mapped["User"] = relationship("User", back_populates="permissions")
    bucket: Mapped["Bucket"] = relationship("Bucket", back_populates="permissions")

    def __repr__(self) -> str:
        return f"BucketPermission(uid='{self.uid}' bucket_name='{self.bucket_name}', scopes='{self.scopes.render()}')"
