import time
from datetime import time as timeduration
from datetime import timedelta
from enum import StrEnum, unique
from typing import TYPE_CHECKING, Any
from uuid import UUID

from sqlalchemy import Boolean, ForeignKey, String
from sqlalchemy.dialects.mysql import ENUM, INTEGER, SMALLINT, TEXT, TIME
from sqlalchemy.orm import Mapped, mapped_column, relationship

from clowm.db.base_class import Base, uuid7
from clowm.db.types import SQLUUID

if TYPE_CHECKING:
    from .workflow_version import WorkflowVersion


class WorkflowExecution(Base):
    """
    Database model for an execution of a workflow.
    """

    @unique
    class WorkflowExecutionStatus(StrEnum):
        """
        Enumeration for the status on a workflow execution.
        """

        PENDING = "PENDING"
        SCHEDULED = "SCHEDULED"
        RUNNING = "RUNNING"
        CANCELED = "CANCELED"
        SUCCESS = "SUCCESS"
        ERROR = "ERROR"

        @staticmethod
        def active_workflows() -> list["WorkflowExecution.WorkflowExecutionStatus"]:
            return [
                WorkflowExecution.WorkflowExecutionStatus.PENDING,
                WorkflowExecution.WorkflowExecutionStatus.SCHEDULED,
                WorkflowExecution.WorkflowExecutionStatus.RUNNING,
            ]

        @staticmethod
        def inactive_workflows() -> list["WorkflowExecution.WorkflowExecutionStatus"]:
            return [
                WorkflowExecution.WorkflowExecutionStatus.CANCELED,
                WorkflowExecution.WorkflowExecutionStatus.SUCCESS,
                WorkflowExecution.WorkflowExecutionStatus.ERROR,
            ]

    __tablename__: str = "workflow_execution"

    execution_id: Mapped[UUID] = mapped_column(
        SQLUUID,
        name="execution_id",
        primary_key=True,
        index=True,
        unique=True,
        default=lambda: uuid7(),
    )
    executor_id: Mapped[UUID | None] = mapped_column(
        ForeignKey("user.uid", ondelete="SET NULL"), name="executor_id", nullable=True
    )
    workflow_version_id: Mapped[str | None] = mapped_column(
        ForeignKey("workflow_version.git_commit_hash", ondelete="SET NULL"), nullable=True
    )
    workflow_mode_id: Mapped[UUID | None] = mapped_column(
        ForeignKey("workflow_mode.mode_id", ondelete="SET NULL"), name="workflow_mode_id", nullable=True
    )
    debug_path: Mapped[str | None] = mapped_column(String(1024), nullable=True)
    logs_path: Mapped[str | None] = mapped_column(String(1024), nullable=True)
    provenance_path: Mapped[str | None] = mapped_column(String(1024), nullable=True)
    start_time: Mapped[int] = mapped_column(INTEGER(unsigned=True), nullable=False, default=lambda: round(time.time()))
    end_time: Mapped[int | None] = mapped_column(INTEGER(unsigned=True), nullable=True)
    cpu_hours: Mapped[timeduration] = mapped_column(TIME(False, 3), default=lambda: timeduration(), nullable=False)
    cpu_days: Mapped[int] = mapped_column(SMALLINT(unsigned=True), default=0, nullable=False)
    status: Mapped[WorkflowExecutionStatus] = mapped_column(
        ENUM(WorkflowExecutionStatus), default=WorkflowExecutionStatus.PENDING, nullable=False
    )
    notes: Mapped[str | None] = mapped_column(TEXT, nullable=True)
    slurm_job_id: Mapped[int] = mapped_column(INTEGER(unsigned=True), nullable=False)
    deleted: Mapped[bool] = mapped_column(Boolean, nullable=False, default=False)
    workflow_version: Mapped["WorkflowVersion"] = relationship("WorkflowVersion", back_populates="workflow_executions")

    @property
    def cpu_time(self) -> timedelta:
        return timedelta(
            days=self.cpu_days,
            hours=self.cpu_hours.hour,
            minutes=self.cpu_hours.minute,
            seconds=self.cpu_hours.second,
            microseconds=self.cpu_hours.microsecond,
        )

    @cpu_time.setter
    def cpu_time(self, value: timedelta) -> None:
        total_seconds = value.seconds
        hours, remainder = divmod(total_seconds, 3600)
        minutes, remainder = divmod(remainder, 60)
        seconds = int(remainder)
        self.cpu_days = value.days
        self.cpu_hours = timeduration(hour=hours, minute=minutes, second=seconds, microsecond=value.microseconds)

    def __repr__(self) -> str:
        return f"WorkflowExecution(execution_id={self.execution_id} user={self.executor_id} workflow_version={self.workflow_version_id})"

    def __eq__(self, other: Any) -> bool:
        return self.execution_id == other.execution_id if isinstance(other, WorkflowExecution) else False
