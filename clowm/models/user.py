from typing import TYPE_CHECKING, Any
from uuid import UUID

from sqlalchemy import Boolean, String
from sqlalchemy.dialects.mysql import INTEGER, TINYINT
from sqlalchemy.orm import InstrumentedAttribute, Mapped, mapped_column, relationship

from clowm.core.oidc.client import OIDCProvider
from clowm.db.base_class import Base, uuid7
from clowm.db.types import SQLUUID, Token

from .role import Role, RoleIdMapping, UserRoleMapping

if TYPE_CHECKING:
    from .bucket_permission import BucketPermission


class User(Base):
    """
    Database model for a user.
    """

    __tablename__: str = "user"
    uid: Mapped[UUID] = mapped_column(
        SQLUUID,
        name="uid",
        primary_key=True,
        index=True,
        unique=True,
        default=lambda: uuid7(),
    )
    lifescience_id: Mapped[str | None] = mapped_column(String(64), index=True, unique=True, nullable=True)
    nfdi_id: Mapped[str | None] = mapped_column(String(64), index=True, unique=True, nullable=True)
    display_name: Mapped[str] = mapped_column(String(256), nullable=False)
    email: Mapped[str | None] = mapped_column(String(256), nullable=True)
    aruna_token: Mapped[str | None] = mapped_column(String(128), nullable=True)
    permissions: Mapped[list["BucketPermission"]] = relationship(
        "BucketPermission",
        back_populates="grantee",
        cascade="all, delete",
        passive_deletes=True,
    )
    initialized: Mapped[bool] = mapped_column(Boolean, default=False, nullable=False)
    max_parallel_executions: Mapped[int | None] = mapped_column(TINYINT(unsigned=True), nullable=True)
    roles: Mapped[list["UserRoleMapping"]] = relationship(
        back_populates="user", cascade="all, delete", passive_deletes=True
    )
    invitation_token: Mapped[str | None] = mapped_column(Token, nullable=True)
    invitation_token_created_at: Mapped[int | None] = mapped_column(INTEGER(unsigned=True), nullable=True)
    last_login: Mapped[int | None] = mapped_column(INTEGER(unsigned=True), nullable=True)

    def has_role(self, role: Role.RoleEnum) -> bool:
        mapping = RoleIdMapping()
        return mapping[role] in (db_role.role_id for db_role in self.roles)

    def provider_id(self, provider: OIDCProvider) -> str | None:
        return getattr(self, self.provider_id_column(provider).key)

    @property
    def registered_providers(self) -> list[OIDCProvider]:
        return [provider for provider in OIDCProvider if self.provider_id(provider) is not None]

    @classmethod
    def provider_id_column(cls, provider: OIDCProvider) -> InstrumentedAttribute[str | None]:
        match provider:
            case OIDCProvider.nfdi:
                return cls.nfdi_id
            case OIDCProvider.lifescience:
                return cls.lifescience_id
            case _:
                raise NotImplementedError(f"unknown provider {provider}")

    def __eq__(self, other: Any) -> bool:
        return self.uid == other.uid if isinstance(other, User) else False

    def __repr__(self) -> str:
        return f"User(uid='{self.uid}', display_name='{self.display_name}')"
