from typing import TYPE_CHECKING, Any
from uuid import UUID

from sqlalchemy import Boolean, ForeignKey, String
from sqlalchemy.orm import Mapped, mapped_column, relationship, synonym

from clowm.db.base_class import Base, uuid7
from clowm.db.mixins.ownership_transfer_mixin import OwnershipTransferRequestMixin, OwnershipTypeEnum
from clowm.db.types import SQLUUID

if TYPE_CHECKING:
    from .resource_version import ResourceVersion


class Resource(Base, OwnershipTransferRequestMixin):
    """
    Database model for a Resource.
    """

    __tablename__: str = "resource"
    resource_id: Mapped[UUID] = mapped_column(
        SQLUUID,
        name="resource_id",
        primary_key=True,
        index=True,
        unique=True,
        default=lambda: uuid7(),
    )
    clowm_id: Mapped[UUID] = synonym("resource_id")
    name: Mapped[str] = mapped_column(String(32), unique=True, nullable=False)
    short_description: Mapped[str] = mapped_column(String(264), nullable=False)
    source: Mapped[str] = mapped_column(String(264))
    private: Mapped[bool] = mapped_column(Boolean, default=True, nullable=False)
    maintainer_id: Mapped[UUID | None] = mapped_column(
        ForeignKey("user.uid", ondelete="SET NULL"), name="maintainer_id", nullable=True
    )
    clowm_owner_id: Mapped[UUID | None] = synonym("maintainer_id")
    versions: Mapped[list["ResourceVersion"]] = relationship(
        "ResourceVersion", back_populates="resource", cascade="all, delete", passive_deletes=True
    )

    def __eq__(self, other: Any) -> bool:
        return self.resource_id == other.resource_id if isinstance(other, Resource) else False

    def __repr__(self) -> str:
        return f"'Resource(id={self.resource_id}', name='{self.name}')"

    @property
    def target_properties(self) -> OwnershipTransferRequestMixin.TargetProperties:
        return OwnershipTransferRequestMixin.TargetProperties(
            name=self.name, description=self.short_description, type=OwnershipTypeEnum.RESOURCE
        )
