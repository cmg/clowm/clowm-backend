from typing import Any
from uuid import UUID

from sqlalchemy import ForeignKey, String
from sqlalchemy.dialects.mysql import INTEGER, TEXT
from sqlalchemy.orm import Mapped, mapped_column

from clowm.db.base_class import Base, uuid7

from ..db.types import SQLUUID, NewsCategory


class News(Base):
    """
    Database model for news.
    """

    __tablename__: str = "news"
    news_id: Mapped[UUID] = mapped_column(SQLUUID, primary_key=True, unique=True, index=True, default=lambda: uuid7())
    content: Mapped[str] = mapped_column(TEXT, nullable=False)
    title: Mapped[str] = mapped_column(String(256), nullable=False)
    important_till: Mapped[int | None] = mapped_column(INTEGER(unsigned=True), nullable=True)
    creator_id: Mapped[UUID | None] = mapped_column(
        ForeignKey("user.uid", ondelete="SET NULL"), name="creator_id", nullable=True
    )
    category: Mapped[NewsCategory]

    def __eq__(self, other: Any) -> bool:
        return self.news_id == other.news_id if isinstance(other, News) else False

    def __repr__(self) -> str:
        return f"News(id='{self.news_id}', title='{self.title}', creator='{self.creator_id}')"
