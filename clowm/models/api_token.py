import secrets
from datetime import datetime
from time import time
from typing import TYPE_CHECKING, Any
from uuid import UUID

from sqlalchemy import ForeignKey, String
from sqlalchemy.dialects.mysql import INTEGER
from sqlalchemy.orm import Mapped, mapped_column, relationship

from clowm.db.base_class import Base, uuid7
from clowm.db.types import SQLUUID, ApiTokenScopesDB, SQLIntFlag, Token

if TYPE_CHECKING:
    from .user import User


class ApiToken(Base):
    __tablename__ = "api_token"
    token_id: Mapped[UUID] = mapped_column(SQLUUID, primary_key=True, index=True, unique=True, default=lambda: uuid7())
    name: Mapped[str] = mapped_column(String(63), nullable=False)
    token: Mapped[str] = mapped_column(Token, default=lambda: secrets.token_urlsafe(32), nullable=False, index=True)
    uid: Mapped[UUID] = mapped_column(ForeignKey("user.uid", ondelete="CASCADE"), nullable=False)
    user: Mapped["User"] = relationship()
    expires_at: Mapped[int | None] = mapped_column(INTEGER(unsigned=True), nullable=True)
    last_used: Mapped[int | None] = mapped_column(INTEGER(unsigned=True), nullable=True)
    scopes: Mapped[ApiTokenScopesDB] = mapped_column(
        SQLIntFlag(flag_type=ApiTokenScopesDB), nullable=False, default=lambda: ApiTokenScopesDB(0)
    )

    @property
    def expired(self) -> bool:
        return False if self.expires_at is None else time() > self.expires_at

    def has_scope(self, scope: ApiTokenScopesDB) -> bool:
        return scope in self.scopes

    def __repr__(self) -> str:
        return f"ApiToken(id='{str(self.token_id)}' name='{self.name}' uid='{str(self.uid)}', scopes={self.scopes.render()} expires_at='{'Never' if self.expires_at is None else datetime.fromtimestamp(self.expires_at).strftime('%Y-%m-%d')}' last_used='{'Never' if self.last_used is None else datetime.fromtimestamp(self.last_used).strftime('%Y-%m-%d')}'))"

    def __eq__(self, other: Any) -> bool:
        return self.token_id == other.token_id if isinstance(other, ApiToken) else False
