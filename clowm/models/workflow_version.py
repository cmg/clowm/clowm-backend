from enum import StrEnum, unique
from typing import TYPE_CHECKING, Any, Final
from uuid import UUID

from sqlalchemy import JSON, Column, ForeignKey, String, Table
from sqlalchemy.dialects.mysql import ENUM, TEXT
from sqlalchemy.orm import Mapped, backref, mapped_column, relationship

from clowm.db.base_class import Base

if TYPE_CHECKING:
    from .workflow import Workflow
    from .workflow_execution import WorkflowExecution
    from .workflow_mode import WorkflowMode

workflow_mode_association_table: Final[Table] = Table(
    "workflow_modus_to_version_mapping",
    Base.metadata,
    Column(
        "workflow_version_commit_hash",
        ForeignKey("workflow_version.git_commit_hash", ondelete="CASCADE"),
        primary_key=True,
    ),
    Column("workflow_mode_id", ForeignKey("workflow_mode.mode_id", ondelete="CASCADE"), primary_key=True),
)


class WorkflowVersion(Base):
    """
    Database model for a Workflow Version.
    """

    @unique
    class WorkflowVersionStatus(StrEnum):
        """
        Enumeration for the possible status of a workflow version.
        """

        CREATED = "CREATED"
        DENIED = "DENIED"
        PUBLISHED = "PUBLISHED"
        DEPRECATED = "DEPRECATED"

    __tablename__: str = "workflow_version"
    git_commit_hash: Mapped[str] = mapped_column(String(40), primary_key=True, index=True, unique=True)
    version: Mapped[str] = mapped_column(String(10), nullable=False)
    status: Mapped[WorkflowVersionStatus] = mapped_column(
        ENUM(WorkflowVersionStatus), default=WorkflowVersionStatus.CREATED, nullable=False
    )
    icon_slug: Mapped[str | None] = mapped_column(String(48), nullable=True)
    parameter_extension: Mapped[dict | None] = mapped_column(JSON, nullable=True)
    nextflow_version: Mapped[str] = mapped_column(String(8), nullable=False)
    default_container: Mapped[str | None] = mapped_column(String(512), nullable=True)
    nextflow_config: Mapped[str | None] = mapped_column(TEXT, nullable=True)
    workflow_id: Mapped[UUID] = mapped_column(
        ForeignKey("workflow.workflow_id", ondelete="CASCADE"), name="workflow_id", nullable=False
    )
    workflow: Mapped["Workflow"] = relationship("Workflow", back_populates="versions")
    previous_version_hash: Mapped[str | None] = mapped_column(
        ForeignKey("workflow_version.git_commit_hash", ondelete="SET NULL"), nullable=True
    )
    previous_version: Mapped["WorkflowVersion"] = relationship(
        "WorkflowVersion", backref=backref("next_version", uselist=False), remote_side="WorkflowVersion.git_commit_hash"
    )
    workflow_executions: Mapped[list["WorkflowExecution"]] = relationship(
        "WorkflowExecution", back_populates="workflow_version"
    )
    workflow_modes: Mapped[list["WorkflowMode"]] = relationship(secondary=workflow_mode_association_table)

    def __eq__(self, other: Any) -> bool:
        return self.git_commit_hash == other.git_commit_hash if isinstance(other, WorkflowVersion) else False

    def __repr__(self) -> str:
        return f"'WorkflowVersion(hash={self.git_commit_hash}', version='{self.version}', status='{self.status}')"
