from typing import TYPE_CHECKING, Any, Optional
from uuid import UUID

from sqlalchemy import ForeignKey, String
from sqlalchemy.dialects.mysql import TEXT
from sqlalchemy.orm import Mapped, mapped_column, relationship, synonym

from clowm.db.base_class import Base, uuid7
from clowm.db.mixins.ownership_transfer_mixin import OwnershipTransferRequestMixin, OwnershipTypeEnum
from clowm.db.types import SQLUUID

if TYPE_CHECKING:
    from .workflow_version import WorkflowVersion


class Workflow(Base, OwnershipTransferRequestMixin):
    """
    Database model for a Workflow.
    """

    __tablename__: str = "workflow"
    workflow_id: Mapped[UUID] = mapped_column(
        SQLUUID,
        name="workflow_id",
        primary_key=True,
        index=True,
        unique=True,
        default=lambda: uuid7(),
    )
    clowm_id: Mapped[UUID] = synonym("workflow_id")
    name: Mapped[str] = mapped_column(String(63), unique=True, nullable=False)
    repository_url: Mapped[str] = mapped_column(String(256), nullable=False)
    short_description: Mapped[str] = mapped_column(TEXT, nullable=False)
    credentials_token: Mapped[str | None] = mapped_column(String(128))
    developer_id: Mapped[UUID | None] = mapped_column(
        ForeignKey("user.uid", ondelete="SET NULL"), name="developer_id", nullable=True
    )
    clowm_owner_id: Mapped[UUID | None] = synonym("developer_id")
    versions: Mapped[list["WorkflowVersion"]] = relationship(
        "WorkflowVersion", back_populates="workflow", cascade="all, delete", passive_deletes=True
    )

    def __eq__(self, other: Any) -> bool:
        return self.workflow_id == other.workflow_id if isinstance(other, Workflow) else False

    def __repr__(self) -> str:
        return f"'Workflow(id={self.workflow_id}', name='{self.name}', developer_id='{self.developer_id}')"

    @property
    def private_repository(self) -> bool:
        return self.credentials_token is not None

    @property
    def latest_version(self) -> Optional["WorkflowVersion"]:
        if len(self.versions) < 1:
            return None
        found_index = 0
        largest_timestamp = self.versions[found_index].created_at
        for index, version in enumerate(self.versions[1:]):
            if version.created_at > largest_timestamp:
                largest_timestamp = version.created_at
                found_index = index + 1
        return self.versions[found_index]

    @property
    def target_properties(self) -> OwnershipTransferRequestMixin.TargetProperties:
        return OwnershipTransferRequestMixin.TargetProperties(
            name=self.name, description=self.short_description, type=OwnershipTypeEnum.WORKFLOW
        )
