from enum import StrEnum, unique
from typing import TYPE_CHECKING, Any
from uuid import UUID

from sqlalchemy import ForeignKey, String
from sqlalchemy.dialects.mysql import BIGINT, ENUM, INTEGER
from sqlalchemy.orm import Mapped, mapped_column, relationship

from clowm.db.base_class import Base, uuid7
from clowm.db.types import SQLUUID

if TYPE_CHECKING:
    from .resource import Resource


class ResourceVersion(Base):
    """
    Database model for a Resource Version.
    """

    @unique
    class ResourceVersionStatus(StrEnum):
        """
        Enumeration for the possible status of a resource version.
        """

        RESOURCE_REQUESTED = "RESOURCE_REQUESTED"
        WAIT_FOR_REVIEW = "WAIT_FOR_REVIEW"
        DENIED = "DENIED"
        APPROVED = "APPROVED"
        SYNC_REQUESTED = "SYNC_REQUESTED"
        SYNCHRONIZING = "SYNCHRONIZING"
        SYNC_ERROR = "SYNC_ERROR"
        SYNCHRONIZED = "SYNCHRONIZED"
        SETTING_LATEST = "SETTING_LATEST"
        LATEST = "LATEST"
        CLUSTER_DELETING = "CLUSTER_DELETING"
        CLUSTER_DELETE_ERROR = "CLUSTER_DELETE_ERROR"
        S3_DELETING = "S3_DELETING"
        S3_DELETE_ERROR = "S3_DELETE_ERROR"
        S3_DELETED = "S3_DELETED"

        @property
        def public(self) -> bool:
            return self.name not in {
                ResourceVersion.ResourceVersionStatus.RESOURCE_REQUESTED.name,
                ResourceVersion.ResourceVersionStatus.WAIT_FOR_REVIEW.name,
                ResourceVersion.ResourceVersionStatus.DENIED.name,
                ResourceVersion.ResourceVersionStatus.S3_DELETING.name,
                ResourceVersion.ResourceVersionStatus.S3_DELETE_ERROR.name,
                ResourceVersion.ResourceVersionStatus.S3_DELETED.name,
            }

    __tablename__ = "resource_version"
    resource_version_id: Mapped[UUID] = mapped_column(
        SQLUUID,
        name="resource_version_id",
        primary_key=True,
        index=True,
        unique=True,
        default=lambda: uuid7(),
    )
    release: Mapped[str] = mapped_column(String(32), nullable=False)
    status: Mapped[ResourceVersionStatus] = mapped_column(
        ENUM(ResourceVersionStatus), default=ResourceVersionStatus.RESOURCE_REQUESTED, nullable=False
    )
    sync_finished_timestamp: Mapped[int | None] = mapped_column(INTEGER(unsigned=True), nullable=True)
    sync_slurm_job_id: Mapped[int | None] = mapped_column(INTEGER(unsigned=True), nullable=True)
    cluster_deleted_timestamp: Mapped[int | None] = mapped_column(INTEGER(unsigned=True), nullable=True)
    cluster_delete_slurm_job_id: Mapped[int] = mapped_column(INTEGER(unsigned=True), nullable=True)
    s3_deleted_timestamp: Mapped[int | None] = mapped_column(INTEGER(unsigned=True), nullable=True)
    set_latest_timestamp: Mapped[int | None] = mapped_column(INTEGER(unsigned=True), nullable=True)
    set_latest_slurm_job_id: Mapped[int | None] = mapped_column(INTEGER(unsigned=True), nullable=True)
    remove_latest_timestamp: Mapped[int | None] = mapped_column(INTEGER(unsigned=True), nullable=True)
    compressed_size: Mapped[int] = mapped_column(BIGINT(unsigned=True), nullable=False, default=0)
    resource_id: Mapped[UUID] = mapped_column(
        ForeignKey("resource.resource_id", ondelete="CASCADE"), name="resource_id", nullable=False
    )
    synchronization_request_uid: Mapped[UUID | None] = mapped_column(
        ForeignKey("user.uid", ondelete="SET NULL"), name="synchronization_request_uid", nullable=True
    )
    synchronization_request_description: Mapped[str | None] = mapped_column(String(512), nullable=True)
    last_used_timestamp: Mapped[int | None] = mapped_column(INTEGER(unsigned=True), nullable=True)
    times_used: Mapped[int] = mapped_column(INTEGER(unsigned=True), nullable=False, default=0)
    resource: Mapped["Resource"] = relationship("Resource", back_populates="versions")
    previous_resource_version_id: Mapped[UUID | None] = mapped_column(
        ForeignKey("resource_version.resource_version_id", ondelete="SET NULL"),
        name="previous_resource_version_id",
        nullable=True,
    )

    def __eq__(self, other: Any) -> bool:
        return self.resource_version_id == other.resource_version_id if isinstance(other, ResourceVersion) else False

    def __repr__(self) -> str:
        return f"'ResourceVersion(id={self.resource_version_id}', release='{self.release}', status='{self.status}', resource='{self.resource_id}')"
