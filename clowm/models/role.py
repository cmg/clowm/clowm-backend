from enum import StrEnum, unique
from threading import Lock
from typing import TYPE_CHECKING, Any
from uuid import UUID

from sqlalchemy import ForeignKey, String, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import Mapped, mapped_column, relationship

from clowm.db.base_class import Base, uuid7
from clowm.db.types import SQLUUID
from clowm.utils import Singleton

if TYPE_CHECKING:
    from .user import User


class Role(Base):
    """
    Database model for a user.
    """

    @unique
    class RoleEnum(StrEnum):
        ADMINISTRATOR = "administrator"
        USER = "user"
        REVIEWER = "reviewer"
        DEVELOPER = "developer"
        DB_MAINTAINER = "db_maintainer"

    __tablename__ = "role"
    role_id: Mapped[UUID] = mapped_column(
        SQLUUID, name="role_id", primary_key=True, index=True, unique=True, default=lambda: uuid7()
    )
    name: Mapped[str] = mapped_column(String(64), nullable=False)
    users: Mapped[list["UserRoleMapping"]] = relationship(
        back_populates="role", cascade="all, delete", passive_deletes=True
    )

    def __eq__(self, other: Any) -> bool:
        return self.role_id == other.role_id if isinstance(other, Role) else False

    def __repr__(self) -> str:
        return f"Role(id='{self.role_id}', name='{self.name}')"


class UserRoleMapping(Base):
    """
    Database model for the user to role mapping.
    """

    __tablename__ = "user_role_mapping"
    uid: Mapped[UUID] = mapped_column(ForeignKey("user.uid", ondelete="CASCADE"), name="uid", primary_key=True)
    role_id: Mapped[UUID] = mapped_column(
        ForeignKey("role.role_id", ondelete="CASCADE"), name="role_id", primary_key=True
    )
    user: Mapped["User"] = relationship(back_populates="roles")
    role: Mapped["Role"] = relationship(back_populates="users")

    def __eq__(self, other: Any) -> bool:
        return self.role_id == other.role_id and self.uid == other.uid if isinstance(other, UserRoleMapping) else False

    def __repr__(self) -> str:
        return f"UserRoleMapping(role_id='{self.role_id}', uid='{self.uid}')"


class RoleIdMapping(metaclass=Singleton):
    def __init__(self) -> None:
        self.lock = Lock()
        self._initialized = False
        self._role_id_mapping: dict[Role.RoleEnum, UUID] = {}
        self._id_role_mapping: dict[bytes, Role.RoleEnum] = {}

    async def load_role_ids(self, db: AsyncSession) -> None:
        if self.initialized:
            return
        roles = list((await db.scalars(select(Role))).all())
        with self.lock:
            if self.initialized:
                return

            self._role_id_mapping = {Role.RoleEnum(role.name): role.role_id for role in roles}
            self._id_role_mapping = {role.role_id.bytes: Role.RoleEnum(role.name) for role in roles}
            self._initialized = True

    @property
    def initialized(self) -> bool:
        return self._initialized

    def __getitem__(self, item: Role.RoleEnum) -> UUID:
        return self._role_id_mapping[item]

    def get_role_name(self, role_id: UUID) -> Role.RoleEnum:
        return self._id_role_mapping[role_id.bytes]
