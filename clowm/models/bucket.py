from typing import TYPE_CHECKING, Any
from uuid import UUID

from sqlalchemy import Boolean, ForeignKey, String
from sqlalchemy.dialects.mysql import INTEGER, TEXT
from sqlalchemy.orm import Mapped, mapped_column, relationship, synonym

from clowm.db.base_class import Base
from clowm.db.mixins.ownership_transfer_mixin import OwnershipTransferRequestMixin, OwnershipTypeEnum

if TYPE_CHECKING:
    from .bucket_permission import BucketPermission


class Bucket(Base, OwnershipTransferRequestMixin):
    """
    Database model for a bucket.
    """

    __tablename__: str = "bucket"
    name: Mapped[str] = mapped_column(String(63), primary_key=True, index=True, unique=True)
    clowm_id: Mapped[str] = synonym("name")
    description: Mapped[str] = mapped_column(TEXT, nullable=False)
    public: Mapped[bool] = mapped_column(Boolean(), default=False, nullable=False)
    owner_id: Mapped[UUID | None] = mapped_column(
        ForeignKey("user.uid", ondelete="SET NULL"), name="owner_id", nullable=True
    )
    clowm_owner_id: Mapped[UUID | None] = synonym("owner_id")
    permissions: Mapped[list["BucketPermission"]] = relationship(
        "BucketPermission",
        back_populates="bucket",
        cascade="all, delete",
        passive_deletes=True,
    )
    # size limit of bucket in KiB
    size_limit: Mapped[int | None] = mapped_column(INTEGER(unsigned=True), nullable=True)
    object_limit: Mapped[int | None] = mapped_column(INTEGER(unsigned=True), nullable=True)

    def __eq__(self, other: Any) -> bool:
        return self.name == other.name if isinstance(other, Bucket) else False

    def __repr__(self) -> str:
        return f"Bucket(name='{self.name}', owner='{self.owner_id}')"

    @property
    def target_properties(self) -> OwnershipTransferRequestMixin.TargetProperties:
        return OwnershipTransferRequestMixin.TargetProperties(
            name=self.name,
            description=self.description,
            type=OwnershipTypeEnum.BUCKET,
        )
