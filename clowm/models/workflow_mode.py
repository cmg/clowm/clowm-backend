from typing import Any
from uuid import UUID

from sqlalchemy import String
from sqlalchemy.orm import Mapped, mapped_column

from clowm.db.base_class import Base, uuid7
from clowm.db.types import SQLUUID


class WorkflowMode(Base):
    __tablename__: str = "workflow_mode"
    mode_id: Mapped[UUID] = mapped_column(
        SQLUUID,
        name="mode_id",
        primary_key=True,
        index=True,
        unique=True,
        default=lambda: uuid7(),
    )
    schema_path: Mapped[str] = mapped_column(String(256))
    entrypoint: Mapped[str] = mapped_column(String(256))
    name: Mapped[str] = mapped_column(String(128))

    def __eq__(self, other: Any) -> bool:
        return self.mode_id == other.mode_id if isinstance(other, WorkflowMode) else False

    def __repr__(self) -> str:
        return f"'WorkflowModus(id={self.mode_id}', name='{self.name}, entrypoint='{self.entrypoint}')"
