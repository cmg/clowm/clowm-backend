from dataclasses import dataclass
from enum import Enum, auto

from opentelemetry import trace

from clowm.models import Role, User
from clowm.models.role import RoleIdMapping

tracer = trace.get_tracer_provider().get_tracer(__name__)


class RBACResource(Enum):
    """
    Enumeration for the available RBAC resources.
    """

    USER = auto()
    BUCKET = auto()
    BUCKET_PERMISSION = auto()
    S3_KEY = auto()
    WORKFLOW = auto()
    WORKFLOW_EXECUTION = auto()
    RESOURCE = auto()
    API_TOKEN = auto()
    NEWS = auto()


class RBACOperation(Enum):
    """
    Enumeration for the available RBAC operation.
    """

    READ = auto()
    READ_ANY = auto()
    LIST = auto()
    LIST_ALL = auto()
    SEARCH = auto()
    CREATE = auto()
    CREATE_ANY = auto()
    UPDATE = auto()
    UPDATE_ANY = auto()
    UPDATE_STATUS = auto()
    DELETE = auto()
    DELETE_ANY = auto()
    CANCEL = auto()
    CANCEL_ANY = auto()
    REQUEST_SYNC = auto()


# map of roles to permissions
_permission_list: dict[Role.RoleEnum, dict[RBACResource, list[RBACOperation]]] = {
    Role.RoleEnum.USER: {
        RBACResource.API_TOKEN: [RBACOperation.READ, RBACOperation.CREATE, RBACOperation.LIST, RBACOperation.DELETE],
        RBACResource.USER: [RBACOperation.READ, RBACOperation.SEARCH, RBACOperation.DELETE],
        RBACResource.BUCKET: [
            RBACOperation.READ,
            RBACOperation.CREATE,
            RBACOperation.LIST,
            RBACOperation.DELETE,
            RBACOperation.UPDATE,
        ],
        RBACResource.BUCKET_PERMISSION: [
            RBACOperation.READ,
            RBACOperation.CREATE,
            RBACOperation.LIST,
            RBACOperation.DELETE,
            RBACOperation.UPDATE,
        ],
        RBACResource.S3_KEY: [
            RBACOperation.READ,
            RBACOperation.CREATE,
            RBACOperation.LIST,
            RBACOperation.DELETE,
        ],
        RBACResource.WORKFLOW: [
            RBACOperation.READ,
            RBACOperation.LIST,
        ],
        RBACResource.WORKFLOW_EXECUTION: [
            RBACOperation.CREATE,
            RBACOperation.READ,
            RBACOperation.LIST,
            RBACOperation.CANCEL,
            RBACOperation.DELETE,
        ],
        RBACResource.RESOURCE: [RBACOperation.READ, RBACOperation.LIST, RBACOperation.REQUEST_SYNC],
    },
    Role.RoleEnum.REVIEWER: {
        RBACResource.USER: [RBACOperation.READ, RBACOperation.SEARCH, RBACOperation.DELETE],
        RBACResource.WORKFLOW: [
            RBACOperation.READ,
            RBACOperation.READ_ANY,
            RBACOperation.LIST,
            RBACOperation.LIST_ALL,
            RBACOperation.UPDATE_STATUS,
        ],
        RBACResource.WORKFLOW_EXECUTION: [
            RBACOperation.CREATE,
            RBACOperation.CREATE_ANY,
            RBACOperation.READ,
            RBACOperation.LIST,
            RBACOperation.CANCEL,
            RBACOperation.DELETE,
        ],
        RBACResource.RESOURCE: [
            RBACOperation.READ,
            RBACOperation.READ_ANY,
            RBACOperation.LIST,
            RBACOperation.LIST_ALL,
            RBACOperation.REQUEST_SYNC,
            RBACOperation.UPDATE_STATUS,
        ],
    },
    Role.RoleEnum.DEVELOPER: {
        RBACResource.WORKFLOW: [
            RBACOperation.CREATE,
            RBACOperation.READ,
            RBACOperation.UPDATE,
            RBACOperation.LIST,
            RBACOperation.DELETE,
        ]
    },
    Role.RoleEnum.DB_MAINTAINER: {
        RBACResource.RESOURCE: [
            RBACOperation.READ,
            RBACOperation.LIST,
            RBACOperation.REQUEST_SYNC,
            RBACOperation.UPDATE_STATUS,
            RBACOperation.CREATE,
            RBACOperation.UPDATE,
        ]
    },
}
# map permissions a user without roles has
_default_permission_list: dict[RBACResource, list[RBACOperation]] = {
    RBACResource.API_TOKEN: [RBACOperation.READ, RBACOperation.CREATE, RBACOperation.LIST, RBACOperation.DELETE],
    RBACResource.USER: [RBACOperation.READ, RBACOperation.DELETE],
    RBACResource.BUCKET: [RBACOperation.READ, RBACOperation.LIST, RBACOperation.UPDATE],
    RBACResource.BUCKET_PERMISSION: [RBACOperation.READ, RBACOperation.LIST, RBACOperation.DELETE],
    RBACResource.S3_KEY: [
        RBACOperation.READ,
        RBACOperation.CREATE,
        RBACOperation.LIST,
        RBACOperation.DELETE,
    ],
    RBACResource.WORKFLOW: [
        RBACOperation.READ,
        RBACOperation.LIST,
    ],
    RBACResource.WORKFLOW_EXECUTION: [
        RBACOperation.CREATE,
        RBACOperation.READ,
        RBACOperation.LIST,
        RBACOperation.CANCEL,
        RBACOperation.DELETE,
    ],
    RBACResource.RESOURCE: [
        RBACOperation.READ,
        RBACOperation.LIST,
    ],
    RBACResource.NEWS: [
        RBACOperation.READ,
        RBACOperation.LIST,
    ],
}


@dataclass
class RBACPermission:
    """
    Data class to hold a RBAC permission.
    """

    resource: RBACResource
    operation: RBACOperation

    def user_authorized(self, user: User) -> bool:
        """
        Check if the user has a role that contains this permission.

        Parameters
        ----------
        user : clowm.models.User
            User with the roles field populated.

        Returns
        -------
        authorized : bool
            Flag if the user is authorized with this permission.
        """
        if len(user.roles) == 0 and self._is_in_operation_list(_default_permission_list.get(self.resource, [])):
            return True
        mapping = RoleIdMapping()
        roles = [mapping.get_role_name(role.role_id) for role in user.roles]
        if Role.RoleEnum.ADMINISTRATOR in roles:
            return True
        for role in roles:
            if self._is_in_operation_list(
                _permission_list.get(role, {}).get(self.resource, _default_permission_list.get(self.resource, []))
            ):
                return True
        return False

    def _is_in_operation_list(self, operations: list[RBACOperation]) -> bool:
        """
        Helper function to determine if the RBAC operation of this permission is in a list of operations.

        Parameters
        ----------
        operations : list[clowm.core.rbac.RBACOperation]
            List of operation.

        Returns
        -------
        contains : bool
            Flag if the check was successful.
        """
        return any(operation == self.operation for operation in operations)

    def __str__(self) -> str:
        return f"{self.resource.name.lower()}:{self.operation.name.lower()}"
