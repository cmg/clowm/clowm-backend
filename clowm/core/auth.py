from datetime import UTC, datetime, timedelta

from authlib.jose import JsonWebToken

from clowm.core.config import settings

ISSUER = "clowm"
ALGORITHM = "RS256"
jwt = JsonWebToken([ALGORITHM])


def create_access_token(subject: str) -> str:
    """
    Create a JWT access token.

    Parameters
    ----------
    subject : str
        Ths subject in the JWT.

    Returns
    -------
    token : str
        The generated JWT.
    """
    expire = datetime.now(UTC) + timedelta(minutes=settings.jwt_token_expire_minutes)
    to_encode = {"exp": expire, "sub": subject, "iss": ISSUER}
    encoded_jwt = jwt.encode(
        header={"alg": ALGORITHM}, payload=to_encode, key=settings.private_key_value.get_secret_value()
    )
    return encoded_jwt.decode("utf-8")


def decode_token(token: str) -> dict[str, str]:
    """
    Decode and verify a JWT token.

    Parameters
    ----------
    token : str
        The JWT to decode.

    Returns
    -------
    decoded_token : dict[str, str]
        Payload of the decoded token.
    """
    claims = jwt.decode(
        s=token,
        key=settings.public_key_value.get_secret_value(),
        claims_options={
            "iss": {"essential": True},
            "sub": {"essential": True},
            "exp": {"essential": True},
        },
    )
    claims.validate()
    return claims
