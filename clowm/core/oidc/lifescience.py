from authlib.oidc.core.claims import UserInfo as AuthlibUserInfo

from .client import AAIUser

__all__ = ["lifescience_parse_userinfo"]


def lifescience_parse_userinfo(userinfo: AuthlibUserInfo) -> AAIUser:
    """
    Parse the users info from the userinfo endpoint response for Lifescience AAI.

    Parameters
    ----------
    userinfo : authlib.oidc.core.claims.UserInfo
        The response from the Lifescience userinfo endpoint.

    Returns
    -------
    userinfo : clowm.core.oidc.UserInfo
        The extracted user information
    """
    user = AAIUser.model_validate(userinfo)
    user.sub = user.sub.split("@")[0]
    if not user.email_verified:
        user.email = None
    return user
