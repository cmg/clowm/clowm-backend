import urllib.parse
from abc import ABCMeta, abstractmethod
from collections.abc import Callable
from enum import StrEnum, unique
from typing import Any

from authlib.integrations.base_client.errors import OAuthError
from authlib.integrations.starlette_client import OAuth
from authlib.oidc.core.claims import UserInfo as AuthlibUserInfo
from fastapi.requests import Request
from fastapi.responses import RedirectResponse
from httpx import HTTPStatusError
from opentelemetry import trace
from pydantic import AnyHttpUrl, BaseModel, ValidationError

from clowm.core.config import OIDCSettings, settings

__all__ = ["UserInfo", "OIDCClient", "OIDCProvider", "AAIUser", "AuthError"]

tracer = trace.get_tracer_provider().get_tracer(__name__)


@unique
class OIDCProvider(StrEnum):
    lifescience = "lifescience-aai"
    nfdi = "nfdi-aai"


class AAIUser(BaseModel):
    sub: str
    name: str
    email: str | None = None
    email_verified: bool = False


class UserInfo(AAIUser):
    provider: OIDCProvider

    @staticmethod
    def from_aai_user(user: AAIUser, provider: OIDCProvider) -> "UserInfo":
        return UserInfo(email=user.email, sub=user.sub, name=user.name, provider=provider)


class AuthError(Exception, metaclass=ABCMeta):
    @property
    @abstractmethod
    def _error_url_param(self) -> str: ...

    @property
    @abstractmethod
    def _redirect_path(self) -> str: ...

    @property
    def redirect_url(self) -> AnyHttpUrl:
        return AnyHttpUrl.build(
            scheme=settings.ui_uri.scheme,
            host=settings.ui_uri.host,  # type: ignore[arg-type]
            port=settings.ui_uri.port,
            path=self._redirect_path,
            query="&".join(f"{key}={urllib.parse.quote_plus(str(val))}" for key, val in self.url_params.items()),
        )

    def __init__(self, error_source: str, url_params: dict[str, str | bool | int] | None = None):
        self.url_params = url_params if url_params is not None else {}
        self.url_params[self._error_url_param] = error_source

    def add_url_params(self, **kwargs: str | int | bool) -> None:
        """
        Add url params to the auth exception.

        Parameters
        ----------
        kwargs : str | bool | str
            Url params to add
        """
        self.url_params |= kwargs


type ParseUserInfoFunction = Callable[[AuthlibUserInfo], AAIUser]


class OIDCClient:
    def __init__(self) -> None:
        self._userinfo_function_map: dict[OIDCProvider, ParseUserInfoFunction] = {}
        self._oauth = OAuth()

    def register_provider(
        self,
        provider: OIDCProvider,
        client_kwargs: dict[str, Any],
        *,
        oidc_settings: OIDCSettings,
        parse_userinfo_function: ParseUserInfoFunction,
    ) -> None:
        """
        Register a new OIDC provider with this client.

        Parameters
        ----------
        provider : clowm.core.oidc.OIDCClient.OIDCProvider
            The OIDC provider to register.
        client_kwargs : dict[str, Any]
             Parameters for Remote app. https://docs.authlib.org/
        oidc_settings : clowm.core.config.OIDCSettings
            Settings for registering the OIDC provider.
        parse_userinfo_function: Callable[[authlib.oidc.core.claims.UserInfo], clowm.core.oidc.UserInfo]
            Function to parse the userinfo endpoint response to a common format.
        """
        self._oauth.register(
            name=provider.name,
            client_id=oidc_settings.client_id,
            client_secret=oidc_settings.client_secret.get_secret_value(),
            server_metadata_url=str(oidc_settings.server_metadata_url),
            client_kwargs={"scope": oidc_settings.scopes, **client_kwargs},
        )
        self._userinfo_function_map[provider] = parse_userinfo_function

    async def authorize_redirect(
        self, request: Request, redirect_uri: AnyHttpUrl, provider: OIDCProvider, error_cls: type[AuthError]
    ) -> RedirectResponse:
        """
        Create a redirect response to the OIDC with the correct url query parameters.
        Raises an LoginException if there is an error.

        Parameters
        ----------
        request : fastapi.Request
            Raw request object to access the session cookie.
        redirect_uri : pydantic.AnyHttpUrl
            Url that the OIDC provider should redirect to after logging in.
        provider : clowm.core.oidc.OIDCClient
            The OIDC provider to use.

        Returns
        -------
        response : fastapi.responses.RedirectResponse
            Response object to initiate the OIDC login flow.
        """
        try:
            return await getattr(self._oauth, provider.name).authorize_redirect(request, redirect_uri=str(redirect_uri))
        except AttributeError as err:
            raise error_cls(error_source=f"unknown provider {provider.name}") from err

    async def verify_fetch_userinfo(
        self, request: Request, provider: OIDCProvider, error_cls: type[AuthError]
    ) -> UserInfo:
        """
        Verify the callback, fetch the access token and fetch the userinfo from the userinfo endpoint.
        Raises an LoginException if there is any error.

        Parameters
        ----------
        request : fastapi.Request
            Raw request object to access the session cookie and request parameters.
        provider : clowm.core.oidc.OIDCClient
            The OIDC provider for which this callback is intended.

        Returns
        -------
        userinfo : clowm.core.oidc.UserInfo
            The parsed userinfo from the OIDC provider.
        """
        try:
            if "error" in request.query_params:
                # if there is an error in the login flow, like a canceled login request, then notify the client
                raise error_cls(
                    error_source=request.query_params.get("error_description", request.query_params["error"])
                )
            with tracer.start_as_current_span("oidc_authorize_access_token"):
                claims = await getattr(self._oauth, provider.name).authorize_access_token(request)
            # ID token doesn't have all necessary information, call userinfo endpoint
            with tracer.start_as_current_span("oidc_get_user_info"):
                userinfo = await getattr(self._oauth, provider.name).userinfo(token=claims)
            return UserInfo.from_aai_user(user=self._userinfo_function_map[provider](userinfo), provider=provider)
        except (OAuthError, HTTPStatusError) as err:
            # if there is an error in the oauth flow, like an expired token, notify the client
            raise error_cls(error_source="generic OIDC error") from err
        except ValidationError as e:
            # extract all missing values from the oidc response
            missing_values = [str(error["loc"][0]) for error in e.errors() if error["type"] == "missing"]
            raise error_cls(
                error_source="missing oidc attributes: " + ", ".join(map(lambda x: f"'{x}'", missing_values))
            ) from e
        except AttributeError as err:
            raise error_cls(error_source=f"unknown provider {provider.name}") from err
