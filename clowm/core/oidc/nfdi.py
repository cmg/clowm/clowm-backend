from authlib.oidc.core.claims import UserInfo as AuthlibUserInfo

from .client import AAIUser

__all__ = ["nfdi_parse_userinfo"]


def nfdi_parse_userinfo(userinfo: AuthlibUserInfo) -> AAIUser:
    """
    Parse the users info from the userinfo endpoint response for NFDI AAI.

    Parameters
    ----------
    userinfo : authlib.oidc.core.claims.UserInfo
        The response from the NFDI userinfo endpoint.

    Returns
    -------
    userinfo : clowm.core.oidc.client.AAIUser
        The extracted user information
    """
    if userinfo.get("name", None) is None:
        userinfo["name"] = f"{userinfo.get('given_name', '')} {userinfo.get('family_name', '')}"
    return AAIUser.model_validate(userinfo)
