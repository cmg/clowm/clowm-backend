import base64
import json
import os
import re
import secrets
from enum import StrEnum
from functools import cached_property
from typing import Literal
from uuid import UUID

from cryptography.hazmat.backends import default_backend as crypto_default_backend
from cryptography.hazmat.primitives import serialization as crypto_serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from pydantic import (
    AnyHttpUrl,
    BaseModel,
    ByteSize,
    Field,
    FilePath,
    MySQLDsn,
    NameEmail,
    PositiveInt,
    SecretStr,
    computed_field,
    field_validator,
)
from pydantic_settings import (
    BaseSettings,
    JsonConfigSettingsSource,
    PydanticBaseSettingsSource,
    SettingsConfigDict,
    TomlConfigSettingsSource,
    YamlConfigSettingsSource,
)

from clowm.schemas.documentation_enum import DocumentationEnum
from clowm.scm import SCM
from clowm.slurm.versions import SlurmVersion

__all__ = [
    "DBSettings",
    "S3Settings",
    "EmailSettings",
    "OTLPSettings",
    "OIDCSettings",
    "Settings",
    "settings",
    "MonitorJobBackoffStrategy",
]


jwt_regex = re.compile(r"^[\w-]{2,}\.([\w-]{2,})\.[\w-]{2,}$")


class MonitorJobBackoffStrategy(StrEnum):
    LINEAR = "LINEAR"
    EXPONENTIAL = "EXPONENTIAL"
    CONSTANT = "CONSTANT"
    NOMONITORING = "NOMONITORING"


class DBSettings(BaseModel):
    port: int = Field(3306, description="Port of the database.")
    host: str = Field("localhost", description="Host of the database.")
    name: str = Field(..., description="Name of the database.")
    user: str = Field(..., description="Username in the database.")
    password: SecretStr = Field(..., description="Password for the database user.")
    verbose: bool = Field(False, description="Flag whether to print the SQL Queries in the logs.")

    @cached_property
    def dsn_sync(self) -> MySQLDsn:
        return MySQLDsn.build(
            scheme="mysql+pymysql",
            password=self.password.get_secret_value(),
            host=self.host,
            port=self.port,
            path=self.name,
            username=self.user,
        )

    @cached_property
    def dsn_async(self) -> MySQLDsn:
        return MySQLDsn.build(
            scheme="mysql+aiomysql",
            password=self.password.get_secret_value(),
            host=self.host,
            port=self.port,
            path=self.name,
            username=self.user,
        )


class S3Settings(BaseModel):
    uri: AnyHttpUrl = Field(..., description="URI of the S3 Object Storage.")
    access_key: str = Field(..., description="Access key for the S3 that owns the buckets.")
    secret_key: SecretStr = Field(..., description="Secret key for the S3 that owns the buckets.")
    username: str = Field(
        "bucket-manager", description="ID of the user in ceph who owns all the buckets. Owner of 'CLOWM_S3__ACCESS_KEY'"
    )
    data_bucket: str = Field(
        "clowm-data", description="Bucket where clowm can store data like icons or workflow parameters"
    )
    # 25 * 2**32 = 100 GiB
    initial_bucket_size_limit: ByteSize = Field(ByteSize(25 * 2**32), description="Size limit of the initial bucket")
    initial_bucket_object_limit: int = Field(
        10000, gt=0, lt=2**32, description="Maximum number of objects in the initial bucket"
    )
    # 25 * 2**32 = 400 GiB
    default_bucket_size_limit: ByteSize = Field(ByteSize(25 * 2**34), description="Size limit of a new Bucket")
    default_bucket_object_limit: int = Field(
        40000, gt=0, lt=2**32, description="Maximum number of objects in a new bucket"
    )

    @field_validator("default_bucket_size_limit")
    @classmethod
    def default_bucket_size_limit_validator(cls, size: ByteSize) -> ByteSize:
        if size.to("KiB") >= 2**32:
            raise ValueError("size can be maximal 4.3TB")
        if size.to("KiB") < 1:
            raise ValueError("size must be at least 1 KiB")
        return size

    @field_validator("initial_bucket_size_limit")
    @classmethod
    def initial_bucket_size_limit_validator(cls, size: ByteSize) -> ByteSize:
        if size.to("KiB") >= 2**32:
            raise ValueError("size can be maximal 4.3TB")
        if size.to("KiB") < 1:
            raise ValueError("size must be at least 1 KiB")
        return size

    __data_bucket_icon_dir: str = "icon"
    __data_bucket_parameter_dir: str = "parameters"
    __data_bucket_workflow_dir: str = "workflow-cache"
    __data_bucket_scm_dir: str = "scm"
    __data_bucket_resource_dir: str = "resources"
    __data_bucket_config_dir: str = "configs"

    def icon_url(self, icon_slug: str) -> AnyHttpUrl:
        return AnyHttpUrl(str(self.uri) + self.data_bucket + "/" + self.icon_key(icon_slug))

    def icon_key(self, icon_slug: str) -> str:
        return f"{self.__data_bucket_icon_dir}/{icon_slug}"

    def workflow_cache_key(
        self, name: DocumentationEnum | str, git_commit_hash: str, mode_id: UUID | None = None
    ) -> str:
        return f"{self.__data_bucket_workflow_dir}/{git_commit_hash}{'' if mode_id is None else '-' + mode_id.hex}-{str(name)}"

    def execution_parameters_key(self, execution_id: UUID) -> str:
        return f"{self.__data_bucket_parameter_dir}/params-{execution_id.hex}.json"

    def scm_key(self, scm_file_id: UUID) -> str:
        return f"{self.__data_bucket_scm_dir}/{SCM.generate_filename(scm_file_id)}"

    def resource_key(self, resource_version_key: str) -> str:
        return f"{self.__data_bucket_resource_dir}/{resource_version_key}"

    def config_key(self, git_commit_hash: str) -> str:
        return f"{self.__data_bucket_config_dir}/{git_commit_hash}.config"


class OTLPSettings(BaseModel):
    grpc_endpoint: str | None = Field(
        None, description="OTLP compatible endpoint to send traces via gRPC, e.g. Jaeger", examples=["localhost:8080"]
    )
    secure: bool = Field(False, description="Connection type")


class EmailSettings(BaseModel):
    server: str | Literal["console"] | None = Field(
        None,
        description="Hostname of SMTP server. If `console`, emails are printed to the console. If None, no emails are sent",
    )
    port: int = Field(587, description="Port of the SMTP server")
    sender_email: NameEmail = Field(
        NameEmail(email="no-reply@clowm.com", name="CloWM"), description="Email address from which the emails are sent."
    )
    reply_email: NameEmail | None = Field(None, description="Email address in the `Reply-To` header.")
    connection_security: Literal["ssl", "tls"] | Literal["starttls"] | None = Field(
        None, description="Connection security to the SMTP server."
    )
    local_hostname: str | None = Field(None, description="Overwrite the local hostname from which the emails are sent.")
    ca_path: FilePath | None = Field(None, description="Path to a custom CA certificate.")
    key_path: FilePath | None = Field(None, description="Path to the CA key.")
    user: str | None = Field(None, description="Username to use for SMTP login")
    password: SecretStr | None = Field(None, description="Password to use for SMTP login")


class OIDCSettings(BaseModel):
    client_secret: SecretStr = Field(..., description="OIDC Client secret")
    client_id: str = Field(..., description="OIDC Client ID")
    server_metadata_url: AnyHttpUrl = Field(..., description="URL to the OIDC metadata endpoint")
    scopes: str = Field("openid profile email", description="Scopes this OIDC client will ask for")


class LifeScienceAAISettings(OIDCSettings):
    server_metadata_url: AnyHttpUrl = Field(
        AnyHttpUrl("https://login.aai.lifescience-ri.eu/oidc/.well-known/openid-configuration"),
        description="URL to the LifScience AAI metadata endpoint",
    )
    scopes: str = Field("openid profile email", description="Scopes this LifeScience AAI client will ask for")


class NfdiAAISettings(OIDCSettings):
    server_metadata_url: AnyHttpUrl = Field(
        AnyHttpUrl("https://infraproxy.nfdi-aai.dfn.de/.well-known/openid-configuration"),
        description="URL to the NFDI AAI metadata endpoint",
    )
    scopes: str = Field("openid email", description="Scopes this NFDI AAI client will ask for")


class SlurmSettings(BaseModel):
    uri: AnyHttpUrl = Field(..., description="URI of the Slurm Cluster")
    token: SecretStr = Field(..., description="JWT for authentication the Slurm REST API")
    partition: str | None = Field(None, description="Slurm partition where the nextflow job should scheduled")
    version: SlurmVersion = Field(SlurmVersion.V42, description="Version of Slurm data parser plugin")

    @field_validator("token")
    @classmethod
    def valid_jwt(cls, jwt: SecretStr) -> SecretStr:
        if jwt_regex.match(jwt.get_secret_value()) is None:
            raise ValueError("Invalid JWT")
        return jwt

    @computed_field  # type: ignore[prop-decorator]
    @cached_property
    def user(self) -> str:
        matched_string = jwt_regex.match(self.token.get_secret_value())
        if matched_string is None:
            raise ValueError("Invalid JWT")
        payload = matched_string.groups()[0]
        decoded = base64.urlsafe_b64decode(payload + "=" * (4 - len(payload) % 4))
        return json.loads(decoded)["sun"]


class ClusterSettings(BaseModel):
    slurm: SlurmSettings
    nxf_config: str | None = Field(None, description="Path to a nextflow configuration for every run")
    nxf_bin: str = Field("nextflow", description="Path to the nextflow executable")
    working_directory: str = Field(
        "/tmp",  # noqa: S108
        description="Working directory for the slurm job with the nextflow command",
    )
    active_workflow_execution_limit: PositiveInt = Field(
        3, description="The limit of active workflow executions per user."
    )
    job_monitoring: MonitorJobBackoffStrategy = Field(
        MonitorJobBackoffStrategy.EXPONENTIAL,
        description="Strategy for polling the slurm job status for monitoring the workflow execution.",
    )
    execution_cleanup: bool = Field(True, description="Remove work directory after the workflow execution")
    resource_cluster_path: str = Field(
        "/vol/data/databases", description="Base path on the cluster where all resources are saved"
    )
    resource_container_path: str = Field(
        "/vol/resources", description="Base path in the container where all resources are available"
    )
    tower_secret: SecretStr = Field(..., description="Shared secret between nextflow cluster and logging services")
    head_job_cpus: int = Field(2, description="Number of CPUs for the Nextflow head job", ge=1, le=96)
    head_job_memory: ByteSize = Field(
        ByteSize(4 * 1024 * 1024 * 1024),
        description="Available RAM for the Nextflow head job",
        ge=1024 * 1024,
        le=4 * 1024 * 1024 * 1024,
    )


class Settings(BaseSettings):
    api_prefix: str = Field("", description="Path Prefix for all API endpoints.")
    dev_system: bool = Field(False, description="Open a endpoint where to execute arbitrary workflows.")
    secret_key: SecretStr = Field(
        SecretStr(secrets.token_urlsafe(32)), description="Secret key to sign the session cookie."
    )
    private_key: SecretStr | None = Field(None, description="Private RSA Key in PEM format to sign the JWTs.")
    private_key_file: FilePath | None = Field(
        None, description="Path to Private RSA Key in PEM format to sign the JWTs."
    )

    # 60 minutes * 24 hours * 7 days = 7 days
    jwt_token_expire_minutes: int = Field(60 * 24 * 7, description="JWT lifespan in minutes.")
    block_foreign_users: bool = Field(False, description="Block users that have no role")
    ui_uri: AnyHttpUrl = Field(..., description="URL of the UI")

    @cached_property
    def public_key_value(self) -> SecretStr:
        private_key = crypto_serialization.load_pem_private_key(
            self.private_key_value.get_secret_value().encode("utf-8"),
            password=None,
        )
        return SecretStr(
            private_key.public_key()
            .public_bytes(crypto_serialization.Encoding.PEM, crypto_serialization.PublicFormat.SubjectPublicKeyInfo)
            .decode("UTF-8")
        )

    @computed_field  # type: ignore[prop-decorator]
    @cached_property
    def private_key_value(self) -> SecretStr:
        private_key = ""
        if self.private_key is not None:
            private_key = self.private_key.get_secret_value()
        if self.private_key_file is not None:
            with open(self.private_key_file) as f:
                private_key = f.read()
        if len(private_key) == 0:
            key = rsa.generate_private_key(backend=crypto_default_backend(), public_exponent=65537, key_size=2048)
            private_key = key.private_bytes(
                crypto_serialization.Encoding.PEM,
                crypto_serialization.PrivateFormat.PKCS8,
                crypto_serialization.NoEncryption(),
            ).decode("UTF-8")
            print("No private key provided. Use randomly generated RSA key")
            print(
                key.public_key()
                .public_bytes(crypto_serialization.Encoding.PEM, crypto_serialization.PublicFormat.SubjectPublicKeyInfo)
                .decode("UTF-8")
            )
        return SecretStr(private_key)

    db: DBSettings
    s3: S3Settings
    lifescience_oidc: LifeScienceAAISettings
    nfdi_oidc: NfdiAAISettings
    smtp: EmailSettings = EmailSettings()
    otlp: OTLPSettings = OTLPSettings()
    cluster: ClusterSettings

    model_config = SettingsConfigDict(
        env_prefix="CLOWM_",
        env_file=".env",
        extra="ignore",
        secrets_dir="/run/secrets" if os.path.isdir("/run/secrets") else None,
        env_nested_delimiter="__",
        yaml_file=os.getenv("CLOWM_CONFIG_FILE_YAML", "config.yaml"),
        toml_file=os.getenv("CLOWM_CONFIG_FILE_TOML", "config.toml"),
        json_file=os.getenv("CLOWM_CONFIG_FILE_JSON", "config.json"),
    )

    @classmethod
    def settings_customise_sources(
        cls,
        settings_cls: type[BaseSettings],
        init_settings: PydanticBaseSettingsSource,
        env_settings: PydanticBaseSettingsSource,
        dotenv_settings: PydanticBaseSettingsSource,
        file_secret_settings: PydanticBaseSettingsSource,
    ) -> tuple[PydanticBaseSettingsSource, ...]:
        return (
            env_settings,
            file_secret_settings,
            dotenv_settings,
            YamlConfigSettingsSource(settings_cls),
            TomlConfigSettingsSource(settings_cls),
            JsonConfigSettingsSource(settings_cls),
        )


settings = Settings()
