from abc import ABC, abstractmethod
from collections.abc import AsyncIterator
from functools import cached_property
from io import IOBase
from pathlib import Path
from tempfile import SpooledTemporaryFile
from typing import TYPE_CHECKING
from zipfile import ZipFile

from fastapi import status
from httpx import USE_CLIENT_DEFAULT, AsyncClient, Auth
from opentelemetry import trace
from pydantic import AnyHttpUrl

tracer = trace.get_tracer_provider().get_tracer(__name__)

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import Object
else:
    Object = object


class GitRepository(ABC):
    """
    Abstract class for Git Repositories
    """

    @property
    @abstractmethod
    def provider(self) -> str: ...

    @property
    def token(self) -> str | None:
        return self._token

    def __init__(self, url: str, git_commit_hash: str, token: str | None = None):
        """
        Initialize Git repository object.

        Parameters
        ----------
        url : str
            URL of the git repository
        git_commit_hash : str
            Pin down git commit git_commit_hash
        token : str | None
            Token to access a private git repository
        """
        self.url = url
        self.name = (url[:-1] if url.endswith("/") else url).split("/")[-1]
        self.commit = git_commit_hash
        self._token = token

    @abstractmethod
    async def download_file_url(self, filepath: str, client: AsyncClient) -> AnyHttpUrl:
        """
        Construct an URL where to download a file from

        Parameters
        ----------
        filepath : str
            Path of a file
        client: httpx.AsyncClient
            HTTP client for requesting a download link, like GitHub API

        Returns
        -------
        url : pydantic.AnyHttpUrl
            URL where to download the specified file from.
        """
        ...

    @property
    @abstractmethod
    def download_archive_url(self) -> AnyHttpUrl:
        """
        Construct an URL where to download the archive in zip format from

        Returns
        -------
        url : pydantic.AnyHttpUrl
            URL where to download the specified file from.
        """
        ...

    @property
    @abstractmethod
    def check_repo_availability_url(self) -> AnyHttpUrl:
        """
        Construct an URL where to download the archive in zip format from

        Returns
        -------
        url : pydantic.AnyHttpUrl
            URL where to download the specified file from.
        """
        ...

    @cached_property
    @abstractmethod
    def request_auth(self) -> Auth | None: ...

    @cached_property
    @abstractmethod
    def request_headers(self) -> dict[str, str]: ...

    @abstractmethod
    def __repr__(self) -> str: ...

    def __str__(self) -> str:
        return repr(self)

    async def check_file_exists(self, filepath: str, client: AsyncClient) -> bool:
        """
        Check if a file exists in the Git Repository

        Parameters
        ----------
        filepath : str
            Path to the file
        client : httpx.AsyncClient
            Async HTTP Client with an open connection

        Returns
        -------
        exist : bool
            Flag if the file exists.
        """
        response = await client.head(
            # use download url to bypass the github ratelimit
            # -> if public, no ratelimit
            # -> if private, then token with generous ratelimit
            str(await self.download_file_url(filepath, client)),
            auth=USE_CLIENT_DEFAULT if self.request_auth is None else self.request_auth,
            follow_redirects=True,
            headers=self.request_headers,
        )
        return response.status_code == status.HTTP_200_OK

    async def check_repo_availability(self, client: AsyncClient) -> bool:
        """
        Check if a file exists in the Git Repository

        Parameters
        ----------
        client : httpx.AsyncClient
            Async HTTP Client with an open connection

        Returns
        -------
        exist : bool
            Flag if the file exists.
        """
        response = await client.head(
            str(self.check_repo_availability_url),
            auth=USE_CLIENT_DEFAULT if self.request_auth is None else self.request_auth,
            follow_redirects=True,
            headers=self.request_headers,
        )
        return response.status_code == status.HTTP_200_OK

    async def copy_file_to_bucket(self, filepath: str, obj: Object, client: AsyncClient) -> None:
        """
        Copy a file from a git repository to a bucket

        Parameters
        ----------
        filepath : str
            Path of the file to copy.
        obj : types_aiobotocore_s3.service_resource import Object
            S3 object to upload file to.
        client : httpx.AsyncClient
            Async HTTP Client with an open connection.
        """
        with (
            tracer.start_as_current_span(
                "git_copy_file_to_bucket",
                attributes={"repository": self.url, "file": filepath, "git_commit_hash": self.commit},
            ),
            SpooledTemporaryFile(max_size=512000) as f,
        ):  # 500kB
            await self.download_file(filepath, client=client, file_handle=f)
            f.seek(0)
            with tracer.start_as_current_span(
                "s3_upload_file", attributes={"bucket_name": obj.bucket_name, "key": obj.key}
            ):
                await obj.upload_fileobj(f)

    async def download_and_extract_archive(self, path: str | Path, client: AsyncClient) -> None:
        with (
            tracer.start_as_current_span(
                "git_download_and_extract_archive",
                attributes={"repository": self.url, "git_commit_hash": self.commit},
            ),
            SpooledTemporaryFile() as f,
        ):
            async with client.stream(
                method="GET",
                url=str(self.download_archive_url),
                auth=USE_CLIENT_DEFAULT if self.request_auth is None else self.request_auth,
                follow_redirects=True,
            ) as r:
                async for chunk in r.aiter_bytes():
                    f.write(chunk)
            ZipFile(file=f).extractall(path=path)

    async def download_file_stream(self, filepath: str, client: AsyncClient) -> AsyncIterator[bytes]:
        """
        Iterate over the stream of bytes of the downloaded file

        Parameters
        ----------
        filepath : str
            Path of the file to copy.
        client : httpx.AsyncClient
            Async HTTP Client with an open connection.

        Returns
        -------
        byte_iterator : AsyncIterator[bytes]
            Async iterator over the bytes of the file
        """
        with tracer.start_as_current_span(
            "git_stream_file_content",
            attributes={"repository": self.url, "file": filepath, "git_commit_hash": self.commit},
        ):
            async with client.stream(
                method="GET",
                url=str(await self.download_file_url(filepath, client)),
                auth=USE_CLIENT_DEFAULT if self.request_auth is None else self.request_auth,
                follow_redirects=True,
            ) as r:
                # only write response to handle if the status is 200
                if r.status_code == status.HTTP_200_OK:
                    async for chunk in r.aiter_bytes():
                        yield chunk

    async def download_file(self, filepath: str, client: AsyncClient, file_handle: IOBase) -> None:
        """
        Download a file from the git repository into a file-like object.

        Parameters
        ----------
        filepath : str
            Path of the file to copy.
        client : httpx.AsyncClient
            Async HTTP Client with an open connection.
        file_handle : IOBase
            Write the file into this stream in binary mode.
        """
        with tracer.start_as_current_span(
            "git_download_file", attributes={"repository": self.url, "file": filepath, "git_commit_hash": self.commit}
        ):
            async for chunk in self.download_file_stream(filepath, client):
                file_handle.write(chunk)
