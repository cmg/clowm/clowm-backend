from functools import cached_property
from urllib.parse import quote, urlparse

from fastapi import status
from httpx import AsyncClient, BasicAuth
from opentelemetry import trace
from pydantic import AnyHttpUrl

from .abstract_repository import GitRepository

tracer = trace.get_tracer_provider().get_tracer(__name__)


class GitHubRepository(GitRepository):
    """
    Implementation for GitHub Repository
    """

    @property
    def provider(self) -> str:
        return "github"

    @cached_property
    def request_auth(self) -> BasicAuth | None:
        if self._token is not None:
            return BasicAuth(username=self.account, password=self._token)
        return None

    @cached_property
    def request_headers(self) -> dict[str, str]:
        return {"Accept": "application/vnd.github.object+json", "X-GitHub-Api-Version": "2022-11-28"}

    def __init__(self, url: str, git_commit_hash: str, token: str | None = None):
        super().__init__(url=url, git_commit_hash=git_commit_hash, token=token)
        parse_result = urlparse(url)
        path_parts = parse_result.path[1:].split("/")
        self.account = path_parts[0]
        self.repository = path_parts[1]

    @property
    def download_archive_url(self) -> AnyHttpUrl:
        return AnyHttpUrl.build(
            scheme="https",
            host="api.github.com",
            path=f"repos/{self.account}/{self.repository}/zipball/{self.commit}",
        )

    @property
    def check_repo_availability_url(self) -> AnyHttpUrl:
        return AnyHttpUrl.build(
            scheme="https",
            host="api.github.com",
            path=f"repos/{self.account}/{self.repository}/git/commits/{self.commit}",
        )

    async def download_file_url(self, filepath: str, client: AsyncClient) -> AnyHttpUrl:
        # If the repo is public, build download URL directly
        if self._token is None:
            return AnyHttpUrl.build(
                scheme="https",
                host="raw.githubusercontent.com",
                path=f"{self.account}/{self.repository}/{self.commit}/{filepath}",
            )
        # If the repo is private, request a download URL with a token from the GitHub API
        with tracer.start_as_current_span(
            "github_get_download_link",
            attributes={"repository": self.url, "file": filepath, "git_commit_hash": self.commit},
        ):
            response = await client.get(
                str(
                    AnyHttpUrl.build(
                        scheme="https",
                        host="api.github.com",
                        path=f"repos/{self.account}/{self.repository}/contents/{quote(filepath)}",
                        query=f"ref={quote(self.commit)}",
                    )
                ),
                auth=self.request_auth,
                headers=self.request_headers,
                follow_redirects=True,
            )
        assert response.status_code == status.HTTP_200_OK
        return AnyHttpUrl(response.json()["download_url"])

    def __repr__(self) -> str:
        url = AnyHttpUrl.build(scheme="https", host="github.com", path=f"{self.account}/{self.repository}")
        return f"GitHub(repo={url} commit={self.commit})"
