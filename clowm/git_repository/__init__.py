from urllib.parse import urlparse

from pydantic import AnyHttpUrl

from clowm.git_repository.abstract_repository import GitRepository
from clowm.git_repository.github import GitHubRepository
from clowm.git_repository.gitlab import GitlabRepository

__all__ = ["GitlabRepository", "GitRepository", "GitHubRepository", "build_repository"]


def build_repository(url: AnyHttpUrl | str, git_commit_hash: str, token: str | None = None) -> GitRepository:
    """
    Build the right git repository object based on the url

    Parameters
    ----------
    url : str | pydantic.AnyHttpUrl
        URL of the git repository
    git_commit_hash : str
        Pin down git commit hash
    token : str | None
        Token to access a private git repository
    Returns
    -------
    repo : GitRepository
        Specialized Git repository object
    """
    domain = urlparse(str(url)).netloc
    if "github" in domain:
        return GitHubRepository(url=str(url), git_commit_hash=git_commit_hash, token=token)
    if "gitlab" in domain:
        return GitlabRepository(url=str(url), git_commit_hash=git_commit_hash, token=token)
    raise NotImplementedError("Unknown Git repository Provider")
