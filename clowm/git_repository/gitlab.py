from functools import cached_property
from urllib.parse import quote, urlparse

from httpx import AsyncClient
from pydantic import AnyHttpUrl

from ..utils.httpx_auth import BearerAuth
from .abstract_repository import GitRepository


class GitlabRepository(GitRepository):
    """
    Implementation for a Gitlab Repository
    """

    @property
    def provider(self) -> str:
        return "gitlab"

    @cached_property
    def request_auth(self) -> BearerAuth | None:
        if self._token is not None:
            return BearerAuth(token=self._token)
        return None

    @cached_property
    def request_headers(self) -> dict[str, str]:
        return {}

    def __init__(self, url: str, git_commit_hash: str, token: str | None = None):
        super().__init__(url=url, git_commit_hash=git_commit_hash, token=token)
        parse_result = urlparse(url)
        self.domain = parse_result.netloc
        self.project = parse_result.path[1:]

    @property
    def download_archive_url(self) -> AnyHttpUrl:
        return AnyHttpUrl.build(
            scheme="https",
            host=self.domain,
            path="/".join(["api", "v4", "projects", quote(self.project, safe=""), "repository", "archive.zip"]),
            query=f"sha={quote(self.commit)}",
        )

    @property
    def check_repo_availability_url(self) -> AnyHttpUrl:
        return AnyHttpUrl.build(
            scheme="https",
            host=self.domain,
            path="/".join(
                ["api", "v4", "projects", quote(self.project, safe=""), "repository", "commits", self.commit]
            ),
            query="stats=false",
        )

    async def download_file_url(self, filepath: str, client: AsyncClient | None = None) -> AnyHttpUrl:
        return AnyHttpUrl.build(
            scheme="https",
            host=self.domain,
            path="/".join(
                [
                    "api",
                    "v4",
                    "projects",
                    quote(self.project, safe=""),
                    "repository",
                    "files",
                    quote(filepath, safe=""),
                    "raw",
                ]
            ),
            query=f"ref={quote(self.commit)}",
        )

    def __repr__(self) -> str:
        url = AnyHttpUrl.build(scheme="https", host=self.domain, path=self.project)
        return f"Gitlab(repo={url} commit={self.commit}))"
