from collections.abc import AsyncGenerator
from contextlib import asynccontextmanager

from brotli_asgi import BrotliMiddleware
from fastapi import FastAPI, Request, Response
from fastapi.exception_handlers import http_exception_handler, request_validation_exception_handler
from fastapi.exceptions import RequestValidationError, StarletteHTTPException
from fastapi.responses import JSONResponse
from opentelemetry import trace
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter
from opentelemetry.instrumentation.fastapi import FastAPIInstrumentor
from opentelemetry.sdk.resources import SERVICE_NAME, Resource
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchSpanProcessor
from opentelemetry.trace import Status, StatusCode
from starlette.middleware.sessions import SessionMiddleware

from .api import clowm_router
from .api.background.dependencies import get_background_http_client, get_background_s3_resource
from .api.dependencies.crud import get_db_context_manager
from .api.exception_handlers import (
    auth_exception_handler,
)
from .ceph.s3 import setup_s3_data_bucket
from .core.config import settings
from .core.oidc import AuthError, OIDCClient, OIDCProvider, lifescience_parse_userinfo, nfdi_parse_userinfo
from .models.role import RoleIdMapping


@asynccontextmanager
async def lifespan(fastapi_app: FastAPI) -> AsyncGenerator[None]:  # pragma: no cover
    # register all oidc providers
    oidc_client = OIDCClient()
    oidc_client.register_provider(
        OIDCProvider.lifescience,
        {"code_challenge_method": "S256", "http2": True},
        oidc_settings=settings.lifescience_oidc,
        parse_userinfo_function=lifescience_parse_userinfo,
    )
    oidc_client.register_provider(
        OIDCProvider.nfdi,
        {"follow_redirects": True, "http2": True},
        oidc_settings=settings.nfdi_oidc,
        parse_userinfo_function=nfdi_parse_userinfo,
    )
    # load the role to id mapping
    async with get_db_context_manager() as db:
        await RoleIdMapping().load_role_ids(db)
    # Create a http client and s3 session once instead for every request and attach it to the app
    async with (
        get_background_http_client() as client,
        get_background_s3_resource() as s3_resource,
    ):
        await setup_s3_data_bucket(s3_resource)
        fastapi_app.requests_client = client  # type: ignore[attr-defined]
        fastapi_app.oidc_client = oidc_client  # type: ignore[attr-defined]
        fastapi_app.s3_resource = s3_resource  # type: ignore[attr-defined]
        yield


app = FastAPI(
    title="CloWM",
    root_path=settings.api_prefix,
    openapi_url=None,  # create it manually to enable caching on client side
    lifespan=lifespan,
)

if settings.otlp.grpc_endpoint is not None and len(settings.otlp.grpc_endpoint) > 0:  # pragma: no cover
    resource = Resource(attributes={SERVICE_NAME: "clowm"})
    provider = TracerProvider(resource=resource)
    provider.add_span_processor(
        BatchSpanProcessor(OTLPSpanExporter(endpoint=settings.otlp.grpc_endpoint, insecure=not settings.otlp.secure))
    )
    trace.set_tracer_provider(provider)

    @app.exception_handler(StarletteHTTPException)
    async def trace_http_exception_handler(request: Request, exc: StarletteHTTPException) -> Response:
        current_span = trace.get_current_span()
        current_span.set_status(Status(StatusCode.ERROR))
        current_span.record_exception(exc)
        return await http_exception_handler(request, exc)

    @app.exception_handler(RequestValidationError)
    async def trace_validation_exception_handler(request: Request, exc: RequestValidationError) -> JSONResponse:
        current_span = trace.get_current_span()
        current_span.set_status(Status(StatusCode.ERROR))
        current_span.record_exception(exc)
        return await request_validation_exception_handler(request, exc)


FastAPIInstrumentor.instrument_app(
    app,
    excluded_urls="health,v1/docs,ui/docs,v1/openapi.json,ui/openapi.json",
    tracer_provider=trace.get_tracer_provider(),
)

# Enable gzip compression for large responses
app.add_middleware(BrotliMiddleware)
app.add_middleware(
    SessionMiddleware, secret_key=settings.secret_key.get_secret_value(), https_only=settings.ui_uri.scheme == "https"
)

# Include all routes
app.include_router(clowm_router)

app.add_exception_handler(AuthError, auth_exception_handler)  # type: ignore[arg-type]
