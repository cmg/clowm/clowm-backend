from uuid import UUID

from opentelemetry import trace
from pydantic import ByteSize
from rgwadmin import RGWAdmin

from clowm.core.config import settings
from clowm.models import Bucket
from clowm.schemas.s3key import S3Key

tracer = trace.get_tracer_provider().get_tracer(__name__)

__all__ = ["rgw", "get_s3_keys", "update_bucket_limits"]

rgw = RGWAdmin(
    access_key=settings.s3.access_key,
    secret_key=settings.s3.secret_key.get_secret_value(),
    secure=settings.s3.uri.scheme == "https",
    server=str(settings.s3.uri).split("://")[-1].strip("/"),
)


def get_s3_keys(rgw: RGWAdmin, uid: UUID) -> list[S3Key]:
    """
    Wrapper for getting the s3 keys for a user.

    Parameters
    ----------
    rgw : rgwadmin.RGWAdmin
        RGWAdmin object to communicate with the RGW admin API.
    uid : uuid.UUID
        ID of a user.

    Returns
    -------
    s3_keys : list[clowm.schemas.s3key.S3Key]
        List of all the S3 keys for the user.
    """
    with tracer.start_as_current_span("rgw_get_user_keys", attributes={"uid": str(uid)}):
        return [S3Key(uid=uid, **key) for key in rgw.get_user(uid=str(uid), stats=False)["keys"]]


def update_bucket_limits(rgw: RGWAdmin, bucket: Bucket) -> None:
    """
    Wrapper for updating the bucket limits for a bucket.

    Parameters
    ----------
    rgw : rgwadmin.RGWAdmin
        RGWAdmin object to communicate with the RGW admin API.
    bucket : clowm.models.Bucket
        The bucket for which the limits should be updated in the RGW.
    """
    with tracer.start_as_current_span(
        "rgw_set_bucket_limits",
        attributes={
            "bucket_name": bucket.name,
            "enabled": bucket.object_limit is not None or bucket.size_limit is not None,
        },
    ) as span:
        if bucket.size_limit is not None:  # pragma: no cover
            span.set_attribute("size_limit", ByteSize(bucket.size_limit * 1024).human_readable())
        if bucket.object_limit is not None:  # pragma: no cover
            span.set_attribute("object_limit", bucket.object_limit)
        rgw.set_bucket_quota(
            uid=settings.s3.username,
            bucket=bucket.name,
            max_size_kb=-1 if bucket.size_limit is None else bucket.size_limit,
            max_objects=-1 if bucket.object_limit is None else bucket.object_limit,
            enabled=bucket.object_limit is not None or bucket.size_limit is not None,
        )
