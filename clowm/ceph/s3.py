import json
from typing import TYPE_CHECKING, Any
from uuid import UUID

import aioboto3
from botocore.exceptions import ClientError
from opentelemetry import trace

from clowm.core.config import settings

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import ObjectSummary, S3ServiceResource
    from types_aiobotocore_s3.type_defs import FileobjTypeDef
else:
    S3ServiceResource = object
    FileobjTypeDef = object
    ObjectSummary = object

tracer = trace.get_tracer_provider().get_tracer(__name__)

boto_session = aioboto3.Session(
    aws_access_key_id=settings.s3.access_key,
    aws_secret_access_key=settings.s3.secret_key.get_secret_value(),
)

cors_rule = {
    "CORSRules": [
        {
            "ID": "websiteaccess",
            "AllowedHeaders": [
                "amz-sdk-invocation-id",
                "amz-sdk-request",
                "authorization",
                "content-type",
                "x-amz-sdk-checksum-algorithm",
                "x-amz-checksum-crc32",
                "x-amz-content-sha256",
                "x-amz-copy-source",
                "x-amz-date",
                "x-amz-user-agent",
                "content-md5",
            ],
            "AllowedMethods": ["GET", "PUT", "POST", "DELETE", "HEAD"],
            "AllowedOrigins": [str(settings.ui_uri).strip("/")],
            "ExposeHeaders": ["Etag", "content-range", "content-length"],
            "MaxAgeSeconds": 100,
        },
    ]
}


def get_base_policy(bucket_name: str, uid: UUID) -> list[dict[str, Any]]:
    return [
        {
            "Sid": "ProxyOwnerPerm",
            "Effect": "Allow",
            "Principal": {"AWS": f"arn:aws:iam:::user/{settings.s3.username}"},
            "Action": ["s3:GetObject"],
            "Resource": [f"arn:aws:s3:::{bucket_name}/*"],
        },
        {
            "Sid": "PseudoOwnerPerm",
            "Effect": "Allow",
            "Principal": {"AWS": f"arn:aws:iam:::user/{uid}"},
            "Action": [
                "s3:GetObject",
                "s3:DeleteObject",
                "s3:PutObject",
                "s3:ListBucket",
                "s3:AbortMultipartUpload",
                "s3:ListBucketMultipartUploads",
                "s3:ListMultipartUploadParts",
            ],
            "Resource": [f"arn:aws:s3:::{bucket_name}/*", f"arn:aws:s3:::{bucket_name}"],
        },
    ]


async def get_s3_bucket_policy(
    bucket_name: str,
    *,
    s3: S3ServiceResource,
) -> dict[str, Any]:
    """
    Wrapper for loading a bucket policy from a S3 bucket.

    Parameters
    ----------
    s3 : boto3.resource
        S3 Resource object to communicate with the S3 API.
    bucket_name : str
        Name of a bucket.

    Returns
    -------
    policy : dict[str, Any]
        The S3 bucket policy object associated to the given bucket.
    """
    with tracer.start_as_current_span("s3_get_bucket_policy", attributes={"bucket_name": bucket_name}):
        s3_policy = await s3.BucketPolicy(bucket_name=bucket_name)
        try:
            return json.loads(await s3_policy.policy)
        except ClientError:  # pragma: no cover
            return {"Version": "2012-10-17", "Statement": []}


async def put_s3_bucket_policy(
    bucket_name: str,
    policy: str,
    *,
    s3: S3ServiceResource,
) -> None:
    """
    Wrapper for putting a bucket policy on a S3 bucket.

    Parameters
    ----------
    s3 : boto3.resource
        S3 Resource object to communicate with the S3 API.
    bucket_name : str
        Name of a bucket.
    policy : str
        The new serialized bucket policy.
    """
    with tracer.start_as_current_span("s3_put_bucket_policy", attributes={"bucket_name": bucket_name}):
        s3_policy = await s3.BucketPolicy(bucket_name=bucket_name)
        await s3_policy.put(Policy=policy)


async def add_s3_bucket_policy_stmt(*stmts: dict[str, Any], bucket_name: str, s3: S3ServiceResource) -> None:
    """
    Add a Statement to a bucket policy. If it doesn't exist, then create a policy.

    Parameters
    ----------
    s3 : types_aiobotocore_s3.service_resource import S3ServiceResource
        S3 Service to perform operations on buckets.
    bucket_name : str
        Name of the bucket.
    stmts : dict[str, Any]
        Statements to add to the policy.
    """
    with tracer.start_as_current_span(
        "s3_add_bucket_policy_statement", attributes={"bucket_name": bucket_name, "policy_stmt": json.dumps(stmts)}
    ):
        policy = await get_s3_bucket_policy(bucket_name, s3=s3)
        for stmt in stmts:
            policy["Statement"].append(stmt)
        await put_s3_bucket_policy(bucket_name=bucket_name, policy=json.dumps(policy), s3=s3)


async def delete_s3_bucket_policy_stmt(bucket_name: str, sid: str | list[str], *, s3: S3ServiceResource) -> None:
    """
    Delete one or multiple Statement based on the Sid from a bucket policy.

    Parameters
    ----------
    s3 : types_aiobotocore_s3.service_resource import S3ServiceResource
        S3 Service to perform operations on buckets.
    bucket_name : str
        Name of the bucket.
    sid : str | list[str]
        ID or IDs of the statement(s).
    """
    with tracer.start_as_current_span(
        "s3_delete_bucket_policy_statement", attributes={"bucket_name": bucket_name, "sid": sid}
    ):
        policy = await get_s3_bucket_policy(bucket_name, s3=s3)

        if isinstance(sid, list):
            policy["Statement"] = [stmt for stmt in policy["Statement"] if stmt["Sid"] not in sid]
        else:
            policy["Statement"] = [stmt for stmt in policy["Statement"] if stmt["Sid"] != sid]
        await put_s3_bucket_policy(bucket_name=bucket_name, policy=json.dumps(policy), s3=s3)


async def get_s3_object(
    bucket_name: str,
    key: str,
    *,
    s3: S3ServiceResource,
) -> ObjectSummary | None:
    """
    Get the object summary from S3 if the object exists.

    Parameters
    ----------
    s3 : types_aiobotocore_s3.service_resource import S3ServiceResource
        S3 Service to perform operations on buckets.
    bucket_name : str
        Name of the bucket.
    key : str
        Key of the objects.

    Returns
    -------
    obj : types_aiobotocore_s3.service_resource.ObjectSummary | None
        The object summary if the object exists.
    """
    with tracer.start_as_current_span(
        "s3_get_object_meta_data", attributes={"bucket_name": bucket_name, "key": key}
    ) as span:
        try:
            obj = await s3.ObjectSummary(bucket_name=bucket_name, key=key)
            await obj.load()
        except ClientError:
            return None
        span.set_attributes(
            {"size": await obj.size, "last_modified": (await obj.last_modified).isoformat(), "etag": await obj.e_tag}
        )
        return obj


async def delete_s3_obj(
    bucket_name: str,
    key: str,
    *,
    s3: S3ServiceResource,
) -> None:
    """
    Delete an object in S3.

    Parameters
    ----------
    bucket_name : str
        The name of the S3 bucket.
    key : str
        The key for the S3 object.
    s3 : boto3.resource
        S3 Resource object to communicate with the S3 API.
    """
    with tracer.start_as_current_span("s3_delete_object", attributes={"bucket_name": bucket_name, "key": key}):
        obj = await s3.Object(bucket_name=bucket_name, key=key)
        await obj.delete()


async def delete_s3_objects(
    bucket_name: str,
    prefix: str | None = None,
    *,
    s3: S3ServiceResource,
) -> None:
    """
    Delete multiple S3 objects based on a prefix.

    Parameters
    ----------
    s3 : types_aiobotocore_s3.service_resource import S3ServiceResource
        S3 Service to perform operations on buckets.
    bucket_name : str
        Name of the bucket.
    prefix : str
        Prefix of the keys that should get deleted.
    """
    with tracer.start_as_current_span("s3_delete_objects", attributes={"bucket_name": bucket_name}) as span:
        bucket = await s3.Bucket(bucket_name)
        obj_collection = bucket.objects
        if prefix is not None:
            span.set_attribute("prefix", prefix)
            obj_collection = obj_collection.filter(Prefix=prefix)
        deleted_objs = await obj_collection.delete()
        if len(deleted_objs) > 0:
            deleted_keys = [obj["Key"] for obj in deleted_objs[0]["Deleted"]]
            span.set_attribute("keys", deleted_keys)


async def upload_s3_obj(
    bucket_name: str, key: str, handle: FileobjTypeDef, *, s3: S3ServiceResource, **kwargs: Any
) -> None:
    """
    Delete an object in S3.

    Parameters
    ----------
    bucket_name : str
        The name of the S3 bucket.
    key : str
        The key for the S3 object.
    handle : types_aiobotocore_s3.type_defs.FileobjTypeDef
        File-like object in binary mode
    s3 : boto3.resource
        S3 Resource object to communicate with the S3 API.
    """
    with tracer.start_as_current_span("s3_upload_object", attributes={"bucket_name": bucket_name, "key": key}):
        bucket = await s3.Bucket(bucket_name)
        with handle as f:
            await bucket.upload_fileobj(f, Key=key, **kwargs)


async def copy_s3_obj(
    src_bucket_name: str, src_key: str, dst_bucket_name: str, dst_key: str, *, s3: S3ServiceResource
) -> None:
    """
    Copy an object in S3.

    Parameters
    ----------
    src_bucket_name : str
        The name of the S3 source bucket.
    src_key : str
        The key for the S3 source object.
    dst_bucket_name : str
        The name of the S3 destination bucket.
    dst_key : str
        The key for the S3 destination object.
    s3 : boto3.resource
        S3 Resource object to communicate with the S3 API.
    """
    with tracer.start_as_current_span(
        "s3_copy_object",
        attributes={
            "src_bucket_name": src_bucket_name,
            "src_key": src_key,
            "dst_bucket_name": dst_bucket_name,
            "dst_key": dst_key,
        },
    ):
        dst_obj = await s3.Object(bucket_name=dst_bucket_name, key=dst_key)
        await dst_obj.copy({"Bucket": src_bucket_name, "Key": src_key})


async def setup_s3_data_bucket(s3: S3ServiceResource) -> None:  # pragma: no cover
    """
    Initialise the clowm data bucket and attach a cors configuration and bucket policy to it.

    Parameters
    ----------
    s3 : boto3.resource
        S3 Resource object to communicate with the S3 API.
    """
    s3_bucket = await s3.Bucket(settings.s3.data_bucket)
    await s3_bucket.create()
    s3_cors = await s3_bucket.Cors()
    try:
        await s3_cors.load()
    except ClientError:
        print("Put Cors rules on data bucket")
        await s3_cors.put(CORSConfiguration=cors_rule)  # type: ignore[arg-type]
    s3_policy = await s3_bucket.Policy()
    try:
        await s3_policy.load()
    except ClientError:
        print("Put a basic policy on data bucket")
        await s3_policy.put(
            Policy=json.dumps(
                {
                    "Version": "2012-10-17",
                    "Statement": [
                        {
                            "Sid": "ManagerReadPermissions",
                            "Effect": "Allow",
                            "Principal": {"AWS": f"arn:aws:iam:::user/{settings.s3.username}"},
                            "Action": ["s3:GetObject"],
                            "Resource": [f"arn:aws:s3:::{settings.s3.data_bucket}/*"],
                        },
                        {
                            "Sid": "IconAccess",
                            "Effect": "Allow",
                            "Principal": "*",
                            "Resource": f"arn:aws:s3:::{settings.s3.data_bucket}/{settings.s3.icon_key('*')}",
                            "Action": ["s3:GetObject"],
                        },
                    ],
                }
            )
        )
