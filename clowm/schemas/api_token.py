import time
from functools import cached_property

from pydantic import BaseModel, Field, field_validator

from clowm.db.types import ApiTokenScopes, ApiTokenScopesDB
from clowm.models.api_token import ApiToken

from .types import UUID


class _ApiTokeBase(BaseModel):
    name: str = Field(
        description="Short name for the API token",
        examples=["api-token-42"],
        min_length=3,
        max_length=63,
        pattern=r"^[a-z\d-]+$",
    )
    expires_at: int | None = Field(
        default=None,
        description="Unix timestamp when the token should expire",
        examples=[1719784800],
        ge=1,
        le=(1 << 32) - 1,
    )
    scopes: list[ApiTokenScopes] = Field(min_length=1, description="List of scopes this Api token has")

    @cached_property
    def scope_db(self) -> ApiTokenScopesDB:
        return ApiTokenScopesDB.parse(self.scopes)


class ApiTokenIn(_ApiTokeBase):
    @field_validator("expires_at")
    @classmethod
    def check_expires_at_field(cls, expires_at: int | None) -> int | None:
        if expires_at is not None and expires_at < time.time():
            raise ValueError("expires_at must be in the future")
        return expires_at


class ApiTokenOut(_ApiTokeBase):
    token_id: UUID = Field(description="The ID of the token", examples=["b4c861a7-7f52-4332-a001-78e0500dabbc"])
    uid: UUID = Field(description="The ID of the owner", examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"])
    created_at: int = Field(description="The UNIX timestamp when this token was created", examples=["1717192800"])
    last_used: int | None = Field(
        default=None, description="The UNIX timestamp when this token was used the last time", examples=["1717193800"]
    )

    @staticmethod
    def from_db(token: ApiToken) -> "ApiTokenOut":
        return ApiTokenOut(
            uid=token.uid,
            token_id=token.token_id,
            expires_at=token.expires_at,
            scopes=token.scopes.render(),
            created_at=token.created_at,
            name=token.name,
            last_used=token.last_used,
        )


class ApiTokenPrivateOut(ApiTokenOut):
    token: str = Field(
        description="The actual token used for authentication", examples=["J21NRKUYgyVUgvJ3cIdllS-MMa9ny1UDKFF18aetDvo"]
    )

    @staticmethod
    def from_db(token: ApiToken) -> "ApiTokenPrivateOut":
        return ApiTokenPrivateOut(
            uid=token.uid,
            token_id=token.token_id,
            token=token.token,
            expires_at=token.expires_at,
            scopes=token.scopes.render(),
            created_at=token.created_at,
            name=token.name,
            last_used=token.last_used,
        )
