from pydantic import BaseModel, Field

from .types import UUID


class S3Key(BaseModel):
    """
    Schema for a S3 key associated with a user.
    """

    uid: UUID = Field(
        ..., description="UID of the user of that access key", examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"]
    )
    access_key: str = Field(..., description="ID of the S3 access key", examples=["CRJ6B037V2ZT4U3W17VC"])
    secret_key: str = Field(
        ...,
        description="Secret of the S3 access key",
        examples=["2F5uNTI1qvt4oAroXV0wWct8rWclL2QvFXKqSqjS"],
    )
