import time
from collections.abc import Sequence
from datetime import date

from pydantic import AnyHttpUrl, BaseModel, Field, computed_field

from clowm.models import User
from clowm.models import Workflow as WorkflowDB
from clowm.models import WorkflowVersion as WorkflowVersionDB

from ..core.config import settings
from .types import UUID
from .workflow_mode import WorkflowModeIn
from .workflow_version import GitCommitField, NextflowVersionField, VersionField, WorkflowVersionOut


class _BaseWorkflow(BaseModel):
    name: str = Field(
        ...,
        description="Short descriptive name of the workflow",
        examples=["RNA ReadMapper"],
        max_length=64,
        min_length=3,
    )
    short_description: str = Field(
        ...,
        description="Short description of the workflow",
        examples=["This should be a very good example of a short and descriptive description"],
        max_length=256,
        min_length=64,
    )
    repository_url: AnyHttpUrl = Field(
        ...,
        description="URL to the Git repository belonging to this workflow",
        examples=["https://github.com/example-user/example"],
    )


class WorkflowIn(_BaseWorkflow):
    git_commit_hash: GitCommitField
    initial_version: VersionField
    token: str | None = Field(
        None,
        description="Token to access the content git repository",
        examples=["vnpau89avpa48iunga984gh9h89pvhj"],
        max_length=128,
    )
    modes: list[WorkflowModeIn] = Field(
        default=[], max_length=10, description="List of modes with alternative entrypoint the new workflow has"
    )
    nextflow_version: NextflowVersionField


class WorkflowOut(_BaseWorkflow):
    workflow_id: UUID = Field(..., description="ID of the workflow", examples=["20128c04-e834-40a8-9878-68939ae46423"])
    versions: list[WorkflowVersionOut] = Field(..., description="Versions of the workflow")
    developer_id: UUID | None = Field(
        None, description="ID of developer of the workflow", examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"]
    )
    private: bool = Field(default=False, description="Flag if the workflow is hosted in a private git repository")

    @staticmethod
    def from_db_workflow(
        db_workflow: WorkflowDB, versions: Sequence[WorkflowVersionDB] | None = None, load_modes: bool = True
    ) -> "WorkflowOut":
        """
        Create a WorkflowOut schema from the workflow database model.

        Parameters
        ----------
        db_workflow : clowm.models.Workflow
            Database model of a workflow.
        versions : list[clowm.models.WorkflowVersion] | None, default None
            List of versions to attach to the schema. If None, they will be loaded from the DB model.
        load_modes : bool, default True
            Flag if the workflow versions should load the modes from the database model.

        Returns
        -------
        workflow : WorkflowOut
            Schema from the database model
        """
        temp_versions = versions if versions is not None else db_workflow.versions
        return WorkflowOut(
            workflow_id=db_workflow.workflow_id,
            name=db_workflow.name,
            short_description=db_workflow.short_description,
            repository_url=db_workflow.repository_url,
            versions=[WorkflowVersionOut.from_db_version(v, load_modes=load_modes) for v in temp_versions],
            developer_id=db_workflow.developer_id,
            private=db_workflow.credentials_token is not None,
        )


class WorkflowStatistic(BaseModel):
    day: date = Field(..., description="Day of the datapoint", examples=[date(day=1, month=1, year=2023)])
    count: int = Field(..., description="Number of started workflows on that day", examples=[1])


class WorkflowCredentialsIn(BaseModel):
    token: str = Field(
        ...,
        description="Token to access the content git repository",
        examples=["vnpau89avpa48iunga984gh9h89pvhj"],
        max_length=128,
    )


class WorkflowCredentialsOut(BaseModel):
    token: str | None = Field(
        None,
        description="Token to access the content git repository",
        examples=["vnpau89avpa48iunga984gh9h89pvhj"],
        max_length=128,
    )


class WorkflowUpdate(BaseModel):
    version: VersionField
    git_commit_hash: GitCommitField
    append_modes: list[WorkflowModeIn] = Field(
        default_factory=lambda: [], description="Add modes to the new workflow version"
    )
    delete_modes: list[UUID] = Field(
        default_factory=lambda: [],
        description="Delete modes for the new workflow version.",
        examples=["2a23a083-b6b9-4681-9ec4-ff4ffbe85d3c"],
    )


class PublicWorkflowOut(_BaseWorkflow):
    workflow_id: UUID = Field(..., description="ID of the workflow", examples=["20128c04-e834-40a8-9878-68939ae46423"])
    latest_git_commit_hash: GitCommitField
    latest_version_tag: VersionField
    developer: str = Field(default="", description="Name of the developer", examples=["Bilbo Baggins"])
    icon_slug: str | None = Field(exclude=True, default=None)
    latest_version_timestamp: int = Field(
        ...,
        description="Timestamp when the last version was created as UNIX timestamp",
        examples=[1640991600],  # 01.01.2022 00:00
        le=(1 << 32) - 1,
        ge=1,
    )

    @computed_field(  # type: ignore[misc]
        description="URL to the workflow icon",
        examples=[
            settings.s3.icon_url("980a9446c7f2460c83187cbb876f8424.png"),
        ],
    )
    @property
    def icon_url(self) -> AnyHttpUrl | None:
        if self.icon_slug is None:
            return None
        return settings.s3.icon_url(self.icon_slug)

    @staticmethod
    def from_db_model(workflow: WorkflowDB, developer: User | None = None) -> "PublicWorkflowOut":
        latest_version = workflow.latest_version
        return PublicWorkflowOut(
            workflow_id=workflow.workflow_id,
            name=workflow.name,
            short_description=workflow.short_description,
            repository_url=workflow.repository_url,
            developer="" if developer is None else developer.display_name,
            latest_git_commit_hash="" if latest_version is None else latest_version.git_commit_hash,
            latest_version_tag="" if latest_version is None else latest_version.version,
            icon_slug=None if latest_version is None else latest_version.icon_slug,
            latest_version_timestamp=round(time.time()) if latest_version is None else latest_version.created_at,
        )
