from datetime import datetime
from pathlib import Path as LibPath
from typing import Annotated, Any

from pydantic import AnyHttpUrl, BaseModel, Field
from pydantic.functional_serializers import PlainSerializer

from .types import UUID

# ruff: noqa: N815


class _BaseTraceRequest(BaseModel):
    instant: int | float = Field(..., description="Unix timestamp of the request")


DateTime = Annotated[datetime, PlainSerializer(lambda date: date.isoformat(), return_type=str, when_used="unless-none")]
Path = Annotated[LibPath, PlainSerializer(lambda path: str(path), return_type=str, when_used="unless-none")]


class Process(BaseModel):
    index: int
    pending: int
    ignored: int
    loadCpus: int
    succeeded: int
    running: int
    retries: int
    peakRunning: int
    name: str
    loadMemory: int
    stored: int
    terminated: bool
    aborted: int
    failed: int
    peakCpus: int
    peakMemory: int
    cached: int
    submitted: int


class Stats(BaseModel):
    succeededCount: int
    computeTimeFmt: str
    cachedCount: int
    processes: list[Process]
    changeTimestamp: int
    peakRunning: int
    succeedDuration: int
    cachedPct: float
    loadMemory: int
    succeedCountFmt: str
    failedPct: float
    ignoredCount: int
    submittedCount: int
    peakMemory: int
    succeedPct: float
    succeedCount: int
    runningCount: int
    pendingCount: int
    loadCpus: int
    cachedDuration: int
    abortedCount: int
    failedDuration: int
    failedCount: int
    loadMemoryFmt: str
    retriesCount: int
    cachedCountFmt: str
    progressLength: int
    peakMemoryFmt: str
    failedCountFmt: str
    ignoredCountFmt: str
    peakCpus: int
    ignoredPct: float


class Manifest(BaseModel):
    doi: str | None = None
    nextflowVersion: str | None = None
    defaultBranch: str | None = None
    version: str | None = None
    homePage: AnyHttpUrl | None = Field(None, examples=["https://example.com"])
    gitmodules: str | None = None
    description: str | None = None
    recurseSubmodules: bool
    name: str | None = None
    mainScript: str
    author: str | None = None


class NextflowVersion(BaseModel):
    version: str
    build: int
    timestamp: DateTime
    enable: dict[str, int]


class Workflow(BaseModel):
    start: DateTime
    projectDir: Path
    manifest: Manifest
    complete: DateTime | None = None
    profile: str
    homeDir: Path
    workDir: Path
    container: str | None = None
    commitId: str | None = None
    errorMessage: str | None = None
    repository: AnyHttpUrl | None = Field(None, examples=["https://example.com"])
    containerEngine: str | None = None
    scriptFile: Path
    userName: str
    launchDir: Path
    runName: str
    configFiles: list[Path]
    sessionId: str
    errorReport: str | None = None
    workflowStats: Stats | None = None
    scriptId: str
    revision: str | None = None
    exitStatus: int | None = None
    commandLine: str
    stubRun: bool
    nextflow: NextflowVersion
    stats: Stats | None = Field(None)
    resume: bool
    success: bool
    projectName: str
    scriptName: str
    duration: int | None = None
    params: Any | None = None
    id: str
    configText: str
    operationId: str | None = None
    logFile: str | None = None
    outFile: str | None = None


class Task(BaseModel):
    taskId: int
    status: str
    hash: str
    name: str
    exit: int
    submit: DateTime | None = None
    start: DateTime | None = None
    process: str
    tag: str | None = None
    module: list[str]
    container: str | None = None
    attempt: int
    script: str
    scratch: str | bool | None = None
    workdir: Path
    queue: str | None = None
    cpus: int
    memory: int | None = None
    disk: str | None = None
    time: int | None = None
    errorAction: str | None = None
    complete: DateTime | None = None
    env: str | None = None
    duration: int | None = Field(None)
    realtime: int | None = Field(None)
    pcpu: float | None = Field(None)
    rchar: int | None = Field(None)
    wchar: int | None = Field(None)
    syscr: int | None = Field(None)
    syscw: int | None = Field(None)
    readBytes: int | None = Field(None)
    writeBytes: int | None = Field(None)
    pmem: float | None = Field(None)
    vmem: int | None = Field(None)
    rss: int | None = Field(None)
    peakVmem: int | None = Field(None)
    peakRss: int | None = Field(None)
    volCtxt: int | None = Field(None)
    invCtxt: int | None = Field(None)
    nativeId: int | None = None
    executor: str | None = None
    cloudZone: str | None = None
    machineType: str | None = None
    priceModel: str | None = None


class Progress(BaseModel):
    pending: int
    ignored: int
    loadCpus: int
    loadMemory: int
    processes: list[Process]
    aborted: int
    succeeded: int
    peakMemory: int
    peakCpus: int
    failed: int
    running: int
    retries: int
    peakRunning: int
    cached: int
    submitted: int


class Metric(BaseModel):
    mean: float
    min: float
    q1: float
    q2: float
    q3: float
    max: float
    minLabel: str
    maxLabel: str
    q1Label: str
    q2Label: str
    q3Label: str


class ProcessMetric(BaseModel):
    cpuUsage: Metric | None = None
    process: str
    mem: Metric | None = None
    memUsage: Metric | None = None
    timeUsage: Metric | None = None
    vmem: Metric | None = None
    reads: Metric | None = None
    cpu: Metric | None = None
    time: Metric | None = None
    writes: Metric | None = None


class CompleteWorkflow(_BaseTraceRequest):
    workflow: Workflow
    metrics: list[ProcessMetric]
    progress: Progress


class ReportProgress(_BaseTraceRequest):
    tasks: list[Task] | None = None
    progress: Progress


class BeginWorkflow(_BaseTraceRequest):
    workflow: Workflow
    processNames: list[str]
    towerLaunch: bool


class _CreateWorkflowBase(BaseModel):
    workflowId: str | None = Field(None, description="ID of the current workflow")


class CreateWorkflowIn(_CreateWorkflowBase, _BaseTraceRequest):
    sessionId: UUID = Field(..., description="Session ID of the nextflow run")
    projectName: str = Field(..., description="Name of the Pipeline nextflow executes")
    repository: AnyHttpUrl | None = Field(
        None, description="URL of the repository the pipeline comes from", examples=["https://example.com"]
    )
    runName: str = Field(..., description="Randomly generated name of the run")


class CreateWorkflowOut(_CreateWorkflowBase):
    pass
