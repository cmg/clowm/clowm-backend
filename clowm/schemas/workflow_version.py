from enum import IntEnum, StrEnum, unique
from typing import Annotated

from pydantic import AnyHttpUrl, BaseModel, Field

from clowm.core.config import settings
from clowm.models import WorkflowVersion as WorkflowVersionDB

from .types import UUID


@unique
class NextflowVersion(StrEnum):
    v22_10_0 = "22.10.0"
    v22_10_1 = "22.10.1"
    v22_10_2 = "22.10.2"
    v22_10_3 = "22.10.3"
    v22_10_4 = "22.10.4"
    v22_10_5 = "22.10.5"
    v22_10_6 = "22.10.6"
    v22_10_7 = "22.10.7"
    v22_10_8 = "22.10.8"
    v23_04_0 = "23.04.0"
    v23_04_1 = "23.04.1"
    v23_04_2 = "23.04.2"
    v23_04_3 = "23.04.3"
    v23_04_4 = "23.04.4"
    v23_04_5 = "23.04.5"
    v23_10_0 = "23.10.0"
    v23_10_1 = "23.10.1"
    v23_10_2 = "23.10.2"
    v23_10_3 = "23.10.3"
    v23_10_4 = "23.10.4"
    v24_04_1 = "24.04.1"
    v24_04_2 = "24.04.2"
    v24_04_3 = "24.04.3"
    v24_04_4 = "24.04.4"
    v24_10_0 = "24.10.0"
    v24_10_1 = "24.10.1"
    v24_10_2 = "24.10.2"
    v24_10_3 = "24.10.3"
    v24_10_4 = "24.10.4"


@unique
class ParameterVisibility(IntEnum):
    SIMPLE = 10
    ADVANCED = 20
    EXPERT = 30
    INVISIBLE = 40


type ParameterVisibilityMapping = dict[str, ParameterVisibility]
type InstanceSpecificDefaultParameters = dict[str, str | int | float | bool]

NextflowVersionField = Annotated[
    NextflowVersion,
    Field(description="The version of Nextflow this workflow version requires.", examples=[NextflowVersion.v24_04_4]),
]
VersionField = Annotated[
    str,
    Field(
        description="Version of the Workflow. Should follow semantic versioning",
        examples=["v1.1.0"],
        min_length=5,
        max_length=10,
    ),
]
GitCommitField = Annotated[
    str,
    Field(
        description="Hash of the git commit",
        examples=["ba8bcd9294c2c96aedefa1763a84a18077c50c0f"],
        pattern=r"^[0-9a-f]{40}$",
        min_length=40,
        max_length=40,
    ),
]
DefaultContainerField = Annotated[
    str | None,
    Field(
        default=None,
        pattern=r":[a-zA-Z0-9_][a-zA-Z0-9-_\.]{0,127}$",
        description="Default container to use for all Nextflow processes that have to container specified",
        examples=["debian:12-slim"],
    ),
]


class ParameterExtension(BaseModel):
    mapping: dict[str, dict[str, str | int | float]] = Field(
        default_factory=lambda: {},
        description="The inner dictionary contains the display name as key and the parameter value as value. The "
        "outer dictionary has the parameter name as key.",
        examples=[{"some-complex-parameter": {"Option 1": "/some/path", "Option 2": "/some/other/path"}}],
    )
    defaults: InstanceSpecificDefaultParameters = Field(
        default_factory=lambda: {},
        description="Dictionary with parameter name as key and default value as value",
        examples=[{"parameter1": "somevalue", "parameter2": 12}],
    )
    parameter_visibility: ParameterVisibilityMapping = Field(default_factory=lambda: {})


class WorkflowVersionStatusSchema(BaseModel):
    status: WorkflowVersionDB.WorkflowVersionStatus = Field(
        ..., description="Status of the workflow version", examples=[WorkflowVersionDB.WorkflowVersionStatus.PUBLISHED]
    )


class WorkflowVersionOut(WorkflowVersionStatusSchema):
    workflow_id: UUID = Field(
        ..., description="ID of the corresponding workflow", examples=["20128c04-e834-40a8-9878-68939ae46423"]
    )
    version: VersionField
    icon_url: AnyHttpUrl | None = Field(
        None,
        description="URL to the uploaded icon",
        examples=[
            settings.s3.icon_url("980a9446c7f2460c83187cbb876f8424.png"),
        ],
    )
    workflow_version_id: GitCommitField
    created_at: int = Field(
        ...,
        description="Timestamp when the version was created as UNIX timestamp",
        examples=[1640991600],  # 01.01.2022 00:00
        le=(1 << 32) - 1,
        ge=1,
    )
    modes: list[UUID] = Field(
        default=[],
        description="Optional modes his workflow version has",
        examples=["2a23a083-b6b9-4681-9ec4-ff4ffbe85d3c"],
    )
    parameter_extension: ParameterExtension | None = Field(
        default=None, description="Parameter extension specific for this CloWM instance"
    )
    nextflow_version: NextflowVersionField

    @staticmethod
    def from_db_version(
        db_version: WorkflowVersionDB, mode_ids: list[UUID] | None = None, load_modes: bool = False
    ) -> "WorkflowVersionOut":
        """
        Create a WorkflowVersion schema from the workflow version database model.

        Parameters
        ----------
        db_version : clowm.models.WorkflowVersion
            Database model of a workflow.version
        mode_ids : list[uuid.UUID] | None, default None
            List of mode IDs to add to the workflow version
        load_modes : bool, default False
            Flag if the workflow versions should load the modes from the database model.

        Returns
        -------
        version : WorkflowVersionOut
            Schema from the database model

        Notes
        -----
        If the parameter `load_modes` is True and the database model has a non-empty list of modes,
        then the parameter `mode_ids` will be ignored.
        """
        if mode_ids is None:
            mode_ids = []
        return WorkflowVersionOut(
            workflow_id=db_version.workflow_id,
            version=db_version.version,
            workflow_version_id=db_version.git_commit_hash,
            icon_url=settings.s3.icon_url(db_version.icon_slug) if db_version.icon_slug is not None else None,
            created_at=db_version.created_at,
            nextflow_version=db_version.nextflow_version,
            status=db_version.status,
            modes=(
                [mode.mode_id for mode in db_version.workflow_modes]
                if load_modes and len(db_version.workflow_modes) > 0
                else mode_ids
            ),
            parameter_extension=(
                None
                if db_version.parameter_extension is None
                else ParameterExtension.model_validate(db_version.parameter_extension)
            ),
        )


class IconUpdateOut(BaseModel):
    icon_url: AnyHttpUrl = Field(
        ...,
        description="URL to the uploaded icon",
        examples=[
            settings.s3.icon_url("980a9446c7f2460c83187cbb876f8424.png"),
        ],
    )


class _WorkflowVersionMetadataBase(BaseModel):
    nextflow_version: NextflowVersionField
    nextflow_config: str | None = Field(
        default=None, description="Nextflow config that overrides the config in the git repository"
    )
    default_container: DefaultContainerField


class WorkflowVersionMetadataIn(_WorkflowVersionMetadataBase):
    pass


class WorkflowVersionMetadataOut(_WorkflowVersionMetadataBase):
    @staticmethod
    def from_db_model(model: WorkflowVersionDB) -> "WorkflowVersionMetadataOut":
        return WorkflowVersionMetadataOut(
            nextflow_version=model.nextflow_version,
            nextflow_config=model.nextflow_config,
            default_container=model.default_container,
        )
