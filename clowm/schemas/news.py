from pydantic import BaseModel, ConfigDict, Field

from clowm.db.types import NewsCategory

from .types import UUID


class BaseNews(BaseModel):
    content: str = Field(
        ...,
        description="Content of the news. Can contain Markdown.",
        min_length=16,
        max_length=2**16,
        examples=["## Header\n\nSome text"],
    )
    title: str = Field(..., description="Title of the news", min_length=3, max_length=256, examples=["Some title"])
    important_till: int | None = Field(
        None, description="UNIX timestamp till the news is important.", ge=1, le=(1 << 32) - 1, examples=[1640991600]
    )
    category: NewsCategory = Field("system", description="Category of the news event")


class NewsOut(BaseNews):
    news_id: UUID = Field(
        ...,
        description="ID of the news event",
        examples=["f3e2acf0-e942-44b2-8f85-52b7deb660ec"],
    )
    creator_id: UUID | None = Field(
        None,
        description="ID of the creator",
        examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
    )
    created_at: int = Field(
        ...,
        examples=[1640991600],  # 01.01.2022 00:00
        description="UNIX timestamp when the bucket was created",
        ge=1,
        le=(1 << 32) - 1,
    )
    category: NewsCategory = Field(..., description="Category of the news event")
    model_config = ConfigDict(from_attributes=True)


class NewsIn(BaseNews):
    pass
