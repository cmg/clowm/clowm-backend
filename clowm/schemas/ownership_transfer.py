from pydantic import BaseModel, Field

from clowm.db.mixins.ownership_transfer_mixin import OwnershipTransferRequestMixin, OwnershipTypeEnum

from .types import UUID


class _OwnershipTransferRequestBase(BaseModel):
    new_owner_uid: UUID = Field(
        ..., description="The new owner that get the request", examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"]
    )


class OwnershipTransferRequestIn(_OwnershipTransferRequestBase):
    comment: str | None = Field(
        default=None,
        description="An optional comment for the transfer request",
        max_length=256,
        examples=["This is an example comment"],
    )


class OwnershipTransferRequestOut(_OwnershipTransferRequestBase):
    comment: str = Field(
        default="",
        description="An optional comment for the transfer request",
        examples=["This is an example comment"],
    )
    created_at: int = Field(
        ...,
        examples=[1640991600],  # 01.01.2022 00:00
        description="Time when the ownership transfer was requested as UNIX timestamp",
        ge=1,
        le=(1 << 32) - 1,
    )
    current_owner_uid: UUID | None = Field(
        default=None,
        description="The current uid of the current owner if he exists",
        examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
    )
    target_id: UUID | str = Field(..., description="Id of the target that gets its ownership transferred")
    target_name: str = Field(
        ..., description="Name of the target", examples=["example-bucket", "Example Workflow", "Example Resource"]
    )
    target_description: str = Field(
        ..., description="Description of then target", examples=["Some long description of a target"]
    )
    target_type: OwnershipTypeEnum = Field(
        ..., description="Target type of the ownership transfer", examples=[OwnershipTypeEnum.BUCKET]
    )

    @staticmethod
    def from_db_model(model: OwnershipTransferRequestMixin) -> "OwnershipTransferRequestOut":
        return OwnershipTransferRequestOut(
            created_at=model.transfer_created_at,
            comment="" if model.transfer_comment is None else model.transfer_comment,
            new_owner_uid=model.transfer_new_owner_uid,
            current_owner_uid=model.clowm_owner_id,
            target_id=model.clowm_id,
            target_name=model.target_properties.name,
            target_description=model.target_properties.description,
            target_type=model.target_properties.type,
        )
