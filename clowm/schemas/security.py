from datetime import datetime

from pydantic import BaseModel, Field


class JWT(BaseModel):
    """
    Schema for a JWT. Only for convenience
    """

    exp: datetime
    sub: str
    raw_token: str


class ErrorDetail(BaseModel):
    """
    Schema for a error due to a rejected request.
    """

    detail: str = Field(..., description="Detail about the occurred error")
