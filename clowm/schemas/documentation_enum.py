from enum import StrEnum, unique


@unique
class DocumentationEnum(StrEnum):
    USAGE = "usage.md"
    INPUT = "input.md"
    OUTPUT = "output.md"
    CHANGELOG = "changelog.md"
    PARAMETER_SCHEMA = "parameter_schema.json"
    CLOWM_INFO = "clowm_info.json"
    CITATIONS = "CITATIONS.md"

    @property
    def repository_paths(self) -> list[str]:
        if self is DocumentationEnum.USAGE:
            return ["clowm/README.md", "README.md"]
        if self is DocumentationEnum.INPUT:
            return ["clowm/usage.md", "docs/usage.md"]
        if self is DocumentationEnum.OUTPUT:
            return ["clowm/output.md", "docs/output.md"]
        if self is DocumentationEnum.CHANGELOG:
            return ["clowm/CHANGELOG.md", "CHANGELOG.md"]
        if self is DocumentationEnum.CLOWM_INFO:
            return ["clowm/clowm_info.json", "clowm_info.json"]
        if self is DocumentationEnum.CITATIONS:
            return ["clowm/CITATIONS.md", "CITATIONS.md"]
        return ["clowm/nextflow_schema.json", "nextflow_schema.json"]
