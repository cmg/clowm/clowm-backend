from datetime import date, timedelta
from typing import Any

from pydantic import AnyHttpUrl, BaseModel, Field

from clowm.models import WorkflowExecution

from .types import UUID
from .workflow_mode import WorkflowModeIn
from .workflow_version import NextflowVersion


class _BaseWorkflowExecution(BaseModel):
    workflow_version_id: str = Field(
        ...,
        description="Workflow version git commit hash",
        examples=["ba8bcd9294c2c96aedefa1763a84a18077c50c0f"],
        pattern=r"^[0-9a-f]{40}$",
        min_length=40,
        max_length=40,
    )
    notes: str | None = Field(
        None,
        description="Optional notes for this workflow execution",
        max_length=2**16,
        examples=["Some workflow execution specific notes"],
    )
    mode_id: UUID | None = Field(
        default=None,
        description="ID of the workflow mode this workflow execution runs in",
        examples=["2a23a083-b6b9-4681-9ec4-ff4ffbe85d3c"],
    )


class _WorkflowExecutionInParameters(BaseModel):
    parameters: dict[str, Any] = Field(..., description="Parameters for this workflow")
    logs_s3_path: str | None = Field(
        None,
        description="S3 Path where to save logs and reports. If None, nothing will be uploaded.",
        min_length=3,
        max_length=1024,
        pattern=r"^s3://(\w){1}(\w-\./)*(\w){1}",
        examples=["s3://example-bucket/logs"],
    )
    provenance_s3_path: str | None = Field(
        None,
        description="S3 Path where to save provenance information. If None, nothing will be uploaded.",
        min_length=3,
        max_length=1024,
        pattern=r"^s3://(\w){1}(\w-\./)*(\w){1}",
        examples=["s3://example-bucket/provenance"],
    )
    debug_s3_path: str | None = Field(
        None,
        description="S3 Path where to save debug information from Nextflow. If None, nothing will be uploaded.",  # noqa: E501
        min_length=3,
        max_length=1024,
        pattern=r"^s3://(\w){1}(\w-\./)*(\w){1}",
        examples=["s3://example-bucket/debug"],
    )


class WorkflowExecutionIn(_BaseWorkflowExecution, _WorkflowExecutionInParameters):
    pass


class WorkflowExecutionOut(_BaseWorkflowExecution):
    execution_id: UUID = Field(
        ..., description="ID of the workflow execution", examples=["591b6a6e-a1f0-420d-8a20-a7a60704f695"]
    )
    executor_id: UUID | None = Field(
        default=None,
        description="UID of user who started the workflow",
        examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
    )
    start_time: int = Field(
        ...,
        description="Start time of the workflow execution as UNIX timestamp",
        examples=[1640991600],  # 01.01.2022 00:00
        le=(1 << 32) - 1,
        ge=1,
    )
    end_time: int | None = Field(
        None,
        description="End time of the workflow execution as UNIX timestamp",
        examples=[1640991600],  # 01.01.2022 00:00
        le=(1 << 32) - 1,
        ge=1,
    )
    status: WorkflowExecution.WorkflowExecutionStatus = Field(
        ...,
        description="Status of the workflow execution",
        examples=[WorkflowExecution.WorkflowExecutionStatus.RUNNING],
    )
    workflow_version_id: str | None = Field(  # type: ignore[assignment]
        None, description="Workflow version git commit hash", examples=["ba8bcd9294c2c96aedefa1763a84a18077c50c0f"]
    )
    workflow_id: UUID | None = Field(
        None, description="Id of the workflow", examples=["20128c04-e834-40a8-9878-68939ae46423"]
    )
    logs_s3_path: str | None = Field(
        None,
        description="S3 Path where logs and reports are saved.",
        examples=["s3://example-bucket/logs/run-591b6a6ea1f0420d8a20a7a60704f695"],
    )
    provenance_s3_path: str | None = Field(
        None,
        description="S3 Path where provenance information is saved.",
        examples=["s3://example-bucket/provenance/run-591b6a6ea1f0420d8a20a7a60704f695"],
    )
    debug_s3_path: str | None = Field(
        None,
        description="S3 Path where debug information from Nextflow is saved.",  # noqa: E501
        examples=["s3://example-bucket/debug/run-591b6a6ea1f0420d8a20a7a60704f695"],
    )
    cpu_time: timedelta = Field(..., description="The consumed cpu time in ISO 8601 format", examples=["P4DT12H30M5S"])

    @staticmethod
    def from_db_model(workflow_execution: WorkflowExecution, workflow_id: UUID | None = None) -> "WorkflowExecutionOut":
        return WorkflowExecutionOut(
            execution_id=workflow_execution.execution_id,
            executor_id=workflow_execution.executor_id,
            start_time=workflow_execution.start_time,
            end_time=workflow_execution.end_time,
            status=workflow_execution.status,
            workflow_version_id=workflow_execution.workflow_version_id,
            workflow_id=workflow_id,
            notes=workflow_execution.notes,
            mode_id=workflow_execution.workflow_mode_id,
            debug_s3_path=workflow_execution.debug_path,
            logs_s3_path=workflow_execution.logs_path,
            provenance_s3_path=workflow_execution.provenance_path,
            cpu_time=workflow_execution.cpu_time,
        )


class DevWorkflowExecutionIn(_WorkflowExecutionInParameters):
    git_commit_hash: str = Field(
        ...,
        description="Hash of the git commit",
        examples=["ba8bcd9294c2c96aedefa1763a84a18077c50c0f"],
        pattern=r"^[0-9a-f]{40}$",
        min_length=40,
        max_length=40,
    )
    repository_url: AnyHttpUrl = Field(
        ...,
        description="URL to the Git repository belonging to this workflow",
        examples=["https://github.com/example-user/example"],
    )
    token: str | None = Field(
        None,
        description="Token to access the content git repository",
        examples=["vnpau89avpa48iunga984gh9h89pvhj"],
        max_length=128,
    )
    mode: WorkflowModeIn | None = Field(default=None, description="Mode of the workflow with an alternative entrypoint")
    nextflow_version: NextflowVersion = Field(description="The version of Nextflow this workflow execution requires")


class AnonymizedWorkflowExecution(BaseModel):
    workflow_execution_id: UUID = Field(
        ..., description="ID of the workflow execution", examples=["591b6a6e-a1f0-420d-8a20-a7a60704f695"]
    )
    pseudo_uid: str = Field(
        ...,
        description="Anonymized user ID of the user who ran the workflow execution",
        examples=["7ed4249857b656e96f456449796e461e6001d3fb2481a44701f70ca437bd53a2"],
    )
    workflow_mode_id: UUID | None = Field(
        None,
        description="ID of the workflow mode this workflow execution ran in",
        examples=["2a23a083-b6b9-4681-9ec4-ff4ffbe85d3c"],
    )
    workflow_version_id: str = Field(
        ..., description="Hash of the git commit", examples=["ba8bcd9294c2c96aedefa1763a84a18077c50c0f"]
    )
    started_at: date = Field(
        ..., description="Day of the workflow execution", examples=[date(day=1, month=1, year=2023)]
    )
    workflow_id: UUID = Field(..., description="ID of the workflow", examples=["20128c04-e834-40a8-9878-68939ae46423"])
    developer_id: UUID = Field(
        ..., description="ID of developer of the workflow", examples=["28c5353b8bb34984a8bd4169ba94c606"]
    )
    status: WorkflowExecution.WorkflowExecutionStatus = Field(
        ...,
        description="End status of the workflow execution",
        examples=[WorkflowExecution.WorkflowExecutionStatus.SUCCESS],
    )
