import hashlib
from datetime import datetime
from typing import Any

from cryptography.utils import cached_property
from pydantic import BaseModel, Field

from clowm.db.types import BucketPermissionScopes, BucketPermissionScopesDB
from clowm.models import BucketPermission as BucketPermissionDB

from .types import UUID


class BucketPermissionParameters(BaseModel):
    """
    Schema for the parameters of a bucket permission.
    """

    from_timestamp: int | None = Field(
        None,
        description="Start date of permission as UNIX timestamp",
        ge=1,
        le=(1 << 32) - 1,
        examples=[1640991600],  # 01.01.2022 00:00
    )
    to_timestamp: int | None = Field(
        None,
        description="End date of permission as UNIX timestamp",
        ge=1,
        le=(1 << 32) - 1,
        examples=[1640991600],  # 01.01.2022 00:00
    )
    file_prefix: str | None = Field(
        None, description="Prefix of subfolder", examples=["pseudo/sub/folder/"], min_length=1, max_length=512
    )
    scopes: list[BucketPermissionScopes] = Field(..., description="Scopes of the bucket permission", min_length=1)

    @cached_property
    def scopes_db(self) -> BucketPermissionScopesDB:
        return BucketPermissionScopesDB.parse(self.scopes)


class BucketPermissionIn(BucketPermissionParameters):
    uid: UUID = Field(..., description="UID of the grantee", examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"])
    bucket_name: str = Field(..., description="Name of Bucket", examples=["test-bucket"], min_length=3, max_length=63)

    def to_hash(self) -> str:
        """
        Combine the bucket name and user id and produce the MD5 hash of it.

        Returns
        -------
        hash : str
            The resulting MD5 hash.
        """
        hasher = hashlib.md5(self.bucket_name.encode("utf-8"))  # noqa: S324
        hasher.update(self.uid.bytes)
        return hasher.hexdigest()

    def map_to_bucket_policy_statement(self) -> list[dict[str, Any]]:
        """
        Create a bucket policy statement from the schema and the user_id.
        The Sid is unique for every bucket and user combination.

        Returns
        -------
        statements : list[dict[str, Any]]
            Bucket and object permission statements.
        """
        own_hash = self.to_hash()
        obj_policy: dict[str, Any] = {
            "Sid": own_hash,
            "Effect": "Allow",
            "Principal": {"AWS": f"arn:aws:iam:::user/{str(self.uid)}"},
            "Resource": f"arn:aws:s3:::{self.bucket_name}/{'' if self.file_prefix is None else self.file_prefix}*",
            "Action": [],
            "Condition": {},
        }
        bucket_policy: dict[str, Any] = {
            "Sid": own_hash,
            "Effect": "Allow",
            "Principal": {"AWS": f"arn:aws:iam:::user/{str(self.uid)}"},
            "Resource": f"arn:aws:s3:::{self.bucket_name}",
            "Action": [],
            "Condition": {},
        }
        bucket_policy["Action"] += ["s3:ListBucket"]
        if BucketPermissionScopesDB.READ in self.scopes_db:
            obj_policy["Action"] += ["s3:GetObject"]
        if BucketPermissionScopesDB.WRITE in self.scopes_db:
            obj_policy["Action"] += ["s3:DeleteObject", "s3:PutObject"]
            bucket_policy["Action"] += ["s3:DeleteObject"]
        if self.to_timestamp is not None:
            obj_policy["Condition"]["DateLessThan"] = {
                "aws:CurrentTime": datetime.fromtimestamp(self.to_timestamp).strftime("%Y-%m-%dT%H:%M:%SZ")
            }
            bucket_policy["Condition"]["DateLessThan"] = obj_policy["Condition"]["DateLessThan"]
        if self.from_timestamp is not None:
            obj_policy["Condition"]["DateGreaterThan"] = {
                "aws:CurrentTime": datetime.fromtimestamp(self.from_timestamp).strftime("%Y-%m-%dT%H:%M:%SZ")
            }
            bucket_policy["Condition"]["DateGreaterThan"] = obj_policy["Condition"]["DateGreaterThan"]
        if self.file_prefix is not None:
            bucket_policy["Condition"]["StringLike"] = {"s3:prefix": self.file_prefix + "*"}
        if len(bucket_policy["Condition"]) == 0:
            del bucket_policy["Condition"]
        if len(obj_policy["Condition"]) == 0:
            del obj_policy["Condition"]
        return [obj_policy, bucket_policy]


class BucketPermissionOut(BucketPermissionIn):
    """
    Schema for the bucket permissions.
    """

    @staticmethod
    def from_db_model(permission: BucketPermissionDB) -> "BucketPermissionOut":
        """
        Create a bucket permission schema from the database model.

        Parameters
        ----------
        permission : clowm.models.BucketPermission
            DB model for the permission.

        Returns
        -------
        permission_schema : clowm.schemas.bucket_permission.BucketPermissionOut
            Schema populated with the values from the database model.
        """
        return BucketPermissionOut(
            uid=permission.uid,
            bucket_name=permission.bucket_name,
            from_timestamp=permission.from_,
            to_timestamp=permission.to,
            file_prefix=permission.file_prefix,
            scopes=permission.scopes.render(),
        )
