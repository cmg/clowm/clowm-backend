from hashlib import sha256

from pydantic import AnyHttpUrl, BaseModel, ConfigDict, EmailStr, Field, computed_field

from clowm.models import Role, User
from clowm.models.role import RoleIdMapping

from .types import UUID


class UserBase(BaseModel):
    display_name: str = Field(
        ...,
        description="Full Name of the user",
        examples=["Bilbo Baggins"],
        max_length=256,
    )


class UserIn(UserBase):
    roles: list[Role.RoleEnum] = Field(default_factory=lambda: [], examples=[[Role.RoleEnum.USER]])
    email: EmailStr = Field(..., description="Email of the user", examples=["user@example.org"])


class UserOut(UserBase):
    """
    Schema for a user.
    """

    uid: UUID = Field(
        ...,
        description="ID of the user",
        examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
    )
    model_config = ConfigDict(from_attributes=True)


class UserRoles(BaseModel):
    roles: list[Role.RoleEnum] = Field(..., description="Roles of the user", examples=[[Role.RoleEnum.USER]])


class UserOutExtended(UserOut, UserRoles):
    lifescience_id: str | None = Field(
        None, description="Lifesicence AAI ID of the user", examples=["18b59678f16d2c59306c0aedb1dc7ddcfe162456"]
    )
    nfdi_id: str | None = Field(
        None, description="NFDI AAI ID of the user", examples=["18b59678f16d2c59306c0aedb1dc7ddcfe162456"]
    )
    invitation_token_created_at: int | None = Field(
        None, description="Timestamp when the invitation token was created as UNIX timestamp"
    )
    email: EmailStr | None = Field(None, description="Email of the user", examples=["user@example.org"])
    last_login: int | None = Field(None, description="Timestamp when the user last logged in")

    @computed_field(  # type: ignore[misc]
        description="URL to the gravatar avatar based on the users email",
        examples=["https://gravatar.com/avatar/87a8c45825eb709ee8e453d2c0f8154e1b9bf4b45bb9b81b7662177340aa9d38"],
    )
    @property
    def gravatar_url(self) -> AnyHttpUrl:
        if self.email is None:
            return AnyHttpUrl(
                "https://gravatar.com/avatar/87a8c45825eb709ee8e453d2c0f8154e1b9bf4b45bb9b81b7662177340aa9d38"
            )
        return AnyHttpUrl(
            f"https://gravatar.com/avatar/{sha256(str(self.email).strip().lower().encode('utf-8')).hexdigest()}"
        )

    @staticmethod
    def from_db_user(user: User) -> "UserOutExtended":
        mapping = RoleIdMapping()
        return UserOutExtended(
            uid=user.uid,
            display_name=user.display_name,
            roles=[mapping.get_role_name(role.role_id) for role in user.roles],
            lifescience_id=user.lifescience_id,
            nfdi_id=user.nfdi_id,
            invitation_token_created_at=user.invitation_token_created_at,
            email=user.email,
            last_login=user.last_login,
        )
