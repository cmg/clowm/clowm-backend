import json
from collections.abc import Sequence
from uuid import UUID

from opentelemetry import trace
from sqlalchemy import desc, insert, or_, select, update
from sqlalchemy.orm import joinedload, selectinload

from clowm.crud.crud_base import CRUDBase
from clowm.models import WorkflowVersion, workflow_mode_association_table
from clowm.schemas.workflow_version import ParameterExtension

tracer = trace.get_tracer_provider().get_tracer(__name__)


class CRUDWorkflowVersion(CRUDBase):
    async def get(
        self, workflow_version_id: str, workflow_id: UUID | None = None, populate_workflow: bool = False
    ) -> WorkflowVersion | None:
        """
        Get a workflow version by its commit git_commit_hash.

        Parameters
        ----------
        workflow_version_id : str
            Git commit git_commit_hash of the version.
        workflow_id : UUID | None, default None
            Specify the workflow the version has to belong to.
        populate_workflow: boolean, default False
            Flag if to populate the workflow attribute.

        Returns
        -------
        user : clowm.models.WorkflowVersion | None
            The workflow version with the given git_commit_hash if it exists, None otherwise
        """
        with tracer.start_as_current_span(
            "db_get_workflow_version",
            attributes={"workflow_version_id": workflow_version_id, "populate_workflow": populate_workflow},
        ) as span:
            stmt = (
                select(WorkflowVersion)
                .where(WorkflowVersion.git_commit_hash == workflow_version_id)
                .options(selectinload(WorkflowVersion.workflow_modes))
            )
            if populate_workflow:
                stmt = stmt.options(joinedload(WorkflowVersion.workflow))
            if workflow_id is not None:
                span.set_attribute("workflow_id", str(workflow_id))
                stmt = stmt.where(WorkflowVersion.workflow_id == workflow_id)
            span.set_attribute("sql_query", str(stmt))
            return await self.db.scalar(stmt)

    async def get_latest(self, workflow_id: UUID, published: bool = True) -> WorkflowVersion | None:
        """
        Get the latest version of a workflow.

        Parameters
        ----------
        workflow_id : uuid.UUID
            Id of a workflow
        published : bool, default = True
            Get the latest versions that is published, otherwise get latest version overall

        Returns
        -------
        user : clowm.models.WorkflowVersion | None
            The latest workflow version of the given workflow if the workflow exists, None otherwise
        """
        with tracer.start_as_current_span(
            "db_get_latest_workflow_version", attributes={"workflow_id": str(workflow_id), "published": published}
        ) as span:
            stmt = (
                select(WorkflowVersion)
                .where(WorkflowVersion.workflow_id == workflow_id)
                .order_by(desc(WorkflowVersion.created_at))
                .limit(1)
                .options(selectinload(WorkflowVersion.workflow_modes))
            )
            if published:
                stmt = stmt.where(
                    or_(
                        *[
                            WorkflowVersion.status == status
                            for status in [
                                WorkflowVersion.WorkflowVersionStatus.PUBLISHED,
                                WorkflowVersion.WorkflowVersionStatus.DEPRECATED,
                            ]
                        ]
                    )
                )
            span.set_attribute("sql_query", str(stmt))
            return await self.db.scalar(stmt)

    async def list_workflow_versions(
        self,
        workflow_id: UUID,
        version_status: list[WorkflowVersion.WorkflowVersionStatus] | None = None,
    ) -> Sequence[WorkflowVersion]:
        """
        List all versions of a workflow.

        Parameters
        ----------
        workflow_id : uuid.UUID
            ID of a workflow.
        version_status : list[clowm.models.WorkflowVersion.Status] | None, default None
            Filter versions based on the status

        Returns
        -------
        user : list[clowm.models.WorkflowVersion]
            All workflow version of the given workflow
        """
        with tracer.start_as_current_span(
            "db_list_workflow_versions", attributes={"workflow_id": str(workflow_id)}
        ) as span:
            stmt = (
                select(WorkflowVersion)
                .options(selectinload(WorkflowVersion.workflow_modes))
                .where(WorkflowVersion.workflow_id == workflow_id)
            )
            if version_status is not None and len(version_status) > 0:
                span.set_attribute("version_status", [stat.name for stat in version_status])
                stmt = stmt.where(or_(*[WorkflowVersion.status == status for status in version_status]))
            stmt = stmt.order_by(WorkflowVersion.created_at)
            span.set_attribute("sql_query", str(stmt))
            return (await self.db.scalars(stmt)).unique().all()

    async def create(
        self,
        git_commit_hash: str,
        version: str,
        workflow_id: UUID,
        nextflow_version: str,
        icon_slug: str | None = None,
        previous_version: str | None = None,
        modes: list[UUID] | None = None,
        parameter_extension: dict | None = None,
        default_container: str | None = None,
        nextflow_config: str | None = None,
    ) -> WorkflowVersion:
        """
        Create a new workflow version.

        Parameters
        ----------
        git_commit_hash : str
            Git commit git_commit_hash of the version.
        version : str
            New version in string format
        nextflow_version : str
            The pinned nextflow version.
        workflow_id : uuid.UUID
            ID of the corresponding workflow
        icon_slug : str | None, default None
            Slug of the icon
        previous_version : str | None, default None
            Git commit git_commit_hash of the previous version
        modes : list[UUID] | None, default None
            List of workflow mode IDs that are linked to the new workflow version
        parameter_extension : dict | None, default None
            The parameter extension dict for the new workflow version
        default_container : str | None, default None
            Default container to use for all Nextflow processes that have to container specified
        nextflow_config : str | None, default None
            Nextflow config that overrides the config in the git repository

        Returns
        -------
        workflow_version : clowm.models.WorkflowVersion
            Newly created workflow version
        """
        with tracer.start_as_current_span(
            "db_create_workflow_version",
            attributes={"git_commit_version": git_commit_hash, "workflow_id": str(workflow_id)},
        ) as span:
            if previous_version is not None:  # pragma: no cover
                span.set_attribute("previous_version", previous_version)
            if parameter_extension is not None:  # pragma: no cover
                span.set_attribute("parameter_extension", json.dumps(parameter_extension))
            if modes is None:
                modes = []
            workflow_version = WorkflowVersion(
                git_commit_hash=git_commit_hash,
                version=version,
                workflow_id=workflow_id,
                icon_slug=icon_slug,
                previous_version_hash=previous_version,
                parameter_extension=parameter_extension,
                nextflow_version=nextflow_version,
                nextflow_config=nextflow_config,
                default_container=default_container,
            )
            self.db.add(workflow_version)
            if len(modes) > 0:
                span.set_attribute("mode_ids", [str(m) for m in modes])
                with tracer.start_as_current_span(
                    "db_create_workflow_version_connect_modes",
                    attributes={"workflow_version_id": git_commit_hash, "mode_ids": [str(m) for m in modes]},
                ):
                    await self.db.flush()
                    await self.db.execute(
                        insert(workflow_mode_association_table),
                        [
                            {"workflow_version_commit_hash": git_commit_hash, "workflow_mode_id": mode_id}
                            for mode_id in modes
                        ],
                    )
            await self.db.flush()
            return workflow_version

    async def update_status(self, workflow_version_id: str, status: WorkflowVersion.WorkflowVersionStatus) -> None:
        """
        Update the status of a workflow version.

        Parameters
        ----------
        workflow_version_id : str
            Git commit git_commit_hash of the version.
        status : clowm.models.WorkflowVersion.WorkflowVersionStatus
            New status of the workflow version
        """
        stmt = (
            update(WorkflowVersion)
            .where(WorkflowVersion.git_commit_hash == workflow_version_id)
            .values(status=status.name)
        )
        with tracer.start_as_current_span(
            "db_update_workflow_version_status",
            attributes={"status": status.name, "workflow_version_id": workflow_version_id, "sql_query": str(stmt)},
        ):
            await self.db.execute(stmt)
            await self.db.flush()

    async def update_meta_data(
        self,
        workflow_version_id: str,
        nextflow_version: str,
        nextflow_config: str | None = None,
        default_container: str | None = None,
    ) -> None:
        """
        Update the meta-data of a workflow version

        Parameters
        ----------
        workflow_version_id : str
            Git commit git_commit_hash of the version.
        nextflow_version : str
            Nextflow version of this workflow version
        nextflow_config : str | None, default None
            Overridden Nextflow config of this workflow version
        default_container: str | None, default None
            Overridden default container for workflow execution
        """
        stmt = (
            update(WorkflowVersion)
            .where(WorkflowVersion.git_commit_hash == workflow_version_id)
            .values(
                nextflow_config=nextflow_config, default_container=default_container, nextflow_version=nextflow_version
            )
        )
        with tracer.start_as_current_span(
            "db_update_workflow_version_meta_data",
            attributes={
                "nextflow_version": nextflow_version,
                "workflow_version_id": workflow_version_id,
                "sql_query": str(stmt),
            },
        ) as span:
            if nextflow_config is not None:
                span.set_attribute("nextflow_config", nextflow_config)
            if default_container is not None:
                span.set_attribute("default_container", default_container)
            await self.db.execute(stmt)
            await self.db.flush()

    async def update_parameter_extension(
        self, workflow_version_id: str, parameter_extension: ParameterExtension | None
    ) -> None:
        """
        Update the parameter extension of a workflow version.

        Parameters
        ----------
        workflow_version_id : str
            Git commit git_commit_hash of the version.
        parameter_extension : clowm.schemas.workflow_version.ParameterExtension | None
            New parameter extension of the workflow version
        """
        stmt = (
            update(WorkflowVersion)
            .where(WorkflowVersion.git_commit_hash == workflow_version_id)
            .values(parameter_extension=None if parameter_extension is None else parameter_extension.model_dump())
        )
        with tracer.start_as_current_span(
            "db_update_workflow_version_parameter_extension",
            attributes={
                "parameters": "" if parameter_extension is None else parameter_extension.model_dump_json(),
                "workflow_version_id": workflow_version_id,
                "sql_query": str(stmt),
            },
        ):
            await self.db.execute(stmt)
            await self.db.flush()

    async def update_icon(self, workflow_version_id: str, icon_slug: str | None = None) -> None:
        """
        Update the icon slug for a workflow version.

        Parameters
        ----------
        workflow_version_id : str
            Git commit git_commit_hash of the version.
        icon_slug : str | None, default None
            The new icon slug
        """
        stmt = (
            update(WorkflowVersion)
            .where(WorkflowVersion.git_commit_hash == workflow_version_id)
            .values(icon_slug=icon_slug)
        )
        with tracer.start_as_current_span(
            "db_update_workflow_version_icon",
            attributes={
                "workflow_version_id": workflow_version_id,
                "icon_slug": icon_slug if icon_slug else "None",
                "sql_query": str(stmt),
            },
        ):
            await self.db.execute(stmt)
            await self.db.flush()

    async def icon_exists(self, icon_slug: str) -> bool:
        """
        Check if there is a workflow version that depends on a icon.

        Parameters
        ----------
        icon_slug : str
            The icon slug to search for

        Returns
        -------
        exists : bool
            Flag if a version exists that depends on the icon
        """
        stmt = select(WorkflowVersion).where(WorkflowVersion.icon_slug == icon_slug).limit(1)
        with tracer.start_as_current_span(
            "db_check_workflow_version_icon_exists", attributes={"icon_slug": icon_slug, "sql_query": str(stmt)}
        ):
            version_with_icon = await self.db.scalar(stmt)
            return version_with_icon is not None
