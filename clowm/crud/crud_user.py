import secrets
import time
from collections.abc import Sequence
from uuid import UUID

from opentelemetry import trace
from sqlalchemy import delete, insert, or_, select, update
from sqlalchemy.orm import selectinload

from clowm.core.oidc import OIDCProvider, UserInfo
from clowm.crud.crud_base import CRUDBase
from clowm.models import Role, User, UserRoleMapping
from clowm.models.role import RoleIdMapping
from clowm.otlp import start_as_current_span_async

tracer = trace.get_tracer_provider().get_tracer(__name__)


class CRUDUser(CRUDBase):
    @start_as_current_span_async("db_create_user", tracer=tracer)
    async def create(self, user: UserInfo, roles: list[Role.RoleEnum] | None = None) -> User:
        """
        Create a new user in the database.

        Parameters
        ----------
        roles : list[clowm.models.Role.RoleEnum] | None, default None
            The initial roles a user should have
        user : clowm.core.oidc.UserInfo
            The user to create.

        Returns
        -------
        user : clowm.models.User
            The newly created user.

        Notes
        -----
        If the sub attribute of user is an empty string, then it will not be set according to the AAI provider attribute.
        """
        current_span = trace.get_current_span()
        db_user = User(display_name=user.name, email=user.email)
        if len(user.sub) > 0:
            setattr(db_user, db_user.provider_id_column(user.provider).key, user.sub)
            current_span.set_attribute("sub", user.sub)
        self.db.add(db_user)
        await self.db.flush()
        current_span.set_attribute("uid", str(db_user.uid))
        if roles is not None and len(roles) > 0:
            current_span.set_attribute("roles", [role.name for role in roles])
            mapping = RoleIdMapping()
            insert_params = [{"uid": db_user.uid, "role_id": mapping[role]} for role in roles]
            await self.db.execute(insert(UserRoleMapping), insert_params)
            await self.db.flush()
        await self.db.refresh(db_user, attribute_names=["roles"])
        return db_user

    async def create_invitation_token(self, uid: UUID) -> str:
        """
        Create an invitation token and save it for the user.

        Parameters
        ----------
        uid : uuid.UUID
            UID of a user.

        Returns
        -------
        token : str
            The saved token for the user
        """
        token = secrets.token_urlsafe(32)
        stmt = (
            update(User)
            .where(User.uid == uid)
            .values(invitation_token=token, invitation_token_created_at=round(time.time()))
        )
        with tracer.start_as_current_span(
            "db_create_invitation_token", attributes={"sql_query": str(stmt), "uid": str(uid)}
        ):
            await self.db.execute(stmt)
            await self.db.flush()
        return token

    async def get_by_invitation_token(self, invitation_token: str) -> User | None:
        """
        Get a user by an invitation token.

        Parameters
        ----------
        invitation_token : str
            The token to search for.

        Returns
        -------
        user : clowm.models.User | None
            The user belonging to the invitation token.
        """
        stmt = select(User).where(User.invitation_token == invitation_token)
        with tracer.start_as_current_span("db_get_by_invitation_token", attributes={"sql_query": str(stmt)}):
            return await self.db.scalar(stmt)

    async def update_roles(self, user: User, roles: list[Role.RoleEnum]) -> None:
        """
        Update the roles of a user.

        Parameters
        ----------
        user : clowm.models.User
            The user whom roles should get updated.
        roles : list[clowm.models.Role.RoleEnum]
            The new roles of the user.
        """
        mapping = RoleIdMapping()
        add_role_ids = [mapping[role] for role in roles if not user.has_role(role)]
        delete_role_ids = [mapping[role] for role in Role.RoleEnum if user.has_role(role) and role not in roles]
        if len(add_role_ids) > 0:
            insert_params = [{"uid": user.uid, "role_id": role_id} for role_id in add_role_ids]
            await self.db.execute(insert(UserRoleMapping), insert_params)
        if len(delete_role_ids) > 0:
            stmt = delete(UserRoleMapping).where(
                UserRoleMapping.uid == user.uid,
                or_(*[UserRoleMapping.role_id == role_id for role_id in delete_role_ids]),
            )
            await self.db.execute(stmt)
        if len(delete_role_ids) > 0 or len(add_role_ids) > 0:
            await self.db.flush()
            await self.db.refresh(user, attribute_names=["roles"])

    async def update_email(self, uid: UUID, email: str) -> None:
        """
        Update the smtp of a user.

        Parameters
        ----------
        uid : uuid.UUID
            UID of a user.
        email : str | None, default None
            The new smtp of the user
        """
        stmt = update(User).where(User.uid == uid).values(email=email)
        with tracer.start_as_current_span(
            "db_update_user_email", attributes={"sql_query": str(stmt), "uid": str(uid), "smtp": str(email)}
        ):
            await self.db.execute(stmt)
            await self.db.flush()

    async def update_invited_user(self, uid: UUID, user: UserInfo) -> None:
        """
        Update the information of an invited user.

        Parameters
        ----------
        uid : uuid.UUID
            UID of a user.
        user : clowm.core.oidc.UserInfo
            The display name of the user.
        """
        stmt = (
            update(User)
            .where(User.uid == uid)
            .values(
                **{
                    "display_name": user.name,
                    "invitation_token": None,
                    "invitation_token_created_at": None,
                    User.provider_id_column(user.provider).key: user.sub,
                }
            )
        )
        if user.email is not None:
            stmt = stmt.values(email=user.email)
        with tracer.start_as_current_span(
            "db_update_registered_user",
            attributes={
                "sql_query": str(stmt),
                "uid": str(uid),
                "sub": user.sub,
            },
        ):
            await self.db.execute(stmt)
            await self.db.flush()

    async def update_last_login(self, uid: UUID, timestamp: int) -> None:
        """
        Update the last_login column of a user.

        Parameters
        ----------
        uid : uuid.UUID
            UID of a user.
        timestamp : int
            UNIX timestamp of last login
        """
        stmt = update(User).where(User.uid == uid).values(last_login=timestamp)
        with tracer.start_as_current_span(
            "db_update_last_login", attributes={"uid": str(uid), "timestamp": timestamp, "sql_query": str(stmt)}
        ):
            await self.db.execute(stmt)
            await self.db.flush()

    async def mark_initialized(self, uid: UUID) -> None:
        """
        Mark a user as initialized.

        Parameters
        ----------
        uid : uuid.UUID
            UID of a user.
        """
        stmt = update(User).where(User.uid == uid).values(initialized=True)
        with tracer.start_as_current_span(
            "db_mark_user_initialized", attributes={"sql_query": str(stmt), "uid": str(uid)}
        ):
            await self.db.execute(stmt)
            await self.db.flush()

    async def get(self, uid: UUID | None) -> User | None:
        """
        Get a user by its UID.

        Parameters
        ----------
        uid : uuid.UUID
            UID of a user.

        Returns
        -------
        user : clowm.models.User | None
            The user for the given UID if he exists, None otherwise
        """
        if uid is None:  # pragma: no cover
            return None
        stmt = select(User).where(User.uid == uid).options(selectinload(User.roles))
        with tracer.start_as_current_span("db_get_user", attributes={"sql_query": str(stmt), "uid": str(uid)}):
            return await self.db.scalar(stmt)

    async def get_by_sub(self, sub: str, provider: OIDCProvider) -> User | None:
        """
        Get a user by its lifescience_id.

        Parameters
        ----------
        sub : str
            Unique ID of a user by the AAI
        provider : clow.core.oidc.OIDCProvider
            AAI provider

        Returns
        -------
        user : clowm.models.User | None
            The user for the given sub if he exists, None otherwise
        """
        stmt = select(User).options(selectinload(User.roles)).where(User.provider_id_column(provider) == sub)
        with tracer.start_as_current_span(
            "db_get_user_by_sub", attributes={"sql_query": str(stmt), "sub": sub, "provider": str(provider)}
        ):
            return await self.db.scalar(stmt)

    async def list_users(
        self, name_substring: str | None = None, roles: list[Role.RoleEnum] | None = None
    ) -> Sequence[User]:
        """
        List and filter all users in the system.

        Parameters
        ----------
        name_substring : str | None, default None
            Substring to search for in the name of a user.
        roles : list[clowm.models.Role.RoleEnum] | None, default None
            The roles a user should have. Concatenated with an OR expression.

        Returns
        -------
        users : list[clowm.models.User]
            List of users.
        """
        stmt = select(User).options(selectinload(User.roles))
        with tracer.start_as_current_span("db_list_users") as span:
            if name_substring is not None:  # pragma: no cover
                span.set_attribute("name_substring", name_substring)
                stmt = stmt.where(User.display_name.contains(name_substring))
            if roles is not None and len(roles) > 0:
                span.set_attribute("roles", [role.name for role in roles])
                mapping = RoleIdMapping()
                stmt = stmt.join(UserRoleMapping).where(
                    or_(*[UserRoleMapping.role_id == mapping[role] for role in roles])
                )
            span.set_attribute("sql_query", str(stmt))
            return (await self.db.scalars(stmt)).unique().all()

    async def delete(self, uid: UUID) -> None:
        """
        Delete a user by its UID.

        Parameters
        ----------
        uid : uuid.UUID
            ID of user to delete
        """
        stmt = delete(User).where(User.uid == uid)
        with tracer.start_as_current_span("db_list_users", attributes={"uid": str(uid)}):
            await self.db.execute(stmt)
            await self.db.flush()
