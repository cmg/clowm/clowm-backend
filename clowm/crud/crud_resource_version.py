import time
from typing import Any
from uuid import UUID

from opentelemetry import trace
from sqlalchemy import select, update

from clowm.crud.crud_base import CRUDBase
from clowm.models import ResourceVersion

tracer = trace.get_tracer_provider().get_tracer(__name__)


class CRUDResourceVersion(CRUDBase):
    async def get(self, resource_version_id: UUID, resource_id: UUID | None = None) -> ResourceVersion | None:
        """
        Get a resource version by its ID.

        Parameters
        ----------
        resource_version_id : uuid.UUID
            ID of a resource version.
        resource_id : uuid.UUID | None
            ID of a resource

        Returns
        -------
        resource_version : clowm.models.ResourceVersion | None
            The resource version with the given ID if it exists, None otherwise
        """
        stmt = select(ResourceVersion).where(ResourceVersion.resource_version_id == resource_version_id)
        with tracer.start_as_current_span(
            "db_get_resource_version",
            attributes={
                "resource_version_id": str(resource_version_id),
            },
        ) as span:
            if resource_id:
                span.set_attribute("resource_id", str(resource_id))
                stmt = stmt.where(ResourceVersion.resource_id == resource_id)
            span.set_attribute("sql_query", str(stmt))
            return await self.db.scalar(stmt)

    async def get_latest(self, resource_id: UUID) -> ResourceVersion | None:
        """
        Get a resource version based on some conditions.

        Parameters
        ----------
        resource_id : uuid.UUID
            ID of a resource.

        Returns
        -------
        resource_version : clowm.models.ResourceVersion | None
            The resource version with the given conditions if it exists, None otherwise
        """

        stmt = (
            select(ResourceVersion)
            .where(ResourceVersion.resource_id == resource_id)
            .where(ResourceVersion.status == ResourceVersion.ResourceVersionStatus.LATEST)
        )
        with tracer.start_as_current_span(
            "db_get_latest_resource_version", attributes={"resource_id": str(resource_id), "sql_query": str(stmt)}
        ):
            return await self.db.scalar(stmt)

    async def update_used_resource_version(self, resource_version_id: UUID) -> None:
        """
        Update the last used timestamp and the number of times the resource version is used.

        Parameters
        ----------
        resource_version_id : uuid.UUID
            ID of a resource version.
        """

        stmt = (
            update(ResourceVersion)
            .where(ResourceVersion.resource_version_id == resource_version_id)
            .values(times_used=ResourceVersion.times_used + 1, last_used_timestamp=round(time.time()))
        )
        with tracer.start_as_current_span(
            "db_update_used_resource_version",
            attributes={"resource_version_id": str(resource_version_id), "sql_query": str(stmt)},
        ):
            await self.db.execute(stmt)
            await self.db.flush()

    async def update_status(
        self,
        status: ResourceVersion.ResourceVersionStatus,
        resource_version_id: UUID,
        resource_id: UUID | None = None,
        slurm_job_id: int | None = None,
        values: dict[str, Any] | None = None,
    ) -> None:
        """
        Update the status of a resource version.

        Parameters
        ----------
        resource_version_id : uuid.UUID
            ID of a resource version.
        resource_id : uuid.UUID | None, default None
            ID of a resource. Must be set if `status` is LATEST.
        status : clowm.models.ResourceVersion.ResourceVersionStatus
            New status of the resource version
        slurm_job_id : int | None, default None
            Slurm job id if the update comes from a executed slurm job
        values : dict[str, Any] | None, defualt None
            Allow to set arbitrary values for the update statement
        """
        if status == ResourceVersion.ResourceVersionStatus.LATEST:
            if resource_id is None:
                raise ValueError("Parameter 'resource_version_id' or 'resource_id' must not be None")
            stmt1 = (
                update(ResourceVersion)
                .values(
                    status=ResourceVersion.ResourceVersionStatus.SYNCHRONIZED.name,
                    remove_latest_timestamp=round(time.time()),
                )
                .where(ResourceVersion.resource_id == resource_id)
            )
            with tracer.start_as_current_span(
                "db_remove_latest_tag",
                attributes={"status": status.name, "resource_id": str(resource_id), "sql_query": str(stmt1)},
            ):
                await self.db.execute(stmt1)

        stmt2 = (
            update(ResourceVersion)
            .values(status=status.name)
            .where(ResourceVersion.resource_version_id == resource_version_id)
        )
        if values:
            stmt2 = stmt2.values(**values)
        if status == ResourceVersion.ResourceVersionStatus.SYNCHRONIZED:
            stmt2 = stmt2.values(sync_finished_timestamp=round(time.time()))
        elif status == ResourceVersion.ResourceVersionStatus.LATEST:
            stmt2 = stmt2.values(set_latest_timestamp=round(time.time()))
        elif status == ResourceVersion.ResourceVersionStatus.S3_DELETED:
            stmt2 = stmt2.values(s3_deleted_timestamp=round(time.time()))

        if slurm_job_id is not None:
            if status == ResourceVersion.ResourceVersionStatus.SYNCHRONIZING:
                stmt2 = stmt2.values(sync_slurm_job_id=slurm_job_id)
            elif status == ResourceVersion.ResourceVersionStatus.CLUSTER_DELETING:
                stmt2 = stmt2.values(cluster_delete_slurm_job_id=slurm_job_id)
            elif status == ResourceVersion.ResourceVersionStatus.SETTING_LATEST:
                stmt2 = stmt2.values(set_latest_slurm_job_id=slurm_job_id)
        with tracer.start_as_current_span(
            "db_update_resource_version_status",
            attributes={
                "status": status.name,
                "resource_version_id": str(resource_version_id),
                "sql_query": str(stmt2),
            },
        ):
            await self.db.execute(stmt2)
            await self.db.flush()

    async def create(self, resource_id: UUID, release: str) -> ResourceVersion:
        """
        Create a new resource version.

        Parameters
        ----------
        resource_id : uuid.UUID
            ID of the resource.
        release : str
            Versions of the resource.

        Returns
        -------
        resource_version : clowm.models.ResourceVersion
            The newly created resource version
        """
        with tracer.start_as_current_span(
            "db_create_resource_version", attributes={"resource_id": str(resource_id), "release": release}
        ) as span:
            resource_version_db = ResourceVersion(release=release, resource_id=resource_id)
            self.db.add(resource_version_db)
            await self.db.flush()
            span.set_attribute("resource_version_id", str(resource_version_db.resource_version_id))
            return resource_version_db
