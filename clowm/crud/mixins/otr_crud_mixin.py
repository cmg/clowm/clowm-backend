import time
from collections.abc import Sequence
from uuid import UUID

from opentelemetry import trace  # type: ignore[attr-defined]
from sqlalchemy import or_, select, update
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.db.mixins import OwnershipTransferRequestMixin

tracer = trace.get_tracer_provider().get_tracer(__name__)


class OTRCrudMixin:
    model: type[OwnershipTransferRequestMixin]
    db: AsyncSession

    async def list_otrs(
        self,
        filter_combination_union: bool = True,
        current_owner_id: UUID | None = None,
        new_owner_id: UUID | None = None,
    ) -> Sequence[OwnershipTransferRequestMixin]:
        """
        Get all ownership transfer requests from the database.

        Parameters
        ----------
        filter_combination_union: bool, default True,
            Flag if the filters should be combined with a union (AND) or intersection (OR)
        new_owner_id : uuid.UUID | None, default None
            Filter OTRs by the new owner id
        current_owner_id : uuid.UUID | None, default None
            Filter OTRs by the current owner id

        Returns
        -------
        targets : list[clowm.db.mixins.ownership_transfer_request.OwnershipTransferRequestMixin]
            List of all targets that have a pending OTR.
        """
        stmt = select(self.model).where(self.model.transfer_new_owner_uid != None)  # noqa:E711
        with tracer.start_as_current_span(f"list_{self.model.__name__.lower()}_otrs") as span:
            if filter_combination_union and current_owner_id is not None and new_owner_id is not None:
                span.set_attributes({"current_owner_id": str(current_owner_id), "new_owner_id": str(new_owner_id)})
                stmt = stmt.where(
                    or_(
                        self.model.clowm_owner_id == current_owner_id,
                        self.model.transfer_new_owner_uid == new_owner_id,
                    )
                )
            else:
                if current_owner_id is not None:
                    span.set_attribute("current_owner_id", str(current_owner_id))
                    stmt = stmt.where(self.model.clowm_owner_id == current_owner_id)
                if new_owner_id is not None:
                    span.set_attribute("new_owner_id", str(new_owner_id))
                    stmt = stmt.where(self.model.transfer_new_owner_uid == new_owner_id)
            span.set_attribute("sql_query", str(stmt))
            return (await self.db.scalars(stmt)).all()

    async def create_otr(self, target_id: str | UUID, new_owner_uid: UUID, comment: str | None = None) -> None:
        """
        Create an OTR for a target overriding an existing one.

        Parameters
        ----------
        target_id : str | uuid.UUID
            Id of the target for the OTR
        new_owner_uid : uuid.UUID
            UID of the new owner
        comment : str | None, default None
            Optional comment for the OTR
        """
        stmt = (
            update(self.model)
            .where(self.model.clowm_id == target_id)
            .values(
                transfer_new_owner_uid=new_owner_uid,
                transfer_comment=comment if comment is not None else "",
                transfer_created_at=round(time.time()),
            )
        )
        with tracer.start_as_current_span(
            f"create_{self.model.__name__.lower()}_otr",
            attributes={
                "target_id": str(target_id),
                "comment": comment if comment is not None else "",
                "new_owner_uid": str(new_owner_uid),
                "sql_query": str(stmt),
            },
        ):
            await self.db.execute(stmt)
            await self.db.flush()

    async def update_otr(self, accept: bool, target_id: str | UUID) -> None:
        """
        Update the OTR of a target.

        Parameters
        ----------
        accept : bool
            Flag if the OTR was accepted
        target_id : str | uuid.UUID
            Id of the target for the OTR
        """
        base_stmt = update(self.model).where(self.model.clowm_id == target_id)
        with tracer.start_as_current_span(
            f"create_{self.model.__name__.lower()}_otr",
            attributes={"accept": accept},
        ) as span:
            stmts = []
            if accept:
                stmts.append(
                    base_stmt.values(
                        clowm_owner_id=self.model.transfer_new_owner_uid,
                        last_transfer_timestamp=round(time.time()),
                    )
                )
            stmts.append(base_stmt.values(transfer_new_owner_uid=None, transfer_comment=None, transfer_created_at=None))
            for stmt in stmts:
                await self.db.execute(stmt)
            span.set_attribute("sql_query", [str(stmt) for stmt in stmts])
            await self.db.flush()
