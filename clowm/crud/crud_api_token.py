import time
from collections.abc import Sequence
from datetime import datetime
from uuid import UUID

from opentelemetry import trace
from sqlalchemy import delete, select, update
from sqlalchemy.exc import StatementError
from sqlalchemy.orm import joinedload
from sqlalchemy.sql import or_

from clowm.crud.crud_base import CRUDBase
from clowm.models import ApiToken, User
from clowm.schemas.api_token import ApiTokenIn

tracer = trace.get_tracer_provider().get_tracer(__name__)


class CRUDApiToken(CRUDBase):
    async def search(
        self,
        token: str,
    ) -> ApiToken | None:
        """
        Search an Api token in the database.

        Parameters
        ----------
        token : str
            Token to search for.

        Returns
        -------
        db_token : clowm.models.ApiToken
            Api token if it exists, None otherwise
        """
        stmt = (
            select(ApiToken).options(joinedload(ApiToken.user).selectinload(User.roles)).where(ApiToken.token == token)
        )
        with tracer.start_as_current_span("db_search_api_token", attributes={"sql_query": str(stmt)}):
            try:
                return await self.db.scalar(stmt)
            except StatementError:
                return None

    async def list(
        self,
        uid: UUID | None = None,
        expired: bool | None = None,
    ) -> Sequence[ApiToken]:
        """
        List all token in the database.

        Parameters
        ----------
        uid : uuid.UUID
            Filter token by uid
        expired : bool | None, default None
            Filter tokens by their expired state.

        Returns
        -------
        tokens : list[clowm.models.ApiToken]
            Api tokens that match the filter
        """
        stmt = select(ApiToken)
        with tracer.start_as_current_span("db_list_api_token") as span:
            if uid is not None:
                span.set_attribute("uid", str(uid))
                stmt = stmt.where(ApiToken.uid == uid)
            if expired is not None:
                span.set_attribute("expired", expired)
                current_time = time.time()
                if expired:
                    stmt = stmt.where(ApiToken.expires_at < current_time)
                else:
                    stmt = stmt.where(or_(ApiToken.expires_at > current_time, ApiToken.expires_at == None))  # noqa:E711
            span.set_attribute("sql_query", str(stmt))
            return (await self.db.scalars(stmt)).all()

    async def get(
        self,
        tid: UUID,
    ) -> ApiToken | None:
        """
        Get an Api token by its ID.

        Parameters
        ----------
        tid : uuid. UUID
            Token id to fetch.

        Returns
        -------
        token : clowm.models.ApiToken | None
            The Api token with the given id if it exists, otherwise None
        """
        stmt = select(ApiToken).where(ApiToken.token_id == tid)
        with tracer.start_as_current_span(
            "db_get_api_token", attributes={"sql_query": str(stmt), "token_id": str(tid)}
        ):
            return await self.db.scalar(stmt)

    async def create(self, token_in: ApiTokenIn, uid: UUID) -> ApiToken:
        """
        Create a new Api token.

        Parameters
        ----------
        token_in : clowm.schemas.api_token.ApiTokenIn
            Meta-information about the new token.
        uid : uuid.UUID
            Id of the user who owns the api token.

        Returns
        -------
        token : clowm.models.ApiToken
            The newly created Api token.
        """
        token = ApiToken(scopes=token_in.scope_db, expires_at=token_in.expires_at, uid=uid, name=token_in.name)
        with tracer.start_as_current_span(
            "db_create_api_token",
            attributes={"uid": str(uid), "scopes": token_in.scopes},
        ) as span:
            if token_in.expires_at is not None:  # pragma: no cover
                span.set_attribute("expires_at", datetime.fromtimestamp(token_in.expires_at).isoformat())
            self.db.add(token)
            await self.db.flush()
            span.set_attribute("token_id", str(token.token_id))
            return token

    async def delete(self, tid: UUID) -> None:
        """
        Delete an Api token by its id.

        Parameters
        ----------
        tid : uuid.UUID
            Id of the token to delete.
        """
        stmt = delete(ApiToken).where(ApiToken.token_id == tid)
        with tracer.start_as_current_span(
            "db_delete_api_token", attributes={"token_id": str(tid), "sql_query": str(stmt)}
        ):
            await self.db.execute(stmt)
            await self.db.flush()

    async def update_last_used(self, tid: UUID) -> None:
        """
        Update the last used timestamp of an Api token

        Parameters
        ----------
        tid : uuid.UUID
            Id of the token to update.
        """
        stmt = update(ApiToken).where(ApiToken.token_id == tid).values(last_used=round(time.time()))
        with tracer.start_as_current_span(
            "db_update_api_token_last_used", attributes={"token_id": str(tid), "sql_query": str(stmt)}
        ):
            await self.db.execute(stmt)
            await self.db.flush()
