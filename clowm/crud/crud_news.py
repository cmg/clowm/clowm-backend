import time
from collections.abc import Sequence
from uuid import UUID

from opentelemetry import trace
from sqlalchemy import delete, or_, select

from clowm.crud.crud_base import CRUDBase
from clowm.db.types import OrderType
from clowm.models import News
from clowm.schemas.news import NewsIn

tracer = trace.get_tracer_provider().get_tracer(__name__)


class CRUDNews(CRUDBase):
    async def create(self, news_in: NewsIn, creator_id: UUID | None = None) -> News:
        """
        Create a news event in the database.

        Parameters
        ----------
        news_in : clowm.schemas.news.NewsIn
            Infos for the created news events.
        creator_id : uuid.UUID | None, default None
            Id of the user who created the news.

        Returns
        -------
        news : clowm.models.News
            The created news event
        """
        news = News(
            content=news_in.content,
            title=news_in.title,
            important_till=news_in.important_till,
            creator_id=creator_id,
            category=news_in.category,
        )
        with tracer.start_as_current_span("db_create_news", attributes={"news_in": news_in.model_dump_json()}) as span:
            if creator_id is not None:
                span.set_attribute("creator_id", str(creator_id))
            self.db.add(news)
            await self.db.flush()
            span.set_attribute("news_id", str(news.news_id))
            return news

    async def list(
        self,
        limit: int | None = None,
        sort: OrderType = "desc",
        id_after: UUID | None = None,
        created_after: int | None = None,
        creator_id: UUID | None = None,
    ) -> Sequence[News]:
        """
        List all news and apply filter.

        Parameters
        ----------
        limit : int | None, default None
            Limit the number of retrieved news events
        sort : "asc" | "desc", default "desc"
            Sort the rows ascending or descending.
        id_after : uuid.UUID | None, default None
            Filter news events that come after this execution id (including).
        created_after : int | None, default None
            Filter news that are created after this UNIX timestamp
        creator_id : uuid.UUID | None, default None
            Filter for the user who created the news

        Returns
        -------
        news : list[clowm.models.News]
            List of filtered news.
        """
        stmt = select(News).limit(limit).order_by(News.news_id.desc() if sort == "desc" else News.news_id.asc())
        with tracer.start_as_current_span("db_list_news", attributes={"sort": sort}) as span:
            if limit is not None:  # pragma: no cover
                span.set_attribute("limit", limit)
            if creator_id is not None:
                span.set_attribute("creator_id", str(creator_id))
                stmt = stmt.where(News.creator_id == creator_id)
            if created_after is not None:
                span.set_attribute("created_after", created_after)
                stmt = stmt.where(News.created_at > created_after)
            if id_after is not None:
                span.set_attribute("id_after", str(id_after))
                stmt = stmt.where(News.news_id <= id_after if sort == "desc" else News.news_id >= id_after)
            span.set_attribute("sql_query", str(stmt))
            return (await self.db.scalars(stmt)).all()

    async def get_latest(self, created_after: int) -> Sequence[News]:
        """
        List all news event created after a timestamp or are still important.

        Parameters
        ----------
        created_after : int
            Timestamp after that the news event has to be created

        Returns
        -------
        news : list[clowm.models.News]
            List of current or important news
        """
        stmt = select(News).where(or_(News.created_at >= created_after, News.important_till > round(time.time())))

        with tracer.start_as_current_span(
            "db.get_latest_news", attributes={"created_after": created_after, "sql_query": str(stmt)}
        ):
            return (await self.db.scalars(stmt)).all()

    async def get(self, news_id: UUID) -> News | None:
        """
        Get a news event by its id from the database.

        Parameters
        ----------
        news_id : uuid.UUID
            ID of a news event

        Returns
        -------
        news : clowm.models.News
            The news event with the given id if it exists, None otherwise
        """
        stmt = select(News).where(News.news_id == news_id)
        with tracer.start_as_current_span("db_get_news", attributes={"news_id": str(news_id), "sql_query": str(stmt)}):
            return await self.db.scalar(stmt)

    async def delete(self, news_id: UUID) -> None:
        """
        Delete a news event in the database.

        Parameters
        ----------
        news_id : uuid.UUID
            ID of the news event
        """
        stmt = delete(News).where(News.news_id == news_id)
        with tracer.start_as_current_span(
            "db_delete_news", attributes={"news_id": str(news_id), "sql_query": str(stmt)}
        ):
            await self.db.execute(stmt)
            await self.db.flush()
