import time
from collections.abc import Sequence
from enum import StrEnum, unique
from uuid import UUID

from opentelemetry import trace
from pydantic import ByteSize
from sqlalchemy import delete, or_, select, update
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.models import Bucket
from clowm.models import BucketPermission as BucketPermissionDB
from clowm.schemas.bucket import BucketIn as BucketInSchema

from .crud_base import CRUDBase
from .crud_error import DuplicateError
from .mixins import OTRCrudMixin

tracer = trace.get_tracer_provider().get_tracer(__name__)


class CRUDBucket(CRUDBase, OTRCrudMixin):
    def __init__(self, db: AsyncSession) -> None:
        super().__init__(db)
        self.model = Bucket

    @unique
    class BucketType(StrEnum):
        """
        Enumeration for the type of buckets to fetch from the DB

        OWN: Only fetch buckets that the user owns
        PERMISSION: Only fetch foreign buckets that the user has access to
        ALL: Fetch all buckets that the user has access to
        """

        OWN = "OWN"
        ALL = "ALL"
        PERMISSION = "PERMISSION"

    async def get(self, bucket_name: str) -> Bucket | None:
        """
        Get a bucket by its name.

        Parameters
        ----------
        bucket_name : str
            Name of the Bucket to get from database.

        Returns
        -------
        bucket : Bucket | None
            Returns the bucket if it exists, None otherwise.
        """
        stmt = select(Bucket).where(Bucket.name == bucket_name)
        with tracer.start_as_current_span(
            "db_get_bucket", attributes={"bucket_name": bucket_name, "sql_query": str(stmt)}
        ):
            return await self.db.scalar(stmt)

    async def list(self) -> Sequence[Bucket]:
        """
        Get all buckets from the database.

        Returns
        -------
        buckets : list[Bucket]
            List of all buckets.
        """
        stmt = select(Bucket)
        with tracer.start_as_current_span("db_list_all_buckets", attributes={"sql_query": str(stmt)}):
            return (await self.db.scalars(stmt)).all()

    async def list_for_user(self, uid: UUID, bucket_type: BucketType = BucketType.ALL) -> Sequence[Bucket]:
        """
        Get all buckets for a user. Depending on the `bucket_type`, the user is either owner of the bucket or has
        permission for the bucket

        Parameters
        ----------
        uid : str
            UID of a user.
        bucket_type : BucketType, default BucketType.ALL


        Returns
        -------
        buckets : list[clowm.models.Bucket]
            A list of all buckets where the given user has READ permissions for.

        Notes
        -----
        SQL Query own buckets:
        ```
        SELECT bucket.name, bucket.description, bucket.public, bucket.owner_id
        FROM bucket
        WHERE bucket.owner_id = %s
        ```

        SQL Query all buckets that the user has access to:
        ```
        SELECT bucket.name, bucket.description, bucket.public, bucket.owner_id
        FROM bucket
        WHERE bucket.owner_id = %s OR (EXISTS
            (SELECT 1 FROM bucketpermission
            WHERE bucket.name = bucketpermission.bucket_name AND bucketpermission.user_id = %s
                AND(UNIX_TIMESTAMP() >= bucketpermission.`from` 0 OR bucketpermission.`from` IS NULL)
                AND(UNIX_TIMESTAMP() <= bucketpermission.`to` >= 0 OR bucketpermission.`to` IS NULL)))
        ```

        SQL Query only foreign buckets where user has permission to
        ```
        SELECT bucket.name, bucket.description, bucket.public, bucket.owner_id
        FROM bucket
        WHERE (EXISTS
            (SELECT 1 FROM bucketpermission
            WHERE bucket.name = bucketpermission.bucket_name AND bucketpermission.user_id = %s
                AND(UNIX_TIMESTAMP() >=  bucketpermission.`from` <= 0 OR bucketpermission.`from` IS NULL)
                AND(UNIX_TIMESTAMP() <=  bucketpermission.`to` >= 0 OR bucketpermission.`to` IS NULL)))
        ```
        """
        stmt = select(Bucket)
        current_time = time.time()
        if bucket_type == CRUDBucket.BucketType.ALL:
            stmt = stmt.where(
                or_(
                    Bucket.owner_id == uid,
                    Bucket.permissions.any(BucketPermissionDB.uid == uid)
                    .where(
                        or_(
                            current_time >= BucketPermissionDB.from_,
                            BucketPermissionDB.from_ == None,  # noqa:E711
                        )
                    )
                    .where(
                        or_(
                            current_time <= BucketPermissionDB.to,
                            BucketPermissionDB.to == None,  # noqa:E711
                        )
                    ),
                )
            )
        elif bucket_type == CRUDBucket.BucketType.OWN:
            stmt = stmt.where(Bucket.owner_id == uid)
        else:
            stmt = stmt.where(
                Bucket.permissions.any(BucketPermissionDB.uid == uid)
                .where(
                    or_(
                        current_time >= BucketPermissionDB.from_,
                        BucketPermissionDB.from_ == None,  # noqa:E711
                    )
                )
                .where(
                    or_(
                        current_time <= BucketPermissionDB.to,
                        BucketPermissionDB.to == None,  # noqa:E711
                    )
                ),
            )
        with tracer.start_as_current_span(
            "db_list_buckets_for_user",
            attributes={"sql_query": str(stmt), "uid": str(uid), "bucket_type": bucket_type.name},
        ):
            return (await self.db.scalars(stmt)).all()

    async def create(
        self,
        bucket_in: BucketInSchema,
        owner_id: UUID,
        size_limit: int | None = None,
        object_limit: int | None = None,
    ) -> Bucket:
        """
        Create a bucket for a given user.

        Parameters
        ----------
        bucket_in : clowm.schemas.bucket.BucketIn
            All relevant information for a new bucket.
        owner_id : uuid.UUID
            UID of the owner for the new bucket.
        size_limit : int | None, default None
            Initial size limit for the bucket.
        object_limit : int | None, default None
            Initial object limit for the bucket.

        Returns
        -------
        bucket : clowm.models.Bucket
            Returns the created bucket.
        """
        bucket = Bucket(**bucket_in.model_dump(), owner_id=owner_id, size_limit=size_limit, object_limit=object_limit)
        with tracer.start_as_current_span(
            "db_create_bucket",
            attributes={"uid": str(owner_id), "bucket_name": bucket.name},
        ):
            if await self.get(bucket.name) is not None:
                raise DuplicateError(f"Bucket {bucket.name} exists already")
            self.db.add(bucket)
            await self.db.flush()
            return bucket

    async def update_public_state(self, bucket_name: str, public: bool) -> None:
        """
        Update the public state of a bucket

        Parameters
        ----------
        bucket_name : str
            Name of a bucket.
        public : bool
            New public state of the bucket.
        """
        stmt = update(Bucket).where(Bucket.name == bucket_name).values(public=public)
        with tracer.start_as_current_span(
            "db_update_bucket_public_state",
            attributes={"bucket_name": bucket_name, "public": public, "sql_query": str(stmt)},
        ):
            await self.db.execute(stmt)
            await self.db.flush()

    async def update_bucket_limits(
        self, bucket_name: str, size_limit: int | None = None, object_limit: int | None = None
    ) -> None:
        """
        Update the bucket limits.

        Parameters
        ----------
        bucket_name : str
            Name of a bucket.
        size_limit : int | None, default None
            New size limit for the bucket.
        object_limit : int | None, default None
            New object limit for the bucket.
        """
        stmt = update(Bucket).where(Bucket.name == bucket_name).values(size_limit=size_limit, object_limit=object_limit)
        with tracer.start_as_current_span(
            "db_update_bucket_limits", attributes={"bucket_name": bucket_name, "sql_query": str(stmt)}
        ) as span:
            if size_limit is not None:  # pragma: no cover
                span.set_attribute("size_limit", ByteSize(size_limit * 1024).human_readable())
            if object_limit is not None:  # pragma: no cover
                span.set_attribute("object_limit", object_limit)
            await self.db.execute(stmt)
            await self.db.flush()

    async def delete(self, bucket_name: str) -> None:
        """
        Delete a specific bucket.

        Parameters
        ----------
        bucket_name : string
            The name of the bucket to delete.
        """
        stmt = delete(Bucket).where(Bucket.name == bucket_name)
        with tracer.start_as_current_span(
            "db_delete_bucket",
            attributes={"sql_query": str(stmt), "bucket_name": bucket_name},
        ):
            await self.db.execute(stmt)
            await self.db.flush()
