from sqlalchemy.ext.asyncio import AsyncSession


class CRUDBase:
    def __init__(self, db: AsyncSession) -> None:
        self.db = db
