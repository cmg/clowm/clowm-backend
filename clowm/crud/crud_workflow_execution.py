from collections.abc import Sequence
from datetime import datetime, timedelta
from math import ceil
from uuid import UUID

from opentelemetry import trace
from sqlalchemy import func, or_, select, update
from sqlalchemy.orm import joinedload, selectinload

from clowm.crud.crud_base import CRUDBase
from clowm.db.types import OrderType
from clowm.models import WorkflowExecution, WorkflowVersion
from clowm.schemas.workflow_execution import DevWorkflowExecutionIn, WorkflowExecutionIn

tracer = trace.get_tracer_provider().get_tracer(__name__)


class CRUDWorkflowExecution(CRUDBase):
    async def create(
        self,
        execution: WorkflowExecutionIn | DevWorkflowExecutionIn,
        executor_id: UUID,
        notes: str | None = None,
    ) -> WorkflowExecution:
        """
        Create a workflow execution in the database.

        Parameters
        ----------
        execution : clowm.schemas.workflow_execution.WorkflowExecutionIn | DevWorkflowExecutionIn
            Workflow execution input parameters.
        executor_id : uuid.UUID
            User who started the workflow execution.
        notes : str | None, default None
            Notes to add to the workflow execution. Only usd if 'execution' has type 'DevWorkflowExecutionIn'.

        Returns
        -------
        workflow_execution : clowm.models.WorkflowExecution
            The newly created workflow execution
        """
        with tracer.start_as_current_span(
            "db_create_workflow_execution", attributes={"executor_id": str(executor_id)}
        ) as span:
            if isinstance(execution, WorkflowExecutionIn):
                span.set_attribute("workflow_version_id", execution.workflow_version_id)
                workflow_execution = WorkflowExecution(
                    executor_id=executor_id,
                    workflow_version_id=execution.workflow_version_id,
                    notes=execution.notes,
                    slurm_job_id=0,
                    workflow_mode_id=execution.mode_id,
                )
            else:
                span.set_attributes(
                    {"git_commit_hash": execution.git_commit_hash, "repository_url": str(execution.repository_url)}
                )
                workflow_execution = WorkflowExecution(
                    executor_id=executor_id,
                    workflow_version_id=None,
                    notes=notes,
                    slurm_job_id=0,
                )
            self.db.add(workflow_execution)
            await self.db.flush()
            await self.db.refresh(workflow_execution)
            span.set_attribute("workflow_execution_id", str(workflow_execution.execution_id))
            with tracer.start_as_current_span(
                "db_create_workflow_execution_update_paths",
                attributes={"execution_id": str(workflow_execution.execution_id)},
            ):
                await self.db.execute(
                    update(WorkflowExecution)
                    .where(WorkflowExecution.execution_id == workflow_execution.execution_id)
                    .values(
                        logs_path=(
                            None
                            if execution.logs_s3_path is None
                            else execution.logs_s3_path + f"/run-{workflow_execution.execution_id.hex}"
                        ),
                        debug_path=(
                            None
                            if execution.debug_s3_path is None
                            else execution.debug_s3_path + f"/run-{workflow_execution.execution_id.hex}"
                        ),
                        provenance_path=(
                            None
                            if execution.provenance_s3_path is None
                            else execution.provenance_s3_path + f"/run-{workflow_execution.execution_id.hex}"
                        ),
                    )
                )
                await self.db.flush()
            return workflow_execution

    async def get(self, execution_id: UUID) -> WorkflowExecution | None:
        """
        Get a workflow execution by its execution id from the database.

        Parameters
        ----------
        execution_id : uuid.UUID
            ID of the workflow execution

        Returns
        -------
        workflow_execution : clowm.models.WorkflowExecution
            The workflow execution with the given id if it exists, None otherwise
        """
        stmt = (
            select(WorkflowExecution)
            .where(WorkflowExecution.execution_id == execution_id)
            .where(WorkflowExecution.deleted == False)  # noqa:E712
            .options(joinedload(WorkflowExecution.workflow_version))
        )
        with tracer.start_as_current_span(
            "db_get_workflow_execution", attributes={"workflow_execution_id": str(execution_id), "sql_query": str(stmt)}
        ):
            return await self.db.scalar(stmt)

    async def list(
        self,
        executor_id: UUID | None = None,
        workflow_version_id: str | None = None,
        workflow_id: UUID | None = None,
        status_list: list[WorkflowExecution.WorkflowExecutionStatus] | None = None,
        limit: int | None = None,
        sort: OrderType = "desc",
        id_after: UUID | None = None,
        start_after: int | None = None,
        start_before: int | None = None,
    ) -> Sequence[WorkflowExecution]:
        """
        List all workflow executions and apply filter.

        Parameters
        ----------
        executor_id : uuid.UUID | None, default None
            Filter for the user who started the workflow execution.
        workflow_id : uuid.UUID | None, default None
            Filter for workflows
        workflow_version_id : str | None, default None
            Filter for the workflow version
        status_list : list[clowm.models.WorkflowExecution.WorkflowExecutionStatus] | None, default None
            Filter for the status of the workflow executions.
        limit : int | None, default None
            Limit the number of retrieved workflow executions
        sort : "asc" | "desc", default "desc"
            Sort the rows ascending or descending.
        id_after : uuid.UUID | None, default None
            Filter workflow executions that come after this execution id (including).
        start_before : int | None, default None
            Filter workflow executions that started before this UNIX timestamp.
        start_after : int | None, default None
            Filter workflows executions that started after this UNIX timestamp.

        Returns
        -------
        workflow_executions : list[clowm.models.WorkflowExecution]
            List of all workflow executions with applied filters.
        """
        with tracer.start_as_current_span("db_list_workflow_executions", attributes={"sort": sort}) as span:
            stmt = (
                select(WorkflowExecution)
                .where(WorkflowExecution.deleted == False)  # noqa:E712
                .options(selectinload(WorkflowExecution.workflow_version))
                .limit(limit)
                .order_by(
                    WorkflowExecution.execution_id.desc() if sort == "desc" else WorkflowExecution.execution_id.asc()
                )
            )
            if limit is not None:  # pragma: no cover
                span.set_attribute("limit", limit)
            if executor_id is not None:
                span.set_attribute("executor_id", str(executor_id))
                stmt = stmt.where(WorkflowExecution.executor_id == executor_id)
            if workflow_version_id is not None:
                span.set_attribute("git_commit_hash", workflow_version_id)
                stmt = stmt.where(WorkflowExecution.workflow_version_id == workflow_version_id)
            if status_list is not None:
                span.set_attribute("status", [stat.name for stat in status_list])
                stmt = stmt.where(or_(*[WorkflowExecution.status == status for status in status_list]))
            if id_after is not None:
                span.set_attribute("id_after", str(id_after))
                stmt = stmt.where(
                    WorkflowExecution.execution_id <= id_after
                    if sort == "desc"
                    else WorkflowExecution.execution_id >= id_after
                )
            if start_before is not None:
                span.set_attribute("start_before", start_before)
                stmt = stmt.where(WorkflowExecution.start_time <= start_before)
            if start_after is not None:
                span.set_attribute("start_after", start_after)
                stmt = stmt.where(WorkflowExecution.start_time >= start_after)
            if workflow_id is not None:
                span.set_attribute("workflow_id", str(workflow_id))
                stmt = stmt.join(WorkflowVersion).where(WorkflowVersion.workflow_id == workflow_id)
            span.set_attribute("sql_query", str(stmt))
            return (await self.db.scalars(stmt)).all()

    async def delete(self, execution_id: UUID) -> None:
        """
        Mark a workflow execution as deleted in the database.

        Parameters
        ----------
        execution_id : uuid.UUID
            ID of the workflow execution
        """
        stmt = update(WorkflowExecution).where(WorkflowExecution.execution_id == execution_id).values(deleted=True)
        with tracer.start_as_current_span(
            "db_delete_workflow_execution",
            attributes={"workflow_execution_id": str(execution_id), "sql_query": str(stmt)},
        ):
            await self.db.execute(stmt)
            await self.db.flush()

    async def delete_by_user(self, uid: UUID) -> None:
        """
        Mark workflow executions by a user as deleted in the database.

        Parameters
        ----------
        uid : uuid.UUID
            ID of a user
        """
        stmt = update(WorkflowExecution).where(WorkflowExecution.executor_id == uid).values(deleted=True)
        with tracer.start_as_current_span(
            "db_delete_workflow_executions_by_user",
            attributes={"uid": str(uid), "sql_query": str(stmt)},
        ):
            await self.db.execute(stmt)
            await self.db.flush()

    async def update_status(
        self,
        execution_id: UUID,
        status: WorkflowExecution.WorkflowExecutionStatus,
        end_timestamp: int | float | None = None,
    ) -> None:
        """
        Update the status and optional end_time of a workflow execution in the database.

        Parameters
        ----------
        execution_id : uuid.UUID
            ID of the workflow execution
        status : clowm.models.WorkflowExecution.WorkflowExecutionStatus
            New status of the workflow execution
        end_timestamp : int | float | None, default None
            Optional end timestamp for the workflow.
        """
        with tracer.start_as_current_span(
            "db_update_workflow_execution_status",
            attributes={"workflow_execution_id": str(execution_id), "workflow_execution_status": str(status)},
        ) as span:
            stmt = (
                update(WorkflowExecution)
                .where(WorkflowExecution.execution_id == execution_id)
                .values(status=status.name)
            )
            if end_timestamp is not None:
                span.set_attribute("end_timestamp", round(end_timestamp))
                try:
                    # Check if end_time is in seconds or milliseconds
                    datetime.fromtimestamp(end_timestamp)
                    stmt = stmt.values(end_time=round(end_timestamp))
                except ValueError:
                    stmt = stmt.values(end_time=round(end_timestamp / 1000))
            span.set_attribute("sql_query", str(stmt))
            await self.db.execute(stmt)
            await self.db.flush()

    async def update_slurm_job_id(self, execution_id: UUID, slurm_job_id: int) -> None:
        """
        Update the status of a workflow execution to CANCELED in the database.

        Parameters
        ----------
        execution_id : uuid.UUID
            ID of the workflow execution
        slurm_job_id : int
            New slurm job ID
        """
        stmt = (
            update(WorkflowExecution)
            .where(WorkflowExecution.execution_id == execution_id)
            .values(slurm_job_id=slurm_job_id)
        )
        with tracer.start_as_current_span(
            "db_update_workflow_execution_slurm_id",
            attributes={
                "workflow_execution_id": str(execution_id),
                "slurm_job_id": slurm_job_id,
                "sql_query": str(stmt),
            },
        ):
            await self.db.execute(stmt)
            await self.db.flush()

    async def update_cpu_time(
        self,
        execution_id: UUID,
        additional_cpu_time: timedelta,
    ) -> None:
        """
        Update the cpu hours for a workflow execution.

        Parameters
        ----------
        execution_id : str | bytes
            ID of the workflow execution
        additional_cpu_time : datetime.timedelta
            The duration to add to the cpu time
        """
        with tracer.start_as_current_span(
            "db_update_cpu_time",
            attributes={
                "execution_id": str(execution_id),
                "cpu_days": additional_cpu_time.days,
                "cpu_seconds": additional_cpu_time.seconds,
            },
        ):
            execution = await self.db.scalar(
                select(WorkflowExecution).where(WorkflowExecution.execution_id == execution_id)
            )
            if execution is None:
                return
            execution.cpu_time += additional_cpu_time
            await self.db.flush(objects=[execution])
            await self.db.flush()

    async def get_by_id_fragment(self, execution_id_start: str | bytes) -> WorkflowExecution | None:
        """
        Get a workflow execution by its execution id from the database.

        Parameters
        ----------
        execution_id_start : str | bytes
            Start of the ID of the workflow execution

        Returns
        -------
        workflow_execution : clowm.models.WorkflowExecution
            The workflow execution with the given id if it exists, None otherwise
        """
        id_start = (
            int(execution_id_start, 16).to_bytes(ceil(len(execution_id_start) / 2), "big")
            if isinstance(execution_id_start, str)
            else execution_id_start
        )
        stmt = select(WorkflowExecution).where(func.hex(WorkflowExecution.execution_id).startswith(func.hex(id_start)))
        with tracer.start_as_current_span(
            "db_get_workflow_execution", attributes={"sql_query": str(stmt), "execution_id_start": id_start.hex()}
        ):
            return await self.db.scalar(stmt)
