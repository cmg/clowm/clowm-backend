import time
from collections.abc import Sequence
from enum import StrEnum, unique
from uuid import UUID

from opentelemetry import trace
from sqlalchemy import and_, delete, or_, select, update
from sqlalchemy.orm import joinedload
from sqlalchemy.sql import Select as SQLSelect

from clowm.models import BucketPermission as BucketPermissionDB
from clowm.schemas.bucket_permission import BucketPermissionIn as BucketPermissionSchema
from clowm.schemas.bucket_permission import BucketPermissionParameters as BucketPermissionParametersSchema

from ..db.types import BucketPermissionScopesDB
from .crud_base import CRUDBase
from .crud_bucket import CRUDBucket
from .crud_error import DuplicateError
from .crud_user import CRUDUser

tracer = trace.get_tracer_provider().get_tracer(__name__)


class CRUDBucketPermission(CRUDBase):
    @unique
    class PermissionStatus(StrEnum):
        """
        Status of a bucket permission. Can be either `ACTIVE` or `INACTIVE`. A permission can only get `INACTIVE` if the
        permission itself has a time limit and the current time is not in the timespan.
        """

        ACTIVE = "ACTIVE"
        INACTIVE = "INACTIVE"

    async def get(
        self,
        bucket_name: str,
        uid: UUID,
    ) -> BucketPermissionDB | None:
        """
        Get a the bucket permission for a given user and bucket.

        Parameters
        ----------
        uid : uuid.UUID
            Id of a user.
        bucket_name : str
            Name of a bucket.

        Returns
        -------

        """
        stmt = select(BucketPermissionDB).where(
            BucketPermissionDB.uid == uid, BucketPermissionDB.bucket_name == bucket_name
        )
        with tracer.start_as_current_span(
            "db_get_bucket_permission",
            attributes={"sql_query": str(stmt), "bucket_name": bucket_name, "uid": str(uid)},
        ):
            return await self.db.scalar(stmt)

    async def list(
        self,
        bucket_name: str | None = None,
        uid: UUID | None = None,
        permission_scopes: BucketPermissionScopesDB | None = None,
        permission_status: PermissionStatus | None = None,
    ) -> Sequence[BucketPermissionDB]:
        """
        List and filter all permissions in the database.

        Parameters
        ----------
        bucket_name : str | None
            Name of the bucket which to query.
        uid : str | None
            UID of the user which to query.
        permission_scopes : clowm.db.types.BucketPermissionScopeDB | None, default None
            Type of Bucket Permissions to fetch.
        permission_status : clowm.crud.crud_bucket_permission.CRUDBucketPermission.PermissionStatus | None, default None
            Status of Bucket Permissions to fetch.

        Returns
        -------
        buckets : list[BucketPermission]
            Filtered list off all bucket permissions.
        """
        stmt = select(BucketPermissionDB)
        with tracer.start_as_current_span("db_list_bucket_permissions") as span:
            if bucket_name is not None:
                span.set_attribute("bucket_name", bucket_name)
                stmt = stmt.options(joinedload(BucketPermissionDB.grantee)).where(
                    BucketPermissionDB.bucket_name == bucket_name
                )
            if uid is not None:
                span.set_attribute("uid", str(uid))
                stmt = stmt.where(BucketPermissionDB.uid == uid)
            if permission_scopes is not None and permission_scopes > 0:
                span.set_attribute("permission_types", permission_scopes.render())
                stmt = stmt.where(BucketPermissionDB.scopes.bitwise_and(permission_scopes) > 0)
            if permission_status is not None:
                span.set_attribute("permission_status", permission_status.name)
                stmt = CRUDBucketPermission._filter_permission_status(stmt, permission_status)
            span.set_attribute("sql_query", str(stmt))
            return (await self.db.scalars(stmt)).all()

    async def create(
        self,
        permission: BucketPermissionSchema,
    ) -> BucketPermissionDB:
        """
        Create a permission in the database and raise Exceptions if there are problems.

        Parameters
        ----------
        permission : clowm.schemas.bucket_permission.BucketPermissionOut
            The permission to create.

        Returns
        -------
        permission : clowm.models.BucketPermission
            Newly created permission model from the self.db.
        """
        with tracer.start_as_current_span(
            "db_create_bucket_permission",
            attributes={"bucket_name": permission.bucket_name, "uid": str(permission.uid)},
        ):
            # Check if user exists
            user = await CRUDUser(db=self.db).get(uid=permission.uid)
            if user is None:
                raise KeyError(
                    f"Unknown user with uid {str(permission.uid)}",
                )
            # Check that grantee is not the owner of the bucket
            bucket = await CRUDBucket(db=self.db).get(permission.bucket_name)
            if bucket is None or bucket.owner_id == user.uid:
                raise ValueError(f"User {str(permission.uid)} is the owner of the bucket {permission.bucket_name}")
            # Check if combination of user and bucket already exists
            previous_permission = await CRUDBucketPermission(db=self.db).get(
                bucket_name=permission.bucket_name, uid=user.uid
            )
            if previous_permission is not None:
                raise DuplicateError(
                    f"bucket permission for combination {permission.bucket_name} {str(permission.uid)} already exists."
                )
            # Add permission to db
            permission_db = BucketPermissionDB(
                uid=user.uid,
                bucket_name=permission.bucket_name,
                from_=permission.from_timestamp,
                to=permission.to_timestamp,
                file_prefix=permission.file_prefix,
                scopes=BucketPermissionScopesDB.parse(permission.scopes),
            )
            self.db.add(permission_db)
            await self.db.flush()
            await self.db.refresh(permission_db)
            return permission_db

    async def check_active_permission(self, bucket_name: str, uid: UUID) -> bool:
        """
        Check if the provided user has any active permission to the provided bucket.

        Parameters
        ----------
        bucket_name : str
            Name of the bucket for which to perform the check.
        uid : str
            UID of the user for which to perform the check.

        Returns
        -------
        permission_check : bool
            Return True if the user has any permission on the bucket, False otherwise.
        """
        with tracer.start_as_current_span(
            "db_check_bucket_permission",
            attributes={"uid": str(uid), "bucket_name": bucket_name},
        ):
            buckets = await CRUDBucket(db=self.db).list_for_user(uid, bucket_type=CRUDBucket.BucketType.ALL)
            return bucket_name in map(lambda x: x.name, buckets)

    async def delete(self, bucket_name: str, uid: UUID) -> None:
        """
        Delete a permission in the database.

        Parameters
        ----------
        bucket_name : str
            Name of the bucket.
        uid : str
            UID of the user.
        """
        stmt = delete(BucketPermissionDB).where(
            BucketPermissionDB.uid == uid, BucketPermissionDB.bucket_name == bucket_name
        )
        with tracer.start_as_current_span(
            "db_delete_bucket_permission",
            attributes={"bucket_name": bucket_name, "uid": str(uid), "sql_query": str(stmt)},
        ):
            await self.db.execute(stmt)
            await self.db.flush()

    async def update_permission(
        self, permission: BucketPermissionDB, new_params: BucketPermissionParametersSchema
    ) -> BucketPermissionDB:
        """
        Update a permission in the database.

        Parameters
        ----------
        permission : clowm.schemas.bucket_permission.BucketPermissionOut
            The permission to update.
        new_params : clowm.schemas.bucket_permission.BucketPermissionParameters
            The parameters which should be updated.

        Returns
        -------
        permission : clowm.models.BucketPermission
            Updated permission model from the self.db.
        """
        stmt = (
            update(BucketPermissionDB)
            .where(
                BucketPermissionDB.bucket_name == permission.bucket_name,
                BucketPermissionDB.uid == permission.uid,
            )
            .values(
                from_=new_params.from_timestamp,
                to=new_params.to_timestamp,
                file_prefix=new_params.file_prefix,
                scopes=BucketPermissionScopesDB.parse(new_params.scopes),
            )
        )
        with tracer.start_as_current_span(
            "db_update_bucket_permission",
            attributes={"sql_query": str(stmt), "bucket_name": permission.bucket_name, "uid": str(permission.uid)},
        ):
            await self.db.execute(stmt)
            await self.db.flush()
            await self.db.refresh(permission)
            return permission

    @staticmethod
    def _filter_permission_status(stmt: SQLSelect, permission_status: PermissionStatus) -> SQLSelect:
        """
        Add a where clauses to the SQL Statement where the status of permission is filtered based on the current time.

        Parameters
        ----------
        stmt : sqlalchemy.sql.Select
            Declarative Select statement from SQLAlchemy
        permission_status : PermissionStatus
            Status of Bucket Permissions to filter for.

        Returns
        -------
        stmt : sqlalchemy.sql.Select
            Declarative Select statement with added where clause.
        """
        current_time = time.time()
        if permission_status == CRUDBucketPermission.PermissionStatus.ACTIVE:
            return stmt.where(
                or_(
                    current_time >= BucketPermissionDB.from_,
                    BucketPermissionDB.from_ == None,  # noqa:E711
                )
            ).where(
                or_(
                    current_time <= BucketPermissionDB.to,
                    BucketPermissionDB.to == None,  # noqa:E711
                )
            )
        return stmt.where(
            or_(
                and_(
                    current_time <= BucketPermissionDB.from_,
                    BucketPermissionDB.from_ != None,  # noqa:E711
                ),
                and_(
                    current_time >= BucketPermissionDB.to,
                    BucketPermissionDB.to != None,  # noqa:E711
                ),
            )
        )
