from collections.abc import Iterable
from uuid import UUID

from opentelemetry import trace
from sqlalchemy import delete, select

from clowm.crud.crud_base import CRUDBase
from clowm.models import WorkflowMode, workflow_mode_association_table
from clowm.schemas.workflow_mode import WorkflowModeIn

tracer = trace.get_tracer_provider().get_tracer(__name__)


class CRUDWorkflowMode(CRUDBase):
    async def list_modes(self, workflow_version_id: str) -> list[WorkflowMode]:
        """
        List all workflow modes of a specific workflow version.

        Parameters
        ----------
        workflow_version_id : str
            The version id for which the modes should be loaded.

        Returns
        -------
        modes : list[clowm.models.WorkflowMode]
            List of workflow modes.
        """
        stmt = (
            select(WorkflowMode)
            .join(workflow_mode_association_table)
            .where(workflow_mode_association_table.columns.workflow_version_commit_hash == workflow_version_id)
        )
        with tracer.start_as_current_span(
            "db_list_workflow_modes", attributes={"workflow_version_id": workflow_version_id, "sql_query": str(stmt)}
        ):
            return list((await self.db.scalars(stmt)).all())

    async def get(self, mode_id: UUID, workflow_version_id: str | None = None) -> WorkflowMode | None:
        """
        Get a specific workflow mode.

        Parameters
        ----------
        mode_id : UUID
            ID of a workflow mode.
        workflow_version_id : str | None, default None
            Optional workflow version the workflow mode has to be connected to.

        Returns
        -------
        workflows : clowm.models.WorkflowMode | None
            Requested workflow mode if it exists, None otherwise
        """
        with tracer.start_as_current_span(
            "db_get_workflow_mode", attributes={"workflow_mode_id": str(mode_id)}
        ) as span:
            stmt = select(WorkflowMode).where(WorkflowMode.mode_id == mode_id)
            if workflow_version_id is not None:
                span.set_attribute("workflow_version_id", workflow_version_id)
                stmt = stmt.join(workflow_mode_association_table).where(
                    workflow_mode_association_table.columns.workflow_version_commit_hash == workflow_version_id
                )
            span.set_attribute("sql_query", str(stmt))
            return await self.db.scalar(stmt)

    async def create(self, modes: list[WorkflowModeIn]) -> list[WorkflowMode]:
        """
        Create multiple workflow modes at once.

        Parameters
        ----------
        modes : list[clowm.schemas.workflow_mode.WorkflowModeIn]
            Schemas of the workflow modes to be created.

        Returns
        -------
        modes : list[clowm.models.WorkflowMode]
            Newly created workflow modes
        """
        with tracer.start_as_current_span("db_create_workflow_mode") as span:
            modes_db = []
            for mode in modes:
                mode_db = WorkflowMode(name=mode.name, entrypoint=mode.entrypoint, schema_path=mode.schema_path)
                self.db.add(mode_db)
                modes_db.append(mode_db)
            await self.db.flush()
            span.set_attribute("workflow_mode_ids", [str(m.mode_id) for m in modes_db])
            return modes_db

    async def delete(self, modes: Iterable[UUID]) -> None:
        """
        Delete multiple workflows at once.

        Parameters
        ----------
        modes : list[uuid.UUID]
            ID of workflow modes to delete
        """
        stmt = delete(WorkflowMode).where(WorkflowMode.mode_id.in_(modes))
        with tracer.start_as_current_span(
            "db_delete_workflow_mode", attributes={"workflow_mode_ids": [str(m) for m in modes], "sql_query": str(stmt)}
        ):
            await self.db.execute(stmt)
            await self.db.flush()
