from datetime import date, datetime
from hashlib import sha256
from os import urandom
from uuid import UUID

from opentelemetry import trace
from sqlalchemy import Date, cast, delete, func, or_, select, update
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import joinedload

from clowm.models import Workflow, WorkflowExecution, WorkflowVersion
from clowm.schemas.workflow import WorkflowIn, WorkflowStatistic
from clowm.schemas.workflow_execution import AnonymizedWorkflowExecution

from .crud_base import CRUDBase
from .crud_workflow_mode import CRUDWorkflowMode
from .crud_workflow_version import CRUDWorkflowVersion
from .mixins import OTRCrudMixin

tracer = trace.get_tracer_provider().get_tracer(__name__)


class CRUDWorkflow(CRUDBase, OTRCrudMixin):
    def __init__(self, db: AsyncSession) -> None:
        super().__init__(db)
        self.model = Workflow

    async def list_workflows(
        self,
        name_substring: str | None = None,
        developer_id: UUID | None = None,
        version_status: list[WorkflowVersion.WorkflowVersionStatus] | None = None,
    ) -> list[Workflow]:
        """
        List all workflows. Populates the version attribute of the workflows.

        Parameters
        ----------
        name_substring : str | None, default None
            Substring to filter for in the name of a workflow.
        developer_id : str | None, default None
            Filter workflows by developer.
        version_status : list[clowm.models.WorkflowVersion.Status] | None, default None
            Filter versions of a workflow based on the status. Removes workflows that have no version after this filter.

        Returns
        -------
        workflows : list[clowm.models.Workflow]
            List of workflows.
        """
        with tracer.start_as_current_span("db_list_workflows") as span:
            stmt = select(Workflow).options(joinedload(Workflow.versions).selectinload(WorkflowVersion.workflow_modes))
            if name_substring is not None:
                span.set_attribute("name_substring", name_substring)
                stmt = stmt.where(Workflow.name.contains(name_substring))
            if developer_id is not None:
                span.set_attribute("uid", str(developer_id))
                stmt = stmt.where(Workflow.developer_id == developer_id)
            if version_status is not None and len(version_status) > 0:
                span.set_attribute("status", [stat.name for stat in version_status])
                stmt = stmt.options(
                    joinedload(
                        Workflow.versions.and_(or_(*[WorkflowVersion.status == status for status in version_status]))
                    )
                )
            span.set_attribute("sql_query", str(stmt))
            return [w for w in (await self.db.scalars(stmt)).unique().all() if len(w.versions) > 0]

    async def delete(self, workflow_id: UUID) -> None:
        """
        Delete a workflow.

        Parameters
        ----------
        workflow_id : uuid.UUID
            UID of a workflow
        """
        stmt = delete(Workflow).where(Workflow.workflow_id == workflow_id)
        with tracer.start_as_current_span(
            "db_delete_workflow", attributes={"workflow_id": str(workflow_id), "sql_query": str(stmt)}
        ):
            await self.db.execute(stmt)
            await self.db.flush()

    async def update_credentials(self, workflow_id: UUID, token: str | None = None) -> None:
        """
        Delete a workflow.

        Parameters
        ----------
        workflow_id : uuid.UUID
            UID of a workflow
        token : str | None
            Token to save in the database. If None, the token in the database gets deleted
        """
        stmt = update(Workflow).where(Workflow.workflow_id == workflow_id).values(credentials_token=token)
        with tracer.start_as_current_span(
            "db_update_workflow_credentials",
            attributes={"workflow_id": str(workflow_id), "sql_query": str(stmt), "delete": token is None},
        ):
            await self.db.execute(stmt)
            await self.db.flush()

    async def developer_statistics(
        self,
        developer_id: UUID | None = None,
        workflow_ids: list[UUID] | None = None,
        start: date | None = None,
        end: date | None = None,
    ) -> list[AnonymizedWorkflowExecution]:
        """
        Get all workflow executions for a set of workflows with anonymized user ID.

        Parameters
        ----------
        developer_id : uuid.UUID | None, default None
            Filter workflow by developer ID.
        workflow_ids : uuid.UUID | None, default None
            Filter workflows by ID.
        start : datetime.date | None, default None
            Filter workflow execution that started after this date.
        end : datetime.date | None, default None
            Filter workflow execution that started before this date

        Returns
        -------
        stats : list[clowm.schemas.Workflow.AnonymizedWorkflowExecution]
            List of datapoints
        """
        with tracer.start_as_current_span("db_get_workflow_developer_statistics") as span:
            stmt = (
                select(
                    cast(func.FROM_UNIXTIME(WorkflowExecution.start_time), Date).label("started_at"),
                    WorkflowExecution.execution_id,
                    WorkflowExecution.executor_id,
                    WorkflowExecution.workflow_mode_id,
                    WorkflowVersion.git_commit_hash,
                    Workflow.workflow_id,
                    Workflow.developer_id,
                    WorkflowExecution.status,
                )
                .select_from(WorkflowExecution)
                .join(WorkflowVersion)
                .join(Workflow)
                .where(WorkflowExecution.end_time != None)  # noqa:E711
            )
            if developer_id:
                span.set_attribute("developer_id", str(developer_id))
                stmt = stmt.where(Workflow.developer_id == developer_id)
            if workflow_ids:
                span.set_attribute("workflow_ids", [str(wid) for wid in workflow_ids])
                stmt = stmt.where(*[Workflow.workflow_id == wid for wid in workflow_ids])
            if start:
                span.set_attribute("start_date", start.isoformat())
                timestamp = round(datetime(year=start.year, month=start.month, day=start.day).timestamp())
                stmt = stmt.where(WorkflowExecution.start_time > timestamp)
            if end:
                span.set_attribute("end_date", end.isoformat())
                timestamp = round(datetime(year=end.year, month=end.month, day=end.day).timestamp())
                stmt = stmt.where(WorkflowExecution.start_time < timestamp)
            _user_hashes = {
                # sha256 hash of 16 zero bytes
                b"\x00" * 16: "374708fff7719dd5979ec875d56cd2286f6d3cf7ec317a3b25632aab28ec37bb"
            }

            def hash_user_id(uid: UUID) -> str:
                if uid.bytes not in _user_hashes:
                    hash_obj = sha256(usedforsecurity=True)
                    hash_obj.update(uid.bytes)
                    hash_obj.update(urandom(32))
                    _user_hashes[uid.bytes] = hash_obj.hexdigest()
                return _user_hashes[uid.bytes]

            span.set_attribute("sql_query", str(stmt))
            rows = await self.db.execute(stmt)
            return [
                AnonymizedWorkflowExecution(
                    workflow_execution_id=row.execution_id,
                    # group all deleted users to a single one
                    pseudo_uid=hash_user_id(row.executor_id if row.executor_id is not None else UUID("0" * 32)),
                    workflow_mode_id=row.workflow_mode_id,
                    started_at=row.started_at,
                    workflow_id=row.workflow_id,
                    developer_id=row.developer_id,
                    workflow_version_id=row.git_commit_hash,
                    status=row.status,
                )
                for row in rows
            ]

    async def statistics(self, workflow_id: UUID) -> list[WorkflowStatistic]:
        """
        Calculate the number of workflows started per day for a specific workflow

        Parameters
        ----------
        workflow_id : uuid.UUID
            UID of a workflow.

        Returns
        -------
        stats : list[clowm.schemas.Workflow.WorkflowStatistic]
            List of datapoints
        """
        stmt = (
            select(cast(func.FROM_UNIXTIME(WorkflowExecution.start_time), Date).label("day"), func.count())
            .select_from(WorkflowExecution)
            .join(WorkflowVersion)
            .where(WorkflowVersion.workflow_id == workflow_id)
            .group_by("day")
            .order_by("day")
        )
        with tracer.start_as_current_span(
            "db_get_workflow_statistics", attributes={"workflow_id": str(workflow_id), "sql_query": str(stmt)}
        ):
            return [WorkflowStatistic(day=row.day, count=row.count) for row in await self.db.execute(stmt)]

    async def get(self, workflow_id: UUID) -> Workflow | None:
        """
        Get a workflow by its ID.

        Parameters
        ----------
        workflow_id : uuid.UUID
            UID of a workflow.

        Returns
        -------
        user : clowm.models.Workflow | None
            The workflow with the given ID if it exists, None otherwise
        """
        stmt = (
            select(Workflow)
            .where(Workflow.workflow_id == workflow_id)
            .options(joinedload(Workflow.versions).selectinload(WorkflowVersion.workflow_modes))
        )
        with tracer.start_as_current_span(
            "db_get_workflow", attributes={"workflow_id": str(workflow_id), "sql_query": str(stmt)}
        ):
            return await self.db.scalar(stmt)

    async def get_by_name(self, workflow_name: str) -> Workflow | None:
        """
        Get a workflow by its name.

        Parameters
        ----------
        workflow_name : str
            Name of a workflow.

        Returns
        -------
        user : clowm.models.Workflow | None
            The workflow with the given name if it exists, None otherwise
        """
        stmt = (
            select(Workflow)
            .where(Workflow.name == workflow_name)
            .options(joinedload(Workflow.versions).selectinload(WorkflowVersion.workflow_modes))
        )
        with tracer.start_as_current_span(
            "db_get_workflow_by_name", attributes={"name": workflow_name, "sql_query": str(stmt)}
        ):
            return await self.db.scalar(stmt)

    async def create(self, workflow: WorkflowIn, developer_id: UUID, icon_slug: str | None = None) -> Workflow:
        """
        Create a workflow and the initial version in the database

        Parameters
        ----------
        workflow : clowm.schemas.workflow.WorkflowIn
            Parameters for creating the workflow
        developer_id : uuid.UUID
            UID of the developer
        icon_slug : str | None, default None
            Optional slug of the icon saved in the icon bucket

        Returns
        -------
        workflow : clowm.models.Workflow
            The newly created workflow
        """
        with tracer.start_as_current_span("db_create_workflow") as span:
            workflow_db = Workflow(
                name=workflow.name,
                repository_url=str(workflow.repository_url).strip("/"),
                short_description=workflow.short_description,
                developer_id=developer_id,
                credentials_token=workflow.token,
            )
            self.db.add(workflow_db)
            await self.db.flush()

            # If there are workflow modes, create them first
            modes_db = []
            if len(workflow.modes) > 0:
                modes_db = await CRUDWorkflowMode(db=self.db).create(workflow.modes)
            await CRUDWorkflowVersion(db=self.db).create(
                git_commit_hash=workflow.git_commit_hash,
                version=workflow.initial_version,
                workflow_id=workflow_db.workflow_id,
                icon_slug=icon_slug,
                modes=[mode.mode_id for mode in modes_db],
                nextflow_version=str(workflow.nextflow_version),
            )
            span.set_attribute("workflow_id", str(workflow_db.workflow_id))
            return await CRUDWorkflow(db=self.db).get(workflow_db.workflow_id)  # type: ignore[return-value]
