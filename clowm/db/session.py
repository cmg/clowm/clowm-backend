from collections.abc import Iterator
from contextlib import contextmanager

from sqlalchemy import create_engine
from sqlalchemy.orm import Session, sessionmaker


@contextmanager
def get_session(url: str, verbose: bool = False) -> Iterator[Session]:
    """
    Get a sessionmaker that can be used as a context manager

    Parameters
    ----------
    url : str
        The url for connecting to the database
    verbose : bool, default False
        Flag for more verbose output

    Returns
    -------
    sessionmaker : sqlalchemy.orm.sessionmaker
        The sessionmaker for creating a session

    Notes
    -----
    The sessionmaker can be used as a context manager:

    with get_session(url=db_url) as db:
            db.select(...)
    """
    engine = create_engine(url, pool_pre_ping=True, pool_recycle=3600, echo=verbose)
    with sessionmaker(autocommit=False, autoflush=False, bind=engine)() as db:
        yield db
