from dataclasses import dataclass
from enum import StrEnum, auto
from uuid import UUID

from sqlalchemy import ForeignKey, String
from sqlalchemy.dialects.mysql import INTEGER
from sqlalchemy.orm import Mapped, mapped_column


class OwnershipTypeEnum(StrEnum):
    BUCKET = auto()
    WORKFLOW = auto()
    RESOURCE = auto()


class OwnershipTransferRequestMixin:
    @dataclass
    class TargetProperties:
        name: str
        description: str
        type: OwnershipTypeEnum

    transfer_new_owner_uid: Mapped[UUID | None] = mapped_column(
        ForeignKey("user.uid", ondelete="SET NULL"), name="transfer_new_owner_id", nullable=True
    )
    transfer_comment: Mapped[str | None] = mapped_column(String(256), nullable=True)
    transfer_created_at: Mapped[int | None] = mapped_column(INTEGER(unsigned=True), nullable=True)
    last_transfer_timestamp: Mapped[int | None] = mapped_column(INTEGER(unsigned=True), nullable=True)
    clowm_owner_id: Mapped[UUID | None]
    clowm_id: Mapped[str | UUID]

    @property
    def target_properties(self) -> TargetProperties:
        """
        Get the target specific properties.

        Returns
        -------
        properties : clowm.db.mixins.ownership_transfer_mixin.OwnershipTransferRequestMixin.TargetProperties
            The properties saved in different columns for each specific target
        """
        raise NotImplementedError("Subclass did not implement this property")
