import time

from sqlalchemy.dialects.mysql import INTEGER
from sqlalchemy.orm import Mapped, mapped_column


class CreateUpdateMixin:
    created_at: Mapped[int] = mapped_column(INTEGER(unsigned=True), nullable=False, default=lambda: round(time.time()))
    updated_at: Mapped[int] = mapped_column(
        INTEGER(unsigned=True), nullable=False, default=lambda: round(time.time()), onupdate=lambda: round(time.time())
    )
