import base64
from enum import IntFlag, auto
from typing import Any, Generic, Literal, TypeVar
from uuid import UUID

import sqlalchemy.types as types
from sqlalchemy.engine.interfaces import Dialect

__all__ = [
    "SQLUUID",
    "Token",
    "SQLIntFlag",
    "ApiTokenScopesDB",
    "ApiTokenScopes",
    "BucketPermissionScopes",
    "BucketPermissionScopesDB",
    "OrderType",
    "NewsCategory",
]


OrderType = Literal["asc", "desc"]

NewsCategory = Literal["workflow", "resource", "system"]

ApiTokenScopes = Literal["read", "write"]


class ApiTokenScopesDB(IntFlag):
    """
    IntFlag for the scopes an API token can have.
    """

    READ = auto()
    WRITE = auto()

    def render(self) -> list[ApiTokenScopes]:
        """
        Convert the IntFlag into a list of strings.

        Returns
        -------
        scopes : list[ApiTokenScopes]
            List of scopes in this IntFlag
        """
        return [scope.name.lower() for scope in ApiTokenScopesDB if scope & self == scope]  # type: ignore[union-attr, misc, valid-type]

    @staticmethod
    def parse(scopes: list[ApiTokenScopes]) -> "ApiTokenScopesDB":
        """
        Parse an IntFlag object from a list of strings.

        Parameters
        ----------
        scopes : list[ApiTokenScopes]
            List of scopes the BitFlag should hold

        Returns
        -------
        scopes : ApiTokenScopesDB
            BitFlag with the specified bits set.
        """
        sc = ApiTokenScopesDB(0)
        for s in scopes:
            sc |= ApiTokenScopesDB[s.upper()]
        return sc

    def __contains__(self, item: "ApiTokenScopesDB") -> bool:
        """
        Checks if the specified scope bit(s) is set in this BitFlags.

        Parameters
        ----------
        item : ApiTokenScopesDB
            Scope to check.

        Returns
        -------
        check : bool
            Flag if the appropriate bits are set
        """
        return self & item == item


BucketPermissionScopes = Literal["read", "write"]


class BucketPermissionScopesDB(IntFlag):
    """
    IntFlag for the scopes a bucket permission can have.
    """

    READ = auto()
    WRITE = auto()

    def render(self) -> list[BucketPermissionScopes]:
        """
        Convert the IntFlag into a list of strings.

        Returns
        -------
        scopes : list[BucketPermissionScopes]
            List of scopes in this IntFlag
        """
        return [s.name.lower() for s in BucketPermissionScopesDB if s & self == s]  # type: ignore[union-attr, misc, valid-type]

    @staticmethod
    def parse(scopes: list[BucketPermissionScopes]) -> "BucketPermissionScopesDB":
        """
        Parse an IntFlag object from a list of strings.

        Parameters
        ----------
        scopes : list[BucketPermissionScopes]
            List of scopes the BitFlag should hold

        Returns
        -------
        scopes : BucketPermissionScopesDB
            BitFlag with the specified bits set.
        """
        sc = BucketPermissionScopesDB(0)
        for s in scopes:
            sc |= BucketPermissionScopesDB[s.upper()]
        return sc

    def __contains__(self, item: "BucketPermissionScopesDB") -> bool:
        """
        Checks if the specified scope bit(s) is set in this BitFlags.

        Parameters
        ----------
        item : BucketPermissionScopesDB
            Scope to check.

        Returns
        -------
        check : bool
            Flag if the appropriate bits are set
        """
        return self & item == item


class SQLUUID(types.TypeDecorator):
    """
    Type adapter for saving and retrieving UUIDs from the database.
    """

    impl = types.BINARY(16)
    cache_ok = True

    def process_bind_param(self, value: UUID | None, dialect: Dialect) -> bytes | None:  # type: ignore[override]
        return value.bytes if value is not None else None

    def process_result_value(self, value: bytes | None, dialect: Dialect) -> UUID | None:  # type: ignore[override]
        return None if value is None else UUID(bytes=value)


class Token(types.TypeDecorator):
    """
    Type adapter for saving tokens in binary format to the database.
    """

    impl = types.BINARY(32)
    cache_ok = True

    def process_bind_param(self, value: str | None, dialect: Dialect) -> bytes | None:  # type: ignore[override]
        return base64.urlsafe_b64decode(value + "==") if value is not None else None

    def process_result_value(self, value: bytes | None, dialect: Dialect) -> str | None:  # type: ignore[override]
        return None if value is None else base64.urlsafe_b64encode(value).rstrip(b"=").decode("ascii")


_T = TypeVar("_T", bound=IntFlag)


class SQLIntFlag(types.TypeDecorator, Generic[_T]):
    """
    Type adapter for saving int flags as bytes in the database.
    """

    def __init__(self, flag_type: type[_T], *args: Any, **kwargs: Any) -> None:
        self.__requiredBytes = len(flag_type) // 8 + 1
        super().__init__(*args, length=self.__requiredBytes, **kwargs)
        self.__flag_type = flag_type

    impl = types.BINARY
    cache_ok = True

    def process_bind_param(self, value: _T, dialect: Dialect) -> bytes:  # type: ignore[override]
        return value.to_bytes(self.__requiredBytes, byteorder="big", signed=False)

    def process_result_value(self, value: bytes, dialect: Dialect) -> _T:  # type: ignore[override]
        return self.__flag_type(int.from_bytes(value, byteorder="big", signed=False))
