import asyncio
import logging

from fastapi import status
from httpx import AsyncClient
from tenacity import after_log, before_log, retry, stop_after_attempt, wait_fixed

from clowm.slurm.client import get_slurm_client

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

max_tries = 60 * 3  # 3 minutes
wait_seconds = 2


@retry(
    stop=stop_after_attempt(max_tries),
    wait=wait_fixed(wait_seconds),
    before=before_log(logger, logging.INFO),
    after=after_log(logger, logging.WARN),
)
async def init(client: AsyncClient) -> None:
    try:
        slurm_client = get_slurm_client(client)
        response = await slurm_client.ping_cluster()
        assert response.status_code == status.HTTP_200_OK
    except Exception as e:
        logger.error(e)
        raise e


async def main() -> None:
    logger.info("Check Slurm Cluster connection")
    async with AsyncClient(timeout=5.0) as client:
        await init(client)
    logger.info("Slurm Cluster connection established")


if __name__ == "__main__":
    asyncio.run(main())
