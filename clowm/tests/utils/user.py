from dataclasses import dataclass
from uuid import UUID

import pytest
from httpx import Cookies
from sqlalchemy import insert
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.api.dependencies.website.auth import cookie_jwt_scheme
from clowm.core.auth import create_access_token
from clowm.core.oidc import OIDCProvider
from clowm.models import Role, User, UserRoleMapping
from clowm.models.role import RoleIdMapping

from .utils import random_lower_string


@dataclass
class UserWithAuthCookie:
    auth_cookie: Cookies
    user: User

    @property
    def uid(self) -> UUID:
        return self.user.uid


@pytest.mark.asyncio
def get_authorization_cookies(uid: UUID) -> Cookies:
    """
    Login a user and return the correct headers for subsequent requests.

    Parameters
    ----------
    uid : str
        UID of the user who should be logged in.

    Returns
    -------
    headers : dict[str,str]
        HTTP Headers to authorize each request.
    """
    jwt = create_access_token(str(uid))
    return Cookies({cookie_jwt_scheme.model.name: jwt})


@pytest.mark.asyncio
async def create_random_user(
    roles: list[Role.RoleEnum] | None = None, provider: OIDCProvider = OIDCProvider.lifescience, *, db: AsyncSession
) -> User:
    """
    Creates a random user in the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.

    roles : list[Role.RoleEnum] | None, default None
        A list of roles to create the user with.
    provider : clowm.core.oidc.OIDCProvider, default OIDCProvider.lifescience
        AAI provider which is connected to the user.

    Returns
    -------
    user : clowm.models.User
        Newly created user.
    """
    user = User(
        display_name=random_lower_string(),
        email=f"{random_lower_string(8)}@example.de",
    )
    setattr(user, user.provider_id_column(provider).key, random_lower_string())
    db.add(user)
    await db.flush()
    mapping = RoleIdMapping()
    if roles is not None and len(roles) > 0:
        insert_params = [{"uid": user.uid, "role_id": mapping[role]} for role in roles]
        await db.execute(insert(UserRoleMapping), insert_params)
        await db.flush()
        await db.refresh(user, attribute_names=["roles"])
    await db.commit()
    return user
