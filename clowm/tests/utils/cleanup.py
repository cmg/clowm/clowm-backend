import json
from collections.abc import Callable
from inspect import iscoroutinefunction
from typing import TYPE_CHECKING, Any, ParamSpec
from uuid import UUID

from sqlalchemy import delete
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.models import Bucket, News, User, Workflow, WorkflowExecution, WorkflowMode
from clowm.utils.job import AsyncJob, Job

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import BucketPolicy
else:
    BucketPolicy = object

P = ParamSpec("P")

__all__ = [
    "CleanupList",
    "delete_user",
    "delete_bucket",
    "delete_workflow",
    "delete_workflow_mode",
    "delete_policy_statement",
    "delete_workflow_execution",
    "delete_news_event",
]


class CleanupList:
    """
    Helper object to hold a queue of functions that can be executed later
    """

    def __init__(self) -> None:
        self.queue: list[Job] = []

    def add_task(self, func: Callable[P, Any], *args: P.args, **kwargs: P.kwargs) -> None:
        """
        Add a (async) function to the queue.

        Parameters
        ----------
        func : Callable[P, Any]
            Function to register.
        args : P.args
            Arguments to the function.
        kwargs : P.kwargs
            Keyword arguments to the function.
        """
        if iscoroutinefunction(func):
            self.queue.append(AsyncJob(func, *args, **kwargs))
        else:
            self.queue.append(Job(func, *args, **kwargs))

    async def empty_queue(self) -> None:
        """
        Empty the queue by executing the registered functions.
        """
        while len(self.queue) > 0:
            func = self.queue.pop()
            if func.is_async:
                await func()
            else:
                func()


async def delete_workflow(db: AsyncSession, workflow_id: UUID) -> None:
    await db.execute(delete(Workflow).where(Workflow.workflow_id == workflow_id))
    await db.commit()


async def delete_workflow_execution(db: AsyncSession, execution_id: UUID) -> None:
    await db.execute(delete(WorkflowExecution).where(WorkflowExecution.execution_id == execution_id))
    await db.commit()


async def delete_workflow_mode(db: AsyncSession, mode_id: UUID) -> None:
    await db.execute(delete(WorkflowMode).where(WorkflowMode.mode_id == mode_id))
    await db.commit()


async def delete_policy_statement(s3policy: BucketPolicy, sid: str) -> None:
    policy = json.loads(await s3policy.policy)
    policy["Statement"] = [stmt for stmt in policy["Statement"] if stmt["Sid"] != sid]
    await s3policy.put(Policy=json.dumps(policy))


async def delete_user(db: AsyncSession, uid: UUID) -> None:
    await db.execute(delete(User).where(User.uid == uid))
    await db.commit()


async def delete_bucket(db: AsyncSession, bucket_name: str) -> None:
    await db.execute(delete(Bucket).where(Bucket.name == bucket_name))
    await db.commit()


async def delete_news_event(db: AsyncSession, news_id: UUID) -> None:
    await db.execute(delete(News).where(News.news_id == news_id))
    await db.commit()
