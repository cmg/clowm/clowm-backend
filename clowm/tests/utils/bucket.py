import json
from datetime import datetime
from typing import TYPE_CHECKING
from uuid import UUID

from sqlalchemy import delete, update
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.api.core.bucket import BucketCore
from clowm.db.types import BucketPermissionScopesDB
from clowm.models import Bucket, BucketPermission, User

from .utils import random_lower_string

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object


async def delete_bucket(bucket_name: str, *, db: AsyncSession) -> None:
    """
    Delete a bucket by name.

    Parameters
    ----------
    bucket_name : str
        Name of bucket.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.
    """
    await db.execute(
        delete(Bucket).where(
            Bucket.name == bucket_name,
        )
    )
    await db.commit()


async def create_random_bucket(db: AsyncSession, user: User) -> Bucket:
    """
    Creates a random bucket in the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.
    user : clowm.models.User
        Owner of the bucket.

    Returns
    -------
    bucket : clowm.models.Bucket
        Newly created bucket.
    """
    bucket = Bucket(
        name=random_lower_string(),
        description=random_lower_string(length=127),
        owner_id=user.uid,
    )
    db.add(bucket)
    await db.commit()
    return bucket


async def add_permission_for_bucket(
    db: AsyncSession,
    bucket_name: str,
    uid: UUID,
    from_: datetime | None = None,
    to: datetime | None = None,
    scopes: BucketPermissionScopesDB = BucketPermissionScopesDB.READ,
) -> None:
    """
    Creates Permission to a bucket for a user in the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.
    bucket_name : str
        name of the bucket.
    uid : str
        UID of the user who we grant the permission.
    from_ : datetime.datetime | None, default None
        Time when from when the permission should be active.
    to : datetime.datetime | None, default None
        Time till when the permissions should be active.
    scopes : clowm.db.types.BucketPermissionScopes, default 0  # noqa:E501
        The permission the user is granted.
    """
    perm = BucketPermission(
        uid=uid,
        bucket_name=bucket_name,
        from_=round(from_.timestamp()) if from_ is not None else None,
        to=round(to.timestamp()) if to is not None else None,
        scopes=scopes,
    )
    db.add(perm)
    await db.commit()


async def make_bucket_public(db: AsyncSession, bucket_name: str, s3: S3ServiceResource) -> None:
    """
    Creates Permission to a bucket for a user in the database.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.
    bucket_name : str
        Name of the bucket.
    s3 : types_aiobotocore_s3.service_resource.S3ServiceResource
        Mock S3 Service to manipulate objects.
    """
    await db.execute(update(Bucket).where(Bucket.name == bucket_name).values(public=True))
    await db.commit()
    s3_policy = await s3.BucketPolicy(bucket_name=bucket_name)
    policy = json.loads(await s3_policy.policy)
    policy["Statement"] += BucketCore.get_anonymously_bucket_policy(bucket_name)
    await s3_policy.put(Policy=json.dumps(policy))
