import time
from uuid import UUID

from sqlalchemy import update
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.db.mixins import OwnershipTransferRequestMixin


async def create_otr(
    target_id: str | UUID,
    new_owner_id: UUID,
    comment: str = "",
    *,
    otr_type: type[OwnershipTransferRequestMixin],
    db: AsyncSession,
) -> None:
    """
    Create a OTR for the target.

    Parameters
    ----------
    target_id : str | uuid.UUID
        Id of the target
    new_owner_id : uuid.UUID
        UID of the new owner
    comment : str, default ''
        The comment for the OTR
    otr_type : Type[OwnershipTransferRequestMixin]
        Type of OTR model
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.
    """
    await db.execute(
        update(otr_type)
        .where(otr_type.clowm_id == target_id)
        .values(
            transfer_new_owner_uid=new_owner_id,
            transfer_comment=comment,
            transfer_created_at=round(time.time()),
        )
    )
    await db.commit()
