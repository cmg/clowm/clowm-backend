import asyncio
import json
import time
from collections.abc import AsyncIterator, Generator, Iterator
from contextlib import asynccontextmanager
from io import BytesIO
from typing import TYPE_CHECKING, Any
from uuid import uuid4

import httpx
import pytest
import pytest_asyncio
from botocore.exceptions import ClientError
from fastapi import status
from fastapi.responses import RedirectResponse
from sqlalchemy import delete, insert, select, update
from sqlalchemy.ext.asyncio import AsyncSession, async_sessionmaker, create_async_engine

from clowm.api.background import dependencies as background_dependencies
from clowm.api.dependencies import services
from clowm.api.dependencies.services import get_httpx_client, get_oidc_client, get_rgw_admin, get_s3_resource
from clowm.ceph.s3 import setup_s3_data_bucket
from clowm.core.config import settings
from clowm.core.oidc import OIDCClient, OIDCProvider
from clowm.db.types import ApiTokenScopesDB
from clowm.git_repository import build_repository
from clowm.main import app
from clowm.models import (
    ApiToken,
    Bucket,
    BucketPermission,
    News,
    Resource,
    ResourceVersion,
    Role,
    User,
    Workflow,
    WorkflowExecution,
    WorkflowMode,
    WorkflowVersion,
    workflow_mode_association_table,
)
from clowm.models.role import RoleIdMapping
from clowm.schemas.bucket_permission import BucketPermissionOut as BucketPermissionSchema
from clowm.schemas.documentation_enum import DocumentationEnum
from clowm.schemas.resource_version import FileTree, resource_version_dir_name, resource_version_key
from clowm.schemas.workflow import WorkflowOut
from clowm.scm import SCM, SCMProvider

from .mocks import DefaultMockHTTPService, MockContainerRegistry, MockHTTPService, MockRGWAdmin, MockSlurmCluster
from .utils import (
    CleanupList,
    UserWithAuthCookie,
    create_random_user,
    delete_workflow,
    get_authorization_cookies,
    random_hex_string,
    random_lower_string,
)

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object


@pytest.fixture(scope="session")
def event_loop() -> Generator:
    """
    Creates an instance of the default event loop for the test session.
    """
    loop = asyncio.new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
def mock_rgw_admin() -> MockRGWAdmin:
    """
    Fixture for creating a mock object for the rgwadmin package.
    """
    return MockRGWAdmin()


@pytest_asyncio.fixture(scope="module")
async def mock_s3_service() -> AsyncIterator[S3ServiceResource]:
    """
    Fixture for creating a service resource to the moto service.
    https://docs.getmoto.org/en/latest/
    """
    async with background_dependencies.get_background_s3_resource() as s3:
        await setup_s3_data_bucket(s3)
        yield s3
        async with httpx.AsyncClient() as client:
            await client.post(url=f"{settings.s3.uri}moto-api/reset")


@pytest.fixture(scope="session")
def mock_default_http_server() -> Iterator[DefaultMockHTTPService]:
    mock_server = DefaultMockHTTPService()
    yield mock_server
    mock_server.reset()


@pytest.fixture(scope="session")
def mock_slurm_cluster() -> Iterator[MockSlurmCluster]:
    mock_slurm = MockSlurmCluster()
    yield mock_slurm
    mock_slurm.reset()


@pytest.fixture(scope="session")
def mock_container_registry() -> Iterator[MockContainerRegistry]:
    mock_registry = MockContainerRegistry()
    yield mock_registry
    mock_registry.reset()


@pytest_asyncio.fixture(scope="session")
async def mock_client(
    mock_slurm_cluster: MockSlurmCluster,
    mock_default_http_server: DefaultMockHTTPService,
    mock_container_registry: MockContainerRegistry,
) -> AsyncIterator[httpx.AsyncClient]:
    def mock_request_handler(request: httpx.Request) -> httpx.Response:
        url = str(request.url)
        handler: MockHTTPService
        if request.url.host == "registry-1.docker.io":
            handler = mock_container_registry
        elif url.startswith(str(settings.cluster.slurm.uri)):
            handler = mock_slurm_cluster
        else:
            handler = mock_default_http_server
        return handler.handle_request(request=request)

    async with httpx.AsyncClient(transport=httpx.MockTransport(mock_request_handler)) as http_client:
        yield http_client


@pytest.fixture(autouse=True)
def monkeypatch_background_dependencies(
    monkeypatch: pytest.MonkeyPatch,
    mock_rgw_admin: MockRGWAdmin,
    mock_client: httpx.AsyncClient,
) -> None:
    """
    Fixture to eliminate the dependency of a mock OIDC service
    """

    @asynccontextmanager
    async def get_http_client() -> AsyncIterator[httpx.AsyncClient]:
        yield mock_client

    async def authorize_mock_redirect(*args: Any, **kwargs: Any) -> RedirectResponse:
        return RedirectResponse(
            url=str(settings.lifescience_oidc.server_metadata_url),
            status_code=status.HTTP_302_FOUND,
        )

    monkeypatch.setattr(OIDCClient, "authorize_redirect", authorize_mock_redirect)
    monkeypatch.setattr(services, "get_rgw_admin", lambda: mock_rgw_admin)
    monkeypatch.setattr(background_dependencies, "get_background_http_client", get_http_client)


@pytest_asyncio.fixture(scope="module")
async def client(
    mock_rgw_admin: MockRGWAdmin,
    mock_client: httpx.AsyncClient,
    mock_slurm_cluster: MockSlurmCluster,
    mock_s3_service: S3ServiceResource,
    db: AsyncSession,
) -> AsyncIterator[httpx.AsyncClient]:
    """
    Fixture for creating a TestClient and perform HTTP Request on it.
    Overrides the dependency for the RGW admin operations.
    """
    app.dependency_overrides[get_rgw_admin] = lambda: mock_rgw_admin
    app.dependency_overrides[get_httpx_client] = lambda: mock_client
    app.dependency_overrides[get_s3_resource] = lambda: mock_s3_service
    app.dependency_overrides[get_oidc_client] = lambda: OIDCClient()
    async with httpx.AsyncClient(transport=httpx.ASGITransport(app=app), base_url="http://localhost") as ac:  # type: ignore[arg-type]
        yield ac
    app.dependency_overrides = {}


@pytest_asyncio.fixture(scope="module")
async def db() -> AsyncIterator[AsyncSession]:
    """
    Fixture for creating a database session to connect to.
    """
    async_engine = create_async_engine(str(settings.db.dsn_async), echo=settings.db.verbose, pool_recycle=3600)
    async_session_maker = async_sessionmaker(async_engine, expire_on_commit=False)
    async with async_session_maker() as session:
        await RoleIdMapping().load_role_ids(db=session)
        yield session
        await session.reset()
        await session.execute(delete(News))
        await session.commit()
    await async_engine.dispose()


@pytest_asyncio.fixture(scope="function")
async def random_user(
    db: AsyncSession,
    mock_rgw_admin: MockRGWAdmin,
) -> AsyncIterator[UserWithAuthCookie]:
    """
    Create a random user and deletes him afterward.
    """
    user = await create_random_user(roles=[Role.RoleEnum.USER, Role.RoleEnum.ADMINISTRATOR], db=db)
    mock_rgw_admin.create_user(uid=str(user.uid), display_name=user.display_name)
    yield UserWithAuthCookie(user=user, auth_cookie=get_authorization_cookies(uid=user.uid))
    mock_rgw_admin.remove_user(uid=str(user.uid))
    await db.delete(user)
    await db.commit()


@pytest.fixture(scope="function", params=[provider for provider in OIDCProvider])
def oidc_provider(request: pytest.FixtureRequest) -> OIDCProvider:
    """
    Create an iterable fixture for every OIDC provider.
    """
    return request.param


@pytest_asyncio.fixture(scope="function")
async def random_user_with_provider(
    db: AsyncSession, random_user: UserWithAuthCookie, oidc_provider: OIDCProvider
) -> UserWithAuthCookie:
    """
    Create a random user and deletes him afterward.
    """
    # Set every provider ID to None that is already registered
    new_provider_ids: dict[str, str | None] = {
        random_user.user.provider_id_column(loop_provider).key: None
        for loop_provider in random_user.user.registered_providers
    }
    # set the requested provider id to a new one
    new_provider_ids[random_user.user.provider_id_column(oidc_provider).key] = random_lower_string()
    await db.execute(update(User).where(User.uid == random_user.uid).values(**new_provider_ids))
    await db.commit()
    await db.refresh(random_user.user)
    return random_user


@pytest_asyncio.fixture(scope="function")
async def random_api_token(
    db: AsyncSession,
    random_user: UserWithAuthCookie,
) -> ApiToken:
    """
    Create a api token for the user.
    """
    # create token with all possible scopes
    token = ApiToken(
        scopes=ApiTokenScopesDB((2 << len(ApiTokenScopesDB) - 1) - 1), uid=random_user.uid, name=random_lower_string(8)
    )
    db.add(token)
    await db.commit()
    return token


@pytest_asyncio.fixture(scope="function")
async def random_second_api_token(
    db: AsyncSession,
    random_second_user: UserWithAuthCookie,
) -> ApiToken:
    """
    Create a api token for a second user.
    """
    # create token with all possible scopes
    token = ApiToken(
        scopes=ApiTokenScopesDB((2 << len(ApiTokenScopesDB) - 1) - 1),
        uid=random_second_user.uid,
        name=random_lower_string(8),
    )
    db.add(token)
    await db.commit()
    return token


@pytest_asyncio.fixture(scope="function")
async def random_second_user(db: AsyncSession, mock_rgw_admin: MockRGWAdmin) -> AsyncIterator[UserWithAuthCookie]:
    """
    Create a random second user and deletes him afterward.
    """
    user = await create_random_user(roles=[Role.RoleEnum.USER, Role.RoleEnum.DEVELOPER], db=db)
    mock_rgw_admin.create_user(uid=str(user.uid), display_name=user.display_name)
    yield UserWithAuthCookie(user=user, auth_cookie=get_authorization_cookies(uid=user.uid))
    mock_rgw_admin.remove_user(uid=str(user.uid))
    await db.delete(user)
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def random_third_user(db: AsyncSession, mock_rgw_admin: MockRGWAdmin) -> AsyncIterator[UserWithAuthCookie]:
    """
    Create a random third user and deletes him afterwards.
    """
    user = await create_random_user(roles=None, db=db)
    mock_rgw_admin.create_user(uid=str(user.uid), display_name=user.display_name)
    yield UserWithAuthCookie(user=user, auth_cookie=get_authorization_cookies(uid=user.uid))
    mock_rgw_admin.remove_user(uid=str(user.uid))
    await db.delete(user)
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def random_bucket(
    db: AsyncSession, random_user: UserWithAuthCookie, mock_s3_service: S3ServiceResource
) -> AsyncIterator[Bucket]:
    """
    Create a random bucket and deletes him afterwards.
    """
    bucket = Bucket(name=random_lower_string(), description=random_lower_string(), owner_id=random_user.uid)
    db.add(bucket)
    await db.commit()
    s3_bucket = await mock_s3_service.Bucket(bucket.name)
    await s3_bucket.create()
    s3_policy = await mock_s3_service.BucketPolicy(bucket.name)
    await s3_policy.put(
        Policy=json.dumps(
            {
                "Version": "2012-10-17",
                "Statement": [
                    {
                        "Sid": "PseudoOwnerPerm",
                        "Effect": "Allow",
                        "Principal": {"AWS": f"arn:aws:iam:::user/{str(random_user.uid)}"},
                        "Action": ["s3:GetObject", "s3:DeleteObject", "s3:PutObject", "s3:ListBucket"],
                        "Resource": [f"arn:aws:s3:::{bucket.name}/*", f"arn:aws:s3:::{bucket.name}"],
                    }
                ],
            }
        )
    )
    yield bucket
    try:
        await s3_bucket.objects.delete()
        await s3_bucket.delete()
    except ClientError:
        pass
    await db.delete(bucket)
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def random_bucket_permission(
    db: AsyncSession,
    random_second_user: UserWithAuthCookie,
    random_bucket: Bucket,
    mock_s3_service: S3ServiceResource,
) -> BucketPermission:
    """
    Create a bucket READ permission for the second user on a bucket.
    """
    permission_db = BucketPermission(uid=random_second_user.uid, bucket_name=random_bucket.name)
    db.add(permission_db)
    await db.commit()
    await db.refresh(permission_db)
    s3_policy = await mock_s3_service.BucketPolicy(bucket_name=random_bucket.name)
    await s3_policy.put(
        Policy=json.dumps(
            {
                "Version": "2012-10-17",
                "Statement": BucketPermissionSchema.from_db_model(permission_db).map_to_bucket_policy_statement(),
            }
        )
    )
    return permission_db


@pytest_asyncio.fixture(scope="function")
async def cleanup(db: AsyncSession) -> AsyncIterator[CleanupList]:
    """
    Yields a Cleanup object where (async) functions can be registered which get executed after a (failed) test
    """
    cleanup_list = CleanupList()
    yield cleanup_list
    await cleanup_list.empty_queue()


@pytest_asyncio.fixture(scope="function")
async def random_workflow(
    db: AsyncSession, random_user: UserWithAuthCookie, mock_s3_service: S3ServiceResource
) -> AsyncIterator[WorkflowOut]:
    """
    Create a random workflow and deletes it afterwards.
    """
    workflow_db = Workflow(
        name=random_lower_string(10),
        repository_url="https://github.de/example-user/example",
        short_description=random_lower_string(65),
        developer_id=random_user.uid,
    )
    db.add(workflow_db)
    await db.commit()
    icon_slug = f"{uuid4().hex}.png"
    workflow_version = WorkflowVersion(
        git_commit_hash=random_hex_string(40),
        version="v1.0.0",
        workflow_id=workflow_db.workflow_id,
        icon_slug=icon_slug,
        nextflow_version="23.04.5",
    )
    db.add(workflow_version)
    await db.commit()
    icon_obj = await mock_s3_service.Object(bucket_name=settings.s3.data_bucket, key=settings.s3.icon_key(icon_slug))
    await icon_obj.upload_fileobj(BytesIO(b"{}"))
    schema_obj = await mock_s3_service.Object(
        bucket_name=settings.s3.data_bucket,
        key=settings.s3.workflow_cache_key(
            name=DocumentationEnum.PARAMETER_SCHEMA, git_commit_hash=workflow_version.git_commit_hash
        ),
    )
    await schema_obj.upload_fileobj(BytesIO(b"{}"))
    yield WorkflowOut.from_db_workflow(db_workflow=workflow_db, versions=[workflow_version], load_modes=False)
    await icon_obj.delete()
    await schema_obj.delete()
    await delete_workflow(db=db, workflow_id=workflow_db.workflow_id)


@pytest_asyncio.fixture(scope="function")
async def random_private_workflow(
    db: AsyncSession,
    random_workflow: WorkflowOut,
    random_workflow_version: WorkflowVersion,
    mock_s3_service: S3ServiceResource,
) -> AsyncIterator[WorkflowOut]:
    """
    Transform the random workflow into a private workflow.
    """
    # Update credentials in Database
    token = random_lower_string(15)
    await db.execute(
        update(Workflow).where(Workflow.workflow_id == random_workflow.workflow_id).values(credentials_token=token)
    )
    await db.commit()

    # Upload SCM file to data_BUCKET
    scm_provider = SCMProvider.from_repo(
        build_repository(str(random_workflow.repository_url), random_workflow_version.git_commit_hash, token=token),
        name=SCMProvider.generate_name(random_workflow.workflow_id),
    )
    assert scm_provider is not None
    scm_obj = await mock_s3_service.Object(
        bucket_name=settings.s3.data_bucket, key=settings.s3.scm_key(random_workflow.workflow_id)
    )
    with BytesIO() as f:
        SCM([scm_provider]).serialize(f)
        f.seek(0)
        await scm_obj.upload_fileobj(f)
    yield random_workflow
    # Delete SCM in PARAMS_BUCKET after tests
    await scm_obj.delete()


@pytest_asyncio.fixture(scope="function")
async def random_workflow_version(db: AsyncSession, random_workflow: WorkflowOut) -> WorkflowVersion:
    """
    Create a random workflow version. Will be deleted, when the workflow is deleted.
    """
    stmt = select(WorkflowVersion).where(
        WorkflowVersion.git_commit_hash == random_workflow.versions[0].workflow_version_id
    )
    version = await db.scalar(stmt)
    assert version is not None
    return version


@pytest_asyncio.fixture(scope="function")
async def random_running_workflow_execution(
    db: AsyncSession,
    random_workflow_version: WorkflowVersion,
    random_user: UserWithAuthCookie,
    mock_s3_service: S3ServiceResource,
    mock_slurm_cluster: MockSlurmCluster,
) -> AsyncIterator[WorkflowExecution]:
    """
    Create a random running workflow execution. Will be deleted, when the user is deleted.
    """
    execution = WorkflowExecution(
        executor_id=random_user.uid,
        workflow_version_id=random_workflow_version.git_commit_hash,
        slurm_job_id=0,
        notes="conftest",
    )
    db.add(execution)
    await db.commit()
    slurm_job_id = mock_slurm_cluster.add_workflow_execution({"job": {"name": str(execution.execution_id)}})
    await db.execute(
        update(WorkflowExecution)
        .where(WorkflowExecution.execution_id == execution.execution_id)
        .values(slurm_job_id=slurm_job_id)
    )
    await db.commit()
    parameter_obj = await mock_s3_service.Object(
        bucket_name=settings.s3.data_bucket, key=settings.s3.execution_parameters_key(execution.execution_id)
    )
    await parameter_obj.upload_fileobj(BytesIO(b"{}"))
    yield execution
    await parameter_obj.delete()
    await db.execute(delete(WorkflowExecution).where(WorkflowExecution.execution_id == execution.execution_id))


@pytest_asyncio.fixture(scope="function")
async def random_completed_workflow_execution(
    db: AsyncSession, random_running_workflow_execution: WorkflowExecution
) -> WorkflowExecution:
    """
    Create a random workflow execution which is completed.
    """

    await db.execute(
        update(WorkflowExecution)
        .where(WorkflowExecution.execution_id == random_running_workflow_execution.execution_id)
        .values(end_time=round(time.time()), status=WorkflowExecution.WorkflowExecutionStatus.SUCCESS)
    )
    await db.commit()
    return random_running_workflow_execution


@pytest_asyncio.fixture(scope="function")
async def random_workflow_mode(
    db: AsyncSession,
    random_workflow_version: WorkflowVersion,
    mock_s3_service: S3ServiceResource,
) -> AsyncIterator[WorkflowMode]:
    """
    Create a random workflow execution. Will be deleted, when the user is deleted.
    """
    mode = WorkflowMode(name=random_lower_string(8), schema_path="alternative.json", entrypoint=random_lower_string(16))
    db.add(mode)
    await db.commit()
    await db.execute(
        insert(workflow_mode_association_table),
        [
            {
                "workflow_version_commit_hash": random_workflow_version.git_commit_hash,
                "workflow_mode_id": mode.mode_id,
            }
        ],
    )
    await db.commit()
    schema_obj = await mock_s3_service.Object(
        bucket_name=settings.s3.data_bucket,
        key=settings.s3.workflow_cache_key(
            name=DocumentationEnum.PARAMETER_SCHEMA,
            git_commit_hash=random_workflow_version.git_commit_hash,
            mode_id=mode.mode_id,
        ),
    )
    await schema_obj.upload_fileobj(BytesIO(b"{}"))
    yield mode
    await schema_obj.delete()
    await db.delete(mode)
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def random_resource(
    db: AsyncSession,
    random_user: UserWithAuthCookie,
    mock_s3_service: S3ServiceResource,
    cleanup: CleanupList,
) -> AsyncIterator[Resource]:
    """
    Create a random resource and deletes it afterward.
    """
    resource_db = Resource(
        name=random_lower_string(8),
        short_description=random_lower_string(32),
        source=random_lower_string(32),
        maintainer_id=random_user.uid,
    )
    db.add(resource_db)
    await db.commit()
    resource_version_db = ResourceVersion(
        release=random_lower_string(8),
        resource_id=resource_db.resource_id,
        status=ResourceVersion.ResourceVersionStatus.LATEST,
    )
    db.add(resource_version_db)
    await db.commit()
    await db.refresh(resource_db, attribute_names=["versions"])

    s3_policy = await mock_s3_service.BucketPolicy(bucket_name=settings.s3.data_bucket)
    policy = json.loads(await s3_policy.policy)
    policy["Statement"].append(
        {
            "Sid": str(resource_db.resource_id),
            "Effect": "Allow",
            "Principal": {"AWS": f"arn:aws:iam:::user/{str(resource_db.maintainer_id)}"},
            "Action": ["s3:GetObject"],
            "Resource": [f"arn:aws:s3:::{settings.s3.data_bucket}/{settings.s3.resource_key('')}*"],
        }
    )
    await s3_policy.put(Policy=json.dumps(policy))

    async def remove_resource_policy() -> None:
        policy["Statement"] = [stmt for stmt in policy["Statement"] if stmt["Sid"] != str(resource_db.resource_id)]
        await s3_policy.put(Policy=json.dumps(policy))

    cleanup.add_task(remove_resource_policy)

    yield resource_db
    await db.execute(delete(Resource).where(Resource.resource_id == resource_db.resource_id))
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def random_resource_version(db: AsyncSession, random_resource: Resource) -> ResourceVersion:
    """
    Create a random resource version and deletes it afterward.
    """
    resource_version: ResourceVersion = random_resource.versions[0]
    return resource_version


@pytest.fixture(scope="function", params=[status for status in ResourceVersion.ResourceVersionStatus])
def resource_state(request: pytest.FixtureRequest) -> ResourceVersion.ResourceVersionStatus:
    """
    Return all possible resource version sates as fixture
    """
    return request.param


@pytest_asyncio.fixture(scope="function")
async def random_resource_version_states(
    db: AsyncSession,
    random_resource: Resource,
    resource_state: ResourceVersion.ResourceVersionStatus,
    mock_s3_service: S3ServiceResource,
    cleanup: CleanupList,
) -> ResourceVersion:
    """
    Create a random resource version with all possible resource version status.
    """
    resource_version: ResourceVersion = random_resource.versions[0]
    stmt = (
        update(ResourceVersion)
        .where(ResourceVersion.resource_version_id == resource_version.resource_version_id)
        .values(status=resource_state)
    )
    if resource_state is ResourceVersion.ResourceVersionStatus.SYNC_REQUESTED:
        stmt = stmt.values(
            synchronization_request_description=random_lower_string(16),
            synchronization_request_uid=random_resource.maintainer_id,
        )
    await db.execute(stmt)
    await db.commit()
    await db.refresh(resource_version, attribute_names=["status"])

    if resource_state is ResourceVersion.ResourceVersionStatus.RESOURCE_REQUESTED:
        # create a permission for the maintainer to upload the resource to S3
        s3_policy = await mock_s3_service.BucketPolicy(settings.s3.data_bucket)
        policy = json.loads(await s3_policy.policy)
        policy["Statement"].append(
            {
                "Sid": str(resource_version.resource_version_id),
                "Effect": "Allow",
                "Principal": {"AWS": f"arn:aws:iam:::user/{str(random_resource.maintainer_id)}"},
                "Action": ["s3:GetObject"],
                "Resource": [f"arn:aws:s3:::{settings.s3.data_bucket}/{settings.s3.resource_key('')}*"],
            }
        )
        await s3_policy.put(Policy=json.dumps(policy))

        # delete policy statement after the test
        async def delete_policy_stmt() -> None:
            policy["Statement"] = [
                s3_stmt
                for s3_stmt in policy["Statement"]
                if s3_stmt["Sid"] != str(resource_version.resource_version_id)
            ]
            await s3_policy.put(Policy=json.dumps(policy))

        cleanup.add_task(delete_policy_stmt)

    if resource_state in [
        ResourceVersion.ResourceVersionStatus.WAIT_FOR_REVIEW,
        ResourceVersion.ResourceVersionStatus.SYNC_REQUESTED,
        ResourceVersion.ResourceVersionStatus.SYNCHRONIZING,
        ResourceVersion.ResourceVersionStatus.SYNC_ERROR,
        ResourceVersion.ResourceVersionStatus.SYNCHRONIZED,
        ResourceVersion.ResourceVersionStatus.LATEST,
        ResourceVersion.ResourceVersionStatus.DENIED,
        ResourceVersion.ResourceVersionStatus.APPROVED,
    ]:
        # create the resource object in S3 for appropriate resource version states
        resource_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket,
            key=resource_version_key(random_resource.resource_id, random_resource.versions[0].resource_version_id),
        )
        await resource_obj.upload_fileobj(BytesIO(b"content"))
        cleanup.add_task(resource_obj.delete)

    if resource_state in [
        ResourceVersion.ResourceVersionStatus.SYNCHRONIZED,
        ResourceVersion.ResourceVersionStatus.LATEST,
        ResourceVersion.ResourceVersionStatus.SETTING_LATEST,
    ]:
        # create the resource object in S3 for appropriate resource version states
        tree_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket,
            key=resource_version_dir_name(random_resource.resource_id, random_resource.versions[0].resource_version_id)
            + "/tree.json",
        )
        await tree_obj.upload_fileobj(
            BytesIO(json.dumps([FileTree(type="file", name="some/path", size=10).model_dump()]).encode("utf-8")),
        )
        cleanup.add_task(tree_obj.delete)

    return resource_version
