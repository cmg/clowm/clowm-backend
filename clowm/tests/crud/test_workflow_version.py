from uuid import uuid4

import pytest
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload

from clowm.crud import CRUDWorkflowVersion
from clowm.models import WorkflowMode, WorkflowVersion
from clowm.schemas.workflow import WorkflowOut
from clowm.schemas.workflow_version import NextflowVersion, ParameterExtension

from ..utils import random_hex_string, random_lower_string


class TestWorkflowVersionCRUDGet:
    @pytest.mark.asyncio
    async def test_get_specific_workflow_version(
        self, db: AsyncSession, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for getting a workflow version by its id from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowm.model.WorkflowVersion
            Random workflow version for testing.
        """
        version = await CRUDWorkflowVersion(db=db).get(
            random_workflow_version.git_commit_hash,
        )
        assert version is not None
        assert version.workflow_id == random_workflow_version.workflow_id
        assert version.git_commit_hash == random_workflow_version.git_commit_hash

    @pytest.mark.asyncio
    async def test_get_specific_workflow_version_with_workflow_id(
        self, db: AsyncSession, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for getting a workflow version and constraint it by a workflow ID from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowm.model.WorkflowVersion
            Random workflow version for testing.
        """
        version = await CRUDWorkflowVersion(db=db).get(
            random_workflow_version.git_commit_hash,
            workflow_id=random_workflow_version.workflow_id,
        )
        assert version is not None
        assert version.workflow_id == random_workflow_version.workflow_id
        assert version.git_commit_hash == random_workflow_version.git_commit_hash

    @pytest.mark.asyncio
    async def test_get_specific_workflow_version_with_false_workflow_id(
        self, db: AsyncSession, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for getting a workflow version and constraint it with a non-existing workflow ID CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowm.model.WorkflowVersion
            Random workflow version for testing.
        """
        version = await CRUDWorkflowVersion(db=db).get(
            random_workflow_version.git_commit_hash,
            workflow_id=uuid4(),
        )
        assert version is None

    @pytest.mark.asyncio
    async def test_get_specific_workflow_version_with_populated_workflow(
        self, db: AsyncSession, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for getting a workflow version by its id with populated workflow from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowm.model.WorkflowVersion
            Random workflow version for testing.
        """
        version = await CRUDWorkflowVersion(db=db).get(
            random_workflow_version.git_commit_hash,
            populate_workflow=True,
        )
        assert version is not None
        assert version.workflow_id == random_workflow_version.workflow_id
        assert version.git_commit_hash == random_workflow_version.git_commit_hash
        assert version.workflow.workflow_id == random_workflow_version.workflow_id

    @pytest.mark.asyncio
    async def test_get_latest_unpublished_workflow_version(
        self, db: AsyncSession, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for getting the latest workflow version from a workflow which is not published from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowm.model.WorkflowVersion
            Random workflow version for testing.
        """
        workflow_version = await CRUDWorkflowVersion(db=db).get_latest(
            random_workflow_version.workflow_id,
            published=False,
        )
        assert workflow_version is not None
        assert workflow_version.workflow_id == random_workflow_version.workflow_id
        assert workflow_version.git_commit_hash == random_workflow_version.git_commit_hash

    @pytest.mark.asyncio
    async def test_get_latest_published_workflow_version(
        self, db: AsyncSession, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for getting the latest workflow version from a workflow which is published from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowm.model.WorkflowVersion
            Random workflow version for testing.
        """
        workflow_version = await CRUDWorkflowVersion(db=db).get_latest(
            random_workflow_version.workflow_id,
        )
        assert workflow_version is None

    @pytest.mark.asyncio
    async def test_get_all_workflow_versions(self, db: AsyncSession, random_workflow_version: WorkflowVersion) -> None:
        """
        Test for getting all versions from a workflow from the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowm.model.WorkflowVersion
            Random workflow version for testing.
        """
        workflow_versions = await CRUDWorkflowVersion(db=db).list_workflow_versions(
            random_workflow_version.workflow_id,
        )
        assert len(workflow_versions) == 1
        assert workflow_versions[0].git_commit_hash == random_workflow_version.git_commit_hash

    @pytest.mark.asyncio
    async def test_get_all_workflow_version_with_specific_status(
        self, db: AsyncSession, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for getting all versions with a specific status from a workflow from the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowm.model.WorkflowVersion
            Random workflow version for testing.
        """
        workflow_versions = await CRUDWorkflowVersion(db=db).list_workflow_versions(
            random_workflow_version.workflow_id,
            version_status=[random_workflow_version.status],
        )
        assert len(workflow_versions) == 1
        assert workflow_versions[0].git_commit_hash == random_workflow_version.git_commit_hash


class TestWorkflowVersionCRUDCreate:
    @pytest.mark.asyncio
    async def test_create_workflow_version(self, db: AsyncSession, random_workflow: WorkflowOut) -> None:
        """
        Test for creating a workflow version in CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random bucket for testing.
        """
        workflow_version = await CRUDWorkflowVersion(db=db).create(
            git_commit_hash=random_hex_string(),
            version="v2.0.0",
            workflow_id=random_workflow.workflow_id,
            previous_version=random_workflow.versions[-1].workflow_version_id,
            nextflow_version="23.04.5",
        )
        assert workflow_version is not None

        stmt = (
            select(WorkflowVersion)
            .where(WorkflowVersion.git_commit_hash == workflow_version.git_commit_hash)
            .options(selectinload(WorkflowVersion.workflow_modes))
        )
        created_workflow_version = await db.scalar(stmt)
        assert created_workflow_version is not None
        assert created_workflow_version == workflow_version
        assert len(created_workflow_version.workflow_modes) == 0

    @pytest.mark.asyncio
    async def test_create_workflow_version_with_mode(
        self, db: AsyncSession, random_workflow: WorkflowOut, random_workflow_mode: WorkflowMode
    ) -> None:
        """
        Test for creating a workflow version in CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random bucket for testing.
        random_workflow_mode : clowm.models.WorkflowMode
            Random workflow mode for testing.
        """
        workflow_version = await CRUDWorkflowVersion(db=db).create(
            git_commit_hash=random_hex_string(),
            version="v2.0.0",
            workflow_id=random_workflow.workflow_id,
            previous_version=random_workflow.versions[-1].workflow_version_id,
            modes=[random_workflow_mode.mode_id],
            nextflow_version="23.04.5",
        )
        assert workflow_version is not None

        stmt = (
            select(WorkflowVersion)
            .where(WorkflowVersion.git_commit_hash == workflow_version.git_commit_hash)
            .options(selectinload(WorkflowVersion.workflow_modes))
        )
        created_workflow_version = await db.scalar(stmt)
        assert created_workflow_version is not None
        assert created_workflow_version == workflow_version
        assert len(created_workflow_version.workflow_modes) == 1
        assert created_workflow_version.workflow_modes[0].mode_id == random_workflow_mode.mode_id


class TestWorkflowVersionCRUDUpdate:
    @pytest.mark.asyncio
    async def test_update_workflow_version_status(
        self, db: AsyncSession, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for upadting a workflow version icon in CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowm.model.WorkflowVersion
            Random workflow version for testing.
        """
        await CRUDWorkflowVersion(db=db).update_status(
            workflow_version_id=random_workflow_version.git_commit_hash,
            status=WorkflowVersion.WorkflowVersionStatus.PUBLISHED,
        )

        stmt = select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == random_workflow_version.git_commit_hash)
        version = await db.scalar(stmt)
        assert version
        assert version.status == WorkflowVersion.WorkflowVersionStatus.PUBLISHED

    @pytest.mark.asyncio
    async def test_update_workflow_version_parameter_extension(
        self, db: AsyncSession, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for updating a workflow version parameter extension in CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowm.model.WorkflowVersion
            Random workflow version for testing.
        """
        await CRUDWorkflowVersion(db=db).update_parameter_extension(
            workflow_version_id=random_workflow_version.git_commit_hash, parameter_extension=ParameterExtension()
        )

        stmt = select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == random_workflow_version.git_commit_hash)
        version = await db.scalar(stmt)
        assert version
        assert version.parameter_extension is not None

    @pytest.mark.asyncio
    async def test_update_workflow_version_icon(
        self, db: AsyncSession, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for updating the worklfow version icon

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowm.model.WorkflowVersion
            Random workflow version for testing.
        """
        new_slug = random_hex_string()
        await CRUDWorkflowVersion(db=db).update_icon(
            workflow_version_id=random_workflow_version.git_commit_hash, icon_slug=new_slug
        )

        stmt = select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == random_workflow_version.git_commit_hash)
        version = await db.scalar(stmt)
        assert version
        assert version.git_commit_hash == random_workflow_version.git_commit_hash
        assert version.icon_slug == new_slug

    @pytest.mark.asyncio
    async def test_remove_workflow_version_icon(
        self, db: AsyncSession, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for removing the workflow version icon

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowm.model.WorkflowVersion
            Random workflow version for testing.
        """
        await CRUDWorkflowVersion(db=db).update_icon(
            workflow_version_id=random_workflow_version.git_commit_hash, icon_slug=None
        )

        stmt = select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == random_workflow_version.git_commit_hash)
        version = await db.scalar(stmt)
        assert version
        assert version.git_commit_hash == random_workflow_version.git_commit_hash
        assert version.icon_slug is None

    @pytest.mark.asyncio
    async def test_update_meta_data(self, db: AsyncSession, random_workflow_version: WorkflowVersion) -> None:
        """
        Test for updating the meta-data

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowm.model.WorkflowVersion
            Random workflow version for testing.
        """
        default_container = f"{random_lower_string(10)}:{random_lower_string(4)}"
        nextflow_config = random_lower_string(128)
        await CRUDWorkflowVersion(db=db).update_meta_data(
            workflow_version_id=random_workflow_version.git_commit_hash,
            default_container=default_container,
            nextflow_config=nextflow_config,
            nextflow_version=NextflowVersion.v22_10_0,
        )

        stmt = select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == random_workflow_version.git_commit_hash)
        version = await db.scalar(stmt)
        assert version
        assert version.git_commit_hash == random_workflow_version.git_commit_hash
        assert version.default_container == default_container
        assert version.nextflow_config == nextflow_config
        assert version.nextflow_version == NextflowVersion.v22_10_0

        await CRUDWorkflowVersion(db=db).update_meta_data(
            workflow_version_id=random_workflow_version.git_commit_hash,
            default_container=None,
            nextflow_config=None,
            nextflow_version=NextflowVersion.v24_04_4,
        )

        stmt = select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == random_workflow_version.git_commit_hash)
        version = await db.scalar(stmt)
        assert version
        assert version.git_commit_hash == random_workflow_version.git_commit_hash
        assert version.default_container is None
        assert version.nextflow_config is None
        assert version.nextflow_version == NextflowVersion.v24_04_4


class TestWorkflowVersionCRUDCheck:
    @pytest.mark.asyncio
    async def test_check_icon_dependency(self, db: AsyncSession, random_workflow_version: WorkflowVersion) -> None:
        """
        Test for checking if a workflow version is dependent on an icon

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowm.model.WorkflowVersion
            Random workflow version for testing.
        """
        assert random_workflow_version.icon_slug is not None
        dependent1 = await CRUDWorkflowVersion(db=db).icon_exists(
            random_workflow_version.icon_slug,
        )

        assert dependent1

        dependent2 = await CRUDWorkflowVersion(db=db).icon_exists(
            random_hex_string(),
        )
        assert not dependent2
