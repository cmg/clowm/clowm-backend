import time
from datetime import timedelta
from uuid import uuid4

import pytest
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.crud import CRUDWorkflowExecution
from clowm.models import WorkflowExecution, WorkflowVersion
from clowm.schemas.workflow_execution import DevWorkflowExecutionIn, WorkflowExecutionIn
from clowm.tests.utils import CleanupList, UserWithAuthCookie, delete_workflow_execution, random_hex_string


class TestWorkflowExecutionCRUDCreate:
    @pytest.mark.asyncio
    async def test_create_workflow_execution(
        self,
        db: AsyncSession,
        random_workflow_version: WorkflowVersion,
        random_user: UserWithAuthCookie,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for creating a workflow execution with the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowm.models.WorkflowVersion
            Random workflow version for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        workflow_execution_in = WorkflowExecutionIn(
            workflow_version_id=random_workflow_version.git_commit_hash,
            parameters={},
        )
        workflow_execution = await CRUDWorkflowExecution(db=db).create(
            workflow_execution_in,
            random_user.uid,
        )
        assert workflow_execution
        cleanup.add_task(delete_workflow_execution, execution_id=workflow_execution.execution_id, db=db)
        assert workflow_execution.executor_id == random_user.uid
        assert workflow_execution.workflow_version_id == random_workflow_version.git_commit_hash
        assert workflow_execution.status == WorkflowExecution.WorkflowExecutionStatus.PENDING

        workflow_execution_db = await db.scalar(
            select(WorkflowExecution).where(WorkflowExecution.execution_id == workflow_execution.execution_id)
        )

        assert workflow_execution_db
        assert workflow_execution_db.executor_id == random_user.uid
        assert workflow_execution_db.workflow_version_id == random_workflow_version.git_commit_hash
        assert workflow_execution_db.status == WorkflowExecution.WorkflowExecutionStatus.PENDING

    @pytest.mark.asyncio
    async def test_create_dev_workflow_execution(
        self, db: AsyncSession, random_user: UserWithAuthCookie, cleanup: CleanupList
    ) -> None:
        """
        Test for creating a dev workflow execution with the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        workflow_execution_in = DevWorkflowExecutionIn(
            git_commit_hash=random_hex_string(),
            repository_url="https://example.com",
            parameters={},
            nextflow_version="23.04.5",
        )
        workflow_execution = await CRUDWorkflowExecution(db=db).create(
            workflow_execution_in,
            random_user.uid,
        )
        assert workflow_execution
        cleanup.add_task(delete_workflow_execution, execution_id=workflow_execution.execution_id, db=db)
        assert workflow_execution.executor_id == random_user.uid
        assert workflow_execution.status == WorkflowExecution.WorkflowExecutionStatus.PENDING

        workflow_execution_db = await db.scalar(
            select(WorkflowExecution).where(WorkflowExecution.execution_id == workflow_execution.execution_id)
        )
        assert workflow_execution_db
        assert workflow_execution_db.executor_id == random_user.uid
        assert workflow_execution_db.status == WorkflowExecution.WorkflowExecutionStatus.PENDING


class TestWorkflowExecutionCRUDGet:
    @pytest.mark.asyncio
    async def test_get_workflow_execution(
        self, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for getting a workflow execution by its execution id.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        execution = await CRUDWorkflowExecution(db=db).get(
            random_running_workflow_execution.execution_id,
        )
        assert execution is not None
        assert execution == random_running_workflow_execution

    @pytest.mark.asyncio
    async def test_get_non_existing_workflow_execution(self, db: AsyncSession) -> None:
        """
        Test for getting a non-existing workflow execution.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        execution = await CRUDWorkflowExecution(db=db).get(
            uuid4(),
        )
        assert execution is None

    @pytest.mark.asyncio
    async def test_get_workflow_execution_by_id_fragment(
        self, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for getting a workflow execution by its execution id.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on. pytest fixture.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing. pytest fixture.
        """
        execution = await CRUDWorkflowExecution(db=db).get_by_id_fragment(
            execution_id_start=random_running_workflow_execution.execution_id.hex[:16]
        )
        assert execution is not None
        assert execution == random_running_workflow_execution

    @pytest.mark.asyncio
    async def test_get_non_existing_workflow_execution_by_id_fragment(self, db: AsyncSession) -> None:
        """
        Test for getting a non-existing workflow execution.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on. pytest fixture.
        """
        execution = await CRUDWorkflowExecution(db=db).get_by_id_fragment(execution_id_start=random_hex_string(16))
        assert execution is None


class TestWorkflowExecutionCRUDList:
    @pytest.mark.asyncio
    async def test_list_workflow_executions(
        self, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for listing all workflow executions.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        executions = await CRUDWorkflowExecution(db=db).list(
            start_after=0,
            start_before=round(time.time()) + 1000,
            id_after=random_running_workflow_execution.execution_id,
        )
        assert len(executions) > 0
        assert sum(1 for execution in executions if execution == random_running_workflow_execution) == 1

    @pytest.mark.asyncio
    async def test_get_list_workflow_executions_of_user(
        self, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for listing all workflow executions and filter by user.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        executions = await CRUDWorkflowExecution(db=db).list(executor_id=random_running_workflow_execution.executor_id)
        assert len(executions) > 0
        assert sum(1 for execution in executions if execution == random_running_workflow_execution) == 1
        assert (
            sum(1 for execution in executions if execution.executor_id == random_running_workflow_execution.executor_id)
            >= 1
        )

    @pytest.mark.asyncio
    async def test_get_list_workflow_executions_of_non_existing_user(
        self, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for listing all workflow executions and filter by non-existing user.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        executions = await CRUDWorkflowExecution(db=db).list(executor_id=uuid4())
        assert len(executions) == 0
        assert sum(1 for execution in executions if execution == random_running_workflow_execution) == 0

    @pytest.mark.asyncio
    async def test_get_list_workflow_executions_of_workflow_version(
        self,
        db: AsyncSession,
        random_workflow_version: WorkflowVersion,
        random_running_workflow_execution: WorkflowExecution,
    ) -> None:
        """
        Test for listing all workflow executions and filter by workflow version id.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        executions = await CRUDWorkflowExecution(db=db).list(
            workflow_version_id=random_running_workflow_execution.workflow_version_id,
            workflow_id=random_workflow_version.workflow_id,
        )
        assert len(executions) > 0
        assert sum(1 for execution in executions if execution == random_running_workflow_execution) == 1
        assert (
            sum(
                1
                for execution in executions
                if execution.workflow_version_id == random_running_workflow_execution.workflow_version_id
            )
            >= 1
        )

    @pytest.mark.asyncio
    async def test_get_list_workflow_executions_of_non_existing_workflow_version(
        self, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for listing all workflow executions and filter by non-existing workflow version id.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        executions = await CRUDWorkflowExecution(db=db).list(workflow_version_id=random_hex_string())
        assert len(executions) == 0
        assert sum(1 for execution in executions if execution == random_running_workflow_execution) == 0

    @pytest.mark.asyncio
    async def test_get_list_workflow_executions_with_given_status(
        self, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for listing all workflow executions and filter by status.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        executions = await CRUDWorkflowExecution(db=db).list(status_list=[random_running_workflow_execution.status])
        assert len(executions) > 0
        assert sum(1 for execution in executions if execution == random_running_workflow_execution) == 1
        assert sum(1 for execution in executions if execution.status == random_running_workflow_execution.status) >= 1


class TestWorkflowExecutionCRUDUpdate:
    @pytest.mark.asyncio
    async def test_update_workflow_execution_slurm_job(
        self, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for updating the slurm job id of a workflow execution.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        await CRUDWorkflowExecution(db=db).update_slurm_job_id(
            random_running_workflow_execution.execution_id,
            250,
        )

        stmt = select(WorkflowExecution).where(
            WorkflowExecution.execution_id == random_running_workflow_execution.execution_id
        )
        execution = await db.scalar(stmt)
        assert execution is not None
        assert execution.slurm_job_id == 250

    @pytest.mark.asyncio
    async def test_update_workflow_status(
        self, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for updating the workflow execution status.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on. pytest fixture.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing. pytest fixture.
        """
        await CRUDWorkflowExecution(db=db).update_status(
            execution_id=random_running_workflow_execution.execution_id,
            status=WorkflowExecution.WorkflowExecutionStatus.RUNNING,
        )

        stmt = select(WorkflowExecution).where(
            WorkflowExecution.execution_id == random_running_workflow_execution.execution_id
        )
        execution = await db.scalar(stmt)

        assert execution is not None
        assert execution == random_running_workflow_execution
        assert execution.status == WorkflowExecution.WorkflowExecutionStatus.RUNNING

    @pytest.mark.asyncio
    async def test_update_workflow_status_with_timestamp(
        self, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for updating the workflow execution status with and end timestamp.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on. pytest fixture.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing. pytest fixture.
        """
        await CRUDWorkflowExecution(db=db).update_status(
            execution_id=random_running_workflow_execution.execution_id,
            status=WorkflowExecution.WorkflowExecutionStatus.ERROR,
            end_timestamp=round(time.time()),
        )

        stmt = select(WorkflowExecution).where(
            WorkflowExecution.execution_id == random_running_workflow_execution.execution_id
        )
        execution = await db.scalar(stmt)

        assert execution is not None
        assert execution == random_running_workflow_execution
        assert execution.status == WorkflowExecution.WorkflowExecutionStatus.ERROR
        assert execution.end_time is not None

    @pytest.mark.asyncio
    async def test_update_execution_cpu_time(
        self, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for updating the workflow execution cpu hours.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on. pytest fixture.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing. pytest fixture.
        """
        await CRUDWorkflowExecution(db=db).update_cpu_time(
            execution_id=random_running_workflow_execution.execution_id,
            additional_cpu_time=timedelta(days=1, seconds=42),
        )

        stmt = select(WorkflowExecution).where(
            WorkflowExecution.execution_id == random_running_workflow_execution.execution_id
        )
        execution = await db.scalar(stmt)

        assert execution is not None
        assert execution == random_running_workflow_execution
        assert execution.cpu_time.seconds == 42
        assert execution.cpu_time.days == 1


class TestWorkflowExecutionCRUDDelete:
    @pytest.mark.asyncio
    async def test_delete_workflow_execution(
        self, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for deleting a workflow execution.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        await CRUDWorkflowExecution(db=db).delete(
            random_running_workflow_execution.execution_id,
        )

        stmt = select(WorkflowExecution).where(
            WorkflowExecution.execution_id == random_running_workflow_execution.execution_id
        )
        execution = await db.scalar(stmt)
        assert execution is not None
        assert execution.deleted

    @pytest.mark.asyncio
    async def test_delete_workflow_executions_by_uid(
        self, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for deleting workflow executions by UID.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        assert random_running_workflow_execution.executor_id is not None
        await CRUDWorkflowExecution(db=db).delete_by_user(uid=random_running_workflow_execution.executor_id)

        stmt = select(WorkflowExecution).where(
            WorkflowExecution.executor_id == random_running_workflow_execution.executor_id
        )
        executions = (await db.scalars(stmt)).all()
        assert len(executions) > 0
        assert sum(execution.deleted for execution in executions) == len(executions)
