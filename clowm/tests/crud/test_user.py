import random
import secrets
import time
import uuid

import pytest
from sqlalchemy import delete, select, update
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload

from clowm.core.oidc import OIDCProvider, UserInfo
from clowm.crud import CRUDUser
from clowm.models import Role, User
from clowm.models.role import RoleIdMapping
from clowm.tests.utils import CleanupList, UserWithAuthCookie, random_lower_string


class TestUserCRUDCreate:
    @pytest.mark.asyncio
    async def test_create_user(self, db: AsyncSession, cleanup: CleanupList, oidc_provider: OIDCProvider) -> None:
        """
        Test for creating a user in the User CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        oidc_provider : clowm.core.oidc.OICDProvider
            One possible OIDC provider for the test.
        """
        user = UserInfo(sub=random_lower_string(), name=random_lower_string(), email=None, provider=oidc_provider)
        created_user = await CRUDUser(db=db).create(user)
        assert created_user is not None

        async def delete_user() -> None:
            await db.execute(delete(User).where(User.uid == created_user.uid))
            await db.commit()

        cleanup.add_task(delete_user)

        db_user = await db.scalar(select(User).where(User.uid == created_user.uid).options(selectinload(User.roles)))
        assert db_user
        assert db_user.uid == created_user.uid
        assert len(db_user.roles) == 0
        assert db_user.provider_id(oidc_provider) == user.sub

    @pytest.mark.asyncio
    async def test_create_user_with_roles(
        self, db: AsyncSession, cleanup: CleanupList, oidc_provider: OIDCProvider
    ) -> None:
        """
        Test for creating a user in the User CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        oidc_provider : clowm.core.oidc.OICDProvider
            One possible OIDC provider for the test.
        """
        user = UserInfo(sub=random_lower_string(), name=random_lower_string(), email=None, provider=oidc_provider)
        created_user = await CRUDUser(db=db).create(
            user,
            roles=[Role.RoleEnum.USER],
        )
        assert created_user is not None

        async def delete_user() -> None:
            await db.execute(delete(User).where(User.uid == created_user.uid))
            await db.commit()

        cleanup.add_task(delete_user)

        db_user = await db.scalar(select(User).where(User.uid == created_user.uid).options(selectinload(User.roles)))
        assert db_user
        assert db_user.uid == created_user.uid
        assert len(db_user.roles) == 1
        assert db_user.provider_id(oidc_provider) == user.sub

    @pytest.mark.asyncio
    async def test_create_user_with_empty_sub(
        self, db: AsyncSession, cleanup: CleanupList, oidc_provider: OIDCProvider
    ) -> None:
        """
        Test for creating a user in the User CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        oidc_provider : clowm.core.oidc.OICDProvider
            One possible OIDC provider for the test.
        """
        user = UserInfo(sub="", name=random_lower_string(), email=None, provider=oidc_provider)
        created_user = await CRUDUser(db=db).create(user)
        assert created_user is not None

        async def delete_user() -> None:
            await db.execute(delete(User).where(User.uid == created_user.uid))
            await db.commit()

        cleanup.add_task(delete_user)

        db_user = await db.scalar(select(User).where(User.uid == created_user.uid).options(selectinload(User.roles)))
        assert db_user
        assert db_user.uid == created_user.uid
        assert db_user.provider_id(oidc_provider) is None


class TestUserCRUDUpdate:
    @pytest.mark.asyncio
    async def test_update_user_email(self, db: AsyncSession, random_user: UserWithAuthCookie) -> None:
        """
        Test for updating a user smtp in the User CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowm.tests.utils.UserWithAuthCookie
            Random user for testing.
        """
        new_mail = f"{random_lower_string(9)}@example.com"
        await CRUDUser(db=db).update_email(
            random_user.uid,
            new_mail,
        )

        db_user = await db.scalar(select(User).where(User.uid == random_user.uid))
        assert db_user
        assert db_user.uid == random_user.uid
        assert db_user.email == new_mail

    @pytest.mark.asyncio
    async def test_mark_user_initialized(self, db: AsyncSession, random_user: UserWithAuthCookie) -> None:
        """
        Test for marking a user initialized in the User CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowm.tests.utils.UserWithAuthCookie
            Random user for testing.
        """
        await CRUDUser(db=db).mark_initialized(
            random_user.uid,
        )

        db_user = await db.scalar(select(User).where(User.uid == random_user.uid))
        assert db_user
        assert db_user.uid == random_user.uid
        assert db_user.initialized

    @pytest.mark.asyncio
    async def test_update_user_roles(self, db: AsyncSession, random_second_user: UserWithAuthCookie) -> None:
        """
        Test for updating a user roles.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : clowm.tests.utils.UserWithAuthCookie
            Random user for testing.
        """
        assert len(random_second_user.user.roles) == 2
        await CRUDUser(db=db).update_roles(
            random_second_user.user,
            roles=[Role.RoleEnum.DEVELOPER, Role.RoleEnum.REVIEWER],
        )

        mapping = RoleIdMapping()

        db_user = await db.scalar(
            select(User).where(User.uid == random_second_user.uid).options(selectinload(User.roles))
        )
        assert db_user
        assert db_user.uid == random_second_user.uid
        assert len(db_user.roles) == 2
        assert (
            db_user.roles[0].role_id == mapping[Role.RoleEnum.REVIEWER]
            or db_user.roles[1].role_id == mapping[Role.RoleEnum.REVIEWER]
        )
        assert (
            db_user.roles[0].role_id == mapping[Role.RoleEnum.DEVELOPER]
            or db_user.roles[1].role_id == mapping[Role.RoleEnum.DEVELOPER]
        )

    @pytest.mark.asyncio
    async def test_update_invited_user(
        self, db: AsyncSession, random_second_user: UserWithAuthCookie, oidc_provider: OIDCProvider
    ) -> None:
        """
        Test for updating an invited user the User CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : clowm.tests.utils.UserWithAuthCookie
            Random user for testing.
        oidc_provider : clowm.core.oidc.OICDProvider
            One possible OIDC provider for the test.
        """
        new_name = random_lower_string()
        new_provider_id = random_lower_string()

        await CRUDUser(db=db).update_invited_user(
            uid=random_second_user.uid,
            user=UserInfo(
                name=new_name,
                sub=new_provider_id,
                email=random_second_user.user.email,
                provider=oidc_provider,
            ),
        )

        db_user = await db.scalar(select(User).where(User.uid == random_second_user.uid))
        await db.refresh(db_user, attribute_names=["invitation_token", "invitation_token_created_at"])
        assert db_user is not None
        assert db_user.uid == random_second_user.uid
        assert db_user.display_name == new_name
        assert db_user.provider_id(oidc_provider) == new_provider_id
        assert db_user.invitation_token is None
        assert db_user.invitation_token_created_at is None

    @pytest.mark.asyncio
    async def test_create_invitation_token(self, db: AsyncSession, random_user: UserWithAuthCookie) -> None:
        """
        Test for creating an invitation token for a user.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowm.tests.utils.UserWithAuthCookie
            Random user for testing.
        """

        token = await CRUDUser(db=db).create_invitation_token(random_user.uid)

        db_user = await db.scalar(select(User).where(User.uid == random_user.uid))
        # await db.refresh(db_user, attribute_names=["invitation_token", "invitation_token_created_at"])
        assert db_user is not None
        assert db_user.invitation_token == token
        assert db_user.invitation_token_created_at is not None

    @pytest.mark.asyncio
    async def test_update_last_login(self, db: AsyncSession, random_user: UserWithAuthCookie) -> None:
        """
        Test for updating the last_login column of a user.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowm.tests.utils.UserWithAuthCookie
            Random user for testing.
        """
        timestamp = round(time.time())
        await CRUDUser(db=db).update_last_login(uid=random_user.uid, timestamp=timestamp)

        db_user = await db.scalar(select(User).where(User.uid == random_user.uid))
        assert db_user is not None
        assert db_user.last_login == timestamp


class TestUserCRUDGet:
    @pytest.mark.asyncio
    async def test_get_user_by_id(self, db: AsyncSession, random_user: UserWithAuthCookie) -> None:
        """
        Test for getting a user by id from the User CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowm.tests.utils.UserWithAuthCookie
            Random user for testing.
        """
        user = await CRUDUser(db=db).get(
            random_user.uid,
        )
        assert user
        assert random_user.uid == user.uid
        assert random_user.user.display_name == user.display_name

    @pytest.mark.asyncio
    async def test_get_user_by_sub(self, db: AsyncSession, random_user_with_provider: UserWithAuthCookie) -> None:
        """
        Test for getting a user by lifescience id from the User CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user_with_provider : clowm.tests.utils.UserWithAuthCookie
            Random user for testing with one provider set.
        """
        assert len(random_user_with_provider.user.registered_providers) > 0
        for provider in random_user_with_provider.user.registered_providers:
            provider_id = random_user_with_provider.user.provider_id(provider)
            assert provider_id is not None
            user = await CRUDUser(db=db).get_by_sub(sub=provider_id, provider=provider)
            assert user is not None
            assert random_user_with_provider.uid == user.uid
            assert random_user_with_provider.user.display_name == user.display_name

    @pytest.mark.asyncio
    async def test_get_unknown_user_by_id(
        self,
        db: AsyncSession,
    ) -> None:
        """
        Test for getting an unknown user by id from the User CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        user = await CRUDUser(db=db).get(
            uuid.uuid4(),
        )
        assert user is None

    @pytest.mark.asyncio
    async def test_search_successful_user_by_name(self, db: AsyncSession, random_user: UserWithAuthCookie) -> None:
        """
        Test for searching a user by a substring of his name in the User CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowm.tests.utils.UserWithAuthCookie
            Random user for testing.
        """
        substring_indices = [0, 0]
        while substring_indices[0] == substring_indices[1]:
            substring_indices = sorted(random.choices(range(len(random_user.user.display_name)), k=2))

        random_substring = random_user.user.display_name[substring_indices[0] : substring_indices[1]]
        users = await CRUDUser(db=db).list_users(
            name_substring=random_substring,
        )
        assert len(users) > 0
        assert sum(1 for u in users if u.uid == random_user.uid) == 1

    @pytest.mark.asyncio
    async def test_search_non_existing_user_by_name(self, db: AsyncSession, random_user: UserWithAuthCookie) -> None:
        """
        Test for searching a non-existing user by a substring of his name in the User CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowm.tests.utils.UserWithAuthCookie
            Random user for testing.
        """
        users = await CRUDUser(db=db).list_users(
            name_substring=2 * random_user.user.display_name,
        )
        assert sum(1 for u in users if u.uid == random_user.uid) == 0

    @pytest.mark.asyncio
    async def test_get_by_invitation_token(self, db: AsyncSession, random_user: UserWithAuthCookie) -> None:
        """
        Test for getting a user by an invitation token from the database.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowm.tests.utils.UserWithAuthCookie
            Random user for testing.
        """
        token = secrets.token_urlsafe(32)
        await db.execute(
            update(User)
            .where(User.uid == random_user.uid)
            .values(invitation_token=token, invitation_token_created_at=round(time.time()))
        )
        await db.commit()
        user = await CRUDUser(db=db).get_by_invitation_token(
            token,
        )
        assert user is not None
        assert user.uid == random_user.uid

    @pytest.mark.asyncio
    async def test_get_by_non_existing_invitation_token(self, db: AsyncSession) -> None:
        """
        Test for getting a user by a non-existing invitation token from the database.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        user = await CRUDUser(db=db).get_by_invitation_token(
            secrets.token_urlsafe(32),
        )
        assert user is None


class TestUserCRUDList:
    @pytest.mark.asyncio
    async def test_list_all_users(self, db: AsyncSession, random_user: UserWithAuthCookie) -> None:
        """
        Test for listing all the users in the User CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowm.tests.utils.UserWithAuthCookie
            Random user for testing.
        """
        users = await CRUDUser(db=db).list_users()
        assert len(users) > 0
        assert sum(1 for u in users if u.uid == random_user.uid) == 1

    @pytest.mark.asyncio
    async def test_list_all_users_filter_by_role(self, db: AsyncSession, random_user: UserWithAuthCookie) -> None:
        """
        Test for listing all the users in the User CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowm.tests.utils.UserWithAuthCookie
            Random user for testing.
        """
        mapping = RoleIdMapping()
        users = await CRUDUser(db=db).list_users(
            roles=[mapping.get_role_name(role.role_id) for role in random_user.user.roles]
        )
        assert len(users) > 0
        assert sum(1 for u in users if u.uid == random_user.uid) == 1


class TestUserCRUDDelete:
    @pytest.mark.asyncio
    async def test_delete_user(self, db: AsyncSession, random_user: UserWithAuthCookie) -> None:
        """
        Test for deleting a user in the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowm.tests.utils.UserWithAuthCookie
            Random user for testing.
        """
        await CRUDUser(db=db).delete(random_user.uid)
        await db.commit()

        stmt = select(User).where(
            User.uid == random_user.uid,
        )
        user = await db.scalar(stmt)
        assert user is None
