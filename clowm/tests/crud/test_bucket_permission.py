from datetime import datetime, timedelta
from uuid import uuid4

import pytest
from sqlalchemy import and_, select
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.crud import CRUDBucketPermission, DuplicateError
from clowm.db.types import BucketPermissionScopesDB
from clowm.models import Bucket, BucketPermission
from clowm.schemas.bucket_permission import BucketPermissionIn
from clowm.schemas.bucket_permission import BucketPermissionParameters as BucketPermissionParametersSchema
from clowm.tests.utils.bucket import add_permission_for_bucket
from clowm.tests.utils.user import UserWithAuthCookie

from ..utils import random_lower_string


class TestBucketPermissionCRUDGet:
    @pytest.mark.asyncio
    async def test_get_specific_bucket_permission(
        self, db: AsyncSession, random_bucket_permission: BucketPermission
    ) -> None:
        """
        Test for getting a specific bucket permission from the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket_permission : clowm.models.BucketPermission
            Bucket permission for a random bucket for testing.
        """
        bucket_permission = await CRUDBucketPermission(db=db).get(
            bucket_name=random_bucket_permission.bucket_name,
            uid=random_bucket_permission.uid,
        )
        assert bucket_permission
        assert bucket_permission.uid == random_bucket_permission.uid
        assert bucket_permission.bucket_name == random_bucket_permission.bucket_name
        assert bucket_permission.scopes == random_bucket_permission.scopes

    @pytest.mark.asyncio
    async def test_get_bucket_permissions_by_bucket_name(
        self, db: AsyncSession, random_bucket_permission: BucketPermission
    ) -> None:
        """
        Test for getting all bucket permissions for a specific bucket from the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket_permission : clowm.models.BucketPermission
            Bucket permission for a random bucket for testing.
        """
        bucket_permissions = await CRUDBucketPermission(db=db).list(bucket_name=random_bucket_permission.bucket_name)
        assert len(bucket_permissions) == 1
        bucket_permission = bucket_permissions[0]
        assert bucket_permission.uid == random_bucket_permission.uid
        assert bucket_permission.bucket_name == random_bucket_permission.bucket_name
        assert bucket_permission.scopes == random_bucket_permission.scopes

    @pytest.mark.asyncio
    async def test_get_read_bucket_permissions_by_bucket_name(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthCookie,
        random_third_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for getting only 'READ' bucket permissions for a specific bucket from the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random third user who has no permissions for the bucket.
        random_third_user : clowm.tests.utils.user.UserWithAuthCookie
            Random third user who has no permissions for the bucket.
        """
        await add_permission_for_bucket(db, random_bucket.name, random_second_user.uid)
        await add_permission_for_bucket(
            db, random_bucket.name, random_third_user.uid, scopes=BucketPermissionScopesDB.WRITE
        )
        bucket_permissions = await CRUDBucketPermission(db=db).list(
            bucket_name=random_bucket.name, permission_scopes=BucketPermissionScopesDB.READ
        )
        assert len(bucket_permissions) == 1
        assert bucket_permissions[0].uid == random_second_user.uid

    @pytest.mark.asyncio
    async def test_get_read_and_write_bucket_permissions_by_bucket_name(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthCookie,
        random_third_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for getting all 'READ' and 'WRITE' bucket permissions for a specific bucket from the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random third user who has no permissions for the bucket.
        random_third_user : clowm.tests.utils.user.UserWithAuthCookie
            Random third user who has no permissions for the bucket.
        """
        await add_permission_for_bucket(
            db, random_bucket.name, random_second_user.uid, scopes=BucketPermissionScopesDB.READ
        )
        await add_permission_for_bucket(
            db, random_bucket.name, random_third_user.uid, scopes=BucketPermissionScopesDB.WRITE
        )
        bucket_permissions = await CRUDBucketPermission(db=db).list(
            bucket_name=random_bucket.name,
            permission_scopes=BucketPermissionScopesDB.READ | BucketPermissionScopesDB.WRITE,
        )
        assert len(bucket_permissions) == 2
        assert random_second_user.uid in map(lambda x: x.uid, bucket_permissions)
        assert random_third_user.uid in map(lambda x: x.uid, bucket_permissions)

    @pytest.mark.asyncio
    async def test_get_active_bucket_permissions_by_bucket_name1(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthCookie,
        random_third_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for getting all active bucket permissions for a specific bucket from the CRUD Repository.
        Only the 'from' timestamp is set.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random third user who has no permissions for the bucket.
        random_third_user : clowm.tests.utils.user.UserWithAuthCookie
            Random third user who has no permissions for the bucket.
        """
        await add_permission_for_bucket(
            db, random_bucket.name, random_second_user.uid, from_=datetime.now() + timedelta(weeks=1)
        )
        await add_permission_for_bucket(
            db, random_bucket.name, random_third_user.uid, from_=datetime.now() - timedelta(weeks=1)
        )
        bucket_permissions = await CRUDBucketPermission(db=db).list(
            bucket_name=random_bucket.name, permission_status=CRUDBucketPermission(db=db).PermissionStatus.ACTIVE
        )
        assert len(bucket_permissions) == 1
        assert bucket_permissions[0].uid == random_third_user.uid

    @pytest.mark.asyncio
    async def test_get_active_bucket_permissions_by_bucket_name2(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthCookie,
        random_third_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for getting all active bucket permissions for a specific bucket from the CRUD Repository.
        Only the 'to' timestamp is set.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random third user who has no permissions for the bucket.
        random_third_user : clowm.tests.utils.user.UserWithAuthCookie
            Random third user who has no permissions for the bucket.
        """
        await add_permission_for_bucket(
            db, random_bucket.name, random_second_user.uid, to=datetime.now() - timedelta(weeks=1)
        )
        await add_permission_for_bucket(
            db, random_bucket.name, random_third_user.uid, to=datetime.now() + timedelta(weeks=1)
        )
        bucket_permissions = await CRUDBucketPermission(db=db).list(
            bucket_name=random_bucket.name, permission_status=CRUDBucketPermission(db=db).PermissionStatus.ACTIVE
        )
        assert len(bucket_permissions) == 1
        assert bucket_permissions[0].uid == random_third_user.uid

    @pytest.mark.asyncio
    async def test_get_active_bucket_permissions_by_bucket_name3(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthCookie,
        random_third_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for getting all active bucket permissions for a specific bucket from the CRUD Repository.
        The 'from' and 'to' timestamp are set.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random third user who has no permissions for the bucket.
        random_third_user : clowm.tests.utils.user.UserWithAuthCookie
            Random third user who has no permissions for the bucket.
        """
        await add_permission_for_bucket(
            db,
            random_bucket.name,
            random_second_user.uid,
            to=datetime.now() - timedelta(weeks=1),
            from_=datetime.now() - timedelta(weeks=2),
        )
        await add_permission_for_bucket(
            db,
            random_bucket.name,
            random_third_user.uid,
            to=datetime.now() + timedelta(weeks=1),
            from_=datetime.now() - timedelta(weeks=1),
        )
        bucket_permissions = await CRUDBucketPermission(db=db).list(
            bucket_name=random_bucket.name, permission_status=CRUDBucketPermission(db=db).PermissionStatus.ACTIVE
        )
        assert len(bucket_permissions) == 1
        assert bucket_permissions[0].uid == random_third_user.uid

    @pytest.mark.asyncio
    async def test_get_inactive_bucket_permissions_by_bucket_name1(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthCookie,
        random_third_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for getting all inactive bucket permissions for a specific bucket from the CRUD Repository.
        Only the 'from' timestamp is set.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random third user who has no permissions for the bucket.
        random_third_user : clowm.tests.utils.user.UserWithAuthCookie
            Random third user who has no permissions for the bucket.
        """
        await add_permission_for_bucket(
            db, random_bucket.name, random_second_user.uid, from_=datetime.now() + timedelta(weeks=1)
        )
        await add_permission_for_bucket(
            db, random_bucket.name, random_third_user.uid, from_=datetime.now() - timedelta(weeks=1)
        )
        bucket_permissions = await CRUDBucketPermission(db=db).list(
            bucket_name=random_bucket.name, permission_status=CRUDBucketPermission(db=db).PermissionStatus.INACTIVE
        )
        assert len(bucket_permissions) == 1
        assert bucket_permissions[0].uid == random_second_user.uid

    @pytest.mark.asyncio
    async def test_get_inactive_bucket_permissions_by_bucket_name2(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthCookie,
        random_third_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for getting all inactive bucket permissions for a specific bucket from the CRUD Repository.
        Only the 'to' timestamp is set.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random third user who has no permissions for the bucket.
        random_third_user : clowm.tests.utils.user.UserWithAuthCookie
            Random third user who has no permissions for the bucket.
        """
        await add_permission_for_bucket(
            db, random_bucket.name, random_second_user.uid, to=datetime.now() - timedelta(weeks=1)
        )
        await add_permission_for_bucket(
            db, random_bucket.name, random_third_user.uid, to=datetime.now() + timedelta(weeks=1)
        )
        bucket_permissions = await CRUDBucketPermission(db=db).list(
            bucket_name=random_bucket.name, permission_status=CRUDBucketPermission(db=db).PermissionStatus.INACTIVE
        )
        assert len(bucket_permissions) == 1
        assert bucket_permissions[0].uid == random_second_user.uid

    @pytest.mark.asyncio
    async def test_get_inactive_bucket_permissions_by_bucket_name3(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthCookie,
        random_third_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for getting all inactive bucket permissions for a specific bucket from the CRUD Repository.
        The 'from' and 'to' timestamp are set.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random third user who has no permissions for the bucket.
        random_third_user : clowm.tests.utils.user.UserWithAuthCookie
            Random third user who has no permissions for the bucket.
        """
        await add_permission_for_bucket(
            db,
            random_bucket.name,
            random_second_user.uid,
            to=datetime.now() - timedelta(weeks=1),
            from_=datetime.now() - timedelta(weeks=2),
        )
        await add_permission_for_bucket(
            db,
            random_bucket.name,
            random_third_user.uid,
            to=datetime.now() + timedelta(weeks=1),
            from_=datetime.now() - timedelta(weeks=1),
        )
        bucket_permissions = await CRUDBucketPermission(db=db).list(
            bucket_name=random_bucket.name, permission_status=CRUDBucketPermission(db=db).PermissionStatus.INACTIVE
        )
        assert len(bucket_permissions) == 1
        assert bucket_permissions[0].uid == random_second_user.uid

    @pytest.mark.asyncio
    async def test_get_bucket_permissions_by_uid(
        self, db: AsyncSession, random_bucket_permission: BucketPermission
    ) -> None:
        """
        Test for getting all bucket permissions for a specific user from the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket_permission : clowm.models.BucketPermission
            Bucket permission for a random bucket for testing.
        """
        bucket_permissions = await CRUDBucketPermission(db=db).list(uid=random_bucket_permission.uid)
        assert len(bucket_permissions) == 1
        bucket_permission = bucket_permissions[0]
        assert bucket_permission.uid == random_bucket_permission.uid
        assert bucket_permission.bucket_name == random_bucket_permission.bucket_name
        assert bucket_permission.scopes == random_bucket_permission.scopes

    @pytest.mark.asyncio
    async def test_get_read_bucket_permissions_by_uid(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for getting only 'READ' bucket permissions for a specific user from the CRUD Repository..

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random third user who has no permissions for the bucket.
        """
        await add_permission_for_bucket(
            db, random_bucket.name, random_second_user.uid, scopes=BucketPermissionScopesDB.WRITE
        )
        bucket_permissions = await CRUDBucketPermission(db=db).list(
            uid=random_second_user.uid, permission_scopes=BucketPermissionScopesDB.READ
        )
        assert len(bucket_permissions) == 0

    @pytest.mark.asyncio
    async def test_get_active_bucket_permissions_by_uid(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for getting all active bucket permissions for a specific bucket from the CRUD Repository.
        Only the 'from' timestamp is set.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random third user who has no permissions for the bucket.
        """
        await add_permission_for_bucket(
            db, random_bucket.name, random_second_user.uid, from_=datetime.now() + timedelta(weeks=1)
        )

        bucket_permissions = await CRUDBucketPermission(db=db).list(
            uid=random_second_user.uid, permission_status=CRUDBucketPermission(db=db).PermissionStatus.ACTIVE
        )
        assert len(bucket_permissions) == 0

    @pytest.mark.asyncio
    async def test_check_active_bucket_permission(
        self, db: AsyncSession, random_bucket_permission: BucketPermission
    ) -> None:
        """
        Test for checking if a user has an active permission for a bucket

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket_permission : clowm.models.BucketPermission
            Bucket permission for a random bucket for testing.
        """
        assert await CRUDBucketPermission(db=db).check_active_permission(
            bucket_name=random_bucket_permission.bucket_name, uid=random_bucket_permission.uid
        )
        assert not await CRUDBucketPermission(db=db).check_active_permission(
            bucket_name=random_lower_string(16), uid=random_bucket_permission.uid
        )
        assert not await CRUDBucketPermission(db=db).check_active_permission(
            bucket_name=random_bucket_permission.bucket_name, uid=uuid4()
        )


class TestBucketPermissionCRUDCreate:
    @pytest.mark.asyncio
    async def test_create_bucket_permissions_for_unknown_user(self, db: AsyncSession, random_bucket: Bucket) -> None:
        """
        Test for creating a bucket permission for an unknown user.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        """
        permission = BucketPermissionIn(bucket_name=random_bucket.name, uid=uuid4(), scopes=["read"])
        with pytest.raises(KeyError):
            await CRUDBucketPermission(db=db).create(
                permission,
            )

    @pytest.mark.asyncio
    async def test_create_bucket_permissions_for_owner(
        self, db: AsyncSession, random_user: UserWithAuthCookie, random_bucket: Bucket
    ) -> None:
        """
        Test for creating a bucket permission for the owner of the bucket.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing who is owner of the bucket.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        """
        permission = BucketPermissionIn(bucket_name=random_bucket.name, uid=random_user.uid, scopes=["read"])
        with pytest.raises(ValueError):
            await CRUDBucketPermission(db=db).create(
                permission,
            )

    @pytest.mark.asyncio
    async def test_create_duplicate_bucket_permissions(
        self, db: AsyncSession, random_bucket_permission: BucketPermission
    ) -> None:
        """
        Test for creating a duplicated bucket permission.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket_permission : clowm.model.BucketPermission
            Bucket permission for a random bucket for testing.
        """
        permission = BucketPermissionIn(
            bucket_name=random_bucket_permission.bucket_name,
            uid=random_bucket_permission.uid,
            scopes=["read"],
        )
        with pytest.raises(DuplicateError):
            await CRUDBucketPermission(db=db).create(
                permission,
            )

    @pytest.mark.asyncio
    async def test_create_valid_bucket_permissions(
        self, db: AsyncSession, random_second_user: UserWithAuthCookie, random_bucket: Bucket
    ) -> None:
        """
        Test for creating a valid bucket permission.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        """
        permission = BucketPermissionIn(bucket_name=random_bucket.name, uid=random_second_user.uid, scopes=["read"])
        created_permission = await CRUDBucketPermission(db=db).create(
            permission,
        )

        assert created_permission.uid == random_second_user.uid
        assert created_permission.bucket_name == random_bucket.name


class TestBucketPermissionCRUDDelete:
    @pytest.mark.asyncio
    async def test_delete_bucket_permissions(
        self, db: AsyncSession, random_bucket_permission: BucketPermission
    ) -> None:
        """
        Test for creating a valid bucket permission.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket_permission : clowm.models.BucketPermission
            Bucket permission for a random bucket for testing.
        """
        await CRUDBucketPermission(db=db).delete(
            bucket_name=random_bucket_permission.bucket_name, uid=random_bucket_permission.uid
        )

        stmt = select(BucketPermission).where(
            and_(
                BucketPermission.bucket_name == random_bucket_permission.bucket_name,
                BucketPermission.uid == random_bucket_permission.uid,
            )
        )
        bucket_permission_db = await db.scalar(stmt)

        assert bucket_permission_db is None


class TestBucketPermissionCRUDUpdate:
    @pytest.mark.asyncio
    async def test_update_bucket_permissions(
        self, db: AsyncSession, random_bucket_permission: BucketPermission
    ) -> None:
        """
        Test for updating a valid bucket permission in the database.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket_permission : clowm.models.BucketPermission
            Bucket permission for a random bucket for testing.
        """
        new_from_time = round(datetime(2022, 1, 1, 0, 0).timestamp())
        new_params = BucketPermissionParametersSchema(
            from_timestamp=new_from_time,
            to_timestamp=new_from_time + 86400,  # plus one day
            scopes=["read", "write"],
            file_prefix="pseudo/folder/",
        )
        new_permission = await CRUDBucketPermission(db=db).update_permission(
            random_bucket_permission,
            new_params,
        )

        assert new_permission.uid == random_bucket_permission.uid
        assert new_permission.bucket_name == random_bucket_permission.bucket_name
        assert new_permission.from_ == new_params.from_timestamp
        assert new_permission.to == new_params.to_timestamp
        assert new_permission.scopes == new_params.scopes_db
        assert new_permission.file_prefix == new_params.file_prefix
