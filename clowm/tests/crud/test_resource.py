import random
from uuid import uuid4

import pytest
from sqlalchemy import delete, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import joinedload

from clowm.crud import CRUDResource
from clowm.models import Resource, ResourceVersion
from clowm.schemas.resource import ResourceIn

from ..utils import CleanupList, UserWithAuthCookie, random_lower_string
from ..utils.otr import create_otr


class TestResourceCRUDBase:
    @pytest.fixture(scope="function")
    def crud_repo(self, db: AsyncSession) -> CRUDResource:
        return CRUDResource(db=db)


class TestResourceCRUDGet(TestResourceCRUDBase):
    @pytest.mark.asyncio
    async def test_get_resource(self, random_resource: Resource, crud_repo: CRUDResource) -> None:
        """
        Test for getting an existing resource from the database

        Parameters
        ----------
        random_resource : clowm.models.Resource
            Random resource for testing.
        crud_repo : clowm.crud.CRUDResource
            Resource CRUD repo with injected DB connection
        """
        resource = await crud_repo.get(resource_id=random_resource.resource_id)
        assert resource is not None
        assert resource == random_resource

    @pytest.mark.asyncio
    async def test_get_non_existing_resource(self, crud_repo: CRUDResource) -> None:
        """
        Test for getting a non-existing resource from the database

        Parameters
        ----------
        crud_repo : clowm.crud.CRUDResource
            Resource CRUD repo with injected DB connection
        """
        resource = await crud_repo.get(resource_id=uuid4())
        assert resource is None

    @pytest.mark.asyncio
    async def test_get_resource_by_name(self, crud_repo: CRUDResource, random_resource: Resource) -> None:
        """
        Test for getting a resource by name from the database

        Parameters
        ----------
        crud_repo : clowm.crud.CRUDResource
            Resource CRUD repo with injected DB connection
        random_resource : clowm.models.Resource
            Random resource for testing.
        """
        resource = await crud_repo.get_by_name(name=random_resource.name)
        assert resource is not None
        assert resource == random_resource

    @pytest.mark.asyncio
    async def test_get_non_existing_resource_by_name(self, crud_repo: CRUDResource) -> None:
        """
        Test for getting a non-existing resource by name from the database

        Parameters
        ----------
        crud_repo : clowm.crud.CRUDResource
            Resource CRUD repo with injected DB connection
        """
        resource = await crud_repo.get_by_name(name=random_lower_string())
        assert resource is None


class TestResourceCRUDList(TestResourceCRUDBase):
    @pytest.mark.asyncio
    async def test_get_all_resources(self, crud_repo: CRUDResource, random_resource: Resource) -> None:
        """
        Test get all resources from the CRUD Repository.

        Parameters
        ----------
        crud_repo : clowm.crud.CRUDResource
            Resource CRUD repo with injected DB connection
        random_resource : clowm.schemas.resource.ResourceOut
            Random resource for testing.
        """
        resources = await crud_repo.list_resources()
        assert len(resources) == 1
        assert resources[0].resource_id == random_resource.resource_id

    @pytest.mark.asyncio
    async def test_get_resources_by_maintainer(
        self, crud_repo: CRUDResource, random_resource: Resource, random_user: UserWithAuthCookie
    ) -> None:
        """
        Test get only resources from the CRUD Repository by specific maintainer.

        Parameters
        ----------
        crud_repo : clowm.crud.CRUDResource
            Resource CRUD repo with injected DB connection
        random_resource : clowm.schemas.resource.ResourceOut
            Random resource for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        resources = await crud_repo.list_resources(maintainer_id=random_user.uid)
        assert len(resources) == 1
        assert resources[0].resource_id == random_resource.resource_id

    @pytest.mark.asyncio
    async def test_get_resources_with_unpublished_version(
        self, crud_repo: CRUDResource, random_resource: Resource
    ) -> None:
        """
        Test get only resources from the CRUD Repository with an unpublished version.

        Parameters
        ----------
        crud_repo : clowm.crud.CRUDResource
            Resource CRUD repo with injected DB connection
        random_resource : clowm.schemas.resource.ResourceOut
            Random resource for testing.
        """
        resources = await crud_repo.list_resources(version_status=[ResourceVersion.ResourceVersionStatus.LATEST])
        assert len(resources) == 1
        assert resources[0].resource_id == random_resource.resource_id

    @pytest.mark.asyncio
    async def test_get_resource_with_name_substring(self, crud_repo: CRUDResource, random_resource: Resource) -> None:
        """
        Test get resources with a substring in their name from the CRUD Repository.

        Parameters
        ----------
        crud_repo : clowm.crud.CRUDResource
            Resource CRUD repo with injected DB connection
        random_resource : clowm.schemas.resource.ResourceOut
            Random resource for testing.
        """
        substring_indices = [0, 0]
        while substring_indices[0] == substring_indices[1]:
            substring_indices = sorted(random.choices(range(len(random_resource.name)), k=2))

        random_substring = random_resource.name[substring_indices[0] : substring_indices[1]]
        resources = await crud_repo.list_resources(name_substring=random_substring)
        assert len(resources) > 0
        assert random_resource.resource_id in map(lambda w: w.resource_id, resources)

    @pytest.mark.asyncio
    async def test_search_non_existing_resource_by_name(
        self, crud_repo: CRUDResource, random_resource: Resource
    ) -> None:
        """
        Test for getting a non-existing resource by its name from CRUD Repository.

        Parameters
        ----------
        crud_repo : clowm.crud.CRUDResource
            Resource CRUD repo with injected DB connection
        random_resource : clowm.schemas.resource.ResourceOut
            Random resource for testing.
        """
        resources = await crud_repo.list_resources(name_substring=2 * random_resource.name)
        assert sum(1 for w in resources if w.resource_id == random_resource.resource_id) == 0

    @pytest.mark.asyncio
    async def test_list_public_resource(self, crud_repo: CRUDResource, random_resource: Resource) -> None:
        """
        Test get only public resources from the CRUD Repository.

        Parameters
        ----------
        crud_repo : clowm.crud.CRUDResource
            Resource CRUD repo with injected DB connection
        random_resource : clowm.schemas.resource.ResourceOut
            Random resource for testing.
        """
        resources = await crud_repo.list_resources(public=not random_resource.private)
        assert sum(1 for w in resources if w.resource_id == random_resource.resource_id) == 1

    @pytest.mark.asyncio
    async def test_list_resource_otr(
        self, crud_repo: CRUDResource, random_resource: Resource, random_second_user: UserWithAuthCookie
    ) -> None:
        """
        Test for listing resource OTRs from CRUD Repository.

        Parameters
        ----------
        crud_repo : clowm.crud.CRUDResource
            Resource CRUD repo with injected DB connection
        random_resource : clowm.schemas.resource.ResourceOut
            Random resource for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        """
        await create_otr(
            target_id=random_resource.resource_id,
            new_owner_id=random_second_user.uid,
            db=crud_repo.db,
            otr_type=Resource,
        )

        assert (
            len(
                await crud_repo.list_otrs(
                    filter_combination_union=True,
                    current_owner_id=random_resource.maintainer_id,
                    new_owner_id=random_second_user.uid,
                )
            )
            == 1
        )
        assert (
            len(
                await crud_repo.list_otrs(
                    filter_combination_union=False,
                    current_owner_id=random_second_user.uid,
                    new_owner_id=random_resource.maintainer_id,
                )
            )
            == 0
        )


class TestResourceCRUDDelete(TestResourceCRUDBase):
    @pytest.mark.asyncio
    async def test_delete_resource(self, crud_repo: CRUDResource, random_resource: Resource) -> None:
        """
        Test for deleting a resource from CRUD Repository.

        Parameters
        ----------
        crud_repo : clowm.crud.CRUDResource
            Resource CRUD repo with injected DB connection
        random_resource : clowm.schemas.resource.ResourceOut
            Random resource for testing.
        """
        await crud_repo.delete(resource_id=random_resource.resource_id)

        deleted_resource = await crud_repo.db.scalar(
            select(Resource).where(Resource.resource_id == random_resource.resource_id)
        )
        assert deleted_resource is None

        deleted_resource_versions = (
            await crud_repo.db.scalars(
                select(ResourceVersion).where(ResourceVersion.resource_id == random_resource.resource_id)
            )
        ).all()
        assert len(deleted_resource_versions) == 0


class TestResourceCRUDCreate(TestResourceCRUDBase):
    @pytest.mark.asyncio
    async def test_create_resource(
        self, crud_repo: CRUDResource, random_user: UserWithAuthCookie, cleanup: CleanupList
    ) -> None:
        """
        Test for creating a new resource with the CRUD Repository.

        Parameters
        ----------
        crud_repo : clowm.crud.CRUDResource
            Resource CRUD repo with injected DB connection
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        resource_in = ResourceIn(
            name=random_lower_string(8),
            description=random_lower_string(),
            source=random_lower_string(),
            release=random_lower_string(8),
        )

        resource = await crud_repo.create(resource_in=resource_in, maintainer_id=random_user.uid)
        assert resource is not None

        async def delete_resource() -> None:
            await crud_repo.db.execute(delete(Resource).where(Resource.resource_id == resource.resource_id))
            await crud_repo.db.commit()

        cleanup.add_task(delete_resource)

        created_resource = await crud_repo.db.scalar(
            select(Resource).where(Resource.resource_id == resource.resource_id).options(joinedload(Resource.versions))
        )
        assert created_resource is not None
        assert created_resource == resource

        assert len(created_resource.versions) == 1
        assert created_resource.versions[0].release == resource_in.release

    @pytest.mark.asyncio
    async def test_create_resource_otr(
        self, crud_repo: CRUDResource, random_second_user: UserWithAuthCookie, random_resource: Resource
    ) -> None:
        """
        Test for creating a resource OTR with the CRUD Repository.

        Parameters
        ----------
        crud_repo : clowm.crud.CRUDResource
            Resource CRUD repo with injected DB connection
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        random_resource : clowm.schemas.resource.ResourceOut
            Random resource for testing.
        """
        comment = random_lower_string(64)
        await crud_repo.create_otr(
            target_id=random_resource.resource_id, new_owner_uid=random_second_user.uid, comment=comment
        )

        await crud_repo.db.refresh(random_resource)
        assert random_resource == random_resource
        assert random_resource.transfer_new_owner_uid == random_second_user.uid
        assert random_resource.transfer_comment == comment
        assert random_resource.transfer_created_at is not None


class TestResourceCRUDUpdate(TestResourceCRUDBase):
    @pytest.mark.asyncio
    async def test_accept_resource_otr(
        self,
        crud_repo: CRUDResource,
        random_resource: Resource,
        random_second_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for accepting a workflow OTR with the CRUD Repository.

        Parameters
        ----------
        crud_repo : clowm.crud.CRUDResource
            Resource CRUD repo with injected DB connection
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        random_resource : clowm.schemas.resource.ResourceOut
            Random resource for testing.
        """
        await create_otr(
            target_id=random_resource.resource_id,
            new_owner_id=random_second_user.uid,
            db=crud_repo.db,
            otr_type=Resource,
        )

        await crud_repo.update_otr(target_id=random_resource.resource_id, accept=True)

        await crud_repo.db.refresh(random_resource)
        assert random_resource.transfer_new_owner_uid is None
        assert random_resource.transfer_comment is None
        assert random_resource.transfer_created_at is None
        assert random_resource.maintainer_id == random_second_user.uid
        assert random_resource.last_transfer_timestamp is not None

    @pytest.mark.asyncio
    async def test_reject_workflow_otr(
        self,
        crud_repo: CRUDResource,
        random_resource: Resource,
        random_user: UserWithAuthCookie,
        random_second_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for rejecting a workflow OTR with the CRUD Repository.

        Parameters
        ----------
        crud_repo : clowm.crud.CRUDResource
            Resource CRUD repo with injected DB connection
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        random_resource : clowm.schemas.resource.ResourceOut
            Random resource for testing.
        """
        await create_otr(
            target_id=random_resource.resource_id,
            new_owner_id=random_second_user.uid,
            db=crud_repo.db,
            otr_type=Resource,
        )

        await crud_repo.update_otr(target_id=random_resource.resource_id, accept=False)

        await crud_repo.db.refresh(random_resource)
        assert random_resource.transfer_new_owner_uid is None
        assert random_resource.transfer_comment is None
        assert random_resource.transfer_created_at is None
        assert random_resource.maintainer_id == random_user.uid
        assert random_resource.last_transfer_timestamp is None
