import secrets
import time

import pytest
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.crud import CRUDApiToken
from clowm.db.types import ApiTokenScopesDB
from clowm.models.api_token import ApiToken
from clowm.schemas.api_token import ApiTokenIn
from clowm.tests.utils import UserWithAuthCookie, random_lower_string


class TestApiTokenCRUDCreate:
    @pytest.mark.asyncio
    async def test_create_time_limited_api_token(self, db: AsyncSession, random_user: UserWithAuthCookie) -> None:
        """
        Test for creating a time-limited Api token in the database.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        token_in = ApiTokenIn(expires_at=round(time.time()) + 1000, scopes=["read"], name=random_lower_string(8))
        token = await CRUDApiToken(db=db).create(token_in=token_in, uid=random_user.uid)
        assert token is not None

        created_token = await db.scalar(select(ApiToken).where(ApiToken.token_id == token.token_id))
        assert created_token is not None
        assert created_token == token
        assert created_token.has_scope(ApiTokenScopesDB.READ)
        assert not created_token.expired
        assert created_token.expires_at is not None

    @pytest.mark.asyncio
    async def test_create_api_token(self, db: AsyncSession, random_user: UserWithAuthCookie) -> None:
        """
        Test for creating a time-unlimited Api token in the database.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        token_in = ApiTokenIn(scopes=["read"], name=random_lower_string(8))
        token = await CRUDApiToken(db=db).create(
            token_in=token_in,
            uid=random_user.uid,
        )
        assert token is not None

        created_token = await db.scalar(select(ApiToken).where(ApiToken.token_id == token.token_id))
        assert created_token is not None
        assert created_token == token
        assert created_token.has_scope(ApiTokenScopesDB.READ)
        assert not created_token.expired
        assert created_token.expires_at is None


class TestApiTokenCRUDDelete:
    @pytest.mark.asyncio
    async def test_delete_api_token(self, db: AsyncSession, random_api_token: ApiToken) -> None:
        """
        Test for deleting an Api token from the database.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        """
        await CRUDApiToken(db=db).delete(
            tid=random_api_token.token_id,
        )

        token = await db.scalar(select(ApiToken).where(ApiToken.token_id == random_api_token.token_id))
        assert token is None


class TestApiTokenCRUDUpdate:
    @pytest.mark.asyncio
    async def test_update_api_token_last_used(self, db: AsyncSession, random_api_token: ApiToken) -> None:
        """
        Test for updating the last_used timestamp of an Api token from the database.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        """
        await CRUDApiToken(db=db).update_last_used(
            tid=random_api_token.token_id,
        )

        token = await db.scalar(select(ApiToken).where(ApiToken.token_id == random_api_token.token_id))
        assert token is not None
        assert token.last_used is not None


class TestApiTokenCRUDGet:
    @pytest.mark.asyncio
    async def test_get_api_token_by_id(self, db: AsyncSession, random_api_token: ApiToken) -> None:
        """
        Test for getting an Api token by its ID from the database.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        """
        token = await CRUDApiToken(db=db).get(
            tid=random_api_token.token_id,
        )
        assert token is not None
        assert random_api_token == token

    @pytest.mark.asyncio
    async def test_get_non_existing_api_token(self, db: AsyncSession) -> None:
        """
        Test for getting a non-existing Api token from the database.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        token = await CRUDApiToken(db=db).search(
            token=secrets.token_urlsafe(32),
        )
        assert token is None

    @pytest.mark.asyncio
    async def test_get_api_token(self, db: AsyncSession, random_api_token: ApiToken) -> None:
        """
        Test for getting an Api token from the database.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        """
        token = await CRUDApiToken(db=db).search(
            token=random_api_token.token,
        )
        assert token is not None
        assert random_api_token == token

    @pytest.mark.asyncio
    async def test_get_non_conform_api_token(self, db: AsyncSession) -> None:
        """
        Test for getting an Api token from the database.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        token = await CRUDApiToken(db=db).search(
            token=random_lower_string(13),
        )
        assert token is None


class TestApiTokenCRUDList:
    @pytest.mark.asyncio
    async def test_list_api_tokens(self, db: AsyncSession, random_api_token: ApiToken) -> None:
        """
        Test for listing all Api token from the database.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        """
        tokens = await CRUDApiToken(db=db).list()
        assert len(tokens) == 1
        assert random_api_token == tokens[0]

    @pytest.mark.asyncio
    async def test_list_api_tokens_by_uid(self, db: AsyncSession, random_api_token: ApiToken) -> None:
        """
        Test for listing all Api token by uid from the database.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        """
        tokens = await CRUDApiToken(db=db).list(uid=random_api_token.uid)
        assert len(tokens) == 1
        assert random_api_token == tokens[0]

    @pytest.mark.asyncio
    async def test_list_expired_api_tokens(self, db: AsyncSession, random_api_token: ApiToken) -> None:
        """
        Test for listing all expired Api token from the database.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        """
        tokens = await CRUDApiToken(db=db).list(expired=True)
        assert len(tokens) == 0

    @pytest.mark.asyncio
    async def test_list_not_expired_api_tokens(self, db: AsyncSession, random_api_token: ApiToken) -> None:
        """
        Test for listing all non-expired Api token from the database.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        """
        tokens = await CRUDApiToken(db=db).list(expired=False)
        assert len(tokens) == 1
        assert random_api_token == tokens[0]
