import random
from datetime import date, timedelta
from uuid import uuid4

import pytest
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import joinedload

from clowm.crud import CRUDWorkflow
from clowm.models import Workflow, WorkflowExecution, WorkflowVersion
from clowm.schemas.workflow import WorkflowIn, WorkflowOut
from clowm.schemas.workflow_mode import WorkflowModeIn

from ..utils import CleanupList, UserWithAuthCookie, delete_workflow, random_hex_string, random_lower_string
from ..utils.otr import create_otr


class TestWorkflowCRUDList:
    @pytest.mark.asyncio
    async def test_get_all_workflows(self, db: AsyncSession, random_workflow: WorkflowOut) -> None:
        """
        Test get all workflows from the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random bucket for testing.
        """
        workflows = await CRUDWorkflow(db=db).list_workflows()
        assert len(workflows) == 1
        assert workflows[0].workflow_id == random_workflow.workflow_id

    @pytest.mark.asyncio
    async def test_get_workflows_by_developer(
        self, db: AsyncSession, random_workflow: WorkflowOut, random_user: UserWithAuthCookie
    ) -> None:
        """
        Test get only workflows from the CRUD Repository by specific developer.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        workflows = await CRUDWorkflow(db=db).list_workflows(
            developer_id=random_user.uid,
        )
        assert len(workflows) == 1
        assert workflows[0].workflow_id == random_workflow.workflow_id

    @pytest.mark.asyncio
    async def test_get_workflows_with_unpublished_version(self, db: AsyncSession, random_workflow: WorkflowOut) -> None:
        """
        Test get only workflows from the CRUD Repository with an unpublished version.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random bucket for testing.
        """
        workflows = await CRUDWorkflow(db=db).list_workflows(
            version_status=[WorkflowVersion.WorkflowVersionStatus.CREATED],
        )
        assert len(workflows) == 1
        assert workflows[0].workflow_id == random_workflow.workflow_id

    @pytest.mark.asyncio
    async def test_get_workflow_with_name_substring(self, db: AsyncSession, random_workflow: WorkflowOut) -> None:
        """
        Test get workflows with a substring in their name from the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random bucket for testing.
        """
        substring_indices = [0, 0]
        while substring_indices[0] == substring_indices[1]:
            substring_indices = sorted(random.choices(range(len(random_workflow.name)), k=2))

        random_substring = random_workflow.name[substring_indices[0] : substring_indices[1]]
        workflows = await CRUDWorkflow(db=db).list_workflows(
            name_substring=random_substring,
        )
        assert len(workflows) > 0
        assert random_workflow.workflow_id in map(lambda w: w.workflow_id, workflows)

    @pytest.mark.asyncio
    async def test_search_non_existing_workflow_by_name(self, db: AsyncSession, random_workflow: WorkflowOut) -> None:
        """
        Test for getting a non-existing workflow by its name from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random bucket for testing.
        """
        workflows = await CRUDWorkflow(db=db).list_workflows(
            name_substring=2 * random_workflow.name,
        )
        assert sum(1 for w in workflows if w.workflow_id == random_workflow.workflow_id) == 0

    @pytest.mark.asyncio
    async def test_list_workflow_otr(
        self, db: AsyncSession, random_workflow: WorkflowOut, random_second_user: UserWithAuthCookie
    ) -> None:
        """
        Test for listing workflow OTRs from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random bucket for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        """
        await create_otr(
            target_id=random_workflow.workflow_id, new_owner_id=random_second_user.uid, db=db, otr_type=Workflow
        )

        crud_repo = CRUDWorkflow(db=db)
        assert (
            len(
                await crud_repo.list_otrs(
                    filter_combination_union=True,
                    current_owner_id=random_workflow.developer_id,
                    new_owner_id=random_second_user.uid,
                )
            )
            == 1
        )
        assert (
            len(
                await crud_repo.list_otrs(
                    filter_combination_union=False,
                    current_owner_id=random_second_user.uid,
                    new_owner_id=random_workflow.developer_id,
                )
            )
            == 0
        )


class TestWorkflowCRUDGet:
    @pytest.mark.asyncio
    async def test_get_specific_workflow(self, db: AsyncSession, random_workflow: WorkflowOut) -> None:
        """
        Test for getting a workflow by its id from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random bucket for testing.
        """
        workflow = await CRUDWorkflow(db=db).get(
            random_workflow.workflow_id,
        )
        assert workflow is not None
        assert workflow.workflow_id == random_workflow.workflow_id

    @pytest.mark.asyncio
    async def test_get_workflow_by_name(self, db: AsyncSession, random_workflow: WorkflowOut) -> None:
        """
        Test for getting a workflow by its name from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random bucket for testing.
        """
        workflow = await CRUDWorkflow(db=db).get_by_name(
            random_workflow.name,
        )
        assert workflow is not None
        assert workflow.workflow_id == random_workflow.workflow_id

    @pytest.mark.asyncio
    async def test_get_workflow_statistics(
        self, db: AsyncSession, random_workflow: WorkflowOut, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for getting the aggregated workflow statistics.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random bucket for testing.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        statistics = await CRUDWorkflow(db=db).statistics(
            random_workflow.workflow_id,
        )
        assert len(statistics) == 1
        assert statistics[0].day == date.today()
        assert statistics[0].count == 1

    @pytest.mark.asyncio
    async def test_get_workflow_developer_statistics(
        self, db: AsyncSession, random_workflow: WorkflowOut, random_completed_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for getting workflow statistics for developer.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random bucket for testing.
        random_completed_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        statistics = await CRUDWorkflow(db=db).developer_statistics()
        assert len(statistics) == 1
        assert statistics[0].started_at == date.today()
        assert statistics[0].workflow_id == random_workflow.workflow_id
        assert statistics[0].workflow_execution_id == random_completed_workflow_execution.execution_id
        assert statistics[0].workflow_version_id == random_completed_workflow_execution.workflow_version_id
        if random_completed_workflow_execution.executor_id is not None:
            assert statistics[0].pseudo_uid != random_completed_workflow_execution.executor_id.hex
        assert statistics[0].developer_id == random_workflow.developer_id

    @pytest.mark.asyncio
    async def test_get_workflow_developer_statistics_with_developer_id(
        self, db: AsyncSession, random_workflow: WorkflowOut, random_completed_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for getting workflow statistics for developer with a developer ID.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random bucket for testing.
        random_completed_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        statistics = await CRUDWorkflow(db=db).developer_statistics(
            developer_id=random_workflow.developer_id,
        )
        assert len(statistics) == 1
        assert statistics[0].started_at == date.today()
        assert statistics[0].workflow_id == random_workflow.workflow_id
        assert statistics[0].workflow_execution_id == random_completed_workflow_execution.execution_id
        assert statistics[0].workflow_version_id == random_completed_workflow_execution.workflow_version_id
        if random_completed_workflow_execution.executor_id is not None:
            assert statistics[0].pseudo_uid != random_completed_workflow_execution.executor_id.hex
        assert statistics[0].developer_id == random_workflow.developer_id

    @pytest.mark.asyncio
    async def test_get_workflow_developer_statistics_with_non_existent_developer_id(
        self, db: AsyncSession, random_workflow: WorkflowOut, random_completed_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for getting workflow statistics for developer with a non-existing developer ID.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random bucket for testing.
        random_completed_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        statistics = await CRUDWorkflow(db=db).developer_statistics(
            developer_id=uuid4(),
        )
        assert len(statistics) == 0

    @pytest.mark.asyncio
    async def test_get_workflow_developer_statistics_with_workflow_id(
        self, db: AsyncSession, random_workflow: WorkflowOut, random_completed_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for getting workflow statistics for developer with a workflow ID.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random bucket for testing.
        random_completed_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        statistics = await CRUDWorkflow(db=db).developer_statistics(
            workflow_ids=[random_workflow.workflow_id],
        )
        assert len(statistics) == 1
        assert statistics[0].started_at == date.today()
        assert statistics[0].workflow_id == random_workflow.workflow_id
        assert statistics[0].workflow_execution_id == random_completed_workflow_execution.execution_id
        assert statistics[0].workflow_version_id == random_completed_workflow_execution.workflow_version_id
        if random_completed_workflow_execution.executor_id is not None:
            assert statistics[0].pseudo_uid != random_completed_workflow_execution.executor_id.hex
        assert statistics[0].developer_id == random_workflow.developer_id

    @pytest.mark.asyncio
    async def test_get_workflow_developer_statistics_with_non_existent_workflow_id(
        self, db: AsyncSession, random_workflow: WorkflowOut, random_completed_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for getting workflow statistics for developer with a non-existing workflow ID.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random bucket for testing.
        random_completed_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        statistics = await CRUDWorkflow(db=db).developer_statistics(
            workflow_ids=[uuid4()],
        )
        assert len(statistics) == 0

    @pytest.mark.asyncio
    async def test_get_workflow_developer_statistics_with_start_date(
        self, db: AsyncSession, random_workflow: WorkflowOut, random_completed_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for getting workflow statistics for developer with a start date in the past.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random bucket for testing.
        random_completed_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        statistics = await CRUDWorkflow(db=db).developer_statistics(
            start=date.today() - timedelta(days=7),
        )
        assert len(statistics) == 1
        assert statistics[0].started_at == date.today()
        assert statistics[0].workflow_id == random_workflow.workflow_id
        assert statistics[0].workflow_execution_id == random_completed_workflow_execution.execution_id
        assert statistics[0].workflow_version_id == random_completed_workflow_execution.workflow_version_id
        if random_completed_workflow_execution.executor_id is not None:
            assert statistics[0].pseudo_uid != random_completed_workflow_execution.executor_id.hex
        assert statistics[0].developer_id == random_workflow.developer_id

    @pytest.mark.asyncio
    async def test_get_workflow_developer_statistics_with_bad_start_day(
        self, db: AsyncSession, random_workflow: WorkflowOut, random_completed_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for getting workflow statistics for developer with a start date in the future.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random bucket for testing.
        random_completed_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        statistics = await CRUDWorkflow(db=db).developer_statistics(
            start=date.today() + timedelta(days=7),
        )
        assert len(statistics) == 0

    @pytest.mark.asyncio
    async def test_get_workflow_developer_statistics_with_end_date(
        self, db: AsyncSession, random_workflow: WorkflowOut, random_completed_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for getting workflow statistics for developer with a end date in the future.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random bucket for testing.
        random_completed_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        statistics = await CRUDWorkflow(db=db).developer_statistics(
            end=date.today() + timedelta(days=7),
        )
        assert len(statistics) == 1
        assert statistics[0].started_at == date.today()
        assert statistics[0].workflow_id == random_workflow.workflow_id
        assert statistics[0].workflow_execution_id == random_completed_workflow_execution.execution_id
        assert statistics[0].workflow_version_id == random_completed_workflow_execution.workflow_version_id
        if random_completed_workflow_execution.executor_id is not None:
            assert statistics[0].pseudo_uid != random_completed_workflow_execution.executor_id.hex
        assert statistics[0].developer_id == random_workflow.developer_id

    @pytest.mark.asyncio
    async def test_get_workflow_developer_statistics_with_bad_end_day(
        self, db: AsyncSession, random_workflow: WorkflowOut, random_completed_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for getting workflow statistics for developer with a end date in the past.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random bucket for testing.
        random_completed_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        statistics = await CRUDWorkflow(db=db).developer_statistics(
            end=date.today() - timedelta(days=7),
        )
        assert len(statistics) == 0


class TestWorkflowCRUDCreate:
    @pytest.mark.asyncio
    async def test_create_workflow(
        self, db: AsyncSession, random_user: UserWithAuthCookie, cleanup: CleanupList
    ) -> None:
        """
        Test for creating a workflow in CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        workflow_in = WorkflowIn(
            git_commit_hash=random_hex_string(),
            name=random_lower_string(10),
            short_description=random_lower_string(65),
            nextflow_version="23.04.5",
            repository_url="https://github.com/example/example",
            initial_version="v1.0.0",
        )
        workflow = await CRUDWorkflow(db=db).create(
            workflow=workflow_in,
            developer_id=random_user.uid,
        )
        assert workflow is not None
        cleanup.add_task(delete_workflow, workflow_id=workflow.workflow_id, db=db)

        stmt = select(Workflow).where(Workflow.workflow_id == workflow.workflow_id)
        created_workflow = await db.scalar(stmt)
        assert created_workflow is not None
        assert created_workflow == workflow

    @pytest.mark.asyncio
    async def test_create_workflow_with_mode(
        self, db: AsyncSession, random_user: UserWithAuthCookie, cleanup: CleanupList
    ) -> None:
        """
        Test for creating a workflow with a mode in CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        workflow_in = WorkflowIn(
            git_commit_hash=random_hex_string(),
            name=random_lower_string(10),
            short_description=random_lower_string(65),
            repository_url="https://github.com/example/example",
            nextflow_version="23.04.5",
            modes=[
                WorkflowModeIn(
                    schema_path="example/schema.json", name=random_lower_string(), entrypoint=random_lower_string()
                )
            ],
            initial_version="v1.0.0",
        )
        workflow = await CRUDWorkflow(db=db).create(workflow=workflow_in, developer_id=random_user.uid)
        assert workflow is not None
        cleanup.add_task(delete_workflow, workflow_id=workflow.workflow_id, db=db)

        stmt = (
            select(Workflow)
            .where(Workflow.workflow_id == workflow.workflow_id)
            .options(joinedload(Workflow.versions).selectinload(WorkflowVersion.workflow_modes))
        )
        created_workflow = await db.scalar(stmt)
        assert created_workflow is not None
        assert created_workflow == workflow

        assert len(created_workflow.versions) == 1
        assert len(created_workflow.versions[0].workflow_modes) == 1

    @pytest.mark.asyncio
    async def test_create_workflow_credentials(self, db: AsyncSession, random_workflow: WorkflowOut) -> None:
        """
        Test for updating the workflow credentials in CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        token = random_lower_string(15)
        await CRUDWorkflow(db=db).update_credentials(workflow_id=random_workflow.workflow_id, token=token)

        stmt = select(Workflow).where(Workflow.workflow_id == random_workflow.workflow_id)
        workflow = await db.scalar(stmt)
        assert workflow is not None
        assert workflow.credentials_token == token

    @pytest.mark.asyncio
    async def test_create_workflow_otr(
        self, db: AsyncSession, random_second_user: UserWithAuthCookie, random_workflow: WorkflowOut
    ) -> None:
        """
        Test for creating a workflow OTR in CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        comment = random_lower_string(64)
        await CRUDWorkflow(db=db).create_otr(
            target_id=random_workflow.workflow_id, new_owner_uid=random_second_user.uid, comment=comment
        )

        workflow_db: Workflow | None = await db.scalar(
            select(Workflow).where(Workflow.workflow_id == random_workflow.workflow_id)
        )
        assert workflow_db is not None
        assert workflow_db.workflow_id == random_workflow.workflow_id
        assert workflow_db.transfer_new_owner_uid == random_second_user.uid
        assert workflow_db.transfer_comment == comment
        assert workflow_db.transfer_created_at is not None


class TestWorkflowCRUDDelete:
    @pytest.mark.asyncio
    async def test_delete_workflow(self, db: AsyncSession, random_workflow: WorkflowOut) -> None:
        """
        Test for deleting a workflow in CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """

        await CRUDWorkflow(db=db).delete(workflow_id=random_workflow.workflow_id)

        stmt1 = select(Workflow).where(Workflow.workflow_id == random_workflow.workflow_id)
        created_workflow = await db.scalar(stmt1)
        assert created_workflow is None

        stmt2 = select(WorkflowVersion).where(WorkflowVersion.workflow_id == random_workflow.workflow_id)
        versions = (await db.scalars(stmt2)).all()

        assert len(versions) == 0

    @pytest.mark.asyncio
    async def test_delete_workflow_credentials(self, db: AsyncSession, random_private_workflow: WorkflowOut) -> None:
        """
        Test for deleting the workflow credentials in CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_private_workflow : clowm.schemas.workflow.WorkflowOut
            Random private workflow for testing.
        """
        await CRUDWorkflow(db=db).update_credentials(workflow_id=random_private_workflow.workflow_id, token=None)

        stmt = select(Workflow).where(Workflow.workflow_id == random_private_workflow.workflow_id)
        workflow = await db.scalar(stmt)
        assert workflow is not None
        assert workflow.credentials_token is None


class TestWorkflowCRUDUpdate:
    @pytest.mark.asyncio
    async def test_accept_workflow_otr(
        self,
        db: AsyncSession,
        random_workflow: WorkflowOut,
        random_second_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for accepting a workflow OTR with the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        await create_otr(
            target_id=random_workflow.workflow_id, new_owner_id=random_second_user.uid, db=db, otr_type=Workflow
        )

        crud_repo = CRUDWorkflow(db=db)
        await crud_repo.update_otr(target_id=random_workflow.workflow_id, accept=True)
        await db.commit()
        await db.reset()

        workflow_db = await db.scalar(select(Workflow).where(Workflow.workflow_id == random_workflow.workflow_id))
        assert workflow_db is not None
        assert workflow_db.transfer_new_owner_uid is None
        assert workflow_db.transfer_comment is None
        assert workflow_db.transfer_created_at is None
        assert workflow_db.developer_id == random_second_user.uid
        assert workflow_db.last_transfer_timestamp is not None

    @pytest.mark.asyncio
    async def test_reject_workflow_otr(
        self,
        db: AsyncSession,
        random_workflow: WorkflowOut,
        random_user: UserWithAuthCookie,
        random_second_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for rejecting a workflow OTR with the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        await create_otr(
            target_id=random_workflow.workflow_id, new_owner_id=random_second_user.uid, db=db, otr_type=Workflow
        )

        crud_repo = CRUDWorkflow(db=db)
        await crud_repo.update_otr(target_id=random_workflow.workflow_id, accept=False)
        await db.commit()
        await db.reset()
        workflow_db = await db.scalar(select(Workflow).where(Workflow.workflow_id == random_workflow.workflow_id))

        assert workflow_db is not None
        assert workflow_db.transfer_new_owner_uid is None
        assert workflow_db.transfer_comment is None
        assert workflow_db.transfer_created_at is None
        assert workflow_db.developer_id == random_user.uid
        assert workflow_db.last_transfer_timestamp is None
