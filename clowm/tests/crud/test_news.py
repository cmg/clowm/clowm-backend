import time
from collections.abc import AsyncIterator
from uuid import uuid4

import pytest
import pytest_asyncio
from sqlalchemy import select, update
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.crud import CRUDNews
from clowm.models import News
from clowm.schemas.news import NewsIn

from ..utils import CleanupList, UserWithAuthCookie, delete_news_event, random_lower_string


class TestNewsCRUDBase:
    @pytest.fixture(scope="function")
    def crud_repo(self, db: AsyncSession) -> CRUDNews:
        return CRUDNews(db=db)

    @pytest_asyncio.fixture(scope="function")
    async def random_news_event(self, db: AsyncSession, random_user: UserWithAuthCookie) -> AsyncIterator[News]:
        """
        Create a random news event and deletes it afterward.
        """
        news = News(
            content=random_lower_string(),
            title=random_lower_string(8),
            creator_id=random_user.uid,
            important_till=round(time.time()) + 1000,
            category="system",
        )
        db.add(news)
        await db.commit()
        yield news
        await db.delete(news)
        await db.commit()


class TestNewsCRUDGet(TestNewsCRUDBase):
    @pytest.mark.asyncio
    async def test_get_news_event(self, random_news_event: News, crud_repo: CRUDNews) -> None:
        """
        Test for getting an existing resource from the database

        Parameters
        ----------
        random_news_event : clowm.models.News
            Random news event for testing.
        crud_repo : clowm.crud.CRUDNews
            News CRUD repo with injected DB connection
        """
        news = await crud_repo.get(news_id=random_news_event.news_id)
        assert news is not None
        assert news == random_news_event
        assert len(news.title) > 0
        assert len(news.content) > 0
        assert news.creator_id is not None

    @pytest.mark.asyncio
    async def test_get_non_existing_news_event(self, crud_repo: CRUDNews) -> None:
        """
        Test for getting an existing resource from the database

        Parameters
        ----------
        crud_repo : clowm.crud.CRUDNews
            News CRUD repo with injected DB connection
        """
        news = await crud_repo.get(news_id=uuid4())
        assert news is None


class TestNewsCRUDCreate(TestNewsCRUDBase):
    @pytest.mark.asyncio
    async def test_create_news_event(
        self, crud_repo: CRUDNews, cleanup: CleanupList, random_user: UserWithAuthCookie
    ) -> None:
        """
        Test for getting an existing resource from the database

        Parameters
        ----------
        crud_repo : clowm.crud.CRUDNews
            News CRUD repo with injected DB connection
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        news_in = NewsIn(
            content=random_lower_string(),
            title=random_lower_string(8),
            important_till=round(time.time()) + 1000,
        )
        news = await crud_repo.create(news_in=news_in, creator_id=random_user.uid)
        assert news is not None

        cleanup.add_task(delete_news_event, db=crud_repo.db, news_id=news.news_id)

        assert news.content == news_in.content
        assert news.title == news_in.title
        assert news.important_till == news_in.important_till


class TestNewsCRUDList(TestNewsCRUDBase):
    @pytest.mark.asyncio
    async def test_list_news_events(self, crud_repo: CRUDNews, random_news_event: News) -> None:
        """
        Test for listing all news events.

        Parameters
        ----------
        crud_repo : clowm.crud.CRUDNews
            News CRUD repo with injected DB connection
        random_news_event : clowm.models.News
            Random news event for testing.
        """
        news = await crud_repo.list(
            id_after=random_news_event.news_id,
        )
        assert len(news) > 0
        assert next((event for event in news if event == random_news_event), None) is not None

    @pytest.mark.asyncio
    async def test_get_list_news_events_of_user(self, crud_repo: CRUDNews, random_news_event: News) -> None:
        """
        Test for listing all news events and filter by user.

        Parameters
        ----------
        crud_repo : clowm.crud.CRUDNews
            News CRUD repo with injected DB connection
        random_news_event : clowm.models.News
            Random news event for testing.
        """
        news1 = await crud_repo.list(creator_id=random_news_event.creator_id)
        assert len(news1) > 0
        assert next((event for event in news1 if event == random_news_event), None) is not None
        assert sum(1 for event in news1 if event.creator_id == random_news_event.creator_id) >= 1

        news2 = await crud_repo.list(creator_id=uuid4())
        assert next((event for event in news2 if event == random_news_event), None) is None
        assert sum(1 for event in news2 if event.creator_id == random_news_event.creator_id) == 0

    @pytest.mark.asyncio
    async def test_get_list_news_events_by_creation_time(self, crud_repo: CRUDNews, random_news_event: News) -> None:
        """
        Test for listing all news events and filter by creation date.

        Parameters
        ----------
        crud_repo : clowm.crud.CRUDNews
            News CRUD repo with injected DB connection
        random_news_event : clowm.models.News
            Random news event for testing.
        """
        news1 = await crud_repo.list(created_after=round(time.time()) - 1000)
        assert len(news1) > 0
        assert next((event for event in news1 if event == random_news_event), None) is not None
        assert sum(1 for event in news1 if event.creator_id == random_news_event.creator_id) >= 1

        news2 = await crud_repo.list(created_after=round(time.time()) + 1000)
        assert next((event for event in news2 if event == random_news_event), None) is None
        assert sum(1 for event in news2 if event.creator_id == random_news_event.creator_id) == 0

    @pytest.mark.asyncio
    async def test_list_latest_news(self, crud_repo: CRUDNews, random_news_event: News) -> None:
        """
        Parameters
        ----------
        crud_repo : clowm.crud.CRUDNews
            News CRUD repo with injected DB connection
        random_news_event : clowm.models.News
            Random news event for testing.
        """
        assert len(await crud_repo.get_latest(created_after=1)) == 1
        assert len(await crud_repo.get_latest(created_after=round(time.time() + 10000))) == 1

        await crud_repo.db.execute(
            update(News).where(News.news_id == random_news_event.news_id).values(important_till=None)
        )
        await crud_repo.db.commit()

        assert len(await crud_repo.get_latest(created_after=1)) == 1
        assert len(await crud_repo.get_latest(created_after=round(time.time() + 10000))) == 0


class TestNewsCRUDDelete(TestNewsCRUDBase):
    @pytest.mark.asyncio
    async def test_delete_news_event(self, crud_repo: CRUDNews, random_news_event: News) -> None:
        """
        Test for deleting a news events.

        Parameters
        ----------
        crud_repo : clowm.crud.CRUDNews
            News CRUD repo with injected DB connection
        random_news_event : clowm.models.News
            Random news event for testing.
        """
        await crud_repo.delete(news_id=random_news_event.news_id)

        assert await crud_repo.db.scalar(select(News).where(News.news_id == random_news_event.news_id)) is None
