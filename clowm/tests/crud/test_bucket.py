from datetime import datetime, timedelta

import pytest
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.crud import CRUDBucket, DuplicateError
from clowm.db.types import BucketPermissionScopesDB
from clowm.models import Bucket
from clowm.schemas.bucket import BucketIn

from ..utils import CleanupList, UserWithAuthCookie, random_lower_string
from ..utils.bucket import add_permission_for_bucket, delete_bucket
from ..utils.otr import create_otr


class TestBucketCRUDList:
    @pytest.mark.asyncio
    async def test_get_all_bucket(self, db: AsyncSession, random_bucket: Bucket) -> None:
        """
        Test for getting all buckets from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        """
        buckets = await CRUDBucket(db=db).list()
        assert len(buckets) > 0
        bucket = None
        for bucket in buckets:
            if bucket.name == random_bucket.name:
                break
        assert bucket is not None
        assert bucket.name == random_bucket.name
        assert bucket.public == random_bucket.public
        assert bucket.description == random_bucket.description

    @pytest.mark.asyncio
    async def test_get_only_own_buckets(self, db: AsyncSession, random_bucket: Bucket) -> None:
        """
        Test for getting only the buckets where a user is the owner from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        """
        assert random_bucket.owner_id is not None
        buckets = await CRUDBucket(db=db).list_for_user(
            random_bucket.owner_id,
            CRUDBucket(db=db).BucketType.OWN,
        )

        assert len(buckets) == 1
        assert buckets[0].name == random_bucket.name

    @pytest.mark.asyncio
    async def test_get_only_foreign_bucket(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthCookie,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for getting only foreign buckets with permissions for a user from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        bucket = Bucket(
            name=random_lower_string(),
            description=random_lower_string(127),
            owner_id=random_second_user.uid,
        )
        db.add(bucket)
        await db.commit()
        cleanup.add_task(delete_bucket, bucket_name=bucket.name, db=db)
        assert random_bucket.owner_id is not None
        await add_permission_for_bucket(db, bucket.name, random_bucket.owner_id, scopes=BucketPermissionScopesDB.READ)

        buckets = await CRUDBucket(db=db).list_for_user(
            random_bucket.owner_id,
            CRUDBucket(db=db).BucketType.PERMISSION,
        )
        assert len(buckets) == 1
        assert buckets[0] != random_bucket
        assert buckets[0].name == bucket.name

    @pytest.mark.asyncio
    async def test_get_bucket_with_read_permission_and_own(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthCookie,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for getting the users own bucket and a foreign bucket with READ permissions from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        bucket = Bucket(
            name=random_lower_string(),
            description=random_lower_string(127),
            owner_id=random_second_user.uid,
        )
        db.add(bucket)
        await db.commit()
        cleanup.add_task(delete_bucket, bucket_name=bucket.name, db=db)
        assert random_bucket.owner_id is not None
        await add_permission_for_bucket(db, bucket.name, random_bucket.owner_id, scopes=BucketPermissionScopesDB.READ)

        buckets = await CRUDBucket(db=db).list_for_user(
            random_bucket.owner_id,
        )

        assert len(buckets) == 2
        assert buckets[0].name == random_bucket.name or buckets[1].name == random_bucket.name
        assert buckets[0].name == bucket.name or buckets[1].name == bucket.name

    @pytest.mark.asyncio
    async def test_get_bucket_with_read_permission(
        self, db: AsyncSession, random_bucket: Bucket, random_second_user: UserWithAuthCookie
    ) -> None:
        """
        Test for getting a foreign bucket with READ permissions from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        """
        await add_permission_for_bucket(
            db, random_bucket.name, random_second_user.uid, scopes=BucketPermissionScopesDB.READ
        )

        buckets = await CRUDBucket(db=db).list_for_user(
            random_second_user.uid,
        )

        assert len(buckets) > 0
        assert buckets[0].name == random_bucket.name

    @pytest.mark.asyncio
    async def test_get_bucket_with_readwrite_permission(
        self, db: AsyncSession, random_bucket: Bucket, random_second_user: UserWithAuthCookie
    ) -> None:
        """
        Test for getting a foreign bucket with READWRITE permissions from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        """
        await add_permission_for_bucket(
            db,
            random_bucket.name,
            random_second_user.uid,
            scopes=BucketPermissionScopesDB.READ | BucketPermissionScopesDB.WRITE,
        )

        buckets = await CRUDBucket(db=db).list_for_user(
            random_second_user.uid,
        )

        assert len(buckets) > 0
        assert buckets[0].name == random_bucket.name

    @pytest.mark.asyncio
    async def test_get_bucket_with_write_permission(
        self, db: AsyncSession, random_bucket: Bucket, random_second_user: UserWithAuthCookie
    ) -> None:
        """
        Test for getting a foreign bucket with WRITE permissions from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        """
        await add_permission_for_bucket(
            db, random_bucket.name, random_second_user.uid, scopes=BucketPermissionScopesDB.WRITE
        )

        buckets = await CRUDBucket(db=db).list_for_user(
            random_second_user.uid,
        )

        assert len(buckets) == 1
        assert buckets[0] == random_bucket

    @pytest.mark.asyncio
    async def test_get_bucket_with_valid_time_permission(
        self, db: AsyncSession, random_bucket: Bucket, random_second_user: UserWithAuthCookie
    ) -> None:
        """
        Test for getting a foreign bucket with valid time permission from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        """
        await add_permission_for_bucket(
            db,
            random_bucket.name,
            random_second_user.uid,
            from_=datetime.now() - timedelta(days=10),
            to=datetime.now() + timedelta(days=10),
        )

        buckets = await CRUDBucket(db=db).list_for_user(
            random_second_user.uid,
        )

        assert len(buckets) > 0
        assert buckets[0].name == random_bucket.name

    @pytest.mark.asyncio
    async def test_get_bucket_with_invalid_from_time_permission(
        self, db: AsyncSession, random_bucket: Bucket, random_second_user: UserWithAuthCookie
    ) -> None:
        """
        Test for getting a foreign bucket with invalid 'from' time permission from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        """
        await add_permission_for_bucket(
            db, random_bucket.name, random_second_user.uid, from_=datetime.now() + timedelta(days=10)
        )

        buckets = await CRUDBucket(db=db).list_for_user(
            random_second_user.uid,
        )

        assert len(buckets) == 0

    @pytest.mark.asyncio
    async def test_get_bucket_with_invalid_to_time_permission(
        self, db: AsyncSession, random_bucket: Bucket, random_second_user: UserWithAuthCookie
    ) -> None:
        """
        Test for getting a foreign bucket with invalid 'to' time permission from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        """
        await add_permission_for_bucket(
            db, random_bucket.name, random_second_user.uid, to=datetime.now() - timedelta(days=10)
        )

        buckets = await CRUDBucket(db=db).list_for_user(
            random_second_user.uid,
        )

        assert len(buckets) == 0

    @pytest.mark.asyncio
    async def test_list_bucket_otr(
        self, db: AsyncSession, random_bucket: Bucket, random_second_user: UserWithAuthCookie
    ) -> None:
        """
        Test for listing bucket OTRs from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        """
        await create_otr(target_id=random_bucket.name, new_owner_id=random_second_user.uid, db=db, otr_type=Bucket)

        crud_repo = CRUDBucket(db=db)
        assert (
            len(
                await crud_repo.list_otrs(
                    filter_combination_union=True,
                    current_owner_id=random_bucket.owner_id,
                    new_owner_id=random_second_user.uid,
                )
            )
            == 1
        )
        assert (
            len(
                await crud_repo.list_otrs(
                    filter_combination_union=False,
                    current_owner_id=random_second_user.uid,
                    new_owner_id=random_bucket.owner_id,
                )
            )
            == 0
        )


class TestBucketCRUDGet:
    @pytest.mark.asyncio
    async def test_get_bucket_by_name(self, db: AsyncSession, random_bucket: Bucket) -> None:
        """
        Test for getting a bucket by name from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        """
        bucket = await CRUDBucket(db=db).get(
            random_bucket.name,
        )
        assert bucket
        assert bucket.name == random_bucket.name
        assert bucket.public == random_bucket.public
        assert bucket.description == random_bucket.description

    @pytest.mark.asyncio
    async def test_get_unknown_bucket(self, db: AsyncSession) -> None:
        """
        Test for getting a not existing bucket from the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        bucket = await CRUDBucket(db=db).get(
            "unknown Bucket",
        )
        assert bucket is None


class TestBucketCRUDCreate:
    @pytest.mark.asyncio
    async def test_create_bucket(
        self,
        db: AsyncSession,
        random_user: UserWithAuthCookie,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for creating a bucket with the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        bucket_info = BucketIn(name=random_lower_string(), description=random_lower_string(127))
        bucket = await CRUDBucket(db=db).create(
            bucket_info,
            random_user.uid,
        )
        cleanup.add_task(delete_bucket, bucket_name=bucket.name, db=db)
        assert bucket.name == bucket_info.name
        assert bucket.owner_id == random_user.uid
        assert bucket.description == bucket_info.description

        stmt = select(Bucket).where(Bucket.name == bucket_info.name)
        bucket_db = await db.scalar(stmt)

        assert bucket_db
        assert bucket_db.name == bucket_info.name
        assert bucket_db.owner_id == random_user.uid
        assert bucket_db.description == bucket_info.description

    @pytest.mark.asyncio
    async def test_create_duplicated_bucket(self, db: AsyncSession, random_bucket: Bucket) -> None:
        """
        Test for creating a duplicated bucket with the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        """
        bucket_info = BucketIn(name=random_bucket.name, description=random_lower_string(127))
        assert random_bucket.owner_id is not None
        with pytest.raises(DuplicateError):
            await CRUDBucket(db=db).create(
                bucket_info,
                random_bucket.owner_id,
            )

    @pytest.mark.asyncio
    async def test_create_bucket_otr(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for creating a bucket OTR with the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        """
        comment = random_lower_string(64)
        await CRUDBucket(db=db).create_otr(
            target_id=random_bucket.name, new_owner_uid=random_second_user.uid, comment=comment
        )

        await db.refresh(random_bucket)
        assert random_bucket.transfer_new_owner_uid == random_second_user.uid
        assert random_bucket.transfer_comment == comment
        assert random_bucket.transfer_created_at is not None


class TestBucketCRUDUpdate:
    @pytest.mark.asyncio
    async def test_update_public_state(self, db: AsyncSession, random_bucket: Bucket) -> None:
        """
        Test for updating the bucket public state with the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        """
        old_public_state = random_bucket.public
        await CRUDBucket(db=db).update_public_state(bucket_name=random_bucket.name, public=not old_public_state)

        stmt = select(Bucket).where(Bucket.name == random_bucket.name)
        bucket_db = await db.scalar(stmt)

        assert bucket_db is not None
        assert bucket_db == random_bucket
        assert old_public_state != bucket_db.public

    @pytest.mark.asyncio
    async def test_update_bucket_limits(self, db: AsyncSession, random_bucket: Bucket) -> None:
        """
        Test for updating the bucket limits with the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        """
        await CRUDBucket(db=db).update_bucket_limits(bucket_name=random_bucket.name, size_limit=100, object_limit=120)

        stmt = select(Bucket).where(Bucket.name == random_bucket.name)
        bucket_db = await db.scalar(stmt)

        assert bucket_db is not None
        assert bucket_db == random_bucket
        assert bucket_db.size_limit is not None
        assert bucket_db.size_limit == 100

        assert bucket_db.object_limit is not None
        assert bucket_db.object_limit == 120


class TestBucketCRUDDelete:
    @pytest.mark.asyncio
    async def test_delete_bucket(self, db: AsyncSession, random_bucket: Bucket) -> None:
        """
        Test for deleting a bucket with the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        """
        await CRUDBucket(db=db).delete(
            random_bucket.name,
        )

        stmt = select(Bucket).where(Bucket.name == random_bucket.name)
        bucket_db = await db.scalar(stmt)

        assert bucket_db is None

    @pytest.mark.asyncio
    async def test_accept_bucket_otr(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_second_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for accepting a bucket OTR with the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        """
        await create_otr(target_id=random_bucket.name, new_owner_id=random_second_user.uid, db=db, otr_type=Bucket)

        crud_repo = CRUDBucket(db=db)
        await crud_repo.update_otr(target_id=random_bucket.name, accept=True)

        await db.refresh(random_bucket)
        assert random_bucket.transfer_new_owner_uid is None
        assert random_bucket.transfer_comment is None
        assert random_bucket.transfer_created_at is None
        assert random_bucket.owner_id == random_second_user.uid
        assert random_bucket.last_transfer_timestamp is not None

    @pytest.mark.asyncio
    async def test_reject_bucket_otr(
        self,
        db: AsyncSession,
        random_bucket: Bucket,
        random_user: UserWithAuthCookie,
        random_second_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for rejecting a bucket OTR with the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        """
        await create_otr(target_id=random_bucket.name, new_owner_id=random_second_user.uid, db=db, otr_type=Bucket)

        crud_repo = CRUDBucket(db=db)
        await crud_repo.update_otr(target_id=random_bucket.name, accept=False)

        await db.refresh(random_bucket)
        assert random_bucket.transfer_new_owner_uid is None
        assert random_bucket.transfer_comment is None
        assert random_bucket.transfer_created_at is None
        assert random_bucket.owner_id == random_user.uid
        assert random_bucket.last_transfer_timestamp is None
