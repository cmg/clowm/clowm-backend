from uuid import uuid4

import pytest
from sqlalchemy import delete, select
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.crud import CRUDResourceVersion
from clowm.models import Resource, ResourceVersion
from clowm.tests.utils import CleanupList, random_lower_string


class TestResourceVersionCRUDGet:
    @pytest.mark.asyncio
    async def test_get_resource_version(self, db: AsyncSession, random_resource_version: ResourceVersion) -> None:
        """
        Test for getting an existing resource version from the database

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_resource_version : clowm.models.ResourceVersion
            Random resource for testing.
        """
        resource = await CRUDResourceVersion(db=db).get(resource_version_id=random_resource_version.resource_version_id)
        assert resource is not None
        assert resource == random_resource_version

    @pytest.mark.asyncio
    async def test_get_resource_version_with_resource_id(
        self, db: AsyncSession, random_resource_version: ResourceVersion
    ) -> None:
        """
        Test for getting an existing resource version from the database and filter is with the correct resource id

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_resource_version : clowm.models.ResourceVersion
            Random resource for testing.
        """
        resource = await CRUDResourceVersion(db=db).get(
            resource_version_id=random_resource_version.resource_version_id,
            resource_id=random_resource_version.resource_id,
        )
        assert resource is not None
        assert resource == random_resource_version

    @pytest.mark.asyncio
    async def test_get_resource_version_with_wrong_resource_id(
        self, db: AsyncSession, random_resource_version: ResourceVersion
    ) -> None:
        """
        Test for getting an existing resource version from the database and filter it with a wrong resource id

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_resource_version : clowm.models.ResourceVersion
            Random resource for testing.
        """
        resource = await CRUDResourceVersion(db=db).get(
            resource_version_id=random_resource_version.resource_id, resource_id=uuid4()
        )
        assert resource is None

    @pytest.mark.asyncio
    async def test_get_non_existing_resource_version(self, db: AsyncSession) -> None:
        """
        Test for getting a non-existing resource version from the database

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        resource = await CRUDResourceVersion(db=db).get(resource_version_id=uuid4())
        assert resource is None

    @pytest.mark.asyncio
    async def test_get_resource_version_latest(
        self,
        db: AsyncSession,
        random_resource_version: ResourceVersion,
    ) -> None:
        """
        Test for getting the latest resource version of a resource.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_resource_version : clowm.models.ResourceVersion
            Random resource version for testing.
        """
        version = await CRUDResourceVersion(db=db).get_latest(
            resource_id=random_resource_version.resource_id,
        )
        assert version == random_resource_version


class TestResourceVersionCRUDUpdate:
    @pytest.mark.asyncio
    async def test_update_resource_version(
        self,
        db: AsyncSession,
        random_resource_version: ResourceVersion,
        resource_state: ResourceVersion.ResourceVersionStatus,
    ) -> None:
        """
        Test for updating the resource version status from the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_resource_version : clowm.models.ResourceVersion
            Random resource for testing.
        """
        if resource_state == ResourceVersion.ResourceVersionStatus.LATEST:
            pytest.skip("Separate test for that")
        await CRUDResourceVersion(db=db).update_status(
            resource_version_id=random_resource_version.resource_version_id,
            status=resource_state,
            slurm_job_id=10,
        )

        updated_resource_version = await db.scalar(
            select(ResourceVersion).where(
                ResourceVersion.resource_version_id == random_resource_version.resource_version_id
            )
        )
        assert updated_resource_version is not None
        assert updated_resource_version == random_resource_version

        assert updated_resource_version.status == resource_state

    @pytest.mark.asyncio
    async def test_update_resource_version_to_latest_without_resource_id(
        self, db: AsyncSession, random_resource_version: ResourceVersion
    ) -> None:
        """
        Test for updating the resource version status from the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_resource_version : clowm.models.ResourceVersion
            Random resource for testing.
        """
        with pytest.raises(ValueError):
            await CRUDResourceVersion(db=db).update_status(
                resource_version_id=random_resource_version.resource_version_id,
                status=ResourceVersion.ResourceVersionStatus.LATEST,
            )

    @pytest.mark.asyncio
    async def test_update_resource_version_to_latest_with_resource_id(
        self, db: AsyncSession, random_resource_version: ResourceVersion
    ) -> None:
        """
        Test for updating the resource version status from the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_resource_version : clowm.models.ResourceVersion
            Random resource for testing.
        """
        await CRUDResourceVersion(db=db).update_status(
            resource_version_id=random_resource_version.resource_version_id,
            status=ResourceVersion.ResourceVersionStatus.LATEST,
            resource_id=random_resource_version.resource_id,
        )
        updated_resource_version = await db.scalars(
            select(ResourceVersion)
            .where(ResourceVersion.resource_id == random_resource_version.resource_id)
            .where(ResourceVersion.status == ResourceVersion.ResourceVersionStatus.LATEST.name)
        )
        assert sum(1 for _ in updated_resource_version) == 1

    @pytest.mark.asyncio
    async def test_update_non_existing_resource_version(
        self, db: AsyncSession, random_resource_version: ResourceVersion
    ) -> None:
        """
        Test for updating a non-existing resource version status from the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_resource_version : clowm.models.ResourceVersion
            Random resource for testing.
        """
        await CRUDResourceVersion(db=db).update_status(
            resource_version_id=uuid4(),
            status=ResourceVersion.ResourceVersionStatus.S3_DELETED,
        )

        resource_version = await db.scalar(
            select(ResourceVersion).where(
                ResourceVersion.resource_version_id == random_resource_version.resource_version_id
            )
        )
        assert resource_version is not None
        assert resource_version == random_resource_version

        assert resource_version.status == random_resource_version.status

    @pytest.mark.asyncio
    async def test_update_used_resource_version(
        self,
        db: AsyncSession,
        random_resource_version: ResourceVersion,
    ) -> None:
        """
        Test for updating a used resource version.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_resource_version : clowm.models.ResourceVersion
            Random resource version for testing.
        """
        await CRUDResourceVersion(db=db).update_used_resource_version(
            resource_version_id=random_resource_version.resource_version_id
        )

        db_version = await db.scalar(
            select(ResourceVersion).where(
                ResourceVersion.resource_version_id == random_resource_version.resource_version_id
            )
        )
        assert db_version is not None
        assert db_version == random_resource_version
        assert db_version.times_used == 1
        assert db_version.last_used_timestamp is not None


class TestResourceVersionCRUDCreate:
    @pytest.mark.asyncio
    async def test_create_resource_version(
        self, db: AsyncSession, random_resource: Resource, cleanup: CleanupList
    ) -> None:
        """
        Test for creating a new resource version with the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_resource : clowm.models.Resource
            Random resource for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        release = random_lower_string(8)

        resource_version = await CRUDResourceVersion(db=db).create(
            resource_id=random_resource.resource_id, release=release
        )

        assert resource_version is not None

        async def delete_resource_version() -> None:
            await db.execute(
                delete(ResourceVersion).where(
                    ResourceVersion.resource_version_id == resource_version.resource_version_id
                )
            )
            await db.commit()

        cleanup.add_task(delete_resource_version)

        created_resource_version = await db.scalar(
            select(ResourceVersion).where(ResourceVersion.resource_version_id == resource_version.resource_version_id)
        )
        assert created_resource_version is not None
        assert created_resource_version == resource_version

        assert resource_version.status == ResourceVersion.ResourceVersionStatus.RESOURCE_REQUESTED

    @pytest.mark.asyncio
    async def test_create_resource_version_with_wrong_resource_id(self, db: AsyncSession) -> None:
        """
        Test for creating a new resource version with a wrong resource id the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        with pytest.raises(IntegrityError):
            await CRUDResourceVersion(db=db).create(resource_id=uuid4(), release=random_lower_string(8))
