import re

from fastapi import status
from httpx import Request, Response

from ..utils import random_lower_string
from .mock_http_service import MockHTTPService

registry_regex = re.compile(r"/v2/\w+[\w\-/]*\w+/manifests/[\w\-.]+$")
secret_token = random_lower_string()


__all__ = ["MockContainerRegistry"]


class MockContainerRegistry(MockHTTPService):
    def handle_request(self, request: Request) -> Response:
        if self.send_error:
            pass
        elif request.method == "GET" and request.url.path == "/v2/":
            return Response(
                status_code=status.HTTP_401_UNAUTHORIZED,
                headers={
                    "www-authenticate": 'Bearer realm="https://registry-1.docker.io/token",service="container-registry"',
                    "content-type": "application/json",
                },
                content=b"{ 'msg': 'unauthorized' }",
            )
        elif request.method == "GET" and request.url.path == "/token":
            if (
                request.url.params.get("service", None) == "container-registry"
                and request.url.params.get("scope", "").startswith("repository:")
                and request.url.params.get("scope", "").endswith(":pull")
            ):
                return Response(
                    status_code=status.HTTP_200_OK,
                    headers={"content-type": "application/json"},
                    content=f'{{ "token": "{secret_token}"}}'.encode(),
                )
        elif (
            request.method == "HEAD"
            and registry_regex.match(request.url.path)
            and request.headers.get("Authorization", "") == f"Bearer {secret_token}"
        ):
            return Response(status_code=status.HTTP_200_OK)
        return Response(status_code=status.HTTP_404_NOT_FOUND)
