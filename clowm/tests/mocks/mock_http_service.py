import json
from abc import ABC, abstractmethod
from io import BytesIO
from zipfile import ZipFile

from fastapi import status
from httpx import ByteStream, Request, Response

from clowm.schemas.documentation_enum import DocumentationEnum

_response_bytes = json.dumps(
    {
        # When checking if a file exists in a git repository, the GitHub API expects this in a response
        "download_url": "https://example.com",
    }
).encode("utf-8")
zip_buffer = BytesIO()
with ZipFile(file=zip_buffer, mode="w") as z:
    for document in DocumentationEnum:
        z.writestr("repo/" + document.repository_paths[0], data=b"{}")
    z.writestr("repo/main.nf", data=b"")
    z.writestr("repo/alternative.json", data=b"{}")

__all__ = ["MockHTTPService", "DefaultMockHTTPService"]


class MockHTTPService(ABC):
    def __init__(self) -> None:
        self.send_error = False

    @abstractmethod
    def handle_request(self, request: Request) -> Response: ...

    def reset(self) -> None:
        self.send_error = False


class DefaultMockHTTPService(MockHTTPService):
    def handle_request(self, request: Request) -> Response:
        global zip_buffer
        # Stream response for documentation endpoint
        if "archive.zip" in request.url.path or "zipball" in request.url.path:
            zip_buffer.seek(0)
            return Response(
                status_code=status.HTTP_404_NOT_FOUND if self.send_error else status.HTTP_200_OK,
                content=zip_buffer.read(),
            )
        return Response(
            status_code=status.HTTP_404_NOT_FOUND if self.send_error else status.HTTP_200_OK,
            headers={"content-type": "application/json"},
            stream=ByteStream(_response_bytes),
        )
