from clowm.api.container_image_check import AuthenticateHeader, ContainerImage


class TestContainerImage:
    def test_docker_registry_check(self) -> None:
        assert ContainerImage(image="ubuntu", tag="latest").docker_image
        assert ContainerImage(image="ontresearch/wf-human-variation", tag="latest").docker_image
        assert not ContainerImage(image="gcr.io/distroless/static-debian12", tag="latest").docker_image
        assert not ContainerImage(image="quay.io/distroless/static-debian12", tag="latest").docker_image
        assert not ContainerImage(
            image="gitlab.ub.uni-bielefeld.de:4567/cmg/clowm/static-debian12", tag="latest"
        ).docker_image

    def test_parse_repo(self) -> None:
        assert ContainerImage(image="ubuntu", tag="latest").repo == "library/ubuntu"
        assert (
            ContainerImage(image="gcr.io/distroless/static-debian12", tag="latest").repo == "distroless/static-debian12"
        )
        assert ContainerImage(image="quay.io/static-debian12", tag="latest").repo == "static-debian12"
        assert (
            ContainerImage(image="gitlab.ub.uni-bielefeld.de:4567/cmg/clowm/static-debian12", tag="latest").repo
            == "cmg/clowm/static-debian12"
        )
        assert (
            ContainerImage(image="ontresearch/wf-human-variation", tag="latest").repo
            == "ontresearch/wf-human-variation"
        )

    def test_parse_registry(self) -> None:
        assert str(ContainerImage(image="ubuntu", tag="latest").registry) == "https://registry-1.docker.io/v2/"
        assert (
            str(ContainerImage(image="ontresearch/wf-human-variation", tag="latest").registry)
            == "https://registry-1.docker.io/v2/"
        )
        assert (
            str(ContainerImage(image="gcr.io/distroless/static-debian12", tag="latest").registry)
            == "https://gcr.io/v2/"
        )
        assert str(ContainerImage(image="quay.io/static-debian12", tag="latest").registry) == "https://quay.io/v2/"
        assert (
            str(
                ContainerImage(image="gitlab.ub.uni-bielefeld.de:4567/cmg/clowm/static-debian12", tag="latest").registry
            )
            == "https://gitlab.ub.uni-bielefeld.de:4567/v2/"
        )

    def test_parse_image_string(self) -> None:
        image1 = ContainerImage.from_string("ubuntu:latest")
        assert image1.tag == "latest"
        assert image1.image == "ubuntu"

        image2 = ContainerImage.from_string("gcr.io/distroless/static-debian12:nonroot")
        assert image2.tag == "nonroot"
        assert image2.image == "gcr.io/distroless/static-debian12"

        image3 = ContainerImage.from_string("quay.io/static-debian12:latest")
        assert image3.tag == "latest"
        assert image3.image == "quay.io/static-debian12"

        image4 = ContainerImage.from_string("gitlab.ub.uni-bielefeld.de:4567/cmg/clowm/static-debian12:nonroot")
        assert image4.tag == "nonroot"
        assert image4.image == "gitlab.ub.uni-bielefeld.de:4567/cmg/clowm/static-debian12"

        image4 = ContainerImage.from_string("ontresearch/wf-human-variation:latest")
        assert image4.tag == "latest"
        assert image4.image == "ontresearch/wf-human-variation"


class TestAuthenticateHeader:
    def test_parse_header(self) -> None:
        header = AuthenticateHeader.parse_string('Bearer realm="https://example.com",service="example-repository"')
        assert header.service == "example-repository"
        assert str(header.realm) == "https://example.com/"
