from clowm.api.utils import flatten_parameters


class TestFlattenParameters:
    def test_identity_function(self) -> None:
        original = {
            "foo": "bar",
            "foz": 123,
            "faz": 1.23,
            "fuz": True,
        }
        flat = flatten_parameters(original)
        assert len(original.keys()) == len(flat.keys())
        for key in original:
            assert original[key] == flat[key]

    def test_one_level(self) -> None:
        original = {
            "foo": "bar",
            "foz": {"faz": "baz", "fuz": "buz"},
        }
        flat = flatten_parameters(original)
        assert len(flat.keys()) == 3
        assert flat["foo"] == "bar"
        assert flat["foz.faz"] == "baz"
        assert flat["foz.fuz"] == "buz"

    def test_two_level(self) -> None:
        original = {
            "foo": "bar",
            "foz": {"faz": {"fuz": "buz"}, "foz": "boz"},
        }
        flat = flatten_parameters(original)
        assert len(flat.keys()) == 3
        assert flat["foo"] == "bar"
        assert flat["foz.faz.fuz"] == "buz"
        assert flat["foz.foz"] == "boz"
