import pytest
from pydantic import AnyHttpUrl

from clowm.api.dependencies.pagination import HTTPLinkHeader


class TestLinkHeader:
    def test_render_value(self) -> None:
        """
        Test rendering of the HTTP link header value
        """
        assert HTTPLinkHeader.header_key.lower() == "link"
        header = HTTPLinkHeader().add(link=AnyHttpUrl("https://example.org"), rel="abc")
        assert header.render_header_value() == '<https://example.org/>; rel="abc"'
        header.add(link=AnyHttpUrl("https://example.org/example"), rel="def")
        assert (
            header.render_header_value()
            == '<https://example.org/>; rel="abc", <https://example.org/example>; rel="def"'
        )

    def test_parse_value(self) -> None:
        """
        Test parsing of the HTTP link header value
        """

        link_rels = HTTPLinkHeader.parse('<https://example.org/>; rel="abc", <http://example.org/example>; rel="def"')
        assert len(link_rels) == 2
        assert str(link_rels[0].link) == "https://example.org/"
        assert link_rels[0].rel == "abc"
        assert str(link_rels[1].link) == "http://example.org/example"
        assert link_rels[1].rel == "def"

        assert link_rels.get("impossible") is None
        link = link_rels.get("abc")
        assert link is not None
        assert str(link.link) == "https://example.org/"

        with pytest.raises(KeyError):
            assert link_rels["impossible"]
        with pytest.raises(ValueError):
            assert link_rels[3.12]  # type: ignore[index]

    def test_parse_and_render_value(self) -> None:
        """
        Test parsing of the HTTP link header value
        """
        header = '<https://example.org/>; rel="abc", <http://example.org/example>; rel="def"'
        assert HTTPLinkHeader.parse(header).render_header_value() == header
