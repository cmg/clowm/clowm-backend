from uuid import uuid4

from clowm.schemas.user import UserOutExtended


class TestGravatar:
    def test_gravatar_url_for_none_email(self) -> None:
        assert (
            str(UserOutExtended(uid=uuid4(), display_name="", roles=[], email=None).gravatar_url)
            == "https://gravatar.com/avatar/87a8c45825eb709ee8e453d2c0f8154e1b9bf4b45bb9b81b7662177340aa9d38"
        )

    def test_gravatar_url_for_email(self) -> None:
        assert (
            str(UserOutExtended(uid=uuid4(), display_name="", roles=[], email="example@example.org").gravatar_url)
            == "https://gravatar.com/avatar/2f0f47c7ed07a0d1a6fe262e7ced0094b2d66032d4393dbb4aa6acbe9fd2af83"
        )
