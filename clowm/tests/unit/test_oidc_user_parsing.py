import pytest
from authlib.oidc.core.claims import UserInfo as AuthlibUserInfo
from pydantic import ValidationError

from clowm.core.oidc import lifescience_parse_userinfo, nfdi_parse_userinfo
from clowm.tests.utils import random_lower_string


class TestLifeScienceAAIParsing:
    def test_valid_user_info_without_email(self) -> None:
        """
        Test parsing valid user info from LifeScience AAI without email claim.
        """
        lifescience_id = random_lower_string(32)
        user_info = AuthlibUserInfo(
            {
                "sub": f"{lifescience_id}@lifescience.org",
                "name": random_lower_string(),
            }
        )
        user = lifescience_parse_userinfo(user_info)
        assert user.sub == lifescience_id
        assert user.name == user_info["name"]
        assert user.email is None

    def test_valid_user_info_with_verified_email(self) -> None:
        """
        Test parsing valid user info from LifeScience AAI with verified email.
        """
        lifescience_id = random_lower_string(32)
        user_info = AuthlibUserInfo(
            {
                "sub": lifescience_id,
                "name": random_lower_string(),
                "email": f"{random_lower_string()}@example.org",
                "email_verified": True,
            }
        )
        user = lifescience_parse_userinfo(user_info)
        assert user.sub == lifescience_id
        assert user.name == user_info["name"]
        assert user.email == user_info["email"]

    def test_valid_user_info_with_unverified_email(self) -> None:
        """
        Test parsing valid user info from LifeScience AAI with unverified email.
        """
        lifescience_id = random_lower_string(32)
        user_info = AuthlibUserInfo(
            {
                "sub": lifescience_id,
                "name": random_lower_string(),
                "email": f"{random_lower_string()}@example.org",
                "email_verified": False,
            }
        )
        user = lifescience_parse_userinfo(user_info)
        assert user.sub == lifescience_id
        assert user.name == user_info["name"]
        assert user.email is None

    def test_invalid_user_info_without_sub(self) -> None:
        """
        Test parsing invalid user info from LifeScience AAI without sub claim.
        """
        user_info = AuthlibUserInfo(
            {
                "name": random_lower_string(),
                "email": random_lower_string(),
            }
        )
        with pytest.raises(ValidationError):
            lifescience_parse_userinfo(user_info)

    def test_invalid_user_info_without_name(self) -> None:
        """
        Test parsing valid user info from LifeScience AAI without name claim.
        """
        user_info = AuthlibUserInfo(
            {
                "sub": random_lower_string(),
                "email": f"{random_lower_string()}@example.org",
            }
        )
        with pytest.raises(ValidationError):
            lifescience_parse_userinfo(user_info)


class TestNFDIAAIParsing:
    def test_valid_user_info_without_email(self) -> None:
        """
        Test parsing valid user info from LifeScience AAI without email claim.
        """
        lifescience_id = random_lower_string(32)
        user_info = AuthlibUserInfo(
            {
                "sub": lifescience_id,
                "name": random_lower_string(),
            }
        )
        user = nfdi_parse_userinfo(user_info)
        assert user.sub == lifescience_id
        assert user.name == user_info["name"]
        assert user.email is None

    def test_valid_user_info_with_email(self) -> None:
        """
        Test parsing valid user info from LifeScience AAI with verified email.
        """
        lifescience_id = random_lower_string(32)
        user_info = AuthlibUserInfo(
            {
                "sub": lifescience_id,
                "name": random_lower_string(),
                "email": random_lower_string(),
            }
        )
        user = nfdi_parse_userinfo(user_info)
        assert user.sub == lifescience_id
        assert user.name == user_info["name"]
        assert user.email == user_info["email"]

    def test_invalid_user_info_without_sub(self) -> None:
        """
        Test parsing invalid user info from LifeScience AAI without sub claim.
        """
        user_info = AuthlibUserInfo(
            {
                "name": random_lower_string(),
                "email": random_lower_string(),
            }
        )
        with pytest.raises(ValidationError):
            nfdi_parse_userinfo(user_info)

    def test_invalid_user_info_without_name(self) -> None:
        """
        Test parsing valid user info from LifeScience AAI without name claim.
        """
        user_info = AuthlibUserInfo(
            {
                "sub": random_lower_string(),
                "email": f"{random_lower_string()}@example.org",
            }
        )
        user = nfdi_parse_userinfo(user_info)
        assert user.sub == user_info["sub"]
        assert len(user.name.strip()) == 0

    def test_user_info_with_given_and_family_name(self) -> None:
        """
        Test parsing valid user info from LifeScience AAI without name claim.
        """
        user_info = AuthlibUserInfo(
            {
                "sub": random_lower_string(),
                "given_name": random_lower_string(8),
                "family_name": random_lower_string(8),
            }
        )
        user = nfdi_parse_userinfo(user_info)
        assert user.sub == user_info["sub"]
        assert user.name == f"{user_info['given_name']} {user_info['family_name']}"
