import pytest
from fastapi import status
from httpx import AsyncClient
from pydantic import TypeAdapter

from clowm.api.dependencies.v1.auth import header_token_scheme
from clowm.models import ApiToken, WorkflowVersion
from clowm.schemas.workflow import WorkflowOut
from clowm.schemas.workflow_version import (
    WorkflowVersionMetadataOut,
)
from clowm.schemas.workflow_version import WorkflowVersionOut as WorkflowVersionOut
from clowm.tests.utils import random_hex_string

WorkflowVersionList = TypeAdapter(list[WorkflowVersionOut])


class _TestWorkflowVersionRoutes:
    base_path: str = "/v1/workflows"


class TestWorkflowVersionRoutesGet(_TestWorkflowVersionRoutes):
    @pytest.mark.asyncio
    async def test_get_workflow_version(
        self,
        client: AsyncClient,
        random_workflow_version: WorkflowVersion,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for getting a workflow version by its commit hash.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.

        random_workflow_version : clowm.schemas.workflow_version.WorkflowVersionOut
            Random workflow version for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow_version.workflow_id),
                    "versions",
                    random_workflow_version.git_commit_hash,
                ]
            ),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_200_OK
        version = WorkflowVersionOut.model_validate_json(response.content)
        assert version.workflow_id == random_workflow_version.workflow_id
        assert version.workflow_version_id == random_workflow_version.git_commit_hash

    @pytest.mark.asyncio
    async def test_get_non_existing_version(
        self,
        client: AsyncClient,
        random_workflow: WorkflowOut,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for getting a non-existing workflow version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(
            "/".join([self.base_path, str(random_workflow.workflow_id), "versions", random_hex_string()]),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_get_workflow_version_meta_data(
        self,
        client: AsyncClient,
        random_workflow_version: WorkflowVersion,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for getting a workflow version by its commit hash.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_workflow_version : clowm.schemas.workflow_version.WorkflowVersionOut
            Random workflow version for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow_version.workflow_id),
                    "versions",
                    random_workflow_version.git_commit_hash,
                    "metadata",
                ]
            ),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_200_OK
        metadata = WorkflowVersionMetadataOut.model_validate_json(response.content)
        assert metadata.nextflow_version == random_workflow_version.nextflow_version
        assert metadata.nextflow_config == random_workflow_version.nextflow_config
        assert metadata.default_container == random_workflow_version.default_container


class TestWorkflowVersionRoutesList(_TestWorkflowVersionRoutes):
    @pytest.mark.asyncio
    async def test_get_workflow_version(
        self,
        client: AsyncClient,
        random_workflow: WorkflowOut,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for listing all workflow version from a given workflow.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.

        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(
            "/".join([self.base_path, str(random_workflow.workflow_id), "versions"]),
            params={"version_status": WorkflowVersion.WorkflowVersionStatus.CREATED.name},
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_200_OK
        versions = WorkflowVersionList.validate_json(response.content)
        assert len(versions) == 1
        assert versions[0].workflow_id == random_workflow.workflow_id
        assert versions[0].workflow_version_id == random_workflow.versions[0].workflow_version_id


class TestWorkflowVersionRoutesUpdate(_TestWorkflowVersionRoutes):
    @pytest.mark.asyncio
    async def test_deprecate_workflow_version(
        self,
        client: AsyncClient,
        random_workflow: WorkflowOut,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test deprecate a workflow version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.patch(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow.workflow_id),
                    "versions",
                    random_workflow.versions[0].workflow_version_id,
                    "deprecate",
                ]
            ),
            headers={header_token_scheme.model.name: random_api_token.token},
        )

        assert response.status_code == status.HTTP_200_OK
        version = WorkflowVersionOut.model_validate_json(response.content)
        assert version.workflow_version_id == random_workflow.versions[0].workflow_version_id
        assert version.status == WorkflowVersion.WorkflowVersionStatus.DEPRECATED

    @pytest.mark.asyncio
    async def test_deprecate_non_existing_workflow_version(
        self,
        client: AsyncClient,
        random_workflow: WorkflowOut,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test deprecate a non-existing workflow version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.patch(
            "/".join([self.base_path, str(random_workflow.workflow_id), "versions", random_hex_string(), "deprecate"]),
            headers={header_token_scheme.model.name: random_api_token.token},
        )

        assert response.status_code == status.HTTP_404_NOT_FOUND
