import json
from typing import TYPE_CHECKING
from uuid import uuid4

import pytest
from botocore.exceptions import ClientError
from fastapi import status
from httpx import AsyncClient
from pydantic import TypeAdapter
from sqlalchemy import delete, select
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.api.dependencies.v1.auth import header_token_scheme
from clowm.core.config import settings
from clowm.models import ApiToken, Resource, ResourceVersion
from clowm.schemas.resource import ResourceIn, ResourceOut
from clowm.schemas.resource_version import resource_version_key
from clowm.tests.utils import CleanupList, delete_policy_statement, random_lower_string

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object


class _TestResourceRoutes:
    base_path: str = "/v1/resources"


class TestResourceRouteCreate(_TestResourceRoutes):
    @pytest.mark.asyncio
    async def test_create_resource_route(
        self,
        client: AsyncClient,
        db: AsyncSession,
        cleanup: CleanupList,
        mock_s3_service: S3ServiceResource,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for creating a new resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        resource = ResourceIn(
            release=random_lower_string(6),
            name=random_lower_string(8),
            description=random_lower_string(16),
            source=random_lower_string(8),
        )
        response = await client.post(
            self.base_path,
            content=resource.model_dump_json(),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_201_CREATED
        resource_out = ResourceOut.model_validate_json(response.content)

        async def delete_resource() -> None:
            await db.execute(delete(Resource).where(Resource.resource_id == resource_out.resource_id))
            await db.commit()

        cleanup.add_task(delete_resource)
        s3_policy = await mock_s3_service.BucketPolicy(settings.s3.data_bucket)
        cleanup.add_task(delete_policy_statement, s3policy=s3_policy, sid=str(resource_out.resource_id))

        assert resource_out.name == resource.name
        assert len(resource_out.versions) == 1
        resource_version = resource_out.versions[0]
        assert resource_version.resource_id == resource_out.resource_id
        assert resource_version.status == ResourceVersion.ResourceVersionStatus.RESOURCE_REQUESTED
        assert resource_version.release == resource.release

        # test if the maintainer get permission to upload to the resource bucket
        s3_policy = await mock_s3_service.BucketPolicy(settings.s3.data_bucket)
        policy = json.loads(await s3_policy.policy)
        assert sum(1 for stmt in policy["Statement"] if stmt["Sid"] == str(resource_out.resource_id)) == 1
        assert sum(1 for stmt in policy["Statement"] if stmt["Sid"] == str(resource_version.resource_version_id)) == 1

        # test that the resource got actually created
        resource_db = await db.scalar(select(Resource).where(Resource.resource_id == resource_out.resource_id))
        assert resource_db is not None

    @pytest.mark.asyncio
    async def test_create_duplicated_resource_route(
        self, client: AsyncClient, db: AsyncSession, random_resource: Resource, random_api_token: ApiToken
    ) -> None:
        """
        Test for creating a duplicated resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_resource : clowm.models.Resource
            Random resource for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        resource = ResourceIn(
            release=random_lower_string(6),
            name=random_resource.name,
            description=random_lower_string(16),
            source=random_lower_string(8),
        )
        response = await client.post(
            self.base_path,
            content=resource.model_dump_json(),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST


class TestResourceRouteDelete(_TestResourceRoutes):
    @pytest.mark.asyncio
    async def test_delete_resource_route(
        self,
        client: AsyncClient,
        random_resource: Resource,
        mock_s3_service: S3ServiceResource,
        cleanup: CleanupList,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for deleting a resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowm.models.Resource
            Random resource for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.delete(
            f"{self.base_path}/{str(random_resource.resource_id)}",
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT

        # test if all permission for the maintainer got deleted in S3
        s3_policy = await mock_s3_service.BucketPolicy(settings.s3.data_bucket)
        policy = json.loads(await s3_policy.policy)
        assert (
            sum(
                1
                for stmt in policy["Statement"]
                if stmt["Sid"]
                in [str(random_resource.resource_id)] + [str(rv.resource_version_id) for rv in random_resource.versions]
            )
            == 0
        )

        # test if the resource got deleted in S3
        s3_key = resource_version_key(random_resource.resource_id, random_resource.versions[0].resource_version_id)
        with pytest.raises(ClientError):
            obj = await mock_s3_service.ObjectSummary(settings.s3.data_bucket, s3_key)
            await obj.load()


class TestResourceRouteList(_TestResourceRoutes):
    @pytest.mark.asyncio
    async def test_list_resources_route(
        self, client: AsyncClient, random_resource: Resource, random_api_token: ApiToken
    ) -> None:
        """
        Test for listing all resources.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowm.models.Resource
            Random resource for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(self.base_path, headers={header_token_scheme.model.name: random_api_token.token})
        assert response.status_code == status.HTTP_200_OK
        ta = TypeAdapter(list[ResourceOut])
        resources = ta.validate_json(response.content)
        assert len(resources) == 1


class TestResourceRouteGet(_TestResourceRoutes):
    @pytest.mark.asyncio
    async def test_get_resource_route(
        self, client: AsyncClient, random_resource: Resource, random_api_token: ApiToken
    ) -> None:
        """
        Test for getting a specific resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowm.models.Resource
            Random resource for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(
            f"{self.base_path}/{str(random_resource.resource_id)}",
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_200_OK
        resource = ResourceOut.model_validate_json(response.content)
        assert resource.resource_id == random_resource.resource_id

    @pytest.mark.asyncio
    async def test_get_non_existing_resource_route(
        self, client: AsyncClient, random_resource: Resource, random_api_token: ApiToken
    ) -> None:
        """
        Test for getting a non-existing resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
            HTTP Client to perform the request on.
        random_resource : clowm.models.Resource
            Random resource for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(
            f"{self.base_path}/{str(uuid4())}", headers={header_token_scheme.model.name: random_api_token.token}
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND
