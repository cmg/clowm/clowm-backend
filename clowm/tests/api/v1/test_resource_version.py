import json
from io import BytesIO
from typing import TYPE_CHECKING
from uuid import uuid4

import pytest
from fastapi import status
from httpx import AsyncClient

from clowm.api.dependencies.v1.auth import header_token_scheme
from clowm.core.config import settings
from clowm.models import ApiToken, Resource, ResourceVersion
from clowm.schemas.resource_version import (
    ResourceVersionIn,
    ResourceVersionOut,
    UserSynchronizationRequestIn,
    resource_version_dir_name,
    resource_version_key,
)
from clowm.tests.utils import CleanupList, random_lower_string

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object


class _TestResourceVersionRoutes:
    base_path: str = "/v1/resources"


class TestResourceVersionRouteCreate(_TestResourceVersionRoutes):
    @pytest.mark.asyncio
    async def test_create_resource_route(
        self,
        client: AsyncClient,
        random_resource: Resource,
        cleanup: CleanupList,
        mock_s3_service: S3ServiceResource,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for creating a new resource version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowm.models.Resource
            Random resource for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        resource_version = ResourceVersionIn(release=random_lower_string(6))
        response = await client.post(
            f"{self.base_path}/{str(random_resource.resource_id)}/versions",
            content=resource_version.model_dump_json(),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_201_CREATED
        resource_version_out = ResourceVersionOut.model_validate_json(response.content)

        # Load bucket policy
        s3_policy = await mock_s3_service.BucketPolicy(settings.s3.data_bucket)
        policy = json.loads(await s3_policy.policy)

        # delete create bucket policy statement after the test
        async def delete_policy_stmt() -> None:
            policy["Statement"] = [
                stmt for stmt in policy["Statement"] if stmt["Sid"] != str(resource_version_out.resource_version_id)
            ]
            await s3_policy.put(Policy=json.dumps(policy))

        cleanup.add_task(delete_policy_stmt)

        # check if a bucket policy statement was created
        assert (
            sum(1 for stmt in policy["Statement"] if stmt["Sid"] == str(resource_version_out.resource_version_id)) == 1
        )


class TestResourceVersionRouteGet(_TestResourceVersionRoutes):
    @pytest.mark.asyncio
    async def test_list_resource_versions_route(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_resource_version: ResourceVersion,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for listing all resource versions of a resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowm.models.Resource
            Random resource for testing.
        random_resource_version : clowm.models.Resource
            Random resource version for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(
            "/".join([self.base_path, str(random_resource.resource_id), "versions"]),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.asyncio
    async def test_get_resource_version_route(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_resource_version: ResourceVersion,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for getting a specif resource version of a resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowm.models.Resource
            Random resource for testing.
        random_resource_version : clowm.models.Resource
            Random resource version for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(
            "/".join(
                [
                    self.base_path,
                    str(random_resource.resource_id),
                    "versions",
                    str(random_resource_version.resource_version_id),
                ]
            ),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.asyncio
    async def test_get_non_existing_resource_route(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for getting a non-existing resource version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowm.models.Resource
            Random resource for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(
            f"{self.base_path}/{str(random_resource.resource_id)}/versions/{str(uuid4())}",
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_get_resource_version_folder_tree(
        self,
        client: AsyncClient,
        random_resource_version_states: ResourceVersion,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for getting the folder structure of a resource version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource_version_states : clowm.models.ResourceVersion
            Random resource version with all possible states for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(
            f"{self.base_path}/{str(random_resource_version_states.resource_id)}/versions/{str(random_resource_version_states.resource_version_id)}/tree",
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        if random_resource_version_states.status.public:
            if random_resource_version_states.status in [
                ResourceVersion.ResourceVersionStatus.SYNCHRONIZED,
                ResourceVersion.ResourceVersionStatus.LATEST,
                ResourceVersion.ResourceVersionStatus.SETTING_LATEST,
            ]:
                assert response.status_code == status.HTTP_200_OK
                response2 = await client.get(
                    f"{self.base_path}/{str(random_resource_version_states.resource_id)}/versions/{str(random_resource_version_states.resource_version_id)}/tree",
                    headers={
                        "if-none-match": response.headers.get("etag", ""),
                        header_token_scheme.model.name: random_api_token.token,
                    },
                )
                assert response2.status_code == status.HTTP_304_NOT_MODIFIED
            else:
                assert response.status_code == status.HTTP_404_NOT_FOUND

        else:
            assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_get_resource_version_folder_tree_large_file(
        self,
        client: AsyncClient,
        random_resource_version: ResourceVersion,
        cleanup: CleanupList,
        mock_s3_service: S3ServiceResource,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for getting the folder structure of a resource version when the file is larger than 10MiB.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource_version : clowm.models.ResourceVersion
            Random resource version for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        # put large file containing the directory structure in S3
        s3_bucket = await mock_s3_service.Bucket(settings.s3.data_bucket)
        s3_key = (
            resource_version_dir_name(random_resource_version.resource_id, random_resource_version.resource_version_id)
            + "/tree.json"
        )
        await s3_bucket.upload_fileobj(
            BytesIO(b"0" * 10500000),
            Key=s3_key,
        )

        cleanup.add_task(
            s3_bucket.delete_objects,
            Delete={"Objects": [{"Key": s3_key}]},
        )
        response = await client.get(
            f"{self.base_path}/{str(random_resource_version.resource_id)}/versions/{str(random_resource_version.resource_version_id)}/tree",
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_200_OK


class TestResourceVersionRoutePutReview(_TestResourceVersionRoutes):
    @pytest.mark.asyncio
    async def test_request_review_resource_version_route_without_uploaded_resource(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_resource_version_states: ResourceVersion,
        mock_s3_service: S3ServiceResource,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for requesting a review of a resource version without uploaded resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowm.models.Resource
            Random resource for testing.
        random_resource_version_states : clowm.models.Resource
            Random resource version with all possible states for testing.

        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        s3_bucket = await mock_s3_service.Bucket(settings.s3.data_bucket)
        s3_key = resource_version_key(random_resource.resource_id, random_resource_version_states.resource_version_id)
        await s3_bucket.delete_objects(Delete={"Objects": [{"Key": s3_key}]})

        response = await client.put(
            "/".join(
                [
                    self.base_path,
                    str(random_resource.resource_id),
                    "versions",
                    str(random_resource_version_states.resource_version_id),
                    "request_review",
                ]
            ),
            content=UserSynchronizationRequestIn(reason=random_lower_string(32)).model_dump_json(),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_request_review_resource_version_route_with_uploaded_resource(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_resource_version_states: ResourceVersion,
        mock_s3_service: S3ServiceResource,
        cleanup: CleanupList,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for requesting a review of a resource version with uploaded resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowm.models.Resource
            Random resource for testing.
        random_resource_version_states : clowm.models.Resource
            Random resource version with all possible states for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        resource_content = b"content"
        previous_state = random_resource_version_states.status
        if previous_state == ResourceVersion.ResourceVersionStatus.RESOURCE_REQUESTED:
            s3_bucket = await mock_s3_service.Bucket(settings.s3.data_bucket)
            s3_key = resource_version_key(
                random_resource.resource_id, random_resource_version_states.resource_version_id
            )
            await s3_bucket.upload_fileobj(
                BytesIO(resource_content),
                Key=s3_key,
            )
            cleanup.add_task(
                s3_bucket.delete_objects,
                Delete={"Objects": [{"Key": s3_key}]},
            )

        response = await client.put(
            "/".join(
                [
                    self.base_path,
                    str(random_resource.resource_id),
                    "versions",
                    str(random_resource_version_states.resource_version_id),
                    "request_review",
                ]
            ),
            content=UserSynchronizationRequestIn(reason=random_lower_string(32)).model_dump_json(),
            headers={header_token_scheme.model.name: random_api_token.token},
        )

        if previous_state is ResourceVersion.ResourceVersionStatus.RESOURCE_REQUESTED:
            assert response.status_code == status.HTTP_200_OK
            updated_response = ResourceVersionOut.model_validate_json(response.content)
            assert updated_response.status == ResourceVersion.ResourceVersionStatus.WAIT_FOR_REVIEW
            assert updated_response.compressed_size == len(resource_content)
            # test if the maintainer gets a permission to upload to the bucket
            s3_policy = await mock_s3_service.BucketPolicy(settings.s3.data_bucket)
            policy = json.loads(await s3_policy.policy)
            assert (
                sum(
                    1
                    for stmt in policy["Statement"]
                    if stmt["Sid"] == str(random_resource_version_states.resource_version_id)
                )
                == 0
            )
        else:
            assert response.status_code == status.HTTP_400_BAD_REQUEST
