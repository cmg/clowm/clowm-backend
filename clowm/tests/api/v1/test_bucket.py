from io import BytesIO
from typing import TYPE_CHECKING

import pytest
from fastapi import status
from httpx import AsyncClient
from pydantic import TypeAdapter
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.api.core.bucket import ANONYMOUS_ACCESS_SID
from clowm.api.dependencies.v1.auth import header_token_scheme
from clowm.core.config import settings
from clowm.models import ApiToken, Bucket
from clowm.schemas.bucket import BucketIn, BucketOut
from clowm.tests.utils import CleanupList, UserWithAuthCookie, random_lower_string
from clowm.tests.utils.bucket import add_permission_for_bucket, delete_bucket, make_bucket_public

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object


class _TestBucketRoutes:
    base_path = "/v1/buckets"


class TestBucketRoutesList(_TestBucketRoutes):
    @pytest.mark.asyncio
    async def test_get_all_buckets(
        self, client: AsyncClient, random_bucket: Bucket, random_api_token: ApiToken
    ) -> None:
        """
        Test for getting all buckets with "list_all" operation.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        """
        response = await client.get(self.base_path, headers={header_token_scheme.model.name: random_api_token.token})
        assert response.status_code == status.HTTP_200_OK

        ta = TypeAdapter(list[BucketOut])
        buckets = ta.validate_json(response.content)
        assert len(buckets) == 1
        bucket = buckets[0]

        assert bucket.name == random_bucket.name
        assert bucket.owner_id == random_bucket.owner_id

    @pytest.mark.asyncio
    async def test_get_own_buckets(
        self, client: AsyncClient, random_bucket: Bucket, random_api_token: ApiToken
    ) -> None:
        """
        Test for getting the buckets where the user is the owner.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(
            self.base_path,
            params={"owner_id": str(random_bucket.owner_id)},
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_200_OK

        ta = TypeAdapter(list[BucketOut])
        buckets = ta.validate_json(response.content)

        assert len(buckets) == 1
        bucket = buckets[0]

        assert bucket.name == random_bucket.name
        assert bucket.owner_id == random_bucket.owner_id


class TestBucketRoutesGet(_TestBucketRoutes):
    @pytest.mark.asyncio
    async def test_get_bucket_by_name(
        self, client: AsyncClient, random_bucket: Bucket, random_api_token: ApiToken
    ) -> None:
        """
        Test for getting a bucket by its name where the user is the owner of the bucket.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(
            f"{self.base_path}/{random_bucket.name}", headers={header_token_scheme.model.name: random_api_token.token}
        )
        assert response.status_code == status.HTTP_200_OK

        bucket = BucketOut.model_validate_json(response.content)

        assert bucket.name == random_bucket.name
        assert bucket.owner_id == random_bucket.owner_id

    @pytest.mark.asyncio
    async def test_get_unknown_bucket(self, client: AsyncClient, random_api_token: ApiToken) -> None:
        """
        Test for getting an unknown bucket by its name.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        """
        response = await client.get(
            f"{self.base_path}/impossible_bucket_name", headers={header_token_scheme.model.name: random_api_token.token}
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_get_foreign_bucket(
        self, client: AsyncClient, random_bucket: Bucket, random_second_api_token: ApiToken
    ) -> None:
        """
        Test for getting a foreign bucket without permission.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_second_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(
            f"{self.base_path}/{random_bucket.name}",
            headers={header_token_scheme.model.name: random_second_api_token.token},
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.asyncio
    async def test_get_foreign_public_bucket(
        self,
        client: AsyncClient,
        random_bucket: Bucket,
        db: AsyncSession,
        mock_s3_service: S3ServiceResource,
        random_second_api_token: ApiToken,
    ) -> None:
        """
        Test for getting a foreign public bucket.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_second_api_token : clowm.models.ApiToken
            Random bucket for testing.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        mock_s3_service: types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        """
        await make_bucket_public(db=db, s3=mock_s3_service, bucket_name=random_bucket.name)
        response = await client.get(
            f"{self.base_path}/{random_bucket.name}",
            headers={header_token_scheme.model.name: random_second_api_token.token},
        )
        assert response.status_code == status.HTTP_200_OK

        bucket = BucketOut.model_validate_json(response.content)

        assert bucket.name == random_bucket.name


class TestBucketRoutesCreate(_TestBucketRoutes):
    @pytest.mark.asyncio
    async def test_create_bucket(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        cleanup: CleanupList,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for creating a bucket.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        bucket_info = BucketIn(name=random_lower_string(), description=random_lower_string(127))
        cleanup.add_task(
            delete_bucket,
            db=db,
            bucket_name=bucket_info.name,
        )
        response = await client.post(
            self.base_path,
            headers={header_token_scheme.model.name: random_api_token.token},
            content=bucket_info.model_dump_json(),
        )

        assert response.status_code == status.HTTP_201_CREATED
        bucket = BucketOut.model_validate_json(response.content)
        assert bucket.name == bucket_info.name
        assert bucket.owner_id == random_user.uid
        assert bucket.size_limit == settings.s3.default_bucket_size_limit.to("KiB")
        assert bucket.object_limit == settings.s3.default_bucket_object_limit

        db_bucket = await db.scalar(select(Bucket).where(Bucket.name == bucket_info.name))
        assert db_bucket is not None
        assert db_bucket.name == bucket_info.name
        assert db_bucket.owner_id == random_user.uid

    @pytest.mark.asyncio
    async def test_create_duplicated_bucket(
        self, client: AsyncClient, random_bucket: Bucket, random_user: UserWithAuthCookie, random_api_token: ApiToken
    ) -> None:
        """
        Test for creating a bucket where the name is already taken.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        bucket_info = BucketIn(name=random_bucket.name, description=random_lower_string(127))
        response = await client.post(
            self.base_path,
            headers={header_token_scheme.model.name: random_api_token.token},
            content=bucket_info.model_dump_json(),
        )

        assert response.status_code == status.HTTP_400_BAD_REQUEST


class TestBucketRoutesUpdate(_TestBucketRoutes):
    @pytest.mark.asyncio
    async def test_make_bucket_public(
        self,
        client: AsyncClient,
        random_bucket: Bucket,
        random_user: UserWithAuthCookie,
        mock_s3_service: S3ServiceResource,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for getting a foreign public bucket.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is owner of the bucket.
        mock_s3_service: types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.patch(
            f"{self.base_path}/{random_bucket.name}/public",
            headers={header_token_scheme.model.name: random_api_token.token},
            json={"public": True},
        )
        assert response.status_code == status.HTTP_200_OK

        bucket = BucketOut.model_validate_json(response.content)

        assert bucket.name == random_bucket.name
        assert bucket.public

        s3_policy = await mock_s3_service.BucketPolicy(bucket.name)
        assert ANONYMOUS_ACCESS_SID in await s3_policy.policy

    @pytest.mark.asyncio
    async def test_make_bucket_private(
        self,
        client: AsyncClient,
        random_bucket: Bucket,
        random_user: UserWithAuthCookie,
        mock_s3_service: S3ServiceResource,
        db: AsyncSession,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for getting a foreign public bucket.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is owner of the bucket.
        mock_s3_service: types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        await make_bucket_public(db=db, s3=mock_s3_service, bucket_name=random_bucket.name)
        response = await client.patch(
            f"{self.base_path}/{random_bucket.name}/public",
            headers={header_token_scheme.model.name: random_api_token.token},
            json={"public": False},
        )
        assert response.status_code == status.HTTP_200_OK

        bucket = BucketOut.model_validate_json(response.content)

        assert bucket.name == random_bucket.name
        assert not bucket.public

        s3_policy = await mock_s3_service.BucketPolicy(bucket.name)
        assert ANONYMOUS_ACCESS_SID not in await s3_policy.policy

    @pytest.mark.asyncio
    async def test_make_private_bucket_private(
        self,
        client: AsyncClient,
        random_bucket: Bucket,
        random_user: UserWithAuthCookie,
        mock_s3_service: S3ServiceResource,
        db: AsyncSession,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for getting a foreign public bucket.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is owner of the bucket.
        mock_s3_service: types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.patch(
            f"{self.base_path}/{random_bucket.name}/public",
            headers={header_token_scheme.model.name: random_api_token.token},
            json={"public": False},
        )
        assert response.status_code == status.HTTP_200_OK

        bucket = BucketOut.model_validate_json(response.content)

        assert bucket.name == random_bucket.name
        assert not bucket.public

        s3_policy = await mock_s3_service.BucketPolicy(bucket.name)
        assert ANONYMOUS_ACCESS_SID not in await s3_policy.policy


class TestBucketRoutesDelete(_TestBucketRoutes):
    @pytest.mark.asyncio
    async def test_delete_empty_bucket(
        self, client: AsyncClient, random_bucket: Bucket, random_user: UserWithAuthCookie, random_api_token: ApiToken
    ) -> None:
        """
        Test for deleting an empty bucket.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.delete(
            f"{self.base_path}/{random_bucket.name}",
            headers={header_token_scheme.model.name: random_api_token.token},
            params={"force_delete": False},
        )

        assert response.status_code == status.HTTP_204_NO_CONTENT

    @pytest.mark.asyncio
    async def test_delete_foreign_bucket_with_permission(
        self,
        client: AsyncClient,
        db: AsyncSession,
        random_second_user: UserWithAuthCookie,
        random_third_user: UserWithAuthCookie,
        cleanup: CleanupList,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for deleting a foreign bucket.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_third_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is not the owner of the bucket.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        bucket = Bucket(
            name=random_lower_string(),
            description=random_lower_string(127),
            owner_id=random_second_user.uid,
        )
        db.add(bucket)
        await db.commit()
        cleanup.add_task(
            delete_bucket,
            db=db,
            bucket_name=bucket.name,
        )
        await add_permission_for_bucket(db, bucket.name, random_third_user.uid)

        response = await client.delete(
            f"{self.base_path}/{bucket.name}", cookies=random_third_user.auth_cookie, params={"force_delete": False}
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.asyncio
    async def test_delete_non_empty_bucket(
        self,
        client: AsyncClient,
        random_bucket: Bucket,
        random_user: UserWithAuthCookie,
        mock_s3_service: S3ServiceResource,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for deleting a non-empty bucket.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        obj = await mock_s3_service.Object(bucket_name=random_bucket.name, key=random_lower_string())
        await obj.upload_fileobj(BytesIO(b""))
        response = await client.delete(
            f"{self.base_path}/{random_bucket.name}",
            headers={header_token_scheme.model.name: random_api_token.token},
            params={"force_delete": False},
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_force_delete_non_empty_bucket(
        self,
        client: AsyncClient,
        random_bucket: Bucket,
        random_user: UserWithAuthCookie,
        mock_s3_service: S3ServiceResource,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for force deleting a non-empty bucket.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        obj = await mock_s3_service.Object(bucket_name=random_bucket.name, key=random_lower_string())
        await obj.upload_fileobj(BytesIO(b""))
        response = await client.delete(
            f"{self.base_path}/{random_bucket.name}",
            headers={header_token_scheme.model.name: random_api_token.token},
            params={"force_delete": True},
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT
