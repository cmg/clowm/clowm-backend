from io import BytesIO
from typing import TYPE_CHECKING
from uuid import uuid4

import pytest
from botocore.exceptions import ClientError
from fastapi import status
from httpx import AsyncClient
from pydantic import TypeAdapter
from sqlalchemy import select, update
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.api.dependencies.v1.auth import header_token_scheme
from clowm.core.config import settings
from clowm.models import ApiToken, WorkflowMode, WorkflowVersion
from clowm.schemas.documentation_enum import DocumentationEnum
from clowm.schemas.workflow import WorkflowOut, WorkflowUpdate
from clowm.schemas.workflow_mode import WorkflowModeIn
from clowm.schemas.workflow_version import NextflowVersion
from clowm.schemas.workflow_version import WorkflowVersionOut as WorkflowVersionOut
from clowm.tests.mocks import DefaultMockHTTPService
from clowm.tests.utils import (
    CleanupList,
    UserWithAuthCookie,
    delete_workflow_mode,
    random_hex_string,
    random_lower_string,
)

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object

WorkflowList = TypeAdapter(list[WorkflowOut])


class _TestWorkflowRoutes:
    base_path: str = "/v1/workflows"


class TestWorkflowRoutesList(_TestWorkflowRoutes):
    @pytest.mark.asyncio
    async def test_list_all_unpublished_workflows(
        self,
        client: AsyncClient,
        random_workflow: WorkflowOut,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for listing all workflows in the system.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(
            self.base_path,
            headers={header_token_scheme.model.name: random_api_token.token},
            params={"version_status": WorkflowVersion.WorkflowVersionStatus.CREATED.name},
        )
        assert response.status_code == status.HTTP_200_OK
        workflows = WorkflowList.validate_json(response.content)
        assert len(workflows) == 1
        assert workflows[0].workflow_id == random_workflow.workflow_id
        assert not workflows[0].private

    @pytest.mark.asyncio
    async def test_list_all_workflows(
        self,
        client: AsyncClient,
        random_workflow: WorkflowOut,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for listing all workflows in the system.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(
            self.base_path,
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_200_OK
        workflows = WorkflowList.validate_json(response.content)
        assert len(workflows) == 0

    @pytest.mark.asyncio
    async def test_list_workflows_by_developer(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow: WorkflowOut,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for listing all workflows in the system.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(
            self.base_path,
            headers={header_token_scheme.model.name: random_api_token.token},
            params={
                "developer_id": str(random_user.uid),
                "version_status": WorkflowVersion.WorkflowVersionStatus.CREATED.name,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        workflows = WorkflowList.validate_json(response.content)
        assert len(workflows) == 1
        assert workflows[0].workflow_id == random_workflow.workflow_id
        assert workflows[0].developer_id == random_user.uid


class TestWorkflowRoutesGet(_TestWorkflowRoutes):
    @pytest.mark.asyncio
    async def test_get_workflow_with_version_status(
        self,
        client: AsyncClient,
        random_workflow: WorkflowOut,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for getting a workflow by its id.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(
            "/".join([self.base_path, str(random_workflow.workflow_id)]),
            params={"version_status": random_workflow.versions[0].status.name},
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_200_OK
        workflow = WorkflowOut.model_validate_json(response.content)
        assert workflow.workflow_id == random_workflow.workflow_id
        assert not workflow.private
        assert len(workflow.versions) > 0

    @pytest.mark.asyncio
    async def test_get_non_existing_workflow(self, client: AsyncClient, random_api_token: ApiToken) -> None:
        """
        Test for getting a non-existing workflow.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(
            "/".join([self.base_path, str(uuid4())]),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND


class TestWorkflowRoutesDelete(_TestWorkflowRoutes):
    @pytest.mark.asyncio
    async def test_delete_workflow(
        self,
        client: AsyncClient,
        random_workflow: WorkflowOut,
        mock_s3_service: S3ServiceResource,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for deleting a workflow by its id.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.delete(
            "/".join([self.base_path, str(random_workflow.workflow_id)]),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT
        icon_slug = str(random_workflow.versions[0].icon_url).split("/")[-1]
        with pytest.raises(ClientError):
            icon_obj = await mock_s3_service.Object(
                bucket_name=settings.s3.data_bucket, key=settings.s3.icon_key(icon_slug)
            )
            await icon_obj.load()
        with pytest.raises(ClientError):
            schema_obj = await mock_s3_service.Object(
                bucket_name=settings.s3.data_bucket,
                key=settings.s3.workflow_cache_key(
                    name=DocumentationEnum.PARAMETER_SCHEMA,
                    git_commit_hash=random_workflow.versions[0].workflow_version_id,
                ),
            )
            await schema_obj.load()

    @pytest.mark.asyncio
    async def test_delete_private_workflow(
        self,
        client: AsyncClient,
        random_private_workflow: WorkflowOut,
        mock_s3_service: S3ServiceResource,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for deleting a workflow by its id.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_private_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.delete(
            "/".join([self.base_path, str(random_private_workflow.workflow_id)]),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT
        icon_slug = str(random_private_workflow.versions[0].icon_url).split("/")[-1]
        with pytest.raises(ClientError):
            icon_obj = await mock_s3_service.Object(
                bucket_name=settings.s3.data_bucket, key=settings.s3.icon_key(icon_slug)
            )
            await icon_obj.load()
        with pytest.raises(ClientError):
            schema_obj = await mock_s3_service.Object(
                bucket_name=settings.s3.data_bucket,
                key=settings.s3.workflow_cache_key(
                    name=DocumentationEnum.PARAMETER_SCHEMA,
                    git_commit_hash=random_private_workflow.versions[0].workflow_version_id,
                ),
            )
            await schema_obj.load()
        with pytest.raises(ClientError):
            scm_obj = await mock_s3_service.Object(
                bucket_name=settings.s3.data_bucket, key=settings.s3.scm_key(random_private_workflow.workflow_id)
            )
            await scm_obj.load()

    @pytest.mark.asyncio
    async def test_delete_workflow_with_mode(
        self,
        client: AsyncClient,
        db: AsyncSession,
        random_workflow: WorkflowOut,
        random_workflow_mode: WorkflowMode,
        mock_s3_service: S3ServiceResource,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for deleting a workflow by its id.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        random_workflow_mode : clowm.model.WorkflowMode
            Random workflow mode for testing
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.delete(
            "/".join([self.base_path, str(random_workflow.workflow_id)]),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT
        icon_slug = str(random_workflow.versions[0].icon_url).split("/")[-1]
        with pytest.raises(ClientError):
            icon_obj = await mock_s3_service.Object(
                bucket_name=settings.s3.data_bucket, key=settings.s3.icon_key(icon_slug)
            )
            await icon_obj.load()
        with pytest.raises(ClientError):
            schema_obj = await mock_s3_service.Object(
                bucket_name=settings.s3.data_bucket,
                key=settings.s3.workflow_cache_key(
                    name=DocumentationEnum.PARAMETER_SCHEMA,
                    git_commit_hash=random_workflow.versions[0].workflow_version_id,
                    mode_id=random_workflow_mode.mode_id,
                ),
            )
            await schema_obj.load()

        mode_db = await db.scalar(select(WorkflowMode).where(WorkflowMode.mode_id == random_workflow_mode.mode_id))
        assert mode_db is None


class TestWorkflowRoutesUpdate(_TestWorkflowRoutes):
    @pytest.mark.asyncio
    async def test_update_workflow(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_workflow: WorkflowOut,
        mock_s3_service: S3ServiceResource,
        cleanup: CleanupList,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for successfully updating a workflow.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        config_value = random_lower_string(128)
        await db.execute(
            update(WorkflowVersion)
            .where(WorkflowVersion.git_commit_hash == random_workflow.versions[0].workflow_version_id)
            .values(nextflow_config=config_value)
        )
        await db.commit()

        config_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket,
            key=settings.s3.config_key(random_workflow.versions[0].workflow_version_id),
        )
        with BytesIO(config_value.encode("utf-8")) as f:
            await config_obj.upload_fileobj(f)
        cleanup.add_task(config_obj.delete)

        git_commit_hash = random_hex_string()
        version_update = WorkflowUpdate(
            git_commit_hash=git_commit_hash,
            version=random_lower_string(8),
            nextflow_version=NextflowVersion.v24_04_4,
        )
        response = await client.post(
            "/".join([self.base_path, str(random_workflow.workflow_id), "update"]),
            content=version_update.model_dump_json(),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_201_CREATED
        new_config_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket, key=settings.s3.config_key(git_commit_hash)
        )
        cleanup.add_task(new_config_obj.delete)

        created_version = WorkflowVersionOut.model_validate_json(response.content)
        assert created_version.workflow_version_id == git_commit_hash
        assert created_version.status == WorkflowVersion.WorkflowVersionStatus.CREATED
        assert created_version.icon_url == random_workflow.versions[0].icon_url

        stmt = select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == git_commit_hash)
        db_version = await db.scalar(stmt)
        assert db_version is not None
        assert db_version.status == WorkflowVersion.WorkflowVersionStatus.CREATED

        with BytesIO() as f:
            await new_config_obj.download_fileobj(f)
            f.seek(0)
            assert f.read().decode("utf-8") == config_value

    @pytest.mark.asyncio
    async def test_update_workflow_with_append_modes(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_workflow: WorkflowOut,
        random_workflow_mode: WorkflowMode,
        mock_s3_service: S3ServiceResource,
        cleanup: CleanupList,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for successfully updating a workflow and adding new modes.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_workflow_mode : clowm.model.WorkflowMode
            Random workflow mode for testing
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        git_commit_hash = random_hex_string()
        version_update = WorkflowUpdate(
            git_commit_hash=git_commit_hash,
            version=random_lower_string(8),
            append_modes=[
                WorkflowModeIn(
                    name=random_lower_string(10), entrypoint=random_lower_string(16), schema_path="alternative.json"
                )
            ],
            nextflow_version=NextflowVersion.v24_04_4,
        )
        response = await client.post(
            "/".join([self.base_path, str(random_workflow.workflow_id), "update"]),
            content=version_update.model_dump_json(),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_201_CREATED
        created_version = WorkflowVersionOut.model_validate_json(response.content)
        assert created_version.workflow_version_id == git_commit_hash
        assert created_version.status == WorkflowVersion.WorkflowVersionStatus.CREATED
        assert created_version.icon_url == random_workflow.versions[0].icon_url
        assert created_version.modes is not None
        assert len(created_version.modes) == 2

        stmt = select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == git_commit_hash)
        db_version = await db.scalar(stmt)
        assert db_version is not None
        assert db_version.status == WorkflowVersion.WorkflowVersionStatus.CREATED

        new_mode_id = next((m for m in created_version.modes if m != random_workflow_mode.mode_id), None)
        assert new_mode_id is not None
        cleanup.add_task(delete_workflow_mode, db=db, mode_id=new_mode_id)
        schema_obj1 = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket,
            key=settings.s3.workflow_cache_key(
                name=DocumentationEnum.PARAMETER_SCHEMA, git_commit_hash=git_commit_hash, mode_id=new_mode_id
            ),
        )
        await schema_obj1.load()
        cleanup.add_task(
            schema_obj1.delete,
        )

        schema_obj2 = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket,
            key=settings.s3.workflow_cache_key(
                name=DocumentationEnum.PARAMETER_SCHEMA,
                git_commit_hash=git_commit_hash,
                mode_id=random_workflow_mode.mode_id,
            ),
        )
        await schema_obj2.load()

    @pytest.mark.asyncio
    async def test_update_workflow_with_delete_non_existing_modes(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_api_token: ApiToken,
        random_workflow: WorkflowOut,
    ) -> None:
        """
        Test for updating a workflow and delete an non-existing mode.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        version_update = WorkflowUpdate(
            git_commit_hash=random_hex_string(),
            version=random_lower_string(8),
            delete_modes=[str(uuid4())],
            nextflow_version=NextflowVersion.v24_04_4,
        )
        response = await client.post(
            "/".join([self.base_path, str(random_workflow.workflow_id), "update"]),
            content=version_update.model_dump_json(),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_update_workflow_with_delete_modes(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_workflow: WorkflowOut,
        random_workflow_mode: WorkflowMode,
        mock_s3_service: S3ServiceResource,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for successfully updating a workflow and delete an old mode.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_workflow_mode : clowm.model.WorkflowMode
            Random workflow mode for testing
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        git_commit_hash = random_hex_string()
        version_update = WorkflowUpdate(
            git_commit_hash=git_commit_hash,
            version=random_lower_string(8),
            delete_modes=[random_workflow_mode.mode_id],
            nextflow_version=NextflowVersion.v24_04_4,
        )
        response = await client.post(
            "/".join([self.base_path, str(random_workflow.workflow_id), "update"]),
            content=version_update.model_dump_json(),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_201_CREATED
        created_version = WorkflowVersionOut.model_validate_json(response.content)
        assert created_version.workflow_version_id == git_commit_hash
        assert created_version.status == WorkflowVersion.WorkflowVersionStatus.CREATED
        assert created_version.icon_url == random_workflow.versions[0].icon_url
        assert created_version.modes is None or len(created_version.modes) == 0

        stmt = select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == git_commit_hash)
        db_version = await db.scalar(stmt)
        assert db_version is not None
        assert db_version.status == WorkflowVersion.WorkflowVersionStatus.CREATED

        with pytest.raises(ClientError):
            schema_obj = await mock_s3_service.Object(
                bucket_name=settings.s3.data_bucket,
                key=settings.s3.workflow_cache_key(
                    name=DocumentationEnum.PARAMETER_SCHEMA,
                    git_commit_hash=git_commit_hash,
                    mode_id=random_workflow_mode.mode_id,
                ),
            )
            await schema_obj.load()

    @pytest.mark.asyncio
    async def test_update_workflow_with_append_and_delete_modes(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_workflow: WorkflowOut,
        random_workflow_mode: WorkflowMode,
        mock_s3_service: S3ServiceResource,
        cleanup: CleanupList,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for successfully updating a workflow with adding a new mode and delete an old mode.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_workflow_mode : clowm.model.WorkflowMode
            Random workflow mode for testing
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        git_commit_hash = random_hex_string()
        version_update = WorkflowUpdate(
            git_commit_hash=git_commit_hash,
            version=random_lower_string(8),
            delete_modes=[str(random_workflow_mode.mode_id)],
            append_modes=[
                WorkflowModeIn(
                    name=random_lower_string(10), entrypoint=random_lower_string(16), schema_path="alternative.json"
                )
            ],
            nextflow_version=NextflowVersion.v24_04_4,
        )
        response = await client.post(
            "/".join([self.base_path, str(random_workflow.workflow_id), "update"]),
            content=version_update.model_dump_json(),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_201_CREATED
        created_version = WorkflowVersionOut.model_validate_json(response.content)
        assert created_version.workflow_version_id == git_commit_hash
        assert created_version.status == WorkflowVersion.WorkflowVersionStatus.CREATED
        assert created_version.icon_url == random_workflow.versions[0].icon_url
        assert len(created_version.modes) == 1
        assert created_version.modes[0] != random_workflow_mode.mode_id

        stmt = select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == git_commit_hash)
        db_version = await db.scalar(stmt)
        assert db_version is not None
        assert db_version.status == WorkflowVersion.WorkflowVersionStatus.CREATED

        mode_id = created_version.modes[0]
        cleanup.add_task(delete_workflow_mode, db=db, mode_id=mode_id)
        schema_obj1 = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket,
            key=settings.s3.workflow_cache_key(
                name=DocumentationEnum.PARAMETER_SCHEMA, git_commit_hash=git_commit_hash, mode_id=mode_id
            ),
        )
        await schema_obj1.load()
        cleanup.add_task(
            schema_obj1.delete,
        )
        with pytest.raises(ClientError):
            schema_obj2 = await mock_s3_service.Object(
                bucket_name=settings.s3.data_bucket,
                key=settings.s3.workflow_cache_key(
                    name=DocumentationEnum.PARAMETER_SCHEMA,
                    git_commit_hash=git_commit_hash,
                    mode_id=random_workflow_mode.mode_id,
                ),
            )
            await schema_obj2.load()

    @pytest.mark.asyncio
    async def test_update_workflow_with_error(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_workflow: WorkflowOut,
        mock_default_http_server: DefaultMockHTTPService,
        cleanup: CleanupList,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for updating a workflow where the file checks don't pass

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        mock_default_http_server : clowm.tests.mocks.DefaultMockHTTPService
            Mock http service for testing
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        mock_default_http_server.send_error = True
        cleanup.add_task(mock_default_http_server.reset)
        version_update = WorkflowUpdate(
            git_commit_hash=random_hex_string(),
            version=random_lower_string(8),
            nextflow_version=NextflowVersion.v24_04_4,
        )
        response = await client.post(
            "/".join([self.base_path, str(random_workflow.workflow_id), "update"]),
            content=version_update.model_dump_json(),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

        db_workflows = (
            await db.scalars(select(WorkflowVersion).where(WorkflowVersion.workflow_id == random_workflow.workflow_id))
        ).all()
        assert len(db_workflows) == 1

    @pytest.mark.asyncio
    async def test_update_workflow_with_registered_git_commit(
        self,
        client: AsyncClient,
        random_workflow: WorkflowOut,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for updating a workflow where the git commit is already in the system.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        version_update = WorkflowUpdate(
            git_commit_hash=random_workflow.versions[0].workflow_version_id,
            version=random_lower_string(8),
            nextflow_version=NextflowVersion.v24_04_4,
        )
        response = await client.post(
            "/".join([self.base_path, str(random_workflow.workflow_id), "update"]),
            content=version_update.model_dump_json(),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_update_workflow_as_foreign_user(
        self,
        client: AsyncClient,
        random_workflow: WorkflowOut,
        random_second_api_token: ApiToken,
    ) -> None:
        """
        Test for updating a workflow where the requested user is not the developer of the workflow.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_second_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        version_update = WorkflowUpdate(
            git_commit_hash=random_hex_string(),
            version=random_lower_string(8),
            nextflow_version=NextflowVersion.v24_04_4,
        )
        response = await client.post(
            "/".join([self.base_path, str(random_workflow.workflow_id), "update"]),
            content=version_update.model_dump_json(),
            headers={header_token_scheme.model.name: random_second_api_token.token},
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN
