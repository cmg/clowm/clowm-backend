import secrets

import pytest
from fastapi import status
from httpx import AsyncClient
from sqlalchemy import select, update
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.api.dependencies.v1.auth import header_token_scheme
from clowm.db.types import ApiTokenScopesDB
from clowm.models import ApiToken


class TestRBACProtectedRoutes:
    admin_route = "/v1/users"
    user_route = "/v1/users/me"

    @pytest.mark.asyncio
    async def test_admin_route_with_admin_user_token(
        self, client: AsyncClient, random_api_token: ApiToken, db: AsyncSession
    ) -> None:
        """
        Test calling a RBAC protected admin route with admin user token.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        response = await client.get(self.admin_route, headers={header_token_scheme.model.name: random_api_token.token})
        assert response.status_code == status.HTTP_200_OK

        await db.reset()
        token = await db.scalar(select(ApiToken).where(ApiToken.token_id == random_api_token.token_id))
        assert token is not None
        assert token.last_used is not None

    @pytest.mark.asyncio
    async def test_admin_route_with_admin_user_token_and_missing_read_scope(
        self, client: AsyncClient, random_api_token: ApiToken, db: AsyncSession
    ) -> None:
        """
        Test calling a RBAC protected admin route with admin user token and missing READ scope.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        await db.execute(
            update(ApiToken).where(ApiToken.token_id == random_api_token.token_id).values(scopes=ApiTokenScopesDB.WRITE)
        )
        await db.commit()
        response = await client.get(self.admin_route, headers={header_token_scheme.model.name: random_api_token.token})
        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.asyncio
    async def test_admin_route_with_admin_user_token_and_missing_write_scope(
        self, client: AsyncClient, random_api_token: ApiToken, db: AsyncSession
    ) -> None:
        """
        Test calling a RBAC protected admin route with admin user token and missing WRITE scope.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        await db.execute(
            update(ApiToken).where(ApiToken.token_id == random_api_token.token_id).values(scopes=ApiTokenScopesDB.READ)
        )
        await db.commit()
        response = await client.post("/v1/buckets", headers={header_token_scheme.model.name: random_api_token.token})
        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.asyncio
    async def test_admin_route_with_admin_user_unknown_token(
        self,
        client: AsyncClient,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test calling a RBAC protected admin route with an unknown token.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        """
        response = await client.get(
            self.admin_route, headers={header_token_scheme.model.name: secrets.token_urlsafe(32)}
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    @pytest.mark.asyncio
    async def test_admin_route_with_admin_user_expired_token(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test calling a RBAC protected admin route with an expired admin user token.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        """
        await db.execute(update(ApiToken).where(ApiToken.token_id == random_api_token.token_id).values(expires_at=1))
        await db.commit()

        response = await client.get(self.admin_route, headers={header_token_scheme.model.name: random_api_token.token})
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
