import pytest
from fastapi import status
from httpx import AsyncClient


class TestOpenAPIRoute:
    @pytest.mark.asyncio
    async def test_openapi_route(self, client: AsyncClient) -> None:
        """
        Test for getting the OpenAPI specification and the caching mechanism.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        """
        response1 = await client.get("/v1/openapi.json")
        assert response1.status_code == status.HTTP_200_OK

    @pytest.mark.asyncio
    async def test_swagger_ui_route(self, client: AsyncClient) -> None:
        """
        Test for getting the Swagger UI.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        """
        response1 = await client.get("/v1/docs")
        assert response1.status_code == status.HTTP_200_OK
