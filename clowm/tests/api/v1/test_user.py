import random
import uuid

import pytest
from fastapi import status
from httpx import AsyncClient
from pydantic import TypeAdapter

from clowm.api.dependencies.v1.auth import header_token_scheme
from clowm.models import ApiToken, Role
from clowm.schemas.user import UserOut, UserOutExtended
from clowm.tests.utils import UserWithAuthCookie


class _TestUserRoutes:
    base_path = "/v1/users"


UserList = TypeAdapter(list[UserOut])
ExtendedUserList = TypeAdapter(list[UserOutExtended])


class TestUserRoutesGet(_TestUserRoutes):
    @pytest.mark.asyncio
    async def test_get_user_me(
        self, client: AsyncClient, random_user: UserWithAuthCookie, random_api_token: ApiToken
    ) -> None:
        """
        Test for getting the currently logged-in user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.test.utils.user.UserWithAuthCookie
            Random user for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(
            f"{self.base_path}/me", headers={header_token_scheme.model.name: random_api_token.token}
        )
        assert response.status_code == status.HTTP_200_OK
        current_user = UserOutExtended.model_validate_json(response.content)
        assert current_user.uid == random_user.uid
        assert current_user.display_name == random_user.user.display_name
        assert current_user.roles is not None
        assert current_user.email == current_user.email

    @pytest.mark.asyncio
    async def test_get_unknown_user(self, client: AsyncClient, random_api_token: ApiToken) -> None:
        """
        Test for getting an unknown user by its uid.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(
            f"{self.base_path}/{uuid.uuid4()}", headers={header_token_scheme.model.name: random_api_token.token}
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_get_user_by_uid(
        self, client: AsyncClient, random_user: UserWithAuthCookie, random_api_token: ApiToken
    ) -> None:
        """
        Test for getting a known user by its uid.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.test.utils.user.UserWithAuthCookie
            Random user for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(
            f"{self.base_path}/{str(random_user.uid)}",
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_200_OK
        current_user = UserOut.model_validate_json(response.content)
        assert current_user.uid == random_user.uid
        assert current_user.display_name == random_user.user.display_name


class TestUserRoutesSearch(_TestUserRoutes):
    @pytest.mark.asyncio
    async def test_search_user_by_name_substring(
        self, client: AsyncClient, random_user: UserWithAuthCookie, random_api_token: ApiToken
    ) -> None:
        """
        Test for searching a user by its name

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.test.utils.user.UserWithAuthCookie
            Random user for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        substring_indices = [0, 0]
        while substring_indices[1] - substring_indices[0] < 3:
            substring_indices = sorted(random.choices(range(len(random_user.user.display_name)), k=2))

        random_substring = random_user.user.display_name[substring_indices[0] : substring_indices[1]]

        response = await client.get(
            f"{self.base_path}/search",
            params={"name_substring": random_substring},
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_200_OK
        users = UserList.validate_json(response.content)

        assert len(users) > 0
        assert sum(1 for u in users if u.uid == random_user.uid) == 1


class TestUserRoutesList(_TestUserRoutes):
    @pytest.mark.asyncio
    async def test_list_all_user(
        self, client: AsyncClient, random_user: UserWithAuthCookie, random_api_token: ApiToken
    ) -> None:
        """
        Test for listing all user in the system.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.test.utils.user.UserWithAuthCookie
            Random user for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """

        response = await client.get(
            self.base_path,
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_200_OK

        users = ExtendedUserList.validate_json(response.content)
        assert len(users) > 0
        assert sum(1 for u in users if u.uid == random_user.uid) == 1
        for u in users:
            assert u.email is not None

    @pytest.mark.asyncio
    async def test_list_user_with_role_filter(
        self, client: AsyncClient, random_user: UserWithAuthCookie, random_api_token: ApiToken
    ) -> None:
        """
        Test for list and filter users by their role.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.test.utils.user.UserWithAuthCookie
            Random user for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """

        response = await client.get(
            self.base_path,
            params={"filter_roles": Role.RoleEnum.USER.value},
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_200_OK

        users = ExtendedUserList.validate_json(response.content)
        assert len(users) > 0
        assert sum(1 for u in users if u.uid == random_user.uid) == 1
        for u in users:
            assert u.roles is not None
            assert Role.RoleEnum.USER.value in u.roles

    @pytest.mark.asyncio
    async def test_list_users_with_name_substring(
        self, client: AsyncClient, random_user: UserWithAuthCookie, random_api_token: ApiToken
    ) -> None:
        """
        Test for listing a user by its name

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.test.utils.user.UserWithAuthCookie
            Random user for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """

        substring_indices = [0, 0]
        while substring_indices[1] - substring_indices[0] < 3:
            substring_indices = sorted(random.choices(range(len(random_user.user.display_name)), k=2))

        random_substring = random_user.user.display_name[substring_indices[0] : substring_indices[1]]

        response = await client.get(
            self.base_path,
            params={"name_substring": random_substring},
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_200_OK

        users = ExtendedUserList.validate_json(response.content)
        assert len(users) > 0
        assert sum(1 for u in users if u.uid == random_user.uid) == 1
        for u in users:
            assert u.roles is not None
