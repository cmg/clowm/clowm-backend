from uuid import uuid4

import pytest
from fastapi import status
from httpx import AsyncClient

from clowm.api.dependencies.v1.auth import header_token_scheme
from clowm.models import ApiToken, WorkflowMode
from clowm.schemas.workflow_mode import WorkflowModeOut


class TestWorkflowModeRoutesGet:
    _base_path = "/v1/workflow_modes"

    @pytest.mark.asyncio
    async def test_get_non_existing_workflow_mode(self, client: AsyncClient, random_api_token: ApiToken) -> None:
        """
        Test for getting a non existing workflow mode.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(
            f"{self._base_path}/{uuid4()}", headers={header_token_scheme.model.name: random_api_token.token}
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_get_existing_workflow_mode(
        self, client: AsyncClient, random_workflow_mode: WorkflowMode, random_api_token: ApiToken
    ) -> None:
        """
        Test for getting a workflow mode successfully.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_workflow_mode : clowm.models.WorkflowMode
            Random workflow mode for testing.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(
            f"{self._base_path}/{str(random_workflow_mode.mode_id)}",
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_200_OK
        mode = WorkflowModeOut.model_validate_json(response.content)

        assert mode.mode_id == random_workflow_mode.mode_id
        assert mode.name == random_workflow_mode.name
        assert mode.entrypoint == random_workflow_mode.entrypoint
        assert mode.schema_path == random_workflow_mode.schema_path
