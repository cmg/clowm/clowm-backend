from uuid import uuid4

import pytest
from fastapi import status
from httpx import AsyncClient
from pydantic import TypeAdapter
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.api.dependencies.v1.auth import header_token_scheme
from clowm.models import ApiToken
from clowm.schemas.api_token import ApiTokenOut
from clowm.tests.utils import UserWithAuthCookie


class _TestApiTokenRoutes:
    base_path = "/v1/tokens"


class TestApiTokenRoutesGet(_TestApiTokenRoutes):
    @pytest.mark.asyncio
    async def test_get_all_tokens(self, client: AsyncClient, random_api_token: ApiToken) -> None:
        """
        Test for getting all Api tokens.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(self.base_path, headers={header_token_scheme.model.name: random_api_token.token})
        assert response.status_code == status.HTTP_200_OK

        ta = TypeAdapter(list[ApiTokenOut])
        tokens = ta.validate_json(response.content)
        assert len(tokens) == 1
        token = tokens[0]

        assert token.token_id == random_api_token.token_id
        assert token.name == random_api_token.name
        assert token.uid == random_api_token.uid

    @pytest.mark.asyncio
    async def test_get_all_tokens_filter_by_uid(
        self, client: AsyncClient, random_api_token: ApiToken, random_user: UserWithAuthCookie
    ) -> None:
        """
        Test for getting all Api tokens filtered by uid.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        response = await client.get(
            self.base_path,
            params={"uid": str(random_user.uid)},
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_200_OK

        ta = TypeAdapter(list[ApiTokenOut])
        tokens = ta.validate_json(response.content)
        assert len(tokens) == 1
        token = tokens[0]

        assert token.token_id == random_api_token.token_id
        assert token.name == random_api_token.name
        assert token.uid == random_api_token.uid

    @pytest.mark.asyncio
    async def test_get_token_by_id(
        self,
        client: AsyncClient,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for getting an Api token by id.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(
            f"{self.base_path}/{str(random_api_token.token_id)}",
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_200_OK

        token = ApiTokenOut.model_validate_json(response.content)

        assert token.token_id == random_api_token.token_id
        assert token.name == random_api_token.name
        assert token.uid == random_api_token.uid

    @pytest.mark.asyncio
    async def test_get_non_existing_token_by_id(self, client: AsyncClient, random_api_token: ApiToken) -> None:
        """
        Test for getting an non-existing Api token by id.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        """
        response = await client.get(
            f"{self.base_path}/{str(uuid4())}", headers={header_token_scheme.model.name: random_api_token.token}
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND


class TestApiTokenRoutesDelete(_TestApiTokenRoutes):
    @pytest.mark.asyncio
    async def test_delete_token_by_id(
        self,
        client: AsyncClient,
        random_api_token: ApiToken,
        db: AsyncSession,
    ) -> None:
        """
        Test for deleting an Api token by id.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        response = await client.delete(
            f"{self.base_path}/{str(random_api_token.token_id)}",
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT

        db_token = await db.scalar(select(ApiToken).where(ApiToken.token_id == random_api_token.token_id))
        assert db_token is None
