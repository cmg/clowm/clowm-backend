import urllib.parse
from io import BytesIO
from typing import TYPE_CHECKING
from uuid import uuid4

import pytest
from fastapi import status
from httpx import AsyncClient
from pydantic import TypeAdapter
from sqlalchemy import select, update
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.api.dependencies.pagination import HTTPLinkHeader
from clowm.api.dependencies.v1.auth import header_token_scheme
from clowm.core.config import settings
from clowm.git_repository import build_repository
from clowm.models import ApiToken, Bucket, ResourceVersion, Workflow, WorkflowExecution, WorkflowMode, WorkflowVersion
from clowm.schemas.documentation_enum import DocumentationEnum
from clowm.schemas.workflow import WorkflowOut
from clowm.schemas.workflow_execution import DevWorkflowExecutionIn, WorkflowExecutionIn, WorkflowExecutionOut
from clowm.schemas.workflow_mode import WorkflowModeIn
from clowm.scm import SCM, SCMProvider
from clowm.tests.mocks import MockSlurmCluster
from clowm.tests.utils import (
    CleanupList,
    UserWithAuthCookie,
    delete_workflow_execution,
    random_hex_string,
    random_lower_string,
)

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object

ExecutionList = TypeAdapter(list[WorkflowExecutionOut])


class _TestWorkflowExecutionRoutes:
    base_path: str = "/v1/workflow_executions"


class TestWorkflowExecutionRoutesCreate(_TestWorkflowExecutionRoutes):
    @pytest.mark.asyncio
    async def test_start_github_workflow_execution(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow_version: WorkflowVersion,
        random_workflow: WorkflowOut,
        mock_s3_service: S3ServiceResource,
        mock_slurm_cluster: MockSlurmCluster,
        cleanup: CleanupList,
        db: AsyncSession,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for starting a workflow execution from a public GitHub repository.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_workflow_version : clowm.models.WorkflowVersion
            Random workflow version for testing.
        mock_s3_service: types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        mock_slurm_cluster : clowm.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        execution_in = WorkflowExecutionIn(workflow_version_id=random_workflow_version.git_commit_hash, parameters={})
        response = await client.post(
            self.base_path,
            headers={header_token_scheme.model.name: random_api_token.token},
            content=execution_in.model_dump_json(exclude_none=True),
        )
        assert response.status_code == status.HTTP_201_CREATED
        execution_response = WorkflowExecutionOut.model_validate_json(response.content)
        cleanup.add_task(delete_workflow_execution, db=db, execution_id=execution_response.execution_id)

        assert execution_response.workflow_version_id == execution_in.workflow_version_id
        assert execution_response.executor_id == random_user.uid
        assert execution_response.status == WorkflowExecution.WorkflowExecutionStatus.PENDING
        assert execution_response.workflow_version_id == random_workflow_version.git_commit_hash

        parameter_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket,
            key=settings.s3.execution_parameters_key(execution_response.execution_id),
        )
        await parameter_obj.load()

        job = mock_slurm_cluster.get_job_by_name(execution_response.execution_id)
        assert job is not None
        assert job["job"]["name"] == execution_response.execution_id.hex
        assert job["job"]["current_working_directory"] == settings.cluster.working_directory
        assert job["job"]["environment"]["TOWER_WORKSPACE_ID"] == execution_response.execution_id.hex[:16]
        assert "NXF_SCM_FILE" not in job["job"]["environment"]

        nextflow_script = job["script"]
        assert "-hub github" in nextflow_script
        assert "-entry" not in nextflow_script
        assert f"-revision {random_workflow_version.git_commit_hash}" in nextflow_script
        assert f"run {random_workflow.repository_url}" in nextflow_script
        assert "export NXF_SCM_FILE" not in nextflow_script
        assert "-with-report" not in nextflow_script
        assert "-with-timeline" not in nextflow_script
        assert ">> $S3COMMANDFILE" not in nextflow_script

    @pytest.mark.asyncio
    async def test_start_github_workflow_execution_with_logs(
        self,
        client: AsyncClient,
        random_workflow_version: WorkflowVersion,
        random_workflow: WorkflowOut,
        mock_s3_service: S3ServiceResource,
        mock_slurm_cluster: MockSlurmCluster,
        random_bucket: Bucket,
        cleanup: CleanupList,
        db: AsyncSession,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for starting a workflow execution with a logs_path.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_workflow_version : clowm.models.WorkflowVersion
            Random workflow version for testing.
        mock_s3_service: types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        mock_slurm_cluster : clowm.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        execution_in = WorkflowExecutionIn(
            workflow_version_id=random_workflow_version.git_commit_hash,
            parameters={},
            logs_s3_path=f"s3://{random_bucket.name}/logs",
        )
        response = await client.post(
            self.base_path,
            headers={header_token_scheme.model.name: random_api_token.token},
            content=execution_in.model_dump_json(exclude_none=True),
        )
        assert response.status_code == status.HTTP_201_CREATED
        execution_response = WorkflowExecutionOut.model_validate_json(response.content)
        cleanup.add_task(delete_workflow_execution, db=db, execution_id=execution_response.execution_id)

        log_bucket = await mock_s3_service.Bucket(random_bucket.name)
        found_obj = False
        async for obj in log_bucket.objects.filter(Prefix="logs/").all():
            if obj.key.endswith("parameters.json"):
                found_obj = True
                break
        assert found_obj, "parameters.json not found in logs path"

        job = mock_slurm_cluster.get_job_by_name(execution_response.execution_id)
        assert job is not None
        assert job["job"]["name"] == execution_response.execution_id.hex

        nextflow_script = job["script"]
        assert "-with-report" in nextflow_script
        assert "-with-timeline" in nextflow_script
        assert ">> $S3COMMANDFILE" in nextflow_script

    @pytest.mark.asyncio
    async def test_start_private_github_workflow_execution(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow_version: WorkflowVersion,
        random_private_workflow: WorkflowOut,
        mock_s3_service: S3ServiceResource,
        mock_slurm_cluster: MockSlurmCluster,
        cleanup: CleanupList,
        db: AsyncSession,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for starting a workflow execution from a private GitHub repository.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        random_workflow_version : clowm.models.WorkflowVersion
            Random workflow version for testing.
        random_private_workflow : clowm.schemas.workflow.WorkflowOut
            Random private workflow for testing.
        mock_s3_service: types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        mock_slurm_cluster : clowm.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """

        execution_in = WorkflowExecutionIn(workflow_version_id=random_workflow_version.git_commit_hash, parameters={})
        response = await client.post(
            self.base_path,
            headers={header_token_scheme.model.name: random_api_token.token},
            content=execution_in.model_dump_json(exclude_none=True),
        )
        assert response.status_code == status.HTTP_201_CREATED
        execution_response = WorkflowExecutionOut.model_validate_json(response.content)
        cleanup.add_task(delete_workflow_execution, db=db, execution_id=execution_response.execution_id)

        assert execution_response.workflow_version_id == execution_in.workflow_version_id
        assert execution_response.executor_id == random_user.uid
        assert execution_response.status == WorkflowExecution.WorkflowExecutionStatus.PENDING
        assert execution_response.workflow_version_id == random_workflow_version.git_commit_hash

        parameter_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket,
            key=settings.s3.execution_parameters_key(execution_response.execution_id),
        )
        await parameter_obj.load()

        job = mock_slurm_cluster.get_job_by_name(execution_response.execution_id)
        assert job is not None
        assert job["job"]["environment"]["TOWER_WORKSPACE_ID"] == execution_response.execution_id.hex[:16]

        nextflow_script = job["script"]
        assert "-hub github" in nextflow_script
        assert "-entry" not in nextflow_script
        assert "export NXF_SCM_FILE" in nextflow_script
        assert f"-revision {random_workflow_version.git_commit_hash}" in nextflow_script

    @pytest.mark.asyncio
    async def test_start_gitlab_workflow_execution(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow: WorkflowOut,
        random_workflow_version: WorkflowVersion,
        mock_s3_service: S3ServiceResource,
        mock_slurm_cluster: MockSlurmCluster,
        cleanup: CleanupList,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for starting a workflow execution from a public GitLab repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_workflow_version : clowm.models.WorkflowVersion
            Random workflow version for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        mock_slurm_cluster : clowm.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        scm_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket, key=settings.s3.scm_key(random_workflow.workflow_id)
        )
        cleanup.add_task(scm_obj.delete)
        stmt = (
            update(Workflow)
            .where(Workflow.workflow_id == random_workflow_version.workflow_id)
            .values(repository_url="https://gitlab.com/example/example")
        )
        await db.execute(stmt)
        await db.commit()

        # Upload SCM file to PARAMS_BUCKET
        scm_provider = SCMProvider.from_repo(
            build_repository(str(random_workflow.repository_url), random_workflow_version.git_commit_hash),
            name=SCMProvider.generate_name(random_workflow.workflow_id),
        )
        assert scm_provider is not None
        with BytesIO() as f:
            SCM([scm_provider]).serialize(f)
            f.seek(0)
            await scm_obj.upload_fileobj(f)

        execution_in = WorkflowExecutionIn(workflow_version_id=random_workflow_version.git_commit_hash, parameters={})
        response = await client.post(
            self.base_path,
            headers={header_token_scheme.model.name: random_api_token.token},
            content=execution_in.model_dump_json(exclude_none=True),
        )
        assert response.status_code == status.HTTP_201_CREATED
        execution_response = WorkflowExecutionOut.model_validate_json(response.content)
        cleanup.add_task(delete_workflow_execution, db=db, execution_id=execution_response.execution_id)
        assert execution_response.workflow_version_id == execution_in.workflow_version_id
        assert execution_response.executor_id == random_user.uid
        assert execution_response.status == WorkflowExecution.WorkflowExecutionStatus.PENDING
        assert execution_response.workflow_version_id == random_workflow_version.git_commit_hash

        parameter_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket,
            key=settings.s3.execution_parameters_key(execution_response.execution_id),
        )
        await parameter_obj.load()

        job = mock_slurm_cluster.get_job_by_name(execution_response.execution_id)
        assert job is not None
        assert job["job"]["environment"]["TOWER_WORKSPACE_ID"] == execution_response.execution_id.hex[:16]

        nextflow_script = job["script"]
        assert f"-hub {SCMProvider.generate_name(random_workflow.workflow_id)}" in nextflow_script
        assert "-entry" not in nextflow_script
        assert "export NXF_SCM_FILE" in nextflow_script
        assert f"-revision {random_workflow_version.git_commit_hash}" in nextflow_script

    @pytest.mark.asyncio
    async def test_start_too_many_workflow_executions(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow_version: WorkflowVersion,
        cleanup: CleanupList,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for starting too many workflow executions.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        random_workflow_version : clowm.models.WorkflowVersion
            Random workflow version for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        active_execution_counter = 0
        while active_execution_counter < settings.cluster.active_workflow_execution_limit:
            execution = WorkflowExecution(
                executor_id=random_user.uid,
                workflow_version_id=random_workflow_version.git_commit_hash,
                slurm_job_id=1,
            )
            db.add(execution)
            await db.commit()
            cleanup.add_task(delete_workflow_execution, db=db, execution_id=execution.execution_id)
            active_execution_counter += 1
        execution_in = WorkflowExecutionIn(workflow_version_id=random_workflow_version.git_commit_hash, parameters={})
        response = await client.post(
            self.base_path,
            headers={header_token_scheme.model.name: random_api_token.token},
            content=execution_in.model_dump_json(exclude_none=True),
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.asyncio
    async def test_start_workflow_execution_with_unknown_workflow_version(
        self, client: AsyncClient, random_api_token: ApiToken
    ) -> None:
        """
        Test for starting a workflow execution with an unknown workflow version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        """
        execution_in = WorkflowExecutionIn(
            workflow_version_id=random_hex_string(),
            parameters={},
        )
        response = await client.post(
            self.base_path,
            headers={header_token_scheme.model.name: random_api_token.token},
            content=execution_in.model_dump_json(exclude_none=True),
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_start_workflow_execution_with_deprecated_workflow_version(
        self,
        client: AsyncClient,
        db: AsyncSession,
        random_workflow_version: WorkflowVersion,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for starting a workflow execution with a deprecated workflow version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        random_workflow_version : clowm.models.WorkflowVersion
            Random workflow version for testing.
        """
        await db.execute(
            update(WorkflowVersion)
            .where(WorkflowVersion.git_commit_hash == random_workflow_version.git_commit_hash)
            .values(status=WorkflowVersion.WorkflowVersionStatus.DEPRECATED.name)
        )
        await db.commit()
        execution_in = WorkflowExecutionIn(
            workflow_version_id=random_workflow_version.git_commit_hash,
            parameters={},
        )
        response = await client.post(
            self.base_path,
            headers={header_token_scheme.model.name: random_api_token.token},
            content=execution_in.model_dump_json(exclude_none=True),
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.asyncio
    async def test_start_workflow_execution_with_workflow_mode(
        self,
        client: AsyncClient,
        random_workflow_version: WorkflowVersion,
        random_workflow_mode: WorkflowMode,
        mock_slurm_cluster: MockSlurmCluster,
        cleanup: CleanupList,
        db: AsyncSession,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for starting a workflow execution with a workflow mode

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        random_workflow_version : clowm.models.WorkflowVersion
            Random workflow version for testing.
        mock_slurm_cluster : clowm.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        execution_in = WorkflowExecutionIn(
            workflow_version_id=random_workflow_version.git_commit_hash,
            parameters={},
            mode_id=random_workflow_mode.mode_id,
        )
        response = await client.post(
            self.base_path,
            headers={header_token_scheme.model.name: random_api_token.token},
            content=execution_in.model_dump_json(exclude_none=True),
        )
        assert response.status_code == status.HTTP_201_CREATED
        execution_response = WorkflowExecutionOut.model_validate_json(response.content)
        cleanup.add_task(delete_workflow_execution, db=db, execution_id=execution_response.execution_id)
        assert execution_response.mode_id == random_workflow_mode.mode_id

        job = mock_slurm_cluster.get_job_by_name(execution_response.execution_id)
        assert job is not None
        assert job["job"]["environment"]["TOWER_WORKSPACE_ID"] == execution_response.execution_id.hex[:16]

        nextflow_script = job["script"]
        assert f"-entry {random_workflow_mode.entrypoint}" in nextflow_script
        assert f"-revision {random_workflow_version.git_commit_hash}" in nextflow_script

    @pytest.mark.asyncio
    async def test_start_workflow_execution_with_non_existing_workflow_mode(
        self,
        client: AsyncClient,
        random_workflow_version: WorkflowVersion,
        mock_slurm_cluster: MockSlurmCluster,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for starting a workflow execution with a non-existing workflow mode

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        random_workflow_version : clowm.models.WorkflowVersion
            Random workflow version for testing.
        mock_slurm_cluster : clowm.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        """
        execution_in = WorkflowExecutionIn(
            workflow_version_id=random_workflow_version.git_commit_hash, parameters={}, mode_id=uuid4()
        )
        response = await client.post(
            self.base_path,
            headers={header_token_scheme.model.name: random_api_token.token},
            content=execution_in.model_dump_json(),
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_start_workflow_execution_without_required_workflow_mode(
        self,
        client: AsyncClient,
        random_workflow_version: WorkflowVersion,
        random_workflow_mode: WorkflowMode,
        mock_slurm_cluster: MockSlurmCluster,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for starting a workflow execution without specifying a required workflow mode

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        random_workflow_version : clowm.models.WorkflowVersion
            Random workflow version for testing.
        mock_slurm_cluster : clowm.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        """
        execution_in = WorkflowExecutionIn(
            workflow_version_id=random_workflow_version.git_commit_hash,
            parameters={},
        )
        response = await client.post(
            self.base_path,
            headers={header_token_scheme.model.name: random_api_token.token},
            content=execution_in.model_dump_json(exclude_none=True),
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_start_workflow_execution_with_resource(
        self,
        client: AsyncClient,
        random_workflow_version: WorkflowVersion,
        random_resource_version_states: ResourceVersion,
        cleanup: CleanupList,
        db: AsyncSession,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for starting a workflow execution with a resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        random_resource_version_states : clowm.schemas.workflow.ResourceVersion
            Random resource version with all possible states for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        parameter = f"/some/path/CLDB-{random_resource_version_states.resource_id.hex}/"
        if random_resource_version_states.status is ResourceVersion.ResourceVersionStatus.LATEST:
            parameter += "latest"
        else:
            parameter += random_resource_version_states.resource_version_id.hex

        execution_in = WorkflowExecutionIn(
            workflow_version_id=random_workflow_version.git_commit_hash,
            parameters={"someparameter": parameter},
        )
        response = await client.post(
            self.base_path,
            headers={header_token_scheme.model.name: random_api_token.token},
            content=execution_in.model_dump_json(exclude_none=True),
        )
        if random_resource_version_states.status in [
            ResourceVersion.ResourceVersionStatus.SYNCHRONIZED,
            ResourceVersion.ResourceVersionStatus.LATEST,
            ResourceVersion.ResourceVersionStatus.SETTING_LATEST,
        ]:
            assert response.status_code == status.HTTP_201_CREATED
            cleanup.add_task(
                delete_workflow_execution,
                db=db,
                execution_id=WorkflowExecutionOut.model_validate_json(response.content).execution_id,
            )
        else:
            assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_start_workflow_execution_non_existing_resource(
        self, client: AsyncClient, random_workflow_version: WorkflowVersion, random_api_token: ApiToken
    ) -> None:
        """
        Test for starting a workflow execution with a non-existing resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        """
        execution_in = WorkflowExecutionIn(
            workflow_version_id=random_workflow_version.git_commit_hash,
            parameters={"someparameter": f"/some/path/CLDB-{uuid4().hex}/latest"},
        )
        response = await client.post(
            self.base_path,
            headers={header_token_scheme.model.name: random_api_token.token},
            content=execution_in.model_dump_json(exclude_none=True),
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_start_execution_with_slurm_error(
        self,
        client: AsyncClient,
        db: AsyncSession,
        random_workflow_version: WorkflowVersion,
        random_workflow: WorkflowOut,
        mock_slurm_cluster: MockSlurmCluster,
        cleanup: CleanupList,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for starting a workflow execution from a public GitHub repository.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_workflow_version : clowm.models.WorkflowVersion
            Random workflow version for testing.
        mock_slurm_cluster : clowm.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        execution_in = WorkflowExecutionIn(workflow_version_id=random_workflow_version.git_commit_hash, parameters={})
        mock_slurm_cluster.send_error = True

        def repair_slurm_cluster() -> None:
            mock_slurm_cluster.send_error = False

        cleanup.add_task(repair_slurm_cluster)
        response = await client.post(
            self.base_path,
            headers={header_token_scheme.model.name: random_api_token.token},
            content=execution_in.model_dump_json(exclude_none=True),
        )
        assert response.status_code == status.HTTP_201_CREATED
        execution_id = WorkflowExecutionOut.model_validate_json(response.content).execution_id
        cleanup.add_task(delete_workflow_execution, db=db, execution_id=execution_id)

        await db.reset()
        stmt = select(WorkflowExecution).where(WorkflowExecution.execution_id == execution_id)
        execution_db = await db.scalar(stmt)
        assert execution_db
        assert execution_db.status == WorkflowExecution.WorkflowExecutionStatus.ERROR

    @pytest.mark.asyncio
    async def test_start_execution_broken_workflow_bucket(
        self,
        client: AsyncClient,
        random_workflow_version: WorkflowVersion,
        random_workflow: WorkflowOut,
        mock_s3_service: S3ServiceResource,
        mock_slurm_cluster: MockSlurmCluster,
        cleanup: CleanupList,
        db: AsyncSession,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for starting a workflow execution from a public GitHub repository.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_workflow_version : clowm.models.WorkflowVersion
            Random workflow version for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        mock_slurm_cluster : clowm.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.

        """
        schema_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket,
            key=settings.s3.workflow_cache_key(
                name=DocumentationEnum.PARAMETER_SCHEMA, git_commit_hash=random_workflow_version.git_commit_hash
            ),
        )
        await schema_obj.delete()
        execution_in = WorkflowExecutionIn(workflow_version_id=random_workflow_version.git_commit_hash, parameters={})
        response = await client.post(
            self.base_path,
            headers={header_token_scheme.model.name: random_api_token.token},
            content=execution_in.model_dump_json(exclude_none=True),
        )
        assert response.status_code == status.HTTP_201_CREATED
        execution_response = WorkflowExecutionOut.model_validate_json(response.content)
        cleanup.add_task(delete_workflow_execution, db=db, execution_id=execution_response.execution_id)

        job = mock_slurm_cluster.get_job_by_name(execution_response.execution_id)
        assert job is not None

        # raises an exception if the object does not exist in the bucket
        await schema_obj.load()


class TestDevWorkflowExecutionRoutesCreate(_TestWorkflowExecutionRoutes):
    @pytest.mark.asyncio
    async def test_start_dev_workflow_execution_from_github_with_mode(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        mock_s3_service: S3ServiceResource,
        mock_slurm_cluster: MockSlurmCluster,
        cleanup: CleanupList,
        db: AsyncSession,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for starting a workflow execution with an arbitrary GitHub repository and a workflow mode.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        mock_slurm_cluster : clowm.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        mode = WorkflowModeIn(
            name=random_lower_string(10), entrypoint=random_lower_string(16), schema_path="alternative.json"
        )
        execution_in = DevWorkflowExecutionIn(
            git_commit_hash=random_hex_string(),
            repository_url="https://github.com/example-user/example",
            parameters={},
            mode=mode,
            nextflow_version="23.04.5",
        )
        response = await client.post(
            f"{self.base_path}/arbitrary",
            headers={header_token_scheme.model.name: random_api_token.token},
            content=execution_in.model_dump_json(),
        )
        assert response.status_code == status.HTTP_201_CREATED
        execution_response = WorkflowExecutionOut.model_validate_json(response.content)
        cleanup.add_task(delete_workflow_execution, db=db, execution_id=execution_response.execution_id)
        assert execution_response.executor_id == random_user.uid
        assert execution_response.status == WorkflowExecution.WorkflowExecutionStatus.PENDING

        parameter_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket,
            key=settings.s3.execution_parameters_key(execution_response.execution_id),
        )
        await parameter_obj.load()

        job = mock_slurm_cluster.get_job_by_name(execution_response.execution_id)
        assert job is not None
        assert job["job"]["environment"]["TOWER_WORKSPACE_ID"] == execution_response.execution_id.hex[:16]
        assert "NXF_SCM_FILE" not in job["job"]["environment"]

        nextflow_script = job["script"]
        assert "-hub github" in nextflow_script
        assert f"-entry {mode.entrypoint}" in nextflow_script
        assert f"-revision {execution_in.git_commit_hash}" in nextflow_script
        assert "export NXF_SCM_FILE" not in nextflow_script
        assert f"run {execution_in.repository_url}" in nextflow_script

    @pytest.mark.asyncio
    async def test_start_dev_workflow_execution_from_github(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        mock_s3_service: S3ServiceResource,
        mock_slurm_cluster: MockSlurmCluster,
        cleanup: CleanupList,
        db: AsyncSession,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for starting a workflow execution with an arbitrary GitHub repository.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        mock_slurm_cluster : clowm.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        execution_in = DevWorkflowExecutionIn(
            git_commit_hash=random_hex_string(),
            repository_url="https://github.com/example-user/example",
            parameters={},
            nextflow_version="23.04.5",
        )
        response = await client.post(
            f"{self.base_path}/arbitrary",
            headers={header_token_scheme.model.name: random_api_token.token},
            content=execution_in.model_dump_json(),
        )
        assert response.status_code == status.HTTP_201_CREATED
        execution_response = WorkflowExecutionOut.model_validate_json(response.content)
        cleanup.add_task(delete_workflow_execution, db=db, execution_id=execution_response.execution_id)
        assert execution_response.executor_id == random_user.uid
        assert execution_response.status == WorkflowExecution.WorkflowExecutionStatus.PENDING

        parameter_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket,
            key=settings.s3.execution_parameters_key(execution_response.execution_id),
        )
        await parameter_obj.load()

        job = mock_slurm_cluster.get_job_by_name(execution_response.execution_id)
        assert job is not None
        assert job["job"]["environment"]["TOWER_WORKSPACE_ID"] == execution_response.execution_id.hex[:16]
        assert "NXF_SCM_FILE" not in job["job"]["environment"]

        nextflow_script = job["script"]
        assert "-hub github" in nextflow_script
        assert "-entry" not in nextflow_script
        assert "export NXF_SCM_FILE" not in nextflow_script
        assert f"-revision {execution_in.git_commit_hash}" in nextflow_script
        assert f"run {execution_in.repository_url}" in nextflow_script

    @pytest.mark.asyncio
    async def test_start_dev_workflow_execution_from_gitlab(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        mock_s3_service: S3ServiceResource,
        mock_slurm_cluster: MockSlurmCluster,
        cleanup: CleanupList,
        db: AsyncSession,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for starting a workflow execution with an arbitrary Gitlab repository.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        mock_slurm_cluster : clowm.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        execution_in = DevWorkflowExecutionIn(
            git_commit_hash=random_hex_string(),
            repository_url="https://gitlab.com/example-user/example",
            parameters={},
            nextflow_version="23.04.5",
        )
        response = await client.post(
            f"{self.base_path}/arbitrary",
            headers={header_token_scheme.model.name: random_api_token.token},
            content=execution_in.model_dump_json(),
        )
        assert response.status_code == status.HTTP_201_CREATED
        execution_response = WorkflowExecutionOut.model_validate_json(response.content)
        cleanup.add_task(delete_workflow_execution, db=db, execution_id=execution_response.execution_id)

        scm_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket, key=settings.s3.scm_key(execution_response.execution_id)
        )
        cleanup.add_task(scm_obj.delete)

        assert execution_response.executor_id == random_user.uid
        assert execution_response.status == WorkflowExecution.WorkflowExecutionStatus.PENDING

        # Check if params file is created
        parameter_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket,
            key=settings.s3.execution_parameters_key(execution_response.execution_id),
        )
        await parameter_obj.load()
        cleanup.add_task(parameter_obj.delete)

        with BytesIO() as f:
            await scm_obj.download_fileobj(f)
            f.seek(0)
            scm = SCM.deserialize(f)

        # Check content of SCM file
        assert len(scm.providers) == 1
        provider = scm.providers[0]
        assert provider.password is None
        assert provider.name == SCMProvider.generate_name(execution_response.execution_id)
        assert provider.platform == "gitlab"
        assert provider.server == "https://gitlab.com"

        job = mock_slurm_cluster.get_job_by_name(execution_response.execution_id)
        assert job is not None
        assert job["job"]["environment"]["TOWER_WORKSPACE_ID"] == execution_response.execution_id.hex[:16]

        nextflow_script = job["script"]
        assert f"-hub {SCMProvider.generate_name(execution_response.execution_id)}" in nextflow_script
        assert "-entry" not in nextflow_script
        assert f"-revision {execution_in.git_commit_hash}" in nextflow_script
        assert f"run {execution_in.repository_url}" in nextflow_script
        assert "export NXF_SCM_FILE" in nextflow_script

    @pytest.mark.asyncio
    async def test_start_dev_workflow_execution_from_private_gitlab(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        mock_s3_service: S3ServiceResource,
        mock_slurm_cluster: MockSlurmCluster,
        cleanup: CleanupList,
        db: AsyncSession,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for starting a workflow execution with an arbitrary private Gitlab repository.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        mock_slurm_cluster : clowm.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        token = random_lower_string(15)
        execution_in = DevWorkflowExecutionIn(
            git_commit_hash=random_hex_string(),
            repository_url="https://gitlab.com/example-user/example",
            parameters={},
            token=token,
            nextflow_version="23.04.5",
        )
        response = await client.post(
            f"{self.base_path}/arbitrary",
            headers={header_token_scheme.model.name: random_api_token.token},
            content=execution_in.model_dump_json(),
        )
        assert response.status_code == status.HTTP_201_CREATED
        execution_response = WorkflowExecutionOut.model_validate_json(response.content)
        cleanup.add_task(delete_workflow_execution, db=db, execution_id=execution_response.execution_id)

        assert execution_response.executor_id == random_user.uid
        assert execution_response.status == WorkflowExecution.WorkflowExecutionStatus.PENDING

        # Check if params file is created
        parameter_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket,
            key=settings.s3.execution_parameters_key(execution_response.execution_id),
        )
        await parameter_obj.load()
        cleanup.add_task(parameter_obj.delete)

        # Check if SCM file is created
        scm_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket, key=settings.s3.scm_key(execution_response.execution_id)
        )
        cleanup.add_task(scm_obj.delete)

        with BytesIO() as f:
            await scm_obj.download_fileobj(f)
            f.seek(0)
            scm = SCM.deserialize(f)

        # Check content of SCM file
        assert len(scm.providers) == 1
        provider = scm.providers[0]
        assert provider.password == token
        assert provider.name == SCMProvider.generate_name(execution_response.execution_id)
        assert provider.platform == "gitlab"
        assert provider.server == "https://gitlab.com"

        job = mock_slurm_cluster.get_job_by_name(execution_response.execution_id)
        assert job is not None
        assert job["job"]["environment"]["TOWER_WORKSPACE_ID"] == execution_response.execution_id.hex[:16]

        nextflow_script = job["script"]
        assert f"-hub {SCMProvider.generate_name(execution_response.execution_id)}" in nextflow_script
        assert "-entry" not in nextflow_script
        assert f"-revision {execution_in.git_commit_hash}" in nextflow_script
        assert "export NXF_SCM_FILE" in nextflow_script
        assert f"run {execution_in.repository_url}" in nextflow_script

    @pytest.mark.asyncio
    async def test_start_dev_workflow_execution_from_private_github(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        mock_s3_service: S3ServiceResource,
        mock_slurm_cluster: MockSlurmCluster,
        cleanup: CleanupList,
        db: AsyncSession,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for starting a workflow execution with an arbitrary private GitHub repository.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        mock_slurm_cluster : clowm.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        token = random_lower_string(15)
        execution_in = DevWorkflowExecutionIn(
            git_commit_hash=random_hex_string(),
            repository_url="https://github.com/example-user/example",
            parameters={},
            token=token,
            nextflow_version="23.04.5",
        )
        response = await client.post(
            f"{self.base_path}/arbitrary",
            headers={header_token_scheme.model.name: random_api_token.token},
            content=execution_in.model_dump_json(),
        )
        assert response.status_code == status.HTTP_201_CREATED
        execution_response = WorkflowExecutionOut.model_validate_json(response.content)
        cleanup.add_task(delete_workflow_execution, db=db, execution_id=execution_response.execution_id)

        assert execution_response.executor_id == random_user.uid
        assert execution_response.status == WorkflowExecution.WorkflowExecutionStatus.PENDING

        # Check if params file is created
        parameter_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket,
            key=settings.s3.execution_parameters_key(execution_response.execution_id),
        )
        await parameter_obj.load()
        cleanup.add_task(parameter_obj.delete)

        # Check if SCM file is created
        scm_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket, key=settings.s3.scm_key(execution_response.execution_id)
        )
        cleanup.add_task(scm_obj.delete)
        cleanup.add_task(scm_obj.delete)
        with BytesIO() as f:
            await scm_obj.download_fileobj(f)
            f.seek(0)
            scm = SCM.deserialize(f)

        # Check content of SCM file
        assert len(scm.providers) == 1
        provider = scm.providers[0]
        assert provider.password == token
        assert provider.name == "github"
        assert provider.user == "example-user"

        job = mock_slurm_cluster.get_job_by_name(execution_response.execution_id)
        assert job is not None
        assert job["job"]["environment"]["TOWER_WORKSPACE_ID"] == execution_response.execution_id.hex[:16]

        nextflow_script = job["script"]
        assert "-hub github" in nextflow_script
        assert "-entry" not in nextflow_script
        assert f"-revision {execution_in.git_commit_hash}" in nextflow_script
        assert "export NXF_SCM_FILE" in nextflow_script
        assert f"run {execution_in.repository_url}" in nextflow_script

    @pytest.mark.asyncio
    async def test_start_dev_workflow_execution_with_unknown_repository(
        self, client: AsyncClient, mock_s3_service: S3ServiceResource, random_api_token: ApiToken
    ) -> None:
        """
        Test for starting a workflow execution with an unknown git repository.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        """
        execution_in = DevWorkflowExecutionIn(
            git_commit_hash=random_hex_string(),
            repository_url="https://bitbucket.com/example/example",
            parameters={},
            nextflow_version="23.04.5",
        )
        response = await client.post(
            f"{self.base_path}/arbitrary",
            headers={header_token_scheme.model.name: random_api_token.token},
            content=execution_in.model_dump_json(),
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST


class TestWorkflowExecutionRoutesGet(_TestWorkflowExecutionRoutes):
    @pytest.mark.asyncio
    async def test_get_workflow_execution(
        self, client: AsyncClient, random_running_workflow_execution: WorkflowExecution, random_api_token: ApiToken
    ) -> None:
        """
        Test for getting a workflow execution.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        response = await client.get(
            "/".join([self.base_path, str(random_running_workflow_execution.execution_id)]),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_200_OK

        execution = response.json()
        assert execution["execution_id"] == str(random_running_workflow_execution.execution_id)

    @pytest.mark.asyncio
    async def test_get_workflow_execution_params(
        self, client: AsyncClient, random_running_workflow_execution: WorkflowExecution, random_api_token: ApiToken
    ) -> None:
        """
        Test for getting the parameters of a workflow execution.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        response = await client.get(
            "/".join([self.base_path, str(random_running_workflow_execution.execution_id), "params"]),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.asyncio
    async def test_get_no_existing_workflow_execution_params(
        self,
        client: AsyncClient,
        random_running_workflow_execution: WorkflowExecution,
        mock_s3_service: S3ServiceResource,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for getting the deleted parameters of a workflow execution.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        mock_s3_service: types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        """
        obj = await mock_s3_service.Object(
            settings.s3.data_bucket,
            settings.s3.execution_parameters_key(random_running_workflow_execution.execution_id),
        )
        await obj.delete()

        response = await client.get(
            "/".join([self.base_path, str(random_running_workflow_execution.execution_id), "params"]),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_get_non_existing_workflow_execution(self, client: AsyncClient, random_api_token: ApiToken) -> None:
        """
        Test for getting a non-existing workflow execution.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        """
        response = await client.get(
            "/".join([self.base_path, str(uuid4())]), headers={header_token_scheme.model.name: random_api_token.token}
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND


class TestWorkflowExecutionRoutesList(_TestWorkflowExecutionRoutes):
    @pytest.mark.asyncio
    async def test_list_workflow_executions(
        self, client: AsyncClient, random_running_workflow_execution: WorkflowExecution, random_api_token: ApiToken
    ) -> None:
        """
        Test for listing all workflow executions.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        response = await client.get(self.base_path, headers={header_token_scheme.model.name: random_api_token.token})
        assert response.status_code == status.HTTP_200_OK

        executions = ExecutionList.validate_json(response.content)
        assert len(executions) > 0
        assert (
            sum(
                1
                for execution in executions
                if execution.execution_id == random_running_workflow_execution.execution_id
            )
            == 1
        )
        assert response.headers.get("link", None) is None

    @pytest.mark.asyncio
    async def test_list_workflow_executions_with_page_size(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        cleanup: CleanupList,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for listing all workflow executions with pagination.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        page_size = 5
        for _ in range(page_size):
            execution = WorkflowExecution(executor_id=random_user.uid, slurm_job_id=1)
            db.add(execution)
            await db.commit()
            cleanup.add_task(delete_workflow_execution, db=db, execution_id=execution.execution_id)

        response1 = await client.get(
            self.base_path,
            headers={header_token_scheme.model.name: random_api_token.token},
            params={"per_page": page_size},
        )

        assert response1.status_code == status.HTTP_200_OK
        assert len(ExecutionList.validate_json(response1.content)) == page_size
        # if there are no more executions, the link header should be empty
        assert response1.headers.get("link", None) is None

        execution2 = WorkflowExecution(executor_id=random_user.uid, slurm_job_id=1)
        db.add(execution2)
        await db.commit()
        cleanup.add_task(delete_workflow_execution, db=db, execution_id=execution2.execution_id)

        response2 = await client.get(
            self.base_path,
            headers={header_token_scheme.model.name: random_api_token.token},
            params={"per_page": page_size},
        )
        assert response2.status_code == status.HTTP_200_OK
        assert len(ExecutionList.validate_json(response2.content)) == page_size
        # if there are more executions, the link header should point to the next pagination url
        next_link = HTTPLinkHeader.parse(response2.headers.get("link", "")).get(rel="next", default=None)
        assert next_link is not None
        link_query_params = urllib.parse.parse_qs(next_link.link.query)

        page_query_param = link_query_params.get("per_page", None)
        assert page_query_param is not None
        assert len(page_query_param) == 1
        assert int(page_query_param[0]) == page_size

        assert link_query_params.get("id_after", None) is not None

        response3 = await client.get(
            str(next_link.link), headers={header_token_scheme.model.name: random_api_token.token}
        )
        assert response3.status_code == status.HTTP_200_OK


class TestWorkflowExecutionRoutesDelete(_TestWorkflowExecutionRoutes):
    @pytest.mark.asyncio
    async def test_delete_unfinished_workflow_execution(
        self, client: AsyncClient, random_running_workflow_execution: WorkflowExecution, random_api_token: ApiToken
    ) -> None:
        """
        Test for deleting an unfinished workflow execution.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        response = await client.delete(
            "/".join([self.base_path, str(random_running_workflow_execution.execution_id)]),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_delete_finished_workflow_execution(
        self,
        client: AsyncClient,
        db: AsyncSession,
        random_completed_workflow_execution: WorkflowExecution,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for deleting a workflow execution.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        random_completed_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        response1 = await client.delete(
            "/".join([self.base_path, str(random_completed_workflow_execution.execution_id)]),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response1.status_code == status.HTTP_204_NO_CONTENT

        response2 = await client.get(
            "/".join([self.base_path, str(random_completed_workflow_execution.execution_id)]),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response2.status_code == status.HTTP_404_NOT_FOUND


class TestWorkflowExecutionRoutesCancel(_TestWorkflowExecutionRoutes):
    @pytest.mark.asyncio
    async def test_cancel_workflow_execution(
        self,
        client: AsyncClient,
        random_running_workflow_execution: WorkflowExecution,
        mock_slurm_cluster: MockSlurmCluster,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for canceling a workflow execution.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        mock_slurm_cluster : clowm.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        """
        response = await client.post(
            "/".join([self.base_path, str(random_running_workflow_execution.execution_id), "cancel"]),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT
        job_active = mock_slurm_cluster.job_active(random_running_workflow_execution.slurm_job_id)
        assert not job_active

    @pytest.mark.asyncio
    async def test_cancel_finished_workflow_execution(
        self,
        client: AsyncClient,
        db: AsyncSession,
        random_running_workflow_execution: WorkflowExecution,
        random_api_token: ApiToken,
    ) -> None:
        """
        Test for canceling a finished workflow execution.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_api_token : clowm.models.ApiToken
            Random api token for testing.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing.
        """
        await db.execute(
            update(WorkflowExecution)
            .where(WorkflowExecution.execution_id == random_running_workflow_execution.execution_id)
            .values(status=WorkflowExecution.WorkflowExecutionStatus.SUCCESS.name)
        )
        await db.commit()
        response = await client.post(
            "/".join([self.base_path, str(random_running_workflow_execution.execution_id), "cancel"]),
            headers={header_token_scheme.model.name: random_api_token.token},
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST
