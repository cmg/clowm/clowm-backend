from io import BytesIO
from typing import TYPE_CHECKING
from uuid import uuid4

import pytest
from botocore.exceptions import ClientError
from fastapi import status
from httpx import AsyncClient
from PIL import Image
from pydantic import TypeAdapter
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.core.config import settings
from clowm.models import WorkflowMode, WorkflowVersion
from clowm.schemas.documentation_enum import DocumentationEnum
from clowm.schemas.workflow import WorkflowOut
from clowm.schemas.workflow_version import (
    IconUpdateOut,
    NextflowVersion,
    ParameterExtension,
    WorkflowVersionMetadataIn,
    WorkflowVersionMetadataOut,
    WorkflowVersionStatusSchema,
)
from clowm.schemas.workflow_version import WorkflowVersionOut as WorkflowVersionOut
from clowm.tests.mocks import MockContainerRegistry
from clowm.tests.utils import CleanupList, UserWithAuthCookie, random_hex_string, random_lower_string

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object

WorkflowVersionList = TypeAdapter(list[WorkflowVersionOut])


class _TestWorkflowVersionRoutes:
    base_path: str = "/ui/workflows"


class TestWorkflowVersionRoutesGet(_TestWorkflowVersionRoutes):
    @pytest.mark.asyncio
    async def test_get_workflow_version(
        self, client: AsyncClient, random_user: UserWithAuthCookie, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for getting a workflow version by its commit hash.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow_version : clowm.schemas.workflow_version.WorkflowVersionOut
            Random workflow version for testing.
        """
        response = await client.get(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow_version.workflow_id),
                    "versions",
                    random_workflow_version.git_commit_hash,
                ]
            ),
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_200_OK
        version = WorkflowVersionOut.model_validate_json(response.content)
        assert version.workflow_id == random_workflow_version.workflow_id
        assert version.workflow_version_id == random_workflow_version.git_commit_hash

    @pytest.mark.asyncio
    async def test_get_non_existing_version(
        self, client: AsyncClient, random_user: UserWithAuthCookie, random_workflow: WorkflowOut
    ) -> None:
        """
        Test for getting a non-existing workflow version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.get(
            "/".join([self.base_path, str(random_workflow.workflow_id), "versions", random_hex_string()]),
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_get_workflow_version_meta_data(
        self, client: AsyncClient, random_user: UserWithAuthCookie, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for getting a workflow version by its commit hash.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow_version : clowm.schemas.workflow_version.WorkflowVersionOut
            Random workflow version for testing.
        """
        response = await client.get(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow_version.workflow_id),
                    "versions",
                    random_workflow_version.git_commit_hash,
                    "metadata",
                ]
            ),
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_200_OK
        metadata = WorkflowVersionMetadataOut.model_validate_json(response.content)
        assert metadata.nextflow_version == random_workflow_version.nextflow_version
        assert metadata.nextflow_config == random_workflow_version.nextflow_config
        assert metadata.default_container == random_workflow_version.default_container


class TestWorkflowVersionRoutesList(_TestWorkflowVersionRoutes):
    @pytest.mark.asyncio
    async def test_get_workflow_version(
        self, client: AsyncClient, random_user: UserWithAuthCookie, random_workflow: WorkflowOut
    ) -> None:
        """
        Test for listing all workflow version from a given workflow.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.get(
            "/".join([self.base_path, str(random_workflow.workflow_id), "versions"]),
            params={"version_status": WorkflowVersion.WorkflowVersionStatus.CREATED.name},
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_200_OK
        versions = WorkflowVersionList.validate_json(response.content)
        assert len(versions) == 1
        assert versions[0].workflow_id == random_workflow.workflow_id
        assert versions[0].workflow_version_id == random_workflow.versions[0].workflow_version_id


class TestWorkflowVersionRoutesUpdate(_TestWorkflowVersionRoutes):
    @pytest.mark.asyncio
    async def test_update_workflow_version_parameter_extension(
        self, client: AsyncClient, random_user: UserWithAuthCookie, random_workflow: WorkflowOut
    ) -> None:
        """
        Test updating a workflow version status parameter extension.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.patch(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow.workflow_id),
                    "versions",
                    random_workflow.versions[0].workflow_version_id,
                    "parameter-extension",
                ]
            ),
            cookies=random_user.auth_cookie,
            content=ParameterExtension().model_dump_json(),
        )

        assert response.status_code == status.HTTP_200_OK
        version = WorkflowVersionOut.model_validate_json(response.content)
        assert version.workflow_version_id == random_workflow.versions[0].workflow_version_id
        assert version.parameter_extension is not None

    @pytest.mark.asyncio
    async def test_update_workflow_version_status(
        self, client: AsyncClient, random_user: UserWithAuthCookie, random_workflow: WorkflowOut
    ) -> None:
        """
        Test updating a workflow version status.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.patch(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow.workflow_id),
                    "versions",
                    random_workflow.versions[0].workflow_version_id,
                    "status",
                ]
            ),
            cookies=random_user.auth_cookie,
            content=WorkflowVersionStatusSchema(
                status=WorkflowVersion.WorkflowVersionStatus.PUBLISHED
            ).model_dump_json(),
        )

        assert response.status_code == status.HTTP_200_OK
        version = WorkflowVersionOut.model_validate_json(response.content)
        assert version.workflow_version_id == random_workflow.versions[0].workflow_version_id
        assert version.status == WorkflowVersion.WorkflowVersionStatus.PUBLISHED

    @pytest.mark.asyncio
    async def test_deprecate_workflow_version(
        self, client: AsyncClient, random_user: UserWithAuthCookie, random_workflow: WorkflowOut
    ) -> None:
        """
        Test deprecate a workflow version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.patch(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow.workflow_id),
                    "versions",
                    random_workflow.versions[0].workflow_version_id,
                    "deprecate",
                ]
            ),
            cookies=random_user.auth_cookie,
        )

        assert response.status_code == status.HTTP_200_OK
        version = WorkflowVersionOut.model_validate_json(response.content)
        assert version.workflow_version_id == random_workflow.versions[0].workflow_version_id
        assert version.status == WorkflowVersion.WorkflowVersionStatus.DEPRECATED

    @pytest.mark.asyncio
    async def test_update_non_existing_workflow_version_status(
        self, client: AsyncClient, random_user: UserWithAuthCookie, random_workflow: WorkflowOut
    ) -> None:
        """
        Test updating a non-existing workflow version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.patch(
            "/".join([self.base_path, str(random_workflow.workflow_id), "versions", random_hex_string(), "status"]),
            cookies=random_user.auth_cookie,
            content=WorkflowVersionStatusSchema(
                status=WorkflowVersion.WorkflowVersionStatus.PUBLISHED
            ).model_dump_json(),
        )

        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_deprecate_non_existing_workflow_version(
        self, client: AsyncClient, random_user: UserWithAuthCookie, random_workflow: WorkflowOut
    ) -> None:
        """
        Test deprecate a non-existing workflow version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.patch(
            "/".join([self.base_path, str(random_workflow.workflow_id), "versions", random_hex_string(), "deprecate"]),
            cookies=random_user.auth_cookie,
        )

        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_update_default_container(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow: WorkflowOut,
        cleanup: CleanupList,
        mock_container_registry: MockContainerRegistry,
    ) -> None:
        """
        Test update the default container of a workflow version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        mock_container_registry: clowm.tests.mocks.mock_container_registry.MockContainerRegistry
            Mock object to temporarily activate errors
        """
        meta_data_in = WorkflowVersionMetadataIn(
            nextflow_version=random_workflow.versions[0].nextflow_version,
            default_container="ubuntu:22.04",
            nextflow_config=None,
        )
        response1 = await client.put(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow.workflow_id),
                    "versions",
                    random_workflow.versions[0].workflow_version_id,
                    "metadata",
                ]
            ),
            cookies=random_user.auth_cookie,
            content=meta_data_in.model_dump_json(),
        )

        assert response1.status_code == status.HTTP_200_OK
        meta_data_out = WorkflowVersionMetadataOut.model_validate_json(response1.content)
        assert meta_data_out.nextflow_version == meta_data_in.nextflow_version
        assert meta_data_out.default_container == meta_data_in.default_container
        assert meta_data_out.nextflow_config == meta_data_in.nextflow_config

        mock_container_registry.send_error = True
        cleanup.add_task(mock_container_registry.reset)
        response2 = await client.put(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow.workflow_id),
                    "versions",
                    random_workflow.versions[0].workflow_version_id,
                    "metadata",
                ]
            ),
            cookies=random_user.auth_cookie,
            content=WorkflowVersionMetadataIn(
                nextflow_version=random_workflow.versions[0].nextflow_version,
                default_container="ubuntu:24.04",
                nextflow_config=None,
            ).model_dump_json(),
        )

        assert response2.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_update_nextflow_version(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow: WorkflowOut,
    ) -> None:
        """
        Test update the Nextflow version of a workflow version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        meta_data_in = WorkflowVersionMetadataIn(
            nextflow_version=NextflowVersion.v22_10_3,
            default_container=None,
            nextflow_config=None,
        )
        response1 = await client.put(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow.workflow_id),
                    "versions",
                    random_workflow.versions[0].workflow_version_id,
                    "metadata",
                ]
            ),
            cookies=random_user.auth_cookie,
            content=meta_data_in.model_dump_json(),
        )

        assert response1.status_code == status.HTTP_200_OK
        meta_data_out = WorkflowVersionMetadataOut.model_validate_json(response1.content)
        assert meta_data_out.nextflow_version == meta_data_in.nextflow_version
        assert meta_data_out.default_container == meta_data_in.default_container
        assert meta_data_out.nextflow_config == meta_data_in.nextflow_config

    @pytest.mark.asyncio
    async def test_update_nextflow_config(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow: WorkflowOut,
        cleanup: CleanupList,
        mock_s3_service: S3ServiceResource,
    ) -> None:
        """
        Test update the Nextflow version of a workflow version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        """
        meta_data_in = WorkflowVersionMetadataIn(
            nextflow_version=random_workflow.versions[0].nextflow_version,
            default_container=None,
            nextflow_config=random_lower_string(128),
        )
        response1 = await client.put(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow.workflow_id),
                    "versions",
                    random_workflow.versions[0].workflow_version_id,
                    "metadata",
                ]
            ),
            cookies=random_user.auth_cookie,
            content=meta_data_in.model_dump_json(),
        )

        assert response1.status_code == status.HTTP_200_OK
        nextflow_config_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket,
            key=settings.s3.config_key(random_workflow.versions[0].workflow_version_id),
        )
        cleanup.add_task(nextflow_config_obj.delete)

        meta_data_out = WorkflowVersionMetadataOut.model_validate_json(response1.content)
        assert meta_data_out.nextflow_version == meta_data_in.nextflow_version
        assert meta_data_out.default_container == meta_data_in.default_container
        assert meta_data_out.nextflow_config == meta_data_in.nextflow_config

        with BytesIO() as f:
            await nextflow_config_obj.download_fileobj(f)
            f.seek(0)
            assert meta_data_in.nextflow_config == f.read().decode("utf-8")

        response2 = await client.put(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow.workflow_id),
                    "versions",
                    random_workflow.versions[0].workflow_version_id,
                    "metadata",
                ]
            ),
            cookies=random_user.auth_cookie,
            content=WorkflowVersionMetadataIn(
                nextflow_version=random_workflow.versions[0].nextflow_version,
                default_container=None,
                nextflow_config=None,
            ).model_dump_json(),
        )
        assert response2.status_code == status.HTTP_200_OK
        with pytest.raises(ClientError):
            await nextflow_config_obj.load()


class TestWorkflowVersionRoutesGetDocumentation(_TestWorkflowVersionRoutes):
    @pytest.mark.asyncio
    @pytest.mark.parametrize("document", [d for d in DocumentationEnum])
    async def test_download_workflow_version_documentation(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow_version: WorkflowVersion,
        document: DocumentationEnum,
        mock_s3_service: S3ServiceResource,
    ) -> None:
        """
        Test downloading all the different documentation file for a workflow version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow_version : clowm.models.WorkflowVersion
            Random workflow version for testing.
        document : clowm.api.endpoints.workflow_version.DocumentationEnum
            All possible documents as pytest parameter.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        """
        response1 = await client.get(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow_version.workflow_id),
                    "versions",
                    random_workflow_version.git_commit_hash,
                    "documentation",
                ]
            ),
            cookies=random_user.auth_cookie,
            params={"document": document},
        )
        assert response1.status_code == status.HTTP_200_OK

        bucket = await mock_s3_service.Bucket(settings.s3.data_bucket)
        await bucket.objects.filter(
            Prefix=settings.s3.workflow_cache_key(
                "", git_commit_hash=random_workflow_version.git_commit_hash, mode_id=None
            )
        ).delete()

        obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket,
            key=settings.s3.workflow_cache_key(
                document, git_commit_hash=random_workflow_version.git_commit_hash, mode_id=None
            ),
        )
        with pytest.raises(ClientError):
            await obj.load()

        response2 = await client.get(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow_version.workflow_id),
                    "versions",
                    random_workflow_version.git_commit_hash,
                    "documentation",
                ]
            ),
            cookies=random_user.auth_cookie,
            params={"document": document},
        )
        assert response2.status_code == status.HTTP_200_OK

        await obj.load()  # will raise error if the document was not cached in S3

    @pytest.mark.asyncio
    @pytest.mark.parametrize("document", [d for d in DocumentationEnum])
    async def test_download_workflow_version_documentation_with_non_existing_mode(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow_version: WorkflowVersion,
        document: DocumentationEnum,
    ) -> None:
        """
        Test downloading all the different documentation file for a workflow version with a non-existing workflow mode.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow_version : clowm.models.WorkflowVersion
            Random workflow version for testing.
        document : clowm.api.endpoints.workflow_version.DocumentationEnum
            All possible documents as pytest parameter.
        """
        response = await client.get(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow_version.workflow_id),
                    "versions",
                    random_workflow_version.git_commit_hash,
                    "documentation",
                ]
            ),
            cookies=random_user.auth_cookie,
            params={"document": document, "mode_id": str(uuid4())},
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    @pytest.mark.parametrize("document", [d for d in DocumentationEnum])
    async def test_download_workflow_version_documentation_with_existing_mode(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow_version: WorkflowVersion,
        random_workflow_mode: WorkflowMode,
        mock_s3_service: S3ServiceResource,
        document: DocumentationEnum,
    ) -> None:
        """
        Test downloading all the different documentation file for a workflow version with a workflow mode.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow_version : clowm.models.WorkflowVersion
            Random workflow version for testing.
        random_workflow_mode : clowm.models.WorkflowMode
            Random workflow mode for testing.
        document : clowm.api.endpoints.workflow_version.DocumentationEnum
            All possible documents as pytest parameter.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        """
        response1 = await client.get(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow_version.workflow_id),
                    "versions",
                    random_workflow_version.git_commit_hash,
                    "documentation",
                ]
            ),
            cookies=random_user.auth_cookie,
            params={"document": document, "mode_id": str(random_workflow_mode.mode_id)},
        )
        assert response1.status_code == status.HTTP_200_OK

        bucket = await mock_s3_service.Bucket(settings.s3.data_bucket)
        await bucket.objects.filter(
            Prefix=settings.s3.workflow_cache_key(
                "", git_commit_hash=random_workflow_version.git_commit_hash, mode_id=None
            )
        ).delete()

        obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket,
            key=settings.s3.workflow_cache_key(
                document,
                git_commit_hash=random_workflow_version.git_commit_hash,
                mode_id=random_workflow_mode.mode_id if document == DocumentationEnum.PARAMETER_SCHEMA else None,
            ),
        )
        with pytest.raises(ClientError):
            await obj.load()

        response2 = await client.get(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow_version.workflow_id),
                    "versions",
                    random_workflow_version.git_commit_hash,
                    "documentation",
                ]
            ),
            cookies=random_user.auth_cookie,
            params={"document": document, "mode_id": str(random_workflow_mode.mode_id)},
        )
        assert response2.status_code == status.HTTP_200_OK

        await obj.load()  # will raise error if the document was not cached in S3


class TestWorkflowVersionIconRoutes(_TestWorkflowVersionRoutes):
    @pytest.mark.asyncio
    async def test_upload_new_icon(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow_version: WorkflowVersion,
        mock_s3_service: S3ServiceResource,
        db: AsyncSession,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for uploading a new icon for a workflow version

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow_version : clowm.models.WorkflowVersion
            Random workflow version for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        img_buffer = BytesIO()
        Image.linear_gradient(mode="L").save(img_buffer, "PNG")
        img_buffer.seek(0)
        files = {"icon": ("RickRoll.png", img_buffer, "image/png")}
        response = await client.post(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow_version.workflow_id),
                    "versions",
                    random_workflow_version.git_commit_hash,
                    "icon",
                ]
            ),
            cookies=random_user.auth_cookie,
            files=files,
        )
        assert response.status_code == status.HTTP_201_CREATED
        await db.reset()
        icon_url = IconUpdateOut.model_validate_json(response.content).icon_url
        assert icon_url.path is not None
        icon_slug = icon_url.path.split("/")[-1]
        icon_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket, key=settings.s3.icon_key(icon_slug)
        )
        await icon_obj.load()
        cleanup.add_task(icon_obj.delete)
        db_version = await db.scalar(
            select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == random_workflow_version.git_commit_hash)
        )
        assert db_version is not None
        assert db_version.icon_slug == icon_slug

    @pytest.mark.asyncio
    async def test_upload_new_icon_as_text(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow_version: WorkflowVersion,
        mock_s3_service: S3ServiceResource,
        db: AsyncSession,
    ) -> None:
        """
        Test for uploading a textfile as a new icon for a workflow version

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow_version : clowm.models.WorkflowVersion
            Random workflow version for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """

        files = {"icon": ("RickRoll.png", BytesIO(b"Never gonna give you up"), "text/plain")}
        response = await client.post(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow_version.workflow_id),
                    "versions",
                    random_workflow_version.git_commit_hash,
                    "icon",
                ]
            ),
            cookies=random_user.auth_cookie,
            files=files,
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_upload_new_icon_as_non_developer(
        self,
        client: AsyncClient,
        random_second_user: UserWithAuthCookie,
        random_workflow_version: WorkflowVersion,
    ) -> None:
        """
        Test for uploading a new icon for a workflow version

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow_version : clowm.models.WorkflowVersion
            Random workflow version for testing.
        """
        files = {"icon": ("RickRoll.txt", BytesIO(b"Never gonna give you up"), "plain/text")}
        response = await client.post(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow_version.workflow_id),
                    "versions",
                    random_workflow_version.git_commit_hash,
                    "icon",
                ]
            ),
            cookies=random_second_user.auth_cookie,
            files=files,
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.asyncio
    async def test_delete_icon(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow_version: WorkflowVersion,
        mock_s3_service: S3ServiceResource,
        db: AsyncSession,
    ) -> None:
        """
        Test for deleting a workflow version icon

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow_version : clowm.models.WorkflowVersion
            Random workflow version for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        old_slug = random_workflow_version.icon_slug
        assert old_slug is not None
        response = await client.delete(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow_version.workflow_id),
                    "versions",
                    random_workflow_version.git_commit_hash,
                    "icon",
                ]
            ),
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT
        await db.reset()
        with pytest.raises(ClientError):
            icon_obj = await mock_s3_service.Object(
                bucket_name=settings.s3.data_bucket, key=settings.s3.icon_key(old_slug)
            )
            await icon_obj.load()
        db_version = await db.scalar(
            select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == random_workflow_version.git_commit_hash)
        )
        assert db_version is not None
        assert db_version.icon_slug is None

    @pytest.mark.asyncio
    async def test_delete_icon_as_non_developer(
        self,
        client: AsyncClient,
        random_second_user: UserWithAuthCookie,
        random_workflow_version: WorkflowVersion,
        mock_s3_service: S3ServiceResource,
        db: AsyncSession,
    ) -> None:
        """
        Test for deleting a workflow version icon

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow_version : clowm.models.WorkflowVersion
            Random workflow version for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        response = await client.delete(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow_version.workflow_id),
                    "versions",
                    random_workflow_version.git_commit_hash,
                    "icon",
                ]
            ),
            cookies=random_second_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN
