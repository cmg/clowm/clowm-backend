import pytest
from fastapi import status
from httpx import AsyncClient

from clowm.tests.utils import UserWithAuthCookie


class TestHealthRoute:
    @pytest.mark.asyncio
    async def test_health_route(self, client: AsyncClient) -> None:
        """
        Test service health route

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        """
        response = await client.get("/health")
        assert response.status_code == status.HTTP_200_OK
        body = response.json()
        assert body["status"] == "OK"


class TestOpenAPIRoute:
    @pytest.mark.asyncio
    async def test_openapi_route(self, client: AsyncClient, random_user: UserWithAuthCookie) -> None:
        """
        Test for getting the OpenAPI specification and the caching mechanism.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        response1 = await client.get("/ui/openapi.json")
        assert response1.status_code == status.HTTP_403_FORBIDDEN

        response2 = await client.get("/ui/openapi.json", cookies=random_user.auth_cookie)
        assert response2.status_code == status.HTTP_200_OK

    @pytest.mark.asyncio
    async def test_swagger_ui_route(self, client: AsyncClient, random_user: UserWithAuthCookie) -> None:
        """
        Test for getting the Swagger UI.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        response1 = await client.get("/ui/docs")
        assert response1.status_code == status.HTTP_403_FORBIDDEN

        response2 = await client.get("/ui/docs", cookies=random_user.auth_cookie)
        assert response2.status_code == status.HTTP_200_OK
