import pytest
from fastapi import status
from httpx import AsyncClient
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.schemas.security import ErrorDetail
from clowm.tests.utils import UserWithAuthCookie


class TestJWTProtectedRoutes:
    protected_route: str = "/ui/users/me"

    @pytest.mark.asyncio
    async def test_missing_authorization_header(self, client: AsyncClient) -> None:
        """
        Test with missing authorization header on a protected route.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        """
        response = await client.get(self.protected_route)
        assert response.status_code == status.HTTP_403_FORBIDDEN
        error = ErrorDetail.model_validate_json(response.content)
        assert error.detail == "Not authenticated"

    @pytest.mark.asyncio
    async def test_malformed_authorization_header(self, client: AsyncClient) -> None:
        """
        Test with malformed authorization header on a protected route.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        """
        response = await client.get(self.protected_route, cookies={"clowm-jwt": "not-a-jwt-token"})
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

        error = ErrorDetail.model_validate_json(response.content)
        assert error.detail == "malformed JWT"

        response_cookie = response.cookies.get("clowm-jwt", None)
        assert response_cookie is None

    @pytest.mark.asyncio
    async def test_correct_authorization_header(self, client: AsyncClient, random_user: UserWithAuthCookie) -> None:
        """
        Test with correct authorization header on a protected route.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.UserWithAuthCookie
            HTTP Headers to authorize the request.
        """
        response = await client.get(self.protected_route, cookies=random_user.auth_cookie)
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.asyncio
    async def test_protected_route_with_deleted_user(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
    ) -> None:
        """
        Test with correct authorization header from a deleted user on a protected route.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.UserWithAuthCookie
            Random user for testing. pytest fixture.
        """
        await db.delete(random_user.user)
        await db.commit()

        response = await client.get(self.protected_route, cookies=random_user.auth_cookie)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED


class TestRBACProtectedRoutes:
    admin_route = "/ui/users"
    user_route = "/ui/users/me"

    @pytest.mark.asyncio
    async def test_admin_route_with_admin_user(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
    ) -> None:
        """
        Test calling a RBAC protected admin route with admin user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.UserWithAuthCookie
            Random user for testing. pytest fixture.
        """
        response = await client.get(self.admin_route, cookies=random_user.auth_cookie)
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.asyncio
    async def test_admin_route_with_normal_user(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_second_user: UserWithAuthCookie,
    ) -> None:
        """
         Test calling a RBAC protected admin route with normal user.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_second_user : clowm.tests.utils.UserWithAuthCookie
            Random user for testing. pytest fixture.
        """

        response = await client.get(self.admin_route, cookies=random_second_user.auth_cookie)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.asyncio
    async def test_admin_route_with_no_role(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_third_user: UserWithAuthCookie,
    ) -> None:
        """
         Test calling a RBAC protected admin route with user no role.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_third_user : clowm.tests.utils.UserWithAuthCookie
            Random user for testing. pytest fixture.
        """

        response = await client.get(self.admin_route, cookies=random_third_user.auth_cookie)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.asyncio
    async def test_normal_route_with_normal_user(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_second_user: UserWithAuthCookie,
    ) -> None:
        """
         Test calling a RBAC protected route with a normal user.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_second_user : clowm.tests.utils.UserWithAuthCookie
            Random user for testing. pytest fixture.
        """

        response = await client.get(self.user_route, cookies=random_second_user.auth_cookie)
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.asyncio
    async def test_normal_route_with_no_role(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_third_user: UserWithAuthCookie,
    ) -> None:
        """
        Test calling a RBAC protected route with user without a role.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_third_user : clowm.tests.utils.UserWithAuthCookie
            Random user for testing. pytest fixture.
        """

        response = await client.get(self.user_route, cookies=random_third_user.auth_cookie)
        assert response.status_code == status.HTTP_200_OK
