from datetime import date
from io import BytesIO
from typing import TYPE_CHECKING
from uuid import uuid4

import pytest
from botocore.exceptions import ClientError
from fastapi import status
from httpx import AsyncClient
from pydantic import TypeAdapter
from sqlalchemy import select, update
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.core.config import settings
from clowm.db.mixins.ownership_transfer_mixin import OwnershipTypeEnum
from clowm.models import Workflow, WorkflowExecution, WorkflowMode, WorkflowVersion
from clowm.schemas.documentation_enum import DocumentationEnum
from clowm.schemas.ownership_transfer import OwnershipTransferRequestIn, OwnershipTransferRequestOut
from clowm.schemas.workflow import WorkflowIn, WorkflowOut, WorkflowStatistic, WorkflowUpdate
from clowm.schemas.workflow_execution import AnonymizedWorkflowExecution
from clowm.schemas.workflow_mode import WorkflowModeIn
from clowm.schemas.workflow_version import NextflowVersion
from clowm.schemas.workflow_version import WorkflowVersionOut as WorkflowVersionOut
from clowm.scm import SCM, SCMProvider
from clowm.tests.mocks import DefaultMockHTTPService
from clowm.tests.utils import (
    CleanupList,
    UserWithAuthCookie,
    delete_workflow,
    delete_workflow_mode,
    random_hex_string,
    random_lower_string,
)
from clowm.tests.utils.otr import create_otr

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object

WorkflowList = TypeAdapter(list[WorkflowOut])


class _TestWorkflowRoutes:
    base_path: str = "/ui/workflows"


class TestWorkflowRoutesCreate(_TestWorkflowRoutes):
    @pytest.mark.asyncio
    async def test_create_workflow_with_github(
        self, db: AsyncSession, client: AsyncClient, random_user: UserWithAuthCookie, cleanup: CleanupList
    ) -> None:
        """
        Exhaustive Test for successfully creating a workflow.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        git_commit_hash = random_hex_string()
        workflow = WorkflowIn(
            git_commit_hash=git_commit_hash,
            name=random_lower_string(10),
            short_description=random_lower_string(65),
            repository_url="https://github.de/example-user/example",
            nextflow_version=NextflowVersion.v24_04_4,
            initial_version="v1.0.0",
        )
        response = await client.post(
            self.base_path, content=workflow.model_dump_json(), cookies=random_user.auth_cookie
        )
        assert response.status_code == status.HTTP_201_CREATED
        await db.reset()
        created_workflow = WorkflowOut.model_validate_json(response.content)
        cleanup.add_task(delete_workflow, db=db, workflow_id=created_workflow.workflow_id)
        assert not created_workflow.private

        stmt1 = select(Workflow).where(Workflow.workflow_id == created_workflow.workflow_id)
        db_workflow = await db.scalar(stmt1)
        assert db_workflow is not None

        stmt2 = select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == git_commit_hash)
        db_version = await db.scalar(stmt2)
        assert db_version is not None

    @pytest.mark.asyncio
    async def test_create_workflow_with_gitlab(
        self,
        client: AsyncClient,
        db: AsyncSession,
        random_user: UserWithAuthCookie,
        mock_s3_service: S3ServiceResource,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for successfully creating a workflow with a Gitlab repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        mock_s3_service: types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        workflow = WorkflowIn(
            git_commit_hash=random_hex_string(),
            name=random_lower_string(10),
            short_description=random_lower_string(65),
            repository_url="https://gitlab.de/example-user/example",
            nextflow_version=NextflowVersion.v24_04_4,
            initial_version="v1.0.0",
        )
        response = await client.post(
            self.base_path, content=workflow.model_dump_json(), cookies=random_user.auth_cookie
        )
        assert response.status_code == status.HTTP_201_CREATED
        await db.reset()
        created_workflow = WorkflowOut.model_validate_json(response.content)
        cleanup.add_task(delete_workflow, db=db, workflow_id=created_workflow.workflow_id)
        assert not created_workflow.private

        stmt = select(Workflow).where(Workflow.workflow_id == created_workflow.workflow_id)
        db_workflow = await db.scalar(stmt)
        assert db_workflow is not None

        # Download SCM file in DATA_BUCKET that should be created
        scm_key = settings.s3.scm_key(db_workflow.workflow_id)
        scm_obj = await mock_s3_service.Object(bucket_name=settings.s3.data_bucket, key=scm_key)
        cleanup.add_task(scm_obj.delete)

        with BytesIO() as f:
            await scm_obj.download_fileobj(f)
            f.seek(0)
            scm = SCM.deserialize(f)

        # Check content of SCM file
        assert len(scm.providers) == 1
        provider = scm.providers[0]
        assert provider.password is None
        assert provider.name == SCMProvider.generate_name(db_workflow.workflow_id)
        assert provider.platform == "gitlab"
        assert provider.server == "https://gitlab.de"

    @pytest.mark.asyncio
    async def test_create_workflow_with_private_gitlab(
        self,
        client: AsyncClient,
        db: AsyncSession,
        random_user: UserWithAuthCookie,
        mock_s3_service: S3ServiceResource,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for successfully creating a workflow with a private Gitlab repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        mock_s3_service: types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        token = random_lower_string(20)
        workflow = WorkflowIn(
            git_commit_hash=random_hex_string(),
            name=random_lower_string(10),
            short_description=random_lower_string(65),
            repository_url="https://gitlab.de/example-user/example",
            token=token,
            nextflow_version=NextflowVersion.v24_04_4,
            initial_version="v1.0.0",
        )
        response = await client.post(
            self.base_path, content=workflow.model_dump_json(), cookies=random_user.auth_cookie
        )
        assert response.status_code == status.HTTP_201_CREATED
        await db.reset()
        created_workflow = WorkflowOut.model_validate_json(response.content)
        cleanup.add_task(delete_workflow, db=db, workflow_id=created_workflow.workflow_id)
        assert created_workflow.private

        # Check if workflow is created in database
        stmt = select(Workflow).where(Workflow.workflow_id == created_workflow.workflow_id)
        db_workflow = await db.scalar(stmt)
        assert db_workflow is not None
        # Check if token is saved
        assert db_workflow.credentials_token == token

        # Download SCM file in DATA_BUCKET that should be created
        scm_key = settings.s3.scm_key(db_workflow.workflow_id)
        scm_obj = await mock_s3_service.Object(bucket_name=settings.s3.data_bucket, key=scm_key)
        cleanup.add_task(scm_obj.delete)
        with BytesIO() as f:
            await scm_obj.download_fileobj(f)
            f.seek(0)
            scm = SCM.deserialize(f)

        # Check content of SCM file
        assert len(scm.providers) == 1
        provider = scm.providers[0]
        assert provider.password == token
        assert provider.name == SCMProvider.generate_name(db_workflow.workflow_id)
        assert provider.platform == "gitlab"
        assert provider.server == "https://gitlab.de"

    @pytest.mark.asyncio
    async def test_create_workflow_with_private_github(
        self,
        client: AsyncClient,
        db: AsyncSession,
        random_user: UserWithAuthCookie,
        mock_s3_service: S3ServiceResource,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for successfully creating a workflow with a private GitHub repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        token = random_lower_string(20)
        workflow = WorkflowIn(
            git_commit_hash=random_hex_string(),
            name=random_lower_string(10),
            short_description=random_lower_string(65),
            repository_url="https://github.de/example-user/example",
            token=token,
            nextflow_version=NextflowVersion.v24_04_4,
            initial_version="v1.0.0",
        )
        response = await client.post(
            self.base_path, content=workflow.model_dump_json(), cookies=random_user.auth_cookie
        )
        assert response.status_code == status.HTTP_201_CREATED
        await db.reset()
        created_workflow = WorkflowOut.model_validate_json(response.content)
        cleanup.add_task(delete_workflow, db=db, workflow_id=created_workflow.workflow_id)
        assert created_workflow.private

        # Check if workflow is created in database
        stmt = select(Workflow).where(Workflow.workflow_id == created_workflow.workflow_id)
        db_workflow = await db.scalar(stmt)
        assert db_workflow is not None
        # Check if token is saved
        assert db_workflow.credentials_token == token

        # Download SCM file in DATA_BUCKET that should be created
        scm_key = settings.s3.scm_key(db_workflow.workflow_id)
        scm_obj = await mock_s3_service.Object(bucket_name=settings.s3.data_bucket, key=scm_key)
        cleanup.add_task(scm_obj.delete)
        with BytesIO() as f:
            await scm_obj.download_fileobj(f)
            f.seek(0)
            scm = SCM.deserialize(f)

        # Check content of SCM file
        assert len(scm.providers) == 1
        provider = scm.providers[0]
        assert provider.password == token
        assert provider.name == "github"
        assert provider.user == "example-user"

    @pytest.mark.asyncio
    async def test_create_workflow_with_error(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        mock_default_http_server: DefaultMockHTTPService,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for creating a workflow where the file checks don't pass

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        mock_default_http_server : clowm.tests.mocks.DefaultMockHTTPService
            Mock http service for testing
        """
        mock_default_http_server.send_error = True
        cleanup.add_task(mock_default_http_server.reset)
        workflow = WorkflowIn(
            git_commit_hash=random_hex_string(),
            name=random_lower_string(10),
            short_description=random_lower_string(65),
            repository_url="https://github.de/example-user/example",
            nextflow_version=NextflowVersion.v24_04_4,
            initial_version="v1.0.0",
        )
        response = await client.post(
            self.base_path,
            content=workflow.model_dump_json(),
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

        db_workflows = (await db.scalars(select(Workflow))).all()
        assert len(db_workflows) == 0

    @pytest.mark.asyncio
    async def test_create_workflow_with_taken_name(
        self, client: AsyncClient, random_user: UserWithAuthCookie, random_workflow: WorkflowOut
    ) -> None:
        """
        Test for creating a workflow with an already taken name.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random bucket for testing.
        """
        workflow = WorkflowIn(
            git_commit_hash=random_hex_string(),
            name=random_workflow.name,
            short_description=random_lower_string(65),
            repository_url="https://github.de/example-user/example",
            nextflow_version=NextflowVersion.v24_04_4,
            initial_version="v1.0.0",
        )
        response = await client.post(
            self.base_path, content=workflow.model_dump_json(), cookies=random_user.auth_cookie
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_create_workflow_with_registered_git_commit(
        self, client: AsyncClient, random_user: UserWithAuthCookie, random_workflow: WorkflowOut
    ) -> None:
        """
        Test for creating a workflow where the git commit is already in the system.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        workflow = WorkflowIn(
            git_commit_hash=random_workflow.versions[0].workflow_version_id,
            name=random_lower_string(10),
            short_description=random_lower_string(65),
            repository_url="https://github.de/example-user/example",
            nextflow_version=NextflowVersion.v24_04_4,
            initial_version="v1.0.0",
        )
        response = await client.post(
            self.base_path, content=workflow.model_dump_json(), cookies=random_user.auth_cookie
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_create_workflow_with_unknown_git_repository(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for creating a workflow with an unknown git repository.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        workflow = WorkflowIn(
            git_commit_hash=random_hex_string(),
            name=random_lower_string(10),
            short_description=random_lower_string(65),
            repository_url="https://example.org",
            nextflow_version=NextflowVersion.v24_04_4,
            initial_version="v1.0.0",
        )
        response = await client.post(
            self.base_path, content=workflow.model_dump_json(), cookies=random_user.auth_cookie
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_create_workflow_with_workflow_mode(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        mock_s3_service: S3ServiceResource,
        cleanup: CleanupList,
    ) -> None:
        """
        Exhaustive Test for successfully creating a workflow with a workflow mode.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        git_commit_hash = random_hex_string()
        workflow_mode = WorkflowModeIn(
            name=random_lower_string(), entrypoint=random_lower_string(), schema_path="alternative.json"
        )
        workflow = WorkflowIn(
            git_commit_hash=git_commit_hash,
            name=random_lower_string(10),
            short_description=random_lower_string(65),
            repository_url="https://github.de/example-user/example",
            modes=[workflow_mode, workflow_mode],
            nextflow_version=NextflowVersion.v24_04_4,
            initial_version="v1.0.0",
        )
        response = await client.post(
            self.base_path, content=workflow.model_dump_json(), cookies=random_user.auth_cookie
        )
        assert response.status_code == status.HTTP_201_CREATED
        await db.reset()
        created_workflow = WorkflowOut.model_validate_json(response.content)
        cleanup.add_task(delete_workflow, db=db, workflow_id=created_workflow.workflow_id)
        assert len(created_workflow.versions[0].modes) == 2
        mode_id = created_workflow.versions[0].modes[0]
        cleanup.add_task(delete_workflow_mode, db=db, mode_id=mode_id)

        stmt = select(WorkflowMode).where(WorkflowMode.mode_id == mode_id)
        db_mode = await db.scalar(stmt)
        assert db_mode is not None
        assert db_mode.name == workflow_mode.name
        assert db_mode.entrypoint == workflow_mode.entrypoint
        assert db_mode.schema_path == workflow_mode.schema_path

        schema_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket,
            key=settings.s3.workflow_cache_key(
                name=DocumentationEnum.PARAMETER_SCHEMA,
                git_commit_hash=workflow.git_commit_hash,
                mode_id=db_mode.mode_id,
            ),
        )
        await schema_obj.load()

    @pytest.mark.asyncio
    async def test_create_workflow_otr(
        self,
        client: AsyncClient,
        random_workflow: WorkflowOut,
        random_user: UserWithAuthCookie,
        random_second_user: UserWithAuthCookie,
        db: AsyncSession,
    ) -> None:
        """
        Test for creating a workflow OTR.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is the owner of the workflow.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is not the owner of the workflow.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        # Test Fail: User is not allowed to create a OTR
        response1 = await client.post(
            f"{self.base_path}/{random_workflow.workflow_id}/ownership_transfer_request",
            cookies=random_second_user.auth_cookie,
            content=OwnershipTransferRequestIn(new_owner_uid=random_second_user.uid).model_dump_json(),
        )

        assert response1.status_code == status.HTTP_403_FORBIDDEN

        # Test Fail: New owner does not exist
        response2 = await client.post(
            f"{self.base_path}/{random_workflow.workflow_id}/ownership_transfer_request",
            cookies=random_user.auth_cookie,
            content=OwnershipTransferRequestIn(new_owner_uid=uuid4()).model_dump_json(),
        )

        assert response2.status_code == status.HTTP_400_BAD_REQUEST

        # Test Fail: New and current owner are the same
        response3 = await client.post(
            f"{self.base_path}/{random_workflow.workflow_id}/ownership_transfer_request",
            cookies=random_user.auth_cookie,
            content=OwnershipTransferRequestIn(new_owner_uid=random_workflow.developer_id).model_dump_json(),
        )

        assert response3.status_code == status.HTTP_400_BAD_REQUEST

        # Test success
        otr_in = OwnershipTransferRequestIn(new_owner_uid=random_second_user.uid, comment=random_lower_string(64))
        response4 = await client.post(
            f"{self.base_path}/{random_workflow.workflow_id}/ownership_transfer_request",
            cookies=random_user.auth_cookie,
            content=otr_in.model_dump_json(),
        )

        assert response4.status_code == status.HTTP_201_CREATED
        otr_out = OwnershipTransferRequestOut.model_validate_json(response4.content)

        assert otr_out.new_owner_uid == otr_in.new_owner_uid
        assert otr_out.comment == otr_in.comment
        assert otr_out.target_name == random_workflow.name
        assert otr_out.target_description == random_workflow.short_description
        assert otr_out.target_id == random_workflow.workflow_id
        assert otr_out.target_type == OwnershipTypeEnum.WORKFLOW

        await db.reset()
        workflow_db: Workflow | None = await db.scalar(
            select(Workflow).where(Workflow.workflow_id == random_workflow.workflow_id)
        )
        assert workflow_db is not None

        assert workflow_db.transfer_created_at is not None
        assert workflow_db.transfer_comment == otr_in.comment
        assert workflow_db.transfer_new_owner_uid == random_second_user.uid


class TestWorkflowRoutesList(_TestWorkflowRoutes):
    @pytest.mark.asyncio
    async def test_list_all_unpublished_workflows(
        self, client: AsyncClient, random_user: UserWithAuthCookie, random_workflow: WorkflowOut
    ) -> None:
        """
        Test for listing all workflows in the system.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.get(
            self.base_path,
            cookies=random_user.auth_cookie,
            params={"version_status": WorkflowVersion.WorkflowVersionStatus.CREATED.name},
        )
        assert response.status_code == status.HTTP_200_OK
        workflows = WorkflowList.validate_json(response.content)
        assert len(workflows) == 1
        assert workflows[0].workflow_id == random_workflow.workflow_id
        assert not workflows[0].private

    @pytest.mark.asyncio
    async def test_list_all_workflows(
        self, client: AsyncClient, random_user: UserWithAuthCookie, random_workflow: WorkflowOut
    ) -> None:
        """
        Test for listing all workflows in the system.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.get(
            self.base_path,
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_200_OK
        workflows = WorkflowList.validate_json(response.content)
        assert len(workflows) == 0

    @pytest.mark.asyncio
    async def test_list_workflows_by_developer(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow: WorkflowOut,
    ) -> None:
        """
        Test for listing all workflows in the system.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.get(
            self.base_path,
            cookies=random_user.auth_cookie,
            params={
                "developer_id": str(random_user.uid),
                "version_status": WorkflowVersion.WorkflowVersionStatus.CREATED.name,
            },
        )
        assert response.status_code == status.HTTP_200_OK
        workflows = WorkflowList.validate_json(response.content)
        assert len(workflows) == 1
        assert workflows[0].workflow_id == random_workflow.workflow_id
        assert workflows[0].developer_id == random_user.uid

    @pytest.mark.asyncio
    async def test_list_workflow_statistics(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow: WorkflowOut,
        random_completed_workflow_execution: WorkflowExecution,
    ) -> None:
        """
        Test for getting all workflow executions as workflow statistics.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_completed_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing
        """
        response = await client.get(
            f"{self.base_path}/developer_statistics",
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_200_OK
        ta = TypeAdapter(list[AnonymizedWorkflowExecution])
        executions = ta.validate_json(response.content)
        assert len(executions) == 1
        execution = executions[0]
        assert execution.workflow_id == random_workflow.workflow_id
        assert execution.workflow_execution_id == random_completed_workflow_execution.execution_id
        assert execution.workflow_version_id == random_completed_workflow_execution.workflow_version_id

    @pytest.mark.asyncio
    async def test_list_workflow_otr(
        self,
        client: AsyncClient,
        random_workflow: WorkflowOut,
        random_user: UserWithAuthCookie,
        random_second_user: UserWithAuthCookie,
        db: AsyncSession,
    ) -> None:
        """
        Test for listing the resource OTRs.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is not the owner of the workflow.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        ta = TypeAdapter(list[OwnershipTransferRequestOut])

        response1 = await client.get(f"{self.base_path}/ownership_transfer_request", cookies=random_user.auth_cookie)
        assert response1.status_code == status.HTTP_200_OK
        assert len(ta.validate_json(response1.content)) == 0

        await create_otr(
            target_id=random_workflow.workflow_id, new_owner_id=random_second_user.uid, db=db, otr_type=Workflow
        )

        response2 = await client.get(f"{self.base_path}/ownership_transfer_request", cookies=random_user.auth_cookie)
        assert response2.status_code == status.HTTP_200_OK

        otrs = ta.validate_json(response2.content)
        assert len(otrs) == 1

        otr = otrs[0]
        assert otr.target_id == random_workflow.workflow_id
        assert otr.comment == ""
        assert otr.new_owner_uid == random_second_user.uid
        assert otr.target_name == random_workflow.name
        assert otr.target_description == random_workflow.short_description
        assert otr.target_type == OwnershipTypeEnum.WORKFLOW


class TestWorkflowRoutesGet(_TestWorkflowRoutes):
    @pytest.mark.asyncio
    async def test_get_workflow_with_version_status(
        self, client: AsyncClient, random_user: UserWithAuthCookie, random_workflow: WorkflowOut
    ) -> None:
        """
        Test for getting a workflow by its id.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.get(
            "/".join([self.base_path, str(random_workflow.workflow_id)]),
            params={"version_status": random_workflow.versions[0].status.name},
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_200_OK
        workflow = WorkflowOut.model_validate_json(response.content)
        assert workflow.workflow_id == random_workflow.workflow_id
        assert not workflow.private
        assert len(workflow.versions) > 0

    @pytest.mark.asyncio
    async def test_get_non_existing_workflow(self, client: AsyncClient, random_user: UserWithAuthCookie) -> None:
        """
        Test for getting a non-existing workflow.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        response = await client.get(
            "/".join([self.base_path, str(uuid4())]),
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_get_workflow_statistics(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow: WorkflowOut,
        random_running_workflow_execution: WorkflowExecution,
    ) -> None:
        """
        Test for getting the aggregated workflow statistics.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing
        """
        response = await client.get(
            "/".join([self.base_path, str(random_workflow.workflow_id), "statistics"]),
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_200_OK
        ta = TypeAdapter(list[WorkflowStatistic])
        statistics = ta.validate_json(response.content)
        assert len(statistics) == 1
        stat = statistics[0]
        assert stat.day == date.today()
        assert stat.count == 1

    @pytest.mark.asyncio
    async def test_get_workflow_otr(
        self,
        client: AsyncClient,
        random_workflow: WorkflowOut,
        random_user: UserWithAuthCookie,
        random_second_user: UserWithAuthCookie,
        db: AsyncSession,
    ) -> None:
        """
        Test for getting a OTR of a workflow.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is the owner of the workflow.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is not the owner of the workflow.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        response1 = await client.get(
            f"{self.base_path}/{random_workflow.workflow_id}/ownership_transfer_request",
            cookies=random_user.auth_cookie,
        )
        assert response1.status_code == status.HTTP_404_NOT_FOUND

        response2 = await client.get(
            f"{self.base_path}/{random_workflow.workflow_id}/ownership_transfer_request",
            cookies=random_second_user.auth_cookie,
        )
        assert response2.status_code == status.HTTP_403_FORBIDDEN

        comment = random_lower_string(64)
        await create_otr(
            target_id=random_workflow.workflow_id,
            new_owner_id=random_second_user.uid,
            comment=comment,
            db=db,
            otr_type=Workflow,
        )

        response3 = await client.get(
            f"{self.base_path}/{random_workflow.workflow_id}/ownership_transfer_request",
            cookies=random_user.auth_cookie,
        )
        assert response3.status_code == status.HTTP_200_OK

        otr1 = OwnershipTransferRequestOut.model_validate_json(response3.content)

        assert otr1.target_id == random_workflow.workflow_id
        assert otr1.comment == comment
        assert otr1.new_owner_uid == random_second_user.uid
        assert otr1.target_name == random_workflow.name
        assert otr1.target_description == random_workflow.short_description
        assert otr1.target_type == OwnershipTypeEnum.WORKFLOW

        response4 = await client.get(
            f"{self.base_path}/{random_workflow.workflow_id}/ownership_transfer_request",
            cookies=random_second_user.auth_cookie,
        )
        assert response4.status_code == status.HTTP_200_OK
        otr2 = OwnershipTransferRequestOut.model_validate_json(response4.content)
        assert otr1 == otr2


class TestWorkflowRoutesDelete(_TestWorkflowRoutes):
    @pytest.mark.asyncio
    async def test_delete_workflow(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow: WorkflowOut,
        mock_s3_service: S3ServiceResource,
    ) -> None:
        """
        Test for deleting a workflow by its id.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        """
        response = await client.delete(
            "/".join([self.base_path, str(random_workflow.workflow_id)]),
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT
        icon_slug = str(random_workflow.versions[0].icon_url).split("/")[-1]
        with pytest.raises(ClientError):
            icon_obj = await mock_s3_service.Object(
                bucket_name=settings.s3.data_bucket, key=settings.s3.icon_key(icon_slug)
            )
            await icon_obj.load()
        with pytest.raises(ClientError):
            schema_obj = await mock_s3_service.Object(
                bucket_name=settings.s3.data_bucket,
                key=settings.s3.workflow_cache_key(
                    name=DocumentationEnum.PARAMETER_SCHEMA,
                    git_commit_hash=random_workflow.versions[0].workflow_version_id,
                ),
            )
            await schema_obj.load()

    @pytest.mark.asyncio
    async def test_delete_private_workflow(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_private_workflow: WorkflowOut,
        mock_s3_service: S3ServiceResource,
    ) -> None:
        """
        Test for deleting a workflow by its id.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_private_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        """
        response = await client.delete(
            "/".join([self.base_path, str(random_private_workflow.workflow_id)]),
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT
        icon_slug = str(random_private_workflow.versions[0].icon_url).split("/")[-1]
        with pytest.raises(ClientError):
            icon_obj = await mock_s3_service.Object(
                bucket_name=settings.s3.data_bucket, key=settings.s3.icon_key(icon_slug)
            )
            await icon_obj.load()
        with pytest.raises(ClientError):
            schema_obj = await mock_s3_service.Object(
                bucket_name=settings.s3.data_bucket,
                key=settings.s3.workflow_cache_key(
                    name=DocumentationEnum.PARAMETER_SCHEMA,
                    git_commit_hash=random_private_workflow.versions[0].workflow_version_id,
                ),
            )
            await schema_obj.load()
        with pytest.raises(ClientError):
            scm_obj = await mock_s3_service.Object(
                bucket_name=settings.s3.data_bucket, key=settings.s3.scm_key(random_private_workflow.workflow_id)
            )
            await scm_obj.load()

    @pytest.mark.asyncio
    async def test_delete_workflow_with_mode(
        self,
        client: AsyncClient,
        db: AsyncSession,
        random_user: UserWithAuthCookie,
        random_workflow: WorkflowOut,
        random_workflow_mode: WorkflowMode,
        mock_s3_service: S3ServiceResource,
    ) -> None:
        """
        Test for deleting a workflow by its id.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        random_workflow_mode : clowm.model.WorkflowMode
            Random workflow mode for testing
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        response = await client.delete(
            "/".join([self.base_path, str(random_workflow.workflow_id)]),
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT
        icon_slug = str(random_workflow.versions[0].icon_url).split("/")[-1]
        with pytest.raises(ClientError):
            icon_obj = await mock_s3_service.Object(
                bucket_name=settings.s3.data_bucket, key=settings.s3.icon_key(icon_slug)
            )
            await icon_obj.load()
        with pytest.raises(ClientError):
            schema_obj = await mock_s3_service.Object(
                bucket_name=settings.s3.data_bucket,
                key=settings.s3.workflow_cache_key(
                    name=DocumentationEnum.PARAMETER_SCHEMA,
                    git_commit_hash=random_workflow.versions[0].workflow_version_id,
                    mode_id=random_workflow_mode.mode_id,
                ),
            )
            await schema_obj.load()

        mode_db = await db.scalar(select(WorkflowMode).where(WorkflowMode.mode_id == random_workflow_mode.mode_id))
        assert mode_db is None

    @pytest.mark.asyncio
    async def test_delete_workflow_otr(
        self,
        client: AsyncClient,
        random_workflow: WorkflowOut,
        random_user: UserWithAuthCookie,
        random_second_user: UserWithAuthCookie,
        db: AsyncSession,
    ) -> None:
        """
        Test for rejecting a workflow OTR.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is the owner of the workflow.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is not the owner of the workflow.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        # Test Fail: OTR does not exist
        response1 = await client.delete(
            f"{self.base_path}/{random_workflow.workflow_id}/ownership_transfer_request",
            cookies=random_user.auth_cookie,
        )
        assert response1.status_code == status.HTTP_404_NOT_FOUND

        await create_otr(
            target_id=random_workflow.workflow_id, new_owner_id=random_second_user.uid, db=db, otr_type=Workflow
        )

        # Test Success
        response2 = await client.delete(
            f"{self.base_path}/{random_workflow.workflow_id}/ownership_transfer_request",
            cookies=random_user.auth_cookie,
        )
        assert response2.status_code == status.HTTP_204_NO_CONTENT

        await db.reset()
        workflow_db: Workflow | None = await db.scalar(
            select(Workflow).where(Workflow.workflow_id == random_workflow.workflow_id)
        )
        assert workflow_db is not None

        assert workflow_db.transfer_created_at is None
        assert workflow_db.transfer_comment is None
        assert workflow_db.transfer_new_owner_uid is None
        assert workflow_db.developer_id == random_user.uid

    @pytest.mark.asyncio
    async def test_reject_workflow_otr(
        self,
        client: AsyncClient,
        random_workflow: WorkflowOut,
        random_second_user: UserWithAuthCookie,
        db: AsyncSession,
    ) -> None:
        """
        Test for rejecting a workflow OTR.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is not the owner of the workflow.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        await create_otr(
            target_id=random_workflow.workflow_id, new_owner_id=random_second_user.uid, db=db, otr_type=Workflow
        )

        response = await client.delete(
            f"{self.base_path}/{random_workflow.workflow_id}/ownership_transfer_request",
            cookies=random_second_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT

        await db.reset()
        workflow_db: Workflow | None = await db.scalar(
            select(Workflow).where(Workflow.workflow_id == random_workflow.workflow_id)
        )
        assert workflow_db is not None

        assert workflow_db.transfer_created_at is None
        assert workflow_db.transfer_comment is None
        assert workflow_db.transfer_new_owner_uid is None
        assert workflow_db.developer_id != random_second_user.uid


class TestWorkflowRoutesUpdate(_TestWorkflowRoutes):
    @pytest.mark.asyncio
    async def test_update_workflow(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow: WorkflowOut,
        mock_s3_service: S3ServiceResource,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for successfully updating a workflow.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        config_value = random_lower_string(128)
        await db.execute(
            update(WorkflowVersion)
            .where(WorkflowVersion.git_commit_hash == random_workflow.versions[0].workflow_version_id)
            .values(nextflow_config=config_value)
        )
        await db.commit()

        config_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket,
            key=settings.s3.config_key(random_workflow.versions[0].workflow_version_id),
        )
        with BytesIO(config_value.encode("utf-8")) as f:
            await config_obj.upload_fileobj(f)
        cleanup.add_task(config_obj.delete)

        git_commit_hash = random_hex_string()
        version_update = WorkflowUpdate(
            git_commit_hash=git_commit_hash,
            version=random_lower_string(8),
            nextflow_version=NextflowVersion.v24_04_4,
        )
        response = await client.post(
            "/".join([self.base_path, str(random_workflow.workflow_id), "update"]),
            content=version_update.model_dump_json(),
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_201_CREATED
        new_config_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket, key=settings.s3.config_key(git_commit_hash)
        )
        cleanup.add_task(new_config_obj.delete)

        created_version = WorkflowVersionOut.model_validate_json(response.content)
        assert created_version.workflow_version_id == git_commit_hash
        assert created_version.status == WorkflowVersion.WorkflowVersionStatus.CREATED
        assert created_version.icon_url == random_workflow.versions[0].icon_url

        stmt = select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == git_commit_hash)
        db_version = await db.scalar(stmt)
        assert db_version is not None
        assert db_version.status == WorkflowVersion.WorkflowVersionStatus.CREATED

        with BytesIO() as f:
            await new_config_obj.download_fileobj(f)
            f.seek(0)
            assert f.read().decode("utf-8") == config_value

    @pytest.mark.asyncio
    async def test_update_workflow_with_append_modes(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow: WorkflowOut,
        random_workflow_mode: WorkflowMode,
        mock_s3_service: S3ServiceResource,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for successfully updating a workflow and adding new modes.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_workflow_mode : clowm.model.WorkflowMode
            Random workflow mode for testing
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        git_commit_hash = random_hex_string()
        version_update = WorkflowUpdate(
            git_commit_hash=git_commit_hash,
            version=random_lower_string(8),
            append_modes=[
                WorkflowModeIn(
                    name=random_lower_string(10), entrypoint=random_lower_string(16), schema_path="alternative.json"
                )
            ],
            nextflow_version=NextflowVersion.v24_04_4,
        )
        response = await client.post(
            "/".join([self.base_path, str(random_workflow.workflow_id), "update"]),
            content=version_update.model_dump_json(),
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_201_CREATED
        created_version = WorkflowVersionOut.model_validate_json(response.content)
        assert created_version.workflow_version_id == git_commit_hash
        assert created_version.status == WorkflowVersion.WorkflowVersionStatus.CREATED
        assert created_version.icon_url == random_workflow.versions[0].icon_url
        assert created_version.modes is not None
        assert len(created_version.modes) == 2

        stmt = select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == git_commit_hash)
        db_version = await db.scalar(stmt)
        assert db_version is not None
        assert db_version.status == WorkflowVersion.WorkflowVersionStatus.CREATED

        new_mode_id = next((m for m in created_version.modes if m != random_workflow_mode.mode_id), None)
        assert new_mode_id is not None
        cleanup.add_task(delete_workflow_mode, db=db, mode_id=new_mode_id)
        schema_obj1 = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket,
            key=settings.s3.workflow_cache_key(
                name=DocumentationEnum.PARAMETER_SCHEMA, git_commit_hash=git_commit_hash, mode_id=new_mode_id
            ),
        )
        await schema_obj1.load()
        cleanup.add_task(
            schema_obj1.delete,
        )

        schema_obj2 = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket,
            key=settings.s3.workflow_cache_key(
                name=DocumentationEnum.PARAMETER_SCHEMA,
                git_commit_hash=git_commit_hash,
                mode_id=random_workflow_mode.mode_id,
            ),
        )
        await schema_obj2.load()

    @pytest.mark.asyncio
    async def test_update_workflow_with_delete_non_existing_modes(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow: WorkflowOut,
    ) -> None:
        """
        Test for updating a workflow and delete an non-existing mode.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        version_update = WorkflowUpdate(
            git_commit_hash=random_hex_string(),
            version=random_lower_string(8),
            delete_modes=[str(uuid4())],
            nextflow_version=NextflowVersion.v24_04_4,
        )
        response = await client.post(
            "/".join([self.base_path, str(random_workflow.workflow_id), "update"]),
            content=version_update.model_dump_json(),
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_update_workflow_with_delete_modes(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow: WorkflowOut,
        random_workflow_mode: WorkflowMode,
        mock_s3_service: S3ServiceResource,
    ) -> None:
        """
        Test for successfully updating a workflow and delete an old mode.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_workflow_mode : clowm.model.WorkflowMode
            Random workflow mode for testing
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        """
        git_commit_hash = random_hex_string()
        version_update = WorkflowUpdate(
            git_commit_hash=git_commit_hash,
            version=random_lower_string(8),
            delete_modes=[random_workflow_mode.mode_id],
            nextflow_version=NextflowVersion.v24_04_4,
        )
        response = await client.post(
            "/".join([self.base_path, str(random_workflow.workflow_id), "update"]),
            content=version_update.model_dump_json(),
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_201_CREATED
        created_version = WorkflowVersionOut.model_validate_json(response.content)
        assert created_version.workflow_version_id == git_commit_hash
        assert created_version.status == WorkflowVersion.WorkflowVersionStatus.CREATED
        assert created_version.icon_url == random_workflow.versions[0].icon_url
        assert created_version.modes is None or len(created_version.modes) == 0

        stmt = select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == git_commit_hash)
        db_version = await db.scalar(stmt)
        assert db_version is not None
        assert db_version.status == WorkflowVersion.WorkflowVersionStatus.CREATED

        with pytest.raises(ClientError):
            schema_obj = await mock_s3_service.Object(
                bucket_name=settings.s3.data_bucket,
                key=settings.s3.workflow_cache_key(
                    name=DocumentationEnum.PARAMETER_SCHEMA,
                    git_commit_hash=git_commit_hash,
                    mode_id=random_workflow_mode.mode_id,
                ),
            )
            await schema_obj.load()

    @pytest.mark.asyncio
    async def test_update_workflow_with_append_and_delete_modes(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow: WorkflowOut,
        random_workflow_mode: WorkflowMode,
        mock_s3_service: S3ServiceResource,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for successfully updating a workflow with adding a new mode and delete an old mode.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_workflow_mode : clowm.model.WorkflowMode
            Random workflow mode for testing
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        git_commit_hash = random_hex_string()
        version_update = WorkflowUpdate(
            git_commit_hash=git_commit_hash,
            version=random_lower_string(8),
            delete_modes=[str(random_workflow_mode.mode_id)],
            append_modes=[
                WorkflowModeIn(
                    name=random_lower_string(10), entrypoint=random_lower_string(16), schema_path="alternative.json"
                )
            ],
            nextflow_version=NextflowVersion.v24_04_4,
        )
        response = await client.post(
            "/".join([self.base_path, str(random_workflow.workflow_id), "update"]),
            content=version_update.model_dump_json(),
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_201_CREATED
        created_version = WorkflowVersionOut.model_validate_json(response.content)
        assert created_version.workflow_version_id == git_commit_hash
        assert created_version.status == WorkflowVersion.WorkflowVersionStatus.CREATED
        assert created_version.icon_url == random_workflow.versions[0].icon_url
        assert len(created_version.modes) == 1
        assert created_version.modes[0] != random_workflow_mode.mode_id

        stmt = select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == git_commit_hash)
        db_version = await db.scalar(stmt)
        assert db_version is not None
        assert db_version.status == WorkflowVersion.WorkflowVersionStatus.CREATED

        mode_id = created_version.modes[0]
        cleanup.add_task(delete_workflow_mode, db=db, mode_id=mode_id)
        schema_obj1 = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket,
            key=settings.s3.workflow_cache_key(
                name=DocumentationEnum.PARAMETER_SCHEMA, git_commit_hash=git_commit_hash, mode_id=mode_id
            ),
        )
        await schema_obj1.load()
        cleanup.add_task(
            schema_obj1.delete,
        )
        with pytest.raises(ClientError):
            schema_obj2 = await mock_s3_service.Object(
                bucket_name=settings.s3.data_bucket,
                key=settings.s3.workflow_cache_key(
                    name=DocumentationEnum.PARAMETER_SCHEMA,
                    git_commit_hash=git_commit_hash,
                    mode_id=random_workflow_mode.mode_id,
                ),
            )
            await schema_obj2.load()

    @pytest.mark.asyncio
    async def test_update_workflow_with_error(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow: WorkflowOut,
        mock_default_http_server: DefaultMockHTTPService,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for updating a workflow where the file checks don't pass

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        mock_default_http_server : clowm.tests.mocks.DefaultMockHTTPService
            Mock http service for testing
        """
        mock_default_http_server.send_error = True
        cleanup.add_task(mock_default_http_server.reset)
        version_update = WorkflowUpdate(
            git_commit_hash=random_hex_string(),
            version=random_lower_string(8),
            nextflow_version=NextflowVersion.v24_04_4,
        )
        response = await client.post(
            "/".join([self.base_path, str(random_workflow.workflow_id), "update"]),
            content=version_update.model_dump_json(),
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

        db_workflows = (
            await db.scalars(select(WorkflowVersion).where(WorkflowVersion.workflow_id == random_workflow.workflow_id))
        ).all()
        assert len(db_workflows) == 1

    @pytest.mark.asyncio
    async def test_update_workflow_with_registered_git_commit(
        self, client: AsyncClient, random_user: UserWithAuthCookie, random_workflow: WorkflowOut
    ) -> None:
        """
        Test for updating a workflow where the git commit is already in the system.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        version_update = WorkflowUpdate(
            git_commit_hash=random_workflow.versions[0].workflow_version_id,
            version=random_lower_string(8),
            nextflow_version=NextflowVersion.v24_04_4,
        )
        response = await client.post(
            "/".join([self.base_path, str(random_workflow.workflow_id), "update"]),
            content=version_update.model_dump_json(),
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_update_workflow_as_foreign_user(
        self, client: AsyncClient, random_second_user: UserWithAuthCookie, random_workflow: WorkflowOut
    ) -> None:
        """
        Test for updating a workflow where the requested user is not the developer of the workflow.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        version_update = WorkflowUpdate(
            git_commit_hash=random_hex_string(),
            version=random_lower_string(8),
            nextflow_version=NextflowVersion.v24_04_4,
        )
        response = await client.post(
            "/".join([self.base_path, str(random_workflow.workflow_id), "update"]),
            content=version_update.model_dump_json(),
            cookies=random_second_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.asyncio
    async def test_accept_workflow_otr(
        self,
        client: AsyncClient,
        random_workflow: WorkflowOut,
        random_user: UserWithAuthCookie,
        random_second_user: UserWithAuthCookie,
        db: AsyncSession,
    ) -> None:
        """
        Test for accepting a workflow OTR.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is owner of the bucket.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is not the owner of the bucket.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        response1 = await client.patch(
            f"{self.base_path}/{random_workflow.workflow_id}/ownership_transfer_request",
            cookies=random_user.auth_cookie,
        )
        assert response1.status_code == status.HTTP_404_NOT_FOUND

        await create_otr(
            target_id=random_workflow.workflow_id, new_owner_id=random_second_user.uid, db=db, otr_type=Workflow
        )

        # Test Success
        response2 = await client.patch(
            f"{self.base_path}/{random_workflow.workflow_id}/ownership_transfer_request",
            cookies=random_second_user.auth_cookie,
        )
        assert response2.status_code == status.HTTP_200_OK

        await db.reset()
        workflow_db: Workflow | None = await db.scalar(
            select(Workflow).where(Workflow.workflow_id == random_workflow.workflow_id)
        )
        assert workflow_db is not None

        assert workflow_db.transfer_created_at is None
        assert workflow_db.transfer_comment is None
        assert workflow_db.transfer_new_owner_uid is None
        assert workflow_db.developer_id == random_second_user.uid
        assert workflow_db.last_transfer_timestamp is not None
