import pytest
from fastapi import status
from httpx import AsyncClient
from pydantic import TypeAdapter
from sqlalchemy import update
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.models import WorkflowVersion
from clowm.schemas.workflow import PublicWorkflowOut, WorkflowOut
from clowm.tests.utils import UserWithAuthCookie

WorkflowList = TypeAdapter(list[PublicWorkflowOut])


class _TestPublicRoutes:
    base_path: str = "/ui/public"


class TestPublicWorkflowRoutes(_TestPublicRoutes):
    @pytest.mark.asyncio
    async def test_list_public_workflows(
        self,
        client: AsyncClient,
        random_workflow: WorkflowOut,
        random_workflow_version: WorkflowVersion,
        random_user: UserWithAuthCookie,
        db: AsyncSession,
    ) -> None:
        """
        Test for creating a new news event.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        await db.execute(
            update(WorkflowVersion)
            .where(WorkflowVersion.git_commit_hash == random_workflow_version.git_commit_hash)
            .values(status=WorkflowVersion.WorkflowVersionStatus.PUBLISHED)
        )
        await db.commit()

        response = await client.get(self.base_path + "/workflows")
        assert response.status_code == status.HTTP_200_OK
        workflows = WorkflowList.validate_json(response.content)

        assert len(workflows) == 1
        workflow = workflows[0]
        assert workflow.name == random_workflow.name
        assert workflow.workflow_id == random_workflow.workflow_id
        assert workflow.short_description == random_workflow.short_description
        assert workflow.repository_url == random_workflow.repository_url
        assert workflow.latest_version_timestamp == random_workflow_version.created_at
        assert workflow.latest_version_tag == random_workflow_version.version
        assert workflow.latest_git_commit_hash == random_workflow_version.git_commit_hash
        assert workflow.developer == random_user.user.display_name

        assert "max-age=" in response.headers["Cache-Control"]
