from datetime import datetime
from uuid import uuid4

import pytest
from fastapi import status
from httpx import AsyncClient
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.core.config import settings
from clowm.models import WorkflowExecution
from clowm.schemas.tower import CreateWorkflowOut
from clowm.tests.utils import random_hex_string, random_lower_string, trace
from clowm.tests.utils.trace import security_header


class _TestTraceRoutes:
    base_path: str = "/trace"


class TestTraceRoutesCreate(_TestTraceRoutes):
    @pytest.mark.asyncio
    async def test_create_workflow_trace(
        self, client: AsyncClient, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for sending the create workflow event.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on. pytest fixture.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing. pytest fixture.
        """
        body = {
            "sessionId": str(uuid4()),
            "projectName": random_lower_string(),
            "repository": "https://github.com/example/example",
            "runName": random_lower_string(),
            "instant": round(datetime.now().timestamp()),
            "workflowId": None,
        }
        response = await client.post(
            f"{self.base_path}/create",
            params={"workspaceId": str(random_running_workflow_execution.execution_id.hex[:16])},
            json=body,
            headers=security_header,
        )
        assert response.status_code == status.HTTP_200_OK

        r = CreateWorkflowOut.model_validate_json(response.content)
        assert r.workflowId == random_running_workflow_execution.execution_id.hex[:16]

    @pytest.mark.asyncio
    async def test_create_workflow_trace_with_unknown_execution(self, client: AsyncClient) -> None:
        """
        Test for sending the create workflow event where the execution id does not exist.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on. pytest fixture.
        """
        body = {
            "sessionId": str(uuid4()),
            "projectName": random_lower_string(),
            "repository": "https://github.com/example/example",
            "runName": random_lower_string(),
            "instant": round(datetime.now().timestamp()),
            "workflowId": None,
        }
        response = await client.post(
            f"{self.base_path}/create",
            params={"workspaceId": random_hex_string(16)},
            json=body,
            headers=security_header,
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND


class TestTraceRoutesBegin(_TestTraceRoutes):
    @pytest.mark.asyncio
    async def test_begin_workflow(
        self, client: AsyncClient, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for sending the begin workflow event.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on. pytest fixture.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing. pytest fixture.
        """
        response = await client.put(
            f"{self.base_path}/{random_running_workflow_execution.execution_id.hex[:16]}/begin",
            content=trace.begin_workflow_event.model_dump_json(),
            headers=security_header,
        )
        assert response.status_code == status.HTTP_200_OK

        r = response.json()
        assert r.get("watchUrl", "").startswith(str(settings.ui_uri))

    @pytest.mark.asyncio
    async def test_begin_workflow_with_unknown_execution(self, client: AsyncClient) -> None:
        """
        Test for sending the begin workflow event where the execution id does not exist.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on. pytest fixture.
        """
        response = await client.put(
            f"{self.base_path}/{random_hex_string(16)}/begin",
            content=trace.begin_workflow_event.model_dump_json(),
            headers=security_header,
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND


class TestTraceRoutesProgress(_TestTraceRoutes):
    @pytest.mark.asyncio
    async def test_report_progress_workflow(
        self, client: AsyncClient, random_running_workflow_execution: WorkflowExecution, db: AsyncSession
    ) -> None:
        """
        Test for sending the report progress workflow event.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on. pytest fixture.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing. pytest fixture.
        """
        response = await client.put(
            f"{self.base_path}/{random_running_workflow_execution.execution_id.hex[:16]}/progress",
            content=trace.progress_event.model_dump_json(),
            headers=security_header,
        )
        assert response.status_code == status.HTTP_200_OK
        assert trace.progress_event.tasks is not None
        assert len(trace.progress_event.tasks) > 0

        task = trace.progress_event.tasks[0]
        assert task.realtime is not None

        stmt = select(WorkflowExecution).where(
            WorkflowExecution.execution_id == random_running_workflow_execution.execution_id
        )
        await db.reset()
        db_execution = await db.scalar(stmt)
        assert db_execution is not None
        assert db_execution.cpu_time.seconds == task.realtime * task.cpus / 1000


class TestTraceRoutesHeartbeat(_TestTraceRoutes):
    @pytest.mark.asyncio
    async def test_heartbeat_workflow(
        self, client: AsyncClient, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for sending the heartbeat workflow event.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on. pytest fixture.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing. pytest fixture.
        """
        body = trace.progress_event.model_copy(deep=True, update={"body": None})
        response = await client.put(
            f"{self.base_path}/{random_running_workflow_execution.execution_id.hex[:16]}/heartbeat",
            content=body.model_dump_json(),
            headers=security_header,
        )
        assert response.status_code == status.HTTP_200_OK


class TestTraceRoutesComplete(_TestTraceRoutes):
    @pytest.mark.asyncio
    async def test_complete_workflow_with_success(
        self, client: AsyncClient, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for sending the complete workflow event with success.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on. pytest fixture.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on. pytest fixture.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing. pytest fixture.
        """
        response = await client.put(
            f"{self.base_path}/{random_running_workflow_execution.execution_id.hex[:16]}/complete",
            content=trace.complete_workflow_event.model_dump_json(),
            headers=security_header,
        )
        assert response.status_code == status.HTTP_200_OK
        await db.reset()

        stmt = select(WorkflowExecution).where(
            WorkflowExecution.execution_id == random_running_workflow_execution.execution_id
        )
        execution = await db.scalar(stmt)
        assert execution is not None
        assert execution == random_running_workflow_execution
        assert execution.status == WorkflowExecution.WorkflowExecutionStatus.SUCCESS

    @pytest.mark.asyncio
    async def test_complete_workflow_with_error(
        self, client: AsyncClient, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for sending the complete workflow event with an error.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on. pytest fixture.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on. pytest fixture.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing. pytest fixture.
        """
        body = trace.complete_workflow_event.model_copy(deep=True)
        body.workflow.success = False
        response = await client.put(
            f"{self.base_path}/{random_running_workflow_execution.execution_id.hex[:16]}/complete",
            content=body.model_dump_json(),
            headers=security_header,
        )
        assert response.status_code == status.HTTP_200_OK
        await db.reset()
        stmt = select(WorkflowExecution).where(
            WorkflowExecution.execution_id == random_running_workflow_execution.execution_id
        )
        execution = await db.scalar(stmt)

        assert execution is not None
        assert execution == random_running_workflow_execution
        assert execution.status == WorkflowExecution.WorkflowExecutionStatus.ERROR
        assert execution.end_time is not None

    @pytest.mark.asyncio
    async def test_complete_workflow_with_cancel(
        self, client: AsyncClient, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for sending the complete workflow event with canceled workflow run.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on. pytest fixture.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on. pytest fixture.
        random_running_workflow_execution : clowm.models.WorkflowExecution
            Random workflow execution for testing. pytest fixture.
        """
        body = trace.complete_workflow_event.model_copy(deep=True)
        body.workflow.success = False
        body.workflow.errorReport = "SIGTERM"
        response = await client.put(
            f"{self.base_path}/{random_running_workflow_execution.execution_id.hex[:16]}/complete",
            content=body.model_dump_json(),
            headers=security_header,
        )
        assert response.status_code == status.HTTP_200_OK
        await db.reset()
        stmt = select(WorkflowExecution).where(
            WorkflowExecution.execution_id == random_running_workflow_execution.execution_id
        )
        execution = await db.scalar(stmt)

        assert execution is not None
        assert execution == random_running_workflow_execution
        assert execution.status == WorkflowExecution.WorkflowExecutionStatus.CANCELED
        assert execution.end_time is not None
