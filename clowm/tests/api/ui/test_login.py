import json
import time
import urllib.parse
from base64 import b64decode
from secrets import token_urlsafe
from typing import TYPE_CHECKING, Any
from uuid import UUID

import pytest
from fastapi import status
from httpx import AsyncClient, Cookies
from pydantic import AnyHttpUrl
from sqlalchemy import delete, select, update
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.api.dependencies.website.auth import cookie_jwt_scheme
from clowm.api.exception_handlers import AccountConnectionError, LoginError
from clowm.api.website.login import ACCOUNT_CONNECTION_KEY, INVITATION_UID_KEY, NEXT_PATH_KEY
from clowm.core.auth import decode_token
from clowm.core.config import settings
from clowm.core.oidc import OIDCClient, OIDCProvider, UserInfo
from clowm.models import Bucket, User
from clowm.tests.mocks import MockRGWAdmin
from clowm.tests.utils import CleanupList, UserWithAuthCookie, random_lower_string

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object


def decode_session_cookie(session_cookie: str) -> dict[str, str]:
    """
    Decode the session cookie without validating it.

    Parameters
    ----------
    session_cookie : str
        Cookie to decode.

    Returns
    -------
    session_state : dict[str, str]
        Decoded session state.
    """
    return json.loads(b64decode(session_cookie.split(".")[0]))


def merge_cookies(*args: Cookies) -> Cookies:
    """
    Merge multiple Cookies to one Cookie object.

    Parameters
    ----------
    *args : httpx.Cookies
        Cookies to merge

    Returns
    -------
    httpx.Cookies
        Merged cookies object
    """
    return Cookies({cookie_name: cookie.get(cookie_name) or "" for cookie in args for cookie_name in cookie})


class _TestAuthRoute:
    auth_path = "/auth"


class TestLoginRouteRedirects(_TestAuthRoute):
    @pytest.mark.asyncio
    async def test_login_redirect(self, client: AsyncClient, oidc_provider: OIDCProvider) -> None:
        """
        Test for the query parameter and session cookie on the login redirect route.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        oidc_provider : clowm.core.oidc.OICDProvider
            One possible OIDC provider for the test.
        """
        r = await client.get(
            self.auth_path + f"/login/{oidc_provider}",
            params={"next": "/dashboard"},
            follow_redirects=False,
        )
        assert r.status_code == status.HTTP_302_FOUND
        assert "login_error=" not in r.headers["location"]
        session_cookie = r.cookies.get("session", None)
        assert session_cookie is not None
        decoded_session_cookie = decode_session_cookie(session_cookie)
        assert decoded_session_cookie[NEXT_PATH_KEY] == "/dashboard"

    @pytest.mark.asyncio
    async def test_successful_invitation_redirect(
        self, client: AsyncClient, db: AsyncSession, random_user: UserWithAuthCookie, oidc_provider: OIDCProvider
    ) -> None:
        """
        Test for the query parameter and session cookie on the login redirect route with an invitation token.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.UserWithAuthCookie
            Random user for testing.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        oidc_provider : clowm.core.oidc.OICDProvider
            One possible OIDC provider for the test.
        """
        token = token_urlsafe(32)
        await db.execute(
            update(User)
            .where(User.uid == random_user.uid)
            .values(invitation_token=token, invitation_token_created_at=round(time.time()))
        )
        await db.commit()
        r = await client.get(
            self.auth_path + f"/login/{oidc_provider}",
            params={"invitation_token": token},
            follow_redirects=False,
        )
        assert r.status_code == status.HTTP_302_FOUND
        assert "login_error=" not in r.headers["location"]
        session_cookie = r.cookies.get("session", None)
        assert session_cookie is not None
        decoded_session_cookie = decode_session_cookie(session_cookie)
        assert decoded_session_cookie[INVITATION_UID_KEY] == str(random_user.uid)

    @pytest.mark.asyncio
    async def test_invitation_redirect_with_wrong_token(
        self, client: AsyncClient, db: AsyncSession, random_user: UserWithAuthCookie, oidc_provider: OIDCProvider
    ) -> None:
        """
        Test login route with an unknown invitation token.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.UserWithAuthCookie
            Random user for testing.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        oidc_provider : clowm.core.oidc.OICDProvider
            One possible OIDC provider for the test.
        """
        await db.execute(
            update(User)
            .where(User.uid == random_user.uid)
            .values(invitation_token=token_urlsafe(32), invitation_token_created_at=round(time.time()))
        )
        await db.commit()
        r = await client.get(
            self.auth_path + f"/login/{oidc_provider}",
            params={"invitation_token": token_urlsafe(32)},
            follow_redirects=False,
        )
        assert r.status_code == status.HTTP_302_FOUND
        assert "login_error=" in r.headers["location"]

    @pytest.mark.asyncio
    async def test_invitation_redirect_with_expired_token(
        self, client: AsyncClient, db: AsyncSession, random_user: UserWithAuthCookie, oidc_provider: OIDCProvider
    ) -> None:
        """
        Test login route with an expired invitation token.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.UserWithAuthCookie
            Random user for testing.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        oidc_provider : clowm.core.oidc.OICDProvider
            One possible OIDC provider for the test.
        """
        token = token_urlsafe(32)
        await db.execute(
            update(User)
            .where(User.uid == random_user.uid)
            .values(invitation_token=token, invitation_token_created_at=0)
        )
        await db.commit()
        r = await client.get(
            self.auth_path + f"/login/{oidc_provider}",
            params={"invitation_token": token},
            follow_redirects=False,
        )
        assert r.status_code == status.HTTP_302_FOUND
        assert "login_error=" in r.headers.get("location", "")


class TestAccountConnectionRouteRedirects(_TestAuthRoute):
    @pytest.mark.asyncio
    async def test_account_connection_redirect(
        self,
        client: AsyncClient,
        random_user_with_provider: UserWithAuthCookie,
    ) -> None:
        """
        Test for the session cookie on the account connection redirect route.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user_with_provider : clowm.tests.utils.UserWithAuthCookie
            Random user for testing with different OIDC providers.
        """
        assert len(random_user_with_provider.user.registered_providers) == 1
        missing_provider = next(
            (prov for prov in OIDCProvider if prov != random_user_with_provider.user.registered_providers[0]), None
        )
        r = await client.get(
            self.auth_path + f"/account/connect/{missing_provider}",
            follow_redirects=False,
            cookies=random_user_with_provider.auth_cookie,
        )
        assert r.status_code == status.HTTP_302_FOUND
        redirect_url = AnyHttpUrl(r.headers.get("location"))
        assert next((_ for (key, _) in redirect_url.query_params() if key == "link_error"), None) is None

        session_cookie = r.cookies.get("session", None)
        assert session_cookie is not None
        decoded_session_cookie = decode_session_cookie(session_cookie)
        assert decoded_session_cookie[ACCOUNT_CONNECTION_KEY] == str(random_user_with_provider.uid)

    @pytest.mark.asyncio
    async def test_account_connection_redirect_with_non_empty_aai_account(
        self,
        client: AsyncClient,
        random_user_with_provider: UserWithAuthCookie,
    ) -> None:
        """
        Test for the session cookie on the account connection redirect route where the user is already connected
        to an AAI account.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user_with_provider : clowm.tests.utils.UserWithAuthCookie
            Random user for testing with different OIDC providers.
        """
        assert len(random_user_with_provider.user.registered_providers) == 1

        provider = random_user_with_provider.user.registered_providers[0]
        r = await client.get(
            self.auth_path + f"/account/connect/{provider}",
            follow_redirects=False,
            cookies=random_user_with_provider.auth_cookie,
        )
        assert r.status_code == status.HTTP_302_FOUND
        redirect_url = AnyHttpUrl(r.headers.get("location"))
        assert next((_ for (key, _) in redirect_url.query_params() if key == "link_error"), None) is not None

    @pytest.mark.asyncio
    async def test_unauthenticated_account_connection_redirect(
        self,
        client: AsyncClient,
        oidc_provider: OIDCProvider,
    ) -> None:
        """
        Test for unauthenticated access on the account connection redirect route.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        oidc_provider : clowm.core.oidc.OICDProvider
            One possible OIDC provider for the test.
        """

        r = await client.get(self.auth_path + f"/account/connect/{oidc_provider}", follow_redirects=False)
        assert r.status_code == status.HTTP_403_FORBIDDEN


class TestNormalAuthRouteCallback(_TestAuthRoute):
    @pytest.mark.asyncio
    async def test_account_creation_with_non_existing_user(
        self,
        client: AsyncClient,
        mock_rgw_admin: MockRGWAdmin,
        db: AsyncSession,
        mock_s3_service: S3ServiceResource,
        cleanup: CleanupList,
        monkeypatch: pytest.MonkeyPatch,
        oidc_provider: OIDCProvider,
    ) -> None:
        """
        Test for login callback route with a non-existing user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        mock_rgw_admin : clowm.tests.mocks.mock_rgw_admin.MockRGWAdmin
            Mock RGW admin for Ceph.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        mock_s3_service: types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service Resource for testing.
        oidc_provider : clowm.core.oidc.OICDProvider
            One possible OIDC provider for the test.
        monkeypatch : pytest.MonkeyPatch
            Entrypoint for monkeypatching dependencies.
        """
        provider_id = random_lower_string()
        display_name = f"{random_lower_string(8)} {random_lower_string(8)}"

        async def mock_userinfo(*args: Any, **kwargs: Any) -> UserInfo:
            return UserInfo(
                sub=provider_id,
                name=display_name,
                email=f"{random_lower_string(10)}@example.com",
                provider=oidc_provider,
            )

        monkeypatch.setattr(OIDCClient, "verify_fetch_userinfo", mock_userinfo)

        r = await client.get(
            self.auth_path + f"/login/callback/{oidc_provider}",
            follow_redirects=False,
        )
        # Check response and valid/right jwt token
        assert r.status_code == status.HTTP_302_FOUND
        assert "login_error" not in r.headers["location"]
        assert "first_login" in r.headers["location"]
        jwt = r.cookies.get("clowm-jwt", None)
        assert jwt is not None
        claim = decode_token(jwt)
        uid = UUID(claim["sub"])

        async def cleanup_db() -> None:
            await db.execute(delete(Bucket).where(Bucket.owner_id == uid))
            await db.execute(delete(User).where(User.uid == uid))
            await db.commit()

        cleanup.add_task(cleanup_db)
        cleanup.add_task(mock_rgw_admin.remove_user, str(uid), purge_data=True)

        # Check that user is created in RGW
        assert mock_rgw_admin.get_user(str(uid))["keys"][0]["user"] is not None

        # Check that user is created in DB
        await db.reset()
        db_user = await db.scalar(select(User).where(User.uid == uid))
        assert db_user
        assert db_user.provider_id(oidc_provider) == provider_id
        assert db_user.display_name == display_name
        assert db_user.initialized
        assert db_user.last_login is not None

        # Check that upload and download bucket are created
        db_buckets = (await db.execute(select(Bucket).where(Bucket.owner_id == db_user.uid))).scalars().all()
        assert len(db_buckets) == 1
        # Check that buckets are created in S3
        for bucket in db_buckets:
            s3_bucket = await mock_s3_service.Bucket(bucket.name)
            cleanup.add_task(s3_bucket.delete)
            # Check that bucket policy is set
            s3_policy = await mock_s3_service.BucketPolicy(bucket.name)
            assert await s3_policy.policy is not None
            # Check that cors rule is set
            s3_cors = await mock_s3_service.BucketCors(bucket.name)
            assert await s3_cors.cors_rules is not None

            assert bucket.size_limit is not None
            assert bucket.object_limit is not None

    @pytest.mark.asyncio
    async def test_successful_login_with_existing_user(
        self, client: AsyncClient, random_user_with_provider: UserWithAuthCookie, monkeypatch: pytest.MonkeyPatch
    ) -> None:
        """
        Test for login callback route with an existing user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user_with_provider : clowm.tests.utils.UserWithAuthCookie
            Random user for testing with different OIDC providers.
        monkeypatch : pytest.MonkeyPatch
            Entrypoint for monkeypatching dependencies.
        """
        assert len(random_user_with_provider.user.registered_providers) == 1
        provider = random_user_with_provider.user.registered_providers[0]

        async def mock_userinfo(*args: Any, **kwargs: Any) -> UserInfo:
            return UserInfo(
                sub=random_user_with_provider.user.provider_id(provider),
                name=random_user_with_provider.user.display_name,
                email=random_user_with_provider.user.email,
                provider=provider,
            )

        monkeypatch.setattr(OIDCClient, "verify_fetch_userinfo", mock_userinfo)
        response = await client.get(
            self.auth_path + f"/login/callback/{provider}",
            follow_redirects=False,
        )
        assert response.status_code == status.HTTP_302_FOUND
        jwt = response.cookies.get("clowm-jwt", None)
        assert jwt is not None
        claim = decode_token(jwt)
        assert claim["sub"] == str(random_user_with_provider.uid)
        assert "first_login" not in response.headers["location"]

    @pytest.mark.asyncio
    async def test_successful_login_with_existing_user_and_different_email(
        self,
        client: AsyncClient,
        random_user_with_provider: UserWithAuthCookie,
        db: AsyncSession,
        monkeypatch: pytest.MonkeyPatch,
    ) -> None:
        """
        Test for login callback route with an existing user and a new email.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user_with_provider : clowm.tests.utils.UserWithAuthCookie
            Random user for testing with different OIDC providers.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        monkeypatch : pytest.MonkeyPatch
            Entrypoint for monkeypatching dependencies.
        """
        assert len(random_user_with_provider.user.registered_providers) == 1
        provider = random_user_with_provider.user.registered_providers[0]

        new_mail = f"{random_lower_string(10)}@example.com"

        async def mock_userinfo(*args: Any, **kwargs: Any) -> UserInfo:
            return UserInfo(
                sub=random_user_with_provider.user.provider_id(provider),
                name=random_user_with_provider.user.display_name,
                email=new_mail,
                provider=provider,
            )

        monkeypatch.setattr(OIDCClient, "verify_fetch_userinfo", mock_userinfo)

        r = await client.get(
            self.auth_path + f"/login/callback/{provider}",
            follow_redirects=False,
        )
        assert r.status_code == status.HTTP_302_FOUND
        await db.reset()
        jwt = r.cookies.get("clowm-jwt", None)
        assert jwt is not None
        claim = decode_token(jwt)
        assert claim["sub"] == str(random_user_with_provider.uid)
        assert r.headers["location"] == str(settings.ui_uri) + "dashboard"
        assert "first_login" not in r.headers["location"]

        db_user = await db.scalar(select(User).where(User.uid == random_user_with_provider.uid))
        assert db_user
        assert db_user.email == new_mail
        assert db_user.last_login is not None

    @pytest.mark.asyncio
    async def test_login_with_error(
        self, client: AsyncClient, monkeypatch: pytest.MonkeyPatch, oidc_provider: OIDCProvider
    ) -> None:
        """
        Test for login callback route with an error when fetching the user info from the OIDC provider.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        oidc_provider : clowm.core.oidc.OICDProvider
            One possible OIDC provider for the test.
        monkeypatch : pytest.MonkeyPatch
            Entrypoint for monkeypatching dependencies.
        """

        async def mock_userinfo(*args: Any, **kwargs: Any) -> UserInfo:
            raise LoginError("mock_error")

        monkeypatch.setattr(OIDCClient, "verify_fetch_userinfo", mock_userinfo)

        pre_request = await client.get(
            self.auth_path + f"/login/{oidc_provider}",
            params={
                "next": "/dashboard",
            },
            follow_redirects=False,
        )
        assert pre_request.status_code == status.HTTP_302_FOUND

        r = await client.get(
            self.auth_path + f"/login/callback/{oidc_provider}",
            follow_redirects=False,
            cookies=pre_request.cookies,
        )
        assert r.status_code == status.HTTP_302_FOUND
        assert r.cookies.get("clowm-jwt", None) is None
        redirect_url = AnyHttpUrl(r.headers["location"])
        assert next((_ for (key, _) in redirect_url.query_params() if key == "login_error"), None) is not None, (
            "expected 'login_error' as query parameter"
        )
        assert next((_ for (key, _) in redirect_url.query_params() if key == "link_error"), None) is None, (
            "expected 'link_error' not as query parameter"
        )

        assert next((_ for (key, _) in redirect_url.query_params() if key == "next"), None) is not None, (
            "expected 'next' as query parameter"
        )
        assert next((_ for (key, _) in redirect_url.query_params() if key == "first_login"), None) is None, (
            "expected 'first_login' as query parameter"
        )


class TestInvitedAuthRouteCallback(_TestAuthRoute):
    @pytest.mark.asyncio
    async def test_login_with_invited_unknown_user(
        self,
        client: AsyncClient,
        mock_rgw_admin: MockRGWAdmin,
        db: AsyncSession,
        cleanup: CleanupList,
        monkeypatch: pytest.MonkeyPatch,
        oidc_provider: OIDCProvider,
    ) -> None:
        """
        Test login callback route with an unknown invited user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        mock_rgw_admin : clowm.tests.mocks.mock_rgw_admin.MockRGWAdmin
            Mock RGW admin for Ceph.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        oidc_provider : clowm.core.oidc.OICDProvider
            One possible OIDC provider for the test.
        monkeypatch : pytest.MonkeyPatch
            Entrypoint for monkeypatching dependencies.
        """
        # set up user that is registered by admin but has not logged in yet
        provider_id = random_lower_string()
        user = User(
            display_name=random_lower_string(),
            email=f"{random_lower_string(10)}@example.com",
            invitation_token=token_urlsafe(32),
            invitation_token_created_at=round(time.time()),
        )
        db.add(user)
        await db.commit()
        assert not user.initialized

        async def mock_userinfo(*args: Any, **kwargs: Any) -> UserInfo:
            return UserInfo(
                sub=provider_id,
                name=user.display_name,
                email=user.email,
                provider=oidc_provider,
            )

        monkeypatch.setattr(OIDCClient, "verify_fetch_userinfo", mock_userinfo)

        pre_request = await client.get(
            self.auth_path + f"/login/{oidc_provider}",
            params={
                "invitation_token": user.invitation_token,
                "next": "/dashboard",
            },
            follow_redirects=False,
        )
        assert pre_request.status_code == status.HTTP_302_FOUND

        await db.execute(delete(User).where(User.uid == user.uid))
        await db.commit()

        r = await client.get(
            self.auth_path + f"/login/callback/{oidc_provider}",
            follow_redirects=False,
            cookies=pre_request.cookies,
        )
        # Check response and valid/right jwt token
        assert r.status_code == status.HTTP_302_FOUND
        assert "login_error" in r.headers["location"]
        assert "first_login" not in r.headers["location"]

    @pytest.mark.asyncio
    async def test_login_with_invited_user_and_taken_oidc_account(
        self,
        client: AsyncClient,
        mock_rgw_admin: MockRGWAdmin,
        db: AsyncSession,
        cleanup: CleanupList,
        monkeypatch: pytest.MonkeyPatch,
        random_user_with_provider: UserWithAuthCookie,
    ) -> None:
        """
        Test login callback route with an invited user that tires to use an already connected AAI account.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        mock_rgw_admin : clowm.tests.mocks.mock_rgw_admin.MockRGWAdmin
            Mock RGW admin for Ceph.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        random_user_with_provider : clowm.tests.utils.UserWithAuthCookie
            Random user for testing with different OIDC providers.
        monkeypatch : pytest.MonkeyPatch
            Entrypoint for monkeypatching dependencies.
        """
        assert len(random_user_with_provider.user.registered_providers) == 1
        provider = random_user_with_provider.user.registered_providers[0]
        # set up user that is registered by admin but has not logged in yet
        user = User(
            display_name=random_lower_string(),
            email=f"{random_lower_string(10)}@example.com",
            invitation_token=token_urlsafe(32),
            invitation_token_created_at=round(time.time()),
        )
        db.add(user)
        await db.commit()

        async def delete_user() -> None:
            await db.execute(delete(User).where(User.uid == user.uid))
            await db.commit()

        cleanup.add_task(delete_user)
        assert not user.initialized

        async def mock_userinfo(*args: Any, **kwargs: Any) -> UserInfo:
            return UserInfo(
                sub=random_user_with_provider.user.provider_id(provider),
                name=user.display_name,
                email=user.email,
                provider=provider,
            )

        monkeypatch.setattr(OIDCClient, "verify_fetch_userinfo", mock_userinfo)

        pre_request = await client.get(
            self.auth_path + f"/login/{provider}",
            params={
                "invitation_token": user.invitation_token,
                "next": "/dashboard",
            },
            follow_redirects=False,
        )
        assert pre_request.status_code == status.HTTP_302_FOUND

        r = await client.get(
            self.auth_path + f"/login/callback/{OIDCProvider.lifescience}",
            follow_redirects=False,
            cookies=pre_request.cookies,
        )
        # Check response and valid/right jwt token
        assert r.status_code == status.HTTP_302_FOUND
        assert "login_error" in r.headers["location"]
        assert "first_login" not in r.headers["location"]

    @pytest.mark.asyncio
    async def test_successful_login_with_invited_user(
        self,
        client: AsyncClient,
        mock_rgw_admin: MockRGWAdmin,
        db: AsyncSession,
        cleanup: CleanupList,
        mock_s3_service: S3ServiceResource,
        monkeypatch: pytest.MonkeyPatch,
        oidc_provider: OIDCProvider,
    ) -> None:
        """
        Test successful login callback route with an invited user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        mock_rgw_admin : clowm.tests.mocks.mock_rgw_admin.MockRGWAdmin
            Mock RGW admin for Ceph.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        mock_s3_service: types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service Resource for testing.
        oidc_provider : clowm.core.oidc.OICDProvider
            One possible OIDC provider for the test.
        monkeypatch : pytest.MonkeyPatch
            Entrypoint for monkeypatching dependencies.
        """
        # set up user that is registered by admin but has not logged in yet
        provider_id = random_lower_string()
        user = User(
            display_name=random_lower_string(),
            email=f"{random_lower_string(10)}@example.com",
            invitation_token=token_urlsafe(32),
            invitation_token_created_at=round(time.time()),
        )
        db.add(user)
        await db.commit()
        assert not user.initialized

        async def delete_user() -> None:
            await db.execute(delete(Bucket).where(Bucket.owner_id == user.uid))
            await db.execute(delete(User).where(User.uid == user.uid))
            await db.commit()

        cleanup.add_task(delete_user)

        async def mock_userinfo(*args: Any, **kwargs: Any) -> UserInfo:
            return UserInfo(
                sub=provider_id,
                name=user.display_name,
                email=user.email,
                provider=oidc_provider,
            )

        monkeypatch.setattr(OIDCClient, "verify_fetch_userinfo", mock_userinfo)

        pre_request = await client.get(
            self.auth_path + f"/login/{oidc_provider}",
            params={
                "invitation_token": user.invitation_token,
                "next": "/dashboard",
            },
            follow_redirects=False,
        )
        assert pre_request.status_code == status.HTTP_302_FOUND

        r = await client.get(
            self.auth_path + f"/login/callback/{oidc_provider}",
            follow_redirects=False,
            cookies=pre_request.cookies,
        )
        # Check response and valid/right jwt token
        assert r.status_code == status.HTTP_302_FOUND
        await db.reset()
        assert "login_error" not in r.headers["location"]
        assert "first_login" in r.headers["location"]
        cleanup.add_task(mock_rgw_admin.remove_user, str(user.uid))

        assert f"next={urllib.parse.quote_plus('/dashboard')}" in r.headers["location"]
        jwt = r.cookies.get("clowm-jwt", None)
        assert jwt is not None
        claim = decode_token(jwt)
        assert claim.get("sub", None) is not None
        assert UUID(claim["sub"]) == user.uid

        # Check that user is created in RGW
        assert mock_rgw_admin.get_user(claim["sub"])["keys"][0]["user"] is not None
        # Check that user is created in DB
        selected_user = await db.scalar(select(User).where(User.uid == user.uid))
        assert selected_user is not None
        assert selected_user.provider_id(oidc_provider) == provider_id
        assert selected_user.initialized
        assert selected_user.invitation_token is None
        assert selected_user.invitation_token_created_at is None
        assert selected_user.last_login is not None


class TestAccountConnectionRouteCallback(_TestAuthRoute):
    @pytest.mark.asyncio
    async def test_unauthenticated_account_connection_callback(
        self,
        client: AsyncClient,
        oidc_provider: OIDCProvider,
    ) -> None:
        """
        Test unauthorized account connection callback route.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        oidc_provider : clowm.core.oidc.OICDProvider
            One possible OIDC provider for the test.
        """
        response = await client.get(
            self.auth_path + f"/account/callback/{oidc_provider}",
            follow_redirects=False,
        )
        assert response.status_code == status.HTTP_302_FOUND
        redirect_url = AnyHttpUrl(response.headers["location"])
        assert next((_ for (key, _) in redirect_url.query_params() if key == "link_error"), None) is None
        assert next((val for (key, val) in redirect_url.query_params() if key == "login_error"), None) is not None
        assert next((val for (key, val) in redirect_url.query_params() if key == "link_success"), None) is None

    @pytest.mark.asyncio
    async def test_malformed_jwt_account_connection_callback(
        self,
        client: AsyncClient,
        oidc_provider: OIDCProvider,
    ) -> None:
        """
        Test account connection callback route with malformed JWT cookie.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        oidc_provider : clowm.core.oidc.OICDProvider
            One possible OIDC provider for the test.
        """
        response = await client.get(
            self.auth_path + f"/account/callback/{oidc_provider}",
            follow_redirects=False,
            cookies=Cookies({cookie_jwt_scheme.model.name: "broken"}),
        )
        assert response.status_code == status.HTTP_302_FOUND
        redirect_url = AnyHttpUrl(response.headers["location"])
        assert next((_ for (key, _) in redirect_url.query_params() if key == "link_error"), None) is None
        assert next((val for (key, val) in redirect_url.query_params() if key == "login_error"), None) is not None
        assert next((val for (key, val) in redirect_url.query_params() if key == "link_success"), None) is None

    @pytest.mark.asyncio
    async def test_account_connection_callback_with_missing_pre_request(
        self, client: AsyncClient, random_user_with_provider: UserWithAuthCookie
    ) -> None:
        """
        Test account connection callback route with a missing pre-request.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user_with_provider : clowm.tests.utils.UserWithAuthCookie
            Random user for testing with different OIDC providers.
        """
        assert len(random_user_with_provider.user.registered_providers) == 1
        random_user_provider = random_user_with_provider.user.registered_providers[0]
        response = await client.get(
            self.auth_path + f"/account/callback/{random_user_provider}",
            follow_redirects=False,
            cookies=random_user_with_provider.auth_cookie,
        )
        assert response.status_code == status.HTTP_302_FOUND
        redirect_url = AnyHttpUrl(response.headers["location"])
        assert next((_ for (key, _) in redirect_url.query_params() if key == "link_error"), None) is not None
        assert next((val for (key, val) in redirect_url.query_params() if key == "link_success"), None) is None

    @pytest.mark.asyncio
    async def test_account_connection_callback_with_mismatching_users(
        self, client: AsyncClient, random_user_with_provider: UserWithAuthCookie, random_second_user: UserWithAuthCookie
    ) -> None:
        """
        Test account connection callback route with mismatching user between pre-request and and callback.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user_with_provider : clowm.tests.utils.UserWithAuthCookie
            Random user for testing with different OIDC providers.
        random_second_user : clowm.tests.utils.UserWithAuthCookie
            Random second user for testing.
        """
        assert len(random_user_with_provider.user.registered_providers) == 1
        random_user_provider = random_user_with_provider.user.registered_providers[0]
        missing_provider = next((prov for prov in OIDCProvider if prov != random_user_provider), None)
        assert missing_provider is not None

        pre_request = await client.get(
            self.auth_path + f"/account/connect/{missing_provider}",
            cookies=random_user_with_provider.auth_cookie,
            follow_redirects=False,
        )
        assert pre_request.status_code == status.HTTP_302_FOUND

        response = await client.get(
            self.auth_path + f"/account/callback/{random_user_provider}",
            follow_redirects=False,
            cookies=merge_cookies(random_second_user.auth_cookie, pre_request.cookies),
        )
        assert response.status_code == status.HTTP_302_FOUND
        redirect_url = AnyHttpUrl(response.headers["location"])
        assert next((_ for (key, _) in redirect_url.query_params() if key == "link_error"), None) is not None
        assert next((val for (key, val) in redirect_url.query_params() if key == "link_success"), None) is None

    @pytest.mark.asyncio
    async def test_account_connection_with_taken_aai_account(
        self,
        client: AsyncClient,
        random_user_with_provider: UserWithAuthCookie,
        monkeypatch: pytest.MonkeyPatch,
        db: AsyncSession,
        cleanup: CleanupList,
    ) -> None:
        """
        Test account connection callback route with already taken AAI account.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user_with_provider : clowm.tests.utils.UserWithAuthCookie
            Random user for testing with different OIDC providers.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        monkeypatch : pytest.MonkeyPatch
            Entrypoint for monkeypatching dependencies.
            Async database session to perform query on.
        cleanup : clowm.tests.utils.utils.CleanupList
        """
        assert len(random_user_with_provider.user.registered_providers) == 1
        random_user_provider = random_user_with_provider.user.registered_providers[0]
        missing_provider = next((prov for prov in OIDCProvider if prov != random_user_provider), None)
        assert missing_provider is not None
        missing_provider_id = random_lower_string()
        user = User(
            **{
                "display_name": random_lower_string(),
                "email": f"{random_lower_string(10)}@example.com",
                User.provider_id_column(missing_provider).key: missing_provider_id,
            }
        )
        db.add(user)
        await db.commit()
        assert not user.initialized

        async def delete_user() -> None:
            await db.execute(delete(User).where(User.uid == user.uid))
            await db.commit()

        cleanup.add_task(delete_user)

        async def mock_userinfo(*args: Any, **kwargs: Any) -> UserInfo:
            return UserInfo(
                sub=missing_provider_id,
                name=random_user_with_provider.user.display_name,
                email=random_user_with_provider.user.email,
                provider=missing_provider,
            )

        pre_request = await client.get(
            self.auth_path + f"/account/connect/{missing_provider}",
            cookies=random_user_with_provider.auth_cookie,
            follow_redirects=False,
        )
        assert pre_request.status_code == status.HTTP_302_FOUND

        monkeypatch.setattr(OIDCClient, "verify_fetch_userinfo", mock_userinfo)
        response = await client.get(
            self.auth_path + f"/account/callback/{missing_provider}",
            follow_redirects=False,
            cookies=merge_cookies(pre_request.cookies, random_user_with_provider.auth_cookie),
        )
        assert response.status_code == status.HTTP_302_FOUND

        redirect_url = AnyHttpUrl(response.headers["location"])
        assert next((_ for (key, _) in redirect_url.query_params() if key == "link_error"), None) is not None
        assert next((val for (key, val) in redirect_url.query_params() if key == "link_success"), None) is None

    @pytest.mark.asyncio
    async def test_account_connection_with_already_connected_account(
        self,
        client: AsyncClient,
        random_user_with_provider: UserWithAuthCookie,
        monkeypatch: pytest.MonkeyPatch,
        db: AsyncSession,
        cleanup: CleanupList,
    ) -> None:
        """
        Test account connection callback route with user that is already connected to a matching AAI account.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user_with_provider : clowm.tests.utils.UserWithAuthCookie
            Random user for testing with different OIDC providers.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        monkeypatch : pytest.MonkeyPatch
            Entrypoint for monkeypatching dependencies.
            Async database session to perform query on.
        cleanup : clowm.tests.utils.utils.CleanupList
        """
        assert len(random_user_with_provider.user.registered_providers) == 1
        random_user_provider = random_user_with_provider.user.registered_providers[0]
        old_id = random_user_with_provider.user.provider_id(random_user_provider)

        await db.execute(
            update(User)
            .where(User.uid == random_user_with_provider.uid)
            .values(**{User.provider_id_column(random_user_provider).key: None})
        )
        await db.commit()

        async def mock_userinfo(*args: Any, **kwargs: Any) -> UserInfo:
            return UserInfo(
                sub=random_lower_string(),
                name=random_user_with_provider.user.display_name,
                email=random_user_with_provider.user.email,
                provider=random_user_provider,
            )

        pre_request = await client.get(
            self.auth_path + f"/account/connect/{random_user_provider}",
            cookies=random_user_with_provider.auth_cookie,
            follow_redirects=False,
        )
        assert pre_request.status_code == status.HTTP_302_FOUND

        await db.execute(
            update(User)
            .where(User.uid == random_user_with_provider.uid)
            .values(**{User.provider_id_column(random_user_provider).key: old_id})
        )
        await db.commit()

        monkeypatch.setattr(OIDCClient, "verify_fetch_userinfo", mock_userinfo)

        response = await client.get(
            self.auth_path + f"/account/callback/{random_user_provider}",
            follow_redirects=False,
            cookies=merge_cookies(pre_request.cookies, random_user_with_provider.auth_cookie),
        )
        assert response.status_code == status.HTTP_302_FOUND
        assert response.cookies.get("clowm-jwt", None) is None

        redirect_url = AnyHttpUrl(response.headers["location"])
        assert next((_ for (key, _) in redirect_url.query_params() if key == "link_error"), None) is not None
        assert next((val for (key, val) in redirect_url.query_params() if key == "link_success"), None) is None

    @pytest.mark.asyncio
    async def test_successful_account_connection(
        self,
        client: AsyncClient,
        random_user_with_provider: UserWithAuthCookie,
        monkeypatch: pytest.MonkeyPatch,
        db: AsyncSession,
    ) -> None:
        """
        Test successful account connection callback route.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user_with_provider : clowm.tests.utils.UserWithAuthCookie
            Random user for testing with different OIDC providers.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        monkeypatch : pytest.MonkeyPatch
            Entrypoint for monkeypatching dependencies.
        """
        new_provider_id = random_lower_string()
        assert len(random_user_with_provider.user.registered_providers) == 1
        random_user_provider = random_user_with_provider.user.registered_providers[0]
        missing_provider = next((prov for prov in OIDCProvider if prov != random_user_provider), None)
        assert missing_provider is not None

        async def mock_userinfo(*args: Any, **kwargs: Any) -> UserInfo:
            return UserInfo(
                sub=new_provider_id,
                name=random_user_with_provider.user.display_name,
                email=random_user_with_provider.user.email,
                provider=missing_provider,
            )

        pre_request = await client.get(
            self.auth_path + f"/account/connect/{missing_provider}",
            cookies=random_user_with_provider.auth_cookie,
            follow_redirects=False,
        )
        assert pre_request.status_code == status.HTTP_302_FOUND

        monkeypatch.setattr(OIDCClient, "verify_fetch_userinfo", mock_userinfo)

        response = await client.get(
            self.auth_path + f"/account/callback/{missing_provider}",
            follow_redirects=False,
            cookies=merge_cookies(pre_request.cookies, random_user_with_provider.auth_cookie),
        )
        assert response.status_code == status.HTTP_302_FOUND

        redirect_url = AnyHttpUrl(response.headers["location"])
        assert redirect_url.path == "/profile"
        assert next((_ for (key, _) in redirect_url.query_params() if key == "link_error"), None) is None
        assert next((val for (key, val) in redirect_url.query_params() if key == "link_success"), None)

        await db.reset()
        # Check that CloWM account is connected to new AAI account
        selected_user = await db.scalar(select(User).where(User.uid == random_user_with_provider.uid))
        assert selected_user is not None
        assert selected_user.provider_id(missing_provider) == new_provider_id

    @pytest.mark.asyncio
    async def test_account_connection_with_error(
        self,
        client: AsyncClient,
        monkeypatch: pytest.MonkeyPatch,
        random_user_with_provider: UserWithAuthCookie,
    ) -> None:
        """
        Test for account connection callback route with an error when fetching the user info from the OIDC provider.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user_with_provider : clowm.tests.utils.UserWithAuthCookie
            Random user for testing with different OIDC providers.
        monkeypatch : pytest.MonkeyPatch
            Entrypoint for monkeypatching dependencies.
        """
        assert len(random_user_with_provider.user.registered_providers) == 1
        random_user_provider = random_user_with_provider.user.registered_providers[0]
        missing_provider = next((prov for prov in OIDCProvider if prov != random_user_provider), None)
        assert missing_provider is not None

        async def mock_userinfo(*args: Any, **kwargs: Any) -> UserInfo:
            raise AccountConnectionError("mock_error")

        monkeypatch.setattr(OIDCClient, "verify_fetch_userinfo", mock_userinfo)

        pre_request = await client.get(
            self.auth_path + f"/account/connect/{missing_provider}",
            cookies=random_user_with_provider.auth_cookie,
            follow_redirects=False,
        )
        assert pre_request.status_code == status.HTTP_302_FOUND

        r = await client.get(
            self.auth_path + f"/account/callback/{missing_provider}",
            follow_redirects=False,
            cookies=merge_cookies(pre_request.cookies, random_user_with_provider.auth_cookie),
        )
        assert r.status_code == status.HTTP_302_FOUND
        assert r.cookies.get("clowm-jwt", None) is None
        redirect_url = AnyHttpUrl(r.headers["location"])
        assert next((_ for (key, _) in redirect_url.query_params() if key == "login_error"), None) is None, (
            "expected 'login_error' not as query parameter"
        )
        assert next((_ for (key, _) in redirect_url.query_params() if key == "link_error"), None) is not None, (
            "expected 'link_error' as query parameter"
        )

        assert next((_ for (key, _) in redirect_url.query_params() if key == "next"), None) is None, (
            "expected 'next' not as query parameter"
        )


class TestLogoutRoute(_TestAuthRoute):
    @pytest.mark.asyncio
    async def test_successful_logout(self, client: AsyncClient, random_user: UserWithAuthCookie) -> None:
        """
        Test for logout route.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.UserWithAuthCookie
            Random user for testing.
        """
        r = await client.get(
            self.auth_path + "/logout",
            follow_redirects=False,
        )
        # Check response and valid/right jwt token
        assert r.status_code == status.HTTP_302_FOUND
        assert r.cookies.get("clowm-jwt", None) is None
