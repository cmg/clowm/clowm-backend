import time
import urllib.parse
from collections.abc import AsyncIterator
from uuid import uuid4

import pytest
import pytest_asyncio
from fastapi import status
from httpx import AsyncClient
from pydantic import TypeAdapter
from sqlalchemy import select, update
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.api.dependencies.pagination import HTTPLinkHeader
from clowm.models import News
from clowm.schemas.news import NewsIn, NewsOut
from clowm.tests.utils import CleanupList, UserWithAuthCookie, delete_news_event, random_lower_string

NewsList = TypeAdapter(list[NewsOut])


class _TestResourceRoutes:
    base_path: str = "/ui/news"

    @pytest_asyncio.fixture(scope="function")
    async def random_news_event(self, db: AsyncSession, random_user: UserWithAuthCookie) -> AsyncIterator[News]:
        """
        Create a random news event and deletes it afterward.
        """
        news = News(
            content=random_lower_string(),
            title=random_lower_string(8),
            creator_id=random_user.uid,
            important_till=round(time.time()) + 1000,
            category="system",
        )
        db.add(news)
        await db.commit()
        yield news
        await db.delete(news)
        await db.commit()


class TestResourceRouteCreate(_TestResourceRoutes):
    @pytest.mark.asyncio
    async def test_create_news_route(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        db: AsyncSession,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for creating a new news event.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        news_in = NewsIn(
            content=random_lower_string(),
            title=random_lower_string(8),
            important_till=round(time.time()) + 1000,
        )
        response = await client.post(self.base_path, content=news_in.model_dump_json(), cookies=random_user.auth_cookie)
        assert response.status_code == status.HTTP_201_CREATED
        await db.reset()
        news_out = NewsOut.model_validate_json(response.content)

        cleanup.add_task(delete_news_event, db=db, news_id=news_out.news_id)

        assert news_out.title == news_in.title
        assert news_out.content == news_in.content
        assert news_out.important_till == news_in.important_till
        assert news_out.creator_id == random_user.uid

        # test that the news got actually created
        news_db = await db.scalar(select(News).where(News.news_id == news_out.news_id))
        assert news_db is not None


class TestResourceRouteGet(_TestResourceRoutes):
    @pytest.mark.asyncio
    async def test_get_news_route(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_news_event: News,
    ) -> None:
        """
        Test for getting a news event.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_news_event : clowm.models.News
            Random news event for testing.
        """
        response = await client.get(f"{self.base_path}/{random_news_event.news_id}", cookies=random_user.auth_cookie)
        assert response.status_code == status.HTTP_200_OK
        news_out = NewsOut.model_validate_json(response.content)

        assert news_out.news_id == random_news_event.news_id
        assert news_out.title == random_news_event.title
        assert news_out.content == random_news_event.content
        assert news_out.important_till == random_news_event.important_till
        assert news_out.creator_id == random_user.uid

    @pytest.mark.asyncio
    async def test_get_non_existing_news_route(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for getting a non-existing news event.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        response = await client.get(f"{self.base_path}/{uuid4()}", cookies=random_user.auth_cookie)
        assert response.status_code == status.HTTP_404_NOT_FOUND


class TestResourceRouteDelete(_TestResourceRoutes):
    @pytest.mark.asyncio
    async def test_delete_news_route(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        db: AsyncSession,
        random_news_event: News,
    ) -> None:
        """
        Test for deleting a news event.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_news_event : clowm.models.News
            Random news event for testing.
        """
        response = await client.delete(f"{self.base_path}/{random_news_event.news_id}", cookies=random_user.auth_cookie)
        assert response.status_code == status.HTTP_204_NO_CONTENT

        # test that the news got actually deleted
        news_db = await db.scalar(select(News).where(News.news_id == random_news_event.news_id))
        assert news_db is None


class TestResourceRouteList(_TestResourceRoutes):
    @pytest.mark.asyncio
    async def test_list_news_events_with_page_size(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for listing all news events with pagination.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        page_size = 5
        for _ in range(page_size):
            news_event = News(
                content=random_lower_string(),
                title=random_lower_string(8),
                creator_id=random_user.uid,
                category="system",
            )
            db.add(news_event)
            await db.commit()
            cleanup.add_task(delete_news_event, db=db, news_id=news_event.news_id)

        response1 = await client.get(self.base_path, cookies=random_user.auth_cookie, params={"per_page": page_size})

        assert response1.status_code == status.HTTP_200_OK
        assert len(NewsList.validate_json(response1.content)) == page_size
        # if there are no more news events, the link header should be empty
        assert response1.headers.get("link", None) is None

        news_event = News(
            content=random_lower_string(), title=random_lower_string(8), creator_id=random_user.uid, category="system"
        )
        db.add(news_event)
        await db.commit()
        cleanup.add_task(delete_news_event, db=db, news_id=news_event.news_id)

        response2 = await client.get(
            self.base_path,
            cookies=random_user.auth_cookie,
            params={
                "per_page": page_size,
                "created_after": round(time.time()) - 1000,
                "creator_id": str(random_user.uid),
            },
        )
        assert response2.status_code == status.HTTP_200_OK
        assert len(NewsList.validate_json(response2.content)) == page_size

        # if there are more news events, the link header should point to the next pagination url
        next_link = HTTPLinkHeader.parse(response2.headers.get("link", "")).get(rel="next", default=None)
        assert next_link is not None
        link_query_params = urllib.parse.parse_qs(next_link.link.query)

        page_query_param = link_query_params.get("per_page", None)
        assert page_query_param is not None
        assert len(page_query_param) == 1
        assert int(page_query_param[0]) == page_size

        assert link_query_params.get("id_after", None) is not None
        assert link_query_params.get("created_after", None) is not None
        assert link_query_params.get("creator_id", None) is not None

    @pytest.mark.asyncio
    async def test_list_latest_news_events(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_news_event: News,
    ) -> None:
        """
        Test for listing the latest news events.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_news_event : clowm.models.News
            Random news event for testing.
        """
        response = await client.get(f"{self.base_path}/latest", cookies=random_user.auth_cookie)
        assert response.status_code == status.HTTP_200_OK
        assert len(NewsList.validate_json(response.content)) == 1

        await db.execute(update(News).where(News.news_id == random_news_event.news_id).values(important_till=None))
        await db.commit()

        response = await client.get(f"{self.base_path}/latest", cookies=random_user.auth_cookie)
        assert response.status_code == status.HTTP_200_OK
        assert len(NewsList.validate_json(response.content)) == 1

        await db.execute(update(News).where(News.news_id == random_news_event.news_id).values(created_at=1))
        await db.commit()

        response = await client.get(f"{self.base_path}/latest", cookies=random_user.auth_cookie)
        assert response.status_code == status.HTTP_200_OK
        assert len(NewsList.validate_json(response.content)) == 0

        await db.execute(
            update(News)
            .where(News.news_id == random_news_event.news_id)
            .values(important_till=round(time.time()) + 1000)
        )
        await db.commit()

        response = await client.get(f"{self.base_path}/latest", cookies=random_user.auth_cookie)
        assert response.status_code == status.HTTP_200_OK
        assert len(NewsList.validate_json(response.content)) == 1

        await db.execute(update(News).where(News.news_id == random_news_event.news_id).values(important_till=1))
        await db.commit()

        response = await client.get(f"{self.base_path}/latest", cookies=random_user.auth_cookie)
        assert response.status_code == status.HTTP_200_OK
        assert len(NewsList.validate_json(response.content)) == 0
