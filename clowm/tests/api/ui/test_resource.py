import json
from typing import TYPE_CHECKING
from uuid import uuid4

import pytest
from botocore.exceptions import ClientError
from fastapi import status
from httpx import AsyncClient
from pydantic import TypeAdapter
from sqlalchemy import delete, insert, select
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.core.config import settings
from clowm.db.mixins.ownership_transfer_mixin import OwnershipTypeEnum
from clowm.models import Resource, ResourceVersion
from clowm.models.role import Role, RoleIdMapping, UserRoleMapping
from clowm.schemas.ownership_transfer import OwnershipTransferRequestIn, OwnershipTransferRequestOut
from clowm.schemas.resource import ResourceIn, ResourceOut
from clowm.schemas.resource_version import UserSynchronizationRequestOut, resource_version_key
from clowm.tests.utils import CleanupList, UserWithAuthCookie, delete_policy_statement, random_lower_string
from clowm.tests.utils.otr import create_otr

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object


class _TestResourceRoutes:
    base_path: str = "/ui/resources"


class TestResourceRouteCreate(_TestResourceRoutes):
    @pytest.mark.asyncio
    async def test_create_resource_route(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        db: AsyncSession,
        cleanup: CleanupList,
        mock_s3_service: S3ServiceResource,
    ) -> None:
        """
        Test for creating a new resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        """
        resource = ResourceIn(
            release=random_lower_string(6),
            name=random_lower_string(8),
            description=random_lower_string(16),
            source=random_lower_string(8),
        )
        response = await client.post(
            self.base_path, content=resource.model_dump_json(), cookies=random_user.auth_cookie
        )
        assert response.status_code == status.HTTP_201_CREATED
        await db.reset()
        resource_out = ResourceOut.model_validate_json(response.content)

        async def delete_resource() -> None:
            await db.execute(delete(Resource).where(Resource.resource_id == resource_out.resource_id))
            await db.commit()

        cleanup.add_task(delete_resource)
        s3_policy = await mock_s3_service.BucketPolicy(settings.s3.data_bucket)
        cleanup.add_task(delete_policy_statement, s3policy=s3_policy, sid=str(resource_out.resource_id))

        assert resource_out.name == resource.name
        assert len(resource_out.versions) == 1
        resource_version = resource_out.versions[0]
        assert resource_version.resource_id == resource_out.resource_id
        assert resource_version.status == ResourceVersion.ResourceVersionStatus.RESOURCE_REQUESTED
        assert resource_version.release == resource.release

        # test if the maintainer get permission to upload to the resource bucket
        s3_policy = await mock_s3_service.BucketPolicy(settings.s3.data_bucket)
        policy = json.loads(await s3_policy.policy)
        assert sum(1 for stmt in policy["Statement"] if stmt["Sid"] == str(resource_out.resource_id)) == 1
        assert sum(1 for stmt in policy["Statement"] if stmt["Sid"] == str(resource_version.resource_version_id)) == 1

        # test that the resource got actually created
        resource_db = await db.scalar(select(Resource).where(Resource.resource_id == resource_out.resource_id))
        assert resource_db is not None

    @pytest.mark.asyncio
    async def test_create_duplicated_resource_route(
        self, client: AsyncClient, random_user: UserWithAuthCookie, db: AsyncSession, random_resource: Resource
    ) -> None:
        """
        Test for creating a duplicated resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_resource : clowm.models.Resource
            Random resource for testing.
        """
        resource = ResourceIn(
            release=random_lower_string(6),
            name=random_resource.name,
            description=random_lower_string(16),
            source=random_lower_string(8),
        )
        response = await client.post(
            self.base_path, content=resource.model_dump_json(), cookies=random_user.auth_cookie
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_create_resource_otr(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_user: UserWithAuthCookie,
        random_second_user: UserWithAuthCookie,
        db: AsyncSession,
    ) -> None:
        """
        Test for creating a resource OTR.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowm.models.Resource
            Random resource for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is the owner of the resource.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is not the owner of the resource.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        response1 = await client.post(
            f"{self.base_path}/{random_resource.resource_id}/ownership_transfer_request",
            cookies=random_second_user.auth_cookie,
            content=OwnershipTransferRequestIn(new_owner_uid=random_second_user.uid).model_dump_json(),
        )

        assert response1.status_code == status.HTTP_403_FORBIDDEN

        # Test Fail: New owner does not exist
        response2 = await client.post(
            f"{self.base_path}/{random_resource.resource_id}/ownership_transfer_request",
            cookies=random_user.auth_cookie,
            content=OwnershipTransferRequestIn(new_owner_uid=uuid4()).model_dump_json(),
        )

        assert response2.status_code == status.HTTP_400_BAD_REQUEST

        # Test Fail: New and current owner are the same
        response3 = await client.post(
            f"{self.base_path}/{random_resource.resource_id}/ownership_transfer_request",
            cookies=random_user.auth_cookie,
            content=OwnershipTransferRequestIn(new_owner_uid=random_resource.maintainer_id).model_dump_json(),
        )

        assert response3.status_code == status.HTTP_400_BAD_REQUEST

        # Test success
        otr_in = OwnershipTransferRequestIn(new_owner_uid=random_second_user.uid, comment=random_lower_string(64))
        response4 = await client.post(
            f"{self.base_path}/{random_resource.resource_id}/ownership_transfer_request",
            cookies=random_user.auth_cookie,
            content=otr_in.model_dump_json(),
        )

        assert response4.status_code == status.HTTP_201_CREATED
        await db.reset()
        otr_out = OwnershipTransferRequestOut.model_validate_json(response4.content)

        assert otr_out.new_owner_uid == otr_in.new_owner_uid
        assert otr_out.comment == otr_in.comment
        assert otr_out.target_name == random_resource.name
        assert otr_out.target_description == random_resource.short_description
        assert otr_out.target_id == random_resource.resource_id
        assert otr_out.target_type == OwnershipTypeEnum.RESOURCE

        selected_resource = await db.scalar(select(Resource).where(Resource.resource_id == random_resource.resource_id))
        assert selected_resource is not None
        assert selected_resource.transfer_created_at is not None
        assert selected_resource.transfer_comment == otr_in.comment
        assert selected_resource.transfer_new_owner_uid == random_second_user.uid


class TestResourceRouteDelete(_TestResourceRoutes):
    @pytest.mark.asyncio
    async def test_delete_resource_route(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_user: UserWithAuthCookie,
        mock_s3_service: S3ServiceResource,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for deleting a resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowm.models.Resource
            Random resource for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        """
        response = await client.delete(
            f"{self.base_path}/{str(random_resource.resource_id)}", cookies=random_user.auth_cookie
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT

        # test if all permission for the maintainer got deleted in S3
        s3_policy = await mock_s3_service.BucketPolicy(settings.s3.data_bucket)
        policy = json.loads(await s3_policy.policy)
        assert (
            sum(
                1
                for stmt in policy["Statement"]
                if stmt["Sid"]
                in [str(random_resource.resource_id)] + [str(rv.resource_version_id) for rv in random_resource.versions]
            )
            == 0
        )

        # test if the resource got deleted in S3
        s3_key = resource_version_key(random_resource.resource_id, random_resource.versions[0].resource_version_id)
        with pytest.raises(ClientError):
            obj = await mock_s3_service.ObjectSummary(settings.s3.data_bucket, s3_key)
            await obj.load()

    @pytest.mark.asyncio
    async def test_delete_resource_otr(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_user: UserWithAuthCookie,
        random_second_user: UserWithAuthCookie,
        db: AsyncSession,
    ) -> None:
        """
        Test for deleting a resource OTR.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowm.models.Resource
            Random resource for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is the owner of the resource.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is not the owner of the resource.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        # Test Fail: OTR does not exist
        response1 = await client.delete(
            f"{self.base_path}/{random_resource.resource_id}/ownership_transfer_request",
            cookies=random_user.auth_cookie,
        )
        assert response1.status_code == status.HTTP_404_NOT_FOUND

        await create_otr(
            target_id=random_resource.resource_id, new_owner_id=random_second_user.uid, db=db, otr_type=Resource
        )

        # Test Success
        response2 = await client.delete(
            f"{self.base_path}/{random_resource.resource_id}/ownership_transfer_request",
            cookies=random_user.auth_cookie,
        )
        assert response2.status_code == status.HTTP_204_NO_CONTENT

        await db.refresh(random_resource)
        assert random_resource.transfer_created_at is None
        assert random_resource.transfer_comment is None
        assert random_resource.transfer_new_owner_uid is None
        assert random_resource.maintainer_id == random_user.uid

    @pytest.mark.asyncio
    async def test_reject_resource_otr(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_second_user: UserWithAuthCookie,
        db: AsyncSession,
    ) -> None:
        """
        Test for rejecting a resource OTR.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowm.models.Resource
            Random resource for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is not the owner of the bucket.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        # Add DB_MAINTAINER role to random_second_user
        mapping = RoleIdMapping()
        await db.execute(
            insert(UserRoleMapping).values(uid=random_second_user.uid, role_id=mapping[Role.RoleEnum.DB_MAINTAINER])
        )
        await db.commit()
        await db.refresh(random_second_user.user, attribute_names=["roles"])
        await create_otr(
            target_id=random_resource.resource_id, new_owner_id=random_second_user.uid, db=db, otr_type=Resource
        )

        response = await client.delete(
            f"{self.base_path}/{random_resource.resource_id}/ownership_transfer_request",
            cookies=random_second_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT

        await db.refresh(random_resource)
        assert random_resource.transfer_created_at is None
        assert random_resource.transfer_comment is None
        assert random_resource.transfer_new_owner_uid is None
        assert random_resource.maintainer_id != random_second_user.uid


class TestResourceRouteList(_TestResourceRoutes):
    @pytest.mark.asyncio
    async def test_list_resources_route(
        self, client: AsyncClient, random_resource: Resource, random_user: UserWithAuthCookie
    ) -> None:
        """
        Test for listing all resources.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowm.models.Resource
            Random resource for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        response = await client.get(self.base_path, cookies=random_user.auth_cookie)
        assert response.status_code == status.HTTP_200_OK
        ta = TypeAdapter(list[ResourceOut])
        resources = ta.validate_json(response.content)
        assert len(resources) == 1

    @pytest.mark.asyncio
    async def test_list_resource_sync_requests_route(
        self, client: AsyncClient, random_resource_version_states: ResourceVersion, random_user: UserWithAuthCookie
    ) -> None:
        """
        Test for listing all resource sync requests.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource_version_states : clowm.models.ResourceVersion
            Random resource version with all possible states for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        response = await client.get(f"{self.base_path}/sync_requests", cookies=random_user.auth_cookie)
        assert response.status_code == status.HTTP_200_OK
        ta = TypeAdapter(list[UserSynchronizationRequestOut])
        requests = ta.validate_json(response.content)
        if random_resource_version_states.status is not ResourceVersion.ResourceVersionStatus.SYNC_REQUESTED:
            assert len(requests) == 0
            return
        assert len(requests) == 1
        request = requests[0]
        assert request.resource_id == random_resource_version_states.resource_id
        assert request.resource_version_id == random_resource_version_states.resource_version_id
        assert request.reason == random_resource_version_states.synchronization_request_description
        assert random_resource_version_states.synchronization_request_uid is not None

    @pytest.mark.asyncio
    async def test_list_resource_otr(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_user: UserWithAuthCookie,
        random_second_user: UserWithAuthCookie,
        db: AsyncSession,
    ) -> None:
        """
        Test for listing the resource OTRs.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowm.models.Resource
            Random resource for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is not the owner of the resource.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        ta = TypeAdapter(list[OwnershipTransferRequestOut])

        response1 = await client.get(f"{self.base_path}/ownership_transfer_request", cookies=random_user.auth_cookie)
        assert response1.status_code == status.HTTP_200_OK
        assert len(ta.validate_json(response1.content)) == 0

        await create_otr(
            target_id=random_resource.resource_id, new_owner_id=random_second_user.uid, db=db, otr_type=Resource
        )

        response2 = await client.get(f"{self.base_path}/ownership_transfer_request", cookies=random_user.auth_cookie)
        assert response2.status_code == status.HTTP_200_OK

        otrs = ta.validate_json(response2.content)
        assert len(otrs) == 1

        otr = otrs[0]
        assert otr.target_id == random_resource.resource_id
        assert otr.comment == ""
        assert otr.new_owner_uid == random_second_user.uid
        assert otr.target_name == random_resource.name
        assert otr.target_description == random_resource.short_description
        assert otr.target_type == OwnershipTypeEnum.RESOURCE


class TestResourceRouteGet(_TestResourceRoutes):
    @pytest.mark.asyncio
    async def test_get_resource_route(
        self, client: AsyncClient, random_resource: Resource, random_user: UserWithAuthCookie
    ) -> None:
        """
        Test for getting a specific resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowm.models.Resource
            Random resource for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        response = await client.get(
            f"{self.base_path}/{str(random_resource.resource_id)}", cookies=random_user.auth_cookie
        )
        assert response.status_code == status.HTTP_200_OK
        resource = ResourceOut.model_validate_json(response.content)
        assert resource.resource_id == random_resource.resource_id

    @pytest.mark.asyncio
    async def test_get_non_existing_resource_route(
        self, client: AsyncClient, random_resource: Resource, random_user: UserWithAuthCookie
    ) -> None:
        """
        Test for getting a non-existing resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
            HTTP Client to perform the request on.
        random_resource : clowm.models.Resource
            Random resource for testing.
        """
        response = await client.get(f"{self.base_path}/{str(uuid4())}", cookies=random_user.auth_cookie)
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_get_resource_otr(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_user: UserWithAuthCookie,
        random_second_user: UserWithAuthCookie,
        db: AsyncSession,
    ) -> None:
        """
        Test for getting a OTR of a resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowm.models.Resource
            Random resource for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is the owner of the resource.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is not the owner of the resource.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        response1 = await client.get(
            f"{self.base_path}/{random_resource.resource_id}/ownership_transfer_request",
            cookies=random_user.auth_cookie,
        )
        assert response1.status_code == status.HTTP_404_NOT_FOUND

        response2 = await client.get(
            f"{self.base_path}/{random_resource.resource_id}/ownership_transfer_request",
            cookies=random_second_user.auth_cookie,
        )
        assert response2.status_code == status.HTTP_403_FORBIDDEN

        comment = random_lower_string(64)
        await create_otr(
            target_id=random_resource.resource_id,
            new_owner_id=random_second_user.uid,
            comment=comment,
            db=db,
            otr_type=Resource,
        )

        response3 = await client.get(
            f"{self.base_path}/{random_resource.resource_id}/ownership_transfer_request",
            cookies=random_user.auth_cookie,
        )
        assert response3.status_code == status.HTTP_200_OK

        otr1 = OwnershipTransferRequestOut.model_validate_json(response3.content)

        assert otr1.target_id == random_resource.resource_id
        assert otr1.comment == comment
        assert otr1.new_owner_uid == random_second_user.uid
        assert otr1.target_name == random_resource.name
        assert otr1.target_description == random_resource.short_description
        assert otr1.target_type == OwnershipTypeEnum.RESOURCE

        response4 = await client.get(
            f"{self.base_path}/{random_resource.resource_id}/ownership_transfer_request",
            cookies=random_second_user.auth_cookie,
        )
        assert response4.status_code == status.HTTP_200_OK
        otr2 = OwnershipTransferRequestOut.model_validate_json(response4.content)
        assert otr1 == otr2


class TestResourceRouteUpdate(_TestResourceRoutes):
    @pytest.mark.asyncio
    async def test_accept_resource_otr(
        self,
        client: AsyncClient,
        random_resource: Resource,
        random_user: UserWithAuthCookie,
        random_second_user: UserWithAuthCookie,
        mock_s3_service: S3ServiceResource,
        random_resource_version_states: ResourceVersion,
        db: AsyncSession,
    ) -> None:
        """
        Test for accepting a resource OTR.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_resource : clowm.models.Resource
            Random resource for testing.
        random_resource_version_states : clowm.models.ResourceVersion
            Random resource version with all possible states for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is owner of the bucket.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is not the owner of the bucket.
        mock_s3_service: types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        # Add DB_MAINTAINER role to random_second_user
        mapping = RoleIdMapping()
        await db.execute(
            insert(UserRoleMapping).values(uid=random_second_user.uid, role_id=mapping[Role.RoleEnum.DB_MAINTAINER])
        )
        await db.commit()
        await db.refresh(random_second_user.user, attribute_names=["roles"])

        response1 = await client.patch(
            f"{self.base_path}/{random_resource.resource_id}/ownership_transfer_request",
            cookies=random_user.auth_cookie,
        )
        assert response1.status_code == status.HTTP_404_NOT_FOUND

        await create_otr(
            target_id=random_resource.resource_id, new_owner_id=random_second_user.uid, db=db, otr_type=Resource
        )

        # Test Success
        response2 = await client.patch(
            f"{self.base_path}/{random_resource.resource_id}/ownership_transfer_request",
            cookies=random_second_user.auth_cookie,
        )
        assert response2.status_code == status.HTTP_200_OK

        await db.refresh(random_resource)
        assert random_resource.transfer_created_at is None
        assert random_resource.transfer_comment is None
        assert random_resource.transfer_new_owner_uid is None
        assert random_resource.maintainer_id == random_second_user.uid
        assert random_resource.last_transfer_timestamp is not None

        # Test if S3 bucket policy was changed
        policy = json.loads(await (await mock_s3_service.BucketPolicy(settings.s3.data_bucket)).policy)

        await db.refresh(random_resource_version_states)
        resource_owner_stmts = [
            stmt
            for stmt in policy["Statement"]
            if stmt["Sid"] == str(random_resource.resource_id)
            or stmt["Sid"] == str(random_resource_version_states.resource_version_id)
        ]
        for stmt in resource_owner_stmts:
            assert str(random_second_user.uid) in stmt["Principal"]["AWS"]
            assert str(random_user.uid) not in stmt["Principal"]["AWS"]
