from datetime import datetime
from uuid import uuid4

import pytest
from fastapi import status
from httpx import AsyncClient
from pydantic import TypeAdapter
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.models import Bucket, BucketPermission
from clowm.schemas.bucket_permission import BucketPermissionIn, BucketPermissionOut
from clowm.schemas.bucket_permission import BucketPermissionParameters as BucketPermissionParametersSchema
from clowm.tests.utils.bucket import add_permission_for_bucket
from clowm.tests.utils.user import UserWithAuthCookie


class _TestBucketPermissionRoutes:
    base_path = "/ui/permissions"


class TestBucketPermissionRoutesGet(_TestBucketPermissionRoutes):
    @pytest.mark.asyncio
    async def test_get_valid_bucket_permission(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_bucket_permission: BucketPermission,
    ) -> None:
        """
        Test for getting a valid bucket permission.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_bucket_permission : clowm.models.BucketPermission
            Bucket permission for a random bucket for testing.
        """
        response = await client.get(
            f"{self.base_path}/bucket/{random_bucket_permission.bucket_name}/user/{str(random_bucket_permission.uid)}",  # noqa:E501
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_200_OK

        permission = BucketPermissionOut.model_validate_json(response.content)

        assert permission
        assert permission.uid == random_bucket_permission.uid
        assert permission.bucket_name == random_bucket_permission.bucket_name

    @pytest.mark.asyncio
    async def test_get_bucket_permission_for_unknown_user(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_bucket_permission: BucketPermission,
    ) -> None:
        """
        Test for getting a bucket permission for an unknown user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_bucket_permission : clowm.models.BucketPermission
            Bucket permission for a random bucket for testing.
        """
        response = await client.get(
            f"{self.base_path}/bucket/{random_bucket_permission.bucket_name}/user/{str(uuid4())}",
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_get_unknown_bucket_permission(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_bucket: Bucket,
        random_second_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for getting a bucket permission for an unknown user/bucket combination.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        """
        response = await client.get(
            f"{self.base_path}/bucket/{random_bucket.name}/user/{str(random_second_user.uid)}",
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_get_foreign_bucket_permission_with_permission(
        self,
        client: AsyncClient,
        random_bucket_permission: BucketPermission,
        random_second_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for getting a bucket permission for a foreign bucket with READ permission for that bucket.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket_permission : clowm.models.BucketPermission
            Bucket permission for a random bucket for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        """
        response = await client.get(
            f"{self.base_path}/bucket/{random_bucket_permission.bucket_name}/user/{str(random_second_user.uid)}",
            cookies=random_second_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_200_OK
        permission = BucketPermissionOut.model_validate_json(response.content)

        assert permission
        assert permission.uid == random_bucket_permission.uid
        assert permission.bucket_name == random_bucket_permission.bucket_name

    @pytest.mark.asyncio
    async def test_get_wrong_bucket_permission_with_permission(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_bucket_permission: BucketPermission,
        random_third_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for getting a bucket permission as a grantee for another grantee for the bucket.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket_permission : clowm.models.BucketPermission
            Bucket permission for a random bucket for testing.
        random_third_user : clowm.tests.utils.user.UserWithAuthCookie
            Random third user who has no permissions for the bucket.
        """
        await add_permission_for_bucket(db, random_bucket_permission.bucket_name, random_third_user.uid)

        response = await client.get(
            f"{self.base_path}/bucket/{random_bucket_permission.bucket_name}/user/{str(random_bucket_permission.uid)}",  # noqa:E501
            cookies=random_third_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.asyncio
    async def test_get_bucket_permissions_for_user(
        self,
        client: AsyncClient,
        random_second_user: UserWithAuthCookie,
        random_bucket_permission: BucketPermission,
    ) -> None:
        """
        Test for getting all bucket permission for a user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        random_bucket_permission : clowm.models.BucketPermission
            Bucket permission for a random bucket for testing.
        """
        response = await client.get(
            f"{self.base_path}/user/{str(random_bucket_permission.uid)}",
            cookies=random_second_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_200_OK

        ta = TypeAdapter(list[BucketPermissionOut])
        permissions = ta.validate_json(response.content)
        assert len(permissions) == 1

        permission = permissions[0]
        assert permission.uid == random_bucket_permission.uid
        assert permission.bucket_name == random_bucket_permission.bucket_name

    @pytest.mark.asyncio
    async def test_get_all_bucket_permissions(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_bucket_permission: BucketPermission,
    ) -> None:
        """
        Test for getting all bucket permission for a user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        random_bucket_permission : clowm.models.BucketPermission
            Bucket permission for a random bucket for testing.
        """
        response = await client.get(self.base_path, cookies=random_user.auth_cookie, params={"allow_admin": True})
        assert response.status_code == status.HTTP_200_OK

        ta = TypeAdapter(list[BucketPermissionOut])
        permissions = ta.validate_json(response.content)
        assert len(permissions) == 1

        permission = permissions[0]
        assert permission.uid == random_bucket_permission.uid
        assert permission.bucket_name == random_bucket_permission.bucket_name

    @pytest.mark.asyncio
    async def test_get_bucket_permissions_for_bucket(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_bucket_permission: BucketPermission,
    ) -> None:
        """
        Test for getting all bucket permission for a bucket.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_bucket_permission : clowm.models.BucketPermission
            Bucket permission for a random bucket for testing.
        """
        response = await client.get(
            f"{self.base_path}/bucket/{random_bucket_permission.bucket_name}",
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_200_OK

        ta = TypeAdapter(list[BucketPermissionOut])
        permissions = ta.validate_json(response.content)
        assert len(permissions) == 1

        permission = permissions[0]
        assert permission.uid == random_bucket_permission.uid
        assert permission.bucket_name == random_bucket_permission.bucket_name

    @pytest.mark.asyncio
    async def test_get_bucket_permissions_for_foreign_bucket(
        self,
        client: AsyncClient,
        random_second_user: UserWithAuthCookie,
        random_bucket_permission: BucketPermission,
    ) -> None:
        """
        Test for getting all bucket permissions for a foreign bucket with READ permission for that bucket.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        random_bucket_permission : clowm.models.BucketPermission
            Bucket permission for a random bucket for testing.
        """
        response = await client.get(
            f"{self.base_path}/bucket/{random_bucket_permission.bucket_name}",
            cookies=random_second_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN


class TestBucketPermissionRoutesCreate(_TestBucketPermissionRoutes):
    @pytest.mark.asyncio
    async def test_create_bucket_permissions_for_unknown_user(
        self, client: AsyncClient, random_user: UserWithAuthCookie, random_bucket: Bucket
    ) -> None:
        """
        Test for creating a bucket permission for an unknown user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        """
        permission = BucketPermissionIn(bucket_name=random_bucket.name, uid=uuid4(), scopes=["read"])
        response = await client.post(
            self.base_path, cookies=random_user.auth_cookie, content=permission.model_dump_json()
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_create_bucket_permissions_for_owner(
        self, client: AsyncClient, random_user: UserWithAuthCookie, random_bucket: Bucket
    ) -> None:
        """
        Test for creating a bucket permission for the owner of the bucket.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        """
        permission = BucketPermissionIn(bucket_name=random_bucket.name, uid=random_user.uid, scopes=["read"])
        response = await client.post(
            self.base_path, cookies=random_user.auth_cookie, content=permission.model_dump_json()
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_create_duplicate_bucket_permissions(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_bucket_permission: BucketPermission,
    ) -> None:
        """
        Test for creating a duplicated bucket permission.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_bucket_permission : clowm.models.BucketPermission
            Bucket permission for a random bucket for testing.
        """
        permission = BucketPermissionIn(
            bucket_name=random_bucket_permission.bucket_name,
            uid=random_bucket_permission.uid,
            scopes=["read"],
        )
        response = await client.post(
            self.base_path, cookies=random_user.auth_cookie, content=permission.model_dump_json()
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_create_bucket_permissions_on_foreign_bucket(
        self,
        client: AsyncClient,
        random_second_user: UserWithAuthCookie,
        random_bucket: Bucket,
    ) -> None:
        """
        Test for creating a valid bucket permission.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        """
        permission = BucketPermissionIn(bucket_name=random_bucket.name, uid=random_second_user.uid, scopes=["read"])

        response = await client.post(
            self.base_path, cookies=random_second_user.auth_cookie, content=permission.model_dump_json()
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.asyncio
    async def test_create_valid_bucket_permissions(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_second_user: UserWithAuthCookie,
        random_bucket: Bucket,
        db: AsyncSession,
    ) -> None:
        """
        Test for creating a valid bucket permission.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        permission = BucketPermissionIn(bucket_name=random_bucket.name, uid=random_second_user.uid, scopes=["read"])

        response = await client.post(
            self.base_path, cookies=random_user.auth_cookie, content=permission.model_dump_json()
        )

        assert response.status_code == status.HTTP_201_CREATED
        created_permission = BucketPermissionOut.model_validate_json(response.content)
        assert created_permission.uid == random_second_user.uid
        assert created_permission.bucket_name == random_bucket.name


class TestBucketPermissionRoutesDelete(_TestBucketPermissionRoutes):
    @pytest.mark.asyncio
    async def test_delete_bucket_permission_from_owner(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_bucket_permission: BucketPermission,
    ) -> None:
        """
        Test for deleting a valid bucket permission as the owner of the bucket.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_bucket_permission : clowm.models.BucketPermission
            Bucket permission for a random bucket for testing.
        """
        response = await client.delete(
            f"{self.base_path}/bucket/{random_bucket_permission.bucket_name}/user/{str(random_bucket_permission.uid)}",  # noqa:E501
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT

    @pytest.mark.asyncio
    async def test_delete_foreign_bucket_permission_with_permission(
        self,
        client: AsyncClient,
        random_second_user: UserWithAuthCookie,
        random_bucket_permission: BucketPermission,
    ) -> None:
        """
        Test for deleting a bucket permission as a grantee.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        random_bucket_permission : clowm.models.BucketPermission
            Bucket permission for a random bucket for testing.
        """
        response = await client.get(
            f"{self.base_path}/bucket/{random_bucket_permission.bucket_name}/user/{str(random_bucket_permission.uid)}",  # noqa:E501
            cookies=random_second_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.asyncio
    async def test_delete_bucket_permission_with_unknown_user(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_bucket_permission: BucketPermission,
    ) -> None:
        """
        Test for deleting a bucket permission as the grantee of the permission.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket_permission : clowm.models.BucketPermission
            Bucket permission for a random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        response = await client.delete(
            f"{self.base_path}/bucket/{random_bucket_permission.bucket_name}/user/{str(uuid4())}",
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_delete_bucket_permission_without_permission(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_bucket: Bucket,
        random_second_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for deleting a bucket permission with an unknown bucket/user combination.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        response = await client.delete(
            f"{self.base_path}/bucket/{random_bucket.name}/user/{str(random_second_user.uid)}",
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_delete_wrong_bucket_permission_with_permission(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_bucket_permission: BucketPermission,
        random_third_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for deleting a bucket permission as a grantee for another grantee for the bucket.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket_permission : clowm.models.BucketPermission
            Bucket permission for a random bucket for testing.
        random_third_user : clowm.tests.utils.user.UserWithAuthCookie
            Random third user who has no permissions for the bucket.
        """
        await add_permission_for_bucket(db, random_bucket_permission.bucket_name, random_third_user.uid)
        response = await client.delete(
            f"{self.base_path}/bucket/{random_bucket_permission.bucket_name}/user/{str(random_bucket_permission.uid)}",  # noqa:E501
            cookies=random_third_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN


class TestBucketPermissionRoutesUpdate(_TestBucketPermissionRoutes):
    @pytest.mark.asyncio
    async def test_update_valid_bucket_permission(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_bucket_permission: BucketPermission,
    ) -> None:
        """
        Test for updating a bucket permission as the owner of the bucket.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket_permission : clowm.models.BucketPermission
            Bucket permission for a random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        new_from_time = round(datetime(2022, 1, 1, 0, 0).timestamp())
        new_params = BucketPermissionParametersSchema(
            from_timestamp=new_from_time,
            to_timestamp=new_from_time + 86400,  # plus one day
            scopes=["read", "write"],
            file_prefix="pseudo/folder/",
        )
        response = await client.put(
            f"{self.base_path}/bucket/{random_bucket_permission.bucket_name}/user/{str(random_bucket_permission.uid)}",  # noqa:E501
            cookies=random_user.auth_cookie,
            content=new_params.model_dump_json(),
        )
        assert response.status_code == status.HTTP_200_OK
        updated_permission = BucketPermissionOut.model_validate_json(response.content)
        assert updated_permission.uid == random_bucket_permission.uid
        assert updated_permission.bucket_name == random_bucket_permission.bucket_name
        if new_params.from_timestamp is not None and new_params.to_timestamp is not None:
            assert updated_permission.from_timestamp == new_params.from_timestamp
            assert updated_permission.to_timestamp == new_params.to_timestamp
        assert updated_permission.scopes == new_params.scopes
        assert updated_permission.file_prefix == new_params.file_prefix

    @pytest.mark.asyncio
    async def test_update_unknown_bucket_permission(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_bucket_permission: BucketPermission,
    ) -> None:
        """
        Test for updating a bucket permission with an unknown user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket_permission : clowm.models.BucketPermission
            Bucket permission for a random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        new_params = BucketPermissionParametersSchema(
            scopes=["read", "write"],
            file_prefix="pseudo/folder/",
        )
        response = await client.put(
            f"{self.base_path}/bucket/{random_bucket_permission.bucket_name}/user/{str(uuid4())}",
            cookies=random_user.auth_cookie,
            content=new_params.model_dump_json(),
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_update_bucket_permission_without_permission(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_bucket: Bucket,
        random_second_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for updating a non-existing bucket permission with a valid user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        new_params = BucketPermissionParametersSchema(
            scopes=["read", "write"],
            file_prefix="pseudo/folder/",
        )
        response = await client.put(
            f"{self.base_path}/bucket/{random_bucket.name}/user/{str(random_second_user.uid)}",
            cookies=random_user.auth_cookie,
            content=new_params.model_dump_json(),
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_update_foreign_bucket_permission_with_permission(
        self,
        client: AsyncClient,
        random_bucket_permission: BucketPermission,
        random_second_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for updating a bucket permission as the grantee of the permission.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket_permission : clowm.models.BucketPermission
            Bucket permission for a random bucket for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        """
        new_params = BucketPermissionParametersSchema(
            scopes=["read", "write"],
            file_prefix="pseudo/folder/",
        )
        response = await client.put(
            f"{self.base_path}/bucket/{random_bucket_permission.bucket_name}/user/{str(random_second_user.uid)}",
            cookies=random_second_user.auth_cookie,
            content=new_params.model_dump_json(),
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.asyncio
    async def test_update_foreign_bucket_permission_without_permission(
        self,
        client: AsyncClient,
        random_bucket_permission: BucketPermission,
        random_third_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for updating a bucket permission as an unrelated third user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket_permission : clowm.models.BucketPermission
            Bucket permission for a random bucket for testing.
        random_third_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        """
        new_params = BucketPermissionParametersSchema(
            scopes=["read", "write"],
            file_prefix="pseudo/folder/",
        )
        response = await client.put(
            f"{self.base_path}/bucket/{random_bucket_permission.bucket_name}/user/{str(random_bucket_permission.uid)}",  # noqa:E501
            cookies=random_third_user.auth_cookie,
            content=new_params.model_dump_json(),
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN
