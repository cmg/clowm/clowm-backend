import pytest
from httpx import AsyncClient

from clowm.api.container_image_check import ContainerImage
from clowm.tests.mocks import MockContainerRegistry
from clowm.tests.utils import CleanupList


class TestContainerImage:
    @pytest.mark.asyncio
    async def test_successful_check(self, mock_client: AsyncClient) -> None:
        """
        Test the process for querying a container registry for the existence of a container image

        Parameters
        ----------
        mock_client : httpx.AsyncClient
            HTTP Client to perform the request on.
        """
        image = ContainerImage.from_string("ubuntu:22.04")
        assert await image.check_existence(mock_client)

    @pytest.mark.asyncio
    async def test_failed_check(
        self, mock_client: AsyncClient, cleanup: CleanupList, mock_container_registry: MockContainerRegistry
    ) -> None:
        """
        Test the process for querying a container registry for the existence of a non-existent container image

        Parameters
        ----------
        mock_client : httpx.AsyncClient
            HTTP Client to perform the request on.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        mock_container_registry: clowm.tests.mocks.mock_container_registry.MockContainerRegistry
            Mock object to temporarily activate errors
        """
        mock_container_registry.send_error = True
        cleanup.add_task(mock_container_registry.reset)
        image = ContainerImage.from_string("ubuntu:22.04")
        assert not await image.check_existence(mock_client)
