import time
from uuid import uuid4

import pytest
from fastapi import status
from httpx import AsyncClient
from pydantic import TypeAdapter
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.models import ApiToken
from clowm.schemas.api_token import ApiTokenIn, ApiTokenOut, ApiTokenPrivateOut
from clowm.tests.utils import UserWithAuthCookie, random_lower_string


class _TestApiTokenRoutes:
    base_path = "/ui/tokens"


class TestApiTokenRoutesGet(_TestApiTokenRoutes):
    @pytest.mark.asyncio
    async def test_get_all_tokens(
        self, client: AsyncClient, random_api_token: ApiToken, random_user: UserWithAuthCookie
    ) -> None:
        """
        Test for getting all Api tokens.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        response = await client.get(self.base_path, cookies=random_user.auth_cookie)
        assert response.status_code == status.HTTP_200_OK

        ta = TypeAdapter(list[ApiTokenOut])
        tokens = ta.validate_json(response.content)
        assert len(tokens) == 1
        token = tokens[0]

        assert token.token_id == random_api_token.token_id
        assert token.name == random_api_token.name
        assert token.uid == random_api_token.uid

    @pytest.mark.asyncio
    async def test_get_all_tokens_filter_by_uid(
        self, client: AsyncClient, random_api_token: ApiToken, random_user: UserWithAuthCookie
    ) -> None:
        """
        Test for getting all Api tokens filtered by uid.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        response = await client.get(
            self.base_path, params={"uid": str(random_user.uid)}, cookies=random_user.auth_cookie
        )
        assert response.status_code == status.HTTP_200_OK

        ta = TypeAdapter(list[ApiTokenOut])
        tokens = ta.validate_json(response.content)
        assert len(tokens) == 1
        token = tokens[0]

        assert token.token_id == random_api_token.token_id
        assert token.name == random_api_token.name
        assert token.uid == random_api_token.uid

    @pytest.mark.asyncio
    async def test_get_token_by_id(
        self, client: AsyncClient, random_api_token: ApiToken, random_user: UserWithAuthCookie
    ) -> None:
        """
        Test for getting an Api token by id.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        response = await client.get(
            f"{self.base_path}/{str(random_api_token.token_id)}", cookies=random_user.auth_cookie
        )
        assert response.status_code == status.HTTP_200_OK

        token = ApiTokenOut.model_validate_json(response.content)

        assert token.token_id == random_api_token.token_id
        assert token.name == random_api_token.name
        assert token.uid == random_api_token.uid

    @pytest.mark.asyncio
    async def test_get_non_existing_token_by_id(self, client: AsyncClient, random_user: UserWithAuthCookie) -> None:
        """
        Test for getting an non-existing Api token by id.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        response = await client.get(f"{self.base_path}/{str(uuid4())}", cookies=random_user.auth_cookie)
        assert response.status_code == status.HTTP_404_NOT_FOUND


class TestApiTokenRoutesDelete(_TestApiTokenRoutes):
    @pytest.mark.asyncio
    async def test_delete_token_by_id(
        self,
        client: AsyncClient,
        random_api_token: ApiToken,
        random_user: UserWithAuthCookie,
        db: AsyncSession,
    ) -> None:
        """
        Test for deleting an Api token by id.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_api_token : clowm.models.ApiToken
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        response = await client.delete(
            f"{self.base_path}/{str(random_api_token.token_id)}", cookies=random_user.auth_cookie
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT

        db_token = await db.scalar(select(ApiToken).where(ApiToken.token_id == random_api_token.token_id))
        assert db_token is None


class TestApiTokenRoutesCreate(_TestApiTokenRoutes):
    @pytest.mark.asyncio
    async def test_create_token(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        db: AsyncSession,
    ) -> None:
        """
        Test for creating an Api token.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        token_in = ApiTokenIn(name=random_lower_string(), expires_at=round(time.time()) + 1000, scopes=["read"])
        response = await client.post(
            self.base_path, cookies=random_user.auth_cookie, content=token_in.model_dump_json()
        )
        assert response.status_code == status.HTTP_201_CREATED
        token_out = ApiTokenPrivateOut.model_validate_json(response.content)
        assert token_out.uid == random_user.uid
        assert token_out.expires_at == token_in.expires_at
        assert token_out.name == token_in.name
        assert token_out.token is not None

        await db.reset()
        db_token = await db.scalar(select(ApiToken).where(ApiToken.token_id == token_out.token_id))
        assert db_token is not None
        assert token_out.token_id == token_out.token_id

    @pytest.mark.asyncio
    async def test_create_token_which_expires_in_past(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        db: AsyncSession,
    ) -> None:
        """
        Test for creating an Api token with an expire date in the past.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        token_in = ApiTokenIn(name=random_lower_string(), scopes=["read"]).model_dump()
        token_in["expires_at"] = round(time.time()) - 1000
        response = await client.post(self.base_path, cookies=random_user.auth_cookie, json=token_in)
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
