import json
from io import BytesIO
from typing import TYPE_CHECKING
from uuid import uuid4

import pytest
from fastapi import status
from httpx import AsyncClient
from pydantic import TypeAdapter
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.api.core.bucket import ANONYMOUS_ACCESS_SID
from clowm.core.config import settings
from clowm.db.mixins.ownership_transfer_mixin import OwnershipTypeEnum
from clowm.models import Bucket, BucketPermission
from clowm.schemas.bucket import BucketIn, BucketOut, BucketSizeLimits
from clowm.schemas.bucket_permission import BucketPermissionOut
from clowm.schemas.ownership_transfer import OwnershipTransferRequestIn, OwnershipTransferRequestOut
from clowm.tests.utils import CleanupList, UserWithAuthCookie, random_lower_string
from clowm.tests.utils.bucket import add_permission_for_bucket, delete_bucket, make_bucket_public
from clowm.tests.utils.otr import create_otr

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object


class _TestBucketRoutes:
    base_path = "/ui/buckets"


class TestBucketRoutesList(_TestBucketRoutes):
    @pytest.mark.asyncio
    async def test_get_all_buckets(
        self, client: AsyncClient, random_bucket: Bucket, random_user: UserWithAuthCookie
    ) -> None:
        """
        Test for getting all buckets with "list_all" operation.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        response = await client.get(self.base_path, cookies=random_user.auth_cookie)
        assert response.status_code == status.HTTP_200_OK

        ta = TypeAdapter(list[BucketOut])
        buckets = ta.validate_json(response.content)
        assert len(buckets) == 1
        bucket = buckets[0]

        assert bucket.name == random_bucket.name
        assert bucket.owner_id == random_bucket.owner_id

    @pytest.mark.asyncio
    async def test_get_own_buckets(
        self,
        client: AsyncClient,
        random_bucket: Bucket,
        random_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for getting the buckets where the user is the owner.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        response = await client.get(
            self.base_path, params={"owner_id": str(random_bucket.owner_id)}, cookies=random_user.auth_cookie
        )
        assert response.status_code == status.HTTP_200_OK

        ta = TypeAdapter(list[BucketOut])
        buckets = ta.validate_json(response.content)

        assert len(buckets) == 1
        bucket = buckets[0]

        assert bucket.name == random_bucket.name
        assert bucket.owner_id == random_bucket.owner_id

    @pytest.mark.asyncio
    async def test_list_bucket_otr(
        self,
        client: AsyncClient,
        random_bucket: Bucket,
        random_user: UserWithAuthCookie,
        random_second_user: UserWithAuthCookie,
        db: AsyncSession,
    ) -> None:
        """
        Test for listing the bucket OTRs.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is not the owner of the bucket.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        ta = TypeAdapter(list[OwnershipTransferRequestOut])

        response1 = await client.get(f"{self.base_path}/ownership_transfer_request", cookies=random_user.auth_cookie)
        assert response1.status_code == status.HTTP_200_OK
        assert len(ta.validate_json(response1.content)) == 0

        await create_otr(target_id=random_bucket.name, new_owner_id=random_second_user.uid, db=db, otr_type=Bucket)

        response2 = await client.get(f"{self.base_path}/ownership_transfer_request", cookies=random_user.auth_cookie)
        assert response2.status_code == status.HTTP_200_OK

        otrs = ta.validate_json(response2.content)
        assert len(otrs) == 1

        otr = otrs[0]
        assert otr.target_id == random_bucket.name
        assert otr.comment == ""
        assert otr.new_owner_uid == random_second_user.uid
        assert otr.target_name == random_bucket.name
        assert otr.target_description == random_bucket.description
        assert otr.target_type == OwnershipTypeEnum.BUCKET


class TestBucketRoutesGet(_TestBucketRoutes):
    @pytest.mark.asyncio
    async def test_get_bucket_by_name(
        self,
        client: AsyncClient,
        random_bucket: Bucket,
        random_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for getting a bucket by its name where the user is the owner of the bucket.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        response = await client.get(f"{self.base_path}/{random_bucket.name}", cookies=random_user.auth_cookie)
        assert response.status_code == status.HTTP_200_OK

        bucket = BucketOut.model_validate_json(response.content)

        assert bucket.name == random_bucket.name
        assert bucket.owner_id == random_bucket.owner_id

    @pytest.mark.asyncio
    async def test_get_unknown_bucket(self, client: AsyncClient, random_user: UserWithAuthCookie) -> None:
        """
        Test for getting an unknown bucket by its name.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        response = await client.get(f"{self.base_path}/impossible_bucket_name", cookies=random_user.auth_cookie)
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_get_foreign_bucket(
        self, client: AsyncClient, random_bucket: Bucket, random_second_user: UserWithAuthCookie
    ) -> None:
        """
        Test for getting a foreign bucket without permission.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is not the owner of the bucket.
        """
        response = await client.get(f"{self.base_path}/{random_bucket.name}", cookies=random_second_user.auth_cookie)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.asyncio
    async def test_get_foreign_public_bucket(
        self,
        client: AsyncClient,
        random_bucket: Bucket,
        random_second_user: UserWithAuthCookie,
        db: AsyncSession,
        mock_s3_service: S3ServiceResource,
    ) -> None:
        """
        Test for getting a foreign public bucket.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is not the owner of the bucket.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        mock_s3_service: types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        """
        await make_bucket_public(db=db, s3=mock_s3_service, bucket_name=random_bucket.name)
        response = await client.get(f"{self.base_path}/{random_bucket.name}", cookies=random_second_user.auth_cookie)
        assert response.status_code == status.HTTP_200_OK

        bucket = BucketOut.model_validate_json(response.content)

        assert bucket.name == random_bucket.name

    @pytest.mark.asyncio
    async def test_get_bucket_otr(
        self,
        client: AsyncClient,
        random_bucket: Bucket,
        random_user: UserWithAuthCookie,
        random_second_user: UserWithAuthCookie,
        db: AsyncSession,
    ) -> None:
        """
        Test for getting a OTR of a bucket.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is the owner of the bucket.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is not the owner of the bucket.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        response1 = await client.get(
            f"{self.base_path}/{random_bucket.name}/ownership_transfer_request", cookies=random_user.auth_cookie
        )
        assert response1.status_code == status.HTTP_404_NOT_FOUND

        response2 = await client.get(
            f"{self.base_path}/{random_bucket.name}/ownership_transfer_request", cookies=random_second_user.auth_cookie
        )
        assert response2.status_code == status.HTTP_403_FORBIDDEN

        comment = random_lower_string(64)
        await create_otr(
            target_id=random_bucket.name, new_owner_id=random_second_user.uid, comment=comment, db=db, otr_type=Bucket
        )

        response3 = await client.get(
            f"{self.base_path}/{random_bucket.name}/ownership_transfer_request", cookies=random_user.auth_cookie
        )
        assert response3.status_code == status.HTTP_200_OK

        otr1 = OwnershipTransferRequestOut.model_validate_json(response3.content)

        assert otr1.target_id == random_bucket.name
        assert otr1.comment == comment
        assert otr1.new_owner_uid == random_second_user.uid
        assert otr1.target_name == random_bucket.name
        assert otr1.target_description == random_bucket.description
        assert otr1.target_type == OwnershipTypeEnum.BUCKET

        response4 = await client.get(
            f"{self.base_path}/{random_bucket.name}/ownership_transfer_request", cookies=random_second_user.auth_cookie
        )
        assert response4.status_code == status.HTTP_200_OK
        otr2 = OwnershipTransferRequestOut.model_validate_json(response4.content)
        assert otr1 == otr2


class TestBucketRoutesCreate(_TestBucketRoutes):
    @pytest.mark.asyncio
    async def test_create_bucket(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for creating a bucket.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        bucket_info = BucketIn(name=random_lower_string(), description=random_lower_string(127))
        cleanup.add_task(
            delete_bucket,
            db=db,
            bucket_name=bucket_info.name,
        )
        response = await client.post(
            self.base_path, cookies=random_user.auth_cookie, content=bucket_info.model_dump_json()
        )
        assert response.status_code == status.HTTP_201_CREATED
        await db.reset()
        bucket = BucketOut.model_validate_json(response.content)
        assert bucket.name == bucket_info.name
        assert bucket.owner_id == random_user.uid
        assert bucket.size_limit == settings.s3.default_bucket_size_limit.to("KiB")
        assert bucket.object_limit == settings.s3.default_bucket_object_limit

        db_bucket = await db.scalar(select(Bucket).where(Bucket.name == bucket_info.name))
        assert db_bucket is not None
        assert db_bucket.name == bucket_info.name
        assert db_bucket.owner_id == random_user.uid

    @pytest.mark.asyncio
    async def test_create_duplicated_bucket(
        self,
        client: AsyncClient,
        random_bucket: Bucket,
        random_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for creating a bucket where the name is already taken.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        bucket_info = BucketIn(name=random_bucket.name, description=random_lower_string(127))
        response = await client.post(
            self.base_path, cookies=random_user.auth_cookie, content=bucket_info.model_dump_json()
        )

        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_create_bucket_otr(
        self,
        client: AsyncClient,
        random_bucket: Bucket,
        random_user: UserWithAuthCookie,
        random_second_user: UserWithAuthCookie,
        db: AsyncSession,
    ) -> None:
        """
        Test for creating a bucket OTR.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is the owner of the bucket.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is not the owner of the bucket.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        # Test Fail: User is not allowed to create a OTR
        response1 = await client.post(
            f"{self.base_path}/{random_bucket.name}/ownership_transfer_request",
            cookies=random_second_user.auth_cookie,
            content=OwnershipTransferRequestIn(new_owner_uid=random_second_user.uid).model_dump_json(),
        )

        assert response1.status_code == status.HTTP_403_FORBIDDEN
        await db.reset()
        # Test Fail: New owner does not exist
        response2 = await client.post(
            f"{self.base_path}/{random_bucket.name}/ownership_transfer_request",
            cookies=random_user.auth_cookie,
            content=OwnershipTransferRequestIn(new_owner_uid=uuid4()).model_dump_json(),
        )

        assert response2.status_code == status.HTTP_400_BAD_REQUEST

        # Test Fail: New and current owner are the same
        response3 = await client.post(
            f"{self.base_path}/{random_bucket.name}/ownership_transfer_request",
            cookies=random_user.auth_cookie,
            content=OwnershipTransferRequestIn(new_owner_uid=random_bucket.owner_id).model_dump_json(),
        )

        assert response3.status_code == status.HTTP_400_BAD_REQUEST

        # Test success
        otr_in = OwnershipTransferRequestIn(new_owner_uid=random_second_user.uid, comment=random_lower_string(64))
        response4 = await client.post(
            f"{self.base_path}/{random_bucket.name}/ownership_transfer_request",
            cookies=random_user.auth_cookie,
            content=otr_in.model_dump_json(),
        )

        assert response4.status_code == status.HTTP_201_CREATED
        otr_out = OwnershipTransferRequestOut.model_validate_json(response4.content)

        assert otr_out.new_owner_uid == otr_in.new_owner_uid
        assert otr_out.comment == otr_in.comment
        assert otr_out.target_description == random_bucket.description
        assert otr_out.target_name == random_bucket.name
        assert otr_out.target_id == random_bucket.name
        assert otr_out.target_type == OwnershipTypeEnum.BUCKET

        selected_bucket = await db.scalar(select(Bucket).where(Bucket.name == random_bucket.name))
        assert selected_bucket is not None
        assert selected_bucket.transfer_created_at is not None
        assert selected_bucket.transfer_comment == otr_in.comment
        assert selected_bucket.transfer_new_owner_uid == random_second_user.uid


class TestBucketRoutesUpdate(_TestBucketRoutes):
    @pytest.mark.asyncio
    async def test_make_bucket_public(
        self,
        client: AsyncClient,
        random_bucket: Bucket,
        random_user: UserWithAuthCookie,
        mock_s3_service: S3ServiceResource,
    ) -> None:
        """
        Test for getting a foreign public bucket.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is owner of the bucket.
        mock_s3_service: types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        """
        response = await client.patch(
            f"{self.base_path}/{random_bucket.name}/public", cookies=random_user.auth_cookie, json={"public": True}
        )
        assert response.status_code == status.HTTP_200_OK

        bucket = BucketOut.model_validate_json(response.content)

        assert bucket.name == random_bucket.name
        assert bucket.public

        s3_policy = await mock_s3_service.BucketPolicy(bucket.name)
        assert ANONYMOUS_ACCESS_SID in await s3_policy.policy

    @pytest.mark.asyncio
    async def test_update_bucket_limits(
        self,
        client: AsyncClient,
        random_bucket: Bucket,
        random_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for getting a foreign public bucket.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is owner of the bucket.
        """
        response = await client.patch(
            f"{self.base_path}/{random_bucket.name}/limits",
            cookies=random_user.auth_cookie,
            content=BucketSizeLimits(size_limit=10240, object_limit=1000).model_dump_json(),
        )
        assert response.status_code == status.HTTP_200_OK

        bucket = BucketOut.model_validate_json(response.content)

        assert bucket.name == random_bucket.name
        assert bucket.size_limit is not None
        assert bucket.size_limit == 10240

        assert bucket.object_limit is not None
        assert bucket.object_limit == 1000

    @pytest.mark.asyncio
    async def test_make_bucket_private(
        self,
        client: AsyncClient,
        random_bucket: Bucket,
        random_user: UserWithAuthCookie,
        mock_s3_service: S3ServiceResource,
        db: AsyncSession,
    ) -> None:
        """
        Test for getting a foreign public bucket.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is owner of the bucket.
        mock_s3_service: types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        await make_bucket_public(db=db, s3=mock_s3_service, bucket_name=random_bucket.name)
        response = await client.patch(
            f"{self.base_path}/{random_bucket.name}/public", cookies=random_user.auth_cookie, json={"public": False}
        )
        assert response.status_code == status.HTTP_200_OK

        bucket = BucketOut.model_validate_json(response.content)

        assert bucket.name == random_bucket.name
        assert not bucket.public

        s3_policy = await mock_s3_service.BucketPolicy(bucket.name)
        assert ANONYMOUS_ACCESS_SID not in await s3_policy.policy

    @pytest.mark.asyncio
    async def test_make_private_bucket_private(
        self,
        client: AsyncClient,
        random_bucket: Bucket,
        random_user: UserWithAuthCookie,
        mock_s3_service: S3ServiceResource,
        db: AsyncSession,
    ) -> None:
        """
        Test for getting a foreign public bucket.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is owner of the bucket.
        mock_s3_service: types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        response = await client.patch(
            f"{self.base_path}/{random_bucket.name}/public", cookies=random_user.auth_cookie, json={"public": False}
        )
        assert response.status_code == status.HTTP_200_OK

        bucket = BucketOut.model_validate_json(response.content)

        assert bucket.name == random_bucket.name
        assert not bucket.public

        s3_policy = await mock_s3_service.BucketPolicy(bucket.name)
        assert ANONYMOUS_ACCESS_SID not in await s3_policy.policy

    @pytest.mark.asyncio
    async def test_accept_bucket_otr(
        self,
        client: AsyncClient,
        random_bucket: Bucket,
        random_user: UserWithAuthCookie,
        random_second_user: UserWithAuthCookie,
        mock_s3_service: S3ServiceResource,
        random_bucket_permission: BucketPermission,
        db: AsyncSession,
    ) -> None:
        """
        Test for accepting a bucket OTR.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is owner of the bucket.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is not the owner of the bucket.
        mock_s3_service: types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        random_bucket_permission : clowm.models.BucketPermission
            Bucket permission for the random bucket for testing.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        response1 = await client.patch(
            f"{self.base_path}/{random_bucket.name}/ownership_transfer_request", cookies=random_user.auth_cookie
        )
        assert response1.status_code == status.HTTP_404_NOT_FOUND

        await create_otr(target_id=random_bucket.name, new_owner_id=random_second_user.uid, db=db, otr_type=Bucket)

        # Test Success
        response2 = await client.patch(
            f"{self.base_path}/{random_bucket.name}/ownership_transfer_request", cookies=random_second_user.auth_cookie
        )
        assert response2.status_code == status.HTTP_200_OK

        await db.refresh(random_bucket)
        assert random_bucket.transfer_created_at is None
        assert random_bucket.transfer_comment is None
        assert random_bucket.transfer_new_owner_uid is None
        assert random_bucket.owner_id == random_second_user.uid
        assert random_bucket.last_transfer_timestamp is not None

        # Assert bucket permission was deleted
        assert (
            await db.scalar(
                select(BucketPermission).where(
                    BucketPermission.bucket_name == random_bucket.name, BucketPermission.uid == random_second_user.uid
                )
            )
            is None
        )

        # Test if S3 bucket policy was changed
        policy = json.loads(await (await mock_s3_service.BucketPolicy(bucket_name=random_bucket.name)).policy)
        assert BucketPermissionOut(
            uid=random_second_user.uid, bucket_name=random_bucket.name, scopes=["read"]
        ).to_hash() not in (stmt["Sid"] for stmt in policy["Statement"])

        pseudo_owner_stmts = [stmt for stmt in policy["Statement"] if stmt["Sid"] == "PseudoOwnerPerm"]
        for stmt in pseudo_owner_stmts:
            assert str(random_second_user.uid) in stmt["Principal"]["AWS"]
            assert str(random_user.uid) not in stmt["Principal"]["AWS"]


class TestBucketRoutesDelete(_TestBucketRoutes):
    @pytest.mark.asyncio
    async def test_delete_empty_bucket(
        self,
        client: AsyncClient,
        random_bucket: Bucket,
        random_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for deleting an empty bucket.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        response = await client.delete(
            f"{self.base_path}/{random_bucket.name}", cookies=random_user.auth_cookie, params={"force_delete": False}
        )

        assert response.status_code == status.HTTP_204_NO_CONTENT

    @pytest.mark.asyncio
    async def test_delete_foreign_bucket_with_permission(
        self,
        client: AsyncClient,
        db: AsyncSession,
        random_second_user: UserWithAuthCookie,
        random_third_user: UserWithAuthCookie,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for deleting a foreign bucket.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_third_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is not the owner of the bucket.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        bucket = Bucket(
            name=random_lower_string(),
            description=random_lower_string(127),
            owner_id=random_second_user.uid,
        )
        db.add(bucket)
        await db.commit()
        cleanup.add_task(
            delete_bucket,
            db=db,
            bucket_name=bucket.name,
        )
        await add_permission_for_bucket(db, bucket.name, random_third_user.uid)

        response = await client.delete(
            f"{self.base_path}/{bucket.name}", cookies=random_third_user.auth_cookie, params={"force_delete": False}
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.asyncio
    async def test_delete_non_empty_bucket(
        self,
        client: AsyncClient,
        random_bucket: Bucket,
        random_user: UserWithAuthCookie,
        mock_s3_service: S3ServiceResource,
    ) -> None:
        """
        Test for deleting a non-empty bucket.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        """
        obj = await mock_s3_service.Object(bucket_name=random_bucket.name, key=random_lower_string())
        await obj.upload_fileobj(BytesIO(b""))
        response = await client.delete(
            f"{self.base_path}/{random_bucket.name}", cookies=random_user.auth_cookie, params={"force_delete": False}
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_force_delete_non_empty_bucket(
        self,
        client: AsyncClient,
        random_bucket: Bucket,
        random_user: UserWithAuthCookie,
        mock_s3_service: S3ServiceResource,
    ) -> None:
        """
        Test for force deleting a non-empty bucket.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        """
        obj = await mock_s3_service.Object(bucket_name=random_bucket.name, key=random_lower_string())
        await obj.upload_fileobj(BytesIO(b""))
        response = await client.delete(
            f"{self.base_path}/{random_bucket.name}", cookies=random_user.auth_cookie, params={"force_delete": True}
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT

    @pytest.mark.asyncio
    async def test_delete_bucket_otr(
        self,
        client: AsyncClient,
        random_bucket: Bucket,
        random_user: UserWithAuthCookie,
        random_second_user: UserWithAuthCookie,
        db: AsyncSession,
    ) -> None:
        """
        Test for deleting a bucket OTR.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is the owner of the bucket.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is not the owner of the bucket.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        # Test Fail: OTR does not exist
        response1 = await client.delete(
            f"{self.base_path}/{random_bucket.name}/ownership_transfer_request",
            cookies=random_user.auth_cookie,
        )
        assert response1.status_code == status.HTTP_404_NOT_FOUND

        await create_otr(target_id=random_bucket.name, new_owner_id=random_second_user.uid, db=db, otr_type=Bucket)

        # Test Success
        response2 = await client.delete(
            f"{self.base_path}/{random_bucket.name}/ownership_transfer_request",
            cookies=random_user.auth_cookie,
        )
        assert response2.status_code == status.HTTP_204_NO_CONTENT

        await db.refresh(random_bucket)
        assert random_bucket.transfer_created_at is None
        assert random_bucket.transfer_comment is None
        assert random_bucket.transfer_new_owner_uid is None
        assert random_bucket.owner_id == random_user.uid

    @pytest.mark.asyncio
    async def test_reject_bucket_otr(
        self,
        client: AsyncClient,
        random_bucket: Bucket,
        random_second_user: UserWithAuthCookie,
        db: AsyncSession,
    ) -> None:
        """
        Test for rejecting a bucket OTR.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_bucket : clowm.models.Bucket
            Random bucket for testing.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user who is not the owner of the bucket.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        await create_otr(target_id=random_bucket.name, new_owner_id=random_second_user.uid, db=db, otr_type=Bucket)

        response = await client.delete(
            f"{self.base_path}/{random_bucket.name}/ownership_transfer_request",
            cookies=random_second_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT

        await db.refresh(random_bucket)
        assert random_bucket.transfer_created_at is None
        assert random_bucket.transfer_comment is None
        assert random_bucket.transfer_new_owner_uid is None
        assert random_bucket.owner_id != random_second_user.uid
