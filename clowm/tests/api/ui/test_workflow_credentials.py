from io import BytesIO
from typing import TYPE_CHECKING

import pytest
from botocore.client import ClientError
from fastapi import status
from httpx import AsyncClient
from sqlalchemy import select, update
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.core.config import settings
from clowm.models import Workflow
from clowm.schemas.workflow import WorkflowCredentialsIn, WorkflowCredentialsOut, WorkflowOut
from clowm.scm import SCM
from clowm.tests.utils import CleanupList, UserWithAuthCookie, random_lower_string

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object


class _TestWorkflowCredentialRoutes:
    base_path: str = "/ui/workflows"


class TestWorkflowCredentialsRoutesUpdate(_TestWorkflowCredentialRoutes):
    @pytest.mark.asyncio
    async def test_update_workflow_credentials_on_public_workflow(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow: WorkflowOut,
        mock_s3_service: S3ServiceResource,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for updating the credentials on a workflow formerly hosted in a public git repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        mock_s3_service: types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        credentials = WorkflowCredentialsIn(token=random_lower_string(15))
        response = await client.put(
            "/".join([self.base_path, str(random_workflow.workflow_id), "credentials"]),
            content=credentials.model_dump_json(),
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_200_OK

        stmt = select(Workflow).where(Workflow.workflow_id == random_workflow.workflow_id)
        db_workflow = await db.scalar(stmt)
        assert db_workflow is not None

        assert db_workflow.credentials_token == credentials.token

        scm_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket, key=settings.s3.scm_key(db_workflow.workflow_id)
        )
        cleanup.add_task(scm_obj.delete)
        with BytesIO() as f:
            await scm_obj.download_fileobj(f)
            f.seek(0)
            scm = SCM.deserialize(f)

        assert len(scm.providers) == 1
        assert scm.providers[0].password == credentials.token

    @pytest.mark.asyncio
    async def test_update_workflow_credentials_on_private_workflow(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_private_workflow: WorkflowOut,
        mock_s3_service: S3ServiceResource,
    ) -> None:
        """
        Test for updating the credentials on a workflow hosted in a private git repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random second user for testing.
        mock_s3_service: types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        random_private_workflow : clowm.schemas.workflow.WorkflowOut
            Random private workflow for testing.
        """
        credentials = WorkflowCredentialsIn(token=random_lower_string(15))
        response = await client.put(
            "/".join([self.base_path, str(random_private_workflow.workflow_id), "credentials"]),
            content=credentials.model_dump_json(),
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_200_OK

        stmt = select(Workflow).where(Workflow.workflow_id == random_private_workflow.workflow_id)
        db_workflow = await db.scalar(stmt)
        assert db_workflow is not None

        assert db_workflow.credentials_token == credentials.token

        scm_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket, key=settings.s3.scm_key(db_workflow.workflow_id)
        )
        with BytesIO() as f:
            await scm_obj.download_fileobj(f)
            f.seek(0)
            scm = SCM.deserialize(f)

        assert len(scm.providers) == 1
        assert scm.providers[0].password == credentials.token

    @pytest.mark.asyncio
    async def test_update_workflow_credentials(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_private_workflow: WorkflowOut,
        mock_s3_service: S3ServiceResource,
    ) -> None:
        """
        Test for updating the credentials on a workflow hosted in a private git repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        mock_s3_service: types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        random_private_workflow : clowm.schemas.workflow.WorkflowOut
            Random private workflow for testing.
        """
        credentials = WorkflowCredentialsIn(token=random_lower_string(15))
        response = await client.put(
            "/".join([self.base_path, str(random_private_workflow.workflow_id), "credentials"]),
            content=credentials.model_dump_json(),
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_200_OK

        stmt = select(Workflow).where(Workflow.workflow_id == random_private_workflow.workflow_id)
        db_workflow = await db.scalar(stmt)
        assert db_workflow is not None

        assert db_workflow.credentials_token == credentials.token

        scm_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket, key=settings.s3.scm_key(db_workflow.workflow_id)
        )
        with BytesIO() as f:
            await scm_obj.download_fileobj(f)
            f.seek(0)
            scm = SCM.deserialize(f)

        assert len(scm.providers) == 1
        assert scm.providers[0].password == credentials.token

    @pytest.mark.asyncio
    async def test_update_workflow_credentials_on_foreign_workflow(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_second_user: UserWithAuthCookie,
        random_private_workflow: WorkflowOut,
        mock_s3_service: S3ServiceResource,
    ) -> None:
        """
        Test for updating the credentials on a workflow hosted in a private git repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        mock_s3_service: types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        random_private_workflow : clowm.schemas.workflow.WorkflowOut
            Random private workflow for testing.
        """
        credentials = WorkflowCredentialsIn(token=random_lower_string(15))
        response = await client.put(
            "/".join([self.base_path, str(random_private_workflow.workflow_id), "credentials"]),
            content=credentials.model_dump_json(),
            cookies=random_second_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN


class TestWorkflowCredentialsRoutesDelete(_TestWorkflowCredentialRoutes):
    @pytest.mark.asyncio
    async def test_delete_workflow_credentials_on_public_workflow(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow: WorkflowOut,
        mock_s3_service: S3ServiceResource,
    ) -> None:
        """
        Test for deleting the credentials on a workflow hosted in a public GitHub repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        mock_s3_service: types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.delete(
            "/".join([self.base_path, str(random_workflow.workflow_id), "credentials"]),
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT

        stmt = select(Workflow).where(Workflow.workflow_id == random_workflow.workflow_id)
        db_workflow = await db.scalar(stmt)
        assert db_workflow is not None

        assert db_workflow.credentials_token is None

        with pytest.raises(ClientError):
            scm_obj = await mock_s3_service.Object(
                bucket_name=settings.s3.data_bucket, key=settings.s3.scm_key(db_workflow.workflow_id)
            )
            await scm_obj.load()

    @pytest.mark.asyncio
    async def test_delete_workflow_credentials_on_private_github_workflow(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_private_workflow: WorkflowOut,
        mock_s3_service: S3ServiceResource,
    ) -> None:
        """
        Test for deleting the credentials on a workflow formerly hosted in a private GitHub repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        random_private_workflow : clowm.schemas.workflow.WorkflowOut
            Random private workflow for testing.
        """
        response = await client.delete(
            "/".join([self.base_path, str(random_private_workflow.workflow_id), "credentials"]),
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT

        stmt = select(Workflow).where(Workflow.workflow_id == random_private_workflow.workflow_id)
        db_workflow = await db.scalar(stmt)
        assert db_workflow is not None

        assert db_workflow.credentials_token is None

        with pytest.raises(ClientError):
            scm_obj = await mock_s3_service.Object(
                bucket_name=settings.s3.data_bucket, key=settings.s3.scm_key(db_workflow.workflow_id)
            )
            await scm_obj.load()

    @pytest.mark.asyncio
    async def test_delete_workflow_credentials_on_gitlab_workflow(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow: Workflow,
        mock_s3_service: S3ServiceResource,
    ) -> None:
        """
        Test for deleting the credentials on a workflow hosted in a public GitLab repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        mock_s3_service : types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service to manipulate objects.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """

        stmt = (
            update(Workflow)
            .where(Workflow.workflow_id == random_workflow.workflow_id)
            .values(repository_url="https://gitlab.com/example/example")
        )
        await db.execute(stmt)
        await db.commit()

        response = await client.delete(
            "/".join([self.base_path, str(random_workflow.workflow_id), "credentials"]),
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT

        scm_obj = await mock_s3_service.Object(
            bucket_name=settings.s3.data_bucket, key=settings.s3.scm_key(random_workflow.workflow_id)
        )
        with BytesIO() as f:
            await scm_obj.download_fileobj(f)
            f.seek(0)
            scm = SCM.deserialize(f)

        assert len(scm.providers) == 1
        assert scm.providers[0].password is None


class TestWorkflowCredentialsRoutesGet(_TestWorkflowCredentialRoutes):
    @pytest.mark.asyncio
    async def test_get_workflow_credentials_of_public_workflow(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_workflow: WorkflowOut,
    ) -> None:
        """
        Test for getting the credentials of a public workflow as the developer.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_workflow : clowm.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.get(
            "/".join([self.base_path, str(random_workflow.workflow_id), "credentials"]),
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_200_OK
        cred = WorkflowCredentialsOut.model_validate_json(response.content)
        assert cred.token is None

    @pytest.mark.asyncio
    async def test_get_workflow_credentials_of_private_workflow(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
        random_private_workflow: WorkflowOut,
    ) -> None:
        """
        Test for getting the credentials of a private workflow as the developer.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_private_workflow : clowm.schemas.workflow.WorkflowOut
            Random private workflow for testing.
        """
        response = await client.get(
            "/".join([self.base_path, str(random_private_workflow.workflow_id), "credentials"]),
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_200_OK

        stmt = select(Workflow).where(Workflow.workflow_id == random_private_workflow.workflow_id)
        db_workflow = await db.scalar(stmt)
        assert db_workflow is not None
        cred = WorkflowCredentialsOut.model_validate_json(response.content)
        assert cred.token == db_workflow.credentials_token

    @pytest.mark.asyncio
    async def test_get_workflow_credentials_as_foreign_user(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_second_user: UserWithAuthCookie,
        random_private_workflow: WorkflowOut,
    ) -> None:
        """
        Test for getting the credentials on a workflow as a user who is not the developer.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_second_user : clowm.tests.utils.user.UserWithAuthCookie
            Random user for testing.
        random_private_workflow : clowm.schemas.workflow.WorkflowOut
            Random private workflow for testing.
        """
        response = await client.get(
            "/".join([self.base_path, str(random_private_workflow.workflow_id), "credentials"]),
            cookies=random_second_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN
