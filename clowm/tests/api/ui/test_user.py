import random
import time
import uuid
from secrets import token_urlsafe
from typing import TYPE_CHECKING

import pytest
from fastapi import status
from httpx import AsyncClient
from pydantic import TypeAdapter
from sqlalchemy import delete, select, update
from sqlalchemy.ext.asyncio import AsyncSession

from clowm.models import BucketPermission, Role, User
from clowm.schemas.user import UserIn, UserOut, UserOutExtended, UserRoles
from clowm.tests.mocks import MockRGWAdmin
from clowm.tests.utils import CleanupList, UserWithAuthCookie, random_lower_string

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object


class _TestUserRoutes:
    base_path = "/ui/users"


UserList = TypeAdapter(list[UserOut])
ExtendedUserList = TypeAdapter(list[UserOutExtended])


class TestUserRoutesGet(_TestUserRoutes):
    @pytest.mark.asyncio
    async def test_get_user_me(self, client: AsyncClient, random_user: UserWithAuthCookie) -> None:
        """
        Test for getting the currently logged-in user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.test.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        response = await client.get(f"{self.base_path}/me", cookies=random_user.auth_cookie)
        assert response.status_code == status.HTTP_200_OK
        current_user = UserOutExtended.model_validate_json(response.content)
        assert current_user.uid == random_user.uid
        assert current_user.display_name == random_user.user.display_name
        assert current_user.roles is not None
        assert current_user.email == current_user.email

    @pytest.mark.asyncio
    async def test_get_unknown_user(self, client: AsyncClient, random_user: UserWithAuthCookie) -> None:
        """
        Test for getting an unknown user by its uid.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.test.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        response = await client.get(f"{self.base_path}/{uuid.uuid4()}", cookies=random_user.auth_cookie)
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_get_user_by_uid(self, client: AsyncClient, random_user: UserWithAuthCookie) -> None:
        """
        Test for getting a known user by its uid.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.test.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        response = await client.get(
            f"{self.base_path}/{str(random_user.uid)}",
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_200_OK
        current_user = UserOut.model_validate_json(response.content)
        assert current_user.uid == random_user.uid
        assert current_user.display_name == random_user.user.display_name


class TestUserRoutesSearch(_TestUserRoutes):
    @pytest.mark.asyncio
    async def test_search_user_by_name_substring(self, client: AsyncClient, random_user: UserWithAuthCookie) -> None:
        """
        Test for searching a user by its name

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.test.utils.user.UserWithAuthCookie
            Random user for testing.
        """
        substring_indices = [0, 0]
        while substring_indices[1] - substring_indices[0] < 3:
            substring_indices = sorted(random.choices(range(len(random_user.user.display_name)), k=2))

        random_substring = random_user.user.display_name[substring_indices[0] : substring_indices[1]]

        response = await client.get(
            f"{self.base_path}/search", params={"name_substring": random_substring}, cookies=random_user.auth_cookie
        )
        assert response.status_code == status.HTTP_200_OK
        users = UserList.validate_json(response.content)

        assert len(users) > 0
        assert sum(1 for u in users if u.uid == random_user.uid) == 1


class TestUserRoutesList(_TestUserRoutes):
    @pytest.mark.asyncio
    async def test_list_all_user(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for listing all user in the system.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.test.utils.user.UserWithAuthCookie
            Random user for testing.
        """

        response = await client.get(self.base_path, cookies=random_user.auth_cookie)
        assert response.status_code == status.HTTP_200_OK

        users = ExtendedUserList.validate_json(response.content)
        assert len(users) > 0
        assert sum(1 for u in users if u.uid == random_user.uid) == 1
        for u in users:
            assert u.email is not None

    @pytest.mark.asyncio
    async def test_list_user_with_role_filter(
        self,
        client: AsyncClient,
        random_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for list and filter users by their role.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.test.utils.user.UserWithAuthCookie
            Random user for testing.
        """

        response = await client.get(
            self.base_path,
            params={"filter_roles": Role.RoleEnum.USER.value},
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_200_OK

        users = ExtendedUserList.validate_json(response.content)
        assert len(users) > 0
        assert sum(1 for u in users if u.uid == random_user.uid) == 1
        for u in users:
            assert u.roles is not None
            assert Role.RoleEnum.USER.value in u.roles

    @pytest.mark.asyncio
    async def test_list_users_with_name_substring(self, client: AsyncClient, random_user: UserWithAuthCookie) -> None:
        """
        Test for listing a user by its name

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.test.utils.user.UserWithAuthCookie
            Random user for testing.
        """

        substring_indices = [0, 0]
        while substring_indices[1] - substring_indices[0] < 3:
            substring_indices = sorted(random.choices(range(len(random_user.user.display_name)), k=2))

        random_substring = random_user.user.display_name[substring_indices[0] : substring_indices[1]]

        response = await client.get(
            self.base_path, params={"name_substring": random_substring}, cookies=random_user.auth_cookie
        )
        assert response.status_code == status.HTTP_200_OK

        users = ExtendedUserList.validate_json(response.content)
        assert len(users) > 0
        assert sum(1 for u in users if u.uid == random_user.uid) == 1
        for u in users:
            assert u.roles is not None


class TestUserRoutesUpdate(_TestUserRoutes):
    @pytest.mark.asyncio
    async def test_update_user_role(
        self, client: AsyncClient, random_user: UserWithAuthCookie, random_second_user: UserWithAuthCookie
    ) -> None:
        """
        Test for updating a user's role.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.test.utils.user.UserWithAuthCookie
            Random user for testing.
        random_second_user : clowm.test.utils.user.UserWithAuthCookie
            Random user that gets is role updated for testing.
        """
        assert len(random_second_user.user.roles) == 2

        response = await client.put(
            f"{self.base_path}/{str(random_second_user.uid)}/roles",
            cookies=random_user.auth_cookie,
            content=UserRoles(roles=[Role.RoleEnum.REVIEWER.value]).model_dump_json(),
        )
        assert response.status_code == status.HTTP_200_OK

        user = UserOutExtended.model_validate_json(response.content)
        assert user.uid == random_second_user.uid
        assert len(user.roles) == 1
        assert user.roles[0] == Role.RoleEnum.REVIEWER

    @pytest.mark.asyncio
    async def test_resend_invitation_for_normal_user(
        self, client: AsyncClient, random_user: UserWithAuthCookie, random_second_user: UserWithAuthCookie
    ) -> None:
        """
        Test for resending an invitation email with no open invitation.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.test.utils.user.UserWithAuthCookie
            Random user for testing.
        random_second_user : clowm.test.utils.user.UserWithAuthCookie
            Random user that gets is role updated for testing.
        """

        response = await client.patch(
            f"{self.base_path}/{str(random_second_user.uid)}/invitation",
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_successful_resend_invitation(
        self,
        client: AsyncClient,
        db: AsyncSession,
        random_user: UserWithAuthCookie,
        random_second_user: UserWithAuthCookie,
    ) -> None:
        """
        Test for resending an invitation email with an open invitation.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : clowm.test.utils.user.UserWithAuthCookie
            Random user for testing.
        random_second_user : clowm.test.utils.user.UserWithAuthCookie
            Random user that gets is role updated for testing.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        token = token_urlsafe(32)
        token_timestamp = round(time.time()) - 100
        await db.execute(
            update(User)
            .where(User.uid == random_second_user.uid)
            .values(invitation_token=token, invitation_token_created_at=token_timestamp)
        )
        await db.commit()

        response = await client.patch(
            f"{self.base_path}/{str(random_second_user.uid)}/invitation",
            cookies=random_user.auth_cookie,
        )
        assert response.status_code == status.HTTP_200_OK
        response_user = UserOutExtended.model_validate_json(response.content)

        user: User | None = await db.scalar(select(User).where(User.uid == response_user.uid))
        assert user is not None
        assert user.invitation_token is not None
        assert user.invitation_token != token
        assert user.invitation_token_created_at is not None
        assert user.invitation_token_created_at != token_timestamp


class TestUserRoutesCreate(_TestUserRoutes):
    @pytest.mark.asyncio
    async def test_create_user(
        self, client: AsyncClient, random_user: UserWithAuthCookie, db: AsyncSession, cleanup: CleanupList
    ) -> None:
        """
        Test for creating a new user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowm.tests.utils.UserWithAuthCookie
            Random user for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        user_in = UserIn(
            display_name=random_lower_string(),
            roles=[Role.RoleEnum.USER.value],
            email=f"{random_lower_string(16)}@example.org",
        )

        response = await client.post(self.base_path, cookies=random_user.auth_cookie, content=user_in.model_dump_json())
        assert response.status_code == status.HTTP_201_CREATED
        await db.reset()
        user = UserOutExtended.model_validate_json(response.content)

        async def delete_user() -> None:
            await db.execute(delete(User).where(User.uid == user.uid))
            await db.commit()

        cleanup.add_task(delete_user)

        assert user.uid
        assert len(user.roles) == 1
        assert user.lifescience_id is None
        assert user.roles[0] == user_in.roles[0]
        assert user.display_name == user_in.display_name
        assert user.invitation_token_created_at is not None

        db_user = await db.scalar(select(User).where(User.uid == user.uid))
        assert db_user is not None
        assert db_user.invitation_token is not None
        assert db_user.invitation_token_created_at is not None

    @pytest.mark.asyncio
    async def test_create_user_without_roles(
        self, client: AsyncClient, random_user: UserWithAuthCookie, db: AsyncSession, cleanup: CleanupList
    ) -> None:
        """
        Test for creating a new user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : clowm.tests.utils.UserWithAuthCookie
            Random user for testing.
        cleanup : clowm.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        user_in = UserIn(
            display_name=random_lower_string(),
            roles=[],
            email=f"{random_lower_string(16)}@example.org",
        )

        response = await client.post(self.base_path, cookies=random_user.auth_cookie, content=user_in.model_dump_json())
        assert response.status_code == status.HTTP_201_CREATED
        await db.reset()
        user = UserOutExtended.model_validate_json(response.content)

        async def delete_user() -> None:
            await db.execute(delete(User).where(User.uid == user.uid))
            await db.commit()

        cleanup.add_task(delete_user)

        assert user.uid
        assert len(user.roles) == 0
        assert user.lifescience_id is None
        assert user.display_name == user_in.display_name
        assert user.invitation_token_created_at is not None

        db_user = await db.scalar(select(User).where(User.uid == user.uid))
        assert db_user is not None
        assert db_user.invitation_token is not None
        assert db_user.invitation_token_created_at is not None


class TestUserRoutesDelete(_TestUserRoutes):
    @pytest.mark.asyncio
    async def test_delete_user(
        self,
        client: AsyncClient,
        random_second_user: UserWithAuthCookie,
        mock_rgw_admin: MockRGWAdmin,
        mock_s3_service: S3ServiceResource,
        random_bucket_permission: BucketPermission,
        db: AsyncSession,
    ) -> None:
        """
        Test for deleting a user.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_second_user : clowm.test.utils.user.UserWithAuthCookie
            Random user for testing.
        mock_s3_service: types_aiobotocore_s3.service_resource.S3ServiceResource
            Mock S3 Service Resource for testing.
        mock_rgw_admin : clowm.tests.mocks.mock_rgw_admin.MockRGWAdmin
            Mock RGW admin for Ceph.
        random_bucket_permission : clowm.models.BucketPermission
            Bucket permission for a random bucket for testing.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        response = await client.delete(
            f"{self.base_path}/{str(random_second_user.uid)}", cookies=random_second_user.auth_cookie
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT

        await db.reset()
        assert await db.scalar(select(User).where(User.uid == random_second_user.uid)) is None

        # check that user got deleted in RGW
        with pytest.raises(KeyError):
            mock_rgw_admin.get_user(str(random_second_user.uid))

        bucket_policy = await mock_s3_service.BucketPolicy(bucket_name=random_bucket_permission.bucket_name)
        assert str(random_second_user.uid) not in await bucket_policy.policy
