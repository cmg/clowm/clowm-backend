from abc import ABC, abstractmethod
from collections.abc import Callable
from enum import Enum, auto

from httpx import AsyncClient, Response
from opentelemetry import trace

from clowm.core.config import SlurmSettings, settings

from .schemas import SlurmJob

tracer = trace.get_tracer_provider().get_tracer(__name__)

__all__ = ["SlurmClient", "JobState", "ResponseCallback"]


class JobState(Enum):
    """
    Enumeration for the possible states of a slurm job.
    """

    RUNNING = auto()
    SUCCESS = auto()
    ERROR = auto()


type ResponseCallback = Callable[[Response], None]


def set_response_attributes_callback(span: trace.Span) -> ResponseCallback:
    def callback(response: Response) -> None:
        span.set_attributes(
            {
                "slurm.response.body": response.content,
                "slurm.response.status_code": response.status_code,
                "slurm.request.url": str(response.request.url),
            }
        )

    return callback


class SlurmClient(ABC):
    def __init__(self, client: AsyncClient, slurm_settings: SlurmSettings | None = None):
        """
        Initialize the client to communicate with a Slurm cluster.

        Parameters
        ----------
        client : httpx.AsyncClient
            Async HTTP client with an open connection.
        slurm_settings : clowm.core.config.SlurmSettings | None, default None
            Settings for the connection to the Slurm cluster. If none, the settings from the settings object will be taken
        """
        local_settings = settings.cluster.slurm if slurm_settings is None else slurm_settings

        self._client = client
        self._headers = {
            "X-SLURM-USER-TOKEN": local_settings.token.get_secret_value(),
            "X-SLURM-USER-NAME": local_settings.user,
        }
        self._base_url = f"{local_settings.uri}slurm/{local_settings.version}"

    @abstractmethod
    async def _submit_job_request(self, job: SlurmJob, callback: ResponseCallback) -> int:
        """
        Submit a job to the version specific url and return the job id.

        Parameters
        ----------
        job : clowm.slurm.schemas.SlurmJob
            Information about the job
        callback : ResponseCallback
            Callback function for response. Used for logging response information

        Returns
        -------
        job_id : int
            Job id of scheduled job
        """
        ...

    @abstractmethod
    async def _delete_job(self, job_id: int, callback: ResponseCallback) -> None:
        """
        Delete or cancel a job.

        Parameters
        ----------
        job_id : int
            Id of the job to delete
        callback : ResponseCallback
            Callback function for response. Used for logging response information
        """
        ...

    @abstractmethod
    async def _get_job_state(self, job_id: int, callback: ResponseCallback) -> JobState:
        """
        Get the state of the job id

        Parameters
        ----------
        job_id : int
            Id of the job to delete
        callback : ResponseCallback
            Callback function for response. Used for logging response information

        Returns
        -------
        state : JobState
            State of the job
        """
        ...

    @abstractmethod
    async def ping_cluster(self) -> Response:
        """
        Ping the cluster.

        Returns
        -------
        response : httpx.Response
            Raw response object
        """
        ...

    async def submit_job(self, job: SlurmJob) -> int:
        """
        Submit a job to the slurm cluster.

        Parameters
        ----------
        job : clowm.slurm.schemas.SlurmJob
            Job with properties to execute on the cluster.

        Returns
        -------
        slurm_job_id : int
            Slurm job ID of submitted job.
        """
        with tracer.start_as_current_span(
            "slurm_submit_job",
            attributes={
                "slurm.request.parameters": job.model_dump_json(
                    exclude_none=True,
                    context={"obscure_keys": ["AWS_ACCESS_KEY_ID", "AWS_SECRET_ACCESS_KEY"]},
                    exclude={"script"},
                ),
                "slurm.request.script": job.script.replace(
                    settings.s3.secret_key.get_secret_value(), "********"
                ).replace(settings.s3.access_key, "********"),
            },
        ) as span:
            job_id = await self._submit_job_request(job, set_response_attributes_callback(span))
            span.set_attribute("job_id", job_id)
            return job_id

    async def cancel_job(self, job_id: int) -> None:
        """
        Cancel a Slurm job on the cluster.

        Parameters
        ----------
        job_id : int
            ID of the job to cancel.
        """
        with tracer.start_as_current_span("slurm_cancel_job", attributes={"slurm.request.job_id": job_id}) as span:
            await self._delete_job(job_id, set_response_attributes_callback(span))

    async def get_job_state(self, job_id: int) -> JobState:
        """
        Check the current state of a slurm job

        Parameters
        ----------
        job_id : int
            ID of the job to cancel.

        Returns
        -------
        finished : JobState
            The state if the specified job

        Notes
        -----
        If the job is not found, then the function returns SUCCESS
        """
        with tracer.start_as_current_span(
            "slurm_check_job_status", attributes={"slurm.request.job_id": job_id}
        ) as span:
            try:
                status = await self._get_job_state(job_id, set_response_attributes_callback(span))
            except Exception as err:
                span.record_exception(err)
                span.set_attribute("status", str(JobState.ERROR))
                return JobState.ERROR
            span.set_attribute("status", str(status))
            return status
