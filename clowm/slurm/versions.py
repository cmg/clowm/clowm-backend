from enum import StrEnum, unique

__all__ = ("SlurmVersion",)


@unique
class SlurmVersion(StrEnum):
    V38 = "v0.0.38"
    V40 = "v0.0.40"
    V41 = "v0.0.41"
    V42 = "v0.0.42"
