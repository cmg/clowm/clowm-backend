from fastapi import status
from httpx import Response

from ..abstract_client import JobState, ResponseCallback, SlurmClient
from ..schemas import SlurmJob
from .schemas import V38JobProperties, V38JobsResponse, V38JobSubmission, V38JobSubmissionResponse

__all__ = ("SlurmClientV38",)


class SlurmClientV38(SlurmClient):
    async def _submit_job_request(self, job: SlurmJob, callback: ResponseCallback) -> int:
        body = V38JobSubmission(
            script=job.script,
            job=V38JobProperties(
                current_working_directory=job.current_working_directory,
                environment=job.environment,
                name=job.name,
                requeue=job.requeue,
                standard_output=job.standard_output,
                partition=job.partition,
                argv=job.argv,
                cpus_per_task=job.cpus_per_task,
                memory_per_node=job.memory_per_node,
            ),
            jobs=None,
        )
        response = await self._client.post(
            self._base_url + "/job/submit",
            headers={"Content-Type": "application/json", **self._headers},
            content=body.model_dump_json(exclude_none=True),
        )
        callback(response)
        if response.status_code != status.HTTP_200_OK:
            raise KeyError("Job ID is None")
        job_id = V38JobSubmissionResponse.model_validate_json(response.content).job_id
        if job_id is None:
            raise KeyError("Job ID is None")
        return job_id

    async def _delete_job(self, job_id: int, callback: ResponseCallback) -> None:
        response = await self._client.delete(
            self._base_url + f"/job/{job_id}", params={"signal": "SIGINT"}, headers=self._headers
        )
        callback(response)

    async def _get_job_state(self, job_id: int, callback: ResponseCallback) -> JobState:
        response = await self._client.get(self._base_url + f"/job/{job_id}", headers=self._headers)
        callback(response)
        jobs = V38JobsResponse.model_validate_json(response.content).jobs
        if jobs is None:
            return JobState.ERROR
        job_state = jobs[0].job_state
        if job_state is None:
            return JobState.ERROR
        if job_state == "COMPLETED":
            return JobState.SUCCESS
        if job_state in ["FAILED", "CANCELLED"]:
            return JobState.ERROR
        return JobState.RUNNING

    async def ping_cluster(self) -> Response:
        return await self._client.get(self._base_url + "/ping", headers=self._headers)
