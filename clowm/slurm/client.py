from httpx import AsyncClient

from clowm.core.config import settings

from .abstract_client import SlurmClient
from .v38.client import SlurmClientV38
from .v40.client import SlurmClientV40
from .v41.client import SlurmClientV41
from .v42.client import SlurmClientV42
from .versions import SlurmVersion

__all__ = ["get_slurm_client"]


def get_slurm_client(client: AsyncClient) -> SlurmClient:
    match settings.cluster.slurm.version:
        case SlurmVersion.V38:
            return SlurmClientV38(client)
        case SlurmVersion.V40:
            return SlurmClientV40(client)
        case SlurmVersion.V41:
            return SlurmClientV41(client)
        case SlurmVersion.V42:
            return SlurmClientV42(client)
        case _:
            raise NotImplementedError()
