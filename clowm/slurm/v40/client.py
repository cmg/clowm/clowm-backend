from fastapi import status
from httpx import Response

from ..abstract_client import JobState, ResponseCallback, SlurmClient
from ..schemas import SlurmJob
from .schemas import V40JobDescMsg, V40JobSubmitReq, V40JobSubmitResponseMsg, V40OpenapiJobInfoResp

__all__ = ("SlurmClientV40",)


class SlurmClientV40(SlurmClient):
    async def _submit_job_request(self, job: SlurmJob, callback: ResponseCallback) -> int:
        body = V40JobSubmitReq(
            script=job.script,
            job=V40JobDescMsg(
                current_working_directory=job.current_working_directory,
                environment=[f"{key}={val}" for key, val in job.environment.items()],
                name=job.name,
                requeue=job.requeue,
                standard_output=job.standard_output,
                partition=job.partition,
                argv=job.argv,
                cpus_per_task=job.cpus_per_task,
                memory_per_node={"set": job.memory_per_node is not None, "number": job.memory_per_node},
            ),
            jobs=None,
        )
        response = await self._client.post(
            self._base_url + "/job/submit",
            headers={"Content-Type": "application/json", **self._headers},
            content=body.model_dump_json(exclude_none=True),
        )
        callback(response)
        if response.status_code != status.HTTP_200_OK:
            raise KeyError("Job ID is None")
        job_id = V40JobSubmitResponseMsg.model_validate_json(response.content).job_id
        if job_id is None:
            raise KeyError("Job ID is None")
        return job_id

    async def _delete_job(self, job_id: int, callback: ResponseCallback) -> None:
        response = await self._client.delete(
            self._base_url + f"/job/{job_id}", params={"signal": "SIGINT"}, headers=self._headers
        )
        callback(response)

    async def _get_job_state(self, job_id: int, callback: ResponseCallback) -> JobState:
        response = await self._client.get(self._base_url + f"/job/{job_id}", headers=self._headers)
        callback(response)
        jobs = V40OpenapiJobInfoResp.model_validate_json(response.content).jobs.root
        job_state = jobs[0].job_state
        if job_state is None:
            return JobState.ERROR
        if "COMPLETED" in job_state:
            return JobState.SUCCESS
        if "FAILED" in job_state or "CANCELLED" in job_state:
            return JobState.ERROR
        return JobState.RUNNING

    async def ping_cluster(self) -> Response:
        return await self._client.get(self._base_url + "/ping", headers=self._headers)
