from typing import Any
from uuid import uuid4

from pydantic import BaseModel, Field, SecretStr, SerializationInfo, field_serializer


class SlurmJob(BaseModel):
    current_working_directory: str = Field(
        "/tmp",  # noqa: S108
        description="Instruct Slurm to connect the batch script's standard output directly to the file name.",
    )
    environment: dict[str, Any] = Field(
        default_factory=lambda: {"NONEMPTY": "NONEMPTY"}, description="Dictionary of environment entries."
    )
    name: str = Field(default_factory=lambda: uuid4().hex, description="Specify a name for the job allocation.")
    requeue: bool = Field(False, description="Specifies that the batch job should eligible to being requeue.")
    standard_output: str | None = Field(
        None, description="Instruct Slurm to connect the batch script's standard output directly to the file name."
    )
    partition: str | None = Field(None, description="Request a specific partition for the resource allocation.")
    argv: list[str] | None = Field(None, description="Arguments to the script.")
    cpus_per_task: int | None = Field(None, description="Number of CPUs for the task", ge=1, le=96)
    memory_per_node: int | None = Field(None, description="Memory for task in megabytes", ge=1)
    script: str = Field(..., description="Executable script to run in batch step")

    @field_serializer("environment")
    def remove_env_with_keys(self, v: dict[str, Any], info: SerializationInfo) -> dict[str, Any]:
        context = info.context
        if context:
            env_keys = context.get("obscure_keys", set())
            return {key: str(SecretStr(value)) if key in env_keys else value for key, value in v.items()}
        return v
