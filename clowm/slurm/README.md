# How to add another Slurm version

## 1. Download OpenAPI schema
Replace the `xx` by the version number to download
```bash
curl -H "X-SLURM-USER-TOKEN: xxx" -H "X-SLURM-USER-NAME: xxx" "http://xxx.xxx.xxx.xxx:6820/openapi.json" |  sed 's/\\\//\//g' |  sed 's/v0.0.xx_/vxx_/g'  >  slurm-vxx.json
```
## 2. Generate schema
Download `datamodel-code-generator`
```bash
pip install datamodel-code-generator
```
Create Python package `vXX` and copy schema in there.

Replace `xx` and generate schema
```bash
datamodel-codegen --input clowm/slurm/vXX/slurm-vXX.json --input-file-type openapi --output clowm/slurm/vXX/schemas.py
git add -A
pre-commmit run --all-files
```

## 3. Generate client
Copy `client.py` from previous version and adjust to new version schema.

## 4. Make version available
Add version to `versions.py` and `client.py`. Now test this version.
