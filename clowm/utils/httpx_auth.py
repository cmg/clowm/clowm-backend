from collections.abc import Generator

from httpx import Auth, Request, Response
from pydantic import BaseModel, SecretStr

__all__ = ["BearerAuth"]


class BearerAuth(BaseModel, Auth):
    token: SecretStr

    def auth_flow(self, request: Request) -> Generator[Request, Response]:
        request.headers["Authorization"] = f"Bearer {self.token.get_secret_value()}"
        yield request

    def __repr__(self) -> str:
        return f"Authorization: Bearer {self.token}"
