from threading import Lock


class Singleton(type):
    _instances = {}  # type: ignore
    _lock = Lock()

    def __call__(cls, *args, **kwargs):  # type: ignore
        if cls not in cls._instances:  # check if an instance already exists
            with cls._lock:  # Acquire lock for thread safety
                if cls not in cls._instances:  # check if an instance was creating during the acquiring of the lock
                    cls._instances[cls] = super().__call__(*args, **kwargs)  # create instance
        return cls._instances[cls]
