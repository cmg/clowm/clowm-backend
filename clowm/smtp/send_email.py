from uuid import UUID

from pydantic import AnyHttpUrl

from clowm.api.dependencies.crud import get_db_context_manager
from clowm.core.config import settings
from clowm.crud import CRUDUser, CRUDWorkflow
from clowm.models import Resource, ResourceVersion, Role, User, Workflow, WorkflowVersion
from clowm.schemas.resource import ResourceOut
from clowm.schemas.resource_version import ResourceVersionOut

from ..db.mixins import OwnershipTransferRequestMixin
from ..schemas.ownership_transfer import OwnershipTransferRequestOut
from .smtp import EmailTemplates, send_email


async def send_first_login_email(user: User) -> None:
    """
    Email a user the first time he logged in.

    Parameters
    ----------
    user : clowm.models.User
        The user who logged in the first time.
    """
    async with get_db_context_manager() as db:
        workflows = await CRUDWorkflow(db=db).list_workflows(
            version_status=[WorkflowVersion.WorkflowVersionStatus.PUBLISHED]
        )
    html, plain = EmailTemplates.FIRST_LOGIN.render(user=user, number_workflows=len(workflows))
    send_email(recipients=user, subject="CloWM first login", html_msg=html, plain_msg=plain)


async def send_invitation_email(user: User, token: str) -> None:
    """
    Email a user when an admin register him.

    Parameters
    ----------
    user : clowm.models.User
        The user who was registered by an admin.
    token : str
    """
    invitation_link = AnyHttpUrl.build(
        scheme=settings.ui_uri.scheme,
        host=settings.ui_uri.host,  # type: ignore[arg-type]
        port=settings.ui_uri.port,
        path="login",
        query=f"invitation_token={token}",
    )
    html, plain = EmailTemplates.INVITATION.render(user=user, invitation_link=invitation_link)
    send_email(recipients=user, subject="Invitation to CloWM", html_msg=html, plain_msg=plain)


async def send_review_request_email(workflow: Workflow, version: WorkflowVersion) -> None:
    """
    Email all administrators and reviewers that a new workflow version must be reviewed.

    Parameters
    ----------
    workflow : clowm.models.Workflow
        The workflow to review.
    version : clowm.models.WorkflowVersion
        The reviewed workflow to review.
    """
    if workflow.developer_id is None:  # pragma: no cover
        return
    async with get_db_context_manager() as db:
        crud_user = CRUDUser(db=db)
        developer = await crud_user.get(uid=workflow.developer_id)
        recipients = await crud_user.list_users(roles=[Role.RoleEnum.ADMINISTRATOR, Role.RoleEnum.REVIEWER])
    html, plain = EmailTemplates.REVIEW_REQUEST.render(workflow=workflow, version=version, developer=developer)
    send_email(
        recipients=list(recipients),
        subject=f"Workflow Review {workflow.name}@{version.version}",
        html_msg=html,
        plain_msg=plain,
    )


async def send_workflow_status_update_email(workflow: Workflow, version: WorkflowVersion) -> None:
    """
    Email the developer of a workflow that the workflow was reviewed.

    Parameters
    ----------
    workflow : clowm.models.Workflow
        The reviewed workflow.
    version : clowm.models.WorkflowVersion
        The reviewed workflow version.
    """
    if workflow.developer_id is None:  # pragma: no cover
        return
    async with get_db_context_manager() as db:
        developer = await CRUDUser(db=db).get(uid=workflow.developer_id)
    if developer is None:
        return
    html, plain = EmailTemplates.REVIEW_RESPONSE.render(workflow=workflow, version=version, developer=developer)
    send_email(
        recipients=developer,
        subject=f"Workflow Review Response {workflow.name}@{version.version}",
        html_msg=html,
        plain_msg=plain,
    )


async def send_new_workflow_email(workflow: Workflow, version: WorkflowVersion) -> None:
    """
    Email all administrators and reviewers that a new workflow was registered and needs to be reviewed.

    Parameters
    ----------
    workflow : clowm.models.Workflow
        The reviewed workflow.
    version : clowm.models.WorkflowVersion
        The reviewed workflow version.
    """
    if workflow.developer_id is None:  # pragma: no cover
        return
    async with get_db_context_manager() as db:
        crud_user = CRUDUser(db=db)
        developer = await crud_user.get(uid=workflow.developer_id)
        recipients = await crud_user.list_users(roles=[Role.RoleEnum.ADMINISTRATOR, Role.RoleEnum.REVIEWER])
    html, plain = EmailTemplates.NEW_WORKFLOW.render(workflow=workflow, version=version, developer=developer)
    send_email(
        recipients=list(recipients),
        subject=f"New Workflow {workflow.name}@{version.version}",
        html_msg=html,
        plain_msg=plain,
    )


async def send_resource_review_request_email(resource: ResourceOut, version: ResourceVersionOut) -> None:
    """
    Email all administrators and reviewers that a new resource version was registered and needs to be reviewed.

    Parameters
    ----------
    resource : clowm.schemas.resource.ResourceOut
        The reviewed workflow.
    version : clowm.schemas.resource_version.ResourceVersionOut
        The reviewed workflow version.
    """
    async with get_db_context_manager() as db:
        crud_user = CRUDUser(db=db)
        maintainer = await crud_user.get(uid=resource.maintainer_id)  # type: ignore[arg-type]
        recipients = await crud_user.list_users(roles=[Role.RoleEnum.ADMINISTRATOR, Role.RoleEnum.REVIEWER])
    html, plain = EmailTemplates.RESOURCE_REVIEW_REQUEST.render(
        resource=resource, version=version, maintainer=maintainer
    )
    send_email(
        recipients=list(recipients),
        subject=f"Resource version review {resource.name}@{version.release}",
        html_msg=html,
        plain_msg=plain,
    )


async def send_resource_review_response_email(
    resource: ResourceOut, version: ResourceVersionOut, deny_reason: str | None
) -> None:
    """
    Email the maintainer of a resource that the version was reviewed.

    Parameters
    ----------
    resource : clowm.schemas.resource.ResourceOut
        The reviewed workflow.
    version : clowm.schemas.resource_version.ResourceVersionOut
        The reviewed workflow version.
    deny_reason : str | None
        If the version was denied, a reason must be given
    """
    async with get_db_context_manager() as db:
        maintainer = await CRUDUser(db=db).get(uid=resource.maintainer_id)  # type: ignore[arg-type]
    if maintainer is None:  # pragma: no cover
        return
    html, plain = EmailTemplates.RESOURCE_REVIEW_RESPONSE.render(
        resource=resource, version=version, maintainer=maintainer, deny_reason=deny_reason
    )
    send_email(
        recipients=maintainer,
        subject=f"Resource version review response {resource.name}@{version.release}",
        html_msg=html,
        plain_msg=plain,
    )


async def send_sync_request_email(
    resource: ResourceOut, version: ResourceVersionOut, request_reason: str, requester: User
) -> None:
    """
    Email all administrators and reviewers that a new resource was registered and needs to be reviewed.

    Parameters
    ----------
    resource : clowm.schemas.resource.ResourceOut
        The reviewed workflow.
    version : clowm.schemas.resource_version.ResourceVersionOut
        The reviewed workflow version.
    request_reason : str
        The reason for the synchronization request
    requester : clowm.models.User
        The user who requested this synchronization
    """
    async with get_db_context_manager() as db:
        recipients = await CRUDUser(db=db).list_users(roles=[Role.RoleEnum.ADMINISTRATOR])
    html, plain = EmailTemplates.SYNC_REQUEST.render(
        resource=resource, version=version, requester=requester, request_reason=request_reason
    )
    send_email(
        recipients=list(recipients),
        subject=f"Resource sync request {resource.name}@{version.release}",
        html_msg=html,
        plain_msg=plain,
    )


async def send_sync_response_email(resource: Resource, version: ResourceVersion, deny_reason: str | None) -> None:
    """
    Email the sync requester the response to the sync request.

    Parameters
    ----------
    resource : clowm.models.Resource
        The reviewed workflow.
    version : clowm.models.ResourceVersion
        The reviewed workflow version.
    deny_reason : str | None
        If the request was denied, a reason must be given
    """
    if version.synchronization_request_uid is None:  # pragma: no cover
        return
    async with get_db_context_manager() as db:
        requester = await CRUDUser(db=db).get(uid=version.synchronization_request_uid)
    if requester is None:  # pragma: no cover
        return
    html, plain = EmailTemplates.SYNC_RESPONSE.render(
        resource=resource, version=version, requester=requester, deny_reason=deny_reason
    )
    send_email(
        recipients=requester,
        subject=f"Resource sync request response {resource.name}@{version.release}",
        html_msg=html,
        plain_msg=plain,
    )


async def send_sync_success_email(resource: ResourceOut, version: ResourceVersionOut, requester: User) -> None:
    """
    Email the sync requester when the synchronization process is done..

    Parameters
    ----------
    resource : clowm.schemas.resource.ResourceOut
        The reviewed workflow.
    version : clowm.schemas.resource_version.ResourceVersionOut
        The reviewed workflow version.
    requester : clowm.models.User
        The user who requested this synchronization
    """
    html, plain = EmailTemplates.SYNC_SUCCESS.render(resource=resource, version=version, requester=requester)
    send_email(
        recipients=requester,
        subject=f"Resource sync success {resource.name}@{version.release}",
        html_msg=html,
        plain_msg=plain,
    )


async def send_sync_error_email(
    resource: ResourceOut, version: ResourceVersionOut, error_msg: str | None = None
) -> None:
    """
    Email all administrators that a resource synchronization has failed.

    Parameters
    ----------
    resource : clowm.schemas.resource.ResourceOut
        The reviewed workflow.
    version : clowm.schemas.resource_version.ResourceVersionOut
        The reviewed workflow version.
    error_msg : str | None, default None
        An optional error message
    """
    async with get_db_context_manager() as db:
        recipients = await CRUDUser(db=db).list_users(roles=[Role.RoleEnum.ADMINISTRATOR])
    html, plain = EmailTemplates.SYNC_ERROR.render(resource=resource, version=version, error_msg=error_msg)
    send_email(
        recipients=list(recipients),
        subject=f"Resource sync error {resource.name}@{version.release}",
        html_msg=html,
        plain_msg=plain,
    )


async def send_otr_created_email(otr: OwnershipTransferRequestOut) -> None:
    """
    Email the OTR to the new owner

    Parameters
    ----------
    otr : clowm.schemas.ownership_transfer.OwnershipTransferRequestOut
        OTR with relevant information
    """
    async with get_db_context_manager() as db:
        crud_user = CRUDUser(db=db)
        new_owner = await crud_user.get(otr.new_owner_uid)
        assert new_owner is not None
        developer = await crud_user.get(otr.current_owner_uid) if otr.current_owner_uid is not None else None
    html, plain = EmailTemplates.OTR_CREATED.render(otr=otr, developer=developer, user=new_owner)
    send_email(
        recipients=new_owner,
        subject=f"Ownership transfer request for {otr.target_type} {otr.target_name}",
        html_msg=html,
        plain_msg=plain,
    )


async def send_otr_accepted_email(
    new_owner_uid: UUID,
    target_name: str,
    target_type: type[OwnershipTransferRequestMixin],
    old_owner_uid: UUID | None = None,
) -> None:
    """
    Email the old owner that the OTR for his target was accepted.

    Parameters
    ----------
    new_owner_uid : uuid.UUID
        UID of the new owner.
    target_name : str
        Name of the OTR target
    target_type : Type[clowm.db.mixins.OwnershipTransferRequestMixin]
        Type of the OTR target
    old_owner_uid : uuid.UUID | None, default None
        UID of the old owner if he exists
    """
    async with get_db_context_manager() as db:
        crud_user = CRUDUser(db=db)
        old_owner = await crud_user.get(old_owner_uid) if old_owner_uid is not None else None
        if old_owner is None:
            return
        new_owner = await crud_user.get(new_owner_uid)
        assert new_owner is not None
    html, plain = EmailTemplates.OTR_ACCEPTED.render(
        target_type=target_type, target_name=target_name, old_owner=old_owner, new_owner=new_owner
    )
    send_email(
        recipients=old_owner,
        subject=f"Accepted ownership transfer request for {target_type.__name__.lower()} {target_name}",
        html_msg=html,
        plain_msg=plain,
    )


async def send_otr_rejected_email(
    new_owner_uid: UUID,
    target_name: str,
    target_type: type[OwnershipTransferRequestMixin],
    owner_uid: UUID | None = None,
) -> None:
    """
    Email the OTR to the new owner

    Parameters
    ----------
    new_owner_uid : uuid.UUID
        UID of the potential new owner.
    target_name : str
        Name of the OTR target
    target_type : Type[clowm.db.mixins.OwnershipTransferRequestMixin]
        Type of the OTR target
    owner_uid : uuid.UUID | None, default None
        UID of the owner if he exists
    """
    async with get_db_context_manager() as db:
        crud_user = CRUDUser(db=db)
        developer = await crud_user.get(owner_uid) if owner_uid is not None else None
        if developer is None:
            return
        new_owner = await crud_user.get(new_owner_uid)
        assert new_owner is not None
    html, plain = EmailTemplates.OTR_REJECTED.render(
        target_type=target_type, target_name=target_name, developer=developer, new_owner=new_owner
    )
    send_email(
        recipients=developer,
        subject=f"Rejected ownership transfer request for {target_type.__name__.lower()} {target_name}",
        html_msg=html,
        plain_msg=plain,
    )


async def send_otr_deleted_email(
    new_owner_uid: UUID,
    target_name: str,
    target_type: type[OwnershipTransferRequestMixin],
) -> None:
    """
    Email the OTR to the new owner

    Parameters
    ----------
    new_owner_uid : uuid.UUID
        UID of the potential new owner.
    target_name : str
        Name of the OTR target
    target_type : Type[clowm.db.mixins.OwnershipTransferRequestMixin]
        Type of the OTR target
    """
    async with get_db_context_manager() as db:
        new_owner = await CRUDUser(db=db).get(new_owner_uid)
        assert new_owner is not None
    html, plain = EmailTemplates.OTR_DELETED.render(
        target_type=target_type, target_name=target_name, new_owner=new_owner
    )
    send_email(
        recipients=new_owner,
        subject=f"Deleted ownership transfer request for {target_type.__name__.lower()} {target_name}",
        html_msg=html,
        plain_msg=plain,
    )


async def send_user_deleted_email(user: User, own_account: bool) -> None:
    """
    Email the OTR to the new owner

    Parameters
    ----------
    user : clowm.models.User
        User that got deleted.
    own_account : bool
        Flag if the user deleted his own account.
    """
    if user.email is None:
        return
    html, plain = EmailTemplates.ACCOUNT_DELETED.render(user=user, own_account=own_account)
    send_email(
        recipients=user,
        subject="Your CloWM account was deleted",
        html_msg=html,
        plain_msg=plain,
    )
