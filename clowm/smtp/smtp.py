import smtplib
from collections.abc import Generator, Sequence
from contextlib import contextmanager
from email import utils as email_utils
from email.message import EmailMessage
from enum import StrEnum, unique
from ssl import PROTOCOL_TLS_CLIENT, SSLContext
from tempfile import mkdtemp
from typing import TYPE_CHECKING, Any

from mako.lookup import TemplateLookup
from opentelemetry import trace

from clowm.core.config import EmailSettings, settings
from clowm.models import User

if TYPE_CHECKING:
    from _typeshed import SizedBuffer
else:
    SizedBuffer = bytes


tracer = trace.get_tracer_provider().get_tracer(__name__)

_email_html_lookup = TemplateLookup(directories=["clowm/smtp/templates/html"], module_directory=mkdtemp())
_email_plain_lookup = TemplateLookup(directories=["clowm/smtp/templates/plain"], module_directory=mkdtemp())


__all__ = ["EmailTemplates", "send_email"]


class FakeSMTP(smtplib.SMTP):  # pragma: no cover
    """
    Fake SMTP class to print sent emails to the console or discard them.
    """

    def __init__(self, print_to_console: bool = False) -> None:
        super().__init__()
        self.print_to_console = print_to_console

    def ehlo_or_helo_if_needed(self) -> None:
        pass

    def sendmail(
        self,
        from_addr: str,
        to_addrs: str | Sequence[str],
        msg: SizedBuffer | str,
        mail_options: Sequence[str] = (),
        rcpt_options: Sequence[str] = (),
    ) -> dict[str, tuple[int, bytes]]:
        if self.print_to_console:
            print(msg if isinstance(msg, str) else msg.decode("utf-8"))  # type: ignore[attr-defined]
        return {}

    def __str__(self) -> str:
        return f"FakeSMTP(print_to_console={self.print_to_console})"

    def __repr__(self) -> str:
        return str(self)


@contextmanager
def smtp_connection(smtp_settings: EmailSettings) -> Generator[smtplib.SMTP]:  # pragma: no cover
    """
    Yield a connection the smtp server configured in the options.

    Returns
    -------
    smtp_settings : clowm.core.config.EmailSettings
        The settings for the SMTP connection.
    """
    context = SSLContext(protocol=PROTOCOL_TLS_CLIENT)
    context.load_default_certs()
    if smtp_settings.ca_path is not None and smtp_settings.key_path is not None:  # pragma: no cover
        context.load_cert_chain(certfile=smtp_settings.ca_path, keyfile=smtp_settings.key_path)

    if smtp_settings.server == "console":
        yield FakeSMTP(print_to_console=True)
        return
    if smtp_settings.server is None:
        yield FakeSMTP(print_to_console=False)
        return
    if smtp_settings.connection_security == "ssl" or smtp_settings.connection_security == "tls":
        smtp_manager: smtplib.SMTP = smtplib.SMTP_SSL(
            smtp_settings.server,
            smtp_settings.port,
            local_hostname=smtp_settings.local_hostname,
            context=context,
        )
    else:
        smtp_manager = smtplib.SMTP(
            smtp_settings.server, smtp_settings.port, local_hostname=smtp_settings.local_hostname
        )

    with smtp_manager as server:
        if smtp_settings.connection_security == "starttls":
            server.starttls(context=context)
        if smtp_settings.user is not None and smtp_settings.password is not None:
            server.login(user=smtp_settings.user, password=smtp_settings.password.get_secret_value())
        yield server


@unique
class EmailTemplates(StrEnum):
    INVITATION = "invitation"
    FIRST_LOGIN = "first_login"
    REVIEW_REQUEST = "review_request"
    REVIEW_RESPONSE = "review_response"
    NEW_WORKFLOW = "new_workflow"
    RESOURCE_REVIEW_REQUEST = "resource_review_request"
    RESOURCE_REVIEW_RESPONSE = "resource_review_response"
    SYNC_REQUEST = "sync_request"
    SYNC_RESPONSE = "sync_response"
    SYNC_SUCCESS = "sync_success"
    SYNC_ERROR = "sync_error"
    OTR_CREATED = "otr_created"
    OTR_ACCEPTED = "otr_accepted"
    OTR_REJECTED = "otr_rejected"
    OTR_DELETED = "otr_deleted"
    ACCOUNT_DELETED = "account_deleted"

    def render(self, **kwargs: Any) -> tuple[str, str]:
        """
        Render both HTML and plain text smtp templates

        Parameters
        ----------
        kwargs : Any
            Arguments for the smtp template

        Returns
        -------
        html, plain : tuple[str, str]
            The tuple of the rendered HTML and plain text smtp
        """
        return _email_html_lookup.get_template(f"{self}.html.tmpl").render(
            settings=settings, **kwargs
        ), _email_plain_lookup.get_template(f"{self}.txt.tmpl").render(settings=settings, **kwargs)


def _format_user_to_email(user: User) -> str:
    return f"{user.display_name} <{user.email}>"


def send_email(
    recipients: list[User] | User,
    subject: str,
    plain_msg: str,
    html_msg: str | None = None,
) -> None:
    """
    Send an smtp via the provided smtp server.

    Parameters
    ----------
    recipients: list[clowm.models.User] | clowm.models.User
        The recipients of the smtp. Multiple will be sent via BCC
    subject: str
        The subject of the smtp.
    plain_msg: str
        An alternative message in plain text format.
    html_msg : str
        An alternative message in HTML format.
    """
    email_msg = EmailMessage()
    email_msg["Subject"] = subject
    email_msg["From"] = (
        f"CloWM <{settings.smtp.sender_email.email}>"
        if settings.smtp.sender_email.email.startswith(settings.smtp.sender_email.name)
        else str(settings.smtp.sender_email)
    )
    if isinstance(recipients, list):
        email_msg["Bcc"] = ",".join(_format_user_to_email(user) for user in recipients)
    else:
        email_msg["To"] = _format_user_to_email(recipients)
    email_msg["Date"] = email_utils.formatdate()
    if settings.smtp.reply_email is not None:  # pragma: no cover
        email_msg["Reply-To"] = (
            f"CloWM Support <{settings.smtp.reply_email.email}>"
            if settings.smtp.reply_email.email.startswith(settings.smtp.reply_email.name)
            else str(settings.smtp.reply_email)
        )
    email_msg.set_content(plain_msg)
    if html_msg is not None:
        email_msg.add_alternative(html_msg, subtype="html")
    with (
        smtp_connection(smtp_settings=settings.smtp) as smtp_server,
        tracer.start_as_current_span(
            "background_send_email",
            attributes={
                "subject": subject,
                "recipients": (
                    [str(user.uid) for user in recipients] if isinstance(recipients, list) else str(recipients.uid)
                ),
            },
        ),
    ):
        smtp_server.send_message(email_msg)
