import json
import time
from uuid import UUID, uuid4

from opentelemetry import trace

from clowm.ceph.s3 import add_s3_bucket_policy_stmt, cors_rule, get_base_policy
from clowm.core.config import settings
from clowm.crud import CRUDBucket, CRUDUser
from clowm.models import User
from clowm.schemas.bucket import BucketIn

from ..background import dependencies as background_dependencies
from ..dependencies import services
from ..dependencies.crud import get_db_context_manager

tracer = trace.get_tracer_provider().get_tracer(__name__)


async def initialize_user(user: User) -> None:
    """
    Create an initial bucket for a user.

    Parameters
    ----------
    user : clowm.models.User
        The user who needs to be initialized.
    """
    async with get_db_context_manager() as db:
        crud_bucket = CRUDBucket(db=db)
        name = "initial-bucket-" + user.uid.hex[-8:]
        while (await crud_bucket.get(name)) is not None:
            name = "initial-bucket-" + uuid4().hex[:8]  # pragma: no cover
        bucket = await crud_bucket.create(
            BucketIn(name=name, description=f"Initial bucket for {user.display_name}"),
            owner_id=user.uid,
            size_limit=int(settings.s3.initial_bucket_size_limit.to("KiB")),
            object_limit=settings.s3.initial_bucket_object_limit,
        )

        async with background_dependencies.get_background_s3_resource() as s3:
            with tracer.start_as_current_span("s3_create_bucket", attributes={"bucket_name": bucket.name}):
                s3_bucket = await s3.Bucket(bucket.name)
                await s3_bucket.create(ObjectOwnership="BucketOwnerEnforced")
            # Add basic permission to the user.
            await add_s3_bucket_policy_stmt(
                *get_base_policy(bucket_name=bucket.name, uid=user.uid), bucket_name=bucket.name, s3=s3
            )

            with tracer.start_as_current_span(
                "s3_put_bucket_cors_rules", attributes={"bucket_name": bucket.name, "rules": json.dumps(cors_rule)}
            ):
                # Add CORS rule to bucket to allow access from the browser
                cors = await s3_bucket.Cors()
                await cors.put(CORSConfiguration=cors_rule)  # type: ignore[arg-type]

        rgw = services.get_rgw_admin()
        with tracer.start_as_current_span("rgw_set_bucket_quota", attributes={"bucket_name": bucket.name}):
            rgw.set_bucket_quota(
                uid=str(user.uid),
                bucket=bucket.name,
                max_objects=bucket.object_limit,
                max_size_kb=bucket.size_limit,
                enabled=True,
            )

        await CRUDUser(db=db).mark_initialized(user.uid)


async def update_last_login(uid: UUID) -> None:
    """
    Update the last_login column of a user in the DB with the current time.

    Parameters
    ----------
    uid : uuid.UUID
        UID of a user.
    """
    with tracer.start_as_current_span("update_last_login", attributes={"uid": str(uid)}):
        async with get_db_context_manager() as db:
            await CRUDUser(db=db).update_last_login(uid=uid, timestamp=round(time.time()))
