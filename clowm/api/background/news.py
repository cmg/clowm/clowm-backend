from mako.template import Template
from opentelemetry import trace

from clowm.core.config import settings
from clowm.crud import CRUDNews, CRUDUser
from clowm.models import Workflow, WorkflowVersion
from clowm.schemas.news import NewsIn

from ..dependencies.crud import get_db_context_manager

_update_template = Template(  # noqa: S702
    """
A workflow \\
% if developer_name is not None:
registered by ${developer_name} \\
% endif
was updated on the CloWM platform :tada:

${"####"} [${workflow.name}@${version.version}](${settings.ui_uri}workflows/${workflow.workflow_id}/version/${version.git_commit_hash})
${workflow.short_description}
""".strip(),
    default_filters=["str", "escape"],
    imports=["from  markupsafe import escape"],
)

_registration_template = Template(  # noqa: S702
    """
There is a new workflow on the CloWM platform \\
% if developer_name is not None:
registered by ${developer_name} \\
% endif
:tada:

${"####"} [${workflow.name}@${version.version}](${settings.ui_uri}workflows/${workflow.workflow_id}/version/${version.git_commit_hash})
${workflow.short_description}
""".strip(),
    default_filters=["str", "escape"],
    imports=["from  markupsafe import escape"],
)

tracer = trace.get_tracer_provider().get_tracer(__name__)

__all__ = ["create_workflow_news_event"]


async def create_workflow_news_event(workflow: Workflow, version: WorkflowVersion) -> None:
    global _registration_template, _update_template
    update = version.previous_version_hash is not None
    with tracer.start_as_current_span(
        "background_create_workflow_registration_news_event",
        attributes={
            "workflow_id": str(workflow.workflow_id),
            "git_commit_hash": version.git_commit_hash,
            "update": update,
        },
    ):
        async with get_db_context_manager() as db:
            developer_name = None
            if workflow.developer_id is not None:
                developer = await CRUDUser(db=db).get(uid=workflow.developer_id)
                if developer is not None:
                    developer_name = developer.display_name
            news_in = NewsIn(
                category="workflow",
                title=f"Workflow {'update' if update else 'registration'}: {workflow.name}@{version.version}",
                content=(_update_template if update else _registration_template).render(
                    workflow=workflow, version=version, developer_name=developer_name, settings=settings
                ),
            )
            await CRUDNews(db=db).create(news_in=news_in)
