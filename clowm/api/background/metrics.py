from datetime import timedelta
from uuid import UUID

from opentelemetry import trace
from pydantic import BaseModel, TypeAdapter

from clowm.crud import CRUDApiToken, CRUDWorkflowExecution
from clowm.schemas.tower import Task

from ..dependencies.crud import get_db_context_manager

tracer = trace.get_tracer_provider().get_tracer(__name__)


class MetricLog(BaseModel):
    cpus: int
    realtime: int
    task_id: int


ta = TypeAdapter(list[MetricLog])


async def save_cpu_time(execution_id: UUID, tasks: list[Task]) -> None:
    """
    Update the cpu_time field of a workflow execution with the spent CPU hours so far.

    Parameters
    ----------
    execution_id : uuid.UUID
        Id of a workflow execution.
    tasks : list[clowm.schemas.tower.Task]
        A list of nextflow tasks.

    Notes
    -----
    `realtime` in ms
    `cpu` number of cores
    `cpu_seconds` = realtime/1000 * cpu
    """
    with tracer.start_as_current_span(
        "background_save_cpu_time", attributes={"execution_id": str(execution_id)}
    ) as span:
        cpu_seconds = 0.0
        job_metrics: list[MetricLog] = []
        for task in tasks:
            if task.status == "COMPLETED" and task.realtime is not None:
                job_metrics.append(MetricLog(cpus=task.cpus, realtime=task.realtime, task_id=task.taskId))
                cpu_seconds += task.cpus * task.realtime / 1000
        if cpu_seconds > 0:
            span.set_attribute("job_metrics", ta.dump_json(job_metrics))
            async with get_db_context_manager() as db:
                await CRUDWorkflowExecution(db=db).update_cpu_time(
                    execution_id=execution_id, additional_cpu_time=timedelta(seconds=cpu_seconds)
                )


async def update_last_used_api_token(token_id: UUID) -> None:
    """
    Save the timestamp every time an API token was used.

    Parameters
    ----------
    token_id : uuid.UUID
        ID of the used token.
    """
    async with get_db_context_manager() as db:
        await CRUDApiToken(db=db).update_last_used(tid=token_id)
