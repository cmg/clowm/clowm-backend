from uuid import UUID

from opentelemetry import trace

from clowm.crud import CRUDResourceVersion

from ..dependencies.crud import get_db_context_manager

tracer = trace.get_tracer_provider().get_tracer(__name__)


async def update_used_resources(ids: list[UUID]) -> None:
    """
    Update the used resources for a workflow execution.

    Parameters
    ----------
    ids : list[uuid.UUID]
        List of resource version ids.
    """
    with tracer.start_as_current_span(
        "background_update_used_resources",
        attributes={"resource_version_ids": [str(rvid) for rvid in ids]},
    ):
        async with get_db_context_manager() as db:
            for rvid in ids:
                await CRUDResourceVersion(db=db).update_used_resource_version(resource_version_id=rvid)
