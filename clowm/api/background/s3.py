from io import BytesIO
from pathlib import Path
from typing import Any
from uuid import UUID

from botocore.exceptions import ClientError
from opentelemetry import trace
from PIL import Image

from clowm.api.background import dependencies as background_dependencies
from clowm.ceph.s3 import (
    add_s3_bucket_policy_stmt,
    copy_s3_obj,
    delete_s3_bucket_policy_stmt,
    delete_s3_obj,
    delete_s3_objects,
    upload_s3_obj,
)
from clowm.core.config import settings
from clowm.crud import CRUDResourceVersion, CRUDWorkflowVersion
from clowm.git_repository import GitRepository
from clowm.models import ResourceVersion
from clowm.schemas.resource import ResourceOut, S3ResourceVersionInfo
from clowm.schemas.resource_version import ResourceVersionOut, resource_dir_name, resource_version_dir_name
from clowm.scm import SCM

from ..dependencies.crud import get_db_context_manager

tracer = trace.get_tracer_provider().get_tracer(__name__)


async def process_and_upload_icon(icon_slug: str, icon_buffer_file: Path) -> None:
    """
    Process the icon and upload it to the S3 Icon Bucket.

    Parameters
    ----------
    icon_slug : str
        Slug of the icon
    icon_buffer_file : pathlib.Path
        Path to the file containing the icon
    """
    with tracer.start_as_current_span(
        "background_process_upload_icon", attributes={"icon_slug": icon_slug, "icon_buffer_file": str(icon_buffer_file)}
    ) as span:
        thumbnail_buffer = BytesIO()
        with open(icon_buffer_file, "rb") as f:  # noqa: ASYNC230
            im = Image.open(f)
            im.thumbnail((64, 64))  # Crop to 64x64 image
            im.save(thumbnail_buffer, "PNG")  # save in buffer as PNG image
            span.set_attribute("uploaded_icon_size", f.tell())
        thumbnail_buffer.seek(0)
        # Upload to bucket
        async with background_dependencies.get_background_s3_resource() as s3:
            await upload_s3_obj(
                bucket_name=settings.s3.data_bucket,
                key=settings.s3.icon_key(icon_slug),
                handle=thumbnail_buffer,
                s3=s3,
                ExtraArgs={"ContentType": "image/png"},
            )


async def upload_scm_file(scm: SCM, scm_file_id: UUID) -> None:
    """
    Upload the SCM file of a private workflow into the PARAMS_BUCKET with the workflow id as key

    Parameters
    ----------
    scm : clowm.scm.SCM
        Python object representing the SCM file
    scm_file_id : str
        ID of the scm file for the name of the file in the bucket
    """
    with (
        BytesIO() as handle,
        tracer.start_as_current_span("background_upload_scm_file", attributes={"scm_file_id": str(scm_file_id)}),
    ):
        scm.serialize(handle)
        handle.seek(0)
        async with background_dependencies.get_background_s3_resource() as s3:
            await upload_s3_obj(
                bucket_name=settings.s3.data_bucket, key=settings.s3.scm_key(scm_file_id), handle=handle, s3=s3
            )


async def delete_from_data_bucket_with_prefix(prefix: str) -> None:
    """
    Delete objects from the data bucket by a prefix.

    Parameters
    ----------
    prefix : str
        The prefix of the keys that should be deleted

    """
    async with background_dependencies.get_background_s3_resource() as s3:
        await delete_s3_objects(bucket_name=settings.s3.data_bucket, prefix=prefix, s3=s3)


async def delete_from_data_bucket(key: str) -> None:
    """
    Delete an object from the data bucket.

    Parameters
    ----------
    key : str
        Key of the object to delete.
    """
    async with background_dependencies.get_background_s3_resource() as s3:
        await delete_s3_obj(bucket_name=settings.s3.data_bucket, key=key, s3=s3)


async def copy_in_data_bucket(src_key: str, dst_key: str) -> None:
    """
    Copy an S3 object within the clowm data bucket

    Parameters
    ----------
    src_key : str
        The key for the S3 source object.
    dst_key : str
        The key for the S3 destination object.
    """
    async with background_dependencies.get_background_s3_resource() as s3:
        await copy_s3_obj(
            src_bucket_name=settings.s3.data_bucket,
            src_key=src_key,
            dst_bucket_name=settings.s3.data_bucket,
            dst_key=dst_key,
            s3=s3,
        )


async def delete_remote_icon(icon_slug: str) -> None:
    """
    Delete icon in S3 Bucket if there are no other workflow versions that depend on it

    Parameters
    ----------
    icon_slug : str
        Name of the icon file.
    """
    with tracer.start_as_current_span("background_delete_remote_icon", attributes={"icon_slug": icon_slug}):
        # If there are no more Workflow versions that have this icon, delete it in the S3 data bucket
        async with get_db_context_manager() as db:
            check = await CRUDWorkflowVersion(db=db).icon_exists(icon_slug)
        if not check:
            await delete_from_data_bucket(key=settings.s3.icon_key(icon_slug))


async def download_file_to_data_bucket(repo: GitRepository, *, filepath: str, key: str) -> None:
    """
    Download a file from a git repository to a bucket.

    Parameters
    ----------
    repo : clowm.git_repository.GitRepository
        The git repository object.
    filepath : str
        The path to the file in the git repository.
    key : str
        The key for the S3 object.
    """
    with tracer.start_as_current_span(
        "background_download_file_to_bucket_from_repository",
        attributes={
            "repository_url": repo.url,
            "filepath": filepath,
            "bucket_name": settings.s3.data_bucket,
            "key": key,
        },
    ):
        async with (
            background_dependencies.get_background_http_client() as client,
            background_dependencies.get_background_s3_resource() as s3,
        ):
            await repo.copy_file_to_bucket(
                filepath=filepath,
                obj=await s3.Object(key=key, bucket_name=settings.s3.data_bucket),
                client=client,
            )


async def upload_file_to_data_bucket(path: Path, key: str) -> None:
    """
    Upload a file to the clowm data bucket.

    Parameters
    ----------
    path : pathlib.Path
        Path to the file to upload.
    key : str
        Key of the created object in S3
    """
    async with (
        background_dependencies.get_background_s3_resource() as s3,
    ):
        with open(path, "rb") as f:  # noqa: ASYNC230
            await upload_s3_obj(bucket_name=settings.s3.data_bucket, key=key, handle=f, s3=s3)


async def upload_string_to_data_bucket(content: str, key: str) -> None:
    """
    Upload a string to the clowm data bucket.

    Parameters
    ----------
    content : str
        Content of a S3 object.
    key : str
        Key of the created object in S3
    """
    async with background_dependencies.get_background_s3_resource() as s3:
        await upload_s3_obj(
            bucket_name=settings.s3.data_bucket, key=key, handle=BytesIO(content.encode("utf-8")), s3=s3
        )


async def give_permission_to_s3_resource(resource: ResourceOut) -> None:
    """
    Give the maintainer permissions to list the S3 objects of his resource.

    Parameters
    ----------
    resource : clowm.schemas.resource.ResourceOut
        Information about the resource.
    """
    policy: dict[str, Any] = {
        "Sid": str(resource.resource_id),
        "Effect": "Allow",
        "Principal": {"AWS": f"arn:aws:iam:::user/{str(resource.maintainer_id)}"},
        "Resource": f"arn:aws:s3:::{settings.s3.data_bucket}",
        "Action": ["s3:ListBucket"],
        "Condition": {"StringLike": {"s3:prefix": resource_dir_name(resource.resource_id) + "/*"}},
    }
    async with background_dependencies.get_background_s3_resource() as s3:
        await add_s3_bucket_policy_stmt(policy, s3=s3, bucket_name=settings.s3.data_bucket)


async def give_permission_to_s3_resource_version(resource_version: ResourceVersionOut, maintainer_id: UUID) -> None:
    """
    Give the maintainer permissions to upload the resource to the appropriate S3 key.

    Parameters
    ----------
    resource_version : clowm.schemas.resource_version.ResourceVersionOut
        Information about the resource version.
    maintainer_id : str
        ID of the maintainer
    """
    policy: dict[str, Any] = {
        "Sid": str(resource_version.resource_version_id),
        "Effect": "Allow",
        "Principal": {"AWS": f"arn:aws:iam:::user/{str(maintainer_id)}"},
        "Resource": f"arn:aws:s3:::{resource_version.s3_path[5:]}",
        "Action": ["s3:DeleteObject", "s3:PutObject", "s3:GetObject"],
    }
    async with background_dependencies.get_background_s3_resource() as s3:
        await add_s3_bucket_policy_stmt(policy, s3=s3, bucket_name=settings.s3.data_bucket)


async def add_s3_resource_version_info(s3_resource_version_info: S3ResourceVersionInfo) -> None:
    """
    Upload the resource version information to S3 for documentation

    Parameters
    ----------
    s3_resource_version_info : clowm.schemas.resource.S3ResourceVersionInfo
        Resource information object that will be uploaded to S3.
    """
    buf = BytesIO(s3_resource_version_info.model_dump_json(indent=2).encode("utf-8"))
    async with background_dependencies.get_background_s3_resource() as s3:
        await upload_s3_obj(
            s3=s3,
            bucket_name=settings.s3.data_bucket,
            key=s3_resource_version_info.s3_path(),
            handle=buf,
            ExtraArgs={"ContentType": "application/json"},
        )


async def remove_permission_to_s3_resource_version(resource_version: ResourceVersionOut) -> None:
    """
    Remove the permission of the maintainer to upload a resource to S3.

    Parameters
    ----------
    resource_version : clowm.schemas.resource_version.ResourceVersionOut
        Information about the resource version.
    """
    async with background_dependencies.get_background_s3_resource() as s3:
        await delete_s3_bucket_policy_stmt(
            s3=s3, bucket_name=settings.s3.data_bucket, sid=str(resource_version.resource_version_id)
        )


async def delete_s3_resource(resource_id: UUID) -> None:
    """
    Delete all objects related to a resource in S3.

    Parameters
    ----------
    resource_id : uuid.UUID
        ID of the resource
    """
    async with background_dependencies.get_background_s3_resource() as s3:
        await delete_s3_objects(s3=s3, bucket_name=settings.s3.data_bucket, prefix=resource_dir_name(resource_id) + "/")


async def delete_s3_resource_version(resource_id: UUID, resource_version_id: UUID) -> None:
    """
    Delete all objects related to a resource version in S3.

    Parameters
    ----------
    resource_id : uuid.UUID
        ID of the resource
    resource_version_id : uuid.UUID
        ID of the resource version
    """
    try:
        async with background_dependencies.get_background_s3_resource() as s3:
            await delete_s3_objects(
                s3=s3,
                bucket_name=settings.s3.data_bucket,
                prefix=resource_version_dir_name(resource_id, resource_version_id),
            )
        async with get_db_context_manager() as db:
            await CRUDResourceVersion(db=db).update_status(
                status=ResourceVersion.ResourceVersionStatus.S3_DELETED, resource_version_id=resource_version_id
            )
    except ClientError:
        async with get_db_context_manager() as db:
            await CRUDResourceVersion(db=db).update_status(
                status=ResourceVersion.ResourceVersionStatus.S3_DELETE_ERROR,
                resource_version_id=resource_version_id,
            )


async def delete_resource_policy_stmt(resource: ResourceOut) -> None:
    """
    Delete all bucket policy statements regarding ths resource.

    Parameters
    ----------
    resource : clowm.schemas.resource.ResourceOut
        Information about the resource.
    """
    async with background_dependencies.get_background_s3_resource() as s3:
        await delete_s3_bucket_policy_stmt(
            s3=s3,
            bucket_name=settings.s3.data_bucket,
            sid=[str(resource.resource_id)]
            + [str(resource_version.resource_version_id) for resource_version in resource.versions],
        )
