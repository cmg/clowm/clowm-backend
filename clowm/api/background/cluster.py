import json
import time
from asyncio import sleep as async_sleep
from enum import StrEnum, unique
from os import environ
from pathlib import Path
from tempfile import SpooledTemporaryFile, mkdtemp
from typing import Any
from uuid import UUID

import botocore.client
import dotenv
from httpx import HTTPError
from mako.lookup import TemplateLookup
from opentelemetry import trace

from clowm.api.dependencies import services
from clowm.ceph.rgw import get_s3_keys
from clowm.ceph.s3 import copy_s3_obj, upload_s3_obj
from clowm.core.config import MonitorJobBackoffStrategy, settings
from clowm.crud import CRUDResourceVersion, CRUDUser, CRUDWorkflowExecution
from clowm.git_repository.abstract_repository import GitRepository
from clowm.models import ResourceVersion, WorkflowExecution
from clowm.schemas.resource import ResourceOut
from clowm.schemas.resource_version import ResourceVersionOut, resource_dir_name, resource_version_dir_name
from clowm.schemas.workflow_version import NextflowVersion
from clowm.slurm.abstract_client import JobState, SlurmClient
from clowm.slurm.client import get_slurm_client
from clowm.slurm.schemas import SlurmJob
from clowm.smtp.send_email import send_sync_error_email, send_sync_success_email
from clowm.utils.backoff_strategy import BackoffStrategy, ExponentialBackoff, LinearBackoff, NoBackoff
from clowm.utils.job import AsyncJob, Job

from ..background import dependencies as background_dependencies
from ..dependencies.crud import get_db_context_manager

_scripts_template_lookup = TemplateLookup(directories=["clowm/slurm/script_templates"], module_directory=mkdtemp())

tracer = trace.get_tracer_provider().get_tracer(__name__)

dotenv.load_dotenv()
execution_env: dict[str, str | int | bool] = {
    key[13:]: val for key, val in environ.items() if len(key) > 12 and key.startswith("WORKFLOW_ENV_")
}
execution_env["SCM_DIR"] = str(Path(settings.cluster.working_directory) / "scm")


@unique
class ShellScriptsTemplates(StrEnum):
    SYNCHRONIZE = "synchronize_resource_version"
    SET_LATEST = "set_latest_resource_version"
    DELETE = "delete_resource"
    DELETE_VERSION = "delete_resource_version"
    NEXTFLOW_COMMAND = "nextflow_command"

    def render(self, **kwargs: Any) -> str:
        """
        Render a resource script templates

        Parameters
        ----------
        kwargs : Any
            Arguments for the email template

        Returns
        -------
        script : str
            The rendered script
        """
        return _scripts_template_lookup.get_template(f"{self}.sh.tmpl").render(**kwargs)


async def update_db_resource_wrapper(**kwargs: Any) -> None:
    async with get_db_context_manager() as db:
        await CRUDResourceVersion(db=db).update_status(**kwargs)


async def cancel_slurm_job(job_id: int) -> None:
    """
    Cancel a slurm job by its id.

    Parameters
    ----------
    job_id : int
        Job id in slurm.
    """
    async with background_dependencies.get_background_http_client() as client:
        slurm_client = get_slurm_client(client)
        await slurm_client.cancel_job(job_id=job_id)


async def start_workflow_execution(
    execution: WorkflowExecution,
    parameters: dict[str, Any],
    git_repo: GitRepository,
    nextflow_version: NextflowVersion,
    scm_file_id: UUID | None = None,
    workflow_entrypoint: str | None = None,
    default_container: str | None = None,
) -> None:
    """
    Start a workflow on the Slurm cluster.

    Parameters
    ----------
    execution : clowm.models.WorkflowExecution
        Workflow execution to execute.
    parameters : dict[str, Any]
        Parameters for the workflow.
    git_repo : clowm.git_repository.abstract_repository.GitRepository
        Git repository of the workflow version.
    scm_file_id : UUID | None
        ID of the SCM file for private git repositories.
    workflow_entrypoint : str | None
        Entrypoint for the workflow by specifying the `-entry` parameter
    nextflow_version : clowm.schemas.workflow_version.NextflowVersion
        The version of nextflow for this execution
    default_container : str | None, default None
        The default container for untagged nextflow processes
    """
    with tracer.start_as_current_span(
        "background_start_workflow_execution",
        attributes={
            "execution_id": str(execution.execution_id),
            "executor_id": str(execution.executor_id),
            "git_commit_hash": git_repo.commit,
            "repository_url": git_repo.url,
        },
    ) as span:
        if workflow_entrypoint is not None:  # pragma: no cover
            span.set_attribute("workflow_entrypoint", workflow_entrypoint)
        if execution.workflow_version_id is not None:  # pragma: no cover
            span.set_attribute("workflow_version_id", execution.workflow_version_id)
        if execution.workflow_mode_id is not None:  # pragma: no cover
            span.set_attribute("workflow_mode_id", str(execution.workflow_mode_id))
        if default_container is not None:  # pragma: no cover
            span.set_attribute("default_container", default_container)

        async with background_dependencies.get_background_s3_resource() as s3:
            # Upload parameters to S3 data bucket
            with SpooledTemporaryFile(max_size=512000) as f:
                f.write(json.dumps(parameters).encode("utf-8"))
                f.seek(0)
                await upload_s3_obj(
                    bucket_name=settings.s3.data_bucket,
                    key=settings.s3.execution_parameters_key(execution.execution_id),
                    handle=f,
                    s3=s3,
                )
            if execution.logs_path is not None:
                # copy parameter json to logs path with name parameters.json
                await copy_s3_obj(
                    src_bucket_name=settings.s3.data_bucket,
                    src_key=settings.s3.execution_parameters_key(execution.execution_id),
                    dst_bucket_name=execution.logs_path.split("/")[2],
                    dst_key="/".join(execution.logs_path.split("/")[3:] + ["parameters.json"]),
                    s3=s3,
                )

        # Check if there is an SCM file for the workflow
        if scm_file_id is not None:
            with tracer.start_as_current_span("s3_check_workflow_scm_file") as scm_span:
                try:
                    scm_obj = await s3.Object(bucket_name=settings.s3.data_bucket, key=settings.s3.scm_key(scm_file_id))
                    await scm_obj.load()
                    scm_span.set_attribute("scm_file_id", str(scm_file_id))
                except botocore.client.ClientError:
                    scm_file_id = None

        nextflow_script = ShellScriptsTemplates.NEXTFLOW_COMMAND.render(
            repo=git_repo,
            execution_id=execution.execution_id,
            nextflow_version=nextflow_version,
            scm_file_id=scm_file_id,
            debug_s3_path=execution.debug_path,
            logs_s3_path=execution.logs_path,
            provenance_s3_path=execution.provenance_path,
            workflow_entrypoint=workflow_entrypoint,
            default_container=default_container,
        )

        rgw = services.get_rgw_admin()
        if execution.executor_id is None:  # pragma: no cover
            # this only happens if the user was deleted before the workflow was started
            return
        keys = get_s3_keys(rgw=rgw, uid=execution.executor_id)
        if len(keys) == 0:  # pragma: no cover
            raise KeyError("no s3 keys available")
        # Setup env for the workflow execution
        work_directory = str(Path(settings.cluster.working_directory) / f"run-{execution.execution_id.hex}")
        env = execution_env.copy()
        env["TOWER_WORKSPACE_ID"] = execution.execution_id.hex[:16]
        env["NXF_WORK"] = str(Path(work_directory) / "work")
        env["NXF_ANSI_LOG"] = False
        env["NXF_ASSETS"] = str(
            Path(env.get("NXF_ASSETS", "$HOME/.nextflow/assets")) / f"{git_repo.name}_{git_repo.commit}"  # type: ignore[arg-type]
        )
        env["AWS_ACCESS_KEY_ID"] = keys[0].access_key
        env["AWS_SECRET_ACCESS_KEY"] = keys[0].secret_key
        env["NXF_VER"] = nextflow_version
        env["NXF_JVM_ARGS"] = (
            str(execution_env.get("NXF_JVM_ARGS", ""))
            + f" -Xms{round(settings.cluster.head_job_memory.to('MiB') * 0.3)}m -Xmx{round(settings.cluster.head_job_memory.to('MiB'))}m"
        ).strip()

        try:
            job_submission = SlurmJob.model_validate(
                {
                    "script": nextflow_script.strip(),
                    "current_working_directory": settings.cluster.working_directory,
                    "environment": env,
                    "name": execution.execution_id.hex,
                    "requeue": False,
                    "standard_output": str(
                        Path(settings.cluster.working_directory) / f"slurm-{execution.execution_id.hex}.out"
                    ),
                    "partition": settings.cluster.slurm.partition,
                    "cpus_per_task": settings.cluster.head_job_cpus,
                    "memory_per_node": round(settings.cluster.head_job_memory.to("MiB")),
                },
            )

            async with background_dependencies.get_background_http_client() as client:
                slurm_client = get_slurm_client(client)
                # Try to start the job on the slurm cluster
                slurm_job_id = await slurm_client.submit_job(job=job_submission)
                async with get_db_context_manager() as db:
                    await CRUDWorkflowExecution(db=db).update_slurm_job_id(
                        slurm_job_id=slurm_job_id, execution_id=execution.execution_id
                    )
                if settings.cluster.job_monitoring != MonitorJobBackoffStrategy.NOMONITORING:

                    async def finish_job() -> None:
                        async with get_db_context_manager() as db:
                            crud_workflow_execution = CRUDWorkflowExecution(db=db)
                            fresh_execution = await crud_workflow_execution.get(execution_id=execution.execution_id)
                            # Check if the execution is marked as finished in the database
                            if fresh_execution is not None:
                                span.set_attribute("workflow_execution_status", str(fresh_execution.status))
                                if fresh_execution.end_time is None:
                                    # Mark job as finished with an error
                                    await crud_workflow_execution.update_status(
                                        execution_id=execution.execution_id,
                                        status=WorkflowExecution.WorkflowExecutionStatus.ERROR,
                                        end_timestamp=round(time.time()),
                                    )

                    await _monitor_proper_job_execution(
                        slurm_client=slurm_client,
                        failed_job=AsyncJob(finish_job),
                        success_job=AsyncJob(finish_job),
                        slurm_job_id=slurm_job_id,
                    )
        except (HTTPError, KeyError):
            # Mark job as aborted when there is an error
            async with get_db_context_manager() as db:
                await CRUDWorkflowExecution(db=db).update_status(
                    execution_id=execution.execution_id,
                    status=WorkflowExecution.WorkflowExecutionStatus.ERROR,
                    end_timestamp=round(time.time()),
                )


async def synchronize_cluster_resource(resource: ResourceOut, resource_version: ResourceVersionOut) -> None:
    """
    Synchronize a resource to the cluster

    Parameters
    ----------
    resource : clowm.schemas.resource.ResourceOut
        Resource schema of the corresponding version.
    resource_version : clowm.schemas.resource_version.ResourceVersionOut
        Resource version schema to synchronize.
    """
    synchronization_script = ShellScriptsTemplates.SYNCHRONIZE.render(
        s3_url=settings.s3.uri,
        resource_version_s3_folder=f"{settings.s3.data_bucket}/{resource_version_dir_name(resource_version.resource_id, resource_version.resource_version_id)}",
        resource_version_path=str(
            Path(settings.cluster.resource_cluster_path)
            / resource_version_dir_name(
                resource_version.resource_id, resource_version.resource_version_id, add_s3_prefix=False
            )
        ),
    )

    async def failed_job(error_msg: str | None = None) -> None:
        await update_db_resource_wrapper(
            resource_version_id=resource_version.resource_version_id,
            status=ResourceVersion.ResourceVersionStatus.SYNC_ERROR,
            resource_id=None,
        )
        await send_sync_error_email(resource=resource, version=resource_version, error_msg=error_msg)

    try:
        job_submission = SlurmJob.model_validate(
            {
                "script": synchronization_script.strip(),
                "current_working_directory": settings.cluster.working_directory,
                "name": f"Synchronize {str(resource_version.resource_version_id)}",
                "requeue": False,
                "standard_output": str(
                    Path(settings.cluster.working_directory)
                    / f"slurm-synchronize-{resource_version.resource_version_id.hex}.out"
                ),
                "environment": {
                    "AWS_ACCESS_KEY_ID": settings.s3.access_key,
                    "AWS_SECRET_ACCESS_KEY": settings.s3.secret_key.get_secret_value(),
                },
                "cpus_per_task": 1,
            }
        )
        async with background_dependencies.get_background_http_client() as client:
            slurm_client = get_slurm_client(client)
            # Try to start the job on the slurm cluster
            slurm_job_id = await slurm_client.submit_job(job=job_submission)

            await update_db_resource_wrapper(
                resource_version_id=resource_version.resource_version_id,
                status=ResourceVersion.ResourceVersionStatus.SYNCHRONIZING,
                resource_id=None,
                slurm_job_id=slurm_job_id,
            )

            async def success_job() -> None:
                user = None
                async with get_db_context_manager() as db:
                    crud_resource_version = CRUDResourceVersion(db=db)
                    version = await crud_resource_version.get(
                        resource_version_id=resource_version.resource_version_id,
                        resource_id=resource_version.resource_id,
                    )
                    if version is not None and version.synchronization_request_uid is not None:
                        user = await CRUDUser(db=db).get(uid=version.synchronization_request_uid)
                    await crud_resource_version.update_status(
                        resource_version_id=resource_version.resource_version_id,
                        status=ResourceVersion.ResourceVersionStatus.SYNCHRONIZED,
                        resource_id=resource_version.resource_id,
                        values={"synchronization_request_uid": None},
                    )

                if user is not None:
                    await send_sync_success_email(resource=resource, version=resource_version, requester=user)

            await _monitor_proper_job_execution(
                slurm_client=slurm_client,
                slurm_job_id=slurm_job_id,
                success_job=AsyncJob(success_job),
                failed_job=AsyncJob(failed_job),
            )
    except (HTTPError, KeyError) as e:  # pragma: no cover
        await failed_job(error_msg=str(e))


async def delete_cluster_resource(
    resource_id: UUID,
) -> None:
    """
    Synchronize a resource to the cluster

    Parameters
    ----------
    resource_id : uuid.UUID
        ID of the resource to delete
    """

    delete_script = ShellScriptsTemplates.DELETE.render(
        resource_path=str(
            Path(settings.cluster.resource_cluster_path) / resource_dir_name(resource_id, add_s3_prefix=False)
        )
    )

    try:
        job_submission = SlurmJob.model_validate(
            {
                "script": delete_script.strip(),
                "current_working_directory": settings.cluster.working_directory,
                "name": f"Delete {str(resource_id)}",
                "requeue": False,
                "standard_output": str(
                    Path(settings.cluster.working_directory) / f"slurm-delete-{resource_id.hex}.out"
                ),
            }
        )
        async with background_dependencies.get_background_http_client() as client:
            slurm_client = get_slurm_client(client)
            # Try to start the job on the slurm cluster
            slurm_job_id = await slurm_client.submit_job(job=job_submission)
            await _monitor_proper_job_execution(slurm_client=slurm_client, slurm_job_id=slurm_job_id)
    except (HTTPError, KeyError):  # pragma: no cover
        pass


async def set_cluster_resource_version_latest(resource_version: ResourceVersionOut) -> None:
    """
    Synchronize a resource to the cluster

    Parameters
    ----------
    resource_version : clowm.schemas.resource_version.ResourceVersionOut
        Resource version schema to synchronize.
    """
    set_latest_script = ShellScriptsTemplates.SET_LATEST.render(
        resource_version_path=str(
            Path(settings.cluster.resource_cluster_path)
            / resource_version_dir_name(
                resource_id=resource_version.resource_id,
                resource_version_id=resource_version.resource_version_id,
                add_s3_prefix=False,
            )
        ),
        latest_version_path=str(
            Path(settings.cluster.resource_cluster_path)
            / resource_dir_name(resource_id=resource_version.resource_id, add_s3_prefix=False)
            / "latest"
        ),
    )
    failed_job = AsyncJob(
        update_db_resource_wrapper,
        status=ResourceVersion.ResourceVersionStatus.SYNCHRONIZED,
        resource_version_id=resource_version.resource_version_id,
        resource_id=resource_version.resource_id,
    )
    job_submission = SlurmJob.model_validate(
        {
            "script": set_latest_script.strip(),
            "current_working_directory": settings.cluster.working_directory,
            "name": f"SET Latest {str(resource_version.resource_version_id)}",
            "requeue": False,
            "standard_output": str(
                Path(settings.cluster.working_directory)
                / f"slurm-set-latest-{resource_version.resource_version_id.hex}.out"
            ),
        }
    )
    try:
        async with background_dependencies.get_background_http_client() as client:
            slurm_client = get_slurm_client(client)
            # Try to start the job on the slurm cluster
            slurm_job_id = await slurm_client.submit_job(job=job_submission)
            await update_db_resource_wrapper(
                resource_version_id=resource_version.resource_version_id,
                status=ResourceVersion.ResourceVersionStatus.SETTING_LATEST,
                resource_id=None,
                slurm_job_id=slurm_job_id,
            )
            await _monitor_proper_job_execution(
                slurm_client=slurm_client,
                slurm_job_id=slurm_job_id,
                success_job=AsyncJob(
                    update_db_resource_wrapper,
                    status=ResourceVersion.ResourceVersionStatus.LATEST,
                    resource_version_id=resource_version.resource_version_id,
                    resource_id=resource_version.resource_id,
                ),
                failed_job=failed_job,
            )
    except (HTTPError, KeyError) as e:
        trace.get_current_span().record_exception(e)
        await failed_job()


async def delete_cluster_resource_version(
    resource_version: ResourceVersionOut,
) -> None:
    """
    Delete a resource version on the cluster

    Parameters
    ----------
    resource_version : clowm.schemas.resource_version.ResourceVersionOut
        Resource version schema to delete.
    """
    delete_script = ShellScriptsTemplates.DELETE_VERSION.render(
        resource_version_path=str(
            Path(settings.cluster.resource_cluster_path)
            / resource_version_dir_name(
                resource_id=resource_version.resource_id,
                resource_version_id=resource_version.resource_version_id,
                add_s3_prefix=False,
            )
        ),
        latest_version_path=str(
            Path(settings.cluster.resource_cluster_path)
            / resource_dir_name(resource_id=resource_version.resource_id, add_s3_prefix=False)
            / "latest"
        ),
    )

    try:
        job_submission = SlurmJob.model_validate(
            {
                "script": delete_script.strip(),
                "current_working_directory": settings.cluster.working_directory,
                "name": f"Delete {str(resource_version.resource_version_id)}",
                "requeue": False,
                "standard_output": str(
                    Path(settings.cluster.working_directory)
                    / f"slurm-delete-version-{resource_version.resource_version_id.hex}.out"
                ),
            }
        )
        async with background_dependencies.get_background_http_client() as client:
            slurm_client = get_slurm_client(client)
            # Try to start the job on the slurm cluster
            slurm_job_id = await slurm_client.submit_job(job=job_submission)
            await update_db_resource_wrapper(
                resource_version_id=resource_version.resource_version_id,
                status=ResourceVersion.ResourceVersionStatus.CLUSTER_DELETING,
                resource_id=None,
                slurm_job_id=slurm_job_id,
            )
            await _monitor_proper_job_execution(
                slurm_client=slurm_client,
                slurm_job_id=slurm_job_id,
                success_job=AsyncJob(
                    update_db_resource_wrapper,
                    status=ResourceVersion.ResourceVersionStatus.APPROVED,
                    resource_version_id=resource_version.resource_version_id,
                    resource_id=None,
                    values={"cluster_deleted_timestamp": round(time.time())},
                ),
                failed_job=AsyncJob(
                    update_db_resource_wrapper,
                    status=ResourceVersion.ResourceVersionStatus.CLUSTER_DELETE_ERROR,
                    resource_version_id=resource_version.resource_version_id,
                    resource_id=resource_version.resource_id,
                ),
            )
    except (HTTPError, KeyError):  # pragma: no cover
        pass


async def _monitor_proper_job_execution(
    slurm_client: SlurmClient, slurm_job_id: int, success_job: Job | None = None, failed_job: Job | None = None
) -> None:
    """
    Check in an interval based on a backoff strategy if the slurm job is still running
    the workflow execution in the database is not marked as finished.

    Parameters
    ----------
    slurm_client : clowm.slurm.rest_client.SlurmClient
        Slurm Rest Client to communicate with Slurm cluster.
    slurm_job_id : int
        ID of the slurm job to monitor
    success_job : clowm.utils.Job | None
        Function to execute after the slurm job was successful
    failed_job : clowm.utils.Job | None
        Function to execute after the slurm job was unsuccessful
    """
    sleep_generator: BackoffStrategy
    if settings.cluster.job_monitoring == MonitorJobBackoffStrategy.EXPONENTIAL:  # pragma: no cover
        # exponential to 50 minutes
        sleep_generator = ExponentialBackoff(max_value=300)
    elif settings.cluster.job_monitoring == MonitorJobBackoffStrategy.LINEAR:  # pragma: no cover
        # 5 seconds increase to 5 minutes
        sleep_generator = LinearBackoff(backoff=5, max_value=300)
    elif settings.cluster.job_monitoring == MonitorJobBackoffStrategy.CONSTANT:  # pragma: no cover
        # constant 30 seconds polling
        sleep_generator = NoBackoff(constant_value=30)
    else:  # pragma: no cover
        return

    # Closure around monitor_job code
    async def monitor_job() -> None:
        with tracer.start_span("monitor_job", attributes={"slurm_job_id": slurm_job_id}):
            job_state = await slurm_client.get_job_state(slurm_job_id)
            if job_state != JobState.RUNNING:
                if job_state == JobState.SUCCESS and success_job is not None:
                    if success_job.is_async:
                        await success_job()
                    else:  # pragma: no cover
                        success_job()
                elif job_state == JobState.ERROR and failed_job is not None:
                    if failed_job.is_async:
                        await failed_job()
                    else:  # pragma: no cover
                        failed_job()
                sleep_generator.close()

    await monitor_job()
    for sleep_time in sleep_generator:  # pragma: no cover
        await async_sleep(sleep_time)
        await monitor_job()
