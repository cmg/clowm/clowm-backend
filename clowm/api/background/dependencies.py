from collections.abc import AsyncIterator
from contextlib import asynccontextmanager
from typing import TYPE_CHECKING

from httpx import AsyncClient

from clowm.ceph.s3 import boto_session
from clowm.core.config import settings

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object


@asynccontextmanager
async def get_background_http_client() -> AsyncIterator[AsyncClient]:  # pragma: no cover
    async with AsyncClient(http2=True) as client:
        yield client


@asynccontextmanager
async def get_background_s3_resource() -> AsyncIterator[S3ServiceResource]:  # pragma: no cover
    async with boto_session.resource(
        service_name="s3",
        endpoint_url=str(settings.s3.uri)[:-1],
        verify=settings.s3.uri.scheme == "https",
    ) as s3_resource:
        yield s3_resource
