from typing import Annotated
from uuid import UUID

from fastapi import Depends, HTTPException, status
from opentelemetry import trace
from rgwadmin import RGWAdmin
from rgwadmin.exceptions import RGWAdminException

from clowm.api.dependencies import RGWService

__all__ = ["S3KeyCoreDep", "S3KeyCore"]

from clowm.ceph.rgw import get_s3_keys
from clowm.schemas.s3key import S3Key

tracer = trace.get_tracer_provider().get_tracer(__name__)


class S3KeyCore:
    """
    Class bundling the core API functionality for S3 keys. Authorization is not included and has to be done before.
    """

    def __init__(self, rgw: RGWAdmin) -> None:
        self.rgw = rgw

    def create(self, uid: UUID) -> S3Key:
        """
        Create a S3 key for a user.

        Parameters
        ----------
        uid : uuid.UUID

        Returns
        -------

        """
        before_keys_set = set(map(lambda key: key.access_key, get_s3_keys(self.rgw, uid)))
        with tracer.start_as_current_span("rgw_create_key", attributes={"uid": str(uid)}):
            # create keys returns all keys for a user including the new one
            after_keys = self.rgw.create_key(uid=uid, key_type="s3", generate_key=True)
        new_key_id = list(set(map(lambda key: key["access_key"], after_keys)) - before_keys_set)[
            0
        ]  # find ID of the key
        index = [key["access_key"] for key in after_keys].index(new_key_id)  # find new key by ID
        return S3Key(uid=uid, **after_keys[index])

    def get(self, access_key: str, uid: UUID) -> S3Key:
        keys = get_s3_keys(self.rgw, uid)
        try:
            index = [key.access_key for key in keys].index(access_key)
            return keys[index]
        except ValueError as err:
            raise HTTPException(status.HTTP_404_NOT_FOUND, detail="Key not found") from err

    def delete(self, access_key: str, uid: UUID) -> None:
        if len(get_s3_keys(self.rgw, uid)) <= 1:
            raise HTTPException(status.HTTP_400_BAD_REQUEST, detail="It's not possible to delete the last key")
        try:
            with tracer.start_as_current_span("rgw_delete_key", attributes={"uid": str(uid)}):
                self.rgw.remove_key(access_key=access_key, uid=str(uid))
        except RGWAdminException as err:
            raise HTTPException(status.HTTP_404_NOT_FOUND, detail="Key not found") from err


def s3key_core_dependency(rgw: RGWService) -> S3KeyCore:
    """
    Get the core functionality for s3_keys with the DB connection injected.

    Parameters
    ----------
    rgw : rgwadmin.RGWAdmin
        The RGWAdmin object from clowm.ceph.rgw

    Returns
    -------
    s3_key_core : S3KeyCore
        Core API functionality for s3 keys
    """
    return S3KeyCore(rgw=rgw)


S3KeyCoreDep = Annotated[S3KeyCore, Depends(s3key_core_dependency)]
