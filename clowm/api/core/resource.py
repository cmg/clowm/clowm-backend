import os
import tempfile
from io import BytesIO
from typing import TYPE_CHECKING, Annotated
from uuid import UUID

from fastapi import BackgroundTasks, Depends, HTTPException, status
from fastapi.requests import Request
from fastapi.responses import FileResponse, Response
from opentelemetry import trace
from starlette.background import BackgroundTask

from clowm.ceph.s3 import get_s3_object
from clowm.core.config import settings
from clowm.crud import CRUDResource, CRUDResourceVersion
from clowm.models import Resource, ResourceVersion, User
from clowm.schemas.resource import ResourceIn, ResourceOut, S3ResourceVersionInfo
from clowm.schemas.resource_version import (
    ResourceVersionIn,
    ResourceVersionOut,
    resource_version_dir_name,
    resource_version_key,
)

from ...smtp.send_email import send_resource_review_request_email
from ..background.cluster import delete_cluster_resource
from ..background.s3 import (
    add_s3_resource_version_info,
    delete_resource_policy_stmt,
    delete_s3_resource,
    give_permission_to_s3_resource,
    give_permission_to_s3_resource_version,
    remove_permission_to_s3_resource_version,
)
from ..dependencies import CRUDSessionDep, S3Service

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object

__all__ = ["ResourceCoreDep", "ResourceCore"]

CrudResourceSession = Annotated[CRUDResource, Depends(CRUDSessionDep(CRUDResource))]
tracer = trace.get_tracer_provider().get_tracer(__name__)


class ResourceCore:
    """
    Class bundling the core API functionality for resources. Authorization is not included and has to be done before.
    """

    pass

    def __init__(self, crud_resource: CRUDResource, background_tasks: BackgroundTasks, s3: S3ServiceResource) -> None:
        self.crud_resource = crud_resource
        self.crud_resource_version = CRUDResourceVersion(db=crud_resource.db)
        self.background_tasks = background_tasks
        self.s3 = s3

    async def create(self, resource_in: ResourceIn, current_user: User) -> Resource:
        """
        Request a new resources.

        Parameters
        ----------
        resource_in : clowm.schemas.resource.ResourceIn
            Data about the new resource
        current_user : clowm.models.User
            User who wants to create the resource

        Returns
        -------
        resource : clowm.models.Resource
            Newly created resource
        """
        if await self.crud_resource.get_by_name(name=resource_in.name) is not None:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"Resource with name '{resource_in.name}' already exists",
            )
        resource = await self.crud_resource.create(resource_in=resource_in, maintainer_id=current_user.uid)
        resource_out = ResourceOut.from_db_resource(db_resource=resource)
        self.background_tasks.add_task(give_permission_to_s3_resource, resource=resource_out)
        self.background_tasks.add_task(
            give_permission_to_s3_resource_version,
            resource_version=resource_out.versions[0],
            maintainer_id=resource_out.maintainer_id,  # type: ignore[arg-type]
        )
        self.background_tasks.add_task(
            add_s3_resource_version_info,
            s3_resource_version_info=S3ResourceVersionInfo.from_models(
                resource=resource, resource_version=resource.versions[0], maintainer=current_user
            ),
        )
        return resource

    async def create_version(
        self,
        resource: Resource,
        version_in: ResourceVersionIn,
        current_user: User,
    ) -> ResourceVersion:
        """
        Request a new resource version.
        Parameters
        ----------
        resource : clowm.models.Resource
            Resource that should get a new version
        version_in : clowm.schemas.resource_version.ResourceVersionIn
            Data about the new resource version.
        current_user : clowm.models.User
            User associated with the request.

        Returns
        -------
        versions : list[clowm.schemas.models.ResourceVersion]
            Newly created resource version
        """
        resource_version = await self.crud_resource_version.create(
            resource_id=resource.resource_id, release=version_in.release
        )
        resource_version_out = ResourceVersionOut.from_db_resource_version(resource_version)
        self.background_tasks.add_task(
            give_permission_to_s3_resource_version,
            resource_version=resource_version_out,
            maintainer_id=current_user.uid,
        )
        self.background_tasks.add_task(
            add_s3_resource_version_info,
            s3_resource_version_info=S3ResourceVersionInfo.from_models(
                resource=resource, resource_version=resource.versions[0], maintainer=current_user
            ),
        )
        return resource_version

    async def download_resource_file_tree(self, resource_version: ResourceVersion, *, request: Request) -> Response:
        """
        Download the file tree file for a resource version.

        Parameters
        ----------
        resource_version : clowm.models.ResourceVersion
            Resource Version to get the file for.
        request : fastapi.requests.Request
            Raw request object to read headers from.

        Returns
        -------
        response : fastapi.responses.Response
            Raw response object containing the file
        """
        if not resource_version.status.public:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"can't fetch folder structure of resource version with status {resource_version.status.name}",
            )
        obj = await get_s3_object(
            s3=self.s3,
            bucket_name=settings.s3.data_bucket,
            key=resource_version_dir_name(resource_version.resource_id, resource_version.resource_version_id)
            + "/tree.json",
        )
        if obj is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="resource version was not synchronized to the cluster yet"
            )
        etag = await obj.e_tag
        # check if browser has the correct object still cached
        if etag == request.headers.get("If-None-Match", None):
            return Response(status_code=status.HTTP_304_NOT_MODIFIED)
        # if obj is larger than 10 MiB, download it to file and stream the file to the client
        if await obj.size > 10485760:
            _, file = tempfile.mkstemp()
            with tracer.start_as_current_span(
                "s3_download_obj_to_file",
                attributes={
                    "bucket_name": settings.s3.data_bucket,
                    "key": resource_version_dir_name(resource_version.resource_id, resource_version.resource_version_id)
                    + "/tree.json",
                    "etag": await obj.e_tag,
                    "content-length": await obj.size,
                    "file_path": file,
                },
            ):
                await (await obj.Object()).download_file(file)
            # stream file to client and delete it afterward
            return FileResponse(
                file,
                headers={"etag": etag},
                media_type="application/json",
                background=BackgroundTask(os.remove, path=file),
            )
        # Download file in memory and send it to the client
        with BytesIO() as f:
            with tracer.start_as_current_span(
                "s3_download_obj_in_memory",
                attributes={
                    "bucket_name": settings.s3.data_bucket,
                    "key": resource_version_dir_name(resource_version.resource_id, resource_version.resource_version_id)
                    + "/tree.json",
                    "etag": await obj.e_tag,
                    "content-length": await obj.size,
                },
            ):
                await (await obj.Object()).download_fileobj(f)
            f.seek(0)
            return Response(content=f.read(), headers={"etag": etag}, media_type="application/json")

    async def request_version_review(
        self,
        resource: Resource,
        version: ResourceVersion,
        current_uid: UUID,
    ) -> ResourceVersion:
        """
        Request the review of a resource version.

        Parameters
        ----------
        resource : clowm.models.Resource
            Resource of the version.
        version : clowm.models.ResourceVersion
            Version for which the review is requested.
        current_uid : uuid.UUID
            Uid of the user associated with the request.

        Returns
        -------
        version : clowm.schemas.resource_version.ResourceVersionOut
            Resource version with updated status
        """
        if current_uid != resource.maintainer_id:
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN, detail="only the maintainer can ask for a review"
            )
        if version.status is not ResourceVersion.ResourceVersionStatus.RESOURCE_REQUESTED:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"Can't request sync for resource version with status {version.status.name}",
            )
        resource_version_obj = await get_s3_object(
            s3=self.s3,
            bucket_name=settings.s3.data_bucket,
            key=resource_version_key(resource_id=version.resource_id, resource_version_id=version.resource_version_id),
        )
        resource_version_out = ResourceVersionOut.from_db_resource_version(version)
        if resource_version_obj is None:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"Missing resource at S3 path {resource_version_out.s3_path}",
            )
        compressed_size = await resource_version_obj.size
        resource_version_out.compressed_size = compressed_size
        await self.crud_resource_version.update_status(
            resource_version_id=version.resource_version_id,
            status=ResourceVersion.ResourceVersionStatus.WAIT_FOR_REVIEW,
            values={"compressed_size": compressed_size},
        )
        resource_version_out.status = ResourceVersion.ResourceVersionStatus.WAIT_FOR_REVIEW
        self.background_tasks.add_task(
            remove_permission_to_s3_resource_version,
            resource_version=resource_version_out,
        )
        self.background_tasks.add_task(
            send_resource_review_request_email,
            resource=ResourceOut.from_db_resource(resource),
            version=ResourceVersionOut.from_db_resource_version(version),
        )
        return version

    async def delete(self, resource: Resource) -> None:
        """
        Delete a resource.

        Parameters
        ----------
        resource : clowm.models.Resource
            Resource to be deleted.
        """
        await self.crud_resource.delete(resource_id=resource.resource_id)
        self.background_tasks.add_task(delete_resource_policy_stmt, resource=ResourceOut.from_db_resource(resource))
        self.background_tasks.add_task(delete_s3_resource, resource_id=resource.resource_id)
        self.background_tasks.add_task(delete_cluster_resource, resource_id=resource.resource_id)


def resource_core_dependency(
    crud_resource: CrudResourceSession, background_tasks: BackgroundTasks, s3: S3Service
) -> ResourceCore:
    """
    Get the core functionality for resources with the DB connection injected.

    Parameters
    ----------
    crud_resource: clowm.crud.crud_resource.CRUDResource
        Active CRUD repository for resources. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Dependency Injection.
    s3 : types_aiobotocore_s3.service_resource.S3ServiceResource
        S3 Service to perform operations on buckets.

    Returns
    -------
    resource_core : ResourceCore
        Core API functionality for resources
    """
    return ResourceCore(crud_resource=crud_resource, background_tasks=background_tasks, s3=s3)


ResourceCoreDep = Annotated[ResourceCore, Depends(resource_core_dependency)]
