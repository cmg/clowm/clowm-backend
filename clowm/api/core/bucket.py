from collections.abc import Iterable
from typing import TYPE_CHECKING, Annotated, Any
from uuid import UUID

from botocore.exceptions import ClientError
from fastapi import Depends, HTTPException, status
from opentelemetry import trace
from rgwadmin import RGWAdmin

from clowm.ceph.rgw import update_bucket_limits as rgw_update_bucket_limits
from clowm.ceph.s3 import (
    add_s3_bucket_policy_stmt,
    cors_rule,
    delete_s3_bucket_policy_stmt,
    delete_s3_objects,
    get_base_policy,
)
from clowm.core.config import settings
from clowm.crud import CRUDBucket, DuplicateError
from clowm.models import Bucket
from clowm.schemas.bucket import BucketIn

from ..dependencies import RGWService, S3Service
from ..dependencies.crud import CRUDSessionDep

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object

__all__ = ["BucketCoreDep", "BucketCore"]

CrudBucketSession = Annotated[CRUDBucket, Depends(CRUDSessionDep(CRUDBucket))]
tracer = trace.get_tracer_provider().get_tracer(__name__)

ANONYMOUS_ACCESS_SID = "AnonymousAccess"


class BucketCore:
    """
    Class bundling the core API functionality for buckets. Authorization is not included and has to be done before.
    """

    def __init__(self, crud_bucket: CRUDBucket, rgw: RGWAdmin, s3: S3ServiceResource) -> None:
        self.crud_bucket = crud_bucket
        self.s3 = s3
        self.rgw = rgw

    async def list_buckets(
        self, owner_id: UUID | None = None, bucket_type: CRUDBucket.BucketType = CRUDBucket.BucketType.ALL
    ) -> Iterable[Bucket]:
        """
        List als buckets in the system.

        Parameters
        ----------
        owner_id : uuid.UUID | None, default None
            Filter for buckets with a specific owner
        bucket_type : clowm.crud.crud_bucket.CRUDBucket.BucketType, default BucketType.ALL
            Type of the bucket to get.

        Returns
        -------
        buckets : list[clowm.models.Bucket]
            All the buckets based on the filters.
        """
        if owner_id is None:
            buckets = await self.crud_bucket.list()
        else:
            buckets = await self.crud_bucket.list_for_user(owner_id, bucket_type)

        return buckets

    async def create_bucket(self, bucket: BucketIn, owner_id: UUID) -> Bucket:
        """
        Create a bucket in the system.

        Parameters
        ----------
        bucket: clowm.schemas.bucket,BucketIn
             Information about the bucket to create.
        owner_id : uuid.UUID
            Id of the owner for the bucket.

        Returns
        -------
        bucket : clowm.models.Bucket
            The newly created bucket.
        """
        try:
            db_bucket = await self.crud_bucket.create(
                bucket,
                owner_id,
                size_limit=int(settings.s3.default_bucket_size_limit.to("KiB")),
                object_limit=settings.s3.default_bucket_object_limit,
            )
        except DuplicateError as e:
            trace.get_current_span().record_exception(e)
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Bucket name is already taken",
            ) from e
        with tracer.start_as_current_span("s3_create_bucket", attributes={"bucket_name": db_bucket.name}):
            s3_bucket = await self.s3.Bucket(db_bucket.name)
            await s3_bucket.create()
        # Add basic permission to the user for getting, creating and deleting objects in the bucket.
        await add_s3_bucket_policy_stmt(
            *get_base_policy(bucket_name=db_bucket.name, uid=owner_id),
            bucket_name=bucket.name,
            s3=self.s3,
        )
        with tracer.start_as_current_span("s3_put_bucket_cors_rules", attributes={"bucket_name": db_bucket.name}):
            cors = await s3_bucket.Cors()
            await cors.put(CORSConfiguration=cors_rule)  # type: ignore[arg-type]
        rgw_update_bucket_limits(rgw=self.rgw, bucket=db_bucket)
        return db_bucket

    async def delete_bucket(self, bucket_name: str, force_delete: bool = False) -> None:
        """
        Delete a bucket by its name.

        Parameters
        ----------
        bucket_name : str
            Name of the bucket to delete.
        force_delete : bool, default False
            Flag for deleting a non-empty bucket. Query parameter.
        """
        if force_delete:
            await delete_s3_objects(bucket_name=bucket_name, s3=self.s3)
        try:
            with tracer.start_as_current_span("s3_delete_bucket", attributes={"bucket_name": bucket_name}):
                s3_bucket = await self.s3.Bucket(name=bucket_name)
                await s3_bucket.delete()
            await self.crud_bucket.delete(bucket_name)
        except ClientError as err:
            raise HTTPException(status.HTTP_400_BAD_REQUEST, detail="Bucket not empty") from err

    async def update_public_state(self, bucket: Bucket, public: bool) -> Bucket:
        """
        Update the buckets public state.

        Parameters
        ----------
        bucket : clowm.models.Bucket
            Bucket for which the state needs to be changed..
        public : bool
            The new public state.

        Returns
        -------
        bucket : clowm.models.Bucket
            Bucket with the updated public state.
        """
        if bucket.public == public:
            return bucket
        if public:
            await add_s3_bucket_policy_stmt(
                *BucketCore.get_anonymously_bucket_policy(bucket_name=bucket.name), bucket_name=bucket.name, s3=self.s3
            )
        else:
            await delete_s3_bucket_policy_stmt(bucket_name=bucket.name, sid=ANONYMOUS_ACCESS_SID, s3=self.s3)
        await self.crud_bucket.update_public_state(public=public, bucket_name=bucket.name)
        return bucket

    @staticmethod
    def get_anonymously_bucket_policy(bucket_name: str) -> list[dict[str, Any]]:
        """
        Get the bucket policy for a bucket to make it public.

        Parameters
        ----------
        bucket_name : str
            Name of a bucket

        Returns
        -------
        policy : list[dict[str, Any]]
            List of policy statements
        """
        return [
            {
                "Sid": ANONYMOUS_ACCESS_SID,
                "Effect": "Allow",
                "Principal": "*",
                "Resource": f"arn:aws:s3:::{bucket_name}/*",
                "Action": ["s3:GetObject"],
            },
            {
                "Sid": ANONYMOUS_ACCESS_SID,
                "Effect": "Allow",
                "Principal": "*",
                "Resource": f"arn:aws:s3:::{bucket_name}",
                "Action": ["s3:ListBucket"],
            },
        ]


def bucket_core_dependency(crud_bucket: CrudBucketSession, s3: S3Service, rgw: RGWService) -> BucketCore:
    """
    Get the core functionality for buckets with the DB connection injected.

    Parameters
    ----------
    crud_bucket: clowm.crud.crud_bucket.CRUDBucket
        Active CRUD repository for buckets. Dependency Injection.
    s3 : types_aiobotocore_s3.service_resource.S3ServiceResource
        S3 Service to perform operations on buckets.
    rgw : rgwadmin.RGWAdmin
        The RGWAdmin object from clowm.ceph.rgw

    Returns
    -------
    bucket_core : BucketCore
        Core API functionality for buckets
    """
    return BucketCore(crud_bucket=crud_bucket, s3=s3, rgw=rgw)


BucketCoreDep = Annotated[BucketCore, Depends(bucket_core_dependency)]
