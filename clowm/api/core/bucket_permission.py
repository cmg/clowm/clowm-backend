import json
from typing import TYPE_CHECKING, Annotated
from uuid import UUID

from fastapi import Depends, HTTPException, status
from opentelemetry import trace

from clowm.ceph.s3 import (
    add_s3_bucket_policy_stmt,
    delete_s3_bucket_policy_stmt,
    get_s3_bucket_policy,
    put_s3_bucket_policy,
)
from clowm.crud import CRUDBucket, CRUDBucketPermission, CRUDUser, DuplicateError
from clowm.models import BucketPermission
from clowm.schemas.bucket_permission import BucketPermissionIn, BucketPermissionOut, BucketPermissionParameters

from ..dependencies import S3Service
from ..dependencies.bucket import get_current_bucket
from ..dependencies.crud import CRUDSessionDep
from ..dependencies.user import get_path_user

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object

__all__ = ["BucketPermissionCoreDep", "BucketPermissionCore"]

CrudBucketPermissionSession = Annotated[CRUDBucketPermission, Depends(CRUDSessionDep(CRUDBucketPermission))]
tracer = trace.get_tracer_provider().get_tracer(__name__)

ANONYMOUS_ACCESS_SID = "AnonymousAccess"


class BucketPermissionCore:
    """
    Class bundling the core API functionality for bucket permissions. Authorization is not included and
    has to be done before.
    """

    def __init__(self, crud_bucket_permission: CRUDBucketPermission, s3: S3ServiceResource) -> None:
        self.crud_bucket_permission = crud_bucket_permission
        self.s3 = s3

    async def create_permission(self, permission: BucketPermissionIn, current_uid: UUID) -> BucketPermission:
        """
        Create a permission for a bucket and user.

        Parameters
        ----------
        permission : clowm.schemas.bucket_permission.BucketPermissionOut
            Information about the permission which should be created
        current_uid : uuid.UUID
            ID of the user associated with the request

        Returns
        -------
        permissions : clowm.schemas.bucket_permission.BucketPermissionOut
            Newly created permission.
        """
        target_bucket = await get_current_bucket(
            permission.bucket_name, crud_bucket=CRUDBucket(db=self.crud_bucket_permission.db)
        )  # Check if the target bucket exists
        if target_bucket.owner_id != current_uid:
            raise HTTPException(status.HTTP_403_FORBIDDEN, detail="Action forbidden.")
        await get_path_user(
            uid=permission.uid, crud_user=CRUDUser(db=self.crud_bucket_permission.db)
        )  # Check if target user exists
        try:
            permission_db = await self.crud_bucket_permission.create(permission)
        except ValueError as e:
            trace.get_current_span().record_exception(e)
            raise HTTPException(
                status.HTTP_400_BAD_REQUEST, detail="The owner of the bucket can't get any more permissions"
            ) from e
        except DuplicateError as e:
            trace.get_current_span().record_exception(e)
            raise HTTPException(
                status.HTTP_400_BAD_REQUEST,
                detail=f"Permission for combination of bucket={permission.bucket_name} and user={permission.uid} already exists",
                # noqa:E501
            ) from e
        await add_s3_bucket_policy_stmt(
            *permission.map_to_bucket_policy_statement(), bucket_name=permission.bucket_name, s3=self.s3
        )
        return permission_db

    async def update_permission(
        self, permission: BucketPermission, update_parameters: BucketPermissionParameters
    ) -> BucketPermission:
        """

        Parameters
        ----------
        permission : clowm.models.BucketPermission
            Permission that should get updated
        update_parameters : clowm.schemas.bucket_permission.BucketPermissionOut
            Information about the permission which should be updated.

        Returns
        -------
        permissions : clowm.schemas.bucket_permission.BucketPermissionOut
            Updated permission.
        """
        updated_permission = await self.crud_bucket_permission.update_permission(permission, update_parameters)
        updated_permission_schema = BucketPermissionOut.from_db_model(updated_permission)
        policy = await get_s3_bucket_policy(s3=self.s3, bucket_name=permission.bucket_name)
        policy["Statement"] = [
            stmt for stmt in policy["Statement"] if stmt["Sid"] != updated_permission_schema.to_hash()
        ]
        policy["Statement"] += updated_permission_schema.map_to_bucket_policy_statement()
        await put_s3_bucket_policy(s3=self.s3, bucket_name=permission.bucket_name, policy=json.dumps(policy))
        return updated_permission

    async def delete_permission(self, permission: BucketPermission, delete_db: bool = True) -> None:
        """
        Delete a bucket permission.

        Parameters
        ----------
        permission : clowm.models.BucketPermission
            Permission to delete
        delete_db : bool, default True
            Flag if the permission should be explicitly deleted in the database

        """
        if delete_db:
            await self.crud_bucket_permission.delete(bucket_name=permission.bucket_name, uid=permission.uid)
        bucket_permission_schema = BucketPermissionOut.from_db_model(permission)
        await delete_s3_bucket_policy_stmt(
            bucket_name=bucket_permission_schema.bucket_name, s3=self.s3, sid=bucket_permission_schema.to_hash()
        )


def bucket_permission_core_dependency(
    crud_bucket_permission: CrudBucketPermissionSession, s3: S3Service
) -> BucketPermissionCore:
    """
    Get the core functionality for buckets with the DB connection injected.

    Parameters
    ----------
    crud_bucket_permission: clowm.crud.CRUDBucketPermission
        Active CRUD repository for buckets permissions. Dependency Injection.
    s3 : types_aiobotocore_s3.service_resource.S3ServiceResource
        S3 Service to perform operations on buckets. Dependency Injection.

    Returns
    -------
    permission_core : BucketPermissionCore
        Core API functionality for bucket permissions
    """
    return BucketPermissionCore(crud_bucket_permission=crud_bucket_permission, s3=s3)


BucketPermissionCoreDep = Annotated[BucketPermissionCore, Depends(bucket_permission_core_dependency)]
