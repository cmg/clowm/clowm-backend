import json
import time
from pathlib import Path
from tempfile import SpooledTemporaryFile, TemporaryDirectory
from typing import TYPE_CHECKING, Annotated
from uuid import UUID

import jsonschema
from anyio import open_file
from botocore.exceptions import ClientError
from fastapi import BackgroundTasks, Depends, HTTPException, status
from fastapi.requests import Request
from fastapi.responses import Response
from httpx import AsyncClient
from opentelemetry import trace
from pydantic import AnyHttpUrl

from clowm.core.config import settings
from clowm.crud import CRUDResourceVersion, CRUDWorkflowExecution
from clowm.git_repository import GitHubRepository, build_repository
from clowm.models import WorkflowExecution, WorkflowVersion
from clowm.schemas.documentation_enum import DocumentationEnum
from clowm.schemas.workflow_execution import DevWorkflowExecutionIn, WorkflowExecutionIn
from clowm.schemas.workflow_version import NextflowVersion
from clowm.scm import SCM, SCMProvider

from ..background.cluster import cancel_slurm_job, start_workflow_execution
from ..background.s3 import delete_from_data_bucket, download_file_to_data_bucket, upload_scm_file
from ..dependencies import CRUDSessionDep, HTTPClient, S3Service
from ..utils import (
    check_active_workflow_execution_limit,
    check_repo,
    check_used_resources,
    flatten_parameters,
    stream_response_from_url,
)

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object

__all__ = ["WorkflowExecutionCoreDep", "WorkflowExecutionCore"]

CrudWorkflowExecutionSession = Annotated[CRUDWorkflowExecution, Depends(CRUDSessionDep(CRUDWorkflowExecution))]
tracer = trace.get_tracer_provider().get_tracer(__name__)


class WorkflowExecutionCore:
    """
    Class bundling the core API functionality for workflow executions. Authorization is not included and
    has to be done before.
    """

    pass

    def __init__(
        self,
        crud_workflow_execution: CRUDWorkflowExecution,
        client: AsyncClient,
        background_tasks: BackgroundTasks,
        s3: S3ServiceResource,
    ) -> None:
        self.crud_workflow_execution = crud_workflow_execution
        self.background_tasks = background_tasks
        self.s3 = s3
        self.client = client

    async def start_workflow(
        self,
        current_uid: UUID,
        workflow_execution_in: WorkflowExecutionIn,
        workflow_version: WorkflowVersion,
    ) -> WorkflowExecution:
        current_span = trace.get_current_span()

        # Check active workflow execution limit
        await check_active_workflow_execution_limit(current_uid, crud_workflow_execution=self.crud_workflow_execution)

        if len(workflow_version.workflow_modes) > 0 and workflow_execution_in.mode_id is None:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"A workflow mode needs to be specified for the workflow version '{workflow_execution_in.workflow_version_id}'",
                # noqa: E501
            )

        # If a workflow mode is specified, check that the mode is associated with the workflow version
        workflow_mode = None
        if workflow_execution_in.mode_id is not None:
            current_span.set_attribute("workflow_mode_id", str(workflow_execution_in.mode_id))
            workflow_mode = next(
                (mode for mode in workflow_version.workflow_modes if mode.mode_id == workflow_execution_in.mode_id),
                None,
            )
            if workflow_mode is None:
                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND,
                    detail=f"Workflow mode '{workflow_execution_in.mode_id}' does not exist on version '{workflow_execution_in.workflow_version_id}'",
                    # noqa: E501
                )

        # Check that the version can be used for execution
        if workflow_version.status not in [
            WorkflowVersion.WorkflowVersionStatus.PUBLISHED,
            WorkflowVersion.WorkflowVersionStatus.CREATED,
        ]:
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail=f"Workflow version with status {workflow_version.status} can't be started.",
            )

        # Validate schema with saved schema in bucket
        with SpooledTemporaryFile(max_size=512000) as f:
            try:
                with tracer.start_as_current_span("s3_download_workflow_parameter_schema"):
                    obj = await self.s3.Object(
                        bucket_name=settings.s3.data_bucket,
                        key=settings.s3.workflow_cache_key(
                            name=DocumentationEnum.PARAMETER_SCHEMA,
                            git_commit_hash=workflow_execution_in.workflow_version_id,
                            mode_id=workflow_execution_in.mode_id,
                        ),
                    )
                    await obj.download_fileobj(f)
            except ClientError as e:  # if schema is not in bucket, download it from repo and cache it again in bucket
                repo = build_repository(
                    url=workflow_version.workflow.repository_url,
                    git_commit_hash=workflow_version.git_commit_hash,
                    token=workflow_version.workflow.credentials_token,
                )
                if workflow_mode is None:
                    for possible_path in DocumentationEnum.PARAMETER_SCHEMA.repository_paths:
                        try:
                            # check if the location exist in the repository
                            if await repo.check_file_exists(possible_path, client=self.client):
                                # if the location exists, stop the search
                                path = possible_path
                                break
                        except AssertionError:
                            # error occurs if the file does not exist in a private github repository -> search continues
                            continue
                else:
                    path = workflow_mode.schema_path
                current_span.record_exception(e)
                self.background_tasks.add_task(
                    download_file_to_data_bucket,
                    repo=repo,
                    filepath=path,
                    key=settings.s3.workflow_cache_key(
                        name=DocumentationEnum.PARAMETER_SCHEMA,
                        git_commit_hash=workflow_execution_in.workflow_version_id,
                        mode_id=workflow_execution_in.mode_id,
                    ),
                )
                f.seek(0)
                await repo.download_file(
                    filepath=path,
                    file_handle=f,
                    client=self.client,
                )
            f.seek(0)
            nextflow_schema = json.load(f)
        try:
            jsonschema.validate(flatten_parameters(workflow_execution_in.parameters), nextflow_schema)
        except jsonschema.exceptions.ValidationError as e:  # pragma: no cover
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail=f"Nextflow Parameter validation error: {e.message}"
            ) from e

        await check_used_resources(
            workflow_execution_in.parameters,
            crud_resource_version=CRUDResourceVersion(db=self.crud_workflow_execution.db),
            background_tasks=self.background_tasks,
        )

        # Create execution in database
        execution = await self.crud_workflow_execution.create(execution=workflow_execution_in, executor_id=current_uid)
        # Start workflow execution
        self.background_tasks.add_task(
            start_workflow_execution,
            execution=execution,
            parameters=workflow_execution_in.parameters,
            git_repo=build_repository(
                url=workflow_version.workflow.repository_url, git_commit_hash=workflow_version.git_commit_hash
            ),
            scm_file_id=workflow_version.workflow_id,
            workflow_entrypoint=workflow_mode.entrypoint if workflow_mode is not None else None,
            nextflow_version=NextflowVersion(workflow_version.nextflow_version),
            default_container=workflow_version.default_container,
        )

        current_span.set_attribute("execution_id", str(execution.execution_id))
        return execution

    async def start_arbitrary_workflow(
        self, current_uid: UUID, workflow_execution_in: DevWorkflowExecutionIn
    ) -> WorkflowExecution:
        current_span = trace.get_current_span()

        await check_active_workflow_execution_limit(current_uid, crud_workflow_execution=self.crud_workflow_execution)

        try:
            # Build a git repository object based on the repository url
            repo = build_repository(
                str(workflow_execution_in.repository_url),
                workflow_execution_in.git_commit_hash,
                token=workflow_execution_in.token,
            )
        except NotImplementedError as err:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail="Supplied Git Repository is not supported"
            ) from err
        if not await repo.check_repo_availability(client=self.client):
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="repo does not exists")

        with TemporaryDirectory() as tmp_dir_name:
            tmp_dir_path = Path(tmp_dir_name)
            await repo.download_and_extract_archive(tmp_dir_name, client=self.client)
            repository_path = tmp_dir_path / next(tmp_dir_path.iterdir(), "")
            await check_repo(
                repository_path=repository_path,
                modes=[workflow_execution_in.mode] if workflow_execution_in.mode is not None else None,
            )
            nextflow_schema_path = repository_path / "nextflow_schema.json"
            if workflow_execution_in.mode is None:
                for relative_repository_path in DocumentationEnum.PARAMETER_SCHEMA.repository_paths:
                    doc_path = repository_path / relative_repository_path
                    if doc_path.is_file():
                        nextflow_schema_path = doc_path
                        break
            else:
                nextflow_schema_path = repository_path / workflow_execution_in.mode.schema_path

            async with await open_file(nextflow_schema_path, "r") as f:
                nextflow_schema = json.loads(await f.read())

        # Validate schema with saved schema in bucket
        try:
            jsonschema.validate(flatten_parameters(workflow_execution_in.parameters), nextflow_schema)
        except jsonschema.exceptions.ValidationError as e:  # pragma: no cover
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail=f"Nextflow Parameter validation error: {e.message}"
            ) from e

        await check_used_resources(
            workflow_execution_in.parameters,
            crud_resource_version=CRUDResourceVersion(db=self.crud_workflow_execution.db),
            background_tasks=self.background_tasks,
        )

        execution_note = f"Repository URL: {workflow_execution_in.repository_url}\nGit Commit Hash: {workflow_execution_in.git_commit_hash}"  # noqa: E501
        if workflow_execution_in.mode is not None:
            execution_note += f"\nEntrypoint {workflow_execution_in.mode.entrypoint}"
        execution = await self.crud_workflow_execution.create(
            execution=workflow_execution_in,
            executor_id=current_uid,
            notes=execution_note,
        )

        scm_provider = SCMProvider.from_repo(repo=repo, name=SCMProvider.generate_name(execution.execution_id))
        if repo.token is not None or not isinstance(repo, GitHubRepository):
            self.background_tasks.add_task(upload_scm_file, scm=SCM([scm_provider]), scm_file_id=execution.execution_id)

        self.background_tasks.add_task(
            start_workflow_execution,
            execution=execution,
            parameters=workflow_execution_in.parameters,
            git_repo=repo,
            scm_file_id=execution.execution_id,
            workflow_entrypoint=workflow_execution_in.mode.entrypoint
            if workflow_execution_in.mode is not None
            else None,
            nextflow_version=workflow_execution_in.nextflow_version,
            default_container=None,
        )
        current_span.set_attribute("execution_id", str(execution.execution_id))
        return execution

    async def get_params(self, execution: WorkflowExecution, *, request: Request) -> Response:
        try:
            obj = await self.s3.Object(
                bucket_name=settings.s3.data_bucket,
                key=settings.s3.execution_parameters_key(execution.execution_id),
            )
            await obj.load()
        except ClientError as err:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="parameters not found") from err

        url = await self.s3.meta.client.generate_presigned_url(
            ClientMethod="get_object",
            Params={
                "Bucket": obj.bucket_name,
                "Key": obj.key,
            },
            ExpiresIn=30,
            HttpMethod="GET",
        )
        return await stream_response_from_url(
            url=AnyHttpUrl(url), request=request, media_type="application/json", client=self.client
        )

    async def delete(self, execution: WorkflowExecution) -> None:
        if execution.status in [
            WorkflowExecution.WorkflowExecutionStatus.PENDING,
            WorkflowExecution.WorkflowExecutionStatus.SCHEDULED,
            WorkflowExecution.WorkflowExecutionStatus.RUNNING,
        ]:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail="Cannot delete workflow execution that is not finished."
            )
        self.background_tasks.add_task(
            delete_from_data_bucket, key=settings.s3.execution_parameters_key(execution.execution_id)
        )
        await self.crud_workflow_execution.delete(execution.execution_id)

    async def cancel(self, execution: WorkflowExecution) -> None:
        if execution.status not in [
            WorkflowExecution.WorkflowExecutionStatus.PENDING,
            WorkflowExecution.WorkflowExecutionStatus.SCHEDULED,
            WorkflowExecution.WorkflowExecutionStatus.RUNNING,
        ]:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail="Cannot cancel workflow execution that is finished."
            )
        if execution.slurm_job_id >= 0:
            self.background_tasks.add_task(cancel_slurm_job, job_id=execution.slurm_job_id)
        await self.crud_workflow_execution.update_status(
            execution.execution_id,
            status=WorkflowExecution.WorkflowExecutionStatus.CANCELED,
            end_timestamp=round(time.time()),
        )


def workflow_execution_core_dependency(
    crud_workflow_execution: CrudWorkflowExecutionSession,
    client: HTTPClient,
    background_tasks: BackgroundTasks,
    s3: S3Service,
) -> WorkflowExecutionCore:
    """
    Get the core functionality for workflow executions with the DB connection injected.

    Parameters
    ----------
    crud_workflow_execution: clowm.crud.crud_workflow_execution.CRUDWorkflowExecution
        Active CRUD repository for workflow_executions. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Dependency Injection.
    s3 : types_aiobotocore_s3.service_workflow_execution.S3ServiceWorkflowExecution
        S3 Service to perform operations on buckets.
    client : httpx.AsyncClient
        Http client with an open connection. Dependency Injection.

    Returns
    -------
    workflow_execution_core : WorkflowExecutionCore
        Core API functionality for workflow executions
    """
    return WorkflowExecutionCore(
        crud_workflow_execution=crud_workflow_execution, client=client, background_tasks=background_tasks, s3=s3
    )


WorkflowExecutionCoreDep = Annotated[WorkflowExecutionCore, Depends(workflow_execution_core_dependency)]
