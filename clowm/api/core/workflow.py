import tempfile
from pathlib import Path
from typing import TYPE_CHECKING, Annotated
from uuid import UUID

from fastapi import BackgroundTasks, Depends, HTTPException, status
from httpx import AsyncClient
from opentelemetry import trace

from clowm.crud import CRUDWorkflow, CRUDWorkflowMode, CRUDWorkflowVersion
from clowm.models import Workflow, WorkflowMode, WorkflowVersion
from clowm.schemas.workflow import WorkflowUpdate
from clowm.schemas.workflow_version import WorkflowVersionOut

from ...core.config import settings
from ...git_repository import build_repository
from ...smtp.send_email import send_review_request_email
from ..background.s3 import copy_in_data_bucket, delete_from_data_bucket, delete_from_data_bucket_with_prefix
from ..dependencies import CRUDSessionDep, HTTPClient, S3Service
from ..utils import check_repo, upload_documentation_files

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object

__all__ = ["WorkflowCoreDep", "WorkflowCore"]

CrudWorkflowSession = Annotated[CRUDWorkflow, Depends(CRUDSessionDep(CRUDWorkflow))]
tracer = trace.get_tracer_provider().get_tracer(__name__)


class WorkflowCore:
    """
    Class bundling the core API functionality for workflows. Authorization is not included and has to be done before.
    """

    def __init__(
        self, crud_workflow: CRUDWorkflow, s3: S3ServiceResource, background_tasks: BackgroundTasks, client: AsyncClient
    ) -> None:
        self.crud_workflow = crud_workflow
        self.crud_workflow_version = CRUDWorkflowVersion(db=crud_workflow.db)
        self.s3 = s3
        self.background_tasks = background_tasks
        self.client = client

    async def delete(self, workflow: Workflow) -> None:
        """
        Delete an entire workflow.

        Parameters
        ----------
        workflow : clowm.models.Workflow
            Workflow to be deleted.
        """
        versions = await self.crud_workflow_version.list_workflow_versions(workflow.workflow_id)
        # Delete SCM file for private repositories
        self.background_tasks.add_task(delete_from_data_bucket, key=settings.s3.scm_key(workflow.workflow_id))
        # Delete files in buckets
        mode_ids: set[UUID] = set()
        for version in versions:
            # Delete parameter schema of every mode
            if version.workflow_modes is not None and len(version.workflow_modes) > 0:
                for mode in version.workflow_modes:
                    mode_ids.add(mode.mode_id)
            # delete files in data bucket cache
            self.background_tasks.add_task(
                delete_from_data_bucket_with_prefix,
                prefix=settings.s3.workflow_cache_key(name="", git_commit_hash=version.git_commit_hash, mode_id=None),
            )
            if version.icon_slug is not None:
                self.background_tasks.add_task(delete_from_data_bucket, key=settings.s3.icon_key(version.icon_slug))
        await self.crud_workflow.delete(workflow.workflow_id)
        if len(mode_ids) > 0:
            await CRUDWorkflowMode(db=self.crud_workflow.db).delete(mode_ids)

    async def update(self, version_update: WorkflowUpdate, workflow: Workflow) -> WorkflowVersionOut:
        """
        Update a workflow with a new version.

        Parameters
        ----------
        version_update : clowm.schemas.workflow.WorkflowUpdate
            Data about the new workflow version.
        workflow : clowm.models.Workflow
            Workflow to be updated.

        Returns
        -------
        version : clowm.models.WorkflowVersion
            The new workflow version
        """
        # Check if git commit is already used
        if await self.crud_workflow_version.get(version_update.git_commit_hash) is not None:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"Workflow Version with git_commit_hash'{version_update.git_commit_hash}' already exists",
            )
        # Get previous version
        previous_version = await self.crud_workflow_version.get_latest(  # type: ignore[assignment]
            workflow.workflow_id, published=False
        )
        assert previous_version is not None

        # Get modes of previous version
        previous_version_modes = await CRUDWorkflowMode(db=self.crud_workflow_version.db).list_modes(
            previous_version.git_commit_hash
        )

        if len(version_update.delete_modes) > 0:
            # Check if mode to delete actually exist
            mode_ids = [mode.mode_id for mode in previous_version_modes]
            for delete_mode in version_update.delete_modes:
                if delete_mode not in mode_ids:
                    raise HTTPException(
                        status_code=status.HTTP_400_BAD_REQUEST,
                        detail=f"Workflow mode {delete_mode} does not exist for the latest version {previous_version.git_commit_hash} of workflow {workflow.workflow_id}",  # noqa: E501
                    )
            # Filter out modes that should be deleted in new workflow version
            previous_version_modes = [
                mode for mode in previous_version_modes if mode.mode_id not in version_update.delete_modes
            ]

        # Build a git repository object based on the repository url
        repo = build_repository(
            workflow.repository_url,
            version_update.git_commit_hash,
            token=workflow.credentials_token,
        )

        if not await repo.check_repo_availability(client=self.client):
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="repo does not exists")

        check_repo_modes = previous_version_modes.copy()
        # If there are new modes, add them to the list for file checking
        if len(version_update.append_modes) > 0:
            check_repo_modes += version_update.append_modes  # type: ignore[arg-type]
        # Check if the relevant files are present in the repository
        with tempfile.TemporaryDirectory() as tmp_dir_name:
            tmp_dir_path = Path(tmp_dir_name)
            await repo.download_and_extract_archive(tmp_dir_name, client=self.client)
            repository_path = tmp_dir_path / next(tmp_dir_path.iterdir(), "")
            await check_repo(repository_path=repository_path, modes=check_repo_modes)

            append_modes_db: list[WorkflowMode] = []
            # Create new modes in database
            if len(version_update.append_modes) > 0:
                append_modes_db = await CRUDWorkflowMode(db=self.crud_workflow_version.db).create(
                    modes=version_update.append_modes
                )
            # Make a list with all DB modes of modes for the new workflow version
            db_modes = previous_version_modes + append_modes_db
            await upload_documentation_files(
                repository_path=repository_path,
                git_commit_hash=version_update.git_commit_hash,
                modes=db_modes,
                background_tasks=self.background_tasks,
            )
        if previous_version.nextflow_config is not None:
            self.background_tasks.add_task(
                copy_in_data_bucket,
                src_key=settings.s3.config_key(previous_version.git_commit_hash),
                dst_key=settings.s3.config_key(version_update.git_commit_hash),
            )

        # Create list with mode ids that are connected to the new workflow version
        mode_ids = [mode.mode_id for mode in db_modes]
        version = await self.crud_workflow_version.create(
            git_commit_hash=version_update.git_commit_hash,
            version=version_update.version,
            workflow_id=workflow.workflow_id,
            icon_slug=previous_version.icon_slug if previous_version else None,
            previous_version=previous_version.git_commit_hash if previous_version else None,
            modes=mode_ids,
            parameter_extension=previous_version.parameter_extension if previous_version else None,
            nextflow_version=previous_version.nextflow_version,
            default_container=previous_version.default_container,
            nextflow_config=previous_version.nextflow_config,
        )
        self.background_tasks.add_task(send_review_request_email, workflow=workflow, version=version)
        return WorkflowVersionOut.from_db_version(version, mode_ids=mode_ids)

    async def deprecate_version(self, version: WorkflowVersion) -> None:
        """
        Deprecate a workflow version.

        Parameters
        ----------
        version : clowm.models.WorkflowVersion
            Version to be deprecated.
        """
        await self.crud_workflow_version.update_status(
            version.git_commit_hash, WorkflowVersion.WorkflowVersionStatus.DEPRECATED
        )
        version.status = WorkflowVersion.WorkflowVersionStatus.DEPRECATED


def workflow_core_dependency(
    crud_workflow: Annotated[CRUDWorkflow, Depends(CRUDSessionDep(CRUDWorkflow))],
    s3: S3Service,
    background_tasks: BackgroundTasks,
    client: HTTPClient,
) -> WorkflowCore:
    """
    Get the core functionality for buckets with the DB connection injected.

    Parameters
    ----------
    crud_workflow: clowm.crud.CRUDWorkflow
        Active CRUD repository for workflows. Dependency Injection.
    s3 : types_aiobotocore_s3.service_resource.S3ServiceResource
        S3 Service to perform operations on buckets.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Dependency Injection.
    client : httpx.AsyncClient
        Http client with an open connection. Dependency Injection.

    Returns
    -------
    workflow_core : WorkflowCore
        Core API functionality for workflows
    """
    return WorkflowCore(crud_workflow=crud_workflow, s3=s3, background_tasks=background_tasks, client=client)


WorkflowCoreDep = Annotated[WorkflowCore, Depends(workflow_core_dependency)]
