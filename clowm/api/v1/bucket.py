from collections.abc import Iterable
from typing import Annotated
from uuid import UUID

from fastapi import APIRouter, Body, Depends, Query, status
from opentelemetry import trace

from clowm.api.dependencies.v1 import AuthorizationDependencyTokenUser as AuthorizationDependency
from clowm.api.dependencies.v1 import CurrentUserApiToken as CurrentUser
from clowm.core.rbac import RBACOperation, RBACResource
from clowm.crud import CRUDBucket, CRUDBucketPermission
from clowm.models import Bucket
from clowm.otlp import start_as_current_span_async
from clowm.schemas.bucket import BucketIn as BucketInSchema
from clowm.schemas.bucket import BucketOut as BucketOutSchema

from ..core import BucketCoreDep
from ..dependencies import CRUDSessionDep, CurrentBucket
from ..dependencies.auth import AuthorizationFunction

router = APIRouter(prefix="/buckets", tags=["Bucket"])
bucket_authorization = AuthorizationDependency(resource=RBACResource.BUCKET)
Authorization = Annotated[AuthorizationFunction, Depends(bucket_authorization)]
CrudBucketSession = Annotated[CRUDBucket, Depends(CRUDSessionDep(CRUDBucket))]
tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.get("", response_model=list[BucketOutSchema], summary="List buckets")
@start_as_current_span_async("api_list_buckets", tracer=tracer)
async def list_buckets(
    current_user: CurrentUser,
    authorization: Authorization,
    bucket_core: BucketCoreDep,
    owner_id: Annotated[
        UUID | None,
        Query(
            description="UID of the user for whom to fetch the buckets for. Permission `bucket:read_any` required if current user is not the target.",  # noqa:E501
            examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
        ),
    ] = None,
    bucket_type: Annotated[
        CRUDBucket.BucketType, Query(description="Type of the bucket to get. Ignored when `user` parameter not set")
    ] = CRUDBucket.BucketType.ALL,
) -> Iterable[Bucket]:
    """
    List all the buckets in the system or of the desired user where the user has permissions for.

    Permission `bucket:list_all` required. See parameter `owner_id` for exception.
    \f
    Parameters
    ----------
    owner_id : uuid.UUID
        User for which to retrieve the buckets. Query Parameter.
    bucket_type : clowm.crud.crud_bucket.CRUDBucket.BucketType, default BucketType.ALL
        Type of the bucket to get. Query Parameter.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    bucket_core : clowm.api.core.BucketCore
        Core functionality for bucket API. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.

    Returns
    -------
    buckets : list[clowm.models.Bucket]
        All the buckets based on the filters.
    """
    current_span = trace.get_current_span()
    if owner_id is not None:  # pragma: no cover
        current_span.set_attribute("owner_id", str(owner_id))
    current_span.set_attribute("bucket_type", bucket_type.name)
    authorization(RBACOperation.LIST if current_user.uid == owner_id else RBACOperation.LIST_ALL)
    return await bucket_core.list_buckets(owner_id, bucket_type)


@router.post(
    "",
    response_model=BucketOutSchema,
    status_code=status.HTTP_201_CREATED,
    summary="Create a bucket for the current user",
)
@start_as_current_span_async("api_create_bucket", tracer=tracer)
async def create_bucket(
    bucket: Annotated[BucketInSchema, Body(description="Meta-data for bucket to create")],
    current_user: CurrentUser,
    authorization: Authorization,
    bucket_core: BucketCoreDep,
) -> Bucket:
    """
    Create a bucket for the current user.\n
    The name of the bucket has some constraints.
    For more information see the
    [Ceph documentation](https://docs.ceph.com/en/quincy/radosgw/s3/bucketops/#constraints)

    Permission `bucket:create` required.
    \f
    Parameters
    ----------
    bucket : clowm.schemas.bucket.BucketIn
        Information about the bucket to create. HTTP Body
    current_user : clowm.models.User
        Current user. Dependency Injection.
    bucket_core : clowm.api.core.BucketCore
        Core functionality for bucket API. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.

    Returns
    -------
    bucket : clowm.models.Bucket
        The newly created bucket.
    """
    current_span = trace.get_current_span()
    current_span.set_attribute("bucket_name", bucket.name)
    authorization(RBACOperation.CREATE)
    return await bucket_core.create_bucket(bucket=bucket, owner_id=current_user.uid)


@router.get("/{bucket_name}", response_model=BucketOutSchema, summary="Get a bucket by its name")
@start_as_current_span_async("api_get_bucket", tracer=tracer)
async def get_bucket(
    bucket: CurrentBucket,
    current_user: CurrentUser,
    authorization: Authorization,
    crud_bucket_permission: Annotated[CRUDBucketPermission, Depends(CRUDSessionDep(CRUDBucketPermission))],
) -> Bucket:
    """
    Get a bucket by its name if the current user has READ permissions for the bucket.

    Permission `bucket:read` required if the current user is the owner of the bucket,
    otherwise `bucket:read_any` required.
    \f
    Parameters
    ----------
    bucket : clowm.models.Bucket
        Bucket with the name provided in the URL path. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    crud_bucket_permission : clowm.crud.crud_bucket_permission.CRUDBucketPermission
        Active CRUD repository for bucket permissions. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.

    Returns
    -------
    bucket : clowm.models.Bucket
        Bucket with the provided name.
    """
    trace.get_current_span().set_attribute("bucket_name", bucket.name)
    rbac_operation = (
        RBACOperation.READ_ANY
        if not bucket.public and not await crud_bucket_permission.check_active_permission(bucket.name, current_user.uid)
        else RBACOperation.READ
    )
    authorization(rbac_operation)
    return bucket


@router.patch("/{bucket_name}/public", response_model=BucketOutSchema, summary="Update public status")
@start_as_current_span_async("api_update_bucket_public_state", tracer=tracer)
async def update_bucket_public_state(
    bucket: CurrentBucket,
    current_user: CurrentUser,
    authorization: Authorization,
    bucket_core: BucketCoreDep,
    public: Annotated[bool, Body(..., embed=True, description="New public state")],
) -> Bucket:
    """
    Update the buckets public state.

    Permission `bucket:update` required if the current user is the owner of the bucket,
    otherwise `bucket:update_any` required.
    \f
    Parameters
    ----------
    bucket : clowm.models.Bucket
        Bucket with the name provided in the URL path. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    bucket_core : clowm.api.core.BucketCore
        Core functionality for bucket API. Dependency Injection.
    public : bool
        The new public state. HTTP Body.

    Returns
    -------
    bucket : clowm.models.Bucket
        Bucket with the updated public state.
    """
    trace.get_current_span().set_attributes({"bucket_name": bucket.name, "public": public})
    authorization(RBACOperation.UPDATE if bucket.owner_id == current_user.uid else RBACOperation.UPDATE_ANY)
    return await bucket_core.update_public_state(bucket=bucket, public=public)


@router.delete("/{bucket_name}", status_code=status.HTTP_204_NO_CONTENT, summary="Delete a bucket")
@start_as_current_span_async("api_delete_bucket", tracer=tracer)
async def delete_bucket(
    bucket: CurrentBucket,
    current_user: CurrentUser,
    authorization: Authorization,
    bucket_core: BucketCoreDep,
    force_delete: Annotated[bool, Query(description="Delete even non-empty bucket")] = False,
) -> None:
    """
    Delete a bucket by its name. Only the owner of the bucket can delete the bucket.

    Permission `bucket:delete` required if the current user is the owner of the bucket,
    otherwise `bucket:delete_any` required.
    \f
    Parameters
    ----------
    force_delete : bool, default False
        Flag for deleting a non-empty bucket. Query parameter.
    bucket : clowm.models.Bucket
        Bucket with the name provided in the URL path. Dependency Injection.
    bucket_core : clowm.api.core.BucketCore
        Core functionality for bucket API. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    """
    trace.get_current_span().set_attributes({"bucket_name": bucket.name, "force_delete": force_delete})
    authorization(RBACOperation.DELETE if bucket.owner_id == current_user.uid else RBACOperation.DELETE_ANY)
    await bucket_core.delete_bucket(bucket_name=bucket.name, force_delete=force_delete)
