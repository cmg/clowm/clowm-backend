from typing import Annotated

from fastapi import APIRouter, Body, Depends, Query, status
from opentelemetry import trace

from clowm.api.dependencies.v1 import AuthorizationDependencyTokenUser as AuthorizationDependency
from clowm.api.dependencies.v1 import CurrentUserApiToken as CurrentUser
from clowm.core.rbac import RBACOperation, RBACResource
from clowm.crud import CRUDBucketPermission
from clowm.db.types import BucketPermissionScopes, BucketPermissionScopesDB
from clowm.otlp import start_as_current_span_async
from clowm.schemas.bucket_permission import BucketPermissionIn, BucketPermissionOut, BucketPermissionParameters

from ..core import BucketPermissionCoreDep
from ..dependencies import CRUDSessionDep, CurrentBucket, PathUser
from ..dependencies.auth import AuthorizationFunction
from ..dependencies.bucket import CurrentBucketPermission

router = APIRouter(prefix="/permissions", tags=["BucketPermission"])
bucket_permission_authorization = AuthorizationDependency(resource=RBACResource.BUCKET_PERMISSION)
Authorization = Annotated[AuthorizationFunction, Depends(bucket_permission_authorization)]
CrudBucketPermissionSession = Annotated[CRUDBucketPermission, Depends(CRUDSessionDep(CRUDBucketPermission))]
tracer = trace.get_tracer_provider().get_tracer(__name__)


QueryPermissionScopes = Annotated[
    list[BucketPermissionScopes] | None,
    Query(description="Scopes of Bucket Permissions to fetch"),
]
QueryPermissionStatus = Annotated[
    CRUDBucketPermission.PermissionStatus | None,
    Query(description="Status of Bucket Permissions to fetch"),
]


@router.get(
    "",
    summary="Get all permissions.",
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_list_bucket_permission", tracer=tracer)
async def list_permissions(
    crud_bucket_permission: CrudBucketPermissionSession,
    authorization: Authorization,
    permission_scopes: QueryPermissionScopes = None,
    permission_status: QueryPermissionStatus = None,
) -> list[BucketPermissionOut]:
    """
    List all the bucket permissions in the system.

    Permission `bucket_permission:list_all` required.
    \f
    Parameters
    ----------
    permission_scopes : list[clowm.db.types.BucketPermissionScopes] | None, default None
        Scopes of Bucket Permissions to fetch. Query Parameter.
    permission_status : clowm.crud.crud_bucket_permission.CRUDBucketPermission.PermissionStatus | None, default None
        Status of Bucket Permissions to fetch. Query Parameter.
    crud_bucket_permission : clowm.crud.crud_bucket_permission.CRUDBucketPermission
        Active CRUD repository for buckets permissions. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.

    Returns
    -------
    permissions : list[clowm.schemas.bucket_permission.BucketPermissionOut]
        List of all permissions.
    """
    current_span = trace.get_current_span()
    if permission_scopes is not None and len(permission_scopes) > 0:  # pragma: no cover
        current_span.set_attribute("permission_scopes", permission_scopes)
    if permission_status is not None:  # pragma: no cover
        current_span.set_attribute("permission_status", permission_status.name)
    authorization(RBACOperation.LIST_ALL)
    bucket_permissions = await crud_bucket_permission.list(
        permission_scopes=None if permission_scopes is None else BucketPermissionScopesDB.parse(permission_scopes),
        permission_status=permission_status,
    )
    return [BucketPermissionOut.from_db_model(p) for p in bucket_permissions]


@router.post(
    "",
    status_code=status.HTTP_201_CREATED,
    summary="Create a permission.",
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_create_bucket_permission", tracer=tracer)
async def create_permission(
    current_user: CurrentUser,
    permission_core: BucketPermissionCoreDep,
    authorization: Authorization,
    permission: Annotated[BucketPermissionIn, Body(..., description="Permission to create")],
) -> BucketPermissionOut:
    """
    Create a permission for a bucket and user.

    Permission `bucket_permission:create` required.
    \f
    Parameters
    ----------
    permission : clowm.schemas.bucket_permission.BucketPermissionOut
        Information about the permission which should be created. HTTP Body parameter.
    permission_core : clowm.api.core.BucketPermissionCore
        Core functionality for bucket permission API. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.

    Returns
    -------
    permissions : clowm.schemas.bucket_permission.BucketPermissionOut
        Newly created permission.
    """
    current_span = trace.get_current_span()
    current_span.set_attributes({"uid": str(permission.uid), "bucket_name": permission.bucket_name})
    authorization(RBACOperation.CREATE)
    permission_db = await permission_core.create_permission(permission=permission, current_uid=current_user.uid)
    return BucketPermissionOut.from_db_model(permission_db)


@router.get(
    "/user/{uid}",
    summary="Get all permissions for a user.",
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_list_bucket_permission_for_user", tracer=tracer)
async def list_permissions_per_user(
    crud_bucket_permission: CrudBucketPermissionSession,
    current_user: CurrentUser,
    authorization: Authorization,
    user: PathUser,
    permission_scopes: QueryPermissionScopes = None,
    permission_status: QueryPermissionStatus = None,
) -> list[BucketPermissionOut]:
    """
    List all the bucket permissions for the given user.

    Permission `bucket_permission:list` required if current user is the target the bucket permission,
    otherwise `bucket_permission:list_all` required.
    \f
    Parameters
    ----------
    permission_scopes : list[clowm.db.types.BucketPermissionScopes] | None, default None
        Scopes of Bucket Permissions to fetch. Query Parameter.
    permission_status : clowm.crud.crud_bucket_permission.CRUDBucketPermission.PermissionStatus | None, default None
        Status of Bucket Permissions to fetch. Query Parameter.
    user : clowm.models.User
        User with given uid. Dependency Injection.
    crud_bucket_permission : clowm.crud.crud_bucket_permission.CRUDBucketPermission
        Active CRUD repository for buckets permissions. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.

    Returns
    -------
    permissions : list[clowm.schemas.bucket_permission.BucketPermissionOut]
        List of all permissions for this user.
    """
    current_span = trace.get_current_span()
    current_span.set_attribute("uid", str(user.uid))
    if permission_scopes is not None and len(permission_scopes) > 0:  # pragma: no cover
        current_span.set_attribute("permission_scopes", permission_scopes)
    if permission_status is not None:  # pragma: no cover
        current_span.set_attribute("permission_status", permission_status.name)
    authorization(RBACOperation.LIST if user == current_user else RBACOperation.LIST_ALL)
    bucket_permissions = await crud_bucket_permission.list(
        uid=user.uid,
        permission_scopes=None if permission_scopes is None else BucketPermissionScopesDB.parse(permission_scopes),
        permission_status=permission_status,
    )
    return [BucketPermissionOut.from_db_model(p) for p in bucket_permissions]


@router.get(
    "/bucket/{bucket_name}",
    summary="Get all permissions for a bucket.",
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_list_bucket_permission_for_bucket", tracer=tracer)
async def list_permissions_per_bucket(
    bucket: CurrentBucket,
    crud_bucket_permission: CrudBucketPermissionSession,
    current_user: CurrentUser,
    authorization: Authorization,
    permission_scopes: QueryPermissionScopes = None,
    permission_status: QueryPermissionStatus = None,
) -> list[BucketPermissionOut]:
    """
    List all the bucket permissions for the given bucket.

    Permission `bucket_permission:list` required if current user is owner of the bucket,
    otherwise `bucket_permission:list_all` required.
    \f
    Parameters
    ----------
    permission_scopes : list[clowm.db.types.BucketPermissionScopes] | None, default None
        Scopes of Bucket Permissions to fetch. Query Parameter.
    permission_status : clowm.crud.crud_bucket_permission.CRUDBucketPermission.PermissionStatus | None, default None
        Status of Bucket Permissions to fetch. Query Parameter.
    bucket : clowm.models.Bucket
        Bucket with the name provided in the URL path. Dependency Injection.
    crud_bucket_permission : clowm.crud.crud_bucket_permission.CRUDBucketPermission
        Active CRUD repository for buckets permissions. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.

    Returns
    -------
    permissions : list[clowm.schemas.bucket_permission.BucketPermissionOut]
        List of all permissions for this bucket.
    """
    current_span = trace.get_current_span()
    current_span.set_attribute("bucket_name", bucket.name)
    if permission_scopes is not None and len(permission_scopes) > 0:  # pragma: no cover
        current_span.set_attribute("permission_scopes", permission_scopes)
    if permission_status is not None:  # pragma: no cover
        current_span.set_attribute("permission_status", permission_status.name)
    authorization(RBACOperation.LIST if bucket.owner_id == current_user.uid else RBACOperation.LIST_ALL)
    bucket_permissions = await crud_bucket_permission.list(
        bucket_name=bucket.name,
        permission_scopes=None if permission_scopes is None else BucketPermissionScopesDB.parse(permission_scopes),
        permission_status=permission_status,
    )
    return [BucketPermissionOut.from_db_model(p) for p in bucket_permissions]


@router.get(
    "/bucket/{bucket_name}/user/{uid}",
    summary="Get permission for bucket and user combination.",
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_get_bucket_permission", tracer=tracer)
async def get_permission(
    bucket: CurrentBucket,
    current_user: CurrentUser,
    authorization: Authorization,
    bucket_permission: CurrentBucketPermission,
) -> BucketPermissionOut:
    """
    Get the bucket permissions for the specific combination of bucket and user.

    Permission `bucket_permission:read` required if current user is the target or owner of the bucket permission,
    otherwise `bucket_permission:read_any` required.
    \f
    Parameters
    ----------
    bucket : clowm.models.Bucket
        Bucket with the name provided in the URL path. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    bucket_permission : clowm.models.BucketPermission
        Permission based on the path. Dependency Injection.

    Returns
    -------
    permissions : clowm.schemas.bucket_permission.BucketPermissionOut
        Permission for this bucket and user combination.
    """
    trace.get_current_span().set_attributes({"bucket_name": bucket.name, "uid": str(bucket_permission.uid)})
    authorization(
        RBACOperation.READ
        if bucket_permission.uid == current_user.uid or current_user.uid == bucket.owner_id
        else RBACOperation.READ_ANY
    )
    return BucketPermissionOut.from_db_model(bucket_permission)


@router.put(
    "/bucket/{bucket_name}/user/{uid}",
    status_code=status.HTTP_200_OK,
    summary="Update a bucket permission",
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_create_bucket_permission", tracer=tracer)
async def update_permission(
    bucket: CurrentBucket,
    bucket_permission: CurrentBucketPermission,
    current_user: CurrentUser,
    permission_core: BucketPermissionCoreDep,
    authorization: Authorization,
    update_parameters: Annotated[BucketPermissionParameters, Body(..., description="Permission to create")],
) -> BucketPermissionOut:
    """
    Update a permission for a bucket and user.

    Permission `bucket_permission:update` required.
    \f
    Parameters
    ----------
    update_parameters : clowm.schemas.bucket_permission.BucketPermissionOut
        Information about the permission which should be updated. HTTP Body parameter.
    bucket_permission : clowm.models.BucketPermission
        Permission based on the path. Dependency Injection.
    bucket : clowm.models.Bucket
        Bucket with the name provided in the URL path. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    permission_core : clowm.api.core.BucketPermissionCore
        Core functionality for bucket permission API. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.

    Returns
    -------
    permissions : clowm.schemas.bucket_permission.BucketPermissionOut
        Updated permission.
    """
    trace.get_current_span().set_attributes(
        {"uid": str(bucket_permission.uid), "bucket_name": bucket_permission.bucket_name}
    )
    authorization(RBACOperation.UPDATE if bucket.owner_id == current_user.uid else RBACOperation.UPDATE_ANY)
    updated_permission = await permission_core.update_permission(
        permission=bucket_permission, update_parameters=update_parameters
    )
    return BucketPermissionOut.from_db_model(updated_permission)


@router.delete(
    "/bucket/{bucket_name}/user/{uid}",
    status_code=status.HTTP_204_NO_CONTENT,
    summary="Delete a bucket permission",
)
@start_as_current_span_async("api_delete_bucket_permission", tracer=tracer)
async def delete_permission(
    bucket: CurrentBucket,
    current_user: CurrentUser,
    permission_core: BucketPermissionCoreDep,
    authorization: Authorization,
    bucket_permission: CurrentBucketPermission,
) -> None:
    """
    Delete the bucket permissions for the specific combination of bucket and user.

    Permission `bucket_permission:delete` required if current user is the target or owner of the bucket permission,
    otherwise `bucket_permission:delete_any` required.
    \f
    Parameters
    ----------
    bucket : clowm.models.Bucket
        Bucket with the name provided in the URL path. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    bucket_permission : clowm.models.BucketPermission
        Permission based on the path. Dependency Injection.
    permission_core : clowm.api.core.BucketPermissionCore
        Core functionality for bucket permission API. Dependency Injection.

    Returns
    -------
    permissions : clowm.schemas.bucket_permission.BucketPermissionOut
        Permission for this bucket and user combination.
    """
    trace.get_current_span().set_attributes({"bucket_name": bucket.name, "uid": str(bucket_permission.uid)})
    authorization(
        RBACOperation.DELETE
        if bucket_permission.uid == current_user.uid or current_user.uid == bucket.owner_id
        else RBACOperation.DELETE_ANY
    )
    await permission_core.delete_permission(permission=bucket_permission)
