from typing import Annotated

from fastapi import APIRouter, Depends, Query
from opentelemetry import trace
from pydantic import TypeAdapter

from clowm.api.dependencies.v1 import AuthorizationDependencyTokenUser as AuthorizationDependency
from clowm.api.dependencies.v1 import CurrentUserApiToken as CurrentUser
from clowm.core.rbac import RBACOperation, RBACResource
from clowm.crud import CRUDUser
from clowm.models import Role, User
from clowm.otlp import start_as_current_span_async
from clowm.schemas.user import UserOut, UserOutExtended

from ..dependencies import CRUDSessionDep, PathUser
from ..dependencies.auth import AuthorizationFunction

router = APIRouter(prefix="/users", tags=["User"])

user_authorization = AuthorizationDependency(resource=RBACResource.USER)
Authorization = Annotated[AuthorizationFunction, Depends(user_authorization)]
CrudUserSession = Annotated[CRUDUser, Depends(CRUDSessionDep(CRUDUser))]

tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.get("/search")
@start_as_current_span_async("api_search_users", tracer=tracer)
async def search_users(
    crud_user: CrudUserSession,
    authorization: Authorization,
    name_substring: Annotated[
        str,
        Query(
            min_length=3,
            max_length=30,
            description="Filter users by a substring in their name.",
        ),
    ],
) -> list[UserOut]:
    """
    Search for users in the system by their name.

    Permission `user: search` required.
    \f
    Parameters
    ----------
    crud_user : clowm.crud.crud_user.CRUDUser
        Active CRUD repository for users. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    name_substring : string
        Filter users by a substring in their name. Query Parameter.

    Returns
    -------
    user : list[clowm.models.User]
        List with the given substring in its name.
    """
    trace.get_current_span().set_attribute("name_substring", name_substring)
    authorization(RBACOperation.SEARCH)
    users = await crud_user.list_users(name_substring=name_substring)
    return TypeAdapter(list[UserOut]).validate_python(users)


@router.get("/me", summary="Get the logged in user")
@start_as_current_span_async("api_get_logged_in_user", tracer=tracer)
async def get_logged_in_user(current_user: CurrentUser, authorization: Authorization) -> UserOutExtended:
    """
    Return the user associated with the used JWT.

    Permission `user:read` required.
    \f
    Parameters
    ----------
    current_user : clowm.models.User
        User from the database associated to the used JWT. Dependency injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.

    Returns
    -------
    current_user : clowm.schemas.user.UserOutExtended
        User associated to used JWT.
    """
    trace.get_current_span().set_attribute("uid", str(current_user.uid))
    authorization(RBACOperation.READ)
    return UserOutExtended.from_db_user(current_user)


@router.get("", summary="List users and search by their name")
@start_as_current_span_async("api_list_users", tracer=tracer)
async def list_users(
    crud_user: CrudUserSession,
    authorization: Authorization,
    name_substring: Annotated[
        str | None,
        Query(
            min_length=3, max_length=30, description="Filter users by a substring in their name.", examples=["Bilbo"]
        ),
    ] = None,
    filter_roles: Annotated[
        list[Role.RoleEnum] | None,
        Query(
            description="Filter users by their role. If multiple are selected, they are concatenating by an OR Expression."  # noqa:E501
        ),
    ] = None,
) -> list[UserOutExtended]:
    """
    List all users in the system..

    Permission `user:list` required.
    \f
    Parameters
    ----------
    name_substring : string | None, default None
        Filter users by a substring in their name. Query Parameter.
    filter_roles : list[clowm.core.authorization.RoleEnum] | None, default None
        Filter users by their role. Query Parameter.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    crud_user : clowm.crud.crud_user.CRUDUser
        Active CRUD repository for users. Dependency Injection.

    Returns
    -------
    users: list[clowm.schemas.user.UserOutExtended]
        List of all the users with the given filter.
    """
    current_span = trace.get_current_span()
    if name_substring is not None:  # pragma: no cover
        current_span.set_attribute("name_substring", name_substring)
    if filter_roles is not None and len(filter_roles) > 0:  # pragma: no cover
        current_span.set_attribute("filter_roles", [role.name for role in filter_roles])

    authorization(RBACOperation.LIST)

    users = await crud_user.list_users(name_substring=name_substring, roles=filter_roles)
    return [UserOutExtended.from_db_user(user) for user in users]


@router.get("/{uid}", response_model=UserOut, summary="Get a user by its uid")
@start_as_current_span_async("api_get_user", tracer=tracer)
async def get_user(authorization: Authorization, user: PathUser) -> User:
    """
    Return the user with the specific uid.

    Permission `user:read` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    user : clowm.models.User
        The user associated with the UID in the path. Dependency Injection.

    Returns
    -------
    user : clowm.models.User
        User with given uid.
    """
    trace.get_current_span().set_attributes({"uid": str(user.uid)})
    authorization(RBACOperation.READ)
    return user
