from typing import Annotated
from uuid import UUID

from fastapi import APIRouter, Body, Depends, Query, status
from opentelemetry import trace

from clowm.api.dependencies.v1 import AuthorizationDependencyTokenUser as AuthorizationDependency
from clowm.api.dependencies.v1 import CurrentUserApiToken as CurrentUser
from clowm.core.rbac import RBACOperation, RBACResource
from clowm.crud import CRUDWorkflow
from clowm.models import WorkflowVersion
from clowm.otlp import start_as_current_span_async
from clowm.schemas.workflow import WorkflowOut, WorkflowUpdate
from clowm.schemas.workflow_version import WorkflowVersionOut

from ..core import WorkflowCoreDep
from ..dependencies import CRUDSessionDep, CurrentWorkflow
from ..dependencies.auth import AuthorizationFunction

router = APIRouter(prefix="/workflows", tags=["Workflow"])
workflow_authorization = AuthorizationDependency(resource=RBACResource.WORKFLOW)
Authorization = Annotated[AuthorizationFunction, Depends(workflow_authorization)]
CrudWorkflowSession = Annotated[CRUDWorkflow, Depends(CRUDSessionDep(CRUDWorkflow))]

tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.get("", status_code=status.HTTP_200_OK, summary="List workflows", response_model_exclude_none=True)
@start_as_current_span_async("api_list_workflow", tracer=tracer)
async def list_workflows(
    crud_workflow: CrudWorkflowSession,
    authorization: Authorization,
    current_user: CurrentUser,
    name_substring: Annotated[
        str | None,
        Query(
            min_length=3,
            max_length=30,
            description="Filter workflows by a substring in their name.",
            examples=["blast"],
        ),
    ] = None,
    version_status: Annotated[
        list[WorkflowVersion.WorkflowVersionStatus] | None,
        Query(
            description=f"Which versions of the workflow to include in the response. Permission `workflow:list_filter` required, unless `developer_id` is provided and current user is developer, then only permission `workflow:list` required. Default `{WorkflowVersion.WorkflowVersionStatus.PUBLISHED.name}` and `{WorkflowVersion.WorkflowVersionStatus.DEPRECATED.name}`.",  # noqa: E501
        ),
    ] = None,
    developer_id: Annotated[
        UUID | None,
        Query(
            description="Filter for workflow by developer. If current user is the developer, permission `workflow:list` required, otherwise `workflow:list_filter`.",  # noqa: E501
            examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
        ),
    ] = None,
) -> list[WorkflowOut]:
    """
    List all workflows.

    Permission `workflow:list` required.
    \f
    Parameters
    ----------
    crud_workflow : clowm.crud.crud_workflow.CRUDWorkflow
        Active CRUD repository for workflows. Dependency Injection.
    developer_id : str | None, default None
        Filter workflows by a developer. Query Parameter.
    name_substring : string | None, default None
        Filter workflows by a substring in their name. Query Parameter.
    version_status : list[clowm.models.WorkflowVersion.Status] | None, default None
        Status of Workflow versions to filter for to fetch. Query Parameter.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency injection.

    Returns
    -------
    workflows : list[clowm.schemas.workflow.WorkflowOut]
        Workflows in the system
    """
    current_span = trace.get_current_span()
    if developer_id is not None:  # pragma: no cover
        current_span.set_attribute("developer_id", str(developer_id))
    if name_substring is not None:  # pragma: no cover
        current_span.set_attribute("name_substring", name_substring)
    if version_status is not None and len(version_status) > 0:  # pragma: no cover
        current_span.set_attribute("version_status", [stat.name for stat in version_status])
    rbac_operation = RBACOperation.LIST
    if (
        developer_id is not None
        and current_user.uid != developer_id
        or version_status is not None
        and developer_id is None
    ):
        rbac_operation = RBACOperation.LIST_ALL

    authorization(rbac_operation)
    workflows = await crud_workflow.list_workflows(
        name_substring=name_substring,
        developer_id=developer_id,
        version_status=(
            [WorkflowVersion.WorkflowVersionStatus.PUBLISHED, WorkflowVersion.WorkflowVersionStatus.DEPRECATED]
            if version_status is None
            else version_status
        ),
    )
    return [WorkflowOut.from_db_workflow(workflow, versions=workflow.versions) for workflow in workflows]


@router.get("/{wid}", status_code=status.HTTP_200_OK, summary="Get a workflow", response_model_exclude_none=True)
@start_as_current_span_async("api_get_workflow", tracer=tracer)
async def get_workflow(
    workflow: CurrentWorkflow,
    current_user: CurrentUser,
    authorization: Authorization,
    version_status: Annotated[
        list[WorkflowVersion.WorkflowVersionStatus] | None,
        Query(
            description=f"Which versions of the workflow to include in the response. Permission `workflow:read_any` required if you are not the developer of this workflow. Default `{WorkflowVersion.WorkflowVersionStatus.PUBLISHED.name}` and `{WorkflowVersion.WorkflowVersionStatus.DEPRECATED.name}`",  # noqa: E501
        ),
    ] = None,
) -> WorkflowOut:
    """
    Get a specific workflow.

    Permission `workflow:read` required.
    \f
    Parameters
    ----------
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    version_status : list[clowm.models.WorkflowVersion.Status] | None, default None
        Status of Workflow versions to filter for to fetch. Query Parameter
    current_user : clowm.models.User
        Current user.  Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.

    Returns
    -------
    workflow : clowm.schemas.workflow.WorkflowOut
        Workflow with existing ID
    """
    current_span = trace.get_current_span()
    current_span.set_attribute("workflow_id", str(workflow.workflow_id))
    if version_status is not None and len(version_status) > 0:  # pragma: no cover
        current_span.set_attribute("version_status", [stat.name for stat in version_status])
    authorization(
        RBACOperation.READ
        if workflow.developer_id == current_user.uid and version_status is not None
        else RBACOperation.READ_ANY
    )
    version_stat = (
        [WorkflowVersion.WorkflowVersionStatus.PUBLISHED, WorkflowVersion.WorkflowVersionStatus.DEPRECATED]
        if version_status is None
        else version_status
    )
    return WorkflowOut.from_db_workflow(
        workflow, versions=[version for version in workflow.versions if version.status in version_stat]
    )


@router.delete("/{wid}", status_code=status.HTTP_204_NO_CONTENT, summary="Delete a workflow")
@start_as_current_span_async("api_workflow_delete", tracer=tracer)
async def delete_workflow(
    workflow: CurrentWorkflow,
    authorization: Authorization,
    current_user: CurrentUser,
    workflow_core: WorkflowCoreDep,
) -> None:
    """
    Delete a workflow.

    Permission `workflow:delete` required if the `developer_id` is the same as the uid of the current user,
    other `workflow:delete_any`.
    \f
    Parameters
    ----------
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    workflow_core : clowm.api.core.WorkflowCore
        Core functionality for workflow API. Dependency Injection.
    """
    trace.get_current_span().set_attribute("workflow_id", str(workflow.workflow_id))
    authorization(RBACOperation.DELETE if workflow.developer_id == current_user.uid else RBACOperation.DELETE_ANY)
    await workflow_core.delete(workflow)


@router.post(
    "/{wid}/update", status_code=status.HTTP_201_CREATED, summary="Update a workflow", response_model_exclude_none=True
)
@start_as_current_span_async("api_workflow_update", tracer=tracer)
async def update_workflow(
    workflow: CurrentWorkflow,
    current_user: CurrentUser,
    authorization: Authorization,
    workflow_core: WorkflowCoreDep,
    version_update: Annotated[WorkflowUpdate, Body(description="Meta-data for the workflow version to create")],
) -> WorkflowVersionOut:
    """
    Create a new workflow version.

    Permission `workflow:update` required.
    \f
    Parameters
    ----------
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    version_update : clowm.schemas.workflow.WorkflowUpdate
        Data about the new workflow version. HTML Body.
    workflow_core : clowm.api.core.WorkflowCore
        Core functionality for workflow API. Dependency Injection.

    Returns
    -------
    version : clowm.schemas.workflow_version.WorkflowVersionOut
        The new workflow version
    """
    trace.get_current_span().set_attribute("workflow_id", str(workflow.workflow_id))
    authorization(RBACOperation.UPDATE if current_user.uid == workflow.developer_id else RBACOperation.UPDATE_ANY)
    return await workflow_core.update(version_update, workflow)
