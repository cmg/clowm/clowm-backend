from typing import Annotated

from fastapi import APIRouter, Body, Depends, Query, status
from fastapi.requests import Request
from fastapi.responses import Response
from opentelemetry import trace

from clowm.api.dependencies.v1 import AuthorizationDependencyTokenUser as AuthorizationDependency
from clowm.api.dependencies.v1 import CurrentUserApiToken as CurrentUser
from clowm.core.rbac import RBACOperation, RBACResource
from clowm.crud import CRUDResourceVersion
from clowm.models import ResourceVersion
from clowm.otlp import start_as_current_span_async
from clowm.schemas.resource_version import (
    FileTree,
    ResourceVersionIn,
    ResourceVersionOut,
)

from ..core import ResourceCoreDep
from ..dependencies import (
    CRUDSessionDep,
    CurrentResource,
    CurrentResourceVersion,
)
from ..dependencies.auth import AuthorizationFunction

router = APIRouter(prefix="/resources/{rid}/versions", tags=["ResourceVersion"])
resource_authorization = AuthorizationDependency(resource=RBACResource.RESOURCE)
Authorization = Annotated[AuthorizationFunction, Depends(resource_authorization)]
CrudResourceVersionSession = Annotated[CRUDResourceVersion, Depends(CRUDSessionDep(CRUDResourceVersion))]
tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.get("", summary="List versions of a resource")
@start_as_current_span_async("api_list_resource_versions", tracer=tracer)
async def list_resource_versions(
    authorization: Authorization,
    resource: CurrentResource,
    current_user: CurrentUser,
    version_status: Annotated[
        list[ResourceVersion.ResourceVersionStatus] | None,
        Query(
            description="Which versions of the resource to include in the response. Permission `resource:read_any` required if None or querying for non-public resources, otherwise only permission `resource:read` required.",  # noqa: E501
        ),
    ] = None,
) -> list[ResourceVersionOut]:
    """
    List all the resource version for a specific resource.

    Permission 'resource:read' required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    resource : clowm.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency injection.
    version_status : list[clowm.models.ResourceVersion.Status] | None, default None
        Filter resource version by their status. Query Parameter.

    Returns
    -------
    versions : list[clowm.schemas.resource_version.ResourceVersionOut]
        List of versions for the request resource
    """
    current_span = trace.get_current_span()
    if version_status:  # pragma: no cover
        current_span.set_attribute("version_status", [state.name for state in version_status])
    current_span.set_attribute("resource_id", str(resource.resource_id))
    rbac_operation = (
        RBACOperation.LIST_ALL
        if resource.maintainer_id != current_user.uid
        # if status is None or contains non-public resources
        and (version_status is None or sum(map(lambda r_status: not r_status.public, version_status)) > 0)
        else RBACOperation.LIST
    )
    authorization(rbac_operation)
    return [
        ResourceVersionOut.from_db_resource_version(version)
        for version in resource.versions
        if version_status is None or version.status in version_status
    ]


@router.post("", summary="Request new version of a resource", status_code=status.HTTP_201_CREATED)
@start_as_current_span_async("api_request_resource_version", tracer=tracer)
async def request_resource_version(
    authorization: Authorization,
    resource: CurrentResource,
    resource_version_in: Annotated[
        ResourceVersionIn, Body(description="Meta-data for the resource version to request")
    ],
    current_user: CurrentUser,
    resource_core: ResourceCoreDep,
) -> ResourceVersionOut:
    """
    Request a new resource version.

    Permission `resource:update` required if the current user is the maintainer, `resource:update_any` otherwise.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    resource : clowm.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    resource_version_in : clowm.schemas.resource_version.ResourceVersionIn
        Data about the new resource version. HTTP Body.
    current_user : clowm.models.User
        Current user. Dependency injection.
    resource_core : clowm.api.core.ResourceCore
        Core functionality for resource API. Dependency Injection.

    Returns
    -------
    version : clowm.schemas.resource_version.ResourceVersionOut
        Newly created resource version
    """
    current_span = trace.get_current_span()
    current_span.set_attributes(
        {"resource_id": str(resource.resource_id), "resource_version_in": resource_version_in.model_dump_json(indent=2)}
    )
    authorization(RBACOperation.UPDATE if current_user.uid == resource.maintainer_id else RBACOperation.UPDATE_ANY)
    resource_version = await resource_core.create_version(
        resource=resource, version_in=resource_version_in, current_user=current_user
    )
    return ResourceVersionOut.from_db_resource_version(resource_version)


@router.get("/{rvid}", summary="Get version of a resource")
@start_as_current_span_async("api_get_resource_version", tracer=tracer)
async def get_resource_version(
    authorization: Authorization,
    resource: CurrentResource,
    current_user: CurrentUser,
    resource_version: CurrentResourceVersion,
) -> ResourceVersionOut:
    """
    Get a specific resource version for a specific resource.

    Permission `resource:read` required. If the status of the resource version is not `LATEST` or `SYNCHRONIZED` and
    the current user is not the maintainer, then the permission `resource:read_any` is required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    resource : clowm.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency injection.
    resource_version : clowm.models.ResourceVersion
        Resource Version associated with the ID in the path. Dependency Injection.

    Returns
    -------
    version : clowm.schemas.resource_version.ResourceVersionOut
        Requested resource version
    """
    trace.get_current_span().set_attributes(
        {"resource_id": str(resource.resource_id), "resource_version_id": str(resource_version.resource_version_id)}
    )
    rbac_operation = RBACOperation.READ
    # Maintainer can read any version of his workflow, others only synchronized and latest ones
    if current_user.uid != resource.maintainer_id and resource_version.status not in [
        ResourceVersion.ResourceVersionStatus.SYNCHRONIZED,
        ResourceVersion.ResourceVersionStatus.LATEST,
    ]:
        rbac_operation = RBACOperation.READ_ANY
    authorization(rbac_operation)
    return ResourceVersionOut.from_db_resource_version(resource_version)


@router.get(
    "/{rvid}/tree",
    summary="Download folder structure of resource",
    response_model_exclude_none=True,
    response_model=list[FileTree],
)
@start_as_current_span_async("api_get_resource_version_folder_structure", tracer=tracer)
async def resource_file_tree(
    authorization: Authorization,
    request: Request,
    resource_version: CurrentResourceVersion,
    resource_core: ResourceCoreDep,
) -> Response:
    """
    Get the folder structure of the resources. Only available if the resource was previously downloaded to the cluster.

    Permission `resource:read` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    request : fastapi.requests.Request
        Raw request object to read headers from.
    resource_version : clowm.models.ResourceVersion
        Resource Version associated with the ID in the path. Dependency Injection.
    resource_core : clowm.api.core.ResourceCore
        Core functionality for resource API. Dependency Injection.

    Returns
    -------
    response : fastapi.responses.Response
        Raw response object containing the file
    """
    authorization(RBACOperation.READ)
    return await resource_core.download_resource_file_tree(resource_version, request=request)


@router.put("/{rvid}/request_review", summary="Request resource version review")
@start_as_current_span_async("api_request_resource_version_review", tracer=tracer)
async def request_resource_version_review(
    authorization: Authorization,
    resource: CurrentResource,
    resource_version: CurrentResourceVersion,
    current_user: CurrentUser,
    resource_core: ResourceCoreDep,
) -> ResourceVersionOut:
    """
    Request the review of a resource version.

    Permission `resource:update` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    resource : clowm.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency injection.
    resource_version : clowm.models.ResourceVersion
        Resource Version associated with the ID in the path. Dependency Injection.
    resource_core : clowm.api.core.ResourceCore
        Core functionality for resource API. Dependency Injection.

    Returns
    -------
    version : clowm.schemas.resource_version.ResourceVersionOut
        Resource version with updated status
    """
    trace.get_current_span().set_attributes(
        {"resource_id": str(resource.resource_id), "resource_version_id": str(resource_version.resource_version_id)}
    )
    authorization(RBACOperation.UPDATE if current_user.uid == resource.maintainer_id else RBACOperation.UPDATE_ANY)
    version = await resource_core.request_version_review(
        resource=resource, version=resource_version, current_uid=current_user.uid
    )
    return ResourceVersionOut.from_db_resource_version(version)
