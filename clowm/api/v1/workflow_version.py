from typing import Annotated

from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    Path,
    Query,
    status,
)
from opentelemetry import trace

from clowm.api.dependencies.v1 import AuthorizationDependencyTokenUser as AuthorizationDependency
from clowm.api.dependencies.v1 import CurrentUserApiToken as CurrentUser
from clowm.core.rbac import RBACOperation, RBACResource
from clowm.crud import CRUDWorkflowVersion
from clowm.models import WorkflowVersion
from clowm.otlp import start_as_current_span_async
from clowm.schemas.workflow_version import (
    WorkflowVersionMetadataOut,
    WorkflowVersionOut,
)

from ..core import WorkflowCoreDep
from ..dependencies import (
    CRUDSessionDep,
    CurrentWorkflow,
    CurrentWorkflowVersion,
)
from ..dependencies.auth import AuthorizationFunction

router = APIRouter(prefix="/workflows/{wid}/versions", tags=["Workflow Version"])
Authorization = Annotated[AuthorizationFunction, Depends(AuthorizationDependency(resource=RBACResource.WORKFLOW))]
CrudWorkflowVersionSession = Annotated[CRUDWorkflowVersion, Depends(CRUDSessionDep(CRUDWorkflowVersion))]

tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.get(
    "", status_code=status.HTTP_200_OK, summary="Get all versions of a workflow", response_model_exclude_none=True
)
@start_as_current_span_async("api_workflow_version_list", tracer=tracer)
async def list_workflow_version(
    current_user: CurrentUser,
    workflow: CurrentWorkflow,
    crud_workflow_version: CrudWorkflowVersionSession,
    authorization: Authorization,
    version_status: Annotated[
        list[WorkflowVersion.WorkflowVersionStatus] | None,
        Query(
            description=f"Which versions of the workflow to include in the response. Permission `workflow:list_filter` required if you are not the developer of this workflow. Default `{WorkflowVersion.WorkflowVersionStatus.PUBLISHED.name}` and `{WorkflowVersion.WorkflowVersionStatus.DEPRECATED.name}`",  # noqa: E501
        ),
    ] = None,
) -> list[WorkflowVersionOut]:
    """
    List all versions of a Workflow.

    Permission `workflow:list` required.
    \f
    Parameters
    ----------
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    version_status : list[clowm.models.WorkflowVersion.Status] | None, default None
        Status of Workflow versions to filter for to fetch. Query Parameter
    crud_workflow_version : clowm.crud.crud_workflow_version.CRUDWorkflowVersion
        Active CRUD repository for workflow versions. Dependency Injection.
    current_user : clowm.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.

    Returns
    -------
    versions : [clowm.schemas.workflow_version.WorkflowVersion]
        All versions of the workflow
    """
    current_span = trace.get_current_span()
    current_span.set_attribute("workflow_id", str(workflow.workflow_id))
    if version_status is not None and len(version_status) > 0:
        current_span.set_attribute("version_status", [stat.name for stat in version_status])
    authorization(
        RBACOperation.LIST
        if workflow.developer_id == current_user.uid and version_status is not None
        else RBACOperation.LIST_ALL
    )

    versions = await crud_workflow_version.list_workflow_versions(
        workflow.workflow_id,
        version_status=(
            version_status
            if version_status is not None
            else [WorkflowVersion.WorkflowVersionStatus.PUBLISHED, WorkflowVersion.WorkflowVersionStatus.DEPRECATED]
        ),
    )
    return [WorkflowVersionOut.from_db_version(v, load_modes=True) for v in versions]


@router.get(
    "/{git_commit_hash}",
    status_code=status.HTTP_200_OK,
    summary="Get a workflow version",
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_workflow_version_get", tracer=tracer)
async def get_workflow_version(
    workflow: CurrentWorkflow,
    crud_workflow_version: CrudWorkflowVersionSession,
    current_user: CurrentUser,
    authorization: Authorization,
    git_commit_hash: Annotated[
        str,
        Path(
            description="Git commit `git_commit_hash` of specific version or `latest`.",
            pattern=r"^([0-9a-f]{40}|latest)$",
            examples=["latest", "ba8bcd9294c2c96aedefa1763a84a18077c50c0f"],
        ),
    ],
) -> WorkflowVersionOut:
    """
    Get a specific version of a workflow.

    Permission `workflow:read` required if the version is public or you are the developer of the workflow,
    otherwise `workflow:read_any`
    \f
    Parameters
    ----------
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    git_commit_hash: str
        Version ID
    crud_workflow_version : clowm.crud.crud_workflow_version.CRUDWorkflowVersion
        Active CRUD repository for workflow versions. Dependency Injection.
    current_user : clowm.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.

    Returns
    -------
    version : clowm.schemas.workflow_version.WorkflowVersionOut
        The specified WorkflowVersion
    """
    trace.get_current_span().set_attributes(
        {"workflow_id": str(workflow.workflow_id), "workflow_version_id": git_commit_hash}
    )
    rbac_operation = RBACOperation.READ
    version = (
        await crud_workflow_version.get_latest(workflow.workflow_id)
        if git_commit_hash == "latest"
        else await crud_workflow_version.get(git_commit_hash, workflow_id=workflow.workflow_id)
    )
    if version is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Workflow Version with git_commit_hash '{git_commit_hash}' not found",
        )
    # Developer can read any version of his workflow, others only published and deprecated ones
    if (
        current_user.uid != workflow.developer_id
        and version.status != WorkflowVersion.WorkflowVersionStatus.PUBLISHED
        and version.status != WorkflowVersion.WorkflowVersionStatus.DEPRECATED
    ):
        rbac_operation = RBACOperation.READ_ANY
    authorization(rbac_operation)
    return WorkflowVersionOut.from_db_version(version, load_modes=True)


@router.get(
    "/{git_commit_hash}/metadata",
    status_code=status.HTTP_200_OK,
    summary="Get metadata of workflow version",
)
@start_as_current_span_async("api_get_workflow_version_status_metadata", tracer=tracer)
async def get_workflow_version_metadata(
    workflow: CurrentWorkflow,
    workflow_version: CurrentWorkflowVersion,
    authorization: Authorization,
    current_user: CurrentUser,
) -> WorkflowVersionMetadataOut:
    """
    Get the metadata of a workflow version.

    Permission `workflow:read` required if the current user is the developer of the workflow,
    otherwise `workflow:read_any`
    \f
    Parameters
    ----------
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    workflow_version : clowm.models.WorkflowVersion
        Workflow version with given ID. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user: clowm.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.

    Returns
    -------
    meta_data : clowm.schemas.workflow_version.WorkflowVersionMetadataOut
        Current meta-data of the workflow version
    """
    trace.get_current_span().set_attributes(
        {"workflow_id": str(workflow_version.workflow_id), "workflow_version_id": workflow_version.git_commit_hash}
    )
    authorization(RBACOperation.READ if workflow.developer_id == current_user.uid else RBACOperation.READ_ANY)
    return WorkflowVersionMetadataOut.from_db_model(workflow_version)


@router.patch(
    "/{git_commit_hash}/deprecate",
    status_code=status.HTTP_200_OK,
    summary="Deprecate a workflow version",
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_workflow_version_status_update", tracer=tracer)
async def deprecate_workflow_version(
    workflow: CurrentWorkflow,
    workflow_version: CurrentWorkflowVersion,
    authorization: Authorization,
    current_user: CurrentUser,
    workflow_core: WorkflowCoreDep,
) -> WorkflowVersionOut:
    """
    Deprecate a workflow version.

    Permission `workflow:update` required if you are the developer of the workflow,
    otherwise `workflow:update_status`
    \f
    Parameters
    ----------
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    workflow_version : clowm.models.WorkflowVersion
        Workflow version with given ID. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user: clowm.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    workflow_core : clowm.api.core.WorkflowCore
        Core functionality for workflow API. Dependency Injection.

    Returns
    -------
    version : clowm.models.WorkflowVersion
        Version of the workflow with deprecated status
    """
    trace.get_current_span().set_attributes(
        {"workflow_id": str(workflow_version.workflow_id), "workflow_version_id": workflow_version.git_commit_hash}
    )
    authorization(RBACOperation.UPDATE if current_user.uid == workflow.developer_id else RBACOperation.UPDATE_STATUS)
    await workflow_core.deprecate_version(workflow_version)
    return WorkflowVersionOut.from_db_version(workflow_version, load_modes=True)
