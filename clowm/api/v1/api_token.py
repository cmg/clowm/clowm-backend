from typing import Annotated
from uuid import UUID

from fastapi import APIRouter, Depends, Query, status
from opentelemetry import trace

from clowm.api.dependencies.v1 import AuthorizationDependencyTokenUser as AuthorizationDependency
from clowm.api.dependencies.v1 import CurrentUserApiToken as CurrentUser
from clowm.core.rbac import RBACOperation, RBACResource
from clowm.crud import CRUDApiToken
from clowm.otlp import start_as_current_span_async
from clowm.schemas.api_token import ApiTokenOut

from ..dependencies import CRUDSessionDep, PathToken
from ..dependencies.auth import AuthorizationFunction

router = APIRouter(prefix="/tokens", tags=["APIToken"])

api_token_authorization = AuthorizationDependency(resource=RBACResource.API_TOKEN)
Authorization = Annotated[AuthorizationFunction, Depends(api_token_authorization)]
CrudApiTokenSession = Annotated[CRUDApiToken, Depends(CRUDSessionDep(CRUDApiToken))]
tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.get("", summary="List API token")
@start_as_current_span_async("api_list_api_token", tracer=tracer)
async def list_token(
    crud_api_token: CrudApiTokenSession,
    current_user: CurrentUser,
    authorization: Authorization,
    uid: Annotated[
        UUID | None,
        Query(
            description="UID of the user to filter for. Permission `api_token:list` required if current users is the target.",
            examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
        ),
    ] = None,
) -> list[ApiTokenOut]:
    """
    List meta information about all API token.

    Permission `api_token:list_all` required. See parameter `uid` for exception.
    \f
    Parameters
    ----------
    crud_api_token : clowm.crud.crud_api_token.CRUDApiToken
        Active CRUD repository for api tokens. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    uid : uuid.UUID | None, default None
        Filter for Api tokens that belong to this user. Query parameter.

    Returns
    -------
    api_tokens : list[clowm.schemas.api_token.ApiTokenOut]
        List of all filtered token.
    """
    if uid is not None:  # pragma: no cover
        trace.get_current_span().set_attribute("uid", str(uid))
    authorization(RBACOperation.LIST if uid == current_user.uid else RBACOperation.LIST_ALL)
    return [ApiTokenOut.from_db(token) for token in await crud_api_token.list(uid=uid)]


@router.get("/{tid}", summary="Get API token")
@start_as_current_span_async("api_get_api_token", tracer=tracer)
async def get_token(token: PathToken, authorization: Authorization, current_user: CurrentUser) -> ApiTokenOut:
    """
    Get an API token by id.

    Permission `api_token:read` required if the current user is the owner of the API token,
    otherwise `api_token:read_any` required.
    \f
    Parameters
    ----------
    token : clowm.models.ApiToken
        The token belonging to the id. Dependency injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.

    Returns
    -------
    api_token : clowm.schemas.api_token.ApiTokenOut
        The Api token belonging the given id
    """
    authorization(RBACOperation.READ if current_user == token.user else RBACOperation.READ_ANY)
    return ApiTokenOut.from_db(token)


@router.delete("/{tid}", status_code=status.HTTP_204_NO_CONTENT, summary="Delete API token")
@start_as_current_span_async("api_delete_api_token", tracer=tracer)
async def delete_token(
    crud_api_token: CrudApiTokenSession, token: PathToken, authorization: Authorization, current_user: CurrentUser
) -> None:
    """
    Delete an API token by id.

    Permission `api_token:delete` required if the current user is the owner of the API token,
    otherwise `api_token:delete_any` required.
    \f
    Parameters
    ----------
    crud_api_token : clowm.crud.crud_api_token.CRUDApiToken
        Active CRUD repository for api tokens. Dependency Injection.
    token : clowm.models.ApiToken
        Token to delete. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    """
    authorization(RBACOperation.DELETE if current_user == token.user else RBACOperation.DELETE_ANY)
    return await crud_api_token.delete(tid=token.token_id)
