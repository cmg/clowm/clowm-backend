from typing import Annotated
from uuid import UUID

from fastapi import APIRouter, Body, Depends, Query, status
from opentelemetry import trace

from clowm.api.dependencies.v1 import AuthorizationDependencyTokenUser as AuthorizationDependency
from clowm.api.dependencies.v1 import CurrentUserApiToken as CurrentUser
from clowm.core.rbac import RBACOperation, RBACResource
from clowm.crud import CRUDResource
from clowm.models import ResourceVersion
from clowm.otlp import start_as_current_span_async
from clowm.schemas.resource import ResourceIn, ResourceOut

from ..core.resource import ResourceCoreDep
from ..dependencies import CRUDSessionDep, CurrentResource
from ..dependencies.auth import AuthorizationFunction

router = APIRouter(prefix="/resources", tags=["Resource"])
resource_authorization = AuthorizationDependency(resource=RBACResource.RESOURCE)
Authorization = Annotated[AuthorizationFunction, Depends(resource_authorization)]
CrudResourceSession = Annotated[CRUDResource, Depends(CRUDSessionDep(CRUDResource))]
tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.post("", summary="Request a new resource", status_code=status.HTTP_201_CREATED)
@start_as_current_span_async("api_request_resource", tracer=tracer)
async def create_resource(
    authorization: Authorization,
    current_user: CurrentUser,
    resource_in: Annotated[ResourceIn, Body(description="Meta-data for the resource to request")],
    resource_core: ResourceCoreDep,
) -> ResourceOut:
    """
    Request a new resources.

    Permission `resource:create` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency injection.
    resource_in : clowm.schemas.resource.ResourceIn
        Data about the new resource. HTTP Body.
    resource_core : clowm.api.core.ResourceCore
        Core functionality for resource API. Dependency Injection.

    Returns
    -------
    resource_out : clowm.schemas.resource.ResourceOut
        Newly created resource
    """
    current_span = trace.get_current_span()
    current_span.set_attribute("resource_in", resource_in.model_dump_json())
    authorization(RBACOperation.CREATE)
    resource = await resource_core.create(resource_in, current_user)
    return ResourceOut.from_db_resource(db_resource=resource)


@router.get("", summary="List resources")
@start_as_current_span_async("api_list_resources", tracer=tracer)
async def list_resources(
    authorization: Authorization,
    current_user: CurrentUser,
    crud_resource: CrudResourceSession,
    maintainer_id: Annotated[
        UUID | None,
        Query(
            description="Filter for resource by maintainer. If current user is the same as maintainer ID, permission `resource:list` required, otherwise `resource:list_filter`.",  # noqa: E501
            examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
        ),
    ] = None,
    version_status: Annotated[
        list[ResourceVersion.ResourceVersionStatus] | None,
        Query(
            description="Which versions of the resource to include in the response. Permission `resource:list_filter` required if None or querying for non-public resources, otherwise only permission `resource:list` required.",  # noqa: E501
        ),
    ] = None,
    name_substring: Annotated[
        str | None,
        Query(max_length=32, description="Filter resources by a substring in their name.", examples=["gtdb"]),
    ] = None,
    public: Annotated[bool | None, Query(description="Filter resources to by the public flag")] = None,
) -> list[ResourceOut]:
    """
    List all resources.

    Permission `resource:list` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency injection.
    crud_resource : clowm.crud.crud_resource.CRUDResource
        Active CRUD repository for buckets. Dependency Injection.
    maintainer_id : uuid.UUID | None, default None
        Filter resources by a maintainer. Query Parameter.
    version_status : list[clowm.models.ResourceVersion.Status] | None, default None
        Filter resource version by their status. Query Parameter.
    name_substring : str | None, default None
        Filter resources by a substring in their name. Query Parameter.
    public : bool | None, default None
      Filter resources to include only public resources. Query Parameters.

    Returns
    -------
    resources : list[clowm.schemas.resource.ResourceOut]
        List of resource based on filters
    """
    current_span = trace.get_current_span()
    if maintainer_id:  # pragma: no cover
        current_span.set_attribute("maintainer_id", str(maintainer_id))
    if version_status:  # pragma: no cover
        current_span.set_attribute("version_status", [state.name for state in version_status])
    if name_substring:  # pragma: no cover
        current_span.set_attribute("name_substring", name_substring)
    if public is not None:  # pragma: no cover
        current_span.set_attribute("public", public)

    rbac_operation = RBACOperation.LIST
    if maintainer_id is not None:
        if current_user.uid != maintainer_id:
            rbac_operation = RBACOperation.LIST_ALL
    # if status is None or contains non-public resources
    elif version_status is None or sum(map(lambda r_status: not r_status.public, version_status)) > 0:
        rbac_operation = RBACOperation.LIST_ALL
    authorization(rbac_operation)
    resources = await crud_resource.list_resources(
        name_substring=name_substring, maintainer_id=maintainer_id, version_status=version_status, public=public
    )
    return [ResourceOut.from_db_resource(resource) for resource in resources]


@router.get("/{rid}", summary="Get a resource")
@start_as_current_span_async("api_get_resource", tracer=tracer)
async def get_resource(
    authorization: Authorization,
    resource: CurrentResource,
    current_user: CurrentUser,
    version_status: Annotated[
        list[ResourceVersion.ResourceVersionStatus] | None,
        Query(
            description="Which versions of the resource to include in the response. Permission `resource:read_any` required if None or querying for non-public resources, otherwise only permission `resource:read` required.",  # noqa: E501
        ),
    ] = None,
) -> ResourceOut:
    """
    Get a specific resource.

    Permission `resource:read` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency injection.
    resource : clowm.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    version_status : list[clowm.models.ResourceVersion.Status] | None, default None
        Filter resource version by their status. Query Parameter.

    Returns
    -------
    resource : clowm.schemas.resource.ResourceOut
        The requested resource
    """
    current_span = trace.get_current_span()
    if version_status:  # pragma: no cover
        current_span.set_attribute("version_status", [state.name for state in version_status])
    current_span.set_attribute("resource_id", str(resource.resource_id))
    rbac_operation = (
        RBACOperation.READ_ANY
        if resource.maintainer_id != current_user.uid
        # if status is None or contains non-public resources
        and (version_status is None or sum(map(lambda r_status: not r_status.public, version_status)) > 0)
        else RBACOperation.READ
    )
    authorization(rbac_operation)
    return ResourceOut.from_db_resource(
        resource,
        versions=[
            version for version in resource.versions if version_status is None or version.status in version_status
        ],
    )


@router.delete("/{rid}", summary="Delete a resource", status_code=status.HTTP_204_NO_CONTENT)
@start_as_current_span_async("api_delete_resource", tracer=tracer)
async def delete_resource(
    authorization: Authorization,
    resource: CurrentResource,
    resource_core: ResourceCoreDep,
) -> None:
    """
    Delete a resources.

    Permission `resource:delete` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    resource : clowm.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    resource_core : clowm.api.core.ResourceCore
        Core functionality for resource API. Dependency Injection.
    """
    trace.get_current_span().set_attribute("resource_id", str(resource.resource_id))
    authorization(RBACOperation.DELETE)
    await resource_core.delete(resource)
