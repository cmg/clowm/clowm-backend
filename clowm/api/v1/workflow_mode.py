from typing import Annotated

from fastapi import APIRouter, Depends, status
from opentelemetry import trace

from clowm.core.rbac import RBACOperation, RBACResource
from clowm.otlp import start_as_current_span_async

from ...models import WorkflowMode
from ...schemas.workflow_mode import WorkflowModeOut
from ..dependencies import CurrentWorkflowMode
from ..dependencies.auth import AuthorizationFunction
from ..dependencies.v1 import AuthorizationDependencyTokenUser as AuthorizationDependency

router = APIRouter(prefix="/workflow_modes", tags=["Workflow Mode"])
workflow_authorization = AuthorizationDependency(resource=RBACResource.WORKFLOW)
Authorization = Annotated[AuthorizationFunction, Depends(workflow_authorization)]

tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.get("/{mode_id}", status_code=status.HTTP_200_OK, summary="Get workflow mode", response_model=WorkflowModeOut)
@start_as_current_span_async("api_workflow_mode_get", tracer=tracer)
async def get_workflow_mode(authorization: Authorization, mode: CurrentWorkflowMode) -> WorkflowMode:
    """
    Get a workflow mode.

    Permission `workflow:read` required
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    mode : clowm.models.WorkflowMode
        ID of workflow mode. Path parameter.

    Returns
    -------
    mode : clowm.models.WorkflowMode
        Mode with the given ID.
    """
    trace.get_current_span().set_attribute("workflow_mode_id", str(mode.mode_id))
    authorization(RBACOperation.READ)
    return mode
