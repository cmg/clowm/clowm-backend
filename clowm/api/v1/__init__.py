from typing import Any

from fastapi import APIRouter, Depends, Response, status
from fastapi.openapi.docs import get_swagger_ui_html
from fastapi.openapi.utils import get_openapi
from fastapi.responses import HTMLResponse, JSONResponse
from fastapi.routing import APIRoute

from clowm.core.config import settings
from clowm.schemas.security import ErrorDetail

from ..dependencies.v1.auth import get_api_token_user
from .api_token import router as token_router
from .bucket import router as bucket_router
from .bucket_permission import router as permission_router
from .resource import router as resource_router
from .resource_version import router as resource_version_router
from .s3key import router as key_router
from .user import router as user_router
from .workflow import router as workflow_router
from .workflow_execution import router as execution_router
from .workflow_mode import router as mode_router
from .workflow_version import router as workflow_version_router

schema: dict[str, Any] | None = None

description = """
This is the API documentation of the CloWM Service.

Look in the [Git repository](https://gitlab.ub.uni-bielefeld.de/cmg/clowm/clowm-backend/-/blob/main/RBAC.md)
to see which role has which permission.
"""

alternative_responses: dict[int | str, dict[str, Any]] = {
    status.HTTP_400_BAD_REQUEST: {
        "model": ErrorDetail,
        "description": "Error decoding JWT Token",
        "content": {"application/json": {"example": {"detail": "Malformed JWT Token"}}},
    },
    status.HTTP_401_UNAUTHORIZED: {
        "model": ErrorDetail,
        "description": "Not Authenticated",
        "content": {"application/json": {"example": {"detail": "Not authenticated"}}},
    },
    status.HTTP_403_FORBIDDEN: {
        "model": ErrorDetail,
        "description": "Not Authorized",
        "content": {"application/json": {"example": {"detail": "Action Forbidden. Permission user:read is missing"}}},
    },
    status.HTTP_404_NOT_FOUND: {
        "model": ErrorDetail,
        "description": "Entity not Found",
        "content": {"application/json": {"example": {"detail": "Entity not found."}}},
    },
}


def custom_generate_unique_id(route: APIRoute) -> str:
    return f"{route.tags[-1]}-v1-{route.name}"


api_router = APIRouter(
    prefix="/v1", responses=alternative_responses, generate_unique_id_function=custom_generate_unique_id
)

api_router.include_router(token_router, dependencies=[Depends(get_api_token_user)])
api_router.include_router(bucket_router, dependencies=[Depends(get_api_token_user)])
api_router.include_router(permission_router, dependencies=[Depends(get_api_token_user)])
api_router.include_router(user_router, dependencies=[Depends(get_api_token_user)])
api_router.include_router(key_router, dependencies=[Depends(get_api_token_user)])
api_router.include_router(workflow_router, dependencies=[Depends(get_api_token_user)])
api_router.include_router(workflow_version_router, dependencies=[Depends(get_api_token_user)])
api_router.include_router(mode_router, dependencies=[Depends(get_api_token_user)])
api_router.include_router(execution_router, dependencies=[Depends(get_api_token_user)])
api_router.include_router(resource_router, dependencies=[Depends(get_api_token_user)])
api_router.include_router(resource_version_router, dependencies=[Depends(get_api_token_user)])


@api_router.get("/openapi.json", include_in_schema=False, tags=["miscellaneous"])
async def openapi_route() -> Response:
    global schema, api_router
    if schema is None:
        schema = get_openapi(
            title="CloWM API",
            description=description,
            version="v1.0",
            routes=api_router.routes,
            license_info={"name": "Apache 2.0", "identifier": "Apache-2.0"},
            contact={
                "name": "Daniel Göbel",
                "url": "https://ekvv.uni-bielefeld.de/pers_publ/publ/PersonDetail.jsp?personId=223066601",
                "email": "support@clowm.bi.denbi.de",
            },
            servers=[{"url": settings.api_prefix}] if settings.api_prefix else None,
            terms_of_service="https://clowm.bi.denbi.de/terms",
        )
    return JSONResponse(schema)


@api_router.get("/docs", include_in_schema=False, tags=["miscellaneous"])
async def swagger_ui_html() -> HTMLResponse:
    return get_swagger_ui_html(
        openapi_url=(settings.api_prefix if settings.api_prefix else "") + api_router.prefix + "/openapi.json",
        title="CloWM - Swagger UI",
        swagger_favicon_url="/favicon.ico",
    )
