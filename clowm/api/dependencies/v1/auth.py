from functools import partial
from typing import Annotated

from fastapi import BackgroundTasks, Depends, HTTPException, Request, status
from fastapi.security import APIKeyHeader
from opentelemetry import trace

from clowm.crud import CRUDApiToken, CRUDUser
from clowm.db.types import ApiTokenScopesDB
from clowm.models import User

from ...background.metrics import update_last_used_api_token
from ..auth import AuthorizationDependency, AuthorizationFunction
from ..crud import CRUDSessionDep

tracer = trace.get_tracer_provider().get_tracer(__name__)
CrudApiTokenSession = Annotated[CRUDApiToken, Depends(CRUDSessionDep(CRUDApiToken))]
CrudUserSession = Annotated[CRUDUser, Depends(CRUDSessionDep(CRUDUser))]

header_token_scheme = APIKeyHeader(
    name="X-CLOWM-TOKEN",
    scheme_name="API Token",
    description="The API token in the header is used to authenticate a user.",
)

__all__ = ["CurrentUserApiToken", "AuthorizationDependencyTokenUser", "header_token_scheme"]


async def get_api_token_user(
    request: Request,
    crud_api_token: CrudApiTokenSession,
    api_token: Annotated[str, Depends(header_token_scheme)],
    background_tasks: BackgroundTasks,
) -> User:
    """
    Get the api token from the HTTP header.

    Parameters
    ----------
    crud_api_token : clowm.crud.crud_api_token.CRUDApiToken
        Active CRUD repository for api tokens. Dependency Injection.
    api_token : str | None
        The value of the api token cookie if it exists
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    request : fastapi.Request
        Raw request object. Provided by FastAPI.

    Returns
    -------
    api_token_db : clowm.models.ApiToken
        The Api token from the db if it exists

    Raises
    ------
    fastapi.HTTPException
        If the token verification fails.
    """
    token = await crud_api_token.search(api_token)
    if token is None:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="token not found")
    trace.get_current_span().set_attribute("token_id", str(token.token_id))
    if token.expired:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="token is expired")
    if request.method == "GET" and not token.has_scope(ApiTokenScopesDB.READ):
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="token is missing the 'read' scope")
    if request.method != "GET" and not token.has_scope(ApiTokenScopesDB.WRITE):
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="token is missing the 'write' scope")
    background_tasks.add_task(update_last_used_api_token, token.token_id)
    return token.user


CurrentUserApiToken = Annotated[User, Depends(get_api_token_user)]


class AuthorizationDependencyTokenUser(AuthorizationDependency):
    """
    Class to inject the current user based on the API token into the RBAC check
    """

    def __call__(self, user: CurrentUserApiToken) -> AuthorizationFunction:
        return partial(self.authorization_wrapper, user=user)
