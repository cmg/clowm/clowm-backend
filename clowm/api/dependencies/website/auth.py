from functools import partial
from typing import Annotated
from uuid import UUID

from authlib.jose.errors import BadSignatureError, DecodeError, ExpiredTokenError
from fastapi import Depends, HTTPException, status
from fastapi.security import APIKeyCookie
from opentelemetry import trace

from clowm.core import auth
from clowm.core.config import settings
from clowm.crud import CRUDApiToken, CRUDUser
from clowm.models import User
from clowm.schemas.security import JWT

from ..auth import AuthorizationDependency, AuthorizationFunction
from ..crud import CRUDSessionDep
from ..user import get_path_user

tracer = trace.get_tracer_provider().get_tracer(__name__)
CrudApiTokenSession = Annotated[CRUDApiToken, Depends(CRUDSessionDep(CRUDApiToken))]
CrudUserSession = Annotated[CRUDUser, Depends(CRUDSessionDep(CRUDUser))]

cookie_jwt_scheme = APIKeyCookie(
    name="clowm-jwt",
    scheme_name="Session Token",
    description="The JWT in this cookie is used by the website and should not be used when communicating directly with the API.",
)

__all__ = ["CurrentUserCookie", "AuthorizationDependencyCookieUser"]


async def get_cookie_jwt_user(
    crud_user: CrudUserSession,
    cookie_jwt: Annotated[str, Depends(cookie_jwt_scheme)],
) -> User:
    """
    Get the decoded JWT or reject request if it is not valid.

    Parameters
    ----------
    crud_user : clowm.crud.crud_user.CRUDUser
        Active CRUD repository for users. Dependency Injection.
    cookie_jwt : str
        JWT sent with the HTTP request as cookie. Dependency Injection.

    Returns
    -------
    token : clowm.models.User
        The user associated with the JWT

    Raises
    ------
    fastapi.HTTPException
        If the JWT validation fails.
    """
    # if the JWT validation fails or the JWT is expired, delete the cookie
    delete_jwt_cookie_header = {
        "set-cookie": f'{cookie_jwt_scheme.model.name}=""; Domain={settings.ui_uri.host}; Max-Age=0; Path=/; SameSite=lax; Secure'
    }
    try:
        jwt = JWT(**auth.decode_token(cookie_jwt), raw_token=cookie_jwt)
        trace.get_current_span().set_attribute("jwt_exp", jwt.exp.isoformat())
        return await get_path_user(crud_user=crud_user, uid=UUID(jwt.sub))  # make sure the user exists
    except HTTPException as err:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="deleted user", headers=delete_jwt_cookie_header
        ) from err
    except ExpiredTokenError as err:  # pragma: no cover
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="JWT signature has expired",
            headers=delete_jwt_cookie_header,
        ) from err
    except (DecodeError, BadSignatureError, ValueError) as err:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="malformed JWT", headers=delete_jwt_cookie_header
        ) from err


CurrentUserCookie = Annotated[User, Depends(get_cookie_jwt_user)]


class AuthorizationDependencyCookieUser(AuthorizationDependency):
    """
    Class to inject the current user based on the session cookie into the RBAC check
    """

    def __call__(self, user: CurrentUserCookie) -> AuthorizationFunction:
        return partial(self.authorization_wrapper, user=user)
