from abc import ABC, abstractmethod
from collections.abc import Callable

from fastapi import HTTPException, status
from opentelemetry import trace

from clowm.core.rbac import RBACOperation, RBACPermission, RBACResource
from clowm.models import User

tracer = trace.get_tracer_provider().get_tracer(__name__)

AuthorizationFunction = Callable[[RBACOperation], None]


class AuthorizationDependency(ABC):
    """
    Abstract class to parameterize the authorization request with the resource to perform an operation on.
    """

    def __init__(self, resource: RBACResource) -> None:
        """
        Parameters
        ----------
        resource : str
            RBAC Resource parameter for the authorization requests
        """
        self.resource = resource

    @abstractmethod
    def __call__(
        self,
        user: User,
    ) -> AuthorizationFunction:
        """
        Get the function to check if the user has a role that contains the required permission.

        Parameters
        ----------
        user : clowm.models.User
            The current user based on the JWT or Api token. Dependency Injection.

        Returns
        -------
        authorization_function : Callable[[clowm.core.rbac.RBACOperation], None]
            Function which expects and RBAC operation for the RBAC check.
        """
        ...

    def authorization_wrapper(self, operation: RBACOperation, user: User) -> None:
        """
        Check if the provided user has a role that contains the required permission.

        Parameters
        ----------
        operation : clowm.core.rbac.RBACOperation
            RBAC operation that needs to be checked.
        user : clowm.models.User
            Current user associated with the request

        Raises
        ------
        fastapi.HTTPException
            If the authorization check fails, an HTTP exception is raised with the status code 403
        """
        with tracer.start_as_current_span(
            "authorization", attributes={"RBACResource": self.resource.name, "RBACOperation": operation.name}
        ):
            requested_permission = RBACPermission(resource=self.resource, operation=operation)
            if not requested_permission.user_authorized(user=user):
                raise HTTPException(
                    status_code=status.HTTP_403_FORBIDDEN,
                    detail=f"Action forbidden. Permission {requested_permission} is missing",
                )
