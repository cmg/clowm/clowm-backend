from typing import TYPE_CHECKING, Annotated

from fastapi import Depends
from fastapi.requests import Request
from httpx import AsyncClient
from opentelemetry import trace
from rgwadmin import RGWAdmin

from clowm.ceph.rgw import rgw
from clowm.core.oidc import OIDCClient

if TYPE_CHECKING:
    from types_aiobotocore_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object

tracer = trace.get_tracer_provider().get_tracer(__name__)


def get_rgw_admin() -> RGWAdmin:  # pragma: no cover
    """
    Wrap the RGWAdmin object into a dependency injection.

    Returns
    -------
    rgw : rgwadmin.RGWAdmin
        The RGWAdmin object from clowm.ceph.rgw
    """
    return rgw


RGWService = Annotated[RGWAdmin, Depends(get_rgw_admin)]


def get_s3_resource(request: Request) -> S3ServiceResource:  # pragma: no cover
    """
    Get an async S3 service with an open connection.

    Parameters
    ----------
    request : fastapi.Request
        Request object from FastAPI where the client is attached to.

    Returns
    -------
    client : types_aiobotocore_s3.service_resource.S3ServiceResource
        Async S3 resource with open connection
    """
    return request.app.s3_resource


S3Service = Annotated[S3ServiceResource, Depends(get_s3_resource)]


def get_oidc_client(request: Request) -> OIDCClient:
    """
    Wrap the OIDCClient object that has all provider registered during the lifespan events and
    is attached to each request object into a dependency injection.

    Parameters
    ----------
    request : fastapi.Request
        The raw request object with the attached OIDCClient object.

    Returns
    -------
    oidc_client : clowm.core.oidc.OIDCClient
        The OIDCClient object with all providers registered.
    """
    return request.app.oidc_client  # type: ignore[attr-defined]


OIDCClientDep = Annotated[OIDCClient, Depends(get_oidc_client)]


async def get_httpx_client(request: Request) -> AsyncClient:  # pragma: no cover
    """
    Wrap the AsyncClient object that was opened during the lifespan events and
    is attached to each request object into a dependency injection.

    Parameters
    ----------
    request : fastapi.Request
        The raw request object with the attached AsyncClient.

    Returns
    -------
    httpx_client : httpx.AsyncClient
        The AsyncClient object with with open connection.
    """
    return request.app.requests_client


HTTPClient = Annotated[AsyncClient, Depends(get_httpx_client)]
