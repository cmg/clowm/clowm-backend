from typing import Annotated
from uuid import UUID

from fastapi import Depends, HTTPException, Path, status
from opentelemetry import trace

from clowm.crud import CRUDResource
from clowm.models import Resource, ResourceVersion

from .crud import CRUDSessionDep

tracer = trace.get_tracer_provider().get_tracer(__name__)
CrudResourceSession = Annotated[CRUDResource, Depends(CRUDSessionDep(CRUDResource))]


async def get_current_resource(
    rid: Annotated[UUID, Path(description="ID of a resource", examples=["4c072e39-2bd9-4fa3-b564-4d890e240ccd"])],
    crud_resource: CrudResourceSession,
) -> Resource:
    """
    Get the current resource from the database based on the ID in the path.

    FastAPI Dependency Injection Function.

    Parameters
    ----------
    rid : uuid. UUID
        ID of a resource. Path parameter.
    crud_resource : clowm.crud.crud_resource.CRUDResource
        Active CRUD repository for resources. Dependency Injection.

    Returns
    -------
    resource : clowm.models.Resource
        Resource associated with the ID in the path.

    Raises
    ------
    fastapi.HTTPException
        If the resource does not exist.
    """
    resource = await crud_resource.get(resource_id=rid)
    if resource:
        return resource
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Resource not found")


CurrentResource = Annotated[Resource, Depends(get_current_resource)]


async def get_current_resource_version(
    rvid: Annotated[
        UUID, Path(description="ID of a resource version", examples=["fb4cee12-1e91-49f3-905f-808845c7c1f4"])
    ],
    resource: CurrentResource,
) -> ResourceVersion:
    """
    Get the current resource version from the database based on the ID in the path.

    FastAPI Dependency Injection Function.

    Parameters
    ----------
    rvid : uuid. UUID
        ID of a resource version. Path parameter.
    resource : clowm.models.Resource
        Resource associated with the ID in the path.

    Returns
    -------
    resource_version : clowm.models.ResourceVersion
        Resource Version associated with the ID in the path.

    Raises
    ------
    fastapi.HTTPException
        If the bucket does not exist.
    """
    resource_version = next((version for version in resource.versions if version.resource_version_id == rvid), None)
    if resource_version is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Resource version not found")
    return resource_version


CurrentResourceVersion = Annotated[ResourceVersion, Depends(get_current_resource_version)]
