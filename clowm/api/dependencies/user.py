from typing import Annotated
from uuid import UUID

from fastapi import Depends, HTTPException, Path, status
from opentelemetry import trace

from clowm.crud import CRUDApiToken, CRUDUser
from clowm.models import ApiToken, User

from .crud import CRUDSessionDep

tracer = trace.get_tracer_provider().get_tracer(__name__)
CrudApiTokenSession = Annotated[CRUDApiToken, Depends(CRUDSessionDep(CRUDApiToken))]
CrudUserSession = Annotated[CRUDUser, Depends(CRUDSessionDep(CRUDUser))]


async def get_path_user(
    crud_user: CrudUserSession,
    uid: Annotated[
        UUID,
        Path(description="UID of a user", examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"]),
    ],
) -> User:
    """
    Get the user from the database based on the path parameter `uid`.

    Parameters
    ----------
    uid : uuid.UUID
        Id of a user in the url. Path parameter.
    crud_user : clowm.crud.crud_user.CRUDUser
        Active CRUD repository for users. Dependency Injection.

    Returns
    -------
    user : clowm.models.User
        User associated with the given UID.

    Raises
    ------
    fastapi.HTTPException
        If the user does not exist.
    """
    trace.get_current_span().set_attribute("uid", str(uid))
    user = await crud_user.get(uid)
    if user:
        return user
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")


PathUser = Annotated[User, Depends(get_path_user)]


async def get_path_token(
    crud_api_token: CrudApiTokenSession,
    tid: Annotated[UUID, Path(description="ID of an API token", examples=["b4c861a7-7f52-4332-a001-78e0500dabbc"])],
) -> ApiToken:
    """
    Get the api token from the database based on the path parameter 'tid'.

    Parameters
    ----------
    crud_api_token : clowm.crud.crud_api_token.CRUDApiToken
        Active CRUD repository for api tokens. Dependency Injection.
    tid : uuid.UUID
        ID of an Api token

    Returns
    -------
    api_token : clowm.models.ApiToken
        Api token associated with the given token id.


    Raises
    ------
    fastapi.HTTPException
        If the api token does not exist.
    """
    trace.get_current_span().set_attribute("token_id", str(tid))
    token = await crud_api_token.get(tid)
    if token is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Token not found")
    return token


PathToken = Annotated[ApiToken, Depends(get_path_token)]
