from typing import Annotated

from fastapi import Depends, HTTPException, Path, status
from opentelemetry import trace

from clowm.crud import CRUDBucket, CRUDBucketPermission
from clowm.models import Bucket, BucketPermission

from .crud import CRUDSessionDep
from .user import PathUser

tracer = trace.get_tracer_provider().get_tracer(__name__)

CrudBucketSession = Annotated[CRUDBucket, Depends(CRUDSessionDep(CRUDBucket))]


async def get_current_bucket(
    bucket_name: Annotated[
        str, Path(..., description="Name of a bucket", examples=["test-bucket"], max_length=63, min_length=3)
    ],
    crud_bucket: CrudBucketSession,
) -> Bucket:
    """
    Get the Bucket from the database based on the name in the path.

    Parameters
    ----------
    bucket_name : str
        Name of a bucket. URL Path Parameter.
    crud_bucket : clowm.crud.crud_bucket.CRUDBucket
        Active CRUD repository for buckets. Dependency Injection.

    Returns
    -------
    bucket : clowm.models.Bucket
        Bucket with the given name.

    Raises
    ------
    fastapi.HTTPException
        If the bucket does not exist.
    """
    bucket = await crud_bucket.get(bucket_name)
    if bucket is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Bucket not found")
    return bucket


CurrentBucket = Annotated[Bucket, Depends(get_current_bucket)]


async def get_current_bucket_permission(
    bucket: CurrentBucket,
    user: PathUser,
    crud: Annotated[CRUDBucketPermission, Depends(CRUDSessionDep(CRUDBucketPermission))],
) -> BucketPermission:
    """
    Get the Bucket Permission from the database based on the path.

    Parameters
    ----------
    bucket : clowm.models.Bucket
        Bucket with the given name.
    user : clowm.models.User
        User with the given uid
    crud : clowm.crud.CRUDBucketPermission
        Active CRUD repository for buckets. Dependency Injection.

    Returns
    -------
    bucket_permission : clowm.models.BucketPermission
        Bucket Permission for the combination of bucket and user if it exists.

    Raises
    ------
    fastapi.HTTPException
        If the bucket permission does not exist.
    """
    bucket_permission = await crud.get(bucket.name, user.uid)

    if bucket_permission is None:
        raise HTTPException(
            status.HTTP_404_NOT_FOUND,
            detail=f"Permission for combination of bucket={bucket.name} and user={user.uid} doesn't exists",
        )
    return bucket_permission


CurrentBucketPermission = Annotated[BucketPermission, Depends(get_current_bucket_permission)]
