import re
from collections.abc import Iterator
from typing import Annotated
from uuid import UUID

from fastapi import Depends, Query
from pydantic import AnyHttpUrl, BaseModel, Field

from clowm.db.types import OrderType

# Regex to extract the link and relation from a HTTP Link header
link_header_regex = re.compile(r"<(https?://[^>\s]+)>;\s?rel=\"(\w+)\"")


class PaginationParametersModel(BaseModel):
    id_after: UUID | None = None
    per_page: int = Field(default=20, le=100, ge=5)
    sort: OrderType = "desc"


class LinkRel(BaseModel):
    link: AnyHttpUrl
    rel: str

    def __str__(self) -> str:
        return f'<{self.link}>; rel="{self.rel}"'


class HTTPLinkHeader:
    # The key for the HTTP link header
    header_key = "link"

    def __init__(self) -> None:
        """
        Initialize the Link Header.
        """
        self.__items: list[LinkRel] = []

    def add(self, link: AnyHttpUrl, rel: str) -> "HTTPLinkHeader":
        """
        Add another link to the header.

        Parameters
        ----------
        link : pydantic.AnyHttpUrl
            The actual HTTP link
        rel : str
            The value for the relation parameter

        Returns
        -------
        self : HTTPLinkHeader
            The same object with the added link header for chaining
        """
        self.__items.append(LinkRel(link=link, rel=rel))
        return self

    def render_header_value(self) -> str:
        """
        Render the value for the Link header.

        Returns
        -------
        value : str
            The value for the http link header
        """
        return ", ".join([str(item) for item in self.__items])

    def __iter__(self) -> Iterator[LinkRel]:  # pragma: no cover
        """
        Iterate over the save links.

        Returns
        -------
        iter : Iterator[LinkRel]

        """
        return iter(self.__items)

    def __len__(self) -> int:
        return len(self.__items)

    def get(self, rel: str, default: LinkRel | None = None) -> LinkRel | None:
        """
        Get a link relation by the name of the relation.

        Parameters
        ----------
        rel : str
            The name of the relation to search for
        default : LinkRel | None, default None
            The returned default value if the relation is not found

        Returns
        -------
        link_rel : LinkRel
            The LinkRel if the relation exists, otherwise the provided default value

        Raises
        ------
        KeyError
            If the key doesn't exists.
        """
        try:
            return self[rel]
        except KeyError:
            return default

    def __getitem__(self, item: int | str) -> LinkRel:
        if type(item) is int:
            return self.__items[item]
        if type(item) is str:
            for link_rel in self.__items:
                if link_rel.rel == item:
                    return link_rel
            raise KeyError(f"link with relation {item} doesn't exist")
        raise ValueError(f"item must be of type int or str, not {type(item)}")

    @staticmethod
    def parse(header_str: str) -> "HTTPLinkHeader":
        """
        Parse a HTTP link header and extract the links.

        Parameters
        ----------
        header_str : str
            Value of the link header

        Returns
        -------
        links : HTTPLinkHeader
            List of found links with their relation
        """
        header = HTTPLinkHeader()
        for match in link_header_regex.finditer(header_str):
            header.add(rel=match.groups()[1], link=AnyHttpUrl(match.groups()[0]))
        return header

    def __str__(self) -> str:
        return self.render_header_value()

    def __repr__(self) -> str:
        return self.render_header_value()


def pagination_parameters(
    id_after: Annotated[
        UUID | None, Query(description="Id of the item to start the query from. DO NOT SET MANUALLY.")
    ] = None,
    per_page: Annotated[int, Query(ge=5, le=100, description="Number of items to list per page")] = 20,
    sort: Annotated[OrderType, Query(description="Sort order of items with creation time")] = "desc",
) -> PaginationParametersModel:
    """
    Group the pagination parameters from the query into an object.

    Parameters
    ----------
    id_after : uuid.UUID | None, default None
        Id of the item to start the query from. Query Parameter.
    per_page : int, default 20
        Number of items per page. Query Parameter.
    sort : "asc" | "desc", default "desc"
        Sorting order. Query Parameter.

    Returns
    -------
    pagination_parameters : clowm.api.dependencies.pagination.PaginationParametersModel
        Model containing the pagination parameters from the query.
    """
    return PaginationParametersModel(id_after=id_after, per_page=per_page, sort=sort)


PaginationParameters = Annotated[PaginationParametersModel, Depends(pagination_parameters)]
