from typing import Annotated
from uuid import UUID

from fastapi import Depends, HTTPException, Path, status

from clowm.api.dependencies import CRUDSessionDep
from clowm.crud import CRUDNews
from clowm.models import News

CrudNewsSession = Annotated[CRUDNews, Depends(CRUDSessionDep(CRUDNews))]


async def get_current_news(
    nid: Annotated[UUID, Path(description="ID of a news event", examples=["f3e2acf0-e942-44b2-8f85-52b7deb660ec"])],
    crud_news: CrudNewsSession,
) -> News:
    """
    Get the current resource from the database based on the ID in the path.

    FastAPI Dependency Injection Function.

    Parameters
    ----------
    nid : uuid. UUID
        ID of news event. Path parameter.
    crud_news : clowm.crud.crud_news.CRUDNews
        Active CRUD repository for news. Dependency Injection.

    Returns
    -------
    resource : clowm.models.Resource
        News event associated with the ID in the path.

    Raises
    ------
    fastapi.HTTPException
        If the news event does not exist.
    """
    news = await crud_news.get(news_id=nid)
    if news:
        return news
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="News not found")


CurrentNews = Annotated[News, Depends(get_current_news)]
