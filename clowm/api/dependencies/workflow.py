import secrets
from typing import Annotated
from uuid import UUID

from fastapi import Depends, HTTPException, Path, status
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from opentelemetry import trace

from clowm.core.config import settings
from clowm.crud import CRUDWorkflow, CRUDWorkflowExecution, CRUDWorkflowMode, CRUDWorkflowVersion
from clowm.models import Workflow, WorkflowExecution, WorkflowMode, WorkflowVersion

from .crud import CRUDSessionDep

tracer = trace.get_tracer_provider().get_tracer(__name__)
CrudWorkflowSession = Annotated[CRUDWorkflow, Depends(CRUDSessionDep(CRUDWorkflow))]
CrudWorkflowVersionSession = Annotated[CRUDWorkflowVersion, Depends(CRUDSessionDep(CRUDWorkflowVersion))]
CrudWorkflowExecutionSession = Annotated[CRUDWorkflowExecution, Depends(CRUDSessionDep(CRUDWorkflowExecution))]

security = HTTPBasic()
secret_bytes = settings.cluster.tower_secret.get_secret_value().encode("utf-8")

__all__ = [
    "CurrentWorkflow",
    "CurrentWorkflowExecution",
    "CurrentWorkflowVersion",
    "TowerWorkflowExecution",
    "CurrentWorkflowMode",
]


async def get_current_workflow(
    wid: Annotated[UUID, Path(description="ID of a workflow", examples=["0cc78936-381b-4bdd-999d-736c40591078"])],
    crud_workflow: CrudWorkflowSession,
) -> Workflow:
    """
    Get the workflow from the database with the ID given in the path.

    FastAPI Dependency Injection Function.

    Parameters
    ----------
    wid: uuid.UUID
        ID of workflow. Path parameter.
    crud_workflow : clowm.crud.CRUDWorkflow
        Active CRUD repository for workflows. Dependency Injection.

    Returns
    -------
    workflow : clowm.models.Workflow
        User associated with ID in the path.

    Raises
    ------
    fastapi.HTTPException
        If the workflow does not exist.
    """
    workflow = await crud_workflow.get(workflow_id=wid)
    if workflow:
        return workflow
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Workflow with ID {wid} not found")


CurrentWorkflow = Annotated[Workflow, Depends(get_current_workflow)]


async def get_current_workflow_execution(
    eid: Annotated[
        UUID, Path(description="ID of a workflow execution.", examples=["0cc78936-381b-4bdd-999d-736c40591078"])
    ],
    crud_workflow_execution: CrudWorkflowExecutionSession,
) -> WorkflowExecution:
    """
    Get the workflow execution from the database with the ID given in the path.

    FastAPI Dependency Injection Function.

    Parameters
    ----------
    eid: uuid.UUID
        ID of workflow execution. Path parameter.
    crud_workflow_execution : clowm.crud.CRUDWorkflowExecution
        Active CRUD repository for workflow executions. Dependency Injection.

    Returns
    -------
    workflow : clowm.models.Workflow
        User associated with ID in the path.

    Raises
    ------
    fastapi.HTTPException
        If the workflow execution does not exist.
    """
    execution = await crud_workflow_execution.get(execution_id=eid)
    if execution is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Workflow Execution with id {eid} not found")
    return execution


CurrentWorkflowExecution = Annotated[WorkflowExecution, Depends(get_current_workflow_execution)]


async def get_current_workflow_version(
    workflow: CurrentWorkflow,
    git_commit_hash: Annotated[
        str,
        Path(
            description="Git commit git_commit_hash of specific version.",
            pattern=r"^([0-9a-f]{40}|latest)$",
            max_length=40,
            examples=["ba8bcd9294c2c96aedefa1763a84a18077c50c0f"],
        ),
    ],
) -> WorkflowVersion:
    """
    Get the workflow version from the database with the ID given in the path.

    FastAPI Dependency Injection Function.

    Parameters
    ----------
    workflow : clowm.models.Workflow
        Current workflow with ID given in Path. Dependency Injection.
    git_commit_hash : str
        ID of a workflow version. Path Parameter.

    Returns
    -------
    version : clowm.models.WorkflowVersion
        Workflow version with the given ID.

    Raises
    ------
    fastapi.HTTPException
        If the workflow version does not exist.
    """
    workflow_version = next(
        (version for version in workflow.versions if version.git_commit_hash == git_commit_hash), None
    )
    if workflow_version is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Combination of Workflow '{workflow.workflow_id}' and Workflow Version '{git_commit_hash}' not found",  # noqa:E501
        )
    return workflow_version


CurrentWorkflowVersion = Annotated[WorkflowVersion, Depends(get_current_workflow_version)]


async def get_current_workflow_mode(
    mode_id: Annotated[
        UUID,
        Path(description="ID of a workflow mode", examples=["8d47e878-f25f-41aa-b4a0-95d426b46f45"]),
    ],
    crud_mode: Annotated[CRUDWorkflowMode, Depends(CRUDSessionDep(CRUDWorkflowMode))],
) -> WorkflowMode:
    """
    Get the workflow mode from the database with the ID given in the path.

    FastAPI Dependency Injection Function.

    Parameters
    ----------
    mode_id : uuid.UUID
        ID of workflow execution. Path parameter.
    crud_mode: clowm.crud.CRUDWorkflowMode
        Active CRUD repository for workflow modes. Dependency Injection.

    Returns
    -------
    mode : clowm.models.WorkflowMode
        Workflow mode with the given ID.

    Raises
    ------
    fastapi.HTTPException
        If the workflow mode does not exist.
    """
    mode = await crud_mode.get(mode_id=mode_id)
    if mode is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=f"Workflow mode with ID '{mode_id}' not found"
        )
    return mode


CurrentWorkflowMode = Annotated[WorkflowMode, Depends(get_current_workflow_mode)]


async def get_current_workflow_execution_by_id_fragment(
    eid: Annotated[
        str, Path(description="Start of ID of a workflow execution.", examples=["0cc78936381b4bdd"], max_length=16)
    ],
    crud_workflow_execution: CrudWorkflowExecutionSession,
) -> WorkflowExecution:
    """
    Get the workflow from the database with the start of ID given in the path.

    FastAPI Dependency Injection Function.

    Parameters
    ----------
    eid: str
        Start of ID of workflow execution. Path parameter.
    crud_workflow_execution : clowm.crud.crud_workflow_execution.CRUDWorkflowExecution
        Active CRUD repository for workflow executions. Dependency Injection.

    Returns
    -------
    workflow_execution : clowm.models.WorkflowExecution
        Workflow execution associated with start of ID in the path.

    Raises
    ------
    fastapi.HTTPException
        If the workflow execution does not exist.
    """
    execution = await crud_workflow_execution.get_by_id_fragment(execution_id_start=eid)
    if execution is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=f"Workflow Execution with start of id '{eid}' not found"
        )
    return execution


TowerWorkflowExecution = Annotated[WorkflowExecution, Depends(get_current_workflow_execution_by_id_fragment)]


@tracer.start_as_current_span("validate_http_credentials")
def validate_tower_secret(credentials: Annotated[HTTPBasicCredentials, Depends(security)]) -> None:
    """
    Validate the secret that nextflow send with the metrics.

    Parameters
    ----------
    credentials : fastapi.security.HTTPBasicCredentials
        Tuple of irrelevant username and tower secret.

    Raises
    ------
    fastapi.HTTPException
        If the secret is not correct.
    """
    current_password_bytes = credentials.password.encode("utf-8")
    is_correct_password = secrets.compare_digest(current_password_bytes, secret_bytes)
    if not is_correct_password:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect Nextflow Tower secret",
            headers={"WWW-Authenticate": "Basic"},
        )
