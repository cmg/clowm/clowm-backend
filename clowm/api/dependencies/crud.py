from collections.abc import AsyncGenerator, AsyncIterator
from contextlib import asynccontextmanager
from typing import Annotated

from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession, async_sessionmaker, create_async_engine

from clowm.core.config import settings
from clowm.crud.crud_base import CRUDBase


@asynccontextmanager
async def get_db_context_manager() -> AsyncIterator[AsyncSession]:
    """
    Get an open database connection within a single transaction. Will commit afterward or rollback on any exception.

    Yields
    -------
    db : AsyncIterator[AsyncSession]
        The database connection as an async context manager.
    """
    async_engine = create_async_engine(str(settings.db.dsn_async), echo=settings.db.verbose, pool_recycle=3600)
    async_session_maker = async_sessionmaker(async_engine, expire_on_commit=False)
    async with async_session_maker.begin() as transaction_session:
        try:
            yield transaction_session
            await transaction_session.commit()
        except Exception:
            await transaction_session.rollback()
            raise
    await async_engine.dispose()


async def get_db() -> AsyncGenerator[AsyncSession]:
    """
    Get a Session with the database.

    FastAPI Dependency Injection Function.

    Yields
    -------
    db : AsyncGenerator[AsyncSession, None]
        Async session object with the database
    """
    async with get_db_context_manager() as db:
        yield db


_DBSession = Annotated[AsyncSession, Depends(get_db)]


class CRUDSessionDep:
    """
    Class to parameterize the dependency injection of CRUD repositories.
    """

    def __init__(self, crud_type: type[CRUDBase]) -> None:
        """
        Parameters
        ----------
        crud_type : clowm.crud.crud_base.CRUDBase
            Type of CRUDRepository this Dependency Injection class should generate.
        """
        self._crud = crud_type

    def __call__(self, db: _DBSession) -> CRUDBase:
        """
        Get specified CRUD repository with injected database connection

        Parameters
        ----------
        db : AsyncGenerator[AsyncSession, None]
            Async session object with the database. Dependency Injection.

        Returns
        -------
        crud : clowm.crud.crud_base.CRUDBase
            Subclass of CRUDBase that was specified in init call
        """
        return self._crud(db=db)
