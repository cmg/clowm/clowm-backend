import re
from functools import cached_property

from fastapi import status
from httpx import USE_CLIENT_DEFAULT, AsyncClient, Auth
from opentelemetry import trace
from pydantic import AnyHttpUrl, BaseModel, computed_field

from clowm.utils.httpx_auth import BearerAuth

r = re.compile(r"(\w+)=\"([\w:/\-.]+)\"")
tracer = trace.get_tracer_provider().get_tracer(__name__)

__all__ = ["ContainerImage"]


class AuthenticateHeader(BaseModel):
    realm: AnyHttpUrl
    service: str

    @staticmethod
    def parse_string(header: str) -> "AuthenticateHeader":
        key_val_pairs = {}
        for match in r.finditer(header):
            key_val_pairs[match.groups()[0]] = match.groups()[1]
        return AuthenticateHeader(**key_val_pairs)


class ContainerImage(BaseModel):
    image: str
    tag: str

    @cached_property
    def docker_image(self) -> bool:
        splitted = self.image.split("/")
        return not (len(splitted) > 1 and "." in splitted[0])

    @computed_field  # type: ignore[misc]
    @property
    def repo(self) -> str:
        if self.docker_image:
            if "/" not in self.image:
                return f"library/{self.image}"
            return self.image
        return "/".join(self.image.split("/")[1:])

    @computed_field  # type: ignore[misc]
    @property
    def registry(self) -> AnyHttpUrl:
        host = "registry-1.docker.io"
        if not self.docker_image:
            host = self.image.split("/")[0]
        return AnyHttpUrl.build(scheme="https", host=host, path="v2/")

    async def check_existence(self, client: AsyncClient) -> bool:
        """
        Check in the registry if the provided images exists.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client with an open connection.

        Returns
        -------
        exist: bool
            Flag if the container image exist in the registry

        Notes
        -----
        Specification of the process
        https://distribution.github.io/distribution/spec/auth/token
        """
        with tracer.start_as_current_span(
            "check_container_image_existence", attributes={"image": self.model_dump_json()}
        ) as span:
            try:
                index_response = await client.get(str(self.registry))
                auth: Auth | None = None
                if index_response.status_code == status.HTTP_200_OK:
                    pass
                elif index_response.status_code == status.HTTP_401_UNAUTHORIZED:
                    authenticate_header = index_response.headers.get("www-authenticate")
                    if authenticate_header is None:  # pragma: no cover
                        raise ValueError("WWW-Authenticate header is empty")
                    authenticate_info = AuthenticateHeader.parse_string(authenticate_header)
                    token_response = await client.get(
                        str(authenticate_info.realm),
                        params={"scope": f"repository:{self.repo}:pull", "service": authenticate_info.service},
                    )
                    if token_response.status_code != status.HTTP_200_OK:
                        return False
                    auth = BearerAuth(token=token_response.json()["token"])
                else:
                    return False
                existence_response = await client.head(
                    f"{self.registry}{self.repo}/manifests/{self.tag}",
                    auth=USE_CLIENT_DEFAULT if auth is None else auth,
                    headers={
                        "Accept": "application/vnd.oci.image.manifest.v1+json,application/vnd.docker.distribution.manifest.v1+prettyjws,application/json,application/vnd.docker.distribution.manifest.v2+json,application/vnd.docker.distribution.manifest.list.v2+json,application/vnd.oci.image.index.v1+json"
                    },
                )
                return existence_response.status_code == status.HTTP_200_OK
            except Exception as e:  # pragma: no cover
                span.record_exception(e)
                return False

    @staticmethod
    def from_string(image: str) -> "ContainerImage":
        """
        Create a ContainerImage object from a string.

        Parameters
        ----------
        image : str
            String of the image in the form of <repo>:<tag>

        Returns
        -------
        container_image: ContainerImage
            Object representing the container image

        Notes
        -----
        The format of the string is the same as docker uses, e.g. `docker pull ubuntu:22.04`
        The tag CANNOT be omitted.

        Examples
        --------
        ContainerImage.from_string("ubuntu:22.04")
        containerImage.from_string("gcr.io/distroless/static-debian12:nonroot")
        """
        splitted = image.split(":")
        return ContainerImage(tag=splitted[-1], image=":".join(splitted[:-1]))
