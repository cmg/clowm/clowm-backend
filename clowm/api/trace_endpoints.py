from typing import Annotated

from fastapi import APIRouter, BackgroundTasks, Depends, Query
from opentelemetry import trace

from clowm.core.config import settings
from clowm.crud import CRUDWorkflowExecution
from clowm.models.workflow_execution import WorkflowExecution
from clowm.otlp import start_as_current_span_async
from clowm.schemas.tower import BeginWorkflow, CompleteWorkflow, CreateWorkflowIn, CreateWorkflowOut, ReportProgress

from .background.metrics import save_cpu_time
from .dependencies import CRUDSessionDep, TowerWorkflowExecution
from .dependencies.workflow import get_current_workflow_execution_by_id_fragment, validate_tower_secret

router = APIRouter(
    prefix="/trace", tags=["Trace"], dependencies=[Depends(validate_tower_secret)], include_in_schema=False
)

CrudWorkflowExecutionSession = Annotated[CRUDWorkflowExecution, Depends(CRUDSessionDep(CRUDWorkflowExecution))]
tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.post("/create", summary="Create workflow run event")
@start_as_current_span_async("api_create_workflow_execution", tracer=tracer)
async def create(
    workspace_id: Annotated[
        str,
        Query(
            alias="workspaceId",
            description="ID of the workflow execution",
            examples=["0cc78936381b4bdd"],
            max_length=16,
        ),
    ],
    event_in: CreateWorkflowIn,
    crud_workflow_execution: CrudWorkflowExecutionSession,
) -> CreateWorkflowOut:
    """
    Event send by nextflow for creating the workflow run.
    \f
    Parameters
    ----------
    workspace_id : str
        ID of the nextflow workspace. Correspond to the start of the workflow execution ID. URL Query Parameter.
    event_in : clowm.schemas.tower.CreateWorkflowIn
        Additional data send by nextflow. HTTP Body.
    crud_workflow_execution : clowm.crud.crud_workflow_execution.CRUDWorkflowExecution
        Active CRUD repository for workflow executions. Dependency Injection.

    Returns
    -------
    workflow_out : clowm.schemas.tower.CreateWorkflowOut
        Response with execution ID which can be parsed by nextflow.
    """
    trace.get_current_span().set_attribute("workflow_execution_id_start", workspace_id)
    # Check that Workflow execution exists
    execution = await get_current_workflow_execution_by_id_fragment(
        eid=workspace_id, crud_workflow_execution=crud_workflow_execution
    )
    await crud_workflow_execution.update_status(
        execution_id=execution.execution_id, status=WorkflowExecution.WorkflowExecutionStatus.SCHEDULED
    )
    return CreateWorkflowOut(workflowId=workspace_id)


@router.put("/{eid}/begin", summary="Begin workflow run event")
@start_as_current_span_async("api_begin_workflow_execution", tracer=tracer)
async def begin(
    event_in: BeginWorkflow,
    workflow_execution: TowerWorkflowExecution,
    crud_workflow_execution: CrudWorkflowExecutionSession,
) -> dict[str, str]:
    """
    Event send by nextflow for beginning the workflow run.
    \f
    Parameters
    ----------
    event_in : clowm.schemas.tower.BeginWorkflow
        Additional data send by nextflow. HTTP Body.
    workflow_execution : clowm.models.Workflow
        Workflow execution associated with start of ID in the path.
    crud_workflow_execution : clowm.crud.crud_workflow_execution.CRUDWorkflowExecution
        Active CRUD repository for workflow executions. Dependency Injection.

    Returns
    -------
    response : dict[str, str]
        Response with watch url
    """
    trace.get_current_span().set_attribute("workflow_execution_id", str(workflow_execution.execution_id))
    await crud_workflow_execution.update_status(
        execution_id=workflow_execution.execution_id, status=WorkflowExecution.WorkflowExecutionStatus.RUNNING
    )
    return {"watchUrl": str(settings.ui_uri) + settings.api_prefix.strip("/")}


@router.put("/{eid}/complete", summary="Complete workflow run event")
@start_as_current_span_async("api_complete_workflow_execution", tracer=tracer)
async def complete(
    event_in: CompleteWorkflow,
    workflow_execution: TowerWorkflowExecution,
    crud_workflow_execution: CrudWorkflowExecutionSession,
) -> dict:
    """
    Event send by nextflow for completing the workflow run.
    \f
    Parameters
    ----------
    event_in : clowm.schemas.tower.CompleteWorkflow
        Additional data send by nextflow. HTTP Body.
    workflow_execution : clowm.models.Workflow
        Workflow execution associated with start of ID in the path.
    crud_workflow_execution : clowm.crud.crud_workflow_execution.CRUDWorkflowExecution
        Active CRUD repository for workflow executions. Dependency Injection.

    Returns
    -------
    response : Dict
        Empty Dict
    """
    current_span = trace.get_current_span()
    status = WorkflowExecution.WorkflowExecutionStatus.ERROR
    current_span.set_attributes(
        {"workflow_execution_id": str(workflow_execution.execution_id), "success": event_in.workflow.success}
    )
    if event_in.workflow.errorReport is not None:
        current_span.set_attribute("error_report", event_in.workflow.errorReport)
    if event_in.workflow.success:
        status = WorkflowExecution.WorkflowExecutionStatus.SUCCESS
    elif event_in.workflow.errorReport is not None and event_in.workflow.errorReport.startswith("SIG"):
        status = WorkflowExecution.WorkflowExecutionStatus.CANCELED
    await crud_workflow_execution.update_status(
        execution_id=workflow_execution.execution_id, status=status, end_timestamp=event_in.instant
    )
    return {}


@router.put("/{eid}/progress", summary="Progress workflow event")
@start_as_current_span_async("api_progress_workflow_execution", tracer=tracer)
async def progress(
    progress_event: ReportProgress,
    workflow_execution: TowerWorkflowExecution,
    background_tasks: BackgroundTasks,
) -> dict:
    """
    Event send by nextflow for progress in the workflow run.
    \f
    Parameters
    ----------
    progress_event : clowm.schemas.tower.ReportProgress
        Additional data send by nextflow. HTTP Body.
    workflow_execution : clowm.models.Workflow
        Workflow execution associated with start of ID in the path.
    background_tasks : fastapi.BackgroundTasks


    Returns
    -------
    response : Dict
        Empty Dict
    """
    trace.get_current_span().set_attribute("workflow_execution_id", str(workflow_execution.execution_id))
    if progress_event.tasks:
        background_tasks.add_task(
            save_cpu_time, execution_id=workflow_execution.execution_id, tasks=progress_event.tasks
        )
    return {}


@router.put("/{eid}/heartbeat", summary="Heartbeat workflow event")
@start_as_current_span_async("api_heartbeat_workflow_execution", tracer=tracer)
async def heartbeat(
    progress_event: ReportProgress,
    workflow_execution: TowerWorkflowExecution,
) -> dict:
    """
    Event send by nextflow for heartbeat signal by workflow run.
    \f
    Parameters
    ----------
    progress_event : clowm.schemas.tower.ReportProgress
        Additional data send by nextflow. HTTP Body.
    workflow_execution : clowm.models.Workflow
        Workflow execution associated with start of ID in the path.

    Returns
    -------
    response : Dict
        Empty Dict
    """
    trace.get_current_span().set_attribute("workflow_execution_id", str(workflow_execution.execution_id))
    return {}
