from typing import Annotated
from uuid import UUID

from fastapi import APIRouter, BackgroundTasks, Body, Depends, Query, status
from opentelemetry import trace

from clowm.api.dependencies.website import AuthorizationDependencyCookieUser as AuthorizationDependency
from clowm.api.dependencies.website import CurrentUserCookie as CurrentUser
from clowm.core.rbac import RBACOperation, RBACResource
from clowm.crud import CRUDResource, CRUDUser
from clowm.models import Resource, ResourceVersion, User
from clowm.otlp import start_as_current_span_async
from clowm.schemas.ownership_transfer import OwnershipTransferRequestIn, OwnershipTransferRequestOut
from clowm.schemas.resource import ResourceIn, ResourceOut, S3ResourceVersionInfo
from clowm.schemas.resource_version import ResourceVersionOut, UserSynchronizationRequestOut

from ...ceph.s3 import delete_s3_bucket_policy_stmt
from ...core.config import settings
from ...smtp.send_email import (
    send_otr_accepted_email,
    send_otr_created_email,
    send_otr_deleted_email,
    send_otr_rejected_email,
)
from ..background.s3 import (
    add_s3_resource_version_info,
    give_permission_to_s3_resource,
    give_permission_to_s3_resource_version,
)
from ..core import ResourceCoreDep
from ..dependencies import CRUDSessionDep, CurrentResource, S3Service
from ..dependencies.auth import AuthorizationFunction
from ..otr_utils import accept_otr, create_otr, delete_otr, get_otr, list_otrs

router = APIRouter(prefix="/resources", tags=["Resource"])
resource_authorization = AuthorizationDependency(resource=RBACResource.RESOURCE)
Authorization = Annotated[AuthorizationFunction, Depends(resource_authorization)]
CrudResourceSession = Annotated[CRUDResource, Depends(CRUDSessionDep(CRUDResource))]
tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.post("", summary="Request a new resource", status_code=status.HTTP_201_CREATED)
@start_as_current_span_async("api_request_resource", tracer=tracer)
async def create_resource(
    authorization: Authorization,
    current_user: CurrentUser,
    resource_in: Annotated[ResourceIn, Body(description="Meta-data for the resource to request")],
    resource_core: ResourceCoreDep,
) -> ResourceOut:
    """
    Request a new resources.

    Permission `resource:create` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency injection.
    resource_in : clowm.schemas.resource.ResourceIn
        Data about the new resource. HTTP Body.
    resource_core : clowm.api.core.ResourceCore
        Core functionality for resource API. Dependency Injection.

    Returns
    -------
    resource_out : clowm.schemas.resource.ResourceOut
        Newly created resource
    """
    current_span = trace.get_current_span()
    current_span.set_attribute("resource_in", resource_in.model_dump_json(indent=2))
    authorization(RBACOperation.CREATE)
    resource = await resource_core.create(resource_in, current_user)
    return ResourceOut.from_db_resource(db_resource=resource)


@router.get("", summary="List resources")
@start_as_current_span_async("api_list_resources", tracer=tracer)
async def list_resources(
    authorization: Authorization,
    current_user: CurrentUser,
    crud_resource: CrudResourceSession,
    maintainer_id: Annotated[
        UUID | None,
        Query(
            description="Filter for resource by maintainer. If current user is the same as maintainer ID, permission `resource:list` required, otherwise `resource:list_filter`.",  # noqa: E501
            examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
        ),
    ] = None,
    version_status: Annotated[
        list[ResourceVersion.ResourceVersionStatus] | None,
        Query(
            description="Which versions of the resource to include in the response. Permission `resource:list_filter` required if None or querying for non-public resources, otherwise only permission `resource:list` required.",  # noqa: E501
        ),
    ] = None,
    name_substring: Annotated[
        str | None,
        Query(max_length=32, description="Filter resources by a substring in their name.", examples=["gtdb"]),
    ] = None,
    public: Annotated[bool | None, Query(description="Filter resources to by the public flag")] = None,
) -> list[ResourceOut]:
    """
    List all resources.

    Permission `resource:list` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency injection.
    crud_resource : clowm.crud.crud_resource.CRUDResource
        Active CRUD repository for buckets. Dependency Injection.
    maintainer_id : uuid.UUID | None, default None
        Filter resources by a maintainer. Query Parameter.
    version_status : list[clowm.models.ResourceVersion.Status] | None, default None
        Filter resource version by their status. Query Parameter.
    name_substring : str | None, default None
        Filter resources by a substring in their name. Query Parameter.
    public : bool | None, default None
      Filter resources to include only public resources. Query Parameters.

    Returns
    -------
    resources : list[clowm.schemas.resource.ResourceOut]
        List of resource based on filters
    """
    current_span = trace.get_current_span()
    if maintainer_id:  # pragma: no cover
        current_span.set_attribute("maintainer_id", str(maintainer_id))
    if version_status:  # pragma: no cover
        current_span.set_attribute("version_status", [state.name for state in version_status])
    if name_substring:  # pragma: no cover
        current_span.set_attribute("name_substring", name_substring)
    if public is not None:  # pragma: no cover
        current_span.set_attribute("public", public)

    rbac_operation = RBACOperation.LIST
    if maintainer_id is not None:
        if current_user.uid != maintainer_id:
            rbac_operation = RBACOperation.LIST_ALL
    # if status is None or contains non-public resources
    elif version_status is None or sum(map(lambda r_status: not r_status.public, version_status)) > 0:
        rbac_operation = RBACOperation.LIST_ALL
    authorization(rbac_operation)
    resources = await crud_resource.list_resources(
        name_substring=name_substring, maintainer_id=maintainer_id, version_status=version_status, public=public
    )
    return [ResourceOut.from_db_resource(resource) for resource in resources]


@router.get("/sync_requests", summary="List resource sync requests")
@start_as_current_span_async("api_list_resources_sync_requests", tracer=tracer)
async def list_sync_requests(
    authorization: Authorization,
    crud_resource: CrudResourceSession,
) -> list[UserSynchronizationRequestOut]:
    """
    List all resource sync requests.

    Permission `resource:update_any` required.
    \f
    Parameters
    ----------
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    crud_resource : clowm.crud.crud_resource.CRUDResource
        Active CRUD repository for buckets. Dependency Injection.

    Returns
    -------
    sync_request : list[clowm.schemas.resource_version.UserSynchronizationRequestOut]
        List of open synchronization requests.
    """
    authorization(RBACOperation.UPDATE_ANY)
    resources = await crud_resource.list_resources(
        version_status=[ResourceVersion.ResourceVersionStatus.SYNC_REQUESTED]
    )
    return [
        UserSynchronizationRequestOut.from_db_resource_version(version)
        for resource in resources
        for version in resource.versions
    ]


@router.get("/ownership_transfer_request", summary="List resource OTRs")
@start_as_current_span_async("api_list_resource_otr", tracer=tracer)
async def list_resource_otrs(
    authorization: Authorization,
    crud_resource: CrudResourceSession,
    current_user: CurrentUser,
    current_owner_id: Annotated[
        UUID | None,
        Query(
            description="UID of user who is the current owner.",
            examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
        ),
    ] = None,
    new_owner_id: Annotated[
        UUID | None,
        Query(
            description="UID of user who will be the new owner.",
            examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
        ),
    ] = None,
) -> list[OwnershipTransferRequestOut]:
    """
    Get the ownership transfer requests for resources.

    Permission `resource:list` required if `current_owner_id` or `new_owner_id` is the current users id,
    otherwise `resource:list_all`
    \f
    Parameters
    ----------
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    crud_resource : clowm.crud.crud_resource.CRUDResource
        Active CRUD repository for buckets. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    current_owner_id : uuid.UUID
        ID of user who is the current owner. Query Parameter.
    new_owner_id : uuid.UUID
        ID of user who will be the new owner. Query Parameter.

    Returns
    -------
    otrs : list[clowm.schemas.ownership_transfer.OwnershipTransferRequestOut]
        List of resource OTRs
    """
    return await list_otrs(
        current_user=current_user,
        authorization=authorization,
        current_owner_id=current_owner_id,
        new_owner_id=new_owner_id,
        crud=crud_resource,
    )


@router.get("/{rid}", summary="Get a resource")
@start_as_current_span_async("api_get_resource", tracer=tracer)
async def get_resource(
    authorization: Authorization,
    resource: CurrentResource,
    current_user: CurrentUser,
    version_status: Annotated[
        list[ResourceVersion.ResourceVersionStatus] | None,
        Query(
            description="Which versions of the resource to include in the response. Permission `resource:read_any` required if None or querying for non-public resources, otherwise only permission `resource:read` required.",  # noqa: E501
        ),
    ] = None,
) -> ResourceOut:
    """
    Get a specific resource.

    Permission `resource:read` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency injection.
    resource : clowm.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    version_status : list[clowm.models.ResourceVersion.Status] | None, default None
        Filter resource version by their status. Query Parameter.

    Returns
    -------
    resource : clowm.schemas.resource.ResourceOut
        The requested resource
    """
    current_span = trace.get_current_span()
    if version_status:  # pragma: no cover
        current_span.set_attribute("version_status", [state.name for state in version_status])
    current_span.set_attribute("resource_id", str(resource.resource_id))
    rbac_operation = (
        RBACOperation.READ_ANY
        if resource.maintainer_id != current_user.uid
        # if status is None or contains non-public resources
        and (version_status is None or sum(map(lambda r_status: not r_status.public, version_status)) > 0)
        else RBACOperation.READ
    )
    authorization(rbac_operation)
    return ResourceOut.from_db_resource(
        resource,
        versions=[
            version for version in resource.versions if version_status is None or version.status in version_status
        ],
    )


@router.get("/{rid}/ownership_transfer_request", summary="Get a resource OTR")
@start_as_current_span_async("api_get_resource_otr", tracer=tracer)
async def get_resource_otr(
    authorization: Authorization, resource: CurrentResource, current_user: CurrentUser
) -> OwnershipTransferRequestOut:
    """
    Get a specific resource ownership transfer request.

    Permission `resource:read` required if the current user is the current or new owner of the resource,
    otherwise `resource:read_any` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency injection.
    resource : clowm.models.Resource
        Resource associated with the ID in the path. Dependency Injection.

    Returns
    -------
    otr : clowm.schemas.ownership_transfer.OwnershipTransferRequestOut
        Resource OTR if it exists
    """
    trace.get_current_span().set_attribute("resource_id", str(resource.resource_id))
    return get_otr(target=resource, current_user=current_user, authorization=authorization)


@router.post("/{rid}/ownership_transfer_request", status_code=status.HTTP_201_CREATED, summary="Create a resource OTR")
@start_as_current_span_async("api_create_resource_otr", tracer=tracer)
async def create_resource_otr(
    resource: CurrentResource,
    current_user: CurrentUser,
    authorization: Authorization,
    otr_in: Annotated[OwnershipTransferRequestIn, Body(description="Meta-data for the resource OTR to create")],
    background_tasks: BackgroundTasks,
    crud_resource: CrudResourceSession,
) -> OwnershipTransferRequestOut:
    """
    Create a ownership transfer request for a specific resource.

    Permission `resource:update` required if the current user is the current owner of the resource,
    otherwise `resource:update_any` required.
    \f
    Parameters
    ----------
    resource : clowm.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    otr_in : clowm.schemas.ownership_transfer.OwnershipTransferRequestIn
        Infos for the OTR. HTTP Body.
    crud_resource : clowm.crud.crud_resource.CRUDResource
        Active CRUD repository for buckets. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.

    Returns
    -------
    otr : clowm.schemas.ownership_transfer.OwnershipTransferRequestOut
        Bucket OTR if it exists
    """
    trace.get_current_span().set_attribute("resource_id", str(resource.resource_id))
    otr = await create_otr(
        target=resource, current_user=current_user, authorization=authorization, otr_info=otr_in, crud=crud_resource
    )
    background_tasks.add_task(send_otr_created_email, otr=otr)
    return otr


@router.patch("/{rid}/ownership_transfer_request", status_code=status.HTTP_200_OK, summary="Accept a resource OTR")
@start_as_current_span_async("api_accept_resource_otr", tracer=tracer)
async def accept_resource_otr(
    resource: CurrentResource,
    current_user: CurrentUser,
    authorization: Authorization,
    crud_resource: CrudResourceSession,
    background_tasks: BackgroundTasks,
    s3: S3Service,
) -> ResourceOut:
    """
    Accept an ownership transfer request for a specific resource.

    Permission `resource:update` required if the current user is the new owner of the resource,
    otherwise `resource:update_any` required.
    \f
    Parameters
    ----------
    resource : clowm.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    crud_resource : clowm.crud.crud_resource.CRUDResource
        Active CRUD repository for buckets. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    s3 : boto3_type_annotations.s3.ServiceResource
        S3 Service to perform operations on buckets in Ceph. Dependency Injection.

    Returns
    -------
    resource : clowm.schemas.resource.ResourceOut
        The resource with the new maintainer id
    """
    trace.get_current_span().set_attribute("resource_id", str(resource.resource_id))
    old_owner_uid = resource.maintainer_id
    new_owner_uid = resource.transfer_new_owner_uid
    await accept_otr(target=resource, current_user=current_user, authorization=authorization, crud=crud_resource)
    await crud_resource.db.refresh(resource, ["maintainer_id"])
    await delete_s3_bucket_policy_stmt(
        bucket_name=settings.s3.data_bucket,
        s3=s3,
        sid=[str(resource.resource_id)] + [str(version.resource_version_id) for version in resource.versions],
    )
    resource_out = ResourceOut.from_db_resource(db_resource=resource)
    background_tasks.add_task(give_permission_to_s3_resource, resource=resource_out)
    new_maintainer: User = (
        current_user  # type: ignore[assignment]
        if current_user.uid == resource_out.maintainer_id
        else await CRUDUser(db=crud_resource.db).get(resource_out.maintainer_id)  # type: ignore[arg-type]
    )
    for version in resource.versions:
        if version.status == ResourceVersion.ResourceVersionStatus.RESOURCE_REQUESTED:
            background_tasks.add_task(
                give_permission_to_s3_resource_version,
                resource_version=ResourceVersionOut.from_db_resource_version(version),
                maintainer_id=new_maintainer.uid,
            )
        background_tasks.add_task(
            add_s3_resource_version_info,
            s3_resource_version_info=S3ResourceVersionInfo.from_models(
                resource=resource,
                resource_version=version,
                maintainer=new_maintainer,
            ),
        )
    background_tasks.add_task(
        send_otr_accepted_email,
        target_name=resource.name,
        target_type=Resource,
        new_owner_uid=new_owner_uid,  # type: ignore[arg-type]
        old_owner_uid=old_owner_uid,
    )
    return resource_out


@router.delete(
    "/{rid}/ownership_transfer_request", status_code=status.HTTP_204_NO_CONTENT, summary="Delete a resource OTR"
)
@start_as_current_span_async("api_delete_resource_otr", tracer=tracer)
async def delete_resource_otr(
    authorization: Authorization,
    resource: CurrentResource,
    current_user: CurrentUser,
    crud_resource: CrudResourceSession,
    background_tasks: BackgroundTasks,
) -> None:
    """
    Delete/Reject a resource ownership transfer request.

    Permission `resource:update` required if the current user is the current or new owner of the resource,
    otherwise `resource:update_any` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency injection.
    resource : clowm.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    crud_resource : clowm.crud.crud_resource.CRUDResource
        Active CRUD repository for buckets. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    """
    trace.get_current_span().set_attribute("resource_id", str(resource.resource_id))
    old_owner_uid = resource.maintainer_id
    new_owner_uid = resource.transfer_new_owner_uid
    await delete_otr(target=resource, current_user=current_user, authorization=authorization, crud=crud_resource)
    if current_user.uid == new_owner_uid:
        background_tasks.add_task(
            send_otr_rejected_email,
            target_name=resource.name,
            target_type=Resource,
            new_owner_uid=new_owner_uid,
            owner_uid=old_owner_uid,
        )
    else:
        background_tasks.add_task(
            send_otr_deleted_email,
            target_name=resource.name,
            target_type=Resource,
            new_owner_uid=new_owner_uid,  # type: ignore[arg-type]
        )


@router.delete("/{rid}", summary="Delete a resource", status_code=status.HTTP_204_NO_CONTENT)
@start_as_current_span_async("api_delete_resource", tracer=tracer)
async def delete_resource(
    authorization: Authorization,
    resource: CurrentResource,
    resource_core: ResourceCoreDep,
) -> None:
    """
    Delete a resources.

    Permission `resource:delete` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    resource : clowm.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    resource_core : clowm.api.core.ResourceCore
        Core functionality for resource API. Dependency Injection.
    """
    trace.get_current_span().set_attribute("resource_id", str(resource.resource_id))
    authorization(RBACOperation.DELETE)
    await resource_core.delete(resource)
