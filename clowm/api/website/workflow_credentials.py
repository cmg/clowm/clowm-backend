from typing import Annotated

from fastapi import APIRouter, BackgroundTasks, Body, Depends, HTTPException, status
from opentelemetry import trace

from clowm.ceph.s3 import delete_s3_obj
from clowm.core.config import settings
from clowm.core.rbac import RBACOperation, RBACResource
from clowm.crud import CRUDWorkflow, CRUDWorkflowVersion
from clowm.git_repository import GitHubRepository, build_repository
from clowm.otlp import start_as_current_span_async
from clowm.schemas.workflow import WorkflowCredentialsIn, WorkflowCredentialsOut
from clowm.scm import SCM, SCMProvider

from ..background.s3 import upload_scm_file
from ..dependencies import (
    CRUDSessionDep,
    CurrentWorkflow,
    HTTPClient,
    S3Service,
)
from ..dependencies.auth import AuthorizationFunction
from ..dependencies.website import AuthorizationDependencyCookieUser as AuthorizationDependency
from ..dependencies.website import CurrentUserCookie as CurrentUser

router = APIRouter(prefix="/workflows/{wid}/credentials", tags=["Workflow Credentials"])
workflow_authorization = AuthorizationDependency(resource=RBACResource.WORKFLOW)
Authorization = Annotated[AuthorizationFunction, Depends(workflow_authorization)]
CrudWorkflowSession = Annotated[CRUDWorkflow, Depends(CRUDSessionDep(CRUDWorkflow))]

tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.get("", status_code=status.HTTP_200_OK, summary="Get the credentials of a workflow")
@start_as_current_span_async("api_workflow_credentials_get", tracer=tracer)
async def get_workflow_credentials(
    workflow: CurrentWorkflow, current_user: CurrentUser, authorization: Authorization
) -> WorkflowCredentialsOut:
    """
    Get the credentials for the repository of a workflow. Only the developer of a workflow can do this.

    Permission `workflow:update` required.
    \f
    Parameters
    ----------
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    current_user : clowm.models.User
        Current user.  Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.

    Returns
    -------
    workflow : clowm.schemas.workflow.WorkflowOut
        Workflow with existing ID
    """
    trace.get_current_span().set_attribute("workflow_id", str(workflow.workflow_id))
    authorization(RBACOperation.UPDATE)
    if current_user.uid != workflow.developer_id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Only the developer can retrieve the repository credentials"
        )
    return WorkflowCredentialsOut(token=workflow.credentials_token)


@router.put("", status_code=status.HTTP_200_OK, summary="Update the credentials of a workflow")
@start_as_current_span_async("api_workflow_credentials_update", tracer=tracer)
async def update_workflow_credentials(
    credentials: Annotated[
        WorkflowCredentialsIn, Body(description="Updated credentials for the workflow git repository")
    ],
    workflow: CurrentWorkflow,
    crud_workflow: CrudWorkflowSession,
    current_user: CurrentUser,
    authorization: Authorization,
    background_tasks: BackgroundTasks,
    client: HTTPClient,
) -> None:
    """
    Update the credentials for the repository of a workflow.

    Permission `workflow:update` required.
    \f
    Parameters
    ----------
    credentials : clowm.schemas.workflow.WorkflowCredentialsIn
        Updated credentials for the workflow git repository
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    crud_workflow : clowm.crud.crud_workflow.CRUDWorkflow
        Active CRUD repository for workflows. Dependency Injection.
    current_user : clowm.models.User
        Current user.  Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    client : httpx.AsyncClient
        Http client with an open connection. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.

    Returns
    -------
    workflow : clowm.schemas.workflow.WorkflowOut
        Workflow with existing ID
    """
    trace.get_current_span().set_attribute("workflow_id", str(workflow.workflow_id))
    authorization(RBACOperation.UPDATE)
    if current_user.uid != workflow.developer_id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Only the developer can update the repository credentials"
        )
    latest_version = await CRUDWorkflowVersion(db=crud_workflow.db).get_latest(workflow.workflow_id, published=False)
    assert latest_version is not None
    # Build a git repository object based on the repository url
    repo = build_repository(
        workflow.repository_url,
        latest_version.git_commit_hash,
        token=credentials.token,
    )

    if not await repo.check_repo_availability(client=client):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="can't read from the repo with updated credentials"
        )
    scm_provider = SCMProvider.from_repo(repo=repo, name=SCMProvider.generate_name(workflow.workflow_id))
    background_tasks.add_task(
        upload_scm_file,
        scm=SCM(providers=[scm_provider]),
        scm_file_id=workflow.workflow_id,
    )
    await crud_workflow.update_credentials(workflow.workflow_id, token=credentials.token)


@router.delete("", status_code=status.HTTP_204_NO_CONTENT, summary="Delete the credentials of a workflow")
@start_as_current_span_async("api_workflow_credentials_delete", tracer=tracer)
async def delete_workflow_credentials(
    background_tasks: BackgroundTasks,
    workflow: CurrentWorkflow,
    crud_workflow: CrudWorkflowSession,
    current_user: CurrentUser,
    authorization: Authorization,
    s3: S3Service,
) -> None:
    """
    Delete the credentials for the repository of a workflow.

    Permission `workflow:delete` required if the developer of the workflow is the same as the current user,
    other `workflow:delete_any`.
    \f
    Parameters
    ----------
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    crud_workflow : clowm.crud.crud_workflow.CRUDWorkflow
        Active CRUD repository for workflows. Dependency Injection.
    current_user : clowm.models.User
        Current user.  Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    s3 : boto3_type_annotations.s3.ServiceResource
        S3 Service to perform operations on buckets in Ceph. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.

    Returns
    -------
    workflow : clowm.schemas.workflow.WorkflowOut
        Workflow with existing ID
    """
    trace.get_current_span().set_attribute("workflow_id", str(workflow.workflow_id))
    authorization(RBACOperation.DELETE if workflow.developer_id == current_user.uid else RBACOperation.DELETE_ANY)
    repo = build_repository(workflow.repository_url, workflow.versions[0].git_commit_hash)
    if isinstance(repo, GitHubRepository):
        await delete_s3_obj(settings.s3.data_bucket, key=settings.s3.scm_key(workflow.workflow_id), s3=s3)
    else:
        scm_provider = SCMProvider.from_repo(repo=repo, name=SCMProvider.generate_name(workflow.workflow_id))
        background_tasks.add_task(
            upload_scm_file,
            scm=SCM(providers=[scm_provider]),
            scm_file_id=workflow.workflow_id,
        )
    await crud_workflow.update_credentials(workflow_id=workflow.workflow_id, token=None)
