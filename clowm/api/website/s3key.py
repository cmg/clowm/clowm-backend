from typing import Annotated

from fastapi import APIRouter, Depends, Path, status
from opentelemetry import trace

from clowm.api.dependencies.website import AuthorizationDependencyCookieUser as AuthorizationDependency
from clowm.api.dependencies.website import CurrentUserCookie as CurrentUser
from clowm.ceph.rgw import get_s3_keys
from clowm.core.rbac import RBACOperation, RBACResource
from clowm.otlp import start_as_current_span_async
from clowm.schemas.s3key import S3Key

from ..core.s3key import S3KeyCoreDep
from ..dependencies import PathUser, RGWService
from ..dependencies.auth import AuthorizationFunction

router = APIRouter(prefix="/users/{uid}/keys", tags=["S3Key"])
s3key_authorization = AuthorizationDependency(resource=RBACResource.S3_KEY)
Authorization = Annotated[AuthorizationFunction, Depends(s3key_authorization)]
tracer = trace.get_tracer_provider().get_tracer(__name__)

AccessID = Annotated[
    str,
    Path(
        ...,
        description="ID of the S3 access key",
        examples=["CRJ6B037V2ZT4U3W17VC"],
    ),
]


@router.get(
    "",
    summary="Get the S3 Access keys from a user",
)
@start_as_current_span_async("api_list_s3_keys", tracer=tracer)
async def get_user_keys(
    rgw: RGWService,
    current_user: CurrentUser,
    authorization: Authorization,
    user: PathUser,
) -> list[S3Key]:
    """
    Get all the S3 Access keys for a specific user.

    Permission `s3_key:list` required if the current user is the target, otherwise `s3_key:list_all` required.
    \f
    Parameters
    ----------
    rgw : rgwadmin.RGWAdmin
        RGW admin interface to manage Ceph's object store. Dependency Injection.
    user : clowm.models.User
        User with given uid. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.

    Returns
    -------
    keys : List(clowm.schemas.user.S3Key)
        All S3 keys from the user.
    """
    trace.get_current_span().set_attribute("uid", str(user.uid))
    authorization(RBACOperation.LIST if current_user.uid == user.uid else RBACOperation.LIST_ALL)
    return get_s3_keys(rgw, user.uid)


@router.post(
    "",
    summary="Create a Access key for a user",
    status_code=status.HTTP_201_CREATED,
)
@start_as_current_span_async("api_create_s3_key", tracer=tracer)
async def create_user_key(
    s3_key_core: S3KeyCoreDep,
    current_user: CurrentUser,
    authorization: Authorization,
    user: PathUser,
) -> S3Key:
    """
    Create a S3 Access key for a specific user.

    Permission `s3_key:create` required if the current user is the target, otherwise `s3_key:create_any` required.
    \f
    Parameters
    ----------
    s3_key_core : clowm.api.core.S3KeyCore
        Core functionality for s3 keys API. Dependency Injection.
    user : clowm.models.User
        User with given uid. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.

    Returns
    -------
    key : clowm.schemas.s3key.S3Key
        Newly created S3 key.
    """
    trace.get_current_span().set_attribute("uid", str(user.uid))
    authorization(RBACOperation.CREATE if current_user.uid == user.uid else RBACOperation.CREATE_ANY)
    return s3_key_core.create(user.uid)


@router.get(
    "/{access_id}",
    summary="Get a specific S3 Access key from a user",
)
@start_as_current_span_async("api_get_s3_key", tracer=tracer)
async def get_user_key(
    s3_key_core: S3KeyCoreDep,
    current_user: CurrentUser,
    authorization: Authorization,
    access_id: AccessID,
    user: PathUser,
) -> S3Key:
    """
    Get a specific S3 Access Key for a specific user.

    Permission `s3_key:read` required if the current user is the target, otherwise `s3_key:read_any` required.
    \f
    Parameters
    ----------
    access_id : str
        ID of the requested S3 key. URL Path Parameter.
    s3_key_core : clowm.api.core.S3KeyCore
        Core functionality for s3 keys API. Dependency Injection.
    user : clowm.models.User
        User with given uid. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.

    Returns
    -------
    key : clowm.schemas.s3key.S3Key
        Requested S3 key.
    """
    trace.get_current_span().set_attribute("uid", str(user.uid))
    authorization(RBACOperation.READ if current_user.uid == user.uid else RBACOperation.READ_ANY)
    return s3_key_core.get(access_id, user.uid)


@router.delete(
    "/{access_id}",
    summary="Delete a specific S3 Access key from a user",
    status_code=status.HTTP_204_NO_CONTENT,
)
@start_as_current_span_async("api_delete_s3_key", tracer=tracer)
async def delete_user_key(
    access_id: AccessID,
    s3_key_core: S3KeyCoreDep,
    current_user: CurrentUser,
    authorization: Authorization,
    user: PathUser,
) -> None:
    """
    Delete a specific S3 Access key for a specific user.

    Permission `s3_key:delete` required if the current user is the target, otherwise `s3_key:delete_any` required.
    \f
    Parameters
    ----------
    access_id : str
        ID of the S3 key to delete. URL Path Parameter.
    s3_key_core : clowm.api.core.S3KeyCore
        Core functionality for s3 keys API. Dependency Injection.
    user : clowm.models.User
        User with given uid. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    """
    trace.get_current_span().set_attribute("uid", str(user.uid))
    authorization(RBACOperation.DELETE if current_user.uid == user.uid else RBACOperation.DELETE_ANY)
    s3_key_core.delete(access_key=access_id, uid=user.uid)
