import time
import urllib
import urllib.parse
from typing import Annotated, TypeGuard
from uuid import UUID

from fastapi import APIRouter, BackgroundTasks, Depends, HTTPException, Path, Query, Request, status
from fastapi.responses import RedirectResponse
from opentelemetry import trace
from pydantic import AnyHttpUrl
from rgwadmin import RGWAdmin

from clowm.api.background.user import update_last_login
from clowm.api.dependencies.website.auth import CurrentUserCookie, cookie_jwt_scheme, get_cookie_jwt_user
from clowm.core.auth import create_access_token
from clowm.core.config import settings
from clowm.core.oidc import OIDCProvider, UserInfo
from clowm.crud import CRUDUser
from clowm.otlp import start_as_current_span_async

from ..dependencies import CRUDSessionDep, OIDCClientDep, RGWService
from ..exception_handlers import AccountConnectionError, LoginError
from ..utils import create_rgw_user

__all__ = ["router"]

router = APIRouter(prefix="/auth", tags=["Auth"], include_in_schema=True)
CrudUserSession = Annotated[CRUDUser, Depends(CRUDSessionDep(CRUDUser))]
tracer = trace.get_tracer_provider().get_tracer(__name__)

NEXT_PATH_KEY = "NEXT"
INVITATION_UID_KEY = "INVITATION_UID"
ACCOUNT_CONNECTION_KEY = "ACCOUNT_CONNECTION_UID"


def filter_empty_and_none(x: str | None) -> TypeGuard[str]:
    """
    Filter function to remove empty strings and None types.

    Parameters
    ----------
    x : str | None
        List element.

    Returns
    -------
    check : bool
        Flag if the input is not None and not empty
    """
    return x is not None and len(x) > 0


def oidc_redirect_uri(provider: OIDCProvider, login: bool) -> AnyHttpUrl:
    return AnyHttpUrl.build(
        scheme=settings.ui_uri.scheme,
        host=settings.ui_uri.host,  # type: ignore[arg-type]
        path="/".join(
            filter(
                filter_empty_and_none,
                [
                    settings.ui_uri.path,
                    settings.api_prefix.strip("/"),
                    router.prefix.strip("/"),
                    "login" if login else "account",
                    "callback",
                    provider,
                ],
            )
        ).strip(  # type: ignore[list-item]
            "/"
        ),
        port=settings.ui_uri.port,
    )


@router.get(
    "/login/{provider}",
    status_code=status.HTTP_302_FOUND,
    summary="Kickstart the login flow",
)
@start_as_current_span_async("api_login_route", tracer=tracer)
async def kickstart_login(
    request: Request,
    oidc_client: OIDCClientDep,
    crud_user: CrudUserSession,
    provider: Annotated[OIDCProvider, Path(description="The OIDC provider to use for login")],
    invitation_token: Annotated[
        str | None,
        Query(
            min_length=43,
            max_length=43,
            description="Unique token to validate an invitation",
        ),
    ] = None,
    next_: Annotated[
        str | None,
        Query(
            alias="next",
            max_length=128,
            description="Will be appended to redirect response in the callback route as URL query parameter `next`",
        ),
    ] = None,
) -> RedirectResponse:
    """
    Redirect route to OIDC provider to kickstart the login process.
    \f
    Parameters
    ----------
    provider : clowm.core.oidc.OIDCClient.OIDCProvider
        The oidc provider from which the user should be authorized. Path Parameter.
    oidc_client: clowm.core.oidc.OIDCClient
        The wrapper around the oidc client. Dependency Injection.
    request : fastapi.requests.Request
        Raw request object.
    crud_user : clowm.crud.crud_user.CRUDUser
        Active CRUD repository for users. Dependency Injection.
    next_ : str | None
        Query parameter that gets stored in the session cookie. Query Parameter.
        Will be appended to RedirectResponse in the callback route as URL query parameter 'next'
    invitation_token : str | None
        Token from the invitation email to connect the created account with an identity provider. Query Parameter.

    Returns
    -------
    response : fastapi.responses.RedirectResponse
        Redirect response to right OAuth2 endpoint
    """
    # Clear session to prevent an overflow
    request.session.clear()
    current_span = trace.get_current_span()
    current_span.set_attribute("provider", provider.name)
    if next_:
        current_span.set_attribute("next", next_)
        request.session[NEXT_PATH_KEY] = next_
    if invitation_token is not None:
        user = await crud_user.get_by_invitation_token(invitation_token)
        if user is None:
            raise LoginError(error_source="invalid invitation link")
        current_span.set_attribute("invitation_uid", str(user.uid))
        if (
            round(time.time()) - (0 if user.invitation_token_created_at is None else user.invitation_token_created_at)
            > 86400  # 24 hours valid
        ):
            raise LoginError(error_source="expired invitation link")
        request.session[INVITATION_UID_KEY] = str(user.uid)
    return await oidc_client.authorize_redirect(
        request, redirect_uri=oidc_redirect_uri(provider=provider, login=True), provider=provider, error_cls=LoginError
    )


@router.get(
    "/login/callback/{provider}",
    response_class=RedirectResponse,
    status_code=status.HTTP_302_FOUND,
    summary="LifeScience Login Callback",
    responses={
        status.HTTP_302_FOUND: {
            "headers": {
                "Set-Cookie": {
                    "description": "JWT for accessing the API",
                    "schema": {
                        "type": "string",
                        "example": f"{cookie_jwt_scheme.model.name}=fake-jwt-cookie; Domain=localhost; Max-Age=691200; Path=/; SameSite=strict; Secure",
                    },
                }
            }
        }
    },
)
@start_as_current_span_async("api_oidc_callback", tracer=tracer)
async def login_callback(
    response: RedirectResponse,
    crud_user: CrudUserSession,
    provider: OIDCProvider,
    rgw: RGWService,
    background_tasks: BackgroundTasks,
    request: Request,
    oidc_client: OIDCClientDep,
) -> str:
    """
    Callback for the login process with an OIDC Provider.\n
    Visit the route login route to start the login process.

    If the user is already known to the system, then a JWT token will be created and sent via the 'set-cookie' header.
    The key for this Cookie is 'bearer'.\n
    If the user is new, he will be created him and then a JWT token is issued.\n
    This JWT has to be sent to all authorized endpoints via the HTTPBearer scheme.
    \f
    Parameters
    ----------
    response : fastapi.responses.RedirectResponse
        Response which will hold the JWT cookie.
    request : fastapi.Request
        Raw request object to access the session cookie.
    provider : clowm.core.oidc.OIDCClient.OIDCProvider
        The oidc provider from which the user was authorized. Path Parameter.
    crud_user : clowm.crud.crud_user.CRUDUser
        Active CRUD repository for users. Dependency Injection.
    rgw : rgwadmin.RGWAdmin
        RGW admin interface to manage Ceph's object store. Dependency Injection.
    oidc_client: clowm.core.oidc.OIDCClient
        The wrapper around the oidc client. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.

    Returns
    -------
    path : str
        Redirect path after successful login.
    """
    redirect_path = str(settings.ui_uri) + "dashboard"
    current_span = trace.get_current_span()
    current_span.set_attribute("provider", provider.name)
    next_path: str | None = request.session.get(NEXT_PATH_KEY, None)  # get return path from session cookie
    url_params: dict[str, str | bool | int] = {}
    if next_path is not None:
        current_span.set_attribute("next", next_path)
        url_params["next"] = next_path
    try:
        user_info = await oidc_client.verify_fetch_userinfo(request=request, provider=provider, error_cls=LoginError)
        current_span.set_attribute("sub", user_info.sub)

        invitation_uid: str | None = request.session.get(INVITATION_UID_KEY, None)
        # if the user was invited
        if invitation_uid is not None:
            uid = await handle_invited_user(
                invitation_uid,
                user_info,
                crud_user=crud_user,
                background_tasks=background_tasks,
                rgw=rgw,
                url_params=url_params,
            )
        # normal login flow
        else:
            uid = await handle_normal_user(
                user_info, crud_user=crud_user, background_tasks=background_tasks, rgw=rgw, url_params=url_params
            )
        jwt = create_access_token(str(uid))
        response.set_cookie(
            key=cookie_jwt_scheme.model.name,
            value=jwt,
            samesite="lax",
            max_age=settings.jwt_token_expire_minutes * 60,
            secure=True,
            domain=settings.ui_uri.host,
            httponly=False,
        )
        background_tasks.add_task(update_last_login, uid=uid)
    except LoginError as e:
        # Add already set url_params to the AuthException
        e.add_url_params(**url_params)
        raise e
    except Exception as e:  # pragma: no cover
        current_span.record_exception(e)
        raise LoginError(error_source="server", url_params=url_params) from e
    finally:
        request.session.clear()
    # append url params to redirect path
    if len(url_params) > 0:
        redirect_path = (
            redirect_path
            + "?"
            + "&".join(f"{key}={urllib.parse.quote_plus(str(val))}" for key, val in url_params.items())
        )
    return redirect_path


@router.get(
    "/account/connect/{provider}",
    status_code=status.HTTP_302_FOUND,
    summary="Kickstart the account connection flow",
)
@start_as_current_span_async("api_login_connect_route", tracer=tracer)
async def kickstart_account_connection(
    request: Request,
    oidc_client: OIDCClientDep,
    current_user: CurrentUserCookie,
    provider: Annotated[OIDCProvider, Path(description="The OIDC provider to use for login")],
) -> RedirectResponse:
    """
    Redirect route to OIDC provider to connect the CloWM account with the AAI account.
    \f
    Parameters
    ----------
    provider : clowm.core.oidc.OIDCClient.OIDCProvider
        The oidc provider from which the user should be authorized. Query Parameter.
    oidc_client: clowm.core.oidc.OIDCClient
        The wrapper around the oidc client. Dependency Injection.
    request : fastapi.requests.Request
        Raw request object.
    current_user : clowm.crud.crud_user.CRUDUser
        Active CRUD repository for users. Dependency Injection.

    Returns
    -------
    response : fastapi.responses.RedirectResponse
        Redirect response to right OAuth2 endpoint
    """
    # Clear session to prevent an overflow
    request.session.clear()
    current_span = trace.get_current_span()
    current_span.set_attribute("provider", provider.name)
    if current_user.provider_id(provider) is not None:
        raise AccountConnectionError(error_source=f"CloWM account already connected to '{provider}' account")
    request.session[ACCOUNT_CONNECTION_KEY] = str(current_user.uid)
    return await oidc_client.authorize_redirect(
        request,
        redirect_uri=oidc_redirect_uri(provider=provider, login=False),
        provider=provider,
        error_cls=AccountConnectionError,
    )


@router.get(
    "/account/callback/{provider}",
    response_class=RedirectResponse,
    status_code=status.HTTP_302_FOUND,
    summary="LifeScience Login Callback",
)
async def account_connection_callback(
    crud_user: CrudUserSession,
    provider: OIDCProvider,
    request: Request,
    oidc_client: OIDCClientDep,
    current_user_jwt: Annotated[
        str | None,
        Depends(
            type(cookie_jwt_scheme)(
                name=cookie_jwt_scheme.model.name,
                scheme_name=cookie_jwt_scheme.scheme_name,
                description=cookie_jwt_scheme.model.description,
                auto_error=False,
            )
        ),
    ],
) -> str:
    """
    Callback for the account connection process with an OIDC provider.

    \f
    Parameters
    ----------
    request : fastapi.Request
        Raw request object to access the session cookie.
    provider : clowm.core.oidc.OIDCClient.OIDCProvider
        The oidc provider from which the user was authorized. Path Parameter.
    crud_user : clowm.crud.crud_user.CRUDUser
        Active CRUD repository for users. Dependency Injection.
    oidc_client: clowm.core.oidc.OIDCClient
        The wrapper around the oidc client. Dependency Injection.
    current_user_jwt : str | None
        JWT in Cookie for authenticating a the user.

    Returns
    -------
    path : str
        Redirect path after (un)successful account connection.
    """
    current_span = trace.get_current_span()
    current_span.set_attribute("provider", provider.name)
    # if the user is not authenticated, redirect him to the login page
    if current_user_jwt is None:
        raise LoginError(error_source="not authenticated")
    # try to validate JWT and get the associated user and redirect to login page if it fails
    try:
        current_user = await get_cookie_jwt_user(crud_user, current_user_jwt)
    except HTTPException as exc:
        raise LoginError(error_source="error authenticating user") from exc

    account_uid: str | None = request.session.get(ACCOUNT_CONNECTION_KEY, None)
    redirect_path = str(settings.ui_uri) + "profile"

    # Check that key is set in Session cookie
    if account_uid is None:
        raise AccountConnectionError(error_source=f"Account {account_uid} not found")
    # check that uid in session cookie and current user matches
    if UUID(account_uid) != current_user.uid:
        raise AccountConnectionError(error_source="mismatch between current user and saved user in state")

    # Check if the CloWM account is already connected to another AAI account from the same provider
    if current_user.provider_id(provider) is not None:
        raise AccountConnectionError(error_source=f"CloWM account already connected to '{provider}' account")
    url_params: dict[str, str | bool | int] = {}
    try:
        # Fetch user info from OIDC provider
        user_info = await oidc_client.verify_fetch_userinfo(
            request=request, provider=provider, error_cls=AccountConnectionError
        )
        current_span.set_attribute("sub", user_info.sub)

        # Check if the AAI account is already connected to another CloWM account
        if await crud_user.get_by_sub(sub=user_info.sub, provider=user_info.provider) is not None:
            raise AccountConnectionError(
                error_source=f"{user_info.provider} account already connected to other CloWM account"
            )

        # connect the CloWM account with the AAI account in the database
        await crud_user.update_invited_user(uid=current_user.uid, user=user_info)
        url_params["link_success"] = True
    except AccountConnectionError:
        # Prevent Auth Errors to be caught by following except block
        raise
    except Exception as e:  # pragma: no cover
        current_span.record_exception(e)
        raise AccountConnectionError(error_source="server") from e
    finally:
        request.session.clear()
    # append url params to redirect path
    if len(url_params) > 0:
        redirect_path = (
            redirect_path
            + "?"
            + "&".join(f"{key}={urllib.parse.quote_plus(str(val))}" for key, val in url_params.items())
        )
    return redirect_path


@start_as_current_span_async("logout", tracer=tracer)
@router.get(
    "/logout",
    response_class=RedirectResponse,
    status_code=status.HTTP_302_FOUND,
    summary="Logout",
    responses={
        status.HTTP_302_FOUND: {
            "headers": {
                "Set-Cookie": {
                    "description": "JWT for accessing the API",
                    "schema": {
                        "type": "string",
                        "example": f"{cookie_jwt_scheme.model.name}=; Domain=localhost; Max-Age=0; Path=/; SameSite=strict; Secure",
                    },
                }
            }
        }
    },
)
async def logout(response: RedirectResponse) -> str:
    """
    Logout the user from the system by deleting the bearer cookie.
    \f
    Parameters
    ----------
    response : fastapi.responses.RedirectResponse
        Response which will delete the JWT cookie.

    Returns
    -------
    path : str
        Redirect path after successful logout.
    """
    response.delete_cookie(key=cookie_jwt_scheme.model.name, secure=True, domain=settings.ui_uri.host, httponly=False)
    return str(settings.ui_uri)


async def handle_invited_user(
    invitation_uid: str,
    user_info: UserInfo,
    *,
    url_params: dict[str, str | bool | int],
    crud_user: CRUDUser,
    background_tasks: BackgroundTasks,
    rgw: RGWAdmin,
) -> UUID:
    """

    Parameters
    ----------
    crud_user : clowm.crud.crud_user.CRUDUser
        Active CRUD repository for users.
    invitation_uid : str

    user_info : clowm.core.oidc.UserInfo
        Information about user from AAI provider.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks.
    rgw : rgwadmin.RGWAdmin
        RGW admin interface to manage Ceph's object store.
    url_params : dict[str, str | bool | int]
        Dict where url params for redirect URL can be appended.

    Returns
    -------
    uid : uuid.UUID
        UID of the user in the system that should be logged in.

    Raises
    ------
    LoginError
        If the login should be abort with a error message
    """
    current_span = trace.get_current_span()
    current_span.set_attribute("invitation_uid", invitation_uid)
    user = await crud_user.get(uid=UUID(invitation_uid))
    # if the UID doesn't exist, abort login
    if user is None:
        raise LoginError(error_source="unknown user")
    # if the AAI account is already connected to another clowm account, abort login
    if await crud_user.get_by_sub(sub=user_info.sub, provider=user_info.provider) is not None:
        raise LoginError(error_source=f"{user_info.provider} account already connected to other CloWM account")
    # update the invited user and initialize him
    await crud_user.update_invited_user(uid=user.uid, user=user_info)
    create_rgw_user(user=user, background_tasks=background_tasks, rgw=rgw)
    url_params["first_login"] = True
    return user.uid


async def handle_normal_user(
    user_info: UserInfo,
    *,
    url_params: dict[str, str | bool | int],
    crud_user: CRUDUser,
    background_tasks: BackgroundTasks,
    rgw: RGWAdmin,
) -> UUID:
    """
    Handle a normal user login. Get or create a user if the user doesn't exist yet.

    Parameters
    ----------
    crud_user : clowm.crud.crud_user.CRUDUser
        Active CRUD repository for users.
    user_info : clowm.core.oidc.UserInfo
        Information about user from AAI provider.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks.
    rgw : rgwadmin.RGWAdmin
        RGW admin interface to manage Ceph's object store.
    url_params : dict[str, str | bool | int]
        Dict where url params for redirect URL can be appended.

    Returns
    -------
    uid : uuid.UUID
        UID of the user in the system that should be logged in.
    """
    user = await crud_user.get_by_sub(sub=user_info.sub, provider=user_info.provider)
    # if we want to block foreign users and the user is None or has no role, reject this login attempt
    if settings.block_foreign_users and (user is None or len(user.roles) == 0):
        raise LoginError(error_source="Access denied for new users")
    # if user does not exist in system, automatically create a new user
    if user is None:
        user = await crud_user.create(
            user_info,
        )
        create_rgw_user(user=user, background_tasks=background_tasks, rgw=rgw)
        url_params["first_login"] = True
    # if the email got updated, update it in the database
    elif user_info.email is not None and user.email != user_info.email:
        await crud_user.update_email(user.uid, user_info.email)
    return user.uid
