import time
import urllib.parse
from collections.abc import Iterable, Sequence
from typing import Annotated
from uuid import UUID

from fastapi import APIRouter, Body, Depends, Query, Response, status
from opentelemetry import trace
from pydantic import AnyHttpUrl

from clowm.api.dependencies.website import AuthorizationDependencyCookieUser as AuthorizationDependency
from clowm.api.dependencies.website import CurrentUserCookie as CurrentUser
from clowm.core.rbac import RBACOperation, RBACResource
from clowm.crud import CRUDNews
from clowm.otlp import start_as_current_span_async

from ...core.config import settings
from ...models import News
from ...schemas.news import NewsIn, NewsOut
from ..dependencies import CRUDSessionDep, CurrentNews
from ..dependencies.auth import AuthorizationFunction
from ..dependencies.pagination import HTTPLinkHeader, PaginationParameters

router = APIRouter(prefix="/news", tags=["News"])
Authorization = Annotated[AuthorizationFunction, Depends(AuthorizationDependency(resource=RBACResource.NEWS))]
CrudNewsSession = Annotated[CRUDNews, Depends(CRUDSessionDep(CRUDNews))]
tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.post("", summary="Create news", response_model=NewsOut, status_code=status.HTTP_201_CREATED)
@start_as_current_span_async("api_create_news", tracer=tracer)
async def create_news(
    authorization: Authorization,
    crud_news: CrudNewsSession,
    current_user: CurrentUser,
    news_in: Annotated[NewsIn, Body(description="Meta-data for news event to create")],
) -> News:
    """
    Create a news event.

    Permission `news:create` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    crud_news : clowm.crud.crud_news.CRUDNews
        Active CRUD repository for news. Dependency Injection.
    news_in : clowm.schemas.news.NewsIn
        Information about the news event. HTTP Body.
    current_user : clowm.models.User
        Current user who will be the creator of the news. Dependency Injection.

    Return
    -------
    news_out : clowm.schemas.news.NewsOut
        The created news event.
    """
    current_span = trace.get_current_span()
    current_span.set_attributes({"creator_id": str(current_user.uid), "news_in": news_in.model_dump_json()})
    authorization(RBACOperation.CREATE)
    news = await crud_news.create(news_in=news_in, creator_id=current_user.uid)
    current_span.set_attribute("news_id", str(news.news_id))
    return news


@router.get(
    "",
    summary="List news",
    response_model=list[NewsOut],
    responses={
        status.HTTP_200_OK: {
            "headers": {
                HTTPLinkHeader.header_key: {
                    "description": "Link for the next pagination page if there is any",
                    "schema": {
                        "type": "string",
                        "example": HTTPLinkHeader()
                        .add(
                            link=AnyHttpUrl(
                                f"{settings.ui_uri}{settings.api_prefix.strip('/')}{router.prefix}?per_page=50&sort=asc&id_after=a16c50f8-c1fb-4b3c-afe3-82f1575bc2f4"
                            ),
                            rel="next",
                        )
                        .render_header_value(),
                    },
                }
            }
        }
    },
)
@start_as_current_span_async("api_list_news", tracer=tracer)
async def list_news(
    authorization: Authorization,
    crud_news: CrudNewsSession,
    response: Response,
    pagination_parameters: PaginationParameters,
    created_after: Annotated[
        int | None,
        Query(ge=0, le=1 << 32, description="Filter for news that are created after this UNIX timestamp"),
    ] = None,
    creator_id: Annotated[UUID | None, Query(description="Filter for news created by a specific user")] = None,
) -> Iterable[News]:
    """
    List all news events with filters.\n
    This endpoint enforces keyset pagination. To iterate over all news, follow the link provided in the `Link` header.
    A missing `Link` header indicates that you iterated over all news with the current filters.

    Permission `news:list` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    crud_news : clowm.crud.crud_news.CRUDNews
        Active CRUD repository for news. Dependency Injection.
    response : fastapi.Response
        Raw response object to set response headers. Provided by FastAPI.
    pagination_parameters : clowm.api.dependencies.pagination.PaginationParametersModel
        Pagination parameters from the query. Dependency Injection.
    created_after : int | None, default None
        Filter for news that are created after this UNIX timestamp. Query Parameter.
    creator_id : uuid.UUID | None, default None
        Filter for news created by a specific user. Query Parameter.

    Returns
    -------
    news_list : list[clowm.schemas.news.NewsOut]
        List of filtered news_events
    """
    current_span = trace.get_current_span()
    query_params: list[tuple[str, str | int]] = [
        ("per_page", pagination_parameters.per_page),
        ("sort", pagination_parameters.sort),
    ]
    current_span.set_attribute("pagination_parameters", pagination_parameters.model_dump_json())
    if created_after is not None:  # pragma: no cover
        query_params.append(("created_after", created_after))
        current_span.set_attribute("created_after", created_after)
    if creator_id is not None:  # pragma: no cover
        query_params.append(("creator_id", str(creator_id)))
        current_span.set_attribute("creator_id", str(creator_id))
    authorization(RBACOperation.LIST)
    news = await crud_news.list(
        id_after=pagination_parameters.id_after,
        limit=pagination_parameters.per_page + 1,
        sort=pagination_parameters.sort,
        created_after=created_after,
        creator_id=creator_id,
    )
    if len(news) == pagination_parameters.per_page + 1:
        query_params.append(("id_after", str(news[-1].news_id)))
        response.headers.append(
            HTTPLinkHeader.header_key,
            HTTPLinkHeader()
            .add(
                link=AnyHttpUrl(
                    f"{str(settings.ui_uri).strip('/')}{settings.api_prefix}{router.prefix}?{urllib.parse.urlencode(query_params)}"
                ),
                rel="next",
            )
            .render_header_value(),
        )
    return news[: pagination_parameters.per_page]


@router.get("/latest", response_model=list[NewsOut], summary="Get latest news")
@start_as_current_span_async("api_list_latest_news", tracer=tracer)
async def list_latest_news(authorization: Authorization, crud_news: CrudNewsSession) -> Sequence[News]:
    """
    List the current news events.

    Permission `news:list` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    crud_news : clowm.crud.crud_news.CRUDNews
        Active CRUD repository for news. Dependency Injection.

    Returns
    -------
    news : list[clowm.models.News]
        List of current news

    Notes
    -----
    Current news is defined as news events that are created in the last two weeks or are marked as still important
    """
    authorization(RBACOperation.LIST)
    return await crud_news.get_latest(created_after=round(time.time()) - 1209600)


@router.get("/{nid}", response_model=NewsOut, summary="Get a specific news")
@start_as_current_span_async("api_get_news", tracer=tracer)
async def get_news(authorization: Authorization, news_event: CurrentNews) -> News:
    """
    Get a specified news event.

    Permission `news:read` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    news_event : clowm.models.News
        News event associated with the ID in the path. Dependency Injection.

    Returns
    -------
    news_event : clowm.models.News
        News event associated with the ID in the path if it exists.
    """
    trace.get_current_span().set_attribute("news_id", str(news_event.news_id))
    authorization(RBACOperation.READ)
    return news_event


@router.delete("/{nid}", summary="Delete a specific news", status_code=status.HTTP_204_NO_CONTENT)
@start_as_current_span_async("api_delete_news", tracer=tracer)
async def delete_news(
    authorization: Authorization,
    news: CurrentNews,
    crud_news: CrudNewsSession,
) -> None:
    """
    Delete a specified news event.

    Permission `news:delete` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    news : clowm.models.News
        News event associated with the ID in the path. Dependency Injection.
    crud_news : clowm.crud.crud_news.CRUDNews
        Active CRUD repository for news. Dependency Injection.
    """
    trace.get_current_span().set_attribute("news_id", str(news.news_id))
    authorization(RBACOperation.DELETE)
    await crud_news.delete(news.news_id)
