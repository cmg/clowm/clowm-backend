import time
import urllib.parse
from collections.abc import Callable
from typing import Annotated, Any
from uuid import UUID

from botocore.exceptions import ClientError
from fastapi import APIRouter, BackgroundTasks, Body, Depends, HTTPException, Query, Request, Response, status
from fastapi.responses import StreamingResponse
from opentelemetry import trace
from pydantic import AnyHttpUrl

from clowm.api.dependencies.website import AuthorizationDependencyCookieUser as AuthorizationDependency
from clowm.api.dependencies.website import CurrentUserCookie as CurrentUser
from clowm.core.config import settings
from clowm.core.rbac import RBACOperation, RBACResource
from clowm.crud import CRUDWorkflowExecution, CRUDWorkflowVersion
from clowm.models import WorkflowExecution, WorkflowVersion
from clowm.otlp import start_as_current_span_async
from clowm.schemas.workflow_execution import DevWorkflowExecutionIn, WorkflowExecutionIn, WorkflowExecutionOut

from ..background.cluster import cancel_slurm_job
from ..background.s3 import delete_from_data_bucket
from ..core import WorkflowExecutionCoreDep
from ..dependencies import (
    CRUDSessionDep,
    CurrentWorkflowExecution,
    HTTPClient,
    S3Service,
)
from ..dependencies.auth import AuthorizationFunction
from ..dependencies.pagination import HTTPLinkHeader, PaginationParameters
from ..utils import stream_response_from_url

router = APIRouter(prefix="/workflow_executions", tags=["Workflow Execution"])
Authorization = Annotated[
    AuthorizationFunction, Depends(AuthorizationDependency(resource=RBACResource.WORKFLOW_EXECUTION))
]
CrudWorkflowExecutionSession = Annotated[CRUDWorkflowExecution, Depends(CRUDSessionDep(CRUDWorkflowExecution))]

tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.post(
    "", status_code=status.HTTP_201_CREATED, summary="Start a new workflow execution", response_model_exclude_none=True
)
@start_as_current_span_async("api_start_workflow_execution", tracer=tracer)
async def start_workflow(
    workflow_execution_in: Annotated[
        WorkflowExecutionIn, Body(description="Meta-data and parameters for the workflow to start")
    ],
    crud_workflow_execution: CrudWorkflowExecutionSession,
    current_user: CurrentUser,
    authorization: Authorization,
    execution_core: WorkflowExecutionCoreDep,
) -> WorkflowExecutionOut:
    """
    Start a new workflow execution. Workflow versions wit status `DEPRECATED` or `DENIED` can't be started.

    Permission `workflow_execution:create` required if workflow versions status is `PUBLISHED`,
    otherwise `workflow_execution:create_any` required.
    \f
    Parameters
    ----------
    workflow_execution_in : clowm.schemas.workflow_executionWorkflowExecutionIn
        Meta-data and parameters for the workflow to start. HTTP Body.
    crud_workflow_execution : clowm.crud.crud_workflow_execution.CRUDWorkflowExecution
        Active CRUD repository for workflow executions. Dependency Injection.
    current_user : clowm.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    execution_core : clowm.api.core.WorkflowExecutionCore
        Core functionality for workflow execution API. Dependency Injection.

    Returns
    -------
    execution : clowm.models.WorkflowExecution
        Created workflow execution from the database
    """
    current_span = trace.get_current_span()
    current_span.set_attribute("workflow_version_id", workflow_execution_in.workflow_version_id)
    # Check if Workflow version exists
    workflow_version = await CRUDWorkflowVersion(db=crud_workflow_execution.db).get(
        workflow_execution_in.workflow_version_id, populate_workflow=True
    )
    if workflow_version is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Workflow version with git commit hash {workflow_execution_in.workflow_version_id} not found",
        )
    current_span.set_attribute("workflow_id", str(workflow_version.workflow_id))
    # Check authorization
    authorization(
        RBACOperation.CREATE
        if workflow_version.status == WorkflowVersion.WorkflowVersionStatus.PUBLISHED
        else RBACOperation.CREATE_ANY
    )
    execution = await execution_core.start_workflow(
        current_uid=current_user.uid, workflow_execution_in=workflow_execution_in, workflow_version=workflow_version
    )
    return WorkflowExecutionOut.from_db_model(execution, workflow_id=workflow_version.workflow_id)


@router.post(
    "/arbitrary",
    status_code=status.HTTP_201_CREATED,
    summary="Start a workflow execution with arbitrary git repository",
    include_in_schema=settings.dev_system,
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_start_arbitrary_workflow_execution", tracer=tracer)
async def start_arbitrary_workflow(
    workflow_execution_in: Annotated[
        DevWorkflowExecutionIn, Body(description="Meta-data and parameters for the workflow to start")
    ],
    current_user: CurrentUser,
    execution_core: WorkflowExecutionCoreDep,
    authorization: Annotated[Callable[[RBACOperation], None], Depends(AuthorizationDependency(RBACResource.WORKFLOW))],
) -> WorkflowExecutionOut:
    """
    Start a new workflow execution from an arbitrary git repository.\n
    For private Gitlab repositories, a Project Access Token with the role Reporter and scope `read_api` is needed.\n
    For private GitHub repositories, a Personal Access Token (classic) with scope `repo` is needed.

    Permission `workflow:create` required.
    \f
    Parameters
    ----------
    workflow_execution_in : clowm.schemas.workflow_executionWorkflowExecutionIn
        Meta-data and parameters for the workflow to start. HTTP Body.
    current_user : clowm.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    execution_core : clowm.api.core.WorkflowExecutionCore
        Core functionality for workflow execution API. Dependency Injection.

    Returns
    -------
    execution : clowm.models.WorkflowExecution
        Created workflow execution from the database
    """
    if not settings.dev_system:  # pragma: no cover
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Not available")
    current_span = trace.get_current_span()
    current_span.set_attributes(
        {
            "repository_url": str(workflow_execution_in.repository_url),
            "git_commit_hash": workflow_execution_in.git_commit_hash,
        }
    )
    if workflow_execution_in.token is not None:
        current_span.set_attribute("private_repository", True)
    if workflow_execution_in.mode is not None:
        current_span.set_attributes(
            {
                "workflow_entrypoint": workflow_execution_in.mode.entrypoint,
                "workflow_schema_path": workflow_execution_in.mode.schema_path,
            }
        )

    authorization(RBACOperation.CREATE)
    execution = await execution_core.start_arbitrary_workflow(
        current_uid=current_user.uid, workflow_execution_in=workflow_execution_in
    )
    return WorkflowExecutionOut.from_db_model(execution)


@router.get(
    "",
    status_code=status.HTTP_200_OK,
    summary="List all workflow executions",
    response_model_exclude_none=True,
    responses={
        status.HTTP_200_OK: {
            "headers": {
                HTTPLinkHeader.header_key: {
                    "description": "Link for the next pagination page if there is any",
                    "schema": {
                        "type": "string",
                        "example": HTTPLinkHeader()
                        .add(
                            link=AnyHttpUrl(
                                f"{settings.ui_uri}{settings.api_prefix.strip('/')}/ui{router.prefix}?per_page=50&sort=asc&id_after=a16c50f8-c1fb-4b3c-afe3-82f1575bc2f4"
                            ),
                            rel="next",
                        )
                        .render_header_value(),
                    },
                }
            }
        }
    },
)
@start_as_current_span_async("api_list_workflow_execution", tracer=tracer)
async def list_workflow_executions(
    crud_workflow_execution: CrudWorkflowExecutionSession,
    current_user: CurrentUser,
    authorization: Authorization,
    response: Response,
    pagination_parameters: PaginationParameters,
    executor_id: Annotated[
        UUID | None,
        Query(
            description="Filter for workflow executions by a user. If none, Permission `workflow_execution:read_any` required.",  # noqa: E501
            examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
        ),
    ] = None,
    execution_status: Annotated[
        list[WorkflowExecution.WorkflowExecutionStatus] | None,
        Query(description="Filter for status of workflow execution"),
    ] = None,
    workflow_version_id: Annotated[
        str | None,
        Query(
            description="Filter for workflow version",
            examples=["ba8bcd9294c2c96aedefa1763a84a18077c50c0f"],
            min_length=40,
            max_length=40,
            pattern=r"^[0-9a-f]+$",
        ),
    ] = None,
    workflow_id: Annotated[
        UUID | None,
        Query(
            description="Filter for workflow",
            examples=["0cc78936-381b-4bdd-999d-736c40591078"],
        ),
    ] = None,
    start_after: Annotated[
        int | None,
        Query(
            description="Filter for workflow executions that started after this UNIX timestamp",
            ge=1,
            le=2**32 - 1,
            examples=[1640991600],  # 01.01.2022 00:00
        ),
    ] = None,
    start_before: Annotated[
        int | None,
        Query(
            description="Filter for workflow executions that started before this UNIX timestamp",
            ge=1,
            le=2**32 - 1,
            examples=[1640991600],  # 01.01.2022 00:00
        ),
    ] = None,
) -> list[WorkflowExecutionOut]:
    """
    Get all workflow executions.\n
    This endpoint enforces keyset pagination. To iterate over all workflow executions, follow the link provided in the
    `Link` header.
    A missing `Link` header indicates that you iterated over all workflow executions with the current filters.

    Permission `workflow_execution:list` required, if `executor_id` is the same as the current user,
    otherwise `workflow_execution:list_all` required.
    \f
    Parameters
    ----------
    executor_id : str | None, default None
        Filter for workflow executions by a user. Query Parameter.
    execution_status : list[clowm.models.WorkflowExecution.WorkflowExecutionStatus] | None, default None
        Filter for status of workflow execution. Query Parameter.
    workflow_id : uuid.UUID | None, default None
        Filter for workflows. Query Parameter.
    workflow_version_id : str | None, default None
        Filter for workflow version. Query Parameter.
    start_after : int | None, default None
        Filter for workflow executions that started after this UNIX timestamp. Query Parameter.
    start_before : int | None, default None
        Filter for workflow executions that started before this UNIX timestamp. Query Parameter.
    crud_workflow_execution : clowm.crud.crud_workflow_execution.CRUDWorkflowExecution
        Active CRUD repository for workflow executions. Dependency Injection.
    current_user : clowm.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    response : fastapi.Response
        Raw response object to set response headers. Provided by FastAPI.
    pagination_parameters : clowm.api.dependencies.pagination.PaginationParametersModel
        Pagination parameters from the query. Dependency Injection.

    Returns
    -------
    executions : list[clowm.models.WorkflowExecution]
        List of filtered workflow executions.
    """
    current_span = trace.get_current_span()
    query_params: list[tuple[str, str | int]] = [
        ("per_page", pagination_parameters.per_page),
        ("sort", pagination_parameters.sort),
    ]
    current_span.set_attribute("pagination_parameters", pagination_parameters.model_dump_json())
    if executor_id is not None:  # pragma: no cover
        query_params.append(("executor_id", str(executor_id)))
        current_span.set_attribute("user_id", str(executor_id))
    if execution_status is not None and len(execution_status) > 0:  # pragma: no cover
        for state in execution_status:
            query_params.append(("execution_status", str(state)))
        current_span.set_attribute("execution_status", [stat.name for stat in execution_status])
    if workflow_version_id is not None:  # pragma: no cover
        query_params.append(("workflow_version_id", workflow_version_id))
        current_span.set_attribute("workflow_version_id", workflow_version_id)
    if workflow_id is not None:  # pragma: no cover
        query_params.append(("workflow_id", str(workflow_id)))
        current_span.set_attribute("workflow_id", str(workflow_id))
    if start_after is not None:  # pragma: no cover
        query_params.append(("start_after", start_after))
        current_span.set_attribute("start_after", start_after)
    if start_before is not None:  # pragma: no cover
        query_params.append(("start_before", start_before))
        current_span.set_attribute("start_before", start_before)

    authorization(
        RBACOperation.LIST if executor_id is not None and executor_id == current_user.uid else RBACOperation.LIST_ALL
    )
    executions = await crud_workflow_execution.list(
        executor_id=executor_id,
        workflow_version_id=workflow_version_id,
        status_list=execution_status,
        id_after=pagination_parameters.id_after,
        limit=pagination_parameters.per_page + 1,
        sort=pagination_parameters.sort,
        start_after=start_after,
        start_before=start_before,
        workflow_id=workflow_id,
    )
    if len(executions) == pagination_parameters.per_page + 1:
        query_params.append(("id_after", str(executions[-1].execution_id)))
        response.headers.append(
            HTTPLinkHeader.header_key,
            HTTPLinkHeader()
            .add(
                link=AnyHttpUrl(
                    f"{str(settings.ui_uri).strip('/')}{settings.api_prefix}/ui{router.prefix}?{urllib.parse.urlencode(query_params)}"
                ),
                rel="next",
            )
            .render_header_value(),
        )
    return [
        WorkflowExecutionOut.from_db_model(
            execution, execution.workflow_version.workflow_id if execution.workflow_version is not None else None
        )
        for execution in executions[: pagination_parameters.per_page]
    ]


@router.get(
    "/{eid}", status_code=status.HTTP_200_OK, summary="Get a workflow execution", response_model_exclude_none=True
)
@start_as_current_span_async("api_get_workflow_execution", tracer=tracer)
async def get_workflow_execution(
    workflow_execution: CurrentWorkflowExecution,
    current_user: CurrentUser,
    authorization: Authorization,
) -> WorkflowExecutionOut:
    """
    Get a specific workflow execution.

    Permission `workflow_execution:read` required if the current user started the workflow execution,
    otherwise `workflow_execution:read_any` required.
    \f
    Parameters
    ----------
    workflow_execution : clowm.models.WorkflowExecution
        Workflow execution with given ID. Dependency Injection.
    current_user : clowm.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.

    Returns
    -------
    execution : clowm.models.WorkflowExecution
        Workflow execution with the given ID.
    """
    trace.get_current_span().set_attribute("execution_id", str(workflow_execution.execution_id))
    authorization(RBACOperation.READ if workflow_execution.executor_id == current_user.uid else RBACOperation.READ_ANY)
    return WorkflowExecutionOut.from_db_model(
        workflow_execution,
        workflow_execution.workflow_version.workflow_id if workflow_execution.workflow_version is not None else None,
    )


@router.get(
    "/{eid}/params",
    status_code=status.HTTP_200_OK,
    summary="Get the parameters of a workflow execution",
    response_model=dict[str, Any],
)
@start_as_current_span_async("api_get_workflow_execution_params", tracer=tracer)
async def get_workflow_execution_params(
    request: Request,
    workflow_execution: CurrentWorkflowExecution,
    current_user: CurrentUser,
    authorization: Authorization,
    s3: S3Service,
    client: HTTPClient,
) -> StreamingResponse:
    """
    Get the parameters of a specific workflow execution.

    Permission `workflow_execution:read` required if the current user started the workflow execution,
    otherwise `workflow_execution:read_any` required.
    \f
    Parameters
    ----------
    request : fastapi.Request
        Raw request object
    workflow_execution : clowm.models.WorkflowExecution
        Workflow execution with given ID. Dependency Injection.
    current_user : clowm.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    s3 : boto3_type_annotations.s3.ServiceResource
        S3 Service to perform operations on buckets in Ceph. Dependency Injection.
    client : httpx.AsyncClient
        HTTP Client with an open connection. Dependency Injection.

    Returns
    -------
    response : StreamingResponse
        Streams the requested workflow parameters from the S3 directly to the client
    """
    trace.get_current_span().set_attribute("execution_id", str(workflow_execution.execution_id))
    authorization(RBACOperation.READ if workflow_execution.executor_id == current_user.uid else RBACOperation.READ_ANY)

    try:
        obj = await s3.Object(
            bucket_name=settings.s3.data_bucket,
            key=settings.s3.execution_parameters_key(workflow_execution.execution_id),
        )
        await obj.load()
    except ClientError as err:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="parameters not found") from err

    url = await s3.meta.client.generate_presigned_url(
        ClientMethod="get_object",
        Params={
            "Bucket": obj.bucket_name,
            "Key": obj.key,
        },
        ExpiresIn=30,
        HttpMethod="GET",
    )
    return await stream_response_from_url(
        url=AnyHttpUrl(url), request=request, media_type="application/json", client=client
    )


@router.delete("/{eid}", status_code=status.HTTP_204_NO_CONTENT, summary="Delete a workflow execution")
@start_as_current_span_async("api_delete_workflow_execution", tracer=tracer)
async def delete_workflow_execution(
    background_tasks: BackgroundTasks,
    crud_workflow_execution: CrudWorkflowExecutionSession,
    current_user: CurrentUser,
    authorization: Authorization,
    workflow_execution: CurrentWorkflowExecution,
) -> None:
    """
    Delete a specific workflow execution.

    Permission `workflow_execution:delete` required if the current user started the workflow execution,
    otherwise `workflow_execution:delete_any` required.
    \f
    Parameters
    ----------
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    workflow_execution : clowm.models.WorkflowExecution
        Workflow execution with given ID. Dependency Injection.
    crud_workflow_execution : clowm.crud.crud_workflow_execution.CRUDWorkflowExecution
        Active CRUD repository for workflow executions. Dependency Injection.
    current_user : clowm.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    """
    trace.get_current_span().set_attribute("execution_id", str(workflow_execution.execution_id))
    authorization(
        RBACOperation.DELETE if workflow_execution.executor_id == current_user.uid else RBACOperation.DELETE_ANY
    )
    if workflow_execution.status in [
        WorkflowExecution.WorkflowExecutionStatus.PENDING,
        WorkflowExecution.WorkflowExecutionStatus.SCHEDULED,
        WorkflowExecution.WorkflowExecutionStatus.RUNNING,
    ]:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Cannot delete workflow execution that is not finished."
        )
    background_tasks.add_task(
        delete_from_data_bucket, key=settings.s3.execution_parameters_key(workflow_execution.execution_id)
    )
    await crud_workflow_execution.delete(workflow_execution.execution_id)


@router.post("/{eid}/cancel", status_code=status.HTTP_204_NO_CONTENT, summary="Cancel a workflow execution")
@start_as_current_span_async("api_cancel_workflow_execution", tracer=tracer)
async def cancel_workflow_execution(
    background_tasks: BackgroundTasks,
    crud_workflow_execution: CrudWorkflowExecutionSession,
    current_user: CurrentUser,
    authorization: Authorization,
    workflow_execution: CurrentWorkflowExecution,
) -> None:
    """
    Cancel a running workflow execution.

    Permission `workflow_execution:cancel` required if the current user started the workflow execution,
    otherwise `workflow_execution:cancel_any` required.
    \f
    Parameters
    ----------
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    workflow_execution : clowm.models.WorkflowExecution
        Workflow execution with given ID. Dependency Injection.
    crud_workflow_execution : clowm.crud.crud_workflow_execution.CRUDWorkflowExecution
        Active CRUD repository for workflow executions. Dependency Injection.
    current_user : clowm.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    """
    trace.get_current_span().set_attribute("execution_id", str(workflow_execution.execution_id))
    authorization(
        RBACOperation.CANCEL if workflow_execution.executor_id == current_user.uid else RBACOperation.CANCEL_ANY
    )

    if workflow_execution.status not in [
        WorkflowExecution.WorkflowExecutionStatus.PENDING,
        WorkflowExecution.WorkflowExecutionStatus.SCHEDULED,
        WorkflowExecution.WorkflowExecutionStatus.RUNNING,
    ]:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Cannot cancel workflow execution that is finished."
        )
    if workflow_execution.slurm_job_id >= 0:
        background_tasks.add_task(cancel_slurm_job, job_id=workflow_execution.slurm_job_id)
    await crud_workflow_execution.update_status(
        workflow_execution.execution_id,
        status=WorkflowExecution.WorkflowExecutionStatus.CANCELED,
        end_timestamp=round(time.time()),
    )
