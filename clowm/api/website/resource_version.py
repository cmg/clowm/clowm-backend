from typing import Annotated

from fastapi import APIRouter, BackgroundTasks, Body, Depends, HTTPException, Query, status
from fastapi.requests import Request
from fastapi.responses import Response
from opentelemetry import trace

from clowm.api.dependencies.website import AuthorizationDependencyCookieUser as AuthorizationDependency
from clowm.api.dependencies.website import CurrentUserCookie as CurrentUser
from clowm.ceph.s3 import get_s3_object
from clowm.core.config import settings
from clowm.core.rbac import RBACOperation, RBACResource
from clowm.crud import CRUDResourceVersion
from clowm.models import ResourceVersion
from clowm.otlp import start_as_current_span_async
from clowm.schemas.resource import ResourceOut
from clowm.schemas.resource_version import (
    FileTree,
    ResourceVersionIn,
    ResourceVersionOut,
    UserRequestAnswer,
    UserSynchronizationRequestIn,
    resource_version_key,
)
from clowm.smtp.send_email import (
    send_resource_review_response_email,
    send_sync_request_email,
    send_sync_response_email,
)

from ..background.cluster import (
    delete_cluster_resource_version,
    set_cluster_resource_version_latest,
    synchronize_cluster_resource,
)
from ..background.s3 import (
    delete_s3_resource_version,
    remove_permission_to_s3_resource_version,
)
from ..core import ResourceCoreDep
from ..dependencies import (
    CRUDSessionDep,
    CurrentResource,
    CurrentResourceVersion,
    S3Service,
)
from ..dependencies.auth import AuthorizationFunction

router = APIRouter(prefix="/resources/{rid}/versions", tags=["ResourceVersion"])
resource_authorization = AuthorizationDependency(resource=RBACResource.RESOURCE)
Authorization = Annotated[AuthorizationFunction, Depends(resource_authorization)]
CrudResourceVersionSession = Annotated[CRUDResourceVersion, Depends(CRUDSessionDep(CRUDResourceVersion))]
tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.get("", summary="List versions of a resource")
@start_as_current_span_async("api_list_resource_versions", tracer=tracer)
async def list_resource_versions(
    authorization: Authorization,
    resource: CurrentResource,
    current_user: CurrentUser,
    version_status: Annotated[
        list[ResourceVersion.ResourceVersionStatus] | None,
        Query(
            description="Which versions of the resource to include in the response. Permission `resource:read_any` required if None or querying for non-public resources, otherwise only permission `resource:read` required.",  # noqa: E501
        ),
    ] = None,
) -> list[ResourceVersionOut]:
    """
    List all the resource version for a specific resource.

    Permission 'resource:read' required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    resource : clowm.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency injection.
    version_status : list[clowm.models.ResourceVersion.Status] | None, default None
        Filter resource version by their status. Query Parameter.

    Returns
    -------
    versions : list[clowm.schemas.resource_version.ResourceVersionOut]
        List of versions for the request resource
    """
    current_span = trace.get_current_span()
    if version_status:  # pragma: no cover
        current_span.set_attribute("version_status", [state.name for state in version_status])
    current_span.set_attribute("resource_id", str(resource.resource_id))
    rbac_operation = (
        RBACOperation.LIST_ALL
        if resource.maintainer_id != current_user.uid
        # if status is None or contains non-public resources
        and (version_status is None or sum(map(lambda r_status: not r_status.public, version_status)) > 0)
        else RBACOperation.LIST
    )
    authorization(rbac_operation)
    return [
        ResourceVersionOut.from_db_resource_version(version)
        for version in resource.versions
        if version_status is None or version.status in version_status
    ]


@router.post("", summary="Request new version of a resource", status_code=status.HTTP_201_CREATED)
@start_as_current_span_async("api_request_resource_version", tracer=tracer)
async def request_resource_version(
    authorization: Authorization,
    resource: CurrentResource,
    resource_version_in: Annotated[
        ResourceVersionIn, Body(description="Meta-data for the resource version to request")
    ],
    current_user: CurrentUser,
    resource_core: ResourceCoreDep,
) -> ResourceVersionOut:
    """
    Request a new resource version.

    Permission `resource:update` required if the current user is the maintainer, `resource:update_any` otherwise.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    resource : clowm.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    resource_version_in : clowm.schemas.resource_version.ResourceVersionIn
        Data about the new resource version. HTTP Body.
    current_user : clowm.models.User
        Current user. Dependency injection.
    resource_core : clowm.api.core.ResourceCore
        Core functionality for resource API. Dependency Injection.

    Returns
    -------
    version : clowm.schemas.resource_version.ResourceVersionOut
        Newly created resource version
    """
    current_span = trace.get_current_span()
    current_span.set_attributes(
        {"resource_id": str(resource.resource_id), "resource_version_in": resource_version_in.model_dump_json(indent=2)}
    )
    authorization(RBACOperation.UPDATE if current_user.uid == resource.maintainer_id else RBACOperation.UPDATE_ANY)
    resource_version = await resource_core.create_version(
        resource=resource, version_in=resource_version_in, current_user=current_user
    )
    return ResourceVersionOut.from_db_resource_version(resource_version)


@router.get("/{rvid}", summary="Get version of a resource")
@start_as_current_span_async("api_get_resource_version", tracer=tracer)
async def get_resource_version(
    authorization: Authorization,
    resource: CurrentResource,
    current_user: CurrentUser,
    resource_version: CurrentResourceVersion,
) -> ResourceVersionOut:
    """
    Get a specific resource version for a specific resource.

    Permission `resource:read` required. If the status of the resource version is not `LATEST` or `SYNCHRONIZED` and
    the current user is not the maintainer, then the permission `resource:read_any` is required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    resource : clowm.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency injection.
    resource_version : clowm.models.ResourceVersion
        Resource Version associated with the ID in the path. Dependency Injection.

    Returns
    -------
    version : clowm.schemas.resource_version.ResourceVersionOut
        Requested resource version
    """
    trace.get_current_span().set_attributes(
        {"resource_id": str(resource.resource_id), "resource_version_id": str(resource_version.resource_version_id)}
    )
    rbac_operation = RBACOperation.READ
    # Maintainer can read any version of his workflow, others only synchronized and latest ones
    if current_user.uid != resource.maintainer_id and resource_version.status not in [
        ResourceVersion.ResourceVersionStatus.SYNCHRONIZED,
        ResourceVersion.ResourceVersionStatus.LATEST,
    ]:
        rbac_operation = RBACOperation.READ_ANY
    authorization(rbac_operation)
    return ResourceVersionOut.from_db_resource_version(resource_version)


@router.get(
    "/{rvid}/tree",
    summary="Download folder structure of resource",
    response_model_exclude_none=True,
    response_model=list[FileTree],
)
@start_as_current_span_async("api_get_resource_version_folder_structure", tracer=tracer)
async def resource_file_tree(
    authorization: Authorization,
    request: Request,
    resource_core: ResourceCoreDep,
    resource_version: CurrentResourceVersion,
) -> Response:
    """
    Get the folder structure of the resources. Only available if the resource was previously downloaded to the cluster.

    Permission `resource:read` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    request : fastapi.requests.Request
        Raw request object to read headers from.
    s3 : types_aiobotocore_s3.service_resource import S3ServiceResource
        S3 Service to perform operations on buckets. Dependency Injection.
    resource_version : clowm.models.ResourceVersion
        Resource Version associated with the ID in the path. Dependency Injection.
    resource_core : clowm.api.core.ResourceCore
        Core functionality for resource API. Dependency Injection.

    Returns
    -------
    response : fastapi.responses.Response
        Raw response object containing the file
    """
    authorization(RBACOperation.READ)
    return await resource_core.download_resource_file_tree(resource_version, request=request)


@router.put("/{rvid}/request_review", summary="Request resource version review")
@start_as_current_span_async("api_request_resource_version_review", tracer=tracer)
async def request_resource_version_review(
    authorization: Authorization,
    resource: CurrentResource,
    resource_version: CurrentResourceVersion,
    current_user: CurrentUser,
    resource_core: ResourceCoreDep,
) -> ResourceVersionOut:
    """
    Request the review of a resource version.

    Permission `resource:update` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    resource : clowm.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency injection.
    resource_version : clowm.models.ResourceVersion
        Resource Version associated with the ID in the path. Dependency Injection.
    resource_core : clowm.api.core.ResourceCore
        Core functionality for resource API. Dependency Injection.

    Returns
    -------
    version : clowm.schemas.resource_version.ResourceVersionOut
        Resource version with updated status
    """
    trace.get_current_span().set_attributes(
        {"resource_id": str(resource.resource_id), "resource_version_id": str(resource_version.resource_version_id)}
    )
    authorization(RBACOperation.UPDATE if current_user.uid == resource.maintainer_id else RBACOperation.UPDATE_ANY)
    version = await resource_core.request_version_review(
        resource=resource, version=resource_version, current_uid=current_user.uid
    )
    return ResourceVersionOut.from_db_resource_version(version)


@router.put("/{rvid}/request_sync", summary="Request resource version synchronization")
@start_as_current_span_async("api_request_resource_version_sync", tracer=tracer)
async def request_resource_version_sync(
    authorization: Authorization,
    resource: CurrentResource,
    resource_version: CurrentResourceVersion,
    current_user: CurrentUser,
    crud_resource_version: CrudResourceVersionSession,
    background_tasks: BackgroundTasks,
    sync_request: Annotated[
        UserSynchronizationRequestIn, Body(description="Meta-data for the synchronization request")
    ],
) -> ResourceVersionOut:
    """
    Request the synchronization of a resource version to the cluster.

    Permission `resource:request_sync` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    resource : clowm.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency injection.
    resource_version : clowm.models.ResourceVersion
        Resource Version associated with the ID in the path. Dependency Injection.
    crud_resource_version : clowm.crud.crud_resource_version.CRUDResourceVersion
        Active CRUD repository for resource versions. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    sync_request : clowm.schemas.resource_version.UserSynchronizationRequestIn
        Reason for the synchronization request, HTTP Body.

    Returns
    -------
    version : clowm.schemas.resource_version.ResourceVersionOut
        Resource version with updated status
    """
    trace.get_current_span().set_attributes(
        {
            "resource_id": str(resource.resource_id),
            "resource_version_id": str(resource_version.resource_version_id),
            **sync_request.model_dump(),
        }
    )
    authorization(RBACOperation.REQUEST_SYNC)
    if resource_version.status is not ResourceVersion.ResourceVersionStatus.APPROVED:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Can't request sync for resource version with status {resource_version.status.name}",
        )
    resource_version_out = ResourceVersionOut.from_db_resource_version(resource_version)
    await crud_resource_version.update_status(
        resource_version_id=resource_version.resource_version_id,
        status=ResourceVersion.ResourceVersionStatus.SYNC_REQUESTED,
        values={
            "synchronization_request_description": sync_request.reason,
            "synchronization_request_uid": current_user.uid,
        },
    )
    resource_version_out.status = ResourceVersion.ResourceVersionStatus.SYNC_REQUESTED
    background_tasks.add_task(
        remove_permission_to_s3_resource_version,
        resource_version=resource_version_out,
    )
    background_tasks.add_task(
        send_sync_request_email,
        resource=ResourceOut.from_db_resource(resource),
        version=ResourceVersionOut.from_db_resource_version(resource_version),
        request_reason=sync_request.reason,
        requester=current_user,
    )
    return resource_version_out


@router.put("/{rvid}/review", summary="Review resource version")
@start_as_current_span_async("api_resource_version_review", tracer=tracer)
async def resource_version_review(
    authorization: Authorization,
    resource: CurrentResource,
    resource_version: CurrentResourceVersion,
    crud_resource_version: CrudResourceVersionSession,
    answer: Annotated[UserRequestAnswer, Body(description="Answer for the resource version review")],
    background_tasks: BackgroundTasks,
) -> ResourceVersionOut:
    """
    Review answer the resource version.

    Permission `resource:update_status` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    resource : clowm.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    resource_version : clowm.models.ResourceVersion
        Resource Version associated with the ID in the path. Dependency Injection.
    crud_resource_version : clowm.crud.crud_resource_version.CRUDResourceVersion
        Active CRUD repository for resource versions. Dependency Injection.
    answer : clowm.schemas.UserRequestAnswer
        Answer to the review request. HTTP Body.

    Returns
    -------
    version : clowm.schemas.resource_version.ResourceVersionOut
        Resource version with updated status
    """
    trace.get_current_span().set_attributes(
        {
            "resource_id": str(resource.resource_id),
            "resource_version_id": str(resource_version.resource_version_id),
            **answer.model_dump(exclude_none=True),
        }
    )
    authorization(RBACOperation.UPDATE_STATUS)
    if resource_version.status != ResourceVersion.ResourceVersionStatus.WAIT_FOR_REVIEW:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Can't deny resource version with status {resource_version.status.name}",
        )

    resource_version_out = ResourceVersionOut.from_db_resource_version(resource_version)
    resource_version_out.status = (
        ResourceVersion.ResourceVersionStatus.DENIED if answer.deny else ResourceVersion.ResourceVersionStatus.APPROVED
    )
    await crud_resource_version.update_status(
        resource_version_id=resource_version.resource_version_id, status=resource_version_out.status
    )
    background_tasks.add_task(
        send_resource_review_response_email,
        resource=ResourceOut.from_db_resource(resource),
        version=ResourceVersionOut.from_db_resource_version(resource_version),
        deny_reason=answer.reason if answer.deny else None,
    )
    return resource_version_out


@router.put("/{rvid}/sync", summary="Synchronize resource version with cluster")
@start_as_current_span_async("api_resource_version_sync", tracer=tracer)
async def resource_version_sync(
    authorization: Authorization,
    resource: CurrentResource,
    resource_version: CurrentResourceVersion,
    crud_resource_version: CrudResourceVersionSession,
    s3: S3Service,
    background_tasks: BackgroundTasks,
    answer: Annotated[UserRequestAnswer, Body(description="Answer to the resource version synchronization request")],
) -> ResourceVersionOut:
    """
    Synchronize the resource version to the cluster.

    Permission `resource:update_any` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    resource : clowm.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    resource_version : clowm.models.ResourceVersion
        Resource Version associated with the ID in the path. Dependency Injection.
    crud_resource_version : clowm.crud.crud_resource_version.CRUDResourceVersion
        Active CRUD repository for resource versions. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    s3 : types_aiobotocore_s3.service_resource import S3ServiceResource
        S3 Service to perform operations on buckets. Dependency Injection.
    answer : clowm.schemas.UserRequestAnswer
        Answer to the synchronization request. HTTP Body.

    Returns
    -------
    version : clowm.schemas.resource_version.ResourceVersionOut
        Resource version with updated status
    """
    trace.get_current_span().set_attributes(
        {
            "resource_id": str(resource.resource_id),
            "resource_version_id": str(resource_version.resource_version_id),
            **answer.model_dump(exclude_none=True),
        }
    )
    authorization(RBACOperation.UPDATE_ANY)
    if resource_version.status not in [
        ResourceVersion.ResourceVersionStatus.APPROVED,
        ResourceVersion.ResourceVersionStatus.SYNC_REQUESTED,
        ResourceVersion.ResourceVersionStatus.SYNC_ERROR,
    ]:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Can't sync resource version with status {resource_version.status.name}",
        )
    if resource_version.status is not ResourceVersion.ResourceVersionStatus.SYNC_REQUESTED and answer.deny:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Can't deny sync request for resource version with status {resource_version.status.name}",
        )
    resource_version_out = ResourceVersionOut.from_db_resource_version(resource_version)
    resource_version_obj = await get_s3_object(
        s3=s3,
        bucket_name=settings.s3.data_bucket,
        key=resource_version_key(
            resource_id=resource_version.resource_id, resource_version_id=resource_version.resource_version_id
        ),
    )
    if not answer.deny and resource_version_obj is None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Missing resource at S3 path {resource_version_out.s3_path}",
        )

    resource_version_out.status = (
        ResourceVersion.ResourceVersionStatus.APPROVED
        if answer.deny
        else ResourceVersion.ResourceVersionStatus.SYNCHRONIZING
    )
    await crud_resource_version.update_status(
        resource_version_id=resource_version.resource_version_id, status=resource_version_out.status
    )

    if not answer.deny:
        background_tasks.add_task(
            synchronize_cluster_resource,
            resource=ResourceOut.from_db_resource(resource),
            resource_version=resource_version_out,
        )
    background_tasks.add_task(
        send_sync_response_email,
        resource=resource,
        version=resource_version,
        deny_reason=answer.reason if answer.deny else None,
    )
    return resource_version_out


@router.put("/{rvid}/latest", summary="Set resource version to latest")
@start_as_current_span_async("api_resource_version_set_latest", tracer=tracer)
async def resource_version_latest(
    authorization: Authorization,
    crud_resource_version: CrudResourceVersionSession,
    resource: CurrentResource,
    resource_version: CurrentResourceVersion,
    background_tasks: BackgroundTasks,
) -> ResourceVersionOut:
    """
    Set the resource version as the latest version.

    Permission `resource:update_any` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    crud_resource_version : clowm.crud.crud_resource_version.CRUDResourceVersion
        Active CRUD repository for resource versions. Dependency Injection.
    resource : clowm.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    resource_version : clowm.models.ResourceVersion
        Resource Version associated with the ID in the path. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.

    Returns
    -------
    version : clowm.schemas.resource_version.ResourceVersionOut
        Resource version with updated status
    """
    trace.get_current_span().set_attributes(
        {"resource_id": str(resource.resource_id), "resource_version_id": str(resource_version.resource_version_id)}
    )
    authorization(RBACOperation.UPDATE_ANY)
    if resource_version.status != ResourceVersion.ResourceVersionStatus.SYNCHRONIZED:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Can't set resource version to {ResourceVersion.ResourceVersionStatus.LATEST.name} with status {resource_version.status.name}",
        )
    await crud_resource_version.update_status(
        resource_version_id=resource_version.resource_version_id,
        status=ResourceVersion.ResourceVersionStatus.SETTING_LATEST,
    )
    resource_version_out = ResourceVersionOut.from_db_resource_version(resource_version)
    resource_version_out.status = ResourceVersion.ResourceVersionStatus.SETTING_LATEST
    background_tasks.add_task(set_cluster_resource_version_latest, resource_version=resource_version_out)
    return resource_version_out


@router.delete("/{rvid}/cluster", summary="Delete resource version on cluster")
@start_as_current_span_async("api_resource_version_delete_cluster", tracer=tracer)
async def delete_resource_version_cluster(
    authorization: Authorization,
    resource: CurrentResource,
    resource_version: CurrentResourceVersion,
    crud_resource_version: CrudResourceVersionSession,
    background_tasks: BackgroundTasks,
) -> ResourceVersionOut:
    """
    Delete the resource version on the cluster.

    Permission `resource:delete_any` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    resource : clowm.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    resource_version : clowm.models.ResourceVersion
        Resource Version associated with the ID in the path. Dependency Injection.
    crud_resource_version : clowm.crud.crud_resource_version.CRUDResourceVersion
        Active CRUD repository for resource versions. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.

    Returns
    -------
    version : clowm.schemas.resource_version.ResourceVersionOut
        Resource version with updated status
    """
    trace.get_current_span().set_attributes(
        {"resource_id": str(resource.resource_id), "resource_version_id": str(resource_version.resource_version_id)}
    )
    authorization(RBACOperation.DELETE_ANY)
    if resource_version.status not in [
        ResourceVersion.ResourceVersionStatus.SYNCHRONIZED,
        ResourceVersion.ResourceVersionStatus.LATEST,
        ResourceVersion.ResourceVersionStatus.CLUSTER_DELETE_ERROR,
    ]:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Can't delete resource version on cluster with status {resource_version.status.name}",
        )
    await crud_resource_version.update_status(
        resource_version_id=resource_version.resource_version_id,
        status=ResourceVersion.ResourceVersionStatus.CLUSTER_DELETING,
    )
    resource_version.status = ResourceVersion.ResourceVersionStatus.CLUSTER_DELETING
    resource_version_out = ResourceVersionOut.from_db_resource_version(resource_version)
    background_tasks.add_task(delete_cluster_resource_version, resource_version=resource_version_out)
    return resource_version_out


@router.delete("/{rvid}/s3", summary="Delete resource version in S3")
@start_as_current_span_async("api_resource_version_delete_cluster", tracer=tracer)
async def delete_resource_version_s3(
    authorization: Authorization,
    resource: CurrentResource,
    resource_version: CurrentResourceVersion,
    crud_resource_version: CrudResourceVersionSession,
    background_tasks: BackgroundTasks,
) -> ResourceVersionOut:
    """
    Delete the resource version in the S3 bucket.

    Permission `resource:delete_any` required.
    \f
    Parameters
    ----------
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    resource : clowm.models.Resource
        Resource associated with the ID in the path. Dependency Injection.
    resource_version : clowm.models.ResourceVersion
        Resource Version associated with the ID in the path. Dependency Injection.
    crud_resource_version : clowm.crud.crud_resource_version.CRUDResourceVersion
        Active CRUD repository for resource versions. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.

    Returns
    -------
    version : clowm.schemas.resource_version.ResourceVersionOut
        Resource version with updated status
    """
    trace.get_current_span().set_attributes(
        {"resource_id": str(resource.resource_id), "resource_version_id": str(resource_version.resource_version_id)}
    )
    authorization(RBACOperation.DELETE_ANY)
    if resource_version.status not in [
        ResourceVersion.ResourceVersionStatus.DENIED,
        ResourceVersion.ResourceVersionStatus.APPROVED,
        ResourceVersion.ResourceVersionStatus.S3_DELETE_ERROR,
    ]:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Can't delete S3 resource version with status {resource_version.status.name}",
        )
    await crud_resource_version.update_status(
        resource_version_id=resource_version.resource_version_id,
        status=ResourceVersion.ResourceVersionStatus.S3_DELETING,
    )
    resource_version.status = ResourceVersion.ResourceVersionStatus.S3_DELETING
    background_tasks.add_task(
        delete_s3_resource_version,
        resource_id=resource.resource_id,
        resource_version_id=resource_version.resource_version_id,
    )
    background_tasks.add_task(
        remove_permission_to_s3_resource_version,
        resource_version=ResourceVersionOut.from_db_resource_version(resource_version),
    )

    return ResourceVersionOut.from_db_resource_version(resource_version)
