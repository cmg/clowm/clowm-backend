from typing import Annotated
from uuid import UUID

from botocore.exceptions import ClientError
from fastapi import (
    APIRouter,
    BackgroundTasks,
    Body,
    Depends,
    File,
    HTTPException,
    Path,
    Query,
    Request,
    UploadFile,
    status,
)
from fastapi.responses import Response, StreamingResponse
from opentelemetry import trace
from pydantic import AnyHttpUrl
from starlette.background import BackgroundTask as StarletteBackgroundTask

from clowm.api.dependencies.website import AuthorizationDependencyCookieUser as AuthorizationDependency
from clowm.api.dependencies.website import CurrentUserCookie as CurrentUser
from clowm.core.config import settings
from clowm.core.rbac import RBACOperation, RBACResource
from clowm.crud import CRUDWorkflowVersion
from clowm.git_repository import build_repository
from clowm.models import WorkflowVersion
from clowm.otlp import start_as_current_span_async
from clowm.schemas.documentation_enum import DocumentationEnum
from clowm.schemas.workflow_version import (
    IconUpdateOut,
    ParameterExtension,
    WorkflowVersionMetadataIn,
    WorkflowVersionMetadataOut,
    WorkflowVersionOut,
    WorkflowVersionStatusSchema,
)
from clowm.smtp.send_email import send_workflow_status_update_email

from ...utils.job import Job
from ..background.news import create_workflow_news_event
from ..background.s3 import (
    delete_from_data_bucket,
    delete_remote_icon,
    download_file_to_data_bucket,
    upload_string_to_data_bucket,
)
from ..container_image_check import ContainerImage
from ..core import WorkflowCoreDep
from ..dependencies import (
    CRUDSessionDep,
    CurrentWorkflow,
    CurrentWorkflowVersion,
    HTTPClient,
    S3Service,
)
from ..dependencies.auth import AuthorizationFunction
from ..utils import stream_response_from_url, upload_icon

router = APIRouter(prefix="/workflows/{wid}/versions", tags=["Workflow Version"])
workflow_authorization = AuthorizationDependency(resource=RBACResource.WORKFLOW)
Authorization = Annotated[AuthorizationFunction, Depends(workflow_authorization)]
CrudWorkflowVersionSession = Annotated[CRUDWorkflowVersion, Depends(CRUDSessionDep(CRUDWorkflowVersion))]

tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.get(
    "", status_code=status.HTTP_200_OK, summary="Get all versions of a workflow", response_model_exclude_none=True
)
@start_as_current_span_async("api_workflow_version_list", tracer=tracer)
async def list_workflow_version(
    current_user: CurrentUser,
    workflow: CurrentWorkflow,
    crud_workflow_version: CrudWorkflowVersionSession,
    authorization: Authorization,
    version_status: Annotated[
        list[WorkflowVersion.WorkflowVersionStatus] | None,
        Query(
            description=f"Which versions of the workflow to include in the response. Permission `workflow:list_filter` required if you are not the developer of this workflow. Default `{WorkflowVersion.WorkflowVersionStatus.PUBLISHED.name}` and `{WorkflowVersion.WorkflowVersionStatus.DEPRECATED.name}`",  # noqa: E501
        ),
    ] = None,
) -> list[WorkflowVersionOut]:
    """
    List all versions of a Workflow.

    Permission `workflow:list` required.
    \f
    Parameters
    ----------
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    version_status : list[clowm.models.WorkflowVersion.Status] | None, default None
        Status of Workflow versions to filter for to fetch. Query Parameter
    crud_workflow_version : clowm.crud.crud_workflow_version.CRUDWorkflowVersion
        Active CRUD repository for workflow versions. Dependency Injection.
    current_user : clowm.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.

    Returns
    -------
    versions : [clowm.schemas.workflow_version.WorkflowVersion]
        All versions of the workflow
    """
    current_span = trace.get_current_span()
    current_span.set_attribute("workflow_id", str(workflow.workflow_id))
    if version_status is not None and len(version_status) > 0:
        current_span.set_attribute("version_status", [stat.name for stat in version_status])
    authorization(
        RBACOperation.LIST
        if workflow.developer_id == current_user.uid and version_status is not None
        else RBACOperation.LIST_ALL
    )

    versions = await crud_workflow_version.list_workflow_versions(
        workflow.workflow_id,
        version_status=(
            version_status
            if version_status is not None
            else [WorkflowVersion.WorkflowVersionStatus.PUBLISHED, WorkflowVersion.WorkflowVersionStatus.DEPRECATED]
        ),
    )
    return [WorkflowVersionOut.from_db_version(v, load_modes=True) for v in versions]


@router.get(
    "/{git_commit_hash}",
    status_code=status.HTTP_200_OK,
    summary="Get a workflow version",
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_workflow_version_get", tracer=tracer)
async def get_workflow_version(
    workflow: CurrentWorkflow,
    crud_workflow_version: CrudWorkflowVersionSession,
    current_user: CurrentUser,
    authorization: Authorization,
    git_commit_hash: Annotated[
        str,
        Path(
            description="Git commit `git_commit_hash` of specific version or `latest`.",
            pattern=r"^([0-9a-f]{40}|latest)$",
            examples=["latest", "ba8bcd9294c2c96aedefa1763a84a18077c50c0f"],
        ),
    ],
) -> WorkflowVersionOut:
    """
    Get a specific version of a workflow.

    Permission `workflow:read` required if the version is public or you are the developer of the workflow,
    otherwise `workflow:read_any`
    \f
    Parameters
    ----------
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    git_commit_hash: str
        Version ID
    crud_workflow_version : clowm.crud.crud_workflow_version.CRUDWorkflowVersion
        Active CRUD repository for workflow versions. Dependency Injection.
    current_user : clowm.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.

    Returns
    -------
    version : clowm.schemas.workflow_version.WorkflowVersionOut
        The specified WorkflowVersion
    """
    trace.get_current_span().set_attributes(
        {"workflow_id": str(workflow.workflow_id), "workflow_version_id": git_commit_hash}
    )
    rbac_operation = RBACOperation.READ
    version = (
        await crud_workflow_version.get_latest(workflow.workflow_id)
        if git_commit_hash == "latest"
        else await crud_workflow_version.get(git_commit_hash, workflow_id=workflow.workflow_id)
    )
    if version is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Workflow Version with git_commit_hash '{git_commit_hash}' not found",
        )
    # Developer can read any version of his workflow, others only published and deprecated ones
    if (
        current_user.uid != workflow.developer_id
        and version.status != WorkflowVersion.WorkflowVersionStatus.PUBLISHED
        and version.status != WorkflowVersion.WorkflowVersionStatus.DEPRECATED
    ):
        rbac_operation = RBACOperation.READ_ANY
    authorization(rbac_operation)
    return WorkflowVersionOut.from_db_version(version, load_modes=True)


@router.patch(
    "/{git_commit_hash}/status",
    status_code=status.HTTP_200_OK,
    summary="Update status of workflow version",
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_update_workflow_version_status", tracer=tracer)
async def update_workflow_version_status(
    background_tasks: BackgroundTasks,
    workflow: CurrentWorkflow,
    version_status: Annotated[WorkflowVersionStatusSchema, Body(description="New Status of the workflow version")],
    workflow_version: CurrentWorkflowVersion,
    crud_workflow_version: CrudWorkflowVersionSession,
    authorization: Authorization,
) -> WorkflowVersionOut:
    """
    Update the status of a workflow version.

    Permission `workflow:update_status`
    \f
    Parameters
    ----------
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    version_status : clowm.schemas.workflow_version.WorkflowVersionStatus
        New Status of the workflow version. HTTP Body.
    workflow_version : clowm.models.WorkflowVersion
        Workflow version with given ID. Dependency Injection.
    crud_workflow_version : clowm.crud.crud_workflow_version.CRUDWorkflowVersion
        Active CRUD repository for workflow versions. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.

    Returns
    -------
    version : clowm.models.WorkflowVersion
        Version of the workflow with updated status
    """
    trace.get_current_span().set_attributes(
        {
            "workflow_id": str(workflow_version.workflow_id),
            "workflow_version_id": workflow_version.git_commit_hash,
            "version_status": version_status.status.name,
        }
    )
    authorization(RBACOperation.UPDATE_STATUS)
    await crud_workflow_version.update_status(workflow_version.git_commit_hash, version_status.status)
    workflow_version.status = version_status.status
    if (
        workflow_version.status == WorkflowVersion.WorkflowVersionStatus.DENIED
        or workflow_version.status == WorkflowVersion.WorkflowVersionStatus.PUBLISHED
    ):
        background_tasks.add_task(send_workflow_status_update_email, workflow=workflow, version=workflow_version)
    if workflow_version.status == WorkflowVersion.WorkflowVersionStatus.PUBLISHED:
        background_tasks.add_task(create_workflow_news_event, workflow=workflow, version=workflow_version)
    return WorkflowVersionOut.from_db_version(workflow_version, load_modes=True)


@router.get(
    "/{git_commit_hash}/metadata",
    status_code=status.HTTP_200_OK,
    summary="Get metadata of workflow version",
)
@start_as_current_span_async("api_get_workflow_version_status_metadata", tracer=tracer)
async def get_workflow_version_metadata(
    workflow: CurrentWorkflow,
    workflow_version: CurrentWorkflowVersion,
    authorization: Authorization,
    current_user: CurrentUser,
) -> WorkflowVersionMetadataOut:
    """
    Get the metadata of a workflow version.

    Permission `workflow:read` required if the current user is the developer of the workflow,
    otherwise `workflow:read_any`
    \f
    Parameters
    ----------
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    workflow_version : clowm.models.WorkflowVersion
        Workflow version with given ID. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user: clowm.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.

    Returns
    -------
    meta_data : clowm.schemas.workflow_version.WorkflowVersionMetadataOut
        Current meta-data of the workflow version
    """
    trace.get_current_span().set_attributes(
        {"workflow_id": str(workflow_version.workflow_id), "workflow_version_id": workflow_version.git_commit_hash}
    )
    authorization(RBACOperation.READ if workflow.developer_id == current_user.uid else RBACOperation.READ_ANY)
    return WorkflowVersionMetadataOut.from_db_model(workflow_version)


@router.put(
    "/{git_commit_hash}/metadata",
    status_code=status.HTTP_200_OK,
    summary="Update metadata of workflow version",
)
@start_as_current_span_async("api_update_workflow_version_status_metadata", tracer=tracer)
async def update_workflow_version_metadata(
    background_tasks: BackgroundTasks,
    workflow: CurrentWorkflow,
    update: WorkflowVersionMetadataIn,
    workflow_version: CurrentWorkflowVersion,
    crud_workflow_version: CrudWorkflowVersionSession,
    authorization: Authorization,
    current_user: CurrentUser,
    client: HTTPClient,
) -> WorkflowVersionMetadataOut:
    """
    Update the metadata of a workflow version.

    Permission `workflow:update` required if the current user is the developer of the workflow,
    otherwise `workflow:update_any`
    \f
    Parameters
    ----------
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    update : clowm.schemas.workflow_version.WorkflowVersionMetadataIn
        New Status of the workflow version. HTTP Body.
    workflow_version : clowm.models.WorkflowVersion
        Workflow version with given ID. Dependency Injection.
    crud_workflow_version : clowm.crud.crud_workflow_version.CRUDWorkflowVersion
        Active CRUD repository for workflow versions. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user: clowm.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    client : httpx.AsyncClient
        HTTP Client with an open connection. Dependency Injection.

    Returns
    -------
    meta_data : clowm.schemas.workflow_version.WorkflowVersionMetadataOut
        Updated meta-data of the workflow version
    """
    trace.get_current_span().set_attributes(
        {
            "workflow_id": str(workflow_version.workflow_id),
            "workflow_version_id": workflow_version.git_commit_hash,
            "version_update": update.model_dump_json(),
        }
    )
    authorization(RBACOperation.UPDATE if workflow.developer_id == current_user.uid else RBACOperation.UPDATE_ANY)
    if (
        update.default_container is not None
        and update.default_container != workflow_version.default_container
        and not await ContainerImage.from_string(update.default_container).check_existence(client)
    ):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"container image {update.default_container} doesn't exist",
        )

    if update.nextflow_config != workflow_version.nextflow_config:
        if update.nextflow_config is None:
            background_tasks.add_task(
                delete_from_data_bucket, key=settings.s3.config_key(workflow_version.git_commit_hash)
            )
        else:
            background_tasks.add_task(
                upload_string_to_data_bucket,
                content=update.nextflow_config,
                key=settings.s3.config_key(workflow_version.git_commit_hash),
            )

    await crud_workflow_version.update_meta_data(
        workflow_version_id=workflow_version.git_commit_hash,
        nextflow_version=update.nextflow_version,
        default_container=update.default_container,
        nextflow_config=update.nextflow_config,
    )

    return WorkflowVersionMetadataOut(
        nextflow_version=update.nextflow_version,
        nextflow_config=update.nextflow_config,
        default_container=update.default_container,
    )


@router.patch(
    "/{git_commit_hash}/deprecate",
    status_code=status.HTTP_200_OK,
    summary="Deprecate a workflow version",
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_workflow_version_status_update", tracer=tracer)
async def deprecate_workflow_version(
    workflow: CurrentWorkflow,
    workflow_version: CurrentWorkflowVersion,
    authorization: Authorization,
    current_user: CurrentUser,
    workflow_core: WorkflowCoreDep,
) -> WorkflowVersionOut:
    """
    Deprecate a workflow version.

    Permission `workflow:update` required if you are the developer of the workflow,
    otherwise `workflow:update_status`
    \f
    Parameters
    ----------
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    workflow_version : clowm.models.WorkflowVersion
        Workflow version with given ID. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user: clowm.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    workflow_core : clowm.api.core.WorkflowCore
        Core functionality for workflow API. Dependency Injection.


    Returns
    -------
    version : clowm.models.WorkflowVersion
        Version of the workflow with deprecated status
    """
    trace.get_current_span().set_attributes(
        {"workflow_id": str(workflow_version.workflow_id), "workflow_version_id": workflow_version.git_commit_hash}
    )
    authorization(RBACOperation.UPDATE if current_user.uid == workflow.developer_id else RBACOperation.UPDATE_STATUS)
    await workflow_core.deprecate_version(workflow_version)
    return WorkflowVersionOut.from_db_version(workflow_version, load_modes=True)


@router.patch(
    "/{git_commit_hash}/parameter-extension",
    status_code=status.HTTP_200_OK,
    summary="Update parameter extension of workflow version",
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_workflow_version_update_parameter_extension", tracer=tracer)
async def update_workflow_version_parameter_extension(
    workflow: CurrentWorkflow,
    workflow_version: CurrentWorkflowVersion,
    crud_workflow_version: CrudWorkflowVersionSession,
    authorization: Authorization,
    current_user: CurrentUser,
    parameter_extension: Annotated[
        ParameterExtension, Body(description="Parameter extension specific for this CloWM instance")
    ],
) -> WorkflowVersionOut:
    """
    Update the parameter extension of a workflow version.

    Permission `workflow:update` required.
    \f
    Parameters
    ----------
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    workflow_version : clowm.models.WorkflowVersion
        Workflow version with given ID. Dependency Injection.
    crud_workflow_version : clowm.crud.crud_workflow_version.CRUDWorkflowVersion
        Active CRUD repository for workflow versions. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user: clowm.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    parameter_extension : clowm.schemas.workflow_version.ParameterExtension
        Parameter extension specific for this CloWM instance. HTTP Body.

    Returns
    -------
    version : clowm.models.WorkflowVersion
        Version of the workflow with the updated parameter extension
    """
    trace.get_current_span().set_attributes(
        {"workflow_id": str(workflow_version.workflow_id), "workflow_version_id": workflow_version.git_commit_hash}
    )
    authorization(RBACOperation.UPDATE)
    if current_user.uid != workflow.developer_id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Only the developer can update the parameter extension"
        )
    await crud_workflow_version.update_parameter_extension(workflow_version.git_commit_hash, parameter_extension)
    workflow_version.parameter_extension = parameter_extension.model_dump()
    return WorkflowVersionOut.from_db_version(workflow_version, load_modes=True)


@router.get(
    "/{git_commit_hash}/documentation",
    status_code=status.HTTP_200_OK,
    summary="Fetch documentation for a workflow version",
    response_class=StreamingResponse,
)
@start_as_current_span_async("api_workflow_version_get_documentation", tracer=tracer)
async def download_workflow_documentation(
    request: Request,
    workflow: CurrentWorkflow,
    workflow_version: CurrentWorkflowVersion,
    authorization: Authorization,
    s3: S3Service,
    client: HTTPClient,
    document: Annotated[
        DocumentationEnum, Query(description="Specify which type of documentation the client wants to fetch")
    ] = DocumentationEnum.USAGE,
    mode_id: Annotated[
        UUID | None, Query(description="Workflow Mode", examples=["8d47e878-f25f-41aa-b4a0-95d426b46f45"])
    ] = None,
) -> Response:
    """
    Get the documentation for a specific workflow version.
    Streams the response directly from the right git repository.

    Permission `workflow:read` required.
    \f
    Parameters
    ----------
    request : fastapi.Request
        Raw request object
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    s3 : boto3_type_annotations.s3.ServiceResource
        S3 Service to perform operations on buckets in Ceph. Dependency Injection.
    workflow_version : clowm.models.WorkflowVersion
        Workflow version with given ID. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    client : httpx.AsyncClient
        HTTP Client with an open connection. Dependency Injection.
    document : DocumentationEnum, default DocumentationEnum.USAGE
        Which type of documentation the client wants to fetch. HTTP Query
    mode_id : UUID | None
        Select the workflow mode of the workflow version. HTTP Query

    Returns
    -------
    response : StreamingResponse
        Streams the requested document from the git repository directly to the client
    """
    current_span = trace.get_current_span()
    current_span.set_attributes(
        {
            "workflow_id": str(workflow_version.workflow_id),
            "workflow_version_id": workflow_version.git_commit_hash,
            "document": document.name,
        }
    )
    if mode_id is not None:
        current_span.set_attribute("workflow_mode_id", str(mode_id))
    authorization(RBACOperation.READ)

    workflow_mode = None
    if mode_id is not None:
        workflow_mode = next((mode for mode in workflow_version.workflow_modes if mode.mode_id == mode_id), None)
        if workflow_mode is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail=f"Workflow mode with ID '{mode_id}' not found"
            )

    key = settings.s3.workflow_cache_key(
        document,
        git_commit_hash=workflow_version.git_commit_hash,
        mode_id=mode_id if document == DocumentationEnum.PARAMETER_SCHEMA else None,
    )
    try:
        obj = await s3.Object(bucket_name=settings.s3.data_bucket, key=key)
        if await obj.content_length == 0:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"File {document} not present in workflow repository",
                headers={"cache-control": "max-age=86400"},
            )
        url = await s3.meta.client.generate_presigned_url(
            ClientMethod="get_object",
            Params={
                "Bucket": settings.s3.data_bucket,
                "Key": key,
            },
            ExpiresIn=30,
            HttpMethod="GET",
        )
        return await stream_response_from_url(
            url=AnyHttpUrl(url),
            request=request,
            media_type="application/json" if str(document).endswith(".json") else "text/plain",
            client=client,
        )
    except ClientError:
        repo = build_repository(
            AnyHttpUrl(workflow.repository_url),
            workflow_version.git_commit_hash,
            workflow.credentials_token,
        )
        # if workflow mode is not None and parameter schema is requested, the path is defined by the mode
        if document is DocumentationEnum.PARAMETER_SCHEMA and workflow_mode is not None:
            path = workflow_mode.schema_path
        else:
            # check all possible locations of the documentation file
            for possible_path in document.repository_paths:
                try:
                    # check if the location exist in the repository
                    if await repo.check_file_exists(possible_path, client=client):
                        # if the location exists, stop the search
                        path = possible_path
                        break
                except AssertionError:
                    # error occurs if the file does not exist in a private github repository -> search continues
                    continue
            else:  # pragma: no cover
                # if the file is not in the repository, upload an empty file into the cache and return 404 response
                return Response(
                    content=f"{{'detail': 'File {document} not present in workflow repository'}}",
                    status_code=status.HTTP_404_NOT_FOUND,
                    headers={"cache-control": "max-age=86400"},
                    media_type="application/json",
                    background=StarletteBackgroundTask(upload_string_to_data_bucket, content="", key=key),
                )

        download_url = await repo.download_file_url(path, client)

        return await stream_response_from_url(
            url=download_url,
            request=request,
            media_type="application/json" if str(document).endswith(".json") else "text/plain",
            auth=repo.request_auth,
            background_job=Job(
                download_file_to_data_bucket,
                repo=repo,
                filepath=path,
                key=key,
            ),
            client=client,
        )


@router.post(
    "/{git_commit_hash}/icon",
    status_code=status.HTTP_201_CREATED,
    summary="Upload icon for workflow version",
)
@start_as_current_span_async("api_workflow_version_upload_icon", tracer=tracer)
async def upload_workflow_version_icon(
    workflow: CurrentWorkflow,
    background_tasks: BackgroundTasks,
    workflow_version: CurrentWorkflowVersion,
    authorization: Authorization,
    current_user: CurrentUser,
    crud_workflow_version: CrudWorkflowVersionSession,
    icon: Annotated[UploadFile, File(description="Icon for the Workflow.")],
) -> IconUpdateOut:
    """
    Upload an icon for the workflow version and returns the new icon URL.

    Permission `workflow:update` required.
    \f
    Parameters
    ----------
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    workflow_version : clowm.models.WorkflowVersion
        Workflow version with given ID. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    crud_workflow_version : clowm.crud.crud_workflow_version.CRUDWorkflowVersion
        Active CRUD repository for workflow versions. Dependency Injection.
    icon : fastapi.UploadFile
        New Icon for the workflow version. HTML Form.

    Returns
    -------
    icon_url : str
        URL where the icon can be downloaded
    """
    current_span = trace.get_current_span()
    current_span.set_attributes(
        {"workflow_id": str(workflow_version.workflow_id), "workflow_version_id": workflow_version.git_commit_hash}
    )
    if icon.content_type is not None:  # pragma: no cover
        current_span.set_attribute("content_type", icon.content_type)
    if icon.filename is not None:  # pragma: no cover
        current_span.set_attribute("filename", icon.filename)
    authorization(RBACOperation.UPDATE)
    if current_user.uid != workflow.developer_id:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Only the developer can update his workflow")
    old_slug = workflow_version.icon_slug
    icon_slug = await upload_icon(background_tasks=background_tasks, icon=icon)
    current_span.set_attribute("icon_slug", icon_slug)
    await crud_workflow_version.update_icon(workflow_version.git_commit_hash, icon_slug)
    # Delete old icon if possible
    if old_slug is not None:
        background_tasks.add_task(delete_remote_icon, icon_slug=old_slug)
    return IconUpdateOut(icon_url=settings.s3.icon_url(icon_slug))


@router.delete(
    "/{git_commit_hash}/icon",
    status_code=status.HTTP_204_NO_CONTENT,
    summary="Delete icon of workflow version",
)
@start_as_current_span_async("api_workflow_version_delete_icon", tracer=tracer)
async def delete_workflow_version_icon(
    workflow: CurrentWorkflow,
    workflow_version: CurrentWorkflowVersion,
    background_tasks: BackgroundTasks,
    authorization: Authorization,
    current_user: CurrentUser,
    crud_workflow_version: CrudWorkflowVersionSession,
) -> None:
    """
    Delete the icon of the workflow version.

    Permission `workflow:update` required.
    \f
    Parameters
    ----------
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    workflow_version : clowm.models.WorkflowVersion
        Workflow version with given ID. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    crud_workflow_version : clowm.crud.crud_workflow_version.CRUDWorkflowVersion
        Active CRUD repository for workflow versions. Dependency Injection.
    """
    current_span = trace.get_current_span()
    current_span.set_attributes(
        {"workflow_id": str(workflow_version.workflow_id), "workflow_version_id": workflow_version.git_commit_hash}
    )
    authorization(RBACOperation.UPDATE if current_user.uid == workflow.developer_id else RBACOperation.UPDATE_ANY)
    if workflow_version.icon_slug is not None:
        background_tasks.add_task(delete_remote_icon, icon_slug=workflow_version.icon_slug)
        await crud_workflow_version.update_icon(workflow_version.git_commit_hash, icon_slug=None)
