import tempfile
from datetime import date
from pathlib import Path
from typing import Annotated
from uuid import UUID

from fastapi import APIRouter, BackgroundTasks, Body, Depends, HTTPException, Query, Response, status
from opentelemetry import trace

from clowm.api.dependencies.website import AuthorizationDependencyCookieUser as AuthorizationDependency
from clowm.api.dependencies.website import CurrentUserCookie as CurrentUser
from clowm.core.rbac import RBACOperation, RBACResource
from clowm.crud import CRUDWorkflow, CRUDWorkflowMode, CRUDWorkflowVersion
from clowm.git_repository import GitHubRepository, build_repository
from clowm.models import Workflow, WorkflowVersion
from clowm.otlp import start_as_current_span_async
from clowm.schemas.ownership_transfer import OwnershipTransferRequestIn, OwnershipTransferRequestOut
from clowm.schemas.workflow import WorkflowIn, WorkflowOut, WorkflowStatistic, WorkflowUpdate
from clowm.schemas.workflow_execution import AnonymizedWorkflowExecution
from clowm.schemas.workflow_version import WorkflowVersionOut
from clowm.scm import SCM, SCMProvider
from clowm.smtp.send_email import (
    send_new_workflow_email,
    send_otr_accepted_email,
    send_otr_created_email,
    send_otr_deleted_email,
    send_otr_rejected_email,
)

from ..background.s3 import (
    upload_scm_file,
)
from ..core import WorkflowCoreDep
from ..dependencies import CRUDSessionDep, CurrentWorkflow, HTTPClient
from ..dependencies.auth import AuthorizationFunction
from ..otr_utils import accept_otr, create_otr, delete_otr, get_otr, list_otrs
from ..utils import check_repo, upload_documentation_files

router = APIRouter(prefix="/workflows", tags=["Workflow"])
workflow_authorization = AuthorizationDependency(resource=RBACResource.WORKFLOW)
Authorization = Annotated[AuthorizationFunction, Depends(workflow_authorization)]
CrudWorkflowSession = Annotated[CRUDWorkflow, Depends(CRUDSessionDep(CRUDWorkflow))]

tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.post("", status_code=status.HTTP_201_CREATED, summary="Create a new workflow")
@start_as_current_span_async("api_workflow_create", tracer=tracer)
async def create_workflow(
    background_tasks: BackgroundTasks,
    crud_workflow: CrudWorkflowSession,
    current_user: CurrentUser,
    authorization: Authorization,
    client: HTTPClient,
    workflow: Annotated[WorkflowIn, Body(description="Meta-date for the workflow to create")],
) -> WorkflowOut:
    """
    Create a new workflow.\n
    For private Gitlab repositories, a Project Access Token with the role Reporter and scope `read_api` is needed.\n
    For private GitHub repositories, a Personal Access Token (classic) with scope `repo` is needed.

    Permission `workflow:create` required.
    \f
    Parameters
    ----------
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    workflow : clowm.schemas.workflow.WorkflowIn
        Data about the new Workflow. HTML Body.
    crud_workflow : clowm.crud.crud_workflow.CRUDWorkflow
        Active CRUD repository for workflows. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    client : httpx.AsyncClient
        Http client with an open connection. Dependency Injection.

    Returns
    -------
    workflow : clowm.schemas.workflow.WorkflowOut
        The newly created workflow
    """
    authorization(RBACOperation.CREATE)
    # Check if name is workflow name is already taken
    if await crud_workflow.get_by_name(workflow_name=workflow.name) is not None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=f"Workflow with name '{workflow.name}' already exists"
        )
    # Check if git commit is already used
    if await CRUDWorkflowVersion(db=crud_workflow.db).get(workflow.git_commit_hash) is not None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Workflow with git_commit_hash'{workflow.git_commit_hash}' already exists",
        )

    try:
        # Build a git repository object based on the repository url
        repo = build_repository(str(workflow.repository_url), workflow.git_commit_hash, token=workflow.token)
    except NotImplementedError as err:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Supplied Git Repository is not supported"
        ) from err

    if not await repo.check_repo_availability(client=client):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="repo does not exists")

    # Check if the relevant files are present in the repository
    with tempfile.TemporaryDirectory() as tmp_dir_name:
        await repo.download_and_extract_archive(tmp_dir_name, client=client)
        tmp_dir_path = Path(tmp_dir_name)
        repository_path = tmp_dir_path / next(tmp_dir_path.iterdir(), "")
        await check_repo(repository_path=repository_path, modes=workflow.modes)
        # Create the workflow in the DB
        workflow_db = await crud_workflow.create(workflow, current_user.uid)
        await upload_documentation_files(
            repository_path=repository_path,
            git_commit_hash=workflow.git_commit_hash,
            modes=(
                await CRUDWorkflowMode(db=crud_workflow.db).list_modes(workflow.git_commit_hash)
                if len(workflow.modes) > 0
                else None
            ),
            background_tasks=background_tasks,
        )

    # If it is a private repository, create an SCM file and upload it to the params bucket
    scm_provider = SCMProvider.from_repo(repo, name=SCMProvider.generate_name(workflow_db.workflow_id))
    if repo.token is not None or not isinstance(repo, GitHubRepository):
        background_tasks.add_task(upload_scm_file, scm=SCM([scm_provider]), scm_file_id=workflow_db.workflow_id)

    trace.get_current_span().set_attribute("workflow_id", str(workflow_db.workflow_id))
    background_tasks.add_task(send_new_workflow_email, workflow=workflow_db, version=workflow_db.versions[0])
    return WorkflowOut.from_db_workflow(workflow_db)


@router.get("", status_code=status.HTTP_200_OK, summary="List workflows", response_model_exclude_none=True)
@start_as_current_span_async("api_list_workflow", tracer=tracer)
async def list_workflows(
    crud_workflow: CrudWorkflowSession,
    authorization: Authorization,
    current_user: CurrentUser,
    name_substring: Annotated[
        str | None,
        Query(
            min_length=3,
            max_length=30,
            description="Filter workflows by a substring in their name.",
            examples=["blast"],
        ),
    ] = None,
    version_status: Annotated[
        list[WorkflowVersion.WorkflowVersionStatus] | None,
        Query(
            description=f"Which versions of the workflow to include in the response. Permission `workflow:list_filter` required, unless `developer_id` is provided and current user is developer, then only permission `workflow:list` required. Default `{WorkflowVersion.WorkflowVersionStatus.PUBLISHED.name}` and `{WorkflowVersion.WorkflowVersionStatus.DEPRECATED.name}`.",  # noqa: E501
        ),
    ] = None,
    developer_id: Annotated[
        UUID | None,
        Query(
            description="Filter for workflow by developer. If current user is the developer, permission `workflow:list` required, otherwise `workflow:list_filter`.",  # noqa: E501
            examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
        ),
    ] = None,
) -> list[WorkflowOut]:
    """
    List all workflows.

    Permission `workflow:list` required.
    \f
    Parameters
    ----------
    crud_workflow : clowm.crud.crud_workflow.CRUDWorkflow
        Active CRUD repository for workflows. Dependency Injection.
    developer_id : str | None, default None
        Filter workflows by a developer. Query Parameter.
    name_substring : string | None, default None
        Filter workflows by a substring in their name. Query Parameter.
    version_status : list[clowm.models.WorkflowVersion.Status] | None, default None
        Status of Workflow versions to filter for to fetch. Query Parameter.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency injection.

    Returns
    -------
    workflows : list[clowm.schemas.workflow.WorkflowOut]
        Workflows in the system
    """
    current_span = trace.get_current_span()
    if developer_id is not None:  # pragma: no cover
        current_span.set_attribute("developer_id", str(developer_id))
    if name_substring is not None:  # pragma: no cover
        current_span.set_attribute("name_substring", name_substring)
    if version_status is not None and len(version_status) > 0:  # pragma: no cover
        current_span.set_attribute("version_status", [stat.name for stat in version_status])
    rbac_operation = RBACOperation.LIST
    if (
        developer_id is not None
        and current_user.uid != developer_id
        or version_status is not None
        and developer_id is None
    ):
        rbac_operation = RBACOperation.LIST_ALL

    authorization(rbac_operation)
    workflows = await crud_workflow.list_workflows(
        name_substring=name_substring,
        developer_id=developer_id,
        version_status=(
            [WorkflowVersion.WorkflowVersionStatus.PUBLISHED, WorkflowVersion.WorkflowVersionStatus.DEPRECATED]
            if version_status is None
            else version_status
        ),
    )
    return [WorkflowOut.from_db_workflow(workflow, versions=workflow.versions) for workflow in workflows]


@router.get("/ownership_transfer_request", summary="List workflow OTRs")
@start_as_current_span_async("api_workflow_list_otr", tracer=tracer)
async def list_workflow_otrs(
    authorization: Authorization,
    crud_workflow: CrudWorkflowSession,
    current_user: CurrentUser,
    current_owner_id: Annotated[
        UUID | None,
        Query(
            description="UID of user who is the current owner.",
            examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
        ),
    ] = None,
    new_owner_id: Annotated[
        UUID | None,
        Query(
            description="UID of user who will be the new owner.",
            examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
        ),
    ] = None,
) -> list[OwnershipTransferRequestOut]:
    """
    Get the ownership transfer requests for workflows.

    Permission `workflow:list` required if `current_owner_id` or `new_owner_id` is the current users id,
    otherwise `workflow:list_all`
    \f
    Parameters
    ----------
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    crud_workflow : clowm.crud.crud_workflow.CRUDWorkflow
        Active CRUD repository for workflows. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    current_owner_id : uuid.UUID
        ID of user who is the current owner. Query Parameter.
    new_owner_id : uuid.UUID
        ID of user who will be the new owner. Query Parameter.

    Returns
    -------
    otrs : list[clowm.schemas.ownership_transfer.OwnershipTransferRequestOut]
        List of workflow OTRs
    """
    return await list_otrs(
        current_user=current_user,
        authorization=authorization,
        current_owner_id=current_owner_id,
        new_owner_id=new_owner_id,
        crud=crud_workflow,
    )


@router.get(
    "/developer_statistics",
    status_code=status.HTTP_200_OK,
    summary="Get anonymized workflow execution",
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_get_workflow_developer_statistics", tracer=tracer)
async def get_developer_workflow_statistics(
    crud_workflow: CrudWorkflowSession,
    authorization: Authorization,
    response: Response,
    current_user: CurrentUser,
    developer_id: Annotated[
        UUID | None,
        Query(
            description="Filter by the developer of the workflows",
            examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
        ),
    ] = None,
    workflow_ids: Annotated[list[UUID] | None, Query(description="Filter by workflow IDs", alias="workflow_id")] = None,
    start: Annotated[date | None, Query(description="Filter by workflow executions after this date")] = None,
    end: Annotated[date | None, Query(description="Filter by workflow executions before this date")] = None,
) -> list[AnonymizedWorkflowExecution]:
    """
    Get the workflow executions with meta information and anonymized user IDs.

    Permission `workflow:read` required if the `developer_id` is the same as the uid of the current user,
    other `workflow:read_any`.
    \f
    Parameters
    ----------
    crud_workflow : clowm.crud.crud_workflow.CRUDWorkflow
        Active CRUD repository for workflows. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    response : fastapi.Response
        Temporary Response object. Dependency Injection.
    current_user : clowm.models.User
        Current user.  Dependency Injection.
    developer_id : str | None, default None
        Filter for workflows developed by a specific user. Query Parameter.
    workflow_ids: list[uuid.UUID] | None, default None
        Filter by workflow IDs. Query Parameter.
    start : datetime.date | None, default None
        Filter by executions that started after the specified date. Query Parameter.
    end : datetime.date | None, default None
        Filter by executions that started before the specified date. Query Parameter.

    Returns
    -------
    statistics : list[clowm.schema.workflow.AnonymizedWorkflowExecution]
        List of raw datapoints for analysis.
    """
    span = trace.get_current_span()
    if developer_id:  # pragma: no cover
        span.set_attribute("developer_id", str(developer_id))
    if workflow_ids:  # pragma: no cover
        span.set_attribute("workflow_ids", [str(wid) for wid in workflow_ids])
    if start:  # pragma: no cover
        span.set_attribute("start_day", start.isoformat())
    if end:  # pragma: no cover
        span.set_attribute("end_day", end.isoformat())
    authorization(RBACOperation.READ if current_user.uid == developer_id else RBACOperation.READ_ANY)
    # Instruct client to cache response for 1 hour
    response.headers["Cache-Control"] = "max-age=3600"
    return await crud_workflow.developer_statistics(
        developer_id=developer_id, workflow_ids=workflow_ids, start=start, end=end
    )


@router.get("/{wid}", status_code=status.HTTP_200_OK, summary="Get a workflow", response_model_exclude_none=True)
@start_as_current_span_async("api_get_workflow", tracer=tracer)
async def get_workflow(
    workflow: CurrentWorkflow,
    current_user: CurrentUser,
    authorization: Authorization,
    version_status: Annotated[
        list[WorkflowVersion.WorkflowVersionStatus] | None,
        Query(
            description=f"Which versions of the workflow to include in the response. Permission `workflow:read_any` required if you are not the developer of this workflow. Default `{WorkflowVersion.WorkflowVersionStatus.PUBLISHED.name}` and `{WorkflowVersion.WorkflowVersionStatus.DEPRECATED.name}`",  # noqa: E501
        ),
    ] = None,
) -> WorkflowOut:
    """
    Get a specific workflow.

    Permission `workflow:read` required.
    \f
    Parameters
    ----------
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    version_status : list[clowm.models.WorkflowVersion.Status] | None, default None
        Status of Workflow versions to filter for to fetch. Query Parameter
    current_user : clowm.models.User
        Current user.  Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.

    Returns
    -------
    workflow : clowm.schemas.workflow.WorkflowOut
        Workflow with existing ID
    """
    current_span = trace.get_current_span()
    current_span.set_attribute("workflow_id", str(workflow.workflow_id))
    if version_status is not None and len(version_status) > 0:  # pragma: no cover
        current_span.set_attribute("version_status", [stat.name for stat in version_status])
    authorization(
        RBACOperation.READ
        if workflow.developer_id == current_user.uid and version_status is not None
        else RBACOperation.READ_ANY
    )
    version_stat = (
        [WorkflowVersion.WorkflowVersionStatus.PUBLISHED, WorkflowVersion.WorkflowVersionStatus.DEPRECATED]
        if version_status is None
        else version_status
    )
    return WorkflowOut.from_db_workflow(
        workflow, versions=[version for version in workflow.versions if version.status in version_stat]
    )


@router.get("/{wid}/ownership_transfer_request", summary="Get a workflow OTR")
@start_as_current_span_async("api_get_workflow_otr", tracer=tracer)
async def get_workflow_otr(
    workflow: CurrentWorkflow, current_user: CurrentUser, authorization: Authorization
) -> OwnershipTransferRequestOut:
    """
    Get a specific workflow ownership transfer request.

    Permission `workflow:read` required if current user is the current or new owner of the workflow,
    otherwise `workflow:read_any` required.
    \f
    Parameters
    ----------
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    current_user : clowm.models.User
        Current user.  Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.

    Returns
    -------
    otr : clowm.schemas.ownership_transfer.OwnershipTransferRequestOut
        Workflow OTR if it exists
    """
    trace.get_current_span().set_attribute("workflow_id", str(workflow.workflow_id))
    return get_otr(target=workflow, current_user=current_user, authorization=authorization)


@router.post("/{wid}/ownership_transfer_request", status_code=status.HTTP_201_CREATED, summary="Create a workflow OTR")
@start_as_current_span_async("api_create_workflow_otr", tracer=tracer)
async def create_workflow_otr(
    workflow: CurrentWorkflow,
    current_user: CurrentUser,
    authorization: Authorization,
    otr_in: Annotated[OwnershipTransferRequestIn, Body(description="Meta-data for workflow OTR to create")],
    crud_workflow: CrudWorkflowSession,
    background_tasks: BackgroundTasks,
) -> OwnershipTransferRequestOut:
    """
    Create a ownership transfer request for a specific workflow.

    Permission `workflow:update` required if the current user is the current owner of the workflow,
    otherwise `workflow:update_any` required.
    \f
    Parameters
    ----------
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    otr_in : clowm.schemas.ownership_transfer.OwnershipTransferRequestIn
        Infos for the OTR. HTTP Body.
    crud_workflow : clowm.crud.crud_workflow.CRUDWorkflow
        Active CRUD repository for workflows. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.

    Returns
    -------
    otr : clowm.schemas.ownership_transfer.OwnershipTransferRequestOut
        Bucket OTR if it exists
    """
    trace.get_current_span().set_attribute("workflow_id", str(workflow.workflow_id))
    otr = await create_otr(
        target=workflow, current_user=current_user, authorization=authorization, otr_info=otr_in, crud=crud_workflow
    )
    background_tasks.add_task(send_otr_created_email, otr=otr)
    return otr


@router.patch("/{wid}/ownership_transfer_request", status_code=status.HTTP_200_OK, summary="Accept a workflow OTR")
@start_as_current_span_async("api_accept_workflow_otr", tracer=tracer)
async def accept_workflow_otr(
    workflow: CurrentWorkflow,
    current_user: CurrentUser,
    authorization: Authorization,
    crud_workflow: CrudWorkflowSession,
    background_tasks: BackgroundTasks,
) -> WorkflowOut:
    """
    Accept an ownership transfer request for a specific workflow.

    Permission `workflow:update` required if the current user is the new owner of the workflow,
    otherwise `workflow:update_any` required.
    \f
    Parameters
    ----------
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    crud_workflow : clowm.crud.crud_workflow.CRUDWorkflow
        Active CRUD repository for workflows. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.

    Returns
    -------
    workflow : clowm.schemas.workflow.WorkflowOut
        The workflow with the new developer id
    """
    trace.get_current_span().set_attribute("workflow_id", str(workflow.workflow_id))
    old_owner_uid = workflow.developer_id
    new_owner_uid = workflow.transfer_new_owner_uid
    await accept_otr(target=workflow, current_user=current_user, authorization=authorization, crud=crud_workflow)
    background_tasks.add_task(
        send_otr_accepted_email,
        target_name=workflow.name,
        target_type=Workflow,
        new_owner_uid=new_owner_uid,  # type: ignore[arg-type]
        old_owner_uid=old_owner_uid,
    )
    await crud_workflow.db.refresh(workflow, ["developer_id"])
    return WorkflowOut.from_db_workflow(workflow)


@router.delete(
    "/{wid}/ownership_transfer_request", status_code=status.HTTP_204_NO_CONTENT, summary="Delete a workflow OTR"
)
@start_as_current_span_async("api_delete_workflow_otr", tracer=tracer)
async def delete_workflow_otr(
    workflow: CurrentWorkflow,
    current_user: CurrentUser,
    authorization: Authorization,
    crud_workflow: CrudWorkflowSession,
    background_tasks: BackgroundTasks,
) -> None:
    """
    Delete/Reject a workflow ownership transfer request.

    Permission `workflow:update` required if current user is the current or new owner of the workflow,
    otherwise `workflow:update_any` required.
    \f
    Parameters
    ----------
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    current_user : clowm.models.User
        Current user.  Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    crud_workflow : clowm.crud.crud_workflow.CRUDWorkflow
        Active CRUD repository for workflows. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    """
    trace.get_current_span().set_attribute("workflow_id", str(workflow.workflow_id))
    old_owner_uid = workflow.developer_id
    new_owner_uid = workflow.transfer_new_owner_uid
    await delete_otr(target=workflow, current_user=current_user, authorization=authorization, crud=crud_workflow)
    if current_user.uid == new_owner_uid:
        background_tasks.add_task(
            send_otr_rejected_email,
            target_name=workflow.name,
            target_type=Workflow,
            new_owner_uid=new_owner_uid,
            owner_uid=old_owner_uid,
        )
    else:
        background_tasks.add_task(
            send_otr_deleted_email,
            target_name=workflow.name,
            target_type=Workflow,
            new_owner_uid=new_owner_uid,  # type: ignore[arg-type]
        )


@router.get("/{wid}/statistics", status_code=status.HTTP_200_OK, summary="Get statistics for a workflow")
@start_as_current_span_async("api_workflow_get_statistics", tracer=tracer)
async def get_workflow_statistics(
    workflow: CurrentWorkflow, crud_workflow: CrudWorkflowSession, authorization: Authorization, response: Response
) -> list[WorkflowStatistic]:
    """
    Get the number of started workflow per day.

    Permission `workflow:read` required.
    \f
    Parameters
    ----------
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    crud_workflow : clowm.crud.crud_workflow.CRUDWorkflow
        Active CRUD repository for workflows. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    response : fastapi.Response
        Temporary Response object. Dependency Injection.

    Returns
    -------
    statistics : list[clowm.schema.workflow.WorkflowStatistic]
        List of datapoints aggregated by day.
    """
    trace.get_current_span().set_attribute("workflow_id", str(workflow.workflow_id))
    authorization(RBACOperation.READ)
    # Instruct client to cache response for 24 hour
    response.headers["Cache-Control"] = "max-age=86400"
    return await crud_workflow.statistics(workflow.workflow_id)


@router.delete("/{wid}", status_code=status.HTTP_204_NO_CONTENT, summary="Delete a workflow")
@start_as_current_span_async("api_workflow_delete", tracer=tracer)
async def delete_workflow(
    workflow: CurrentWorkflow,
    authorization: Authorization,
    current_user: CurrentUser,
    workflow_core: WorkflowCoreDep,
) -> None:
    """
    Delete a workflow.

    Permission `workflow:delete` required if the `developer_id` is the same as the uid of the current user,
    other `workflow:delete_any`.
    \f
    Parameters
    ----------
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    workflow_core : clowm.api.core.WorkflowCore
        Core functionality for workflow API. Dependency Injection.
    """
    trace.get_current_span().set_attribute("workflow_id", str(workflow.workflow_id))
    authorization(RBACOperation.DELETE if workflow.developer_id == current_user.uid else RBACOperation.DELETE_ANY)
    await workflow_core.delete(workflow)


@router.post(
    "/{wid}/update", status_code=status.HTTP_201_CREATED, summary="Update a workflow", response_model_exclude_none=True
)
@start_as_current_span_async("api_workflow_update", tracer=tracer)
async def update_workflow(
    workflow: CurrentWorkflow,
    current_user: CurrentUser,
    authorization: Authorization,
    workflow_core: WorkflowCoreDep,
    version_update: Annotated[WorkflowUpdate, Body(description="Meta-data for the workflow version to create")],
) -> WorkflowVersionOut:
    """
    Create a new workflow version.

    Permission `workflow:update` required.
    \f
    Parameters
    ----------
    workflow : clowm.models.Workflow
        Workflow with given ID. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    version_update : clowm.schemas.workflow
        Data about the new workflow version. HTML Body.
    workflow_core : clowm.api.core.WorkflowCore
        Core functionality for workflow API. Dependency Injection.

    Returns
    -------
    version : clowm.schemas.workflow_version.WorkflowVersionOut
        The new workflow version
    """
    trace.get_current_span().set_attribute("workflow_id", str(workflow.workflow_id))
    authorization(RBACOperation.UPDATE if current_user.uid == workflow.developer_id else RBACOperation.UPDATE_ANY)
    return await workflow_core.update(version_update, workflow)
