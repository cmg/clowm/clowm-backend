from typing import Any

from fastapi import APIRouter, Depends, HTTPException, Response
from fastapi.openapi.docs import get_swagger_ui_html
from fastapi.openapi.utils import get_openapi
from fastapi.responses import HTMLResponse, JSONResponse
from fastapi.routing import APIRoute

from clowm.core.config import settings

from ..dependencies.website.auth import get_cookie_jwt_user
from . import (
    api_token,
    bucket,
    bucket_permission,
    login,
    news,
    resource,
    resource_version,
    s3key,
    user,
    workflow,
    workflow_credentials,
    workflow_execution,
    workflow_mode,
    workflow_version,
)
from .public import router as public_router

__all__ = ["ui_router"]


def custom_generate_unique_id(route: APIRoute) -> str:
    return f"{route.tags[-1]}-{route.name}"


ui_router = APIRouter(generate_unique_id_function=custom_generate_unique_id)
ui_router.include_router(login.router)
for endpoint in [
    api_token,
    user,
    s3key,
    bucket,
    bucket_permission,
    workflow,
    workflow_version,
    workflow_execution,
    workflow_credentials,
    workflow_mode,
    resource,
    resource_version,
    news,
]:
    ui_router.include_router(
        endpoint.router,
        prefix="/ui",
        dependencies=[Depends(get_cookie_jwt_user)],
    )

ui_router.include_router(public_router, prefix="/ui")

schema: dict[str, Any] | None = None


@ui_router.get(
    "/ui/openapi.json", include_in_schema=False, tags=["miscellaneous"], dependencies=[Depends(get_cookie_jwt_user)]
)
async def openapi_route() -> Response:
    global schema, ui_router
    if not settings.dev_system:
        raise HTTPException(status_code=404)
    if schema is None:
        schema = get_openapi(
            title="CloWM UI API",
            version="1.0",
            description="This is the API for the CloWM UI. It is only for internal use and will change without prior notice.",
            servers=[{"url": settings.api_prefix}] if settings.api_prefix else None,
            routes=ui_router.routes,
        )
    return JSONResponse(schema)


@ui_router.get("/ui/docs", include_in_schema=False, dependencies=[Depends(get_cookie_jwt_user)], tags=["miscellaneous"])
async def swagger_ui_html() -> HTMLResponse:
    if not settings.dev_system:
        raise HTTPException(status_code=404)
    return get_swagger_ui_html(
        openapi_url=(settings.api_prefix if settings.api_prefix else "") + ui_router.prefix + "/ui/openapi.json",
        title="CloWM - Swagger UI",
        swagger_favicon_url="/favicon.ico",
    )
