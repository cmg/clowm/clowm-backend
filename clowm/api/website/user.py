from asyncio import TaskGroup
from typing import Annotated

from fastapi import APIRouter, BackgroundTasks, Body, Depends, HTTPException, Query, Response, status
from opentelemetry import trace
from pydantic import TypeAdapter

from clowm.api.dependencies.website import AuthorizationDependencyCookieUser as AuthorizationDependency
from clowm.api.dependencies.website import CurrentUserCookie as CurrentUser
from clowm.api.dependencies.website.auth import cookie_jwt_scheme
from clowm.core.oidc import OIDCProvider, UserInfo
from clowm.core.rbac import RBACOperation, RBACResource
from clowm.crud import CRUDBucketPermission, CRUDUser, CRUDWorkflowExecution
from clowm.models import Role, User
from clowm.otlp import start_as_current_span_async
from clowm.schemas.user import UserIn, UserOut, UserOutExtended, UserRoles
from clowm.smtp.send_email import send_invitation_email, send_user_deleted_email

from ..core import BucketPermissionCore
from ..dependencies import CRUDSessionDep, PathUser, RGWService, S3Service
from ..dependencies.auth import AuthorizationFunction

router = APIRouter(prefix="/users", tags=["User"])

user_authorization = AuthorizationDependency(resource=RBACResource.USER)
Authorization = Annotated[AuthorizationFunction, Depends(user_authorization)]
CrudUserSession = Annotated[CRUDUser, Depends(CRUDSessionDep(CRUDUser))]

tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.post("", status_code=status.HTTP_201_CREATED)
@start_as_current_span_async("api_invite_user", tracer=tracer)
async def create_user(
    crud_user: CrudUserSession,
    user_in: Annotated[UserIn, Body(description="Meta-data for user to create")],
    background_tasks: BackgroundTasks,
    authorization: Authorization,
) -> UserOutExtended:
    """
    Create a new user in the system and notify him.

    Permission `user:create` required.
    \f
    Parameters
    ----------
    crud_user : clowm.crud.crud_user.CRUDUser
        Active CRUD repository for users. Dependency Injection.
    user_in : clowm.schemas.users.UserIn
        Parameters of the user that should be created. HTTP Body.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.

    Returns
    -------
    user : clowm.schemas.user.UserOutExtended
        The newly created user.
    """
    current_span = trace.get_current_span()
    current_span.set_attribute("user_in", user_in.model_dump_json())
    authorization(RBACOperation.CREATE)
    user = await crud_user.create(
        UserInfo(name=user_in.display_name, email=str(user_in.email), sub="", provider=OIDCProvider.lifescience),
        roles=user_in.roles,
    )
    current_span.set_attribute("uid", str(user.uid))
    token = await crud_user.create_invitation_token(user.uid)
    background_tasks.add_task(send_invitation_email, user=user, token=token)
    await crud_user.db.refresh(user, attribute_names=["invitation_token_created_at"])
    return UserOutExtended.from_db_user(user)


@router.get("/search")
@start_as_current_span_async("api_search_users", tracer=tracer)
async def search_users(
    crud_user: CrudUserSession,
    authorization: Authorization,
    name_substring: Annotated[
        str,
        Query(
            min_length=3,
            max_length=30,
            description="Filter users by a substring in their name.",
        ),
    ],
) -> list[UserOut]:
    """
    Search for users in the system by their name.

    Permission `user: search` required.
    \f
    Parameters
    ----------
    crud_user : clowm.crud.crud_user.CRUDUser
        Active CRUD repository for users. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    name_substring : string
        Filter users by a substring in their name. Query Parameter.

    Returns
    -------
    user : list[clowm.models.User]
        List with the given substring in its name.
    """
    trace.get_current_span().set_attribute("name_substring", name_substring)
    authorization(RBACOperation.SEARCH)
    users = await crud_user.list_users(name_substring=name_substring)
    return TypeAdapter(list[UserOut]).validate_python(users)


@router.get("/me", summary="Get the logged in user")
@start_as_current_span_async("api_get_logged_in_user", tracer=tracer)
async def get_logged_in_user(current_user: CurrentUser, authorization: Authorization) -> UserOutExtended:
    """
    Return the user associated with the used JWT.

    Permission `user:read` required.
    \f
    Parameters
    ----------
    current_user : clowm.models.User
        User from the database associated to the used JWT. Dependency injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.

    Returns
    -------
    current_user : clowm.schemas.user.UserOutExtended
        User associated to used JWT.
    """
    trace.get_current_span().set_attribute("uid", str(current_user.uid))
    authorization(RBACOperation.READ)
    return UserOutExtended.from_db_user(current_user)


@router.get("", summary="List users and search by their name")
@start_as_current_span_async("api_list_users", tracer=tracer)
async def list_users(
    crud_user: CrudUserSession,
    authorization: Authorization,
    name_substring: Annotated[
        str | None,
        Query(
            min_length=3, max_length=30, description="Filter users by a substring in their name.", examples=["Bilbo"]
        ),
    ] = None,
    filter_roles: Annotated[
        list[Role.RoleEnum] | None,
        Query(
            description="Filter users by their role. If multiple are selected, they are concatenating by an OR Expression."  # noqa:E501
        ),
    ] = None,
) -> list[UserOutExtended]:
    """
    List all users in the system..

    Permission `user:list` required.
    \f
    Parameters
    ----------
    name_substring : string | None, default None
        Filter users by a substring in their name. Query Parameter.
    filter_roles : list[clowm.core.authorization.RoleEnum] | None, default None
        Filter users by their role. Query Parameter.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    crud_user : clowm.crud.crud_user.CRUDUser
        Active CRUD repository for users. Dependency Injection.

    Returns
    -------
    users: list[clowm.schemas.user.UserOutExtended]
        List of all the users with the given filter.
    """
    current_span = trace.get_current_span()
    if name_substring is not None:  # pragma: no cover
        current_span.set_attribute("name_substring", name_substring)
    if filter_roles is not None and len(filter_roles) > 0:  # pragma: no cover
        current_span.set_attribute("filter_roles", [role.name for role in filter_roles])

    authorization(RBACOperation.LIST)

    users = await crud_user.list_users(name_substring=name_substring, roles=filter_roles)
    return [UserOutExtended.from_db_user(user) for user in users]


@router.get("/{uid}", response_model=UserOut, summary="Get a user by its uid")
@start_as_current_span_async("api_get_user", tracer=tracer)
async def get_user(authorization: Authorization, user: PathUser) -> User:
    """
    Return the user with the specific uid.

    Permission `user:read` required.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    user : clowm.models.User
        The user associated with the UID in the path. Dependency Injection.

    Returns
    -------
    user : clowm.models.User
        User with given uid.
    """
    trace.get_current_span().set_attributes({"uid": str(user.uid)})
    authorization(RBACOperation.READ)
    return user


@router.delete("/{uid}", summary="Delete a user", status_code=status.HTTP_204_NO_CONTENT)
@start_as_current_span_async("api_delete_user", tracer=tracer)
async def delete_user(
    authorization: Authorization,
    user: PathUser,
    current_user: CurrentUser,
    crud_user: CrudUserSession,
    rgw: RGWService,
    s3: S3Service,
    response: Response,
    background_tasks: BackgroundTasks,
) -> None:
    """
    Delete a user and his S3 account.

    Permission `user:delete` required if the current user is the target user.
    \f
    Parameters
    ----------
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    user : clowm.models.User
        The user associated with the UID in the path. Dependency Injection.
    crud_user : clowm.crud.crud_user.CRUDUser
        Active CRUD repository for users. Dependency Injection.
    current_user : clowm.models.User
        User from the database associated to the used JWT. Dependency injection.
    s3 : types_aiobotocore_s3.service_resource.S3ServiceResource
        S3 Service to perform operations on buckets. Dependency Injection.
    rgw : rgwadmin.RGWAdmin
        RGW admin interface to manage Ceph's object store. Dependency Injection.
    response : fastapi.Response
        Raw response object.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    """
    trace.get_current_span().set_attributes({"uid": str(user.uid)})
    authorization(RBACOperation.DELETE if current_user == user else RBACOperation.DELETE_ANY)
    with tracer.start_as_current_span("delete_rgw_user", attributes={"uid": str(user.uid)}):
        rgw.remove_user(uid=str(user.uid), purge_data=False)
    permissions = await CRUDBucketPermission(db=crud_user.db).list(uid=user.uid)
    await crud_user.delete(user.uid)
    await CRUDWorkflowExecution(db=crud_user.db).delete_by_user(user.uid)
    permission_core = BucketPermissionCore(CRUDBucketPermission(db=crud_user.db), s3=s3)
    async with TaskGroup() as tg:
        for permission in permissions:
            tg.create_task(permission_core.delete_permission(permission, delete_db=False))
    background_tasks.add_task(send_user_deleted_email, user=user, own_account=current_user == user)
    if current_user == user:
        response.delete_cookie(key=cookie_jwt_scheme.model.name)


@router.put("/{uid}/roles", summary="Update user roles")
async def update_roles(
    crud_user: CrudUserSession,
    user: PathUser,
    authorization: Authorization,
    roles_body: Annotated[UserRoles, Body(description="The new roles of the user")],
) -> UserOutExtended:
    """
    Update the roles of a user.

    Permission `user:update` required.
    \f
    Parameters
    ----------
    crud_user : clowm.crud.crud_user.CRUDUser
        Active CRUD repository for users. Dependency Injection.
    user : clowm.models.User
        The user associated with the UID in the path. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    roles_body: clowm.schemas.users.UserRoles
        The new roles of the user. HTTP Body.

    Returns
    -------
    user : clowm.schemas.user.UserOutExtended
        User with the updated roles.
    """
    trace.get_current_span().set_attributes({"uid": str(user.uid), "roles": [role.name for role in roles_body.roles]})
    authorization(RBACOperation.UPDATE)
    await crud_user.update_roles(user, roles_body.roles)
    return UserOutExtended.from_db_user(user)


@router.patch("/{uid}/invitation")
@start_as_current_span_async("api_resend_invitation", tracer=tracer)
async def resend_invitation(
    crud_user: CrudUserSession, user: PathUser, authorization: Authorization, background_tasks: BackgroundTasks
) -> UserOutExtended:
    """
    Resend the invitation link for an user that has an open invitation.

    Permission `user:create` required.
    \f
    Parameters
    ----------
    crud_user : clowm.crud.crud_user.CRUDUser
        Active CRUD repository for users. Dependency Injection.
    user : clowm.models.User
        The user associated with the UID in the path. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.

    Returns
    -------
    user : clowm.schemas.user.UserOutExtended
        User with the updated roles.
    """
    authorization(RBACOperation.CREATE)
    if user.invitation_token is None:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, detail=f"user {user.uid} has not open invitation")
    token = await crud_user.create_invitation_token(user.uid)
    await crud_user.db.refresh(user, attribute_names=["invitation_token_created_at"])
    background_tasks.add_task(send_invitation_email, user=user, token=token)
    return UserOutExtended.from_db_user(user)
