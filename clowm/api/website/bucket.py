from collections.abc import Iterable
from typing import Annotated
from uuid import UUID

from fastapi import APIRouter, BackgroundTasks, Body, Depends, Query, status
from opentelemetry import trace
from pydantic import ByteSize

from clowm.api.dependencies.website import AuthorizationDependencyCookieUser as AuthorizationDependency
from clowm.api.dependencies.website import CurrentUserCookie as CurrentUser
from clowm.ceph.rgw import update_bucket_limits as rgw_update_bucket_limits
from clowm.ceph.s3 import (
    add_s3_bucket_policy_stmt,
    delete_s3_bucket_policy_stmt,
    get_base_policy,
)
from clowm.core.rbac import RBACOperation, RBACResource
from clowm.crud import CRUDBucket, CRUDBucketPermission
from clowm.models import Bucket
from clowm.otlp import start_as_current_span_async
from clowm.schemas.bucket import BucketIn as BucketInSchema
from clowm.schemas.bucket import BucketOut as BucketOutSchema
from clowm.schemas.bucket import BucketSizeLimits
from clowm.schemas.ownership_transfer import OwnershipTransferRequestIn, OwnershipTransferRequestOut

from ...schemas.bucket_permission import BucketPermissionOut
from ...smtp.send_email import (
    send_otr_accepted_email,
    send_otr_created_email,
    send_otr_deleted_email,
    send_otr_rejected_email,
)
from ..core import BucketCoreDep
from ..dependencies import CRUDSessionDep, CurrentBucket, RGWService, S3Service
from ..dependencies.auth import AuthorizationFunction
from ..otr_utils import accept_otr, create_otr, delete_otr, get_otr, list_otrs

router = APIRouter(prefix="/buckets", tags=["Bucket"])
bucket_authorization = AuthorizationDependency(resource=RBACResource.BUCKET)
Authorization = Annotated[AuthorizationFunction, Depends(bucket_authorization)]
CrudBucketSession = Annotated[CRUDBucket, Depends(CRUDSessionDep(CRUDBucket))]
tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.get("", response_model=list[BucketOutSchema], summary="List buckets")
@start_as_current_span_async("api_list_buckets", tracer=tracer)
async def list_buckets(
    current_user: CurrentUser,
    authorization: Authorization,
    bucket_core: BucketCoreDep,
    owner_id: Annotated[
        UUID | None,
        Query(
            description="UID of the user for whom to fetch the buckets for. Permission `bucket:read_any` required if current user is not the target.",
            # noqa:E501
            examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
        ),
    ] = None,
    bucket_type: Annotated[
        CRUDBucket.BucketType, Query(description="Type of the bucket to get. Ignored when `user` parameter not set")
    ] = CRUDBucket.BucketType.ALL,
) -> Iterable[Bucket]:
    """
    List all the buckets in the system or of the desired user where the user has permissions for.

    Permission `bucket:list_all` required. See parameter `owner_id` for exception.
    \f
    Parameters
    ----------
    owner_id : uuid.UUID
        User for which to retrieve the buckets. Query Parameter.
    bucket_type : clowm.crud.crud_bucket.CRUDBucket.BucketType, default BucketType.ALL
        Type of the bucket to get. Query Parameter.
    bucket_core : clowm.api.core.BucketCore
        Core functionality for bucket API. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.

    Returns
    -------
    buckets : list[clowm.models.Bucket]
        All the buckets for which the user has READ permissions.
    """
    current_span = trace.get_current_span()
    if owner_id is not None:  # pragma: no cover
        current_span.set_attribute("owner_id", str(owner_id))
    current_span.set_attribute("bucket_type", bucket_type.name)
    authorization(RBACOperation.LIST if current_user.uid == owner_id else RBACOperation.LIST_ALL)
    return await bucket_core.list_buckets(owner_id=owner_id, bucket_type=bucket_type)


@router.post(
    "",
    response_model=BucketOutSchema,
    status_code=status.HTTP_201_CREATED,
    summary="Create a bucket for the current user",
)
@start_as_current_span_async("api_create_bucket", tracer=tracer)
async def create_bucket(
    bucket: Annotated[BucketInSchema, Body(description="Meta-data for bucket to create")],
    current_user: CurrentUser,
    authorization: Authorization,
    bucket_core: BucketCoreDep,
) -> Bucket:
    """
    Create a bucket for the current user.\n
    The name of the bucket has some constraints.
    For more information see the
    [Ceph documentation](https://docs.ceph.com/en/quincy/radosgw/s3/bucketops/#constraints)

    Permission `bucket:create` required.
    \f
    Parameters
    ----------
    bucket : clowm.schemas.bucket.BucketIn
        Information about the bucket to create. HTTP Body
    current_user : clowm.models.User
        Current user. Dependency Injection.
    bucket_core : clowm.api.core.BucketCore
        Core functionality for bucket API. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.

    Returns
    -------
    bucket : clowm.models.Bucket
        The newly created bucket.
    """
    current_span = trace.get_current_span()
    current_span.set_attribute("bucket_name", bucket.name)
    authorization(RBACOperation.CREATE)
    return await bucket_core.create_bucket(bucket=bucket, owner_id=current_user.uid)


@router.get("/ownership_transfer_request", summary="List bucket OTRs")
@start_as_current_span_async("api_list_bucket_otr", tracer=tracer)
async def list_bucket_otrs(
    crud_bucket: CrudBucketSession,
    current_user: CurrentUser,
    authorization: Authorization,
    current_owner_id: Annotated[
        UUID | None,
        Query(
            description="UID of user who is the current owner.",
            examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
        ),
    ] = None,
    new_owner_id: Annotated[
        UUID | None,
        Query(
            description="UID of user who will be the new owner.",
            examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
        ),
    ] = None,
) -> list[OwnershipTransferRequestOut]:
    """
    Get the ownership transfer requests for buckets.

    Permission `bucket:list` required if `current_owner_id` or `new_owner_id` is the current users id,
    otherwise `bucket:list_all`
    \f
    Parameters
    ----------
    current_owner_id : uuid.UUID
        ID of user who is the current owner. Query Parameter.
    new_owner_id : uuid.UUID
        ID of user who will be the new owner. Query Parameter.
    crud_bucket : clowm.crud.crud_bucket.CRUDBucket
        Active CRUD repository for buckets. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.

    Returns
    -------
    otrs : list[clowm.schemas.ownership_transfer.OwnershipTransferRequestOut]
        List of bucket OTRs
    """
    return await list_otrs(
        current_user=current_user,
        authorization=authorization,
        current_owner_id=current_owner_id,
        new_owner_id=new_owner_id,
        crud=crud_bucket,
    )


@router.get("/{bucket_name}", response_model=BucketOutSchema, summary="Get a bucket by its name")
@start_as_current_span_async("api_get_bucket", tracer=tracer)
async def get_bucket(
    bucket: CurrentBucket,
    current_user: CurrentUser,
    authorization: Authorization,
    crud_bucket_permission: Annotated[CRUDBucketPermission, Depends(CRUDSessionDep(CRUDBucketPermission))],
) -> Bucket:
    """
    Get a bucket by its name if the current user has READ permissions for the bucket.

    Permission `bucket:read` required if the current user is the owner of the bucket,
    otherwise `bucket:read_any` required.
    \f
    Parameters
    ----------
    bucket : clowm.models.Bucket
        Bucket with the name provided in the URL path. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    crud_bucket_permission : clowm.crud.crud_bucket_permission.CRUDBucketPermission
        Active CRUD repository for bucket permissions. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.

    Returns
    -------
    bucket : clowm.models.Bucket
        Bucket with the provided name.
    """
    trace.get_current_span().set_attribute("bucket_name", bucket.name)
    rbac_operation = (
        RBACOperation.READ_ANY
        if not bucket.public and not await crud_bucket_permission.check_active_permission(bucket.name, current_user.uid)
        else RBACOperation.READ
    )
    authorization(rbac_operation)
    return bucket


@router.get("/{bucket_name}/ownership_transfer_request", summary="Get a bucket OTR")
@start_as_current_span_async("api_get_bucket_otr", tracer=tracer)
async def get_bucket_otr(
    bucket: CurrentBucket, current_user: CurrentUser, authorization: Authorization
) -> OwnershipTransferRequestOut:
    """
    Get a specific bucket ownership transfer request.

    Permission `bucket:read` required if the current user is the current or new owner of the bucket,
    otherwise `bucket:read_any` required.
    \f
    Parameters
    ----------
    bucket : clowm.models.Bucket
        Bucket with the name provided in the URL path. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.

    Returns
    -------
    otr : clowm.schemas.ownership_transfer.OwnershipTransferRequestOut
        Bucket OTR if it exists
    """
    trace.get_current_span().set_attribute("bucket_name", bucket.name)
    return get_otr(target=bucket, current_user=current_user, authorization=authorization)


@router.post(
    "/{bucket_name}/ownership_transfer_request", status_code=status.HTTP_201_CREATED, summary="Create a bucket OTR"
)
@start_as_current_span_async("api_create_bucket_otr", tracer=tracer)
async def create_bucket_otr(
    bucket: CurrentBucket,
    current_user: CurrentUser,
    authorization: Authorization,
    otr_in: Annotated[OwnershipTransferRequestIn, Body(description="Meta-data for the bucket OTR to create")],
    crud_bucket: CrudBucketSession,
    background_tasks: BackgroundTasks,
) -> OwnershipTransferRequestOut:
    """
    Create a ownership transfer request for a specific bucket.

    Permission `bucket:update` required if the current user is the current owner of the bucket,
    otherwise `bucket:update_any` required.
    \f
    Parameters
    ----------
    bucket : clowm.models.Bucket
        Bucket with the name provided in the URL path. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    otr_in : clowm.schemas.ownership_transfer.OwnershipTransferRequestIn
        Infos for the OTR. HTTP Body.
    crud_bucket : clowm.crud.crud_bucket.CRUDBucket
        Active CRUD repository for buckets. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.

    Returns
    -------
    otr : clowm.schemas.ownership_transfer.OwnershipTransferRequestOut
        Bucket OTR if it exists
    """
    trace.get_current_span().set_attribute("bucket_name", bucket.name)
    otr = await create_otr(
        target=bucket, current_user=current_user, authorization=authorization, otr_info=otr_in, crud=crud_bucket
    )
    background_tasks.add_task(send_otr_created_email, otr=otr)
    return otr


@router.patch(
    "/{bucket_name}/ownership_transfer_request",
    response_model=BucketOutSchema,
    status_code=status.HTTP_200_OK,
    summary="Accept a bucket OTR",
)
@start_as_current_span_async("api_accept_workflow_otr", tracer=tracer)
async def accept_bucket_otr(
    bucket: CurrentBucket,
    current_user: CurrentUser,
    authorization: Authorization,
    s3: S3Service,
    crud_bucket: CrudBucketSession,
    background_tasks: BackgroundTasks,
) -> Bucket:
    """
    Accept an ownership transfer request for a specific workflow.

    Permission `bucket:update` required if the current user is the new owner of the workflow,
    otherwise `bucket:update_any` required.
    \f
    Parameters
    ----------
    bucket : clowm.models.Bucket
        Bucket with the name provided in the URL path. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    crud_bucket : clowm.crud.crud_bucket.CRUDBucket
        Active CRUD repository for buckets. Dependency Injection.
    s3 : boto3_type_annotations.s3.ServiceResource
        S3 Service to perform operations on buckets in Ceph. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.

    Returns
    -------
    bucket : clowm.models.Bucket
        The bucket with the new owner id
    """
    trace.get_current_span().set_attribute("bucket_name", bucket.name)
    old_owner_uid = bucket.owner_id
    new_owner_uid = bucket.transfer_new_owner_uid
    await accept_otr(target=bucket, current_user=current_user, authorization=authorization, crud=crud_bucket)
    await crud_bucket.db.refresh(bucket, ["owner_id"])

    await CRUDBucketPermission(db=crud_bucket.db).delete(bucket_name=bucket.name, uid=bucket.owner_id)  # type: ignore[arg-type]

    new_policy_stmts = get_base_policy(bucket_name=bucket.name, uid=bucket.owner_id)  # type: ignore[arg-type]
    await delete_s3_bucket_policy_stmt(
        bucket_name=bucket.name,
        s3=s3,
        sid=[
            BucketPermissionOut(uid=bucket.owner_id, bucket_name=bucket.name, scopes=["read"]).to_hash(),
        ]
        + [stmt["Sid"] for stmt in new_policy_stmts],
    )
    await add_s3_bucket_policy_stmt(
        *new_policy_stmts,
        bucket_name=bucket.name,
        s3=s3,
    )
    background_tasks.add_task(
        send_otr_accepted_email,
        target_name=bucket.name,
        target_type=Bucket,
        new_owner_uid=new_owner_uid,  # type: ignore[arg-type]
        old_owner_uid=old_owner_uid,
    )
    return bucket


@router.delete(
    "/{bucket_name}/ownership_transfer_request", status_code=status.HTTP_204_NO_CONTENT, summary="Delete a bucket OTR"
)
@start_as_current_span_async("api_delete_bucket_otr", tracer=tracer)
async def delete_bucket_otr(
    bucket: CurrentBucket,
    current_user: CurrentUser,
    authorization: Authorization,
    crud_bucket: CrudBucketSession,
    background_tasks: BackgroundTasks,
) -> None:
    """
    Delete/Reject a bucket ownership transfer request.

    Permission `bucket:update` required if the current user is the current or new owner of the bucket,
    otherwise `bucket:update_any` required.
    \f
    Parameters
    ----------
    bucket : clowm.models.Bucket
        Bucket with the name provided in the URL path. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    crud_bucket : clowm.crud.crud_bucket.CRUDBucket
        Active CRUD repository for buckets. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    """
    trace.get_current_span().set_attribute("bucket_name", bucket.name)
    old_owner_uid = bucket.owner_id
    new_owner_uid = bucket.transfer_new_owner_uid
    await delete_otr(target=bucket, current_user=current_user, authorization=authorization, crud=crud_bucket)
    if current_user.uid == new_owner_uid:
        background_tasks.add_task(
            send_otr_rejected_email,
            target_name=bucket.name,
            target_type=Bucket,
            new_owner_uid=new_owner_uid,
            owner_uid=old_owner_uid,
        )
    else:
        background_tasks.add_task(
            send_otr_deleted_email,
            target_name=bucket.name,
            target_type=Bucket,
            new_owner_uid=new_owner_uid,  # type: ignore[arg-type]
        )


@router.patch("/{bucket_name}/public", response_model=BucketOutSchema, summary="Update public status")
@start_as_current_span_async("api_update_bucket_public_state", tracer=tracer)
async def update_bucket_public_state(
    bucket: CurrentBucket,
    current_user: CurrentUser,
    authorization: Authorization,
    bucket_core: BucketCoreDep,
    public: Annotated[bool, Body(..., embed=True, description="New public state")],
) -> Bucket:
    """
    Update the buckets public state.

    Permission `bucket:update` required if the current user is the owner of the bucket,
    otherwise `bucket:update_any` required.
    \f
    Parameters
    ----------
    bucket : clowm.models.Bucket
        Bucket with the name provided in the URL path. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    bucket_core : clowm.api.core.BucketCore
        Core functionality for bucket API. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    public : bool
        The new public state. HTTP Body.

    Returns
    -------
    bucket : clowm.models.Bucket
        Bucket with the toggled public state.
    """
    trace.get_current_span().set_attributes({"bucket_name": bucket.name, "public": public})
    authorization(RBACOperation.UPDATE if bucket.owner_id == current_user.uid else RBACOperation.UPDATE_ANY)
    return await bucket_core.update_public_state(bucket=bucket, public=public)


@router.patch("/{bucket_name}/limits", response_model=BucketOutSchema, summary="Update bucket limits")
@start_as_current_span_async("api_update_bucket_limits", tracer=tracer)
async def update_bucket_limits(
    bucket: CurrentBucket,
    authorization: Authorization,
    crud_bucket: CrudBucketSession,
    rgw: RGWService,
    limits: Annotated[BucketSizeLimits, Body(description="New size limits for bucket")],
) -> Bucket:
    """
    Update the buckets size limits.

    Permission `bucket:update_any` required.
    \f
    Parameters
    ----------
    bucket : clowm.models.Bucket
        Bucket with the name provided in the URL path. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    crud_bucket : clowm.crud.crud_bucket.CRUDBucket
        Active CRUD repository for buckets. Dependency Injection.
    rgw : rgwadmin.RGWAdmin
        RGW admin interface to manage Ceph's object store. Dependency Injection.
    limits : clowm.schemas.bucket.BucketSizeLimits
        New bucket limits. HTTP Body.

    Returns
    -------
    bucket : clowm.models.Bucket
        Bucket with the updated limits
    """
    current_span = trace.get_current_span()
    current_span.set_attribute("bucket_name", bucket.name)
    if limits.size_limit is not None:  # pragma: no cover
        current_span.set_attribute("size_limit", ByteSize(limits.size_limit * 1024).human_readable())
    if limits.object_limit is not None:  # pragma: no cover
        current_span.set_attribute("object_limit", limits.object_limit)
    authorization(RBACOperation.UPDATE_ANY)
    await crud_bucket.update_bucket_limits(
        bucket_name=bucket.name, object_limit=limits.object_limit, size_limit=limits.size_limit
    )
    rgw_update_bucket_limits(rgw=rgw, bucket=bucket)
    return bucket


@router.delete("/{bucket_name}", status_code=status.HTTP_204_NO_CONTENT, summary="Delete a bucket")
@start_as_current_span_async("api_delete_bucket", tracer=tracer)
async def delete_bucket(
    bucket: CurrentBucket,
    current_user: CurrentUser,
    authorization: Authorization,
    bucket_core: BucketCoreDep,
    force_delete: Annotated[bool, Query(description="Delete even non-empty bucket")] = False,
) -> None:
    """
    Delete a bucket by its name. Only the owner of the bucket can delete the bucket.

    Permission `bucket:delete` required if the current user is the owner of the bucket,
    otherwise `bucket:delete_any` required.
    \f
    Parameters
    ----------
    force_delete : bool, default False
        Flag for deleting a non-empty bucket. Query parameter.
    bucket : clowm.models.Bucket
        Bucket with the name provided in the URL path. Dependency Injection.
    bucket_core : clowm.api.core.BucketCore
        Core functionality for bucket API. Dependency Injection.
    current_user : clowm.models.User
        Current user. Dependency Injection.
    authorization : Callable[[clowm.core.rbac.RBACOperation], None]
        Function to call determines if the current user is authorized for this request. Dependency Injection.
    """
    trace.get_current_span().set_attributes({"bucket_name": bucket.name, "force_delete": force_delete})
    authorization(RBACOperation.DELETE if bucket.owner_id == current_user.uid else RBACOperation.DELETE_ANY)
    await bucket_core.delete_bucket(bucket_name=bucket.name, force_delete=force_delete)
