from fastapi import APIRouter

from .workflow import router as workflow_router

router = APIRouter(prefix="/public")
router.include_router(workflow_router)
