from typing import Annotated

from fastapi import APIRouter, Depends, Response

from clowm.crud import CRUDUser, CRUDWorkflow
from clowm.models import WorkflowVersion
from clowm.schemas.workflow import PublicWorkflowOut

from ...dependencies import CRUDSessionDep

router = APIRouter(prefix="/workflows", tags=["Public"])

CrudWorkflowSession = Annotated[CRUDWorkflow, Depends(CRUDSessionDep(CRUDWorkflow))]


@router.get("", summary="Get available workflows")
async def get_public_workflows(crud_workflow: CrudWorkflowSession, response: Response) -> list[PublicWorkflowOut]:
    """
    Public route to fetch all available workflows.
    \f
    Parameters
    ----------
    crud_workflow : clowm.crud.crud_workflow.CRUDWorkflow
        Active CRUD repository for workflows. Dependency Injection.
    response : fastapi.Response
        Raw response object. Provided by fastapi

    Returns
    -------
    workflows : list[clowm.schemas.workflow.PublicWorkflowOut]
        List of available workflows.
    """
    workflows = await crud_workflow.list_workflows(
        version_status=[WorkflowVersion.WorkflowVersionStatus.PUBLISHED],
    )
    crud_user = CRUDUser(db=crud_workflow.db)
    # Cache for one hour in browser
    response.headers["cache-control"] = "max-age=3600"
    return [
        PublicWorkflowOut.from_db_model(workflow, developer=await crud_user.get(workflow.developer_id))
        for workflow in workflows
    ]
