import json
import os
import re
import shutil
import tempfile
from collections.abc import Sequence
from pathlib import Path
from typing import Any
from uuid import UUID, uuid4

from anyio import open_file
from fastapi import BackgroundTasks, HTTPException, Request, UploadFile, status
from fastapi.responses import StreamingResponse
from httpx import URL as httpxUrl
from httpx import AsyncClient, Auth
from opentelemetry import trace
from PIL import Image, UnidentifiedImageError
from pydantic import AnyHttpUrl
from rgwadmin import RGWAdmin
from starlette.background import BackgroundTasks as StarletteBackgroundTasks

from clowm.core.config import settings
from clowm.crud import CRUDResourceVersion, CRUDUser, CRUDWorkflowExecution
from clowm.models import ResourceVersion, User, WorkflowExecution, WorkflowMode
from clowm.schemas.documentation_enum import DocumentationEnum
from clowm.schemas.workflow_mode import WorkflowModeIn
from clowm.smtp.send_email import send_first_login_email

from ..utils.job import Job
from .background.resources import update_used_resources
from .background.s3 import process_and_upload_icon, upload_file_to_data_bucket
from .background.user import initialize_user

# regex to find resources in parameters of workflow execution
resource_regex = re.compile(r"/[\w\-/]*/CLDB-([a-f0-9]{32})/([a-f0-9]{32}|latest)")
main_script_regex = re.compile(r"manifest\s+{[\s\S]+mainScript\s+=\s+[\"']{1}(\S+)[\"']{1}")

tracer = trace.get_tracer_provider().get_tracer(__name__)


async def upload_icon(background_tasks: BackgroundTasks, icon: UploadFile) -> str:
    """
    Upload an icon to the icon bucket.

    Parameters
    ----------
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks.
    icon : fastapi.UploadFile
        Icon for the workflow version.

    Returns
    -------
    icon_slug : str
        Slug of the icon in the bucket
    """
    try:
        Image.open(icon.file)
        trace.get_current_span().set_attribute("uploaded_icon_size", icon.file.tell())
        icon.file.seek(0)
    except UnidentifiedImageError as err:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="icon needs to be an image") from err
    icon_slug = f"{uuid4().hex}.png"
    # Save the icon to a file to access it in a background task
    _, icon_filename = tempfile.mkstemp()
    async with await open_file(icon_filename, "wb") as f:
        size = 32000
        buffer = await icon.read(size)
        while len(buffer) > 0:
            await f.write(buffer)
            buffer = await icon.read(size)

    background_tasks.add_task(process_and_upload_icon, icon_slug=icon_slug, icon_buffer_file=Path(icon_filename))
    background_tasks.add_task(os.remove, path=icon_filename)
    return icon_slug


async def check_repo(repository_path: Path, modes: Sequence[WorkflowModeIn | WorkflowMode] | None = None) -> None:
    """
    Check if the necessary files are present in the git repository. If not, raises an HTTP Exception.

    Parameters
    ----------
    repository_path : pathlib.Path
        Path to the local folder containing the repository
    modes : list[clowm.schemas.workflow_mode.WorkflowModeIn | clowm.models.WorkflowMode] | None
        List of different mode of the workflow with different parameter schemas.
    """
    missing_files = []
    if modes is not None and len(modes) > 0:
        # If there are modes, add check if there parameter schem exist
        for mode in modes:
            if not (repository_path / mode.schema_path).is_file():
                missing_files.append(mode.schema_path)
    # check if main.nf exists and if not parse the mainScript location from the nextflow.config
    if not (repository_path / "main.nf").is_file():
        config_path = repository_path / "nextflow.config"
        if config_path.is_file():
            main_script_path = None
            # read in nextflow.config
            async with await open_file(config_path) as f:
                # find the mainScript in the files content
                for match in main_script_regex.finditer(await f.read()):
                    (main_script,) = match.groups()
                    main_script_path = repository_path / main_script
            if main_script_path is None:
                # mainScript is not referenced -> main.nf is missing
                missing_files.append("main.nf")
            elif not main_script_path.is_file():
                # mainScript is referenced but the script is missing
                missing_files.append(main_script)
        else:
            # nextflow.config is missing and therefore no entrypoint to the workflow
            missing_files.append("main.nf")
    for doc_file in DocumentationEnum:
        # clowm_info.json and CITATION.md are optional
        if doc_file == DocumentationEnum.CLOWM_INFO or doc_file == DocumentationEnum.CITATIONS:
            continue
        # if there are modes, ignore the default paths for parameter schemas
        if doc_file == DocumentationEnum.PARAMETER_SCHEMA and modes is not None and len(modes) > 0:
            continue
        relative_repository_path = None
        # loop over possible locations of the repository path of this doc file
        for relative_repository_path in doc_file.repository_paths:
            doc_path = repository_path / relative_repository_path
            if doc_path.is_file():
                break
        else:  # loop runs without break -> doc file is missing
            if relative_repository_path is not None:
                missing_files.append(relative_repository_path)

    if len(missing_files) > 0:
        trace.get_current_span().set_attribute("missing_files", missing_files)
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"the files {', '.join(missing_files)} are missing in the repo",
        )


async def upload_documentation_files(
    repository_path: Path,
    git_commit_hash: str,
    modes: list[WorkflowMode] | None = None,
    *,
    background_tasks: BackgroundTasks,
) -> None:
    """
    Upload the workflow documentation files from a local folder to the S3 data cache.

    Parameters
    ----------
    repository_path : pathlib.Path
        Path to the local folder containing the repository
    git_commit_hash : str
        The git commit hash of the workflow version
    modes : list[clowm.models.WorkflowMode] | None, default None
        The modes of the workflow
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks
    """
    if modes is not None and len(modes) > 0:
        for mode in modes:
            _, temp_file = tempfile.mkstemp()
            # Copy file to upload it in a background thread
            shutil.copyfile(repository_path / mode.schema_path, temp_file)
            background_tasks.add_task(
                upload_file_to_data_bucket,
                path=Path(temp_file),
                key=settings.s3.workflow_cache_key(
                    name=DocumentationEnum.PARAMETER_SCHEMA,
                    git_commit_hash=git_commit_hash,
                    mode_id=mode.mode_id,
                ),
            )
            background_tasks.add_task(os.remove, path=temp_file)
    # Cache all relevant files in the data bucket
    for doc_file in DocumentationEnum:
        _, temp_file = tempfile.mkstemp()
        for relative_repository_path in doc_file.repository_paths:
            doc_path = repository_path / relative_repository_path
            if doc_path.is_file():
                shutil.copyfile(doc_path, temp_file)
                break
        background_tasks.add_task(
            upload_file_to_data_bucket,
            path=Path(temp_file),
            key=settings.s3.workflow_cache_key(name=doc_file, git_commit_hash=git_commit_hash),
        )
        background_tasks.add_task(os.remove, path=temp_file)


def create_rgw_user(user: User, background_tasks: BackgroundTasks, rgw: RGWAdmin) -> None:
    """
    Create the user in RGW and initializes him in the background.

    Parameters
    ----------
    user : clowm.models.User
        User that should be created.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks
    rgw : rgwadmin.RGWAdmin
        RGW admin interface to manage Ceph's object store.
    """
    with tracer.start_as_current_span(
        "rgw_create_user", attributes={"uid": str(user.uid), "display_name": user.display_name}
    ):
        rgw.create_user(
            uid=str(user.uid),
            max_buckets=-1,
            display_name=user.display_name,
        )

    background_tasks.add_task(initialize_user, user=user)
    background_tasks.add_task(send_first_login_email, user=user)


async def check_active_workflow_execution_limit(uid: UUID, *, crud_workflow_execution: CRUDWorkflowExecution) -> None:
    """
    Check the number of active workflow executions of a user and raise an HTTP exception if a new one would violate the
    limit of active workflow executions.

    Parameters
    ----------
    crud_workflow_execution : clowm.crud.crud_workflow_execution.CRUDWorkflowExecution
        Active CRUD repository for workflow executions.
    uid : str
        ID of a user.
    """
    active_executions = await crud_workflow_execution.list(
        executor_id=uid, status_list=WorkflowExecution.WorkflowExecutionStatus.active_workflows()
    )
    user = await CRUDUser(db=crud_workflow_execution.db).get(uid)
    # If there is a specific limit for the user
    if user is not None and user.max_parallel_executions is not None:
        if len(active_executions) + 1 > user.max_parallel_executions:
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail=f"your active workflow execution limit is {user.max_parallel_executions}",
            )
        return
    # if there is no specific limit, use the general limit
    if -1 < settings.cluster.active_workflow_execution_limit < len(active_executions) + 1:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail=f"the active workflow execution limit per user is {settings.cluster.active_workflow_execution_limit}",
        )


async def check_used_resources(
    parameters: dict[str, Any], *, crud_resource_version: CRUDResourceVersion, background_tasks: BackgroundTasks
) -> None:
    """
    Check which resource are used in a workflow execution.

    Parameters
    ----------
    parameters : dict[str, Any]
        Parameters for the workflow execution
    crud_resource_version : clowm.crud.crud_resource_version.CRUDResourceVersion
        Active CRUD repository for resource versions.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks
    """
    with tracer.start_as_current_span("check_used_resources"):
        used_rvid: list[UUID] = []
        for match in resource_regex.finditer(json.dumps(parameters)):
            rid, rvid = match.groups()
            resource_version = (
                await crud_resource_version.get(
                    resource_id=UUID(hex=rid),
                    resource_version_id=UUID(hex=rvid),
                )
                if rvid != "latest"
                else await crud_resource_version.get_latest(
                    resource_id=UUID(hex=rid),
                )
            )
            if resource_version is None or resource_version.status not in [
                ResourceVersion.ResourceVersionStatus.SYNCHRONIZED,
                ResourceVersion.ResourceVersionStatus.LATEST,
                ResourceVersion.ResourceVersionStatus.SETTING_LATEST,
            ]:
                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND,
                    detail=f"resource at path {match.group(0)} does not exist on cluster",
                )
            used_rvid.append(resource_version.resource_version_id)
    if len(used_rvid) > 0:
        background_tasks.add_task(update_used_resources, ids=used_rvid)


async def stream_response_from_url(
    url: AnyHttpUrl | httpxUrl,
    request: Request,
    media_type: str | None = None,
    auth: Auth | None = None,
    background_job: Job | None = None,
    *,
    client: AsyncClient,
) -> StreamingResponse:
    """
    Stream the response from an URL to the client.

    Parameters
    ----------
    url : pydantic.AnyHttpUrl | httpx.URL
        URL where the response should be streamed
    request : fastapi.Request
        Raw request object
    media_type : str | None, default None
        Media type of the return response
    auth : httpx.Auth | None, default None
        The authentication for the request to the url
    background_job : clowm.utils.Job | None, default None
        Job that should run in the background, after the response is finished
    client : httpx.AsyncClient
        HTTP Client with an open connection. Dependency Injection.

    Returns
    -------
    stream : fastapi.responses.StreamingResponse
        The streamed response from the url
    """
    background_tasks = StarletteBackgroundTasks()
    url_request_headers = {}
    response_headers = {"cache-control": "max-age=86400"}
    if request.headers.get("accept-encoding", None) is not None:  # pragma: no cover
        url_request_headers["accept-encoding"] = request.headers["accept-encoding"]
    if request.headers.get("etag", None) is not None:  # pragma: no cover
        url_request_headers["etag"] = request.headers["etag"]
    req = client.build_request(
        method="GET",
        url=str(url),
        headers=url_request_headers,
    )
    with tracer.start_as_current_span(
        "stream_response_from_url",
        attributes={
            "url": str(url),
            **{"stream.request.headers." + key: val for key, val in url_request_headers.items()},
        },
    ) as span:
        r = await client.send(
            req,
            stream=True,
            follow_redirects=True,
            auth=auth,
        )
        span.set_attributes({"response.headers." + key: val for key, val in response_headers.items()})

    if r.headers.get("content-length", None) is not None:  # pragma: no cover
        response_headers["content-length"] = r.headers["content-length"]
    if r.headers.get("content-encoding", None) is not None:  # pragma: no cover
        response_headers["content-encoding"] = r.headers["content-encoding"]
    if r.headers.get("etag", None) is not None:  # pragma: no cover
        response_headers["etag"] = r.headers["etag"]
    background_tasks.add_task(r.aclose)
    if background_job is not None:
        background_tasks.add_task(background_job.func, *background_job.args, **background_job.kwargs)
    return StreamingResponse(
        r.aiter_raw(),
        status_code=r.status_code,
        headers=response_headers,
        background=background_tasks,
        media_type=media_type,
    )


FlatParameters = dict[str, str | bool | float | int]


def flatten_parameters(params: dict[str, Any], parents: list[str] | None = None) -> FlatParameters:
    """
    Flatten the nested parameters for the parameter validation.

    Parameters
    ----------
    params : dict[str, Any]
        Nested parameters.
    parents : list[str] | None, default None
        Parameter for recursion. Should be None.

    Returns
    -------
    flat_parameters : dict[str, str | bool | float | int]
        Flatten parameters
    """
    parent_list = [] if parents is None else parents
    new_params: FlatParameters = {}
    for param_name in params:
        if isinstance(params[param_name], dict):
            new_params = {**new_params, **flatten_parameters(params[param_name], [*parent_list, param_name])}
            continue
        new_params[".".join([*parent_list, param_name])] = params[param_name]
    return new_params
