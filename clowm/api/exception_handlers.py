from fastapi import Request, Response, status
from fastapi.responses import RedirectResponse

from clowm.core.config import settings
from clowm.core.oidc import AuthError

from .dependencies.website.auth import cookie_jwt_scheme

__all__ = ["auth_exception_handler", "AccountConnectionError", "LoginError"]


class AccountConnectionError(AuthError):
    """
    Exception class when something went wrong during the account connection with different OIDC provider
    """

    @property
    def _error_url_param(self) -> str:
        return "link_error"

    @property
    def _redirect_path(self) -> str:
        return "profile"


class LoginError(AuthError):
    """
    Exception class when something went wrong during the login.
    """

    @property
    def _error_url_param(self) -> str:
        return "login_error"

    @property
    def _redirect_path(self) -> str:
        return "login"


def auth_exception_handler(request: Request, exc: AuthError) -> Response:
    """
    Exception handler for all kinds of Auth errors.

    Parameters
    ----------
    request : fastapi.Request
        Original request where the exception occurred.
    exc : AuthError
        The exception that was raised.

    Returns
    -------
    redirect : fastapi.Response
        Redirect to base URL with error as query parameter
    """
    response = RedirectResponse(str(exc.redirect_url), status_code=status.HTTP_302_FOUND)
    if isinstance(exc, LoginError):
        response.delete_cookie(
            key=cookie_jwt_scheme.model.name, secure=True, domain=settings.ui_uri.host, httponly=False
        )
    return response
