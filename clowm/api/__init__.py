from fastapi import APIRouter

from .miscellaneous_endpoints import miscellaneous_router
from .trace_endpoints import router as trace_router
from .v1 import api_router
from .website import ui_router

clowm_router = APIRouter()
__all__ = ["clowm_router"]

clowm_router.include_router(api_router)
clowm_router.include_router(ui_router)
clowm_router.include_router(miscellaneous_router)
clowm_router.include_router(trace_router)
