from uuid import UUID

from fastapi import HTTPException, status
from opentelemetry import trace

from clowm.models import User

from ..core.rbac import RBACOperation
from ..crud import CRUDUser
from ..crud.mixins import OTRCrudMixin
from ..db.mixins import OwnershipTransferRequestMixin
from ..schemas.ownership_transfer import OwnershipTransferRequestIn, OwnershipTransferRequestOut
from .dependencies.auth import AuthorizationFunction


async def create_otr(
    target: OwnershipTransferRequestMixin,
    current_user: User,
    authorization: AuthorizationFunction,
    otr_info: OwnershipTransferRequestIn,
    crud: OTRCrudMixin,
) -> OwnershipTransferRequestOut:
    """
    Create an OTR for a target.

    Parameters
    ----------
    target : clowm.db.mixins.ownership_transfer_mixin.OwnershipTransferRequestMixin
        Target to create the OTR for.
    current_user : clowm.models.User
        Current user.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization.
    otr_info : clowm.schemas.ownership_transfer.OwnershipTransferRequestIn
        Infos for the OTR.
    crud : clowm.crud.mixins.otr_crud_mixin.OTRCrudMixin
        Active CRUD repository for OTRs.

    Returns
    -------
    otr : clowm.schemas.ownership_transfer.OwnershipTransferRequestOut
        Created OTR for the target
    """
    authorization(RBACOperation.UPDATE if target.clowm_owner_id == current_user.uid else RBACOperation.UPDATE_ANY)
    if target.clowm_owner_id == otr_info.new_owner_uid:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"user '{target.clowm_owner_id}' is already owner of the {type(target).__name__.lower()}",
        )
    if await CRUDUser(db=crud.db).get(otr_info.new_owner_uid) is None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"user '{otr_info.new_owner_uid}' does not exist",
        )
    await crud.create_otr(target_id=target.clowm_id, new_owner_uid=otr_info.new_owner_uid, comment=otr_info.comment)
    return OwnershipTransferRequestOut.from_db_model(target)


def get_otr(
    target: OwnershipTransferRequestMixin, current_user: User, authorization: AuthorizationFunction
) -> OwnershipTransferRequestOut:
    """
    Extract the OTR information from a target if the OTR exists.

    Parameters
    ----------
    target : clowm.db.mixins.ownership_transfer_mixin.OwnershipTransferRequestMixin
        Target to extract the info from.
    current_user : clowm.models.User
        Current user.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization.

    Returns
    -------
    otr : clowm.schemas.ownership_transfer.OwnershipTransferRequestOut
        OTR if it exists
    """
    authorization(
        RBACOperation.READ
        if target.clowm_owner_id == current_user.uid or target.transfer_new_owner_uid == current_user.uid
        else RBACOperation.READ_ANY
    )
    if target.transfer_new_owner_uid is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"OTR for {type(target).__name__.lower()} '{target.clowm_id}' does not exist",
        )
    return OwnershipTransferRequestOut.from_db_model(target)


async def list_otrs(
    current_user: User,
    authorization: AuthorizationFunction,
    current_owner_id: UUID | None = None,
    new_owner_id: UUID | None = None,
    *,
    crud: OTRCrudMixin,
) -> list[OwnershipTransferRequestOut]:
    """
    List OTRs from specified target type.

    Parameters
    ----------
    current_owner_id : uuid.UUID
        ID of user who is the current owner. Query Parameter.
    new_owner_id : uuid.UUID
        ID of user who will be the new owner. Query Parameter.
    current_user : clowm.models.User
        Current user.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization.
    crud : clowm.crud.mixins.otr_crud_mixin.OTRCrudMixin
        Active CRUD repository for OTRs.

    Returns
    -------
    otr : list[clowm.schemas.ownership_transfer.OwnershipTransferRequestOut]
        OTRs of requested type in DB
    """
    current_span = trace.get_current_span()
    if current_owner_id is not None:  # pragma: no cover
        current_span.set_attribute("owner_id", str(current_owner_id))
    if new_owner_id is not None:  # pragma: no cover
        current_span.set_attribute("owner_id", str(new_owner_id))
    authorization(
        RBACOperation.LIST
        if current_user.uid == new_owner_id or current_user.uid == current_owner_id
        else RBACOperation.LIST_ALL
    )
    otrs = await crud.list_otrs(
        filter_combination_union=False, current_owner_id=current_owner_id, new_owner_id=new_owner_id
    )
    return [OwnershipTransferRequestOut.from_db_model(otr) for otr in otrs]


async def delete_otr(
    target: OwnershipTransferRequestMixin, current_user: User, authorization: AuthorizationFunction, crud: OTRCrudMixin
) -> None:
    """
    Delete the OTR information from a target if the OTR exists.

    Parameters
    ----------
    target : clowm.db.mixins.ownership_transfer_mixin.OwnershipTransferRequestMixin
        Target to delete OTR from.
    current_user : clowm.models.User
        Current user.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization.
    crud : clowm.crud.mixins.otr_crud_mixin.OTRCrudMixin
        Active CRUD repository for OTRs.
    """
    authorization(
        RBACOperation.UPDATE
        if target.clowm_owner_id == current_user.uid or target.transfer_new_owner_uid == current_user.uid
        else RBACOperation.UPDATE_ANY
    )
    if target.transfer_new_owner_uid is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"OTR for {type(target).__name__.lower()} '{target.clowm_id}' does not exist",
        )
    if target.transfer_new_owner_uid == current_user.uid:
        # OTR was rejected
        pass
    else:
        # OTR was deleted by owner or Administrator
        pass
    await crud.update_otr(target_id=target.clowm_id, accept=False)


async def accept_otr(
    target: OwnershipTransferRequestMixin,
    current_user: User,
    authorization: AuthorizationFunction,
    crud: OTRCrudMixin,
) -> None:
    """
    Accept an OTR if the OTR exists.

    Parameters
    ----------
    target : clowm.db.mixins.ownership_transfer_mixin.OwnershipTransferRequestMixin
        Target to delete OTR from.
    current_user : clowm.models.User
        Current user.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization.
    crud : clowm.crud.mixins.otr_crud_mixin.OTRCrudMixin
        Active CRUD repository for OTRs.
    """
    authorization(
        RBACOperation.UPDATE if target.transfer_new_owner_uid == current_user.uid else RBACOperation.UPDATE_ANY
    )
    if target.transfer_new_owner_uid is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"OTR for {type(target).__name__.lower()} '{target.clowm_id}' does not exist",
        )
    await crud.update_otr(target_id=target.clowm_id, accept=True)
