import logging

from alembic import command, config
from sqlalchemy import create_engine, text
from sqlalchemy.orm import sessionmaker
from tenacity import after_log, before_log, retry, stop_after_attempt, wait_fixed

from clowm.core.config import settings

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

max_tries = 60  # 2 minute
wait_seconds = 3


@retry(
    stop=stop_after_attempt(max_tries),
    wait=wait_fixed(wait_seconds),
    before=before_log(logger, logging.INFO),
    after=after_log(logger, logging.WARN),
)
def init() -> None:
    engine = create_engine(str(settings.db.dsn_sync), pool_pre_ping=True, pool_recycle=3600, echo=False)
    try:
        with sessionmaker(autocommit=False, autoflush=False, bind=engine)() as db:
            # Try to create session to check if DB is awake
            db.execute(text("SELECT 1"))
        alembic_config = config.Config("./alembic.ini")
        command.upgrade(alembic_config, "head")
    finally:
        engine.dispose()


def main() -> None:
    logger.info("Initializing DB")
    init()
    logger.info("DB finished initializing")


if __name__ == "__main__":
    main()
