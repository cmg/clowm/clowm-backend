# CloWM Backend

## Description

CloWM is a user-friendly system that allows researchers to start a workflow using the Web UI easily. With minimal
effort, workflow developers can quickly turn their versioned workflow into a web service that reviewers inspect before
publication. End users access CloWM with LS Login, a popular SSO provider in the life sciences community. They can
easily manage and upload their data into personalized S3 buckets and execute workflows on a scalable compute cluster.
The Web UI makes it simple to select a workflow by automatically loading all necessary documentation and generating a
user interface to input parameters and data for workflow execution.

## Role-based Access Control (RBAC)

The authorization of this system follows the principles of RBAC. Look at [RBAC.md](RBAC.md) to see which roles and
permissions there are and which roles has which permission.

## Configuration

See [CONFIG.md](CONFIG.md) for configuration options of the system.

## License

The API is licensed under the [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) license. See
the [License](LICENSE) file for more information
