#! /usr/bin/env sh
set -e

./prestart.sh

# Start Gunicorn
exec gunicorn -k uvicorn_worker.UvicornWorker -c /app/gunicorn_conf.py clowm.main:app
