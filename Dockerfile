FROM python:3.13-slim
ENV PORT=8000
EXPOSE $PORT
ENV OTEL_INSTRUMENTATION_HTTP_CAPTURE_HEADERS_SANITIZE_FIELDS="set-cookie,cookie,x-clowm-token"
ENV OTEL_INSTRUMENTATION_HTTP_CAPTURE_HEADERS_SERVER_RESPONSE=".*"

# dumb-init forwards the kill signal to the python process
RUN apt-get update && apt-get -y install dumb-init && apt-get clean
ENTRYPOINT ["/usr/bin/dumb-init", "--"]
STOPSIGNAL SIGINT
RUN pip install --no-cache-dir --disable-pip-version-check httpx[cli] "uvicorn[standard]<0.33.0"

HEALTHCHECK --interval=5s --timeout=2s CMD httpx http://localhost:$PORT/health || exit 1


RUN useradd -m worker
USER worker
WORKDIR /home/worker/code
ENV PYTHONPATH=/home/worker/code
ENV PATH="/home/worker/.local/bin:${PATH}"

COPY ./start_service_uvicorn.sh ./entrypoint.sh
COPY scripts/prestart.sh ./prestart.sh
CMD ["./entrypoint.sh"]

COPY --chown=worker:worker alembic.ini alembic.ini
COPY --chown=worker:worker requirements.txt ./requirements.txt

RUN pip install --user --disable-pip-version-check --no-cache-dir --upgrade -r requirements.txt

COPY --chown=worker:worker ./migrations ./migrations
COPY --chown=worker:worker ./clowm ./clowm

ARG GIT_COMMIT_HASH=latest
ENV CLOWM_GIT_COMMIT_HASH=${GIT_COMMIT_HASH}
