# Configuration

## General

| Env variable                | Config file key       | Default       | Value    | Example                | Description                                                                                                                      |
|-----------------------------|-----------------------|---------------|----------|------------------------|----------------------------------------------------------------------------------------------------------------------------------|
| `CLOWM_CONFIG_FILE_YAML`    | -                     | `config.yaml` | Filepath | `/path/to/config.yaml` | Path to a YAML file to read the config. See [example-config/example-config.yaml](example-config/example-config.yaml).            |
| `CLOWM_CONFIG_FILE_TOML`    | -                     | `config.toml` | Filepath | `/path/to/config.toml` | Path to a TOML file to read the config. See [example-config/example-config.toml](example-config/example-config.toml).            |
| `CLOWM_CONFIG_FILE_JSON`    | -                     | `config.json` | Filepath | `/path/to/config.json` | Path to a JSON file to read the config. See [example-config/example-config.json](example-config/example-config.json).            |
| `CLOWM_API_PREFIX`          | `api_prefix`          | unset         | URI path | `/api`                 | Prefix before every URL path                                                                                                     |
| * `CLOWM_UI_URI`            | `ui_uri`              | unset         | HTTP URL | `https://localhost`    | HTTP URL of the CloWM website                                                                                                    |
| `CLOWM_BLOCK_FOREIGN_USERS` | `block_foreign_users` | `false`       | boolean  | `false`                | Block users that have no role                                                                                                    |
| `DEV_SYSTEM`                | `dev_system`          | `false`       | Boolean  | `false`                | Activates an endpoint that allows execution of an workflow from an arbitrary Git Repository.<br>HAS TO BE `False` in PRODUCTION! |

## Database

| Env variable           | Config file key | Default     | Value              | Example       | Description                                                    |
|------------------------|-----------------|-------------|--------------------|---------------|----------------------------------------------------------------|
| `CLOWM_DB__HOST`       | `db.host`       | `localhost` | <db hostname / IP> | `localhost`   | IP or Hostname Address of DB                                   |
| `CLOWM_DB__PORT`       | `db.port`       | 3306        | Integer            | 3306          | Port of the database                                           |
| * `CLOWM_DB__USER`     | `db.user`       | unset       | String             | `db-user`     | Username of the database user                                  |
| * `CLOWM_DB__PASSWORD` | `db.password`   | unset       | String             | `db-password` | Password of the database user                                  |
| * `CLOWM_DB__NAME`     | `db.name`       | unset       | String             | `db-name`     | Name of the database                                           |
| `CLOWM_DB__VERBOSE`    | `db.verbose`    | `false`     | Boolean            | `false`       | Enables verbose SQL output.<br>Should be `false` in production |

## Email

| Variable                          | Config file key            | Default              | Value                   | Example              | Description                                                                                                |
|-----------------------------------|----------------------------|----------------------|-------------------------|----------------------|------------------------------------------------------------------------------------------------------------|
| `CLOWM_SMTP__SERVER`              | `smtp.server`              | unset                | SMTP domain / `console` | `localhost`          | Hostname of SMTP server. If `console`, emails are printed to the console. If not set, emails are not sent. |
| `CLOWM_SMTP__PORT`                | `smtp.port`                | 587                  | Integer                 | 587                  | Port of the SMTP server                                                                                    |
| `CLOWM_SMTP__SENDER_EMAIL`        | `smtp.sender_email`        | `no-reply@clowm.com` | Email                   | `no-reply@clowm.com` | Email address from which the emails are sent.                                                              |
| `CLOWM_SMTP__REPLY_EMAIL`         | `smtp.reply_email`         | unset                | Email                   | `clowm@example.org`  | Email address in the `Reply-To` header.                                                                    |
| `CLOWM_SMTP__CONNECTION_SECURITY` | `smtp.connection_security` | unset                | `starttls` / `ssl`      | `starttls`           | Connection security to the SMTP server.                                                                    |
| `CLOWM_SMTP__LOCAL_HOSTNAME`      | `smtp.local_hostname`      | unset                | String                  | `clowm.local`        | Overwrite the local hostname from which the emails are sent.                                               |
| `CLOWM_SMTP__CA_PATH`             | `smtp.ca_path`             | unset                | Filepath                | `/path/to/ca.pem`    | Path to a custom CA certificate.                                                                           |
| `CLOWM_SMTP__KEY_PATH`            | `smtp.key_path`            | unset                | Filepath                | `/path/to/key.pem`   | Path to the CA key.                                                                                        |
| `CLOWM_SMTP__USER`                | `smtp.user`                | unset                | String                  | `smtp-user`          | Username to use for SMTP login.                                                                            |
| `CLOWM_SMTP__PASSWORD`            | `smtp.password`            | unset                | String                  | `smtp-password`      | Password to use for SMTP login.                                                                            |

## S3

| Env variable                            | Config file key                  | Default          | Value    | Example                    | Description                                                                      |
|-----------------------------------------|----------------------------------|------------------|----------|----------------------------|----------------------------------------------------------------------------------|
| * `CLOWM_S3__URI`                       | `s3.uri`                         | unset            | HTTP URL | `http://localhost`         | URI of the S3 Object Storage                                                     |
| * `CLOWM_S3__ACCESS_KEY`                | `s3.acess_key`                   | unset            | String   | `ZR7U56KMK20VW`            | Access key for the S3 that owns the buckets and `user=*,bucket=*` capabilities   |
| * `CLOWM_S3__SECRET_KEY`                | `s3.secret_key`                  | unset            | String   | `9KRUU41EGSCB3H9ODECNHW`   | Secret key for the S3 that owns the buckets                                      |
| `CLOWM_S3__USERNAME`                    | `s3.username`                    | `bucket-manager` | String   | `bucket-manager`           | ID of the user in ceph who owns all the buckets. Owner of `CLOWM_S3__ACCESS_KEY` |
| `CLOWM_S3__INITIAL_BUCKET_SIZE_LIMIT`   | `s3.initial_bucket_size_limit`   | `100 GiB`        | ByteSize | `10 KB`, `10 KiB`, `10 MB` | Size limit of the initial bucket. Between `1 KiB` and `4.3 TB`                   |
| `CLOWM_S3__INITIAL_BUCKET_OBJECT_LIMIT` | `s3.initial_bucket_object_limit` | `10000`          | Integer  | `10000`                    | Number of object limit in the initial bucket. Must be $<2^{32}$                  |
| `CLOWM_S3__DEFAULT_BUCKET_SIZE_LIMIT`   | `s3.default_bucket_size_limit`   | `400 GiB`        | ByteSize | `10 KB`, `10 KiB`, `10 MB` | Size limit of a new Bucket. Between `1 KiB` and `4.3 TB`                         |
| `CLOWM_S3__DEFAULT_BUCKET_OBJECT_LIMIT` | `s3.default_bucket_object_limit` | `40000`          | Integer  | `10000`                    | Maximum number of objects in a new bucket. Must be $<2^{32}$                     |
| `CLOWM_S3__DATA_BUCKET`                 | `s3.data_bucket`                 | `clowm-data`     | String   | `clowm-data`               | Bucket where the workflow execution parameters should be saved                   |

## Security

| Env variable                                   | Config file key                    | Default            | Value                           | Example        | Description                                                                                   |
|------------------------------------------------|------------------------------------|--------------------|---------------------------------|----------------|-----------------------------------------------------------------------------------------------|
| `CLOWM_PRIVATE_KEY` / `CLOWM_PRIVATE_KEY_FILE` | `private_key` / `private_key_file` | randomly generated | Public Key / Path to Public Key | `/path/to/key` | Private part of RSA Key in PEM format to sign JWTs. Public part is inferred from private key. |
| `CLOWM_JWT_TOKEN_EXPIRE_MINUTES`               | `jwt_token_expire_minutes`         | 7 days             | number                          | 11520          | Minutes till a JWT expires                                                                    |
| `CLOWM_SECRET_KEY`                             | `secret_key`                       | randomly generated | string                          | `xxxx`         | Secret key to sign Session Cookies                                                            |

## Lifescience OIDC

| Env variable                                  | Config file key                        | Default                                                                     | Value    | Example | Description                                         |
|-----------------------------------------------|----------------------------------------|-----------------------------------------------------------------------------|----------|---------|-----------------------------------------------------|
| * `CLOWM_LIFESCIENCE_OIDC__CLIENT_ID`         | `lifescience_oidc.client_id`           | unset                                                                       | string   | `xxx`   | OIDC Client secret                                  |
| * `CLOWM_LIFESCIENCE_OIDC__CLIENT_SECRET`     | `lifescience_oidc.client_secret`       | unset                                                                       | string   | `xxx`   | OIDC Client ID                                      |
| `CLOWM_LIFESCIENCE_OIDC__SCOPES`              | `lifescience_oidc.scopes`              | `openid profile email`                                                      | string   | -       | Scopes this LifeScience AAI client will ask for     |
| `CLOWM_LIFESCIENCE_OIDC__SERVER_METADATA_URL` | `lifescience_oidc.server_metadata_url` | `https://login.aai.lifescience-ri.eu/oidc/.well-known/openid-configuration` | HTTP URL | -       | URL where to fetch the config for the OIDC provider |

## NFDI OIDC

| Env variable                           | Config file key                 | Default                                                               | Value    | Example | Description                                         |
|----------------------------------------|---------------------------------|-----------------------------------------------------------------------|----------|---------|-----------------------------------------------------|
| * `CLOWM_NFDI_OIDC__CLIENT_ID`         | `nfdi_oidc.client_id`           | unset                                                                 | string   | `xxx`   | OIDC Client secret                                  |
| * `CLOWM_NFDI_OIDC__CLIENT_SECRET`     | `nfdi_oidc.client_secret`       | unset                                                                 | string   | `xxx`   | OIDC Client ID                                      |
| `CLOWM_NFDI_OIDC__SCOPES`              | `nfdi_oidc.scopes`              | `openid email`                                                        | string   | -       | Scopes this NFDI AAI client will ask for            |
| `CLOWM_NFDI_OIDC__SERVER_METADATA_URL` | `nfdi_oidc.server_metadata_url` | `https://infraproxy.nfdi-aai.dfn.de/.well-known/openid-configuration` | HTTP URL | -       | URL where to fetch the config for the OIDC provider |

## Cluster

| Env variable                                     | Config file key                           | Default               | Value                                      | Example                    | Description                                                                       |
|--------------------------------------------------|-------------------------------------------|-----------------------|--------------------------------------------|----------------------------|-----------------------------------------------------------------------------------|
| * `CLOWM_CLUSTER__TOWER_SECRET`                  | `cluster.tower_secret`                    | unset                 | string                                     | `tiI-h_PiFt7HqOc38ekbcQ`   | Shared secret between cluster and logging service                                 |
| * `CLOWM_CLUSTER__SLURM__URI`                    | `cluster.slurm.uri`                       | unset                 | HTTP Url                                   | `http://localhost`         | HTTP URL to communicate with the Slurm cluster                                    |
| * `CLOWM_CLUSTER__SLURM__TOKEN`                  | `cluster.slurm.token`                     | unset                 | String                                     | -                          | JWT for communication with the Slurm REST API.                                    |
| `CLOWM_CLUSTER__SLURM__VERSION`                  | `cluster.slurm.version`                   | `v0.0.42`             | String                                     | `v0.0.42`                  | Version of the Slurm REST API to use.                                             |
| `CLOWM_CLUSTER__SLURM__PARTITION`                | `cluster.slurm.partition`                 | unset                 | String                                     | `nextflow`                 | Partition where all jobs should be scheduled to.                                  |
| `CLOWM_CLUSTER__NXF_CONFIG`                      | `cluster.nxf_config`                      | unset                 | Path on slurm cluster                      | `/path/to/nextflow.config` | Configuration file on the slurm cluster that is the same for every nextflow run   |
| `CLOWM_CLUSTER__NXF_BIN`                         | `cluster.nxf_bin`                         | `nextflow`            | Path on slurm cluster                      | `/path/to/nextflow`        | Path to the nextflow executable. Default it is in the `PATH`                      |
| `CLOWM_CLUSTER__WORKING_DIRECTORY`               | `cluster.working_directory`               | `/tmp`                | Path on slurm cluster                      | `/path/to/directory`       | Working directory for the slurm job with the nextflow command                     |
| `CLOWM_CLUSTER__ACTIVE_WORKFLOW_EXECUTION_LIMIT` | `cluster.active_workflow_execution_limit` | `3`                   | Integer                                    | `3`                        | Limit of active workflow execution a user is allowed to have. `-1` means infinite |
| `CLOWM_CLUSTER__JOB_MONITORING`                  | `cluster.job_monitoring`                  | `EXPONENTIAL`         | `EXPONENTIAL,LINEAR,CONSTANT,NOMONITORING` | `EXPONENTIAL`              | Strategy for polling the slurm job status for monitoring the workflow execution   |
| `CLOWM_CLUSTER__EXECUTION_CLEANUP`               | `cluster.execution_cleanup`               | `true`                | Boolean                                    | `true`                     | Enable/Disable the cleanup after a workflow execution                             |
| `CLOWM_CLUSTER__RESOURCE_CLUSTER_PATH`           | `cluster.execution_cleanup`               | `/vol/data/databases` | Path on slurm cluster                      | `/vol/data/databases`      | Directory on the cluster where the resources should be downloaded to              |
| `CLOWM_CLUSTER__RESOURCE_CONTAINER_PATH`         | `cluster.execution_cleanup`               | `/vol/resources`      | Path                                       | `/vol/resources`           | Directory in the container where al resources are readonly available              |
| `CLOWM_CLUSTER__HEAD_JOB_CPUS`                   | `cluster.head_job_cpus`                   | 2                     | Integer                                    | 2                          | Number of CPUs for the Nextflow head job                                          |
| `CLOWM_CLUSTER__HEAD_JOB_MEMORY`                 | `cluster.head_job_memory`                 | `4GiB`                | ByteSize                                   | `4GiB`                     | Available RAM for the Nextflow head job. Enforced by the JVM.                     |

## Monitoring

| Env variable                | Config file key      | Default | Value   | Example     | Description                                                                                  |
|-----------------------------|----------------------|---------|---------|-------------|----------------------------------------------------------------------------------------------|
| `CLOWM_OTLP__GRPC_ENDPOINT` | `otlp.grpc_endpoint` | unset   | String  | `localhost` | OTLP compatible endpoint to send traces via gRPC, e.g. Jaeger. If unset, no traces are sent. |
| `CLOWM_OTLP__SECURE`        | `otlp.secure`        | `false` | Boolean | `false`     | Connection type                                                                              |

### Nextflow Variables

All environment variables with the prefix `WORKFLOW_ENV_` are included in the environment of the nextflow job of the
cluster with the prefix cut off.

Example:
Pin the nextflow version
`WORKFLOW_ENV_NXF_VER=23.04.0` -> `NXF_VER=23.04.0`
