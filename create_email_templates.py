import argparse
from pathlib import Path
from sys import stderr


def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description="Helper script to create email template files")
    parser.add_argument("names", help="Name of the template", nargs="+")
    parser.add_argument(
        "-t", "--template-path", help="Path to the templates", type=Path, default="./clowm/smtp/templates"
    )
    return parser


def create(name: str, path: Path):
    """
    Create the template files in the subdirectory 'html' and 'plain' of the path.

    Parameters
    ----------
    name : str
        Name of the template
    path : pathlib.Path
        Path to the template directory
    """
    print("Template", name)
    html_file = path / f"html/{name}.html.tmpl"
    if html_file.is_file():
        print(f"Template {html_file.absolute()} already exists. Skip...")
    else:
        with open(html_file, "w") as f:
            f.write('<%inherit file="base.html.tmpl"/>\n')
        print(f"Template {html_file.absolute()} created")
    txt_file = path / f"plain/{name}.txt.tmpl"
    if txt_file.is_file():
        print(f"Template {txt_file.absolute()} already exists. Skip...")
    else:
        with open(txt_file, "w") as f:
            f.write('<%inherit file="base.txt.tmpl"/>\n')
        print(f"Template {txt_file.absolute()} created")


if __name__ == "__main__":
    parser = create_parser()
    args = parser.parse_args()
    for dir_name in ["html", "plain"]:
        dir_path = args.template_path / dir_name
        if not dir_path.is_dir():
            print(f"Directory {dir_path.absolute()} does not exist. Can't create templates.", file=stderr)
            exit(1)
    for tmpl_name in args.names:
        create(name=tmpl_name, path=args.template_path)
