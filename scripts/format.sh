#!/bin/sh -e
set -x

ruff --version
ruff check --fix --show-fixes clowm migrations
ruff format clowm migrations
