#!/usr/bin/env bash

set -x

ruff --version
ruff check clowm migrations
ruff format --diff clowm migrations

mypy --version
mypy clowm migrations
