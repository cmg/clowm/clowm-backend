#! /usr/bin/env bash

set -e

# Check Connection to Ceph RGW
python clowm/check_ceph_connection.py
# Check Connection to OIDC provider
python clowm/check_oidc_connection.py
# Check Connection to Slurm cluster
python clowm/check_slurm_connection.py
# Let the DB start
python clowm/check_database_connection.py
