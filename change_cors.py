import boto3

from clowm.core.config import settings

s3 = boto3.resource(
    "s3",
    aws_access_key_id=settings.s3.access_key,
    aws_secret_access_key=settings.s3.secret_key.get_secret_value(),
    endpoint_url=str(settings.s3.uri).strip("/"),
)
client = boto3.client(
    "s3",
    aws_access_key_id=settings.s3.access_key,
    aws_secret_access_key=settings.s3.secret_key.get_secret_value(),
    endpoint_url=str(settings.s3.uri).strip("/"),
)

# See clowm/ceph/s3.py
cors_rule = {
    "CORSRules": [
        {
            "ID": "websiteaccess",
            "AllowedHeaders": [
                "amz-sdk-invocation-id",
                "amz-sdk-request",
                "authorization",
                "content-type",
                "x-amz-sdk-checksum-algorithm",
                "x-amz-checksum-crc32",
                "x-amz-content-sha256",
                "x-amz-copy-source",
                "x-amz-date",
                "x-amz-user-agent",
                "content-md5",
            ],
            "AllowedMethods": ["GET", "PUT", "POST", "DELETE", "HEAD"],
            "AllowedOrigins": [str(settings.ui_uri).strip("/")],
            "ExposeHeaders": ["Etag", "content-range", "content-length"],
            "MaxAgeSeconds": 100,
        },
    ]
}

for b in client.list_buckets()["Buckets"]:
    print("Changing", b["Name"])
    bucket = s3.Bucket(b["Name"]).Cors().put(CORSConfiguration=cors_rule)
