#! /usr/bin/env bash
set -e

./prestart.sh

# Start webserver
uvicorn clowm.main:app --host 0.0.0.0 --port "$PORT" --no-server-header
